SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[viewBatteryRpt]
AS
SELECT     MAX(dbo.Assets.SerialNo) AS SerialNo, MAX(dbo.Assets.Description) AS Description, MAX(dbo.Assets.AssetStatusID) AS AssetStatusID, MAX(dbo.Assets.SiteID) 
                      AS SiteID, MIN(dbo.SessionDataCurrent.ChargeLevel) AS ChargeLevel, MIN(dbo.SessionDataCurrent.FullChargeCapacity) AS FullChargeCapacity, MIN(dbo.SessionDataCurrent.Temperature) 
                      AS Temperature, MAX(dbo.SessionDataCurrent.CycleCount) AS CycleCount, MAX(dbo.SessionDataCurrent.MaxCycleCount) AS MaxCycleCount, MIN(dbo.Assets.IDAssetType) 
                      AS IDAssetType, MIN(dbo.SessionDataCurrent.Move) AS Move, MIN(dbo.SessionDataCurrent.SSID) AS SSID, MAX(dbo.SessionDataCurrent.CreatedDateUTC) AS CreatedDateUTC, 
                      MIN(dbo.SessionDataCurrent.SourceIPAddress) AS SourceIPAddr, MIN(dbo.SessionDataCurrent.APMAC) AS APMAC, MIN(dbo.SessionDataCurrent.DeviceSerialNumber) 
                      AS DeviceSerialNumber, MIN(dbo.SessionDataCurrent.BatterySerialNumber) AS BatterySerialNumber
FROM         dbo.SessionDataCurrent RIGHT OUTER JOIN
                      dbo.Assets ON dbo.SessionDataCurrent.DeviceSerialNumber = dbo.Assets.SerialNo
WHERE     (dbo.Assets.IDAssetType IN (5, 9, 14, 15))
GROUP BY dbo.Assets.SerialNo

GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[26] 2[15] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Assets"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 260
               Right = 251
            End
            DisplayFlags = 280
            TopColumn = 12
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 33
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'viewBatteryRpt', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'viewBatteryRpt', NULL, NULL
GO
