SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[viewSiteAssets]
AS
SELECT        dbo.Sites.IDSite, dbo.Sites.SiteName, dbo.Assets.Description, dbo.Assets.SerialNo, dbo.Assets.AssetStatusID, dbo.Sites.CustomerID, dbo.Assets.IDAssetType, 
                         dbo.Departments.Description AS DeptDescription, dbo.Assets.FullChargeCapacity, dbo.Assets.CycleCount, dbo.Assets.Activity, dbo.Assets.OutOfService, 
                         dbo.Assets.AssetNumber, dbo.Assets.IsActive, dbo.Assets.InvoiceNumber, dbo.Assets.DeviceMAC, dbo.Assets.UtilizationLevel, dbo.Assets.Availability, 
                         dbo.Assets.WarrantyExpDate, dbo.Assets.FriendlyDescription, dbo.Assets.Notes, dbo.Assets.Temperature, dbo.Assets.ChargeLevel, dbo.Assets.LastPostDateUTC, 
                         dbo.Assets.DepartmentID, dbo.Assets.SiteID, dbo.Assets.IDAsset,  dbo.Assets.Wing, dbo.Assets.Floor
                         
FROM            dbo.Assets LEFT OUTER JOIN
                         dbo.Departments ON dbo.Assets.DepartmentID = dbo.Departments.IDDepartment FULL OUTER JOIN
                         dbo.Sites ON dbo.Assets.SiteID = dbo.Sites.IDSite

GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[25] 2[16] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Assets"
            Begin Extent = 
               Top = 6
               Left = 42
               Bottom = 221
               Right = 224
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Departments"
            Begin Extent = 
               Top = 6
               Left = 506
               Bottom = 118
               Right = 676
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Sites"
            Begin Extent = 
               Top = 83
               Left = 323
               Bottom = 275
               Right = 493
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2160
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'viewSiteAssets', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'viewSiteAssets', NULL, NULL
GO
