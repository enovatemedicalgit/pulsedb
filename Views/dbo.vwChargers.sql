SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwChargers]
AS
SELECT        dbo.AssetsCharger.IDAsset, dbo.AssetsCharger.BayWirelessRevision, dbo.AssetsCharger.Bay1ChargerRevision, dbo.AssetsCharger.Bay2ChargerRevision, 
                         dbo.AssetsCharger.Bay3ChargerRevision, dbo.AssetsCharger.Bay4ChargerRevision, dbo.AssetsCharger.BayCount, dbo.AssetsCharger.Bay1LastPostDateUTC, 
                         dbo.AssetsCharger.Bay2LastPostDateUTC, dbo.AssetsCharger.Bay3LastPostDateUTC, dbo.AssetsCharger.Bay4LastPostDateUTC, dbo.AssetsCharger.Bay1Amps, 
                         dbo.AssetsCharger.Bay3Amps, dbo.AssetsCharger.Bay2Amps, dbo.AssetsCharger.Bay4Amps, dbo.AssetsCharger.Bay2Activity, dbo.AssetsCharger.Bay1Activity, 
                         dbo.AssetsCharger.Bay3Activity, dbo.AssetsCharger.Bay1BSN, dbo.AssetsCharger.Bay4Activity, dbo.AssetsCharger.Bay2BSN, dbo.AssetsCharger.Bay4BSN, 
                         dbo.AssetsCharger.Bay3BSN, dbo.AssetsCharger.Bay1ChargeLevel, dbo.AssetsCharger.Bay3ChargeLevel, dbo.AssetsCharger.Bay2ChargeLevel, 
                         dbo.AssetsCharger.Bay4ChargeLevel, dbo.tblAssets.Description, dbo.tblAssets.SerialNo, dbo.tblAssets.IDAssetType, dbo.tblAssets.SiteID, 
                         dbo.tblAssets.DepartmentID, dbo.tblAssets.CreatedDateUTC, dbo.tblAssets.AssetStatusID, dbo.tblAssets.Activity, dbo.tblAssets.Wing, dbo.tblAssets.SiteWingID, 
                         dbo.tblAssets.Floor, dbo.tblAssets.SiteFloorID, dbo.tblAssets.AssetNumber, dbo.tblAssets.DeviceMAC, dbo.tblAssets.Availability, dbo.tblAssets.FriendlyDescription, 
                         dbo.tblAssets.Notes, dbo.tblAssets.Temperature, dbo.tblAssets.LastPostDateUTC, dbo.tblAssets.Retired, dbo.tblAssets.AccessPointId, 
                         dbo.tblAssets.WiFiFirmwareRevision, dbo.tblAssets.IP, dbo.tblAssets.SourceIPAddress, dbo.tblAssets.APMAC, dbo.tblAssets.LastPacketType, 
                         dbo.tblAssets.RowPointer, dbo.tblAssets.Amps, dbo.tblAssets.ManualAllocation, dbo.tblAssets.Other
FROM            dbo.AssetsCharger LEFT OUTER JOIN
                         dbo.tblAssets ON dbo.AssetsCharger.IDAsset = dbo.tblAssets.IDAsset
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "AssetsCharger"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 312
               Right = 243
            End
            DisplayFlags = 280
            TopColumn = 13
         End
         Begin Table = "tblAssets"
            Begin Extent = 
               Top = 20
               Left = 520
               Bottom = 263
               Right = 740
            End
            DisplayFlags = 280
            TopColumn = 23
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vwChargers', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vwChargers', NULL, NULL
GO
