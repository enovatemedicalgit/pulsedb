SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SummaryProcedures] AS 

WITH ImportantProcedures AS 
(
    SELECT
	     o.object_id AS major_id,
		o.Name AS ProcedureName,
		m.Definition AS ProcedureCode
    FROM
	    sys.objects o
	    INNER JOIN
	    sys.sql_modules m ON o.object_id = m.object_id
    WHERE  --o.name IN('Build_Sessions', 'Build_SumarryTrending', 'Build_SummaryActiveAssets', 'Build_SummaryWorkstation', 'Build_SummarySiteDepartment', 'Build_SummaryTotals', 'Build_SummaryWeekly')
	   m.definition LIKE '%INTO%MOVEMENT%'
)
, ExtendedProperties AS 
(
    SELECT major_id, name , value FROM sys.extended_properties

)




SELECT ProcedureName, ProcedureCode,  Name AS PropertyName, Value
FROM 
    ImportantProcedures c
    LEFT OUTER JOIN
    ExtendedProperties p
	   ON c.major_id = p.major_id
GO
