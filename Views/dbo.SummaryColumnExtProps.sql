SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SummaryColumnExtProps] AS 

WITH ImportantColumns AS 
(
    SELECT
	     c.object_id AS major_id,
		c.column_id AS minor_id,
		o.Name AS TableName,
		c.Name AS ColumnName
    FROM
	    sys.objects o
	    INNER JOIN
	    sys.columns c ON o.object_id = c.object_id
    WHERE  o.name IN('Sessions', 'SummarryTrending', 'SummaryActiveAssets', 'SummaryWorkstation', 'SummarySiteDepartment', 'SummartTotals', 'SummaryWeekly')

)
, ExtendedProperties AS 
(
    SELECT major_id, minor_id, name , value FROM sys.extended_properties

)




SELECT TableName, ColumnName, Name AS PropertyName, Value
FROM 
    ImportantColumns c
    LEFT OUTER JOIN
    ExtendedProperties p
	   ON c.major_id = p.major_id
	   AND c.minor_id = p.minor_id

 

GO
