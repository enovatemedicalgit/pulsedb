SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[viewWorkstationRpt]
AS
SELECT        MAX(dbo.Assets.SerialNo) AS SerialNo, MAX(dbo.Assets.Description) AS Description, MAX(dbo.Assets.SiteID) AS SiteID, MAX(dbo.Assets.AssetNumber) 
                         AS AssetNumber, MAX(dbo.Assets.AssetStatusID) AS AssetStatusID, MIN(dbo.Assets.ChargeLevel) AS ChargeLevel, MIN(dbo.Assets.FullChargeCapacity) 
                         AS FullChargeCapacity, MIN(dbo.Assets.Temperature) AS Temperature, MAX(dbo.Assets.CycleCount) AS CycleCount, MAX(dbo.Assets.MaxCycleCount) 
                         AS MaxCycleCount, MIN(dbo.Assets.IDAssetType) AS IDAssetType, MAX(dbo.Assets.LastPostDateUTC) AS LastPostDateUTC, MAX(dbo.Assets.UtilizationLevel) 
                         AS UtilizationLevel, MAX(dbo.Assets.DepartmentID) AS DepartmentID, MAX(dbo.Departments.Description) AS Department
FROM            dbo.Assets INNER JOIN
                         dbo.Departments ON dbo.Assets.DepartmentID = dbo.Departments.IDDepartment
WHERE        (dbo.Assets.IDAssetType IN (1, 3, 6)) AND (dbo.Assets.Retired = 0)
GROUP BY dbo.Assets.SerialNo, dbo.Departments.Description
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[43] 4[23] 2[19] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Assets"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 340
               Right = 270
            End
            DisplayFlags = 280
            TopColumn = 9
         End
         Begin Table = "Departments"
            Begin Extent = 
               Top = 6
               Left = 308
               Bottom = 135
               Right = 494
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 19
         Width = 284
         Width = 1500
         Width = 5445
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 1770
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'viewWorkstationRpt', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'viewWorkstationRpt', NULL, NULL
GO
