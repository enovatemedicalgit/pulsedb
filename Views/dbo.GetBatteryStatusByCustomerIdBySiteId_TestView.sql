SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*<HeaderBlock>*/
CREATE VIEW [dbo].[GetBatteryStatusByCustomerIdBySiteId_TestView] 
--(	
--	@IdCustomer INT = NULL,
--	@IdSite INT = NULL
--)
AS
/*</HeaderBlock>*/


/*<TestScriptHeader>
DECLARE @IdCustomer  Int
DECLARE @IdSite  Int
SET @IdCustomer = NULL --  = 29
SET @IdSite = NULL -- =  68
</TestScriptHeader>*/

--BEGIN
--	DECLARE @SelectedSites  Table
--	(
--		SiteId int NOT NULL
--	)
--	IF @IdSite IS NULL AND @IdCustomer IS NULL
--		/*
--			 Comment out the line below to force a user to choose one 
--			 parameter.  Uncomment it if you want the report to pull back every
--			 site initially
--		*/
--		--INSERT INTO @SelectedSites Select IdSite from dbo.Sites where IsInternal = 0 OR IsInternal IS NULL 
--	IF @IdSite IS NULL AND @IdCustomer IS NOT NULL
--		INSERT INTO @SelectedSites Select IdSite from dbo.Sites Where CustomerID = @IdCustomer
--	IF @IdSite IS NOT NULL 
--		INSERT INTO @SelectedSites VALUES( @IdSite)


	Select
		  SiteName
		, Description
		, SerialNo
		, Generation
		, LastReportedToPulse
		, DaysSinceLastReport
		, ManufacturedDate
		, CycleCount
		, MaxCycleCount
		, FullChargeCapacity
		, RecentlyReportedToPulse
		, WarrantyYears
		, ExpirationDate
		, CapacityHealth
		, CASE	
			WHEN CapacityHealth > 85
				THEN 'Over 85'
			WHEN CapacityHealth BETWEEN 70 and 85
				THEN '70 to 85'
			WHEN CapacityHealth < 70 
				THEN 'Under 70'
			Else 
				'Not Available'
		  END as BatteryHealth			 		
	From
	(SELECT s.SiteName
		,isnull(bpl.Description, 'Mobius ' + Cast(bpl2.AmpHours / 1000 AS VARCHAR(10)) + ' AH Battery') as Description
		,SerialNo
		,bpl2.Generation AS Generation
		,LastPostDateUTC AS LastReportedToPulse
		,datediff(d,LastPostDateUTC,GETDATE()) as DaysSinceLastReport
		,CreatedDateUTC AS ManufacturedDate
		,CycleCount as CycleCount
		,MaxCycleCount as MaxCycleCount
		,FullChargeCapacity as FullChargeCapacity
		,IIF(lastpostdateutc < dateadd(month, - 3, getdate()), 'No', 'Yes') AS RecentlyReportedToPulse
		,cast(IIF(wd.DurationYears > 4, 'N/A', cast(wd.DurationYears AS VARCHAR(4))) AS VARCHAR(9)) AS WarrantyYears
		,Convert(Date, dateadd(day,isnull( (select durationdays  from warrantyduration where serialnumberprefix = left(a.SerialNo,7)),1460),cast('20' + Substring(a.Serialno,8,2) + '-' + Substring(a.SerialNo,10,2) + '-01' as datetime) )) as ExpirationDate
		,isnull(nullif([dbo].[fnGetBatteryCapacity_Int](a.SerialNo, a.FullChargeCapacity), 0), 0) AS CapacityHealth
	FROM assets a
	LEFT JOIN sites s
		ON s.IDSite = a.SiteID
	LEFT JOIN PartNumber bpl
		ON bpl.PartNumber = LEFT(a.SerialNo, 7)
	LEFT JOIN BatteryPartList bpl2
		ON bpl2.PartNumber = LEFT(a.SerialNo, 7)
	LEFT JOIN Customers cus
		ON a.SiteID = cus.idcustomer  
	LEFT JOIN WarrantyDuration wd
		ON wd.SerialNumberPrefix = left(a.serialno, 7)
	--INNER JOIN
	--	@SelectedSites ss
	--	ON s.IDSite = ss.SiteId
	WHERE [dbo].[SerialNumberClassification](a.serialno) = 1
		AND IDAssetType = 5
		AND Retired = 0
		AND idSite = 68
	) base
--	ORDER BY 
--		  SiteName ASC
--		, Generation DESC
--		, CapacityHealth DESC
--END


GO
