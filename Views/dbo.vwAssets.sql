SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwAssets]
AS
	SELECT
		  dbo.AssetsCharger.BayWirelessRevision
		, dbo.AssetsCharger.Bay1ChargerRevision
		, dbo.AssetsCharger.Bay2ChargerRevision
		, dbo.AssetsCharger.Bay3ChargerRevision
		, dbo.AssetsCharger.Bay4ChargerRevision
		, dbo.AssetsCharger.BayCount
		, dbo.AssetsCharger.Bay1LastPostDateUTC
		, dbo.AssetsCharger.Bay2LastPostDateUTC
		, dbo.AssetsCharger.Bay3LastPostDateUTC
		, dbo.AssetsCharger.Bay4LastPostDateUTC
		, dbo.AssetsBattery.FullChargeCapacity
		, dbo.AssetsBattery.CycleCount
		, dbo.AssetsBattery.ChargeLevel
		, dbo.AssetsBattery.MaxCycleCount
		, dbo.AssetsWorkstation.Move
		, dbo.AssetsWorkstation.UtilizationLevel
		, dbo.tblAssets.Description
		, dbo.tblAssets.SerialNo
		, dbo.tblAssets.IDAssetType
		, dbo.tblAssets.SiteID
		, dbo.tblAssets.DepartmentID
		, dbo.tblAssets.Image
		, dbo.tblAssets.ImageFilename
		, dbo.tblAssets.CreatedDateUTC
		, dbo.tblAssets.AssetStatusID
		, dbo.tblAssets.Activity
		, dbo.tblAssets.Wing
		, dbo.tblAssets.SiteWingID
		, dbo.tblAssets.Floor
		, dbo.tblAssets.SiteFloorID
		, dbo.tblAssets.OutOfService
		, dbo.tblAssets.AssetNumber
		, dbo.tblAssets.IsActive
		, dbo.tblAssets.CommandCode
		, dbo.tblAssets.InvoiceNumber
		, dbo.tblAssets.DeviceMAC
		, dbo.tblAssets.Availability
		, dbo.tblAssets.FriendlyDescription
		, dbo.tblAssets.Notes
		, dbo.tblAssets.WarrantyExpDate
		, dbo.tblAssets.Temperature
		, dbo.tblAssets.LastPostDateUTC
		, dbo.tblAssets.Other
		, dbo.tblAssets.Retired
		, dbo.tblAssets.AccessPointId
		, dbo.tblAssets.LastInternalPostDateUTC
		, dbo.tblAssets.WiFiFirmwareRevision
		, dbo.tblAssets.IP
		, dbo.tblAssets.SourceIPAddress
		, dbo.tblAssets.APMAC
		, dbo.tblAssets.LastPacketType
		, dbo.tblAssets.RowPointer
		, dbo.tblAssets.Amps
		, dbo.tblAssets.RemoveFromField
		, dbo.tblAssets.ModifiedBy
		, dbo.tblAssets.ManualAllocation
		, dbo.AssetsWorkstation.IDAsset
		, dbo.tblAssets.IDAsset AS Expr1
	FROM
		dbo.AssetsCharger RIGHT OUTER JOIN
		dbo.AssetsBattery RIGHT OUTER JOIN
		dbo.AssetsWorkstation RIGHT OUTER JOIN
							  dbo.tblAssets
		ON dbo.AssetsWorkstation.IDAsset = dbo.tblAssets.IDAsset
		ON dbo.AssetsBattery.IDAsset = dbo.tblAssets.IDAsset ON dbo.AssetsCharger.IDAsset = dbo.tblAssets.IDAsset
GO
