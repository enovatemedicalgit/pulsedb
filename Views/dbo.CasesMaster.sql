SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CasesMaster]
AS
SELECT DISTINCT 
                         s.RegionID, s.IDSite, s.POCEmail, s.SiteName, c.SFAcctName, c.SFCaseStatus, c.SFCaseContactEmail, c.SFCaseOwnerId, c.SFCaseContactName, 
                         c.SFCaseContactId, c.SFCaseAccountID, c.SFCaseID, c.SFAccountAlertNote, c.SFPulseAlertSubjectLine, c.ServiceType, i.SroCreatedDate, i.SroCreatedBy, 
                         i.ContactName, i.ContactEmail, i.ContactPhone, i.OrderStatusCode, i.SFCaseNoString, i.FedExShippingDate, i.FedExTrackingNo, i.FedExTrackingUrl, 
                         i.SfCustomerAcctID, i.SroNumber, c.SFAcctExternalID, c.open_date, c.Row_ID, c.SFCaseNum AS SFCaseNumber, c.Account_Alert, c.AlertID, c.ClosedDate, 
                         c.Date_Time_Shipped, c.Expected_Date_of_Delivery, c.Expected_Service_Date, c.IsClosed, c.LastModifiedDate, c.Last_Comment, c.Origin, 
                         c.Original_Invoice_Number, c.Power_System, c.RMA_Number, c.Subject, c.Type, c.CreateDate, s.State, s.City, s.Address1, s.Address2, s.ZipCode
FROM            dbo.Cases AS c INNER JOIN
                         dbo.Sites AS s ON c.SFCaseAccountID = s.SfAccountId LEFT OUTER JOIN
                             (SELECT        c.SFCaseNum, MAX(x.SroNumber) AS LastSRONum
                               FROM            dbo.Cases AS c LEFT OUTER JOIN
                                                         dbo.SfCaseSroXRef AS x ON x.SFCaseNumber = c.SFCaseNum
                               WHERE        (c.SFCaseNum IS NOT NULL) AND (ISNUMERIC(c.SFCaseNum) = 1)
                               GROUP BY c.SFCaseNum) AS h ON c.SFCaseNum = h.SFCaseNum LEFT OUTER JOIN
                         staging.SroSfIntegration AS i ON h.LastSRONum = i.SroNumber
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[52] 4[8] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1[61] 2[27] 3) )"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 2
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "c"
            Begin Extent = 
               Top = 30
               Left = 42
               Bottom = 652
               Right = 312
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 161
               Left = 526
               Bottom = 606
               Right = 749
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "i"
            Begin Extent = 
               Top = 0
               Left = 815
               Bottom = 289
               Right = 1118
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "h"
            Begin Extent = 
               Top = 6
               Left = 350
               Bottom = 101
               Right = 520
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 51
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2970
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2010
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
   ', 'SCHEMA', N'dbo', 'VIEW', N'CasesMaster', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'       Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'CasesMaster', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'CasesMaster', NULL, NULL
GO
