SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwBatteryVarianceNotification]
AS
SELECT        MinVal, MaxVal, V_Diff, CreatedDateUTC, DeviceSerialNumber, BatterySerialNumber, DeviceType, Activity, Bay, FullChargeCapacity, Voltage, Amps, Temperature, 
                         ChargeLevel, CycleCount, MaxCycleCount, VoltageCell1, VoltageCell2, VoltageCell3, FETStatus, RemainingTime, BatteryName, Efficiency, ControlBoardRevision, 
                         LCDRevision, HolsterChargerRevision, DCBoardOneRevision, DCBoardTwoRevision, WifiFirmwareRevision, BayWirelessRevision, Bay1ChargerRevision, 
                         Bay2ChargerRevision, Bay3ChargerRevision, Bay4ChargerRevision, MedBoardRevision, BackupBatteryVoltage, BackupBatteryStatus, IsAC, DCUnit1AVolts, 
                         DCUnit1ACurrent, DCUnit1BVolts, DCUnit1BCurrent, DCUnit2AVolts, DCUnit2ACurrent, DCUnit2BVolts, DCUnit2BCurrent, XValue, XMax, YValue, YMax, ZValue, ZMax, 
                         Move, BatteryStatus, SafetyStatus, PermanentFailureStatus, PermanentFailureAlert, BatteryChargeStatus, BatterySafetyAlert, BatteryOpStatus, BatteryMode, 
                         DC1Error, DC1Status, DC2Error, DC2Status, MouseFailureNotification, KeyboardFailureNotification, WindowsShutdownNotification, LinkQuality, IP, DeviceMAC, 
                         APMAC, LastTransmissionStatus, AuthType, ChannelNumber, PortNumber, DHCP, WEPKey, PassCode, StaticIP, SSID, SparePinSwitch, ControlBoardSerialNumber, 
                         HolsterBoardSerialNumber, LCDBoardSerialNumber, DC1BoardSerialNumber, DC2BoardSerialNumber, MedBoardSerialNumber, BayWirelessBoardSerialNumber, 
                         Bay1BoardSerialNumber, Bay2BoardSerialNumber, Bay3BoardSerialNumber, Bay4BoardSerialNumber, MedBoardDrawerOpenTime, MedBoardDrawerCount, 
                         MedBoardMotorUp, MedBoardMotorDown, MedBoardUnlockCount, MedBoardAdminPin, MedBoardNarcPin, MedBoardUserPin1, MedBoardUserPin2, 
                         MedBoardUserPin3, MedBoardUserPin4, MedBoardUserPin5, GenericError, SessionRecordType, ExaminedDateUTC, QueryStringID, CommandCode, ROW_ID, 
                         SourceIPAddress, MeasuredVoltage, BatteryErrorCode, QueryStringBatchID
FROM            (SELECT        dbo.fnMinValues(VoltageCell1, VoltageCell2, VoltageCell3) AS MinVal, dbo.fnMaxValues(VoltageCell1, VoltageCell2, VoltageCell3) AS MaxVal, 
                                                    dbo.fnMaxValues(VoltageCell1, VoltageCell2, VoltageCell3) - dbo.fnMinValues(VoltageCell1, VoltageCell2, VoltageCell3) AS V_Diff, CreatedDateUTC, 
                                                    DeviceSerialNumber, BatterySerialNumber, DeviceType, Activity, Bay, FullChargeCapacity, Voltage, Amps, Temperature, ChargeLevel, CycleCount, 
                                                    MaxCycleCount, VoltageCell1, VoltageCell2, VoltageCell3, FETStatus, RemainingTime, BatteryName, Efficiency, ControlBoardRevision, LCDRevision, 
                                                    HolsterChargerRevision, DCBoardOneRevision, DCBoardTwoRevision, WifiFirmwareRevision, BayWirelessRevision, Bay1ChargerRevision, 
                                                    Bay2ChargerRevision, Bay3ChargerRevision, Bay4ChargerRevision, MedBoardRevision, BackupBatteryVoltage, BackupBatteryStatus, IsAC, 
                                                    DCUnit1AVolts, DCUnit1ACurrent, DCUnit1BVolts, DCUnit1BCurrent, DCUnit2AVolts, DCUnit2ACurrent, DCUnit2BVolts, DCUnit2BCurrent, XValue, XMax, 
                                                    YValue, YMax, ZValue, ZMax, Move, BatteryStatus, SafetyStatus, PermanentFailureStatus, PermanentFailureAlert, BatteryChargeStatus, 
                                                    BatterySafetyAlert, BatteryOpStatus, BatteryMode, DC1Error, DC1Status, DC2Error, DC2Status, MouseFailureNotification, KeyboardFailureNotification, 
                                                    WindowsShutdownNotification, LinkQuality, IP, DeviceMAC, APMAC, LastTransmissionStatus, AuthType, ChannelNumber, PortNumber, DHCP, WEPKey, 
                                                    PassCode, StaticIP, SSID, SparePinSwitch, ControlBoardSerialNumber, HolsterBoardSerialNumber, LCDBoardSerialNumber, DC1BoardSerialNumber, 
                                                    DC2BoardSerialNumber, MedBoardSerialNumber, BayWirelessBoardSerialNumber, Bay1BoardSerialNumber, Bay2BoardSerialNumber, 
                                                    Bay3BoardSerialNumber, Bay4BoardSerialNumber, MedBoardDrawerOpenTime, MedBoardDrawerCount, MedBoardMotorUp, MedBoardMotorDown, 
                                                    MedBoardUnlockCount, MedBoardAdminPin, MedBoardNarcPin, MedBoardUserPin1, MedBoardUserPin2, MedBoardUserPin3, MedBoardUserPin4, 
                                                    MedBoardUserPin5, GenericError, SessionRecordType, ExaminedDateUTC, QueryStringID, CommandCode, ROW_ID, SourceIPAddress, 
                                                    MeasuredVoltage, BatteryErrorCode, QueryStringBatchID
                          FROM            dbo.NonSessionDataCurrent AS b) AS a
WHERE        (V_Diff BETWEEN 150 AND 3000) AND (MinVal <> 0) AND (MaxVal <> 0)
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 293
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vwBatteryVarianceNotification', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vwBatteryVarianceNotification', NULL, NULL
GO
