SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[Assets]
AS
SELECT        dbo.AssetsCharger.BayWirelessRevision, dbo.AssetsCharger.Bay1ChargerRevision, dbo.AssetsCharger.Bay2ChargerRevision, 
                         dbo.AssetsCharger.Bay3ChargerRevision, dbo.AssetsCharger.Bay4ChargerRevision, dbo.AssetsCharger.BayCount, dbo.AssetsCharger.Bay1LastPostDateUTC, 
                         dbo.AssetsCharger.Bay2LastPostDateUTC, dbo.AssetsCharger.Bay3LastPostDateUTC, dbo.AssetsCharger.Bay4LastPostDateUTC, 
                         dbo.AssetsBattery.FullChargeCapacity, dbo.AssetsBattery.CycleCount, dbo.AssetsBattery.ChargeLevel, dbo.AssetsBattery.MaxCycleCount, 
                         dbo.AssetsWorkstation.Move, dbo.AssetsWorkstation.UtilizationLevel, dbo.tblAssets.Description, dbo.tblAssets.SerialNo, dbo.tblAssets.IDAssetType, 
                         dbo.tblAssets.SiteID, dbo.tblAssets.DepartmentID, dbo.tblAssets.Image, dbo.tblAssets.ImageFilename, dbo.tblAssets.CreatedDateUTC, dbo.tblAssets.AssetStatusID, 
                         dbo.tblAssets.Activity, dbo.tblAssets.Wing, dbo.tblAssets.SiteWingID, dbo.tblAssets.Floor, dbo.tblAssets.SiteFloorID, dbo.tblAssets.OutOfService, 
                         dbo.tblAssets.AssetNumber, dbo.tblAssets.IsActive, dbo.tblAssets.CommandCode, dbo.tblAssets.InvoiceNumber, dbo.tblAssets.DeviceMAC, 
                         dbo.tblAssets.Availability, dbo.tblAssets.FriendlyDescription, dbo.tblAssets.Notes, dbo.tblAssets.WarrantyExpDate, dbo.tblAssets.Temperature, 
                         dbo.tblAssets.LastPostDateUTC, dbo.tblAssets.Other, dbo.tblAssets.Retired, dbo.tblAssets.AccessPointId, dbo.tblAssets.LastInternalPostDateUTC, 
                         dbo.tblAssets.WiFiFirmwareRevision, dbo.tblAssets.IP, dbo.tblAssets.SourceIPAddress, dbo.tblAssets.APMAC, dbo.tblAssets.LastPacketType, 
                         dbo.tblAssets.RowPointer, dbo.tblAssets.Amps, dbo.tblAssets.RemoveFromField, dbo.tblAssets.ModifiedBy, dbo.tblAssets.ManualAllocation, dbo.tblAssets.IDAsset, 
                         dbo.AssetsCharger.Bay1Amps, dbo.AssetsCharger.Bay2Amps, dbo.AssetsCharger.Bay3Amps, dbo.AssetsCharger.Bay4Amps, dbo.AssetsCharger.Bay1Activity, 
                         dbo.AssetsCharger.Bay2Activity, dbo.AssetsCharger.Bay3Activity, dbo.AssetsCharger.Bay4Activity, dbo.AssetsCharger.Bay1BSN, dbo.AssetsCharger.Bay2BSN, 
                         dbo.AssetsCharger.Bay3BSN, dbo.AssetsCharger.Bay4BSN, dbo.AssetsCharger.Bay1ChargeLevel, dbo.AssetsCharger.Bay2ChargeLevel, 
                         dbo.AssetsCharger.Bay3ChargeLevel, dbo.AssetsCharger.Bay4ChargeLevel, dbo.tblAssets.UtilizationThreshhold
FROM            dbo.AssetsCharger RIGHT OUTER JOIN
                         dbo.AssetsBattery RIGHT OUTER JOIN
                         dbo.AssetsWorkstation RIGHT OUTER JOIN
                         dbo.tblAssets ON dbo.AssetsWorkstation.IDAsset = dbo.tblAssets.IDAsset ON dbo.AssetsBattery.IDAsset = dbo.tblAssets.IDAsset ON 
                         dbo.AssetsCharger.IDAsset = dbo.tblAssets.IDAsset
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'    Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'Assets', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[53] 4[23] 2[14] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "AssetsCharger"
            Begin Extent = 
               Top = 145
               Left = 221
               Bottom = 274
               Right = 426
            End
            DisplayFlags = 280
            TopColumn = 23
         End
         Begin Table = "AssetsBattery"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 230
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "AssetsWorkstation"
            Begin Extent = 
               Top = 0
               Left = 260
               Bottom = 131
               Right = 430
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tblAssets"
            Begin Extent = 
               Top = 17
               Left = 678
               Bottom = 315
               Right = 898
            End
            DisplayFlags = 280
            TopColumn = 29
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 3195
         Alias = 900
         Table = 1950
         Output = 1425
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
      ', 'SCHEMA', N'dbo', 'VIEW', N'Assets', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=1

GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'Assets', NULL, NULL
GO
