SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[viewLostAssetComm]
AS
SELECT        TOP (100) PERCENT MAX(dbo.Sites.SiteName) AS SiteName, MAX(dbo.Sites.SiteDescription) AS SiteDescription, MAX(dbo.Assets.IDAssetType) AS IDAssetType, 
                         MAX(dbo.AssetType.Description) AS Description, MAX(dbo.Sites.IDSite) AS IDSite, MAX(dbo.Assets.AssetStatusID) AS AssetStatusID, 
                         MAX(dbo.AssetStatus.LongDescription) AS LongDescription, MAX(dbo.Assets.DepartmentID) AS DepartmentID, MAX(dbo.Departments.Description) AS DeptDesc, 
                         MAX(dbo.Assets.SerialNo) AS SerialNo, MAX(dbo.SessionDataCurrent.DeviceSerialNumber) AS DeviceSerialNumber, MAX(dbo.SessionDataCurrent.APMAC) AS APMAC, 
                         MAX(dbo.SessionDataCurrent.SSID) AS SSID, MAX(dbo.SessionDataCurrent.CreatedDateUTC) AS Created, MAX(dbo.SessionDataCurrent.LinkQuality) AS LinkQuality, 
                         MAX(dbo.SessionDataCurrent.IP) AS IP, MAX(dbo.Assets.LastPostDateUTC) AS LastPostDateUTC
FROM            dbo.Assets LEFT OUTER JOIN
                         dbo.Departments ON dbo.Assets.DepartmentID = dbo.Departments.IDDepartment LEFT OUTER JOIN
                         dbo.AssetType ON dbo.Assets.IDAssetType = dbo.AssetType.IDAssetType LEFT OUTER JOIN
                         dbo.AssetStatus ON dbo.Assets.AssetStatusID = dbo.AssetStatus.IDAssetStatus RIGHT OUTER JOIN
                         dbo.SessionDataCurrent ON dbo.Assets.SerialNo = dbo.SessionDataCurrent.DeviceSerialNumber FULL OUTER JOIN
                         dbo.Sites ON dbo.Assets.SiteID = dbo.Sites.IDSite
WHERE        (dbo.Assets.AssetStatusID IN (0, 3)) AND (dbo.Assets.LastPostDateUTC < DATEADD(day, - 1, getutcdate()))
GROUP BY dbo.Assets.SerialNo

GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[60] 4[4] 2[13] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Assets"
            Begin Extent = 
               Top = 135
               Left = 550
               Bottom = 254
               Right = 721
            End
            DisplayFlags = 280
            TopColumn = 21
         End
         Begin Table = "Departments"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 171
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AssetType"
            Begin Extent = 
               Top = 269
               Left = 265
               Bottom = 414
               Right = 425
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AssetStatus"
            Begin Extent = 
               Top = 236
               Left = 789
               Bottom = 433
               Right = 954
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SessionData"
            Begin Extent = 
               Top = 17
               Left = 793
               Bottom = 237
               Right = 1039
            End
            DisplayFlags = 280
            TopColumn = 27
         End
         Begin Table = "Sites"
            Begin Extent = 
               Top = 346
               Left = 444
               Bottom = 497
               Right = 587
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 18
         Width = 284
         Width = 1500
         Wid', 'SCHEMA', N'dbo', 'VIEW', N'viewLostAssetComm', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'th = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'viewLostAssetComm', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'viewLostAssetComm', NULL, NULL
GO
