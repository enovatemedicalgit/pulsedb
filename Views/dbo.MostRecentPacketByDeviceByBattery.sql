SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[MostRecentPacketByDeviceByBattery] as

SELECT
	  BatterySerialNumber,
	  Bay1ChargerRevision,
	  Bay2ChargerRevision,
	  Bay3ChargerRevision,
	  Bay4ChargerRevision,
	  WifiFirmwareRevision,
	  BayWirelessRevision,
	  DeviceSerialNumber,
	  MaxCycleCount,
	  APMac,
	  CreatedDateUTC,
	  ChargeLevel,
	  CycleCount,
	  FullChargeCapacity,
	  Temperature,
	  SourceIPAddress,
	  IP,
	  Bay,
	  Activity,
	  DeviceType,
	  MOVE,
	  Amps
FROM
(
    SELECT
		 nsd.BatterySerialNumber,
		 ISNULL(nsd.Bay1ChargerRevision, '') AS                                              Bay1ChargerRevision,
		 ISNULL(nsd.Bay2ChargerRevision, '') AS                                              Bay2ChargerRevision,
		 ISNULL(nsd.Bay3ChargerRevision, '') AS                                              Bay3ChargerRevision,
		 ISNULL(nsd.Bay4ChargerRevision, '') AS                                              Bay4ChargerRevision,
		 ISNULL(nsd.WifiFirmwareRevision, '') AS                                             WifiFirmwareRevision,
		 ISNULL(nsd.BayWirelessRevision, '') AS                                              BayWirelessRevision,
		 nsd.DeviceSerialNumber,
		 nsd.MaxCycleCount,
		 nsd.APMac,
		 nsd.CreatedDateUTC,
		 nsd.ChargeLevel,
		 nsd.CycleCount,
		 nsd.FullChargeCapacity,
		 nsd.Temperature,
		 nsd.SourceIPAddress AS                                                              SourceIPAddress,
		 nsd.IP AS                                                                           IP,
		 nsd.Bay AS                                                                          Bay,
		 nsd.Activity AS                                                                     Activity,
		 nsd.DeviceType AS                                                                   DeviceType,
		 nsd.MOVE AS                                                                         MOVE,
		 nsd.Amps AS                                                                         Amps,
		 ROW_NUMBER() OVER(PARTITION BY DeviceSerialNumber,
								  BatterySerialNumber ORDER BY CreatedDateUTC DESC) AS RN
    FROM
	    viewAllSessionData nsd WITH (NOLOCK)
    WHERE  nsd.createddateutc > DATEADD(Minute, -60, GETUTCDATE())
) MostRecent
WHERE RN = 1
GO
