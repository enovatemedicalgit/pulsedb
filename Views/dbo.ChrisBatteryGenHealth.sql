SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
	CREATE VIEW [dbo].[ChrisBatteryGenHealth] AS
	
	Select 
		SiteName,
		Generation,
		BatteryHealth,
		Count(SerialNo) as BatteryCount
	From
	(
	Select 
		siteName
		,Generation
		,CASE	
			WHEN CapacityHealth > 85
				THEN 'Over 85'
			WHEN CapacityHealth BETWEEN 70 and 85
				THEN '70 to 85'
			WHEN CapacityHealth < 70 
				THEN 'Under 70'
			Else 
				'Not Available'
		 END as BatteryHealth
		 ,SerialNo		
		From
		(
		SELECT s.sitename
			,SerialNo
			,bpl2.Generation AS Generation
			,isnull(nullif([dbo].[fnGetBatteryCapacity_Int](a.SerialNo, a.FullChargeCapacity), 0), 0) AS CapacityHealth
		FROM assets a
		LEFT JOIN sites s
			ON s.IDSite = a.SiteID
		LEFT JOIN PartNumber bpl
			ON bpl.PartNumber = LEFT(a.SerialNo, 7)
		LEFT JOIN BatteryPartList bpl2
			ON bpl2.PartNumber = LEFT(a.SerialNo, 7)
		LEFT JOIN Customers cus
			ON a.SiteID = cus.idcustomer  
		LEFT JOIN WarrantyDuration wd
			ON wd.SerialNumberPrefix = left(a.serialno, 7)
		--INNER JOIN
		--	@SelectedSites ss
		--	ON s.IDSite = ss.SiteId
		WHERE [dbo].[SerialNumberClassification](a.serialno) = 1
			AND IDAssetType = 5
			AND Retired = 0
			and s.IdSite = 68  --- Test Only
		) as  base
		) as b2
		GROUP BY
		SiteName, Generation, BatteryHealth
GO
