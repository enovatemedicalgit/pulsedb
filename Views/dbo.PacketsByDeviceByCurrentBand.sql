SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[PacketsByDeviceByCurrentBand]
AS
SELECT TOP 100 PERCENT
    DeviceSerialNumber,
    DATEADD(HOUR,DATEDIFF(d,GETUTCDATE(),GETDATE()), CreatedDateUTC) CreatedDateLocal,
    CurrentBand
    FROM	  
    (
    SELECT  
	   DeviceSerialNumber,
	   CreatedDateUTC,
	   'Band0' AS CurrentBand
    FROM 
	   SessionDataCurrent sd
    WHERE
	   EXISTS (SELECT 1 
			 FROM DCCurrentBandingByWorkstation b 
			 WHERE b.DeviceSerialNumber = sd.DeviceSerialNumber 
			 AND (
				    sd.DCUnit1ACurrent BETWEEN b.MinCurrent AND b.Band20
				    OR
				    sd.DCUnit1bCurrent BETWEEN b.MinCurrent AND b.Band20
				    OR
				    sd.DCUnit2ACurrent BETWEEN b.MinCurrent AND b.Band20
				    OR
				    sd.DCUnit2bCurrent BETWEEN b.MinCurrent AND b.Band20				   
				)
			 )
    UNION ALL
    SELECT  
	   DeviceSerialNumber,
	   CreatedDateUTC,
	   'Band20' AS CurrentBand
    FROM 
	   SessionDataCurrent sd
    WHERE
	   EXISTS (SELECT 1 
			 FROM DCCurrentBandingByWorkstation b 
			 WHERE b.DeviceSerialNumber = sd.DeviceSerialNumber 
			 AND (
				    sd.DCUnit1ACurrent BETWEEN b.Band20 +1 AND b.Band40
				    OR
				    sd.DCUnit1bCurrent BETWEEN b.Band20 +1 AND b.Band40
				    OR
				    sd.DCUnit2ACurrent BETWEEN b.Band20 +1 AND b.Band40
				    OR
				    sd.DCUnit2bCurrent BETWEEN b.Band20 +1 AND b.Band40
				)
			 )
    UNION ALL
    SELECT  
	   DeviceSerialNumber,
	   CreatedDateUTC,
	   'Band40' AS CurrentBand
    FROM 
	   SessionDataCurrent sd
    WHERE
	   EXISTS (SELECT 1 
			 FROM DCCurrentBandingByWorkstation b 
			 WHERE b.DeviceSerialNumber = sd.DeviceSerialNumber 
			 AND (
				    sd.DCUnit1ACurrent BETWEEN b.Band40 +1 AND b.Band60
				    OR
				    sd.DCUnit1bCurrent BETWEEN b.Band40 +1 AND b.Band60
				    OR
				    sd.DCUnit2ACurrent BETWEEN b.Band40 +1 AND b.Band60
				    OR
				    sd.DCUnit2bCurrent BETWEEN b.Band40 +1 AND b.Band60
				)
			 )
    UNION ALL
    SELECT  
	   DeviceSerialNumber,
	   CreatedDateUTC,
	   'Band60' AS CurrentBand
    FROM 
	   SessionDataCurrent sd
    WHERE
	   EXISTS (SELECT 1 
			 FROM DCCurrentBandingByWorkstation b 
			 WHERE b.DeviceSerialNumber = sd.DeviceSerialNumber 
			 AND (
				    sd.DCUnit1ACurrent BETWEEN b.Band60 +1 AND b.Band80
				    OR
				    sd.DCUnit1bCurrent BETWEEN b.Band60 +1 AND b.Band80
				    OR
				    sd.DCUnit2ACurrent BETWEEN b.Band60 +1 AND b.Band80
				    OR
				    sd.DCUnit2bCurrent BETWEEN b.Band60 +1 AND b.Band80
				)
			 )
    UNION ALL
    SELECT  
	   DeviceSerialNumber,
	   CreatedDateUTC,
	   'Band80' AS CurrentBand
    FROM 
	   SessionDataCurrent sd
    WHERE
	   EXISTS (SELECT 1 
			 FROM DCCurrentBandingByWorkstation b 
			 WHERE b.DeviceSerialNumber = sd.DeviceSerialNumber 
			 AND (
				    sd.DCUnit1ACurrent BETWEEN b.Band80 +1 AND b.MaxCurrent
				    OR
				    sd.DCUnit1bCurrent BETWEEN b.Band80 +1 AND b.MaxCurrent
				    OR
				    sd.DCUnit2ACurrent BETWEEN b.Band80 +1 AND b.MaxCurrent
				    OR
				    sd.DCUnit2bCurrent BETWEEN b.Band80 +1 AND b.MaxCurrent
				)
			 )
	)
	   AS outerUnion
 ORDER BY
    DeviceSerialNumber,
    CreatedDateUTC DESC
GO
