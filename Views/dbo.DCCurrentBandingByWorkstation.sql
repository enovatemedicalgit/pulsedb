SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[DCCurrentBandingByWorkstation] as

SELECT TOP 100 PERCENT
    DeviceSerialNumber,
    MinCurrent,
    (MinCurrent + (CurrentDifferential*.2)) AS Band20,
    (MinCurrent + (CurrentDifferential*.4)) AS Band40,
    (MinCurrent + (CurrentDifferential*.6)) AS Band60,
    (MinCurrent + (CurrentDifferential*.8)) AS Band80,
    MaxCurrent
From        
    (
    SELECT 
	    DeviceSerialNumber,
	    MAX(MinCurrent) AS MinCurrent,
	    MAX(MaxCurrent) AS MaxCurrent,
	    MAX(MaxCurrent) - MAX(MinCurrent) AS CurrentDifferential
    From
	   (
	   SELECT 
		  DeviceSerialNumber, 
		  MIN(DCUnit1aCurrent) AS MinCurrent,
		  MAX(DCUnit1aCurrent) AS MaxCurrent,
		  --MAX(DCUnit1aCurrent) - MIN(DcUnit1aCurrent) AS CurrentDifferential,
		  --MIN(DCUnit1aCurrent) + ((MAX(DCUnit1aCurrent) - MIN(DcUnit1aCurrent))*.6) AS ActivityThreshhold,
		  '1a' AS Channel

	   FROM 
		  dbo.SessionDataCurrent
	   WHERE
		  --DATEDIFF(day,Sessiondatacurrent.CreatedDateUTC, GETUTCDATE()) < 2 AND
		  DCUnit1aCurrent >100 AND
		  DeviceType IN (6,3,1)
	   GROUP BY
		  DeviceSerialNumber

	   UNION ALL

	   SELECT 
		  DeviceSerialNumber, 
		  MIN(DCUnit1bCurrent) AS MinCurrent,
		  MAX(DCUnit1BCurrent) AS MaxCurrent,
		  --MAX(DCUnit1bCurrent) - MIN(DcUnit1bCurrent) AS CurrentDifferential,
		  --MIN(DCUnit1bCurrent) + ((MAX(DCUnit1bCurrent) - MIN(DcUnit1bCurrent))*.6) AS ActivityThreshhold,
		  '1b' AS Channel
	   FROM 
		  dbo.SessionDataCurrent
	   WHERE
		  --DATEDIFF(day,Sessiondatacurrent.CreatedDateUTC, GETUTCDATE()) < 2 AND
		  DCUnit1BCurrent >100 AND
		  DeviceType IN (6,3,1)
	   GROUP BY
		  DeviceSerialNumber

	   UNION ALL

	   SELECT 
		  DeviceSerialNumber, 
		  MIN(DCUnit2aCurrent) AS MinCurrent,
		  MAX(DCUnit2aCurrent) AS MaxCurrent,
		  --MAX(DCUnit2aCurrent) - MIN(DcUnit2aCurrent) AS CurrentDifferential,
		  --MIN(DCUnit2aCurrent) + ((MAX(DCUnit2aCurrent) - MIN(DcUnit2aCurrent))*.6) AS ActivityThreshhold,
		  '2a' AS Channel
	   FROM 
		  dbo.SessionDataCurrent
	   WHERE
		  --DATEDIFF(day,Sessiondatacurrent.CreatedDateUTC, GETUTCDATE()) < 2 AND
		  DCUnit2aCurrent >100 AND
		  DeviceType IN (6,3,1)
	   GROUP BY
		  DeviceSerialNumber

	   UNION ALL

	   SELECT 
		  DeviceSerialNumber, 
		  MIN(DCUnit2bCurrent) AS MinCurrent,
		  MAX(DCUnit2bCurrent) AS MaxCurrent,
		  --MAX(DCUnit2bCurrent) - MIN(DcUnit2bCurrent) AS CurrentDifferential,
		  --MIN(DCUnit2bCurrent) + ((MAX(DCUnit2bCurrent) - MIN(DcUnit2bCurrent))*.6) AS ActivityThreshhold,
		  '1b' AS Channel
	   FROM 
		  dbo.SessionDataCurrent
	   WHERE
		  --DATEDIFF(day,Sessiondatacurrent.CreatedDateUTC, GETUTCDATE()) < 2 AND
		  DCUnit2bCurrent >100 AND
		  DeviceType IN (6,3,1)
	   GROUP BY
		  DeviceSerialNumber
	   ) AS u

    GROUP BY 
	   DeviceSerialNumber
    ) b
  ORDER BY 
    DeviceSerialNumber DESC
GO
