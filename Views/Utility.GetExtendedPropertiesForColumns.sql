SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [Utility].[GetExtendedPropertiesForColumns]
AS
SELECT        base.TableName, base.ColumnName, base.ObjectId, base.ColumnId, ep.class, ep.class_desc, ep.major_id, ep.minor_id, ep.name, ep.value
FROM            (SELECT        t.name AS TableName, c.name AS ColumnName, c.object_id AS ObjectId, c.column_id AS ColumnId
                          FROM            pulse.sys.tables AS t INNER JOIN
                                                    pulse.sys.columns AS c ON c.object_id = t.object_id) AS base LEFT OUTER JOIN
                         pulse.sys.extended_properties AS ep ON base.ObjectId = ep.major_id AND base.ColumnId = ep.minor_id
GO
