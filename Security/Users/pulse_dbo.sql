IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'pulse_dbo')
CREATE LOGIN [pulse_dbo] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [pulse_dbo] FOR LOGIN [pulse_dbo]
GO
