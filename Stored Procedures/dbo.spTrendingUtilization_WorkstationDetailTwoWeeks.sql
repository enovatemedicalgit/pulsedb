SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

 
CREATE PROCEDURE [dbo].[spTrendingUtilization_WorkstationDetailTwoWeeks] @SiteID INT
--	@DevSN varchar(50),
AS
    BEGIN
	
        SET NOCOUNT ON;

   --test variables
 --   declare @SiteID int = 212
	--declare @DevSN varchar(50) = '311695400934'
	--declare @Start datetime = '4/1/2013'
	--declare @End datetime = '4/8/2013'
   
   
        SELECT  dbo.fnFlipVarchar_ToStardardDateFormat(s.Date) AS 'Date' ,
                b2.CustomerName AS 'IDN' ,
                b.SiteDescription AS 'Site' ,
	--s.IDAssetType as 'Type',
                'Workstation' AS 'Type' ,
                bud.Description AS 'Department' ,
                s.SerialNo ,
                s.AssetNumber ,
                s.Wing ,
                s.Floor ,
                s.Other ,
                s.Status ,
                s.LoChargeInsertsCount ,
                s.HiChargeRemovalsCount ,
                s.AvgRunRate ,
                ( CAST(s.AvgAmpDraw AS INT) * 0.001 ) AS 'AvgAmpDraw' ,
                CAST(CAST(100 * s.AvgEstimatedPCUtilization AS INT) AS VARCHAR(4))
                + '%' AS 'AvgEstPCUtil'
        FROM    dbo.SummaryWorkstation s
                LEFT JOIN dbo.Sites b ON b.IDSite = @SiteID
                LEFT JOIN dbo.Customers b2 ON b2.IDCustomer = b.CustomerID
                LEFT JOIN dbo.Departments bud ON bud.IDDepartment = s.IDDepartment
        WHERE   s.IDSite = @SiteID 
		--	and s.SerialNo = @DevSN
                AND [Date] BETWEEN DATEADD(DAY, -15, GETDATE()) AND GETDATE()
        ORDER BY [Date] DESC ,
                s.Status DESC ,
                Department DESC;
   
    END;



GO
