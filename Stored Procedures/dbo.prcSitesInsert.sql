SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcSitesInsert]
    @IDSite INT ,
    @CustomerID INT ,
    @IPAddr VARCHAR(50) = NULL ,
    @SiteDescription VARCHAR(200) = NULL ,
    @SiteName VARCHAR(150) = NULL
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
    BEGIN TRAN;
	
    INSERT  INTO dbo.Sites
            ( [CustomerID] ,
              [IPAddr] ,
              [SiteDescription] ,
              [SiteName]
            )
            SELECT  @CustomerID ,
                    @IPAddr ,
                    @SiteDescription ,
                    @SiteName;
	
	-- Begin Return Select <- do not remove
    SELECT  [IDSite] ,
            [CustomerID] ,
            [IPAddr] ,
            [SiteDescription] ,
            [SiteName]
    FROM    dbo.Sites
    WHERE   [IDSite] = @IDSite;
	-- End Return Select <- do not remove
               
    COMMIT;



GO
