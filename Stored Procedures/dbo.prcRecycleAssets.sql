SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcRecycleAssets]
    @SerialList NVARCHAR(4000) ,
    @iUserID NVARCHAR(100)
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
    BEGIN TRAN;
    DECLARE @SQLs NVARCHAR(MAX);
    DECLARE @iUser INT;
    SET @iUser = CONVERT(INT, @iUserID);
    DECLARE @SQLSelect NVARCHAR(MAX);
    SET @SQLs = 'update [dbo].[Assets] SET SiteID = 1125 WHERE serialno in ('
        + @SerialList + ')';
    SET @SQLSelect = 'Select SiteId from Assets WHERE serialno in ('
        + @SerialList + ')';
    PRINT @SQLs;

	--UPDATE [dbo].[Assets]
	--SET   SiteID = 1125
	--WHERE  serialno in (@SerialList)
    EXEC sys.sp_executesql @SQLs;
    COMMIT;
    SET @SQLs = 'update [dbo].[Assets] SET ModifiedBy = ' + @iUserID
        + '  where serialno in(' + @SerialList + ')';
    EXEC sys.sp_executesql @SQLs;
    SELECT  @SQLs + ' ' + @SQLSelect;



GO
