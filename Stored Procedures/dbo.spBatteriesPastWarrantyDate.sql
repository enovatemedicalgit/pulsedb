SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[spBatteriesPastWarrantyDate]
AS
    BEGIN	
        SELECT  *
        FROM    ( SELECT    ISNULL(s.SiteName, cus.CustomerName) AS Account ,
                            ass.SerialNo ,
                            'Past Warranty Date' AS 'Opportunity Type' ,
                            wd.DurationYears AS 'Warranty Years' ,
                            CONVERT(DATE, DATEADD(DAY,
                                                  ISNULL(( SELECT
                                                              DurationDays
                                                           FROM
                                                              dbo.WarrantyDuration
                                                           WHERE
                                                              SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
                                                         ), 5000),
                                                  CAST('20'
                                                  + SUBSTRING(ass.SerialNo, 8,
                                                              2) + '-'
                                                  + SUBSTRING(ass.SerialNo, 10,
                                                              2) + '-01' AS DATETIME))) AS 'Expiration Date' ,
                            ass.CycleCount ,
                            ass.MaxCycleCount ,
                            ISNULL(NULLIF(dbo.fnGetBatteryCapacity_Int(ass.SerialNo,
                                                              ass.FullChargeCapacity),
                                          0), 0) AS 'Capacity Health' ,
                            ass.FullChargeCapacity ,
                            ass.LastPostDateUTC
                  FROM      dbo.Assets ass
                            LEFT JOIN dbo.Sites s ON s.IDSite = ass.SiteID
                            LEFT JOIN dbo.Customers cus ON ass.SiteID = cus.IDCustomer
                            LEFT JOIN dbo.WarrantyDuration wd ON wd.SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
                  WHERE     ass.LastPostDateUTC > DATEADD(DAY, -20, GETDATE())
                            AND ass.IDAssetType = 5
                            AND GETDATE() > DATEADD(DAY,
                                                    ISNULL(( SELECT
                                                              DurationDays
                                                             FROM
                                                              dbo.WarrantyDuration
                                                             WHERE
                                                              SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
                                                           ), 5000),
                                                    CAST('20'
                                                    + SUBSTRING(ass.SerialNo,
                                                              8, 2) + '-'
                                                    + SUBSTRING(ass.SerialNo,
                                                              10, 2)
                                                    + '-01 12:00:00.000' AS DATETIME))
                            AND dbo.SerialNumberClassification(ass.SerialNo) = 1
                            AND ass.MaxCycleCount <> 0
                            AND ass.MaxCycleCount <> 10
                            AND ass.SiteID NOT IN ( 4, 28, 1125 )
                            AND ass.FullChargeCapacity > 0
                            AND ass.Retired = 0
                  UNION ALL
                  SELECT    ISNULL(s.SiteDescription, cus.CustomerName) AS Account ,
                            ass.SerialNo ,
                            'Past Warranty Cycle Count' AS 'Opportunity Type' ,
                            wd.DurationYears AS 'Warranty Years' ,
                            CONVERT(DATE, DATEADD(DAY,
                                                  ISNULL(( SELECT
                                                              DurationDays
                                                           FROM
                                                              dbo.WarrantyDuration
                                                           WHERE
                                                              SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
                                                         ), 5000),
                                                  CAST('20'
                                                  + SUBSTRING(ass.SerialNo, 8,
                                                              2) + '-'
                                                  + SUBSTRING(ass.SerialNo, 10,
                                                              2) + '-01' AS DATETIME))) AS 'Expiration Date' ,
                            ass.CycleCount ,
                            ass.MaxCycleCount ,
                            ISNULL(NULLIF(dbo.fnGetBatteryCapacity_Int(ass.SerialNo,
                                                              ass.FullChargeCapacity),
                                          0), 0) AS 'Capacity Health' ,
                            ass.FullChargeCapacity ,
                            ass.LastPostDateUTC
                  FROM      dbo.Assets ass
                            LEFT JOIN dbo.Sites s ON s.IDSite = ass.SiteID
                            LEFT JOIN dbo.Customers cus ON ass.SiteID = cus.IDCustomer
                            LEFT JOIN dbo.WarrantyDuration wd ON wd.SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
                  WHERE     ass.IDAssetType = 5
                            AND ass.CycleCount > ass.MaxCycleCount
                            AND ass.MaxCycleCount != 0
                            AND ass.MaxCycleCount IS NOT NULL
                            AND ass.MaxCycleCount <> 0
                            AND ass.MaxCycleCount <> 10
                            AND ass.CycleCount <> 0
                            AND ass.CycleCount IS NOT NULL
                            AND dbo.SerialNumberClassification(ass.SerialNo) = 1
                            AND ass.FullChargeCapacity > 0
                            AND ass.Retired = 0
                            AND s.IDSite NOT IN ( 1125, 28, 4 ) --order by s.SiteName, MaxCycleCount desc	
                ) fullrpt
        ORDER BY fullrpt.Account DESC ,
                fullrpt.SerialNo DESC; 

    END;



GO
