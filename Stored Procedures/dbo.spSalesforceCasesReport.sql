SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[spSalesforceCasesReport]
    @SiteID VARCHAR(20) = NULL
AS
    BEGIN

        BEGIN
--Select top 3000 isnull(astat.Description,'New, No Response') as "Case Status",am.DeviceSerialNumber,am.BatterySerialNumber,am.Description,am.CreatedDateUTC as "Case Opened Date", ('https://na32.salesforce.com/' + am.SFCID) as "Case Link",vsd.BatteryErrorCode,vsd.VoltageCell1,vsd.VoltageCell2,vsd.VoltageCell3  from AlertMessage am left join AlertStatus astat on am.AlertStatus = astat.ROW_ID  join viewAllSessionData vsd on am.BatterySerialNumber = vsd.BatterySerialNumber and am.DeviceSerialNumber = vsd.DeviceSerialNumber  and SFCID is not null and vsd.CreatedDateUTC  between dateadd(mm,-50,am.createddateutc) and dateadd(mm,50,am.createddateutc) and am.Priority = 911 and SiteID = @SiteID order by am.CreatedDateUTC desc
            SELECT TOP 100
                    ISNULL(astat.Description, 'New, No Response') AS "Case Status" ,
                    am.DeviceSerialNumber ,
                    am.BatterySerialNumber ,
                    am.Description ,
                    am.CreatedDateUTC AS "Case Opened Date" ,
                    ass.Floor ,
                    ass.Wing ,
                    ( 'https://na32.salesforce.com/' + am.SFCID ) AS "Case Link"
            FROM    dbo.AlertMessage am
                    LEFT JOIN dbo.AlertStatus astat ON am.AlertStatus = astat.ROW_ID
                    LEFT JOIN dbo.Assets ass ON ass.SerialNo = am.DeviceSerialNumber
                    LEFT JOIN dbo.Assets ass2 ON ass.SerialNo = am.BatterySerialNumber
                                             AND am.SFCID IS NOT NULL
                                             AND am.Priority = 911
                                             AND am.SiteID = @SiteID
            ORDER BY am.CreatedDateUTC DESC;
        END;

    END;

GO
