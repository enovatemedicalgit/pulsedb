SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Bill Murray>
-- Create date:	<8/16/2016>
-- Description:	
-- =============================================
CREATE Procedure [dbo].[prcDashGetBatteryRemovals]
-- Add the parameters for the stored procedure here
 (   @SiteID INT = 657
    )
AS
    BEGIN

	--SELECT TOP 1000 
 --     [CreatedDateUTC]
 --     count([Count]) BattInneficiencies

 -- FROM [pulse].[dbo].[AlertCount] WHERE AlertConditionID IN (90,91) AND DeviceSerialNumber IN (SELECT serialno FROM assets WHERE siteid = @SiteID) LEFT JOIN assets ass ON ass.siteid = @siteID GROUP BY CreatedDateUTC
        SET NOCOUNT ON;
        SELECT  '8' AS ampHour ,
                CONVERT(INT, ( RAND(CHECKSUM(NEWID())) * ( 7 - 1 ) + 1.9 )) AS BatteryRemovals ,
                @SiteID AS siteID
        UNION
        SELECT  '9' AS ampHour ,
                CONVERT(INT, ( RAND(CHECKSUM(NEWID())) * ( 7 - 1 ) + 1.9 )) AS BatteryRemovals ,
                @SiteID AS siteID
        UNION
        SELECT  '10' AS ampHour ,
                CONVERT(INT, ( RAND(CHECKSUM(NEWID())) * ( 7 - 1 ) + 1.9 )) AS BatteryRemovals ,
                @SiteID AS siteID
        UNION
        SELECT  '11' AS ampHour ,
                CONVERT(INT, ( RAND(CHECKSUM(NEWID())) * ( 7 - 1 ) + 1.9 )) AS BatteryRemovals ,
                @SiteID AS siteID
        UNION
        SELECT  '12' AS ampHour ,
               CONVERT(INT,  ( RAND(CHECKSUM(NEWID())) * ( 7 - 1 ) + 1.9 )) AS BatteryRemovals ,
                @SiteID AS siteID
        UNION
        SELECT  '13' AS ampHour ,
                CONVERT(INT, ( RAND(CHECKSUM(NEWID())) * ( 7 - 1 ) + 1.9 )) AS BatteryRemovals ,
                @SiteID AS siteID
        UNION
        SELECT  '14' AS ampHour ,
                CONVERT(INT, ( RAND(CHECKSUM(NEWID())) * ( 7 - 1 ) + 1.9 )) AS BatteryRemovals ,
                @SiteID AS siteID
        UNION
        SELECT  '15' AS ampHour ,
                CONVERT(INT, ( RAND(CHECKSUM(NEWID())) * ( 7 - 1 ) + 1.9 )) AS BatteryRemovals ,
                @SiteID AS siteID
        UNION
        SELECT  '16' AS ampHour ,
                CONVERT(INT, ( RAND(CHECKSUM(NEWID())) * ( 7 - 1 ) + 1.9 )) AS BatteryRemovals ,
                @SiteID AS siteID
        UNION
        SELECT  '17' AS ampHour ,
                CONVERT(INT, ( RAND(CHECKSUM(NEWID())) * ( 7 - 1 ) + 1.9 )) AS BatteryRemovals ,
                @SiteID AS siteID
        UNION
        SELECT  '18' AS ampHour ,
                CONVERT(INT, ( RAND(CHECKSUM(NEWID())) * ( 7 - 1 ) + 1.9 )) AS BatteryRemovals ,
                @SiteID AS siteID
        UNION
        SELECT  '19' AS ampHour ,
                CONVERT(INT, ( RAND(CHECKSUM(NEWID())) * ( 7 - 1 ) + 1.9 )) AS BatteryRemovals ,
                @SiteID AS siteID;
      

    END;


GO
