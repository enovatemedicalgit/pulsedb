SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcVendorsInsert]
    @VendorName VARCHAR(150) = NULL
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
    BEGIN TRAN;
	
    INSERT  INTO dbo.Vendors
            ( [VendorName] )
            SELECT  @VendorName;
	
	-- Begin Return Select <- do not remove
    SELECT  [IDVendor] ,
            [VendorName]
    FROM    dbo.Vendors
    WHERE   [IDVendor] = SCOPE_IDENTITY();
	-- End Return Select <- do not remove
               
    COMMIT;



GO
