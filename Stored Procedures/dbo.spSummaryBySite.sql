SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Arlow Farrell
-- Create date: 2011-04-06
-- Description:	Summary counts for all
--              business units within
--              the supplied parent business unit
-- =============================================
CREATE PROCEDURE [dbo].[spSummaryBySite]
    (
      @ParentBusinessUnitID INT
    )
AS
    BEGIN
        SELECT  bu.SiteDescription ,
                bu.IDSite AS BusinessUnitID ,
                0 AS AverageAvailability ,
                SUM(COALESCE(s.BatteryCount, 0)) AS BatteryCount ,
                SUM(COALESCE(s.ActiveBatteryCount, 0)) AS ActiveBatteryCount ,
                SUM(COALESCE(s.ChargerCount, 0)) AS ChargerCount ,
                SUM(COALESCE(s.ActiveChargerCount, 0)) AS ActiveChargerCount ,
                SUM(COALESCE(s.WorkstationCount, 0)) AS WorkstationCount ,
                SUM(COALESCE(s.ActiveWorkstationCount, 0)) AS ActiveWorkstationCount ,
                AVG(COALESCE(mru.AvgRunRateAvg, 0)) AS AvgRunRateAvg ,
                CAST(AVG(COALESCE(mru.Utilization, 0.0000)) AS DECIMAL(6, 4)) AS Utilization ,
                SUM(COALESCE(mru.SessionCount, 0)) AS SessionCount ,
                AVG(COALESCE(AvgSessionLengthHours, 0)) AS AverageSessionRuntimeHours ,
                AVG(COALESCE(AvgSessionLengthMinutes, 0)) AS AverageSessionRuntimeMinutes
        FROM    dbo.Sites bu
                LEFT JOIN dbo.SummarySiteAssets s ON s.IDSite = bu.IDSite
                LEFT JOIN ( SELECT  st.SiteID ,
                                    SUM(COALESCE(st.SessionCount, 0)) AS SessionCount ,
                                    AVG(COALESCE(st.AvgSessionLengthHours, 0)) AS AvgSessionLengthHours ,
                                    AVG(COALESCE(st.AvgSessionLengthMinutes,
                                                 0)) AS AvgSessionLengthMinutes ,
                                    AVG(COALESCE(st.AvgRunRate, 0)) AS AvgRunRateAvg ,
                                    SUM(COALESCE(st.ActiveDeviceCount, 0)) AS ActiveDeviceCount ,
                                    AVG(COALESCE(st.Utilization, 0.0000)) AS Utilization ,
                                    SUM(COALESCE(st.NonSessionRecordCount, 0)) AS NonSessionRecordCount
                            FROM    dbo.Sites bu
                                    LEFT JOIN ( SELECT  *
                                                FROM    dbo.SummaryTrending
                                                WHERE   [Date] = ( SELECT
                                                              MAX([Date]) AS MaxDate
                                                              FROM
                                                              dbo.SummaryTrending
                                                              )
                                              ) st ON bu.IDSite = st.SiteID
                            GROUP BY st.SiteID
                          ) mru -- MostRecentUtilization
                ON mru.SiteID = bu.IDSite
        WHERE   bu.CustomerID = @ParentBusinessUnitID
        GROUP BY bu.SiteDescription ,
                bu.IDSite
        ORDER BY bu.SiteDescription;
    END;


GO
