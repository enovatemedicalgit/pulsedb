SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[spAlertMessages_Get_ForSalesForce]
AS
    BEGIN
	
        SET NOCOUNT ON;   
--Select 
--		a.ROW_ID,
--		a.BusinessUnitID as BUID, 
--		a.CreatedDateUTC as Dates, 
--		b.SiteDescription,
--		a.AlertConditionID,
--		a.DeviceSerialNumber as DeviceSN, 
--		a.BatterySerialNumber as BatterySN,   
--		a.[Description], 
--		'Recommended Action: ' + c.FriendlyDescription as RecommendedAction, 
--		CASE WHEN a.AlertStatus = 0 THEN 'New, No Action Taken' 
--		END as [Status],
--		CASE a.Notes 
--			WHEN '' THEN '' 
--			ELSE 'CARE Notes: ' + a.Notes 
--		END as Notes, 	
--		d.AssetNumber, 
--		f.MACAddress as AccessPoint, 
--		d.Wing, 
--		d.[Floor], 
--		d.Other, 
--		CASE WHEN Coalesce(a.ModifiedUserID, 0) = 0 
--			THEN '' else 'CARE Technician: ' + Coalesce(u.FirstName, '') + ' ' + Coalesce(u.LastName, '') 
--		END as CARE,
--		e.[Description] as Department,   
--		a.CreatedDateUTC,
--		 ROW_NUMBER() over ( Partition by a.BusinessUnitID, a.DeviceSerialNumber, a.BatterySerialNumber 
--		order by a.BusinessUnitID, a.DeviceSerialNumber, a.BatterySerialNumber)
--		as [count]        
	    
--		from AlertMessage a 
--		left join AlertConditions c on a.AlertConditionID = c.ROW_ID 
--		left join BusinessUnit b on b.ROW_ID = a.BusinessUnitID 
--		left join [User] u on a.ModifiedUserID = u.ROW_ID 
--		left join Device d on d.SerialNumber = a.DeviceSerialNumber 
--		left join BusinessUnitDepartment e on e.ROW_ID = d.BusinessUnitDepartmentID
--		left join AccessPoint f on f.ROW_ID = d.AccessPointID 
	    
--		where (a.AlertStatus = 0) and a.Priority = 911 and a.AlertConditionID > 0 and a.CreatedDateUTC > convert(varchar(10), DATEADD(day, -1, getutcdate()), 111)
--		and left(a.[Description], 2) <> 'CR' 
        SELECT  *
        FROM    dbo.viewGetSFAlertsGrouped;

    END;

GO
