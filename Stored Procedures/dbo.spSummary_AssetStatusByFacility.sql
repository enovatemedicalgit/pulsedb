SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[spSummary_AssetStatusByFacility]
AS
    BEGIN

        SELECT  bu.SiteDescription ,
                ( SELECT    COUNT(d.IDAsset)
                  FROM      dbo.Assets d
                            INNER JOIN dbo.AssetStatus ds ON ds.IDAssetStatus = d.AssetStatusID
                                                         AND ds.Description = 'Available'
                            INNER JOIN dbo.AssetType dt ON dt.IDAssetType = d.IDAssetType
                                                       AND dt.Category LIKE '%Workstation%'
                  WHERE     d.SiteID = bu.IDSite -- and d.Retired = 0
                ) AS wks_avail ,
                ( SELECT    COUNT(d.IDAsset)
                  FROM      dbo.Assets d
                            INNER JOIN dbo.AssetStatus ds ON ds.IDAssetStatus = d.AssetStatusID
                                                         AND ds.Description = 'In Service'
                            INNER JOIN dbo.AssetType dt ON dt.IDAssetType = d.IDAssetType
                                                       AND dt.Category LIKE '%Workstation%'
                  WHERE     d.SiteID = bu.IDSite -- and d.Retired = 0
                ) AS wks_inserv ,
                ( SELECT    COUNT(d.IDAsset)
                  FROM      dbo.Assets d
                            INNER JOIN dbo.AssetStatus ds ON ds.IDAssetStatus = d.AssetStatusID
                                                         AND ds.Description = 'Offline'
                            INNER JOIN dbo.AssetType dt ON dt.IDAssetType = d.IDAssetType
                                                       AND dt.Category LIKE '%Workstation%'
                  WHERE     d.SiteID = bu.IDSite -- and d.Retired = 0
                ) AS wks_offline ,
                ( SELECT    COUNT(d.IDAsset)
                  FROM      dbo.Assets d
                            INNER JOIN dbo.AssetStatus ds ON ds.IDAssetStatus = d.AssetStatusID
                                                         AND ds.Description = 'Not commissioned'
                            INNER JOIN dbo.AssetType dt ON dt.IDAssetType = d.IDAssetType
                                                       AND dt.Category LIKE '%Workstation%'
                  WHERE     d.SiteID = bu.IDSite--  and d.Retired = 0
                ) AS wks_notcomm ,
                ( SELECT    COUNT(d.IDAsset)
                  FROM      dbo.Assets d
                            INNER JOIN dbo.AssetStatus ds ON ds.IDAssetStatus = d.AssetStatusID
                            INNER JOIN dbo.AssetType dt ON dt.IDAssetType = d.IDAssetType
                                                       AND dt.Category LIKE '%Workstation%'
                  WHERE     d.SiteID = bu.IDSite
                            AND d.Retired = 0
                ) AS wks_total ,
                ( SELECT    COUNT(d.IDAsset)
                  FROM      dbo.Assets d
                            INNER JOIN dbo.AssetStatus ds ON ds.IDAssetStatus = d.AssetStatusID
                                                         AND ds.Description = 'Available'
                            INNER JOIN dbo.AssetType dt ON dt.IDAssetType = d.IDAssetType
                                                       AND dt.Category LIKE '%Charger%'
                  WHERE     d.SiteID = bu.IDSite
                            AND d.Retired = 0
                ) AS chrg_avail ,
                ( SELECT    COUNT(d.IDAsset)
                  FROM      dbo.Assets d
                            INNER JOIN dbo.AssetStatus ds ON ds.IDAssetStatus = d.AssetStatusID
                                                         AND ds.Description = 'In Service'
                            INNER JOIN dbo.AssetType dt ON dt.IDAssetType = d.IDAssetType
                                                       AND dt.Category LIKE '%Charger%'
                  WHERE     d.SiteID = bu.IDSite
                            AND d.Retired = 0
                ) AS chrg_inserv ,
                ( SELECT    COUNT(d.IDAsset)
                  FROM      dbo.Assets d
                            INNER JOIN dbo.AssetStatus ds ON ds.IDAssetStatus = d.AssetStatusID
                                                         AND ds.Description = 'Offline'
                            INNER JOIN dbo.AssetType dt ON dt.IDAssetType = d.IDAssetType
                                                       AND dt.Category LIKE '%Charger%'
                  WHERE     d.SiteID = bu.IDSite
                            AND d.Retired = 0
                ) AS chrg_offline ,
                ( SELECT    COUNT(d.IDAsset)
                  FROM      dbo.Assets d
                            INNER JOIN dbo.AssetStatus ds ON ds.IDAssetStatus = d.AssetStatusID
                                                         AND ds.Description = 'Not commissioned'
                            INNER JOIN dbo.AssetType dt ON dt.IDAssetType = d.IDAssetType
                                                       AND dt.Category LIKE '%Charger%'
                  WHERE     d.SiteID = bu.IDSite
                            AND d.Retired = 0
                ) AS chrg_notcomm ,
                ( SELECT    COUNT(d.IDAsset)
                  FROM      dbo.Assets d
                            INNER JOIN dbo.AssetStatus ds ON ds.IDAssetStatus = d.AssetStatusID
                            INNER JOIN dbo.AssetType dt ON dt.IDAssetType = d.IDAssetType
                                                       AND dt.Category = '%Charger%'
                  WHERE     d.SiteID = bu.IDSite
                            AND d.Retired = 0
                ) AS chrg_total ,
                ( SELECT    COUNT(d.IDAsset)
                  FROM      dbo.Assets d
                            INNER JOIN dbo.AssetStatus ds ON ds.IDAssetStatus = d.AssetStatusID
                            INNER JOIN dbo.AssetType dt ON dt.IDAssetType = d.IDAssetType
                                                       AND dt.Category IN (
                                                       'Charger',
                                                       'Workstation' )
                  WHERE     d.SiteID = bu.IDSite
                            AND d.Retired = 0
                ) AS devices_total
        FROM    dbo.Sites bu
        WHERE   bu.SiteDescription NOT LIKE '%Stinger%'
                AND bu.SiteDescription NOT LIKE '%Enovate%'
	-- and bu.BUType = 2
        ORDER BY bu.SiteDescription;


    END;


GO
