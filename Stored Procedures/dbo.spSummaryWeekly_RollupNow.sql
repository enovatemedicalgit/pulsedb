SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Arlow Farrell
-- Create date: 2011-12-14
-- Description:	Adds summary rows to 
--              SummaryWeekly table
-- =============================================
CREATE PROCEDURE [dbo].[spSummaryWeekly_RollupNow]
AS
BEGIN
	SET NOCOUNT ON;


	declare @Date varchar(10)
	set @Date = convert(varchar(10), dateadd(day, -1, getutcdate()) , 111)

	/*

		Results intended for email
		- HQs will be first, followed by facility totals, then department totals

	*/

	delete from SummaryWeekly where [Date] = @Date

	insert into SummaryWeekly ([Date], [Description], HQID, SiteID, DepartmentID, RowType, ActiveWorkstationCount, AvailableWorkstationCount, OfflineWorkstationCount)
	select @Date as [Date]
		, bu.CustomerName as [Description]
		, sbud.IDCustomer as HQID
	    , case when bu.IDCustomer = 0 then 1 else 0 end as BusinessUnitID
		, case when bu.IDCustomer = 0 then 1 else 0 end as BusinessUnitDepartmentID
		, case when bu.IDCustomer = 0 then '' else 'HQ' end as RowType
		, sum(ActiveWorkstationCount) as ActiveWorkstationCount
		, sum(AvailableWorkstationCount) as AvailableWorkstationCount
		, sum(OfflineWorkstationCount) as OfflineWorkstationCount
	from SummarySiteDepartment sbud
	join Customers bu on sbud.IDCustomer = bu.IDCustomer
	where sbud.[Date] = @Date
	group by bu.CustomerName, sbud.IDCustomer, case when bu.IDCustomer = 0 then 1 else 0 end, case when bu.IDCustomer = 0 then 1 else 0 end, case when bu.IDCustomer = 0 then '' else 'HQ' end


	union

	select @Date as [Date]
		, bu.SiteDescription as [Description]
		, sbud.IDCustomer as HQID
		, bu.IDSite
		, case when bu.IDSite = 0 then 1 else 0 end as BusinessUnitDepartmentID
		, case when bu.IDSite = 0 then '' else 'Facility' end as RowType
		, sum(ActiveWorkstationCount) as ActiveWorkstationCount
		, sum(AvailableWorkstationCount) as AvailableWorkstationCount
		, sum(OfflineWorkstationCount) as OfflineWorkstationCount
	from SummarySiteDepartment sbud
	join Sites bu on sbud.IDSite = bu.IDSite
	where sbud.[Date] = @Date
	group by bu.SiteDescription, sbud.IDCustomer, bu.IDSite, case when bu.IDSite = 0 then 1 else 0 end, case when bu.IDSite = 0 then '' else 'Facility' end

	union

	select @Date as [Date]
		, bud.[Description]
		, sbud.IDCustomer as HQID
		, sbud.IDSite
		, sbud.IDDepartment
		, case when bud.IDDepartment = -1 then '' else 'Department' end as RowType
		, sum(ActiveWorkstationCount) as ActiveWorkstationCount
		, sum(AvailableWorkstationCount) as AvailableWorkstationCount
		, sum(OfflineWorkstationCount) as OfflineWorkstationCount
	from SummarySiteDepartment sbud
	join Departments bud on sbud.IDDepartment = bud.IDDepartment
	where sbud.[Date] = @Date
	group by bud.[Description], sbud.IDCustomer, sbud.IDSite, sbud.IDDepartment, case when bud.IDDepartment = -1 then '' else 'Department' end




	/*	---------------- >  RunRate & AvgAmpDraw  < -----------------	*/


	-- RunRate and AvgAmpDraw : this (past) week : HQ level
	update SummaryWeekly
	set RunRateTW = coalesce(a.AvgRunRateTW,0)
		, AvgAmpDrawTW = coalesce(a.AvgAmpDrawTW,0)
	from (
			select bu.IDCustomer as HQID, Avg(AvgRunRate) as AvgRunRateTW, Avg(AvgAmpDraw) as AvgAmpDrawTW
			from SummarySiteDepartment sbud
			join Customers bu on sbud.IDCustomer = bu.IDCustomer
			where [Date] between dateadd(day, -7, @Date) and cast(@Date as datetime) 
			group by bu.IDCustomer
		) a
	where SummaryWeekly.[Date] = @Date
	and SummaryWeekly.HQID = a.HQID
	and SummaryWeekly.RowType = 'HQ'


	-- RunRate and AvgAmpDraw : last week : HQ level
	update SummaryWeekly
	set RunRateTW = coalesce(a.AvgRunRateLW,0)
		, AvgAmpDrawTW = coalesce(a.AvgAmpDrawLW,0)
	from (
			select sbud.IDCustomer as HQID, Avg(AvgRunRate) as AvgRunRateLW, Avg(AvgAmpDraw) as AvgAmpDrawLW
			from SummarySiteDepartment sbud
			join Customers bu on sbud.IDCustomer = bu.IDCustomer
			where [Date] between dateadd(day, -14, @Date) and dateadd(day, -8, @Date) 
			group by sbud.IDCustomer
		) a
	where SummaryWeekly.[Date] = @Date
	and SummaryWeekly.HQID = a.HQID
	and SummaryWeekly.RowType = 'HQ'




	-- RunRate and AvgAmpDraw : this (past) week : business unit level
	update SummaryWeekly
	set RunRateTW = coalesce(a.AvgRunRateTW,0)
		, AvgAmpDrawTW = coalesce(a.AvgAmpDrawTW,0)
	from (
			select IDSite, Avg(AvgRunRate) as AvgRunRateTW, Avg(AvgAmpDraw) as AvgAmpDrawTW
			from SummarySiteDepartment
			where [Date] between dateadd(day, -7, @Date) and cast(@Date as datetime) 
			group by IDSite
		) a
	where SummaryWeekly.[Date] = @Date
	and SummaryWeekly.SiteID = a.IDSite
	and SummaryWeekly.RowType = 'Facility'


	-- RunRate and AvgAmpDraw : last week : business unit level
	update SummaryWeekly
	set RunRateLW = coalesce(a.AvgRunRateLW,0)
		, AvgAmpDrawLW = coalesce(a.AvgAmpDrawLW,0)
	from (
			select IDSite, Avg(AvgRunRate) as AvgRunRateLW, Avg(AvgAmpDraw) as AvgAmpDrawLW
			from SummarySiteDepartment
			where [Date] between dateadd(day, -14, @Date) and dateadd(day, -8, @Date) 
			group by IDSite
		) a
	where SummaryWeekly.[Date] = @Date
	and SummaryWeekly.SiteID = a.IDSite
	and SummaryWeekly.RowType = 'Facility'



	-- RunRate and AvgAmpDraw : this (past) week : Department level
	update SummaryWeekly
	set RunRateTW = coalesce(a.AvgRunRateTW,0)
		, AvgAmpDrawTW = coalesce(a.AvgAmpDrawTW,0)
	from (
			select IDSite, IDDepartment, Avg(AvgRunRate) as AvgRunRateTW, Avg(AvgAmpDraw) as AvgAmpDrawTW
			from SummarySiteDepartment
			where [Date] between dateadd(day, -7, @Date) and cast(@Date as datetime) 
			group by IDSite, IDDepartment
		) a
	where SummaryWeekly.[Date] = @Date
	and SummaryWeekly.SiteID = a.IDSite
	and SummaryWeekly.DepartmentID = a.IDDepartment
	and SummaryWeekly.RowType = 'Department'


	-- RunRate and AvgAmpDraw : last week : Department level
	update SummaryWeekly
	set RunRateLW = coalesce(a.AvgRunRateLW,0)
		, AvgAmpDrawLW = coalesce(a.AvgAmpDrawLW,0)
	from (
			select IDSite, IDDepartment, Avg(AvgRunRate) as AvgRunRateLW, Avg(AvgAmpDraw) as AvgAmpDrawLW
			from SummarySiteDepartment
			where [Date] between dateadd(day, -14, @Date) and dateadd(day, -8, @Date) 
			group by IDSite, IDDepartment
		) a
	where SummaryWeekly.[Date] = @Date
	and SummaryWeekly.SiteID = a.IDSite
	and SummaryWeekly.DepartmentID = a.IDDepartment
	and SummaryWeekly.RowType = 'Department'


	/*	---------------- >  Utilization  < -----------------	*/

	/*

		Getting utilization from SummaryTrending because SummaryBusinessUnitDepartment
		is already averaged, which may skew the numbers more than those we can get
		from SummaryTrending

	*/

	-- Utilization : this (past) week : HQ level
	update SummaryWeekly
	set UtilizationTW = coalesce(a.AvgUtilizationTW,0)
	from (
			select st.IDCustomer as HQID, Avg(Utilization) as AvgUtilizationTW
			from SummarySiteDepartment st
			join Customers bu on st.IDCustomer = bu.IDCustomer
			where [Date] between dateadd(day, -7, @Date) and cast(@Date as datetime)
			and Utilization <> 0
			group by st.IDCustomer
		) a
	where SummaryWeekly.[Date] = @Date
	and SummaryWeekly.HQID = a.HQID
	and SummaryWeekly.RowType = 'HQ'


	-- Utilization : last week : HQ level
	update SummaryWeekly
	set UtilizationLW = coalesce(a.AvgUtilizationLW,0)
	from (
			select st.IDCustomer as HQID, Avg(Utilization) as AvgUtilizationLW
			from SummarySiteDepartment st
			join Customers bu on st.IDCustomer = bu.IDCustomer
			where [Date] between dateadd(day, -14, @Date) and dateadd(day, -8, @Date) 
			and Utilization <> 0
			group by st.IDCustomer
		) a
	where SummaryWeekly.[Date] = @Date
	and SummaryWeekly.HQID = a.HQID
	and SummaryWeekly.RowType = 'HQ'



	-- Utilization : this (past) week : business unit level
	update SummaryWeekly
	set UtilizationTW = coalesce(a.AvgUtilizationTW,0)
	from (
			select SiteID, Avg(Utilization) as AvgUtilizationTW
			from SummaryTrending
			where [Date] between dateadd(day, -7, @Date) and cast(@Date as datetime)
			and Utilization <> 0
			group by SiteID
		) a
	where SummaryWeekly.[Date] = @Date
	and SummaryWeekly.SiteID = a.SiteID
	and SummaryWeekly.RowType = 'Facility'


	-- Utilization : last week : business unit level
	update SummaryWeekly
	set UtilizationLW = a.AvgUtilizationLW
	from (
			select SiteID, Avg(Utilization) as AvgUtilizationLW
			from SummaryTrending
			where [Date] between dateadd(day, -14, @Date) and dateadd(day, -8, @Date) 
			and Utilization <> 0
			group by SiteID
		) a
	where SummaryWeekly.[Date] = @Date
	and SummaryWeekly.SiteID = a.SiteID
	and SummaryWeekly.RowType = 'Facility'
		


	-- Utilization : this (past) week : department level
	update SummaryWeekly
	set UtilizationTW = a.AvgUtilizationTW
	from (
			select SiteID, DepartmentID, Avg(Utilization) as AvgUtilizationTW
			from SummaryTrending
			where [Date] between dateadd(day, -7, @Date) and cast(@Date as datetime)
			and Utilization <> 0
			group by SiteID, DepartmentID
		) a
	where SummaryWeekly.[Date] = @Date
	and SummaryWeekly.DepartmentID = a.DepartmentID
	and SummaryWeekly.RowType = 'Department'



	-- Utilization : last week : department level
	update SummaryWeekly
	set UtilizationLW = coalesce(a.AvgUtilizationLW,0)
	from (
			select SiteID, DepartmentID, Avg(Utilization) as AvgUtilizationLW
			from SummaryTrending
			where [Date] between dateadd(day, -14, @Date) and dateadd(day, -8, @Date) 
			and Utilization <> 0
			group by SiteID, DepartmentID
		) a
	where SummaryWeekly.[Date] = @Date
	and SummaryWeekly.DepartmentID = a.DepartmentID
	and SummaryWeekly.RowType = 'Department'

END
GO
