SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcDepartmentsSelect] @IDDepartment INT
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  

    BEGIN TRAN;

    SELECT  [IDDepartment] ,
            [Description] ,
            [SiteID]
    FROM    dbo.Departments
    WHERE   ( [IDDepartment] = @IDDepartment
              OR @IDDepartment IS NULL
            ); 

    COMMIT;



GO
