SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[prcDashGetCaseDetail] @SiteId VARCHAR(20) = 1328
AS
     BEGIN
	--SET @SiteId = 1365
	SET @SiteId = 693
         BEGIN
             --Select top 3000 isnull(astat.Description,'New, No Response') as "Case Status",am.DeviceSerialNumber,am.BatterySerialNumber,am.Description,am.CreatedDateUTC as "Case Opened Date", ('https://na32.salesforce.com/' + am.SFCID) as "Case Link",vsd.BatteryErrorCode,vsd.VoltageCell1,vsd.VoltageCell2,vsd.VoltageCell3  from AlertMessage am left join AlertStatus astat on am.AlertStatus = astat.ROW_ID  join viewAllSessionData vsd on am.BatterySerialNumber = vsd.BatterySerialNumber and am.DeviceSerialNumber = vsd.DeviceSerialNumber  and SFCID is not null and vsd.CreatedDateUTC  between dateadd(mm,-50,am.createddateutc) and dateadd(mm,50,am.createddateutc) and am.Priority = 911 and SiteID = @SiteID order by am.CreatedDateUTC desc
             SELECT 
             --                    ISNULL(astat.Description, 'New, No Response') AS "CaseStatus" ,
             ISNULL(cs.SFCaseStatus, 'New, No Response') AS "CaseStatus",
			  ISNULL(cs.SFCaseStatus, 'New, No Response') AS "Status ",
			 cs.Subject,
			  cs.SFCaseNumber,
		   u.FirstName + ' ' + u.LastName AS UserName,

             u.Title,
             u.ProfileImage,
			cs.ContactName,
			cs.ContactEmail,
			cs.SiteName,
			cs.ContactPhone,
			 cs.SFCaseStatus AS 'Status',
		 
       cs.SFCaseContactEmail ,
	   cs.SFCaseContactName ,
	   u2.Title AS SFCaseContactTitle,
	   cs.SroCreatedDate ,
       cs.SroCreatedBy ,
       cs.OrderStatusCode ,
	    cs.Origin ,
       cs.Original_Invoice_Number ,
	     cs.FedExTrackingUrl AS 'TrackingLink',
		   cs.FedExTrackingNo AS 'TrackingNumber',
		    CONVERT(VARCHAR,CAST(CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, cs.FedExShippingDate),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS DATETIME), 100)
		 AS 'ShipDate',
	       CONVERT(VARCHAR,CAST(CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, 	cs.Expected_Date_of_Delivery),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS DATETIME), 100)
			AS 'Expected Delivery Date',
			cs.POCEmail,
		
         
                 ISNULL( ISNULL(ISNULL(CONVERT(VARCHAR(24), LastModifiedDate, 100), CONVERT(VARCHAR(24), cs.open_date, 100)),cs.createdate),GETUTCDATE()) AS "CaseOpenedDate"
      
         --    ('https://na32.salesforce.com/'+am.SFCID) AS "CaseLink"

		--   am.Description
             FROM dbo.CasesMaster  AS cs
              
                  LEFT JOIN dbo.[User] AS U ON cs.SFCaseOwnerId = u.SFUserId
				    LEFT JOIN dbo.[User] AS U2 ON cs.ContactEmail = u2.Email
             --     LEFT JOIN dbo.Assets assbatt ON am.BatterySerialNumber = assbatt.SerialNo
             WHERE cs.IDSite = @SiteID
              AND u.FirstName IS NOT null
			    AND cs.SroNumber IS NOT NULL
                   AND cs.SFCaseNumber IS NOT NULL
                   AND cs.SFCaseStatus IS NOT null
                  -- AND am.Priority = 911
                --  AND cs.LastModifiedDate > DATEADD(DAY, -60, GETUTCDATE())
             ORDER BY cs.LastModifiedDate DESC;
         END;
     END;

GO
