SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Bill Murray>
-- Create date:	<8/16/2016>
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[prcDashGetBatteryAmpDrawSummary]
	@SiteID int= 657, @LookbackDays int= 7
AS


	SELECT DATEDIFF(day, s.EndDateUTC, GETUTCDATE())  AS ampDayWeek, DATENAME(WEEKDAY,s.EndDateUTC) AS ampDay, CONVERT(DECIMAL(3,2),SUM(-.001*s.AvgAmpDraw)/COUNT(*)) AS AvgAmpDraw, MAX(-.001*s.AvgAmpDraw) AS MaxAvgAmpDraw , s.SiteID, a.DepartmentId
	FROM Sessions s 
	INNER JOIN Assets a	
		ON a.SerialNo = s.DeviceSerialNumber
	WHERE
		s.SiteID = @SiteId
	AND
	DATEDIFF(day, EndDateUTC, GETUTCDATE()) BETWEEN 1 AND  @LookbackDays 
	GROUP BY
		DATEDIFF(day, s.EndDateUTC, GETUTCDATE()),
		DATENAME(WEEKDAY,s.EndDateUTC),
		a.DepartmentId,
	    s.SiteID
	ORDER BY 
		DATEDIFF(day, s.EndDateUTC, GETUTCDATE()) DESC;




	


GO
