SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcCustomersUpdate]
    @IDCustomer INT ,
    @CASTBUID VARCHAR(50) = NULL ,
    @CustomerName VARCHAR(50) ,
    @SFCustomerID VARCHAR(50) = NULL ,
    @SytelineCustomerNum VARCHAR(50) = NULL ,
    @SytelineCustomerSeq VARCHAR(50) = NULL
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
    BEGIN TRAN;

    UPDATE  dbo.Customers
    SET     [CASTBUID] = @CASTBUID ,
            [CustomerName] = @CustomerName ,
            [SFCustomerID] = @SFCustomerID ,
            [SytelineCustomerNum] = @SytelineCustomerNum ,
            [SytelineCustomerSeq] = @SytelineCustomerSeq
    WHERE   [IDCustomer] = @IDCustomer;
	
	-- Begin Return Select <- do not remove
    SELECT  [IDCustomer] ,
            [CASTBUID] ,
            [CustomerName] ,
            [SFCustomerID] ,
            [SytelineCustomerNum] ,
            [SytelineCustomerSeq]
    FROM    dbo.Customers
    WHERE   [IDCustomer] = @IDCustomer;	
	-- End Return Select <- do not remove

    COMMIT;



GO
