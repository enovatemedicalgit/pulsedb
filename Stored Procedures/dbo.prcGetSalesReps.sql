SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcGetSalesReps]
AS
    SET NOCOUNT ON; 
    SELECT  *
    FROM    dbo.[User]
    WHERE   UserTypeID = 2
            AND SFAcctOwnerID IS NOT NULL;

	



GO
