SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[spRestoreMaxCycleCount]
AS
    BEGIN
        DECLARE @sCommandText VARCHAR(200);
        SET @sCommandText = ( SELECT TOP 1
                                        CommandCodeText
                              FROM      dbo.CommandCode
                              WHERE     ROW_ID = 51
                            );
        DECLARE @SN VARCHAR(50);
        DECLARE @SubtractionNum VARCHAR(50);
        DECLARE @DeviceSerial VARCHAR(50);
        DECLARE @Bay INT;
        DECLARE @OldMaxCycleCount INT;
        DECLARE db_cursor CURSOR
        FOR
            SELECT DISTINCT
                    BatterySerialNumber
            FROM    dbo.NonSessionDataCurrent AS NSDC WITH ( NOLOCK )
            WHERE   DeviceType IN ( 2, 5 )
                    AND Amps <> 0
                    AND MaxCycleCount < 11
                    AND ChargeLevel < 99
                    AND CreatedDateUTC > DATEADD(day, -2, GETDATE())
                    AND BatterySerialNumber NOT IN (
                    SELECT  SerialNumber
                    FROM    dbo.DisabledBatteryList ); 

        OPEN db_cursor;   
        FETCH NEXT FROM db_cursor INTO @SN;   
        WHILE @@FETCH_STATUS = 0
            BEGIN  
                SET @DeviceSerial = ( SELECT TOP 1
                                                DeviceSerialNumber
                                      FROM      dbo.NonSessionDataCurrent   WITH ( NOLOCK )
                                      WHERE     BatterySerialNumber = @SN
                                                AND Amps <> 0
                                                AND ChargeLevel < 90
                                                AND Bay > 0
                                                AND ( CreatedDateUTC > DATEADD(MINUTE,
                                                              -40, GETDATE()) )
                                    );
                SET @Bay = ( SELECT TOP 1
                                    Bay
                             FROM   dbo.viewAllSessionData WITH ( NOLOCK )
                             WHERE  BatterySerialNumber = @SN
                                    AND Bay > 0
                                    AND Amps <> 0
                                    AND ( CreatedDateUTC > DATEADD(MINUTE, -40,
                                                              GETDATE()) )
                           );
                SET @OldMaxCycleCount = ( SELECT TOP 1
                                                    MaxCycleCount
                                          FROM      dbo.NonSessionDataCurrent  WITH ( NOLOCK )
                                          WHERE     BatterySerialNumber = @SN
                                                    AND Bay > 0
                                                    AND ChargeLevel < 90
                                                    AND ( MaxCycleCount > 10
                                                          AND MaxCycleCount <> 0
                                                          AND CreatedDateUTC > DATEADD(DAY,
                                                              -5, GETDATE())
                                                          AND CreatedDateUTC < DATEADD(DAY,
                                                              -2, GETDATE())
                                                        )
                                          ORDER BY  CreatedDateUTC ASC
                                        );
                IF ( @OldMaxCycleCount IS NULL
                     OR @OldMaxCycleCount < 100
                   )
                    BEGIN
                        SET @OldMaxCycleCount = ( SELECT TOP 1
                                                            MaxCycleCount
                                                  FROM      dbo.NonSessionDataCurrent 
                                                            WITH ( NOLOCK )
                                                  WHERE     BatterySerialNumber = @SN
                                                            AND ( MaxCycleCount > 10
                                                              AND MaxCycleCount <> 0
                                                              AND CreatedDateUTC > DATEADD(DAY,
                                                              -10, GETDATE())
                                                              )
                                                            AND CreatedDateUTC < DATEADD(DAY,
                                                              -5, GETDATE())
                                                  ORDER BY  CreatedDateUTC ASC
                                                );
                    END;
                IF ( @OldMaxCycleCount IS NULL
                     OR @OldMaxCycleCount < 100
                   )
                    BEGIN
                        SET @OldMaxCycleCount = ( SELECT TOP 1
                                                            MaxCycleCount
                                                  FROM      dbo.SessionDataCurrent
                                                  WHERE     BatterySerialNumber = @SN
                                                            AND ( MaxCycleCount > 10
                                                              AND MaxCycleCount <> 0
                                                              AND CreatedDateUTC > DATEADD(DAY,
                                                              -10, GETDATE())
                                                              )
                                                            AND CreatedDateUTC < DATEADD(DAY,
                                                              -5, GETDATE())
                                                  ORDER BY  CreatedDateUTC ASC
                                                );
                    END;
-- select MaxCycleCount from 8013BatteryList where SerialNumber = @SN
-- set @SubtractionNum = (@MaxCycleCount - 1)
                DECLARE @cnt1 INT;
                DECLARE @cnt2 INT;
                DECLARE @cnt3 INT;
                DECLARE @cnt4 INT;
                SET @cnt1 = ( SELECT    COUNT(SerialNumber)
                              FROM      dbo.CommandQueue
                              WHERE     SerialNumber = @DeviceSerial
                                        AND CommandText LIKE '809(1%'
                            );
                SET @cnt2 = ( SELECT    COUNT(SerialNumber)
                              FROM      dbo.CommandQueue
                              WHERE     SerialNumber = @DeviceSerial
                                        AND CommandText LIKE '809(2%'
                            );
                SET @cnt3 = ( SELECT    COUNT(SerialNumber)
                              FROM      dbo.CommandQueue
                              WHERE     SerialNumber = @DeviceSerial
                                        AND CommandText LIKE '809(3%'
                            );
                SET @cnt4 = ( SELECT    COUNT(SerialNumber)
                              FROM      dbo.CommandQueue
                              WHERE     SerialNumber = @DeviceSerial
                                        AND CommandText LIKE '809(4%'
                            );
                IF ( @Bay IS NOT NULL
                     AND ( @Bay = 1
                           OR @Bay = 2
                           OR @Bay = 3
                           OR @Bay = 4
                         )
                   )
                    BEGIN
                        BEGIN TRANSACTION; 
                        IF ( ( @Bay = 1
                               AND @cnt1 < 1
                             )
                             OR ( @Bay = 2
                                  AND @cnt2 < 1
                                )
                             OR ( @Bay = 3
                                  AND @cnt3 < 1
                                )
                             OR ( @Bay = 4
                                  AND @cnt4 < 1
                                )
                           )
                            IF ( @DeviceSerial IS NOT NULL
                                 AND @OldMaxCycleCount IS NOT NULL
                                 AND @OldMaxCycleCount <> 0
                               )
                                BEGIN
                                    BEGIN
                                        INSERT  INTO dbo.CommandQueue
                                                ( SerialNumber ,
                                                  CommandText
                                                )
                                        VALUES  ( @DeviceSerial ,
                                                  '809('
                                                  + CAST(@Bay AS VARCHAR(3))
                                                  + ')['
                                                  + CAST(@OldMaxCycleCount AS VARCHAR(5))
                                                  + ']'
                                                );
                                    END;
                                END;

                        COMMIT;
                    END;
                FETCH NEXT FROM db_cursor INTO @SN;
            END;   

        CLOSE db_cursor;   
        DEALLOCATE db_cursor;

    END;





GO
