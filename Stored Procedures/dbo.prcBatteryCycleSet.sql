SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- =============================================
CREATE PROCEDURE [dbo].[prcBatteryCycleSet] (@BatterySerialNumber varchar(50))
AS					
begin

	
	   DECLARE @sCommandText VARCHAR(200);
        SET @sCommandText = ( SELECT TOP 1
                                        CommandCodeText
                              FROM      dbo.CommandCode
                              WHERE     ROW_ID = 51
                            );
        DECLARE @SN VARCHAR(50);
        DECLARE @SubtractionNum VARCHAR(50);
        DECLARE @DeviceSerial VARCHAR(50);
        DECLARE @Bay INT;
        DECLARE @OldMaxCycleCount INT;
		DELETE FROM dbo.DisabledBatteryList WHERE SerialNumber = @BatterySerialNumber
		IF (EXISTS(SELECT SerialNumber FROM dbo.DisabledBatteryList AS DBL WHERE DBL.SerialNumber = @BatterySerialNumber))
		BEGIN
		DELETE FROM dbo.DisabledBatteryList WHERE SerialNumber = @BatterySerialNumber --AND DeviceSerial = @DeviceSerial;
		  SET @DeviceSerial = ( SELECT TOP 1
                                                DeviceSerialNumber
                                      FROM      dbo.NonSessionDataCurrent   WITH ( NOLOCK )
                                      WHERE     BatterySerialNumber = @BatterySerialNumber
                                                AND Amps <> 0
                                                AND ChargeLevel < 90
                                                AND Bay > 0
                                                AND ( CreatedDateUTC > DATEADD(MINUTE,
                                                              -40, GETDATE()) )
                                    );
                SET @Bay = ( SELECT TOP 1
                                    Bay
                             FROM   dbo.viewAllSessionData WITH ( NOLOCK )
                             WHERE  BatterySerialNumber =  @BatterySerialNumber
                                    AND Bay > 0
                                    AND Amps <> 0
                                    AND ( CreatedDateUTC > DATEADD(MINUTE, -40,
                                                              GETDATE()) )
                           );
                SET @OldMaxCycleCount = ( SELECT TOP 1
                                                    MaxCycleCount
                                          FROM      dbo.NonSessionDataCurrent  WITH ( NOLOCK )
                                          WHERE     BatterySerialNumber =  @BatterySerialNumber
                                                    AND Bay > 0
                                                    AND ChargeLevel < 90
                                                    AND ( MaxCycleCount > 10
                                                          AND MaxCycleCount <> 0
                                                        )
                                          ORDER BY  CreatedDateUTC ASC
                                        );
               
               
                DECLARE @cnt1 INT;
                DECLARE @cnt2 INT;
                DECLARE @cnt3 INT;
                DECLARE @cnt4 INT;
                SET @cnt1 = ( SELECT    COUNT(SerialNumber)
                              FROM      dbo.CommandQueue
                              WHERE     SerialNumber = @DeviceSerial
                                        AND CommandText LIKE '809(1%'
                            );
                SET @cnt2 = ( SELECT    COUNT(SerialNumber)
                              FROM      dbo.CommandQueue
                              WHERE     SerialNumber = @DeviceSerial
                                        AND CommandText LIKE '809(2%'
                            );
                SET @cnt3 = ( SELECT    COUNT(SerialNumber)
                              FROM      dbo.CommandQueue
                              WHERE     SerialNumber = @DeviceSerial
                                        AND CommandText LIKE '809(3%'
                            );
                SET @cnt4 = ( SELECT    COUNT(SerialNumber)
                              FROM      dbo.CommandQueue
                              WHERE     SerialNumber = @DeviceSerial
                                        AND CommandText LIKE '809(4%'
                            );
                IF ( @Bay IS NOT NULL
                     AND ( @Bay = 1
                           OR @Bay = 2
                           OR @Bay = 3
                           OR @Bay = 4
                         )
                   )
                    BEGIN
                        BEGIN TRANSACTION; 
                        IF ( ( @Bay = 1
                               AND @cnt1 < 1
                             )
                             OR ( @Bay = 2
                                  AND @cnt2 < 1
                                )
                             OR ( @Bay = 3
                                  AND @cnt3 < 1
                                )
                             OR ( @Bay = 4
                                  AND @cnt4 < 1
                                )
                           )
                            IF ( @DeviceSerial IS NOT NULL
                                 AND @OldMaxCycleCount IS NOT NULL
                                 AND @OldMaxCycleCount <> 0
                               )
                                BEGIN
                                    BEGIN
                                        INSERT  INTO dbo.CommandQueue
                                                ( SerialNumber ,
                                                  CommandText
                                                )
                                        VALUES  ( @DeviceSerial ,
                                                  '809('
                                                  + CAST(@Bay AS VARCHAR(3))
                                                  + ')['
                                                  + CAST(@OldMaxCycleCount AS VARCHAR(5))
                                                  + ']'
                                                );
                                    END;
                                END;

                        COMMIT;
        END
        ELSE
        begin
		  SET @DeviceSerial = ( SELECT TOP 1
                                                DeviceSerialNumber
                                      FROM      dbo.NonSessionDataCurrent   WITH ( NOLOCK )
                                      WHERE     BatterySerialNumber = @BatterySerialNumber
                                                AND Amps <> 0
                                                AND ChargeLevel < 90
                                                AND Bay > 0
                                                AND ( CreatedDateUTC > DATEADD(MINUTE,
                                                              -40, GETDATE()) )
                                    );
                SET @Bay = ( SELECT TOP 1
                                    Bay
                             FROM   dbo.viewAllSessionData WITH ( NOLOCK )
                             WHERE  BatterySerialNumber =  @BatterySerialNumber
                                    AND Bay > 0
                                    AND Amps <> 0
                                    AND ( CreatedDateUTC > DATEADD(MINUTE, -40,
                                                              GETDATE()) )
                           );
            
             
                SET @cnt1 = ( SELECT    COUNT(SerialNumber)
                              FROM      dbo.CommandQueue
                              WHERE     SerialNumber = @DeviceSerial
                                        AND CommandText LIKE '809(1%'
                            );
                SET @cnt2 = ( SELECT    COUNT(SerialNumber)
                              FROM      dbo.CommandQueue
                              WHERE     SerialNumber = @DeviceSerial
                                        AND CommandText LIKE '809(2%'
                            );
                SET @cnt3 = ( SELECT    COUNT(SerialNumber)
                              FROM      dbo.CommandQueue
                              WHERE     SerialNumber = @DeviceSerial
                                        AND CommandText LIKE '809(3%'
                            );
                SET @cnt4 = ( SELECT    COUNT(SerialNumber)
                              FROM      dbo.CommandQueue
                              WHERE     SerialNumber = @DeviceSerial
                                        AND CommandText LIKE '809(4%'
                            );
                IF ( @Bay IS NOT NULL
                     AND ( @Bay = 1
                           OR @Bay = 2
                           OR @Bay = 3
                           OR @Bay = 4
                         )
                   )
                    BEGIN
					
                        BEGIN TRANSACTION; 
                        IF ( ( @Bay = 1
                               AND @cnt1 < 1
                             )
                             OR ( @Bay = 2
                                  AND @cnt2 < 1
                                )
                             OR ( @Bay = 3
                                  AND @cnt3 < 1
                                )
                             OR ( @Bay = 4
                                  AND @cnt4 < 1
                                )
                           )
                                        INSERT  INTO dbo.CommandQueue
                                                ( SerialNumber ,
                                                  CommandText
                                                )
                                        VALUES  ( @DeviceSerial ,
                                                  '809('
                                                  + CAST(@Bay AS VARCHAR(3))
                                                  + ')[10]'
                                                );

												COMMIT TRANSACTION;
                                    END;
									INSERT INTO dbo.DisabledBatteryList
									        ( SerialNumber ,
									          MaxCycleCount ,
									          Bay ,
									          DeviceSerial ,
									          LastUpdateDateTime ,
									          LastPacketDate ,
									          SiteID ,
									          ManualEntryUser ,
									          InsertedDateUTC ,
									          SiteDescription ,
									          CommandSentOn
									        )
									VALUES  ( @BatterySerialNumber , -- SerialNumber - varchar(50)
									          '9999' , -- MaxCycleCount - varchar(50)
									          @Bay , -- Bay - int
									          @DeviceSerial , -- DeviceSerial - varchar(50)
									          GETUTCDATE() , -- LastUpdateDateTime - datetime
									          GETutcDATE() , -- LastPacketDate - datetime
									          0 , -- SiteID - int
									         CAST( (SELECT TOP 1 iduser FROM dbo.[User] AS U WHERE U.UserName LIKE '%Gordon.Waid%') AS NVARCHAR(59)), -- ManualEntryUser - nvarchar(50)
									          GETutcDATE() , -- InsertedDateUTC - datetime
									          N' ' , -- SiteDescription - nvarchar(50)
									          GETutcDATE()  -- CommandSentOn - datetime
									        )
					END;
END;
END
GO
