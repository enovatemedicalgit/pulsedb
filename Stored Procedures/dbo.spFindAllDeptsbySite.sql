SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Bill Murray
-- Create date:	7/6/2016
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spFindAllDeptsbySite] @SiteId INT
AS
    BEGIN
        SET NOCOUNT ON;

    -- Insert statements for procedure here
        SELECT  *
        FROM    dbo.Departments
        WHERE   SiteID = @SiteId
        ORDER BY Description;
    END;

GO
