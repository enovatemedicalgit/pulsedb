SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[prcDashGetChargerPacketsFast]
    @SiteID VARCHAR(20) = NULL ,
    @AssetSerialNumber VARCHAR(20) = NULL ,
    @RowLimit INT = 500 ,
    @StartDate DATETIME = NULL ,
    @EndDate DATETIME = NULL ,
    @IsAdmin INT = 0 ,
    @UserTypeID INT = 0
AS
    BEGIN


        IF ( @StartDate IS NULL )
            BEGIN
                SET @StartDate = DATEADD(HOUR, -12, GETUTCDATE());
                SET @EndDate = DATEADD(HOUR, 6, GETUTCDATE());
            END;

              
            BEGIN
                --PRINT 'ReturnSet3 - Site or Customer Packets, Recent';
                SELECT TOP ( @RowLimit )
            sd.DeviceSerialNumber AS Charger ,
                      sd.BatterySerialNumber ,
                    CONVERT(VARCHAR,CAST(CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, sd.CreatedDateUTC),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS DATETIME), 100) AS 'Local Time' ,
                    
                         d.AssetNumber ,  sd.Bay ,
                                                     
                                                
						 a.ActivityDescription AS "Activity" ,
                                                    bud.Description AS  Department ,
											
                                                    d.Floor ,
                                                    d.Wing ,
                                                    d.Other ,
                                                    sd.ChargeLevel ,
										
                                               

                                                    sd.LinkQuality ,
                                                  
                                                    sd.DeviceMAC ,
                                                  
													  sd.IP ,
                                                    sd.SourceIPAddress, 
													 sd.APMAC ,
													 sd.WifiFirmwareRevision ,
                                                    sd.BayWirelessRevision ,
                                                    sd.Bay1ChargerRevision ,
                                                    sd.Bay2ChargerRevision ,
                                                    sd.Bay3ChargerRevision ,
                                                    sd.Bay4ChargerRevision 
                FROM    dbo.NonSessionDataCurrent AS  sd ( NOLOCK )
                        INNER JOIN dbo.Assets (NOLOCK) d ON sd.DeviceSerialNumber = d.SerialNo
                        LEFT JOIN dbo.Departments bud ON d.DepartmentID = bud.IDDepartment
                        LEFT JOIN dbo.Sites bu ON d.SiteID = bu.IDSite
                        LEFT JOIN dbo.AssetType (NOLOCK) dt ON sd.DeviceType = dt.IDAssetType
                        LEFT JOIN dbo.vwActivity (NOLOCK) a ON sd.Activity = a.ActivityCode
                        LEFT JOIN dbo.AccessPoint (NOLOCK) ap ON d.AccessPointId = ap.ROW_ID
              
                WHERE   bu.IDSite IN (
                        SELECT  IDSite
                        FROM    dbo.Sites
                        WHERE   bu.IDSite = @SiteID) AND sd.DeviceType IN (2,4) and sd.CreatedDateUTC BETWEEN @StartDate AND @EndDate; --order by CreatedDateUTC desc
            END;
			 
			
    END;

GO
