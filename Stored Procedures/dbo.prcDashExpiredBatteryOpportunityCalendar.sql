SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[prcDashExpiredBatteryOpportunityCalendar]
@intSiteId INT = 693
AS

DECLARE @SiteId INT;
SET @SiteId = @intSiteId;


SELECT ass.SerialNo + ' - ' + 'Past Warranty Date' AS Title
	,DATEADD(MINUTE,1,DATEADD(DAY, ISNULL((
				SELECT DurationDays
				FROM dbo.WarrantyDuration
				WHERE SerialNumberPrefix = LEFT(ass.SerialNo, 7)
				), 5000), CAST('20' + SUBSTRING(ass.SerialNo, 8, 2) + '-' + SUBSTRING(ass.SerialNo, 10, 2) + '-01' AS DATETIME))) AS Start
	,DATEADD(MINUTE, - 1, DATEADD(DAY, ISNULL((
					SELECT DurationDays
					FROM dbo.WarrantyDuration
					WHERE SerialNumberPrefix = LEFT(ass.SerialNo, 7)
					), 5000) + 1, CAST('20' + SUBSTRING(ass.SerialNo, 8, 2) + '-' + SUBSTRING(ass.SerialNo, 10, 2) + '-01' AS DATETIME))) AS [End]
	,ISNULL(NULLIF(dbo.fnGetBatteryCapacity_Int(ass.SerialNo, ass.FullChargeCapacity), 0), 0) AS [Capacity Health]
FROM dbo.Assets ass
LEFT JOIN dbo.Sites s
	ON s.IDSite = ass.SiteID
LEFT JOIN dbo.Customers cus
	ON ass.SiteID = cus.IDCustomer
LEFT JOIN dbo.WarrantyDuration wd
	ON wd.SerialNumberPrefix = LEFT(ass.SerialNo, 7)
WHERE ass.LastPostDateUTC > DATEADD(DAY, - 30, GETDATE())
	AND ass.IDAssetType = 5
	AND GETDATE() > DATEADD(DAY, ISNULL((
				SELECT DurationDays
				FROM dbo.WarrantyDuration
				WHERE SerialNumberPrefix = LEFT(ass.SerialNo, 7)
				), 5000), CAST('20' + SUBSTRING(ass.SerialNo, 8, 2) + '-' + SUBSTRING(ass.SerialNo, 10, 2) + '-01 12:00:00.000' AS DATETIME))
	AND dbo.SerialNumberClassification(ass.SerialNo) = 1
	AND ass.MaxCycleCount <> 0
	AND ass.MaxCycleCount <> 10
	AND ass.SiteID = @SiteId
	AND ass.FullChargeCapacity > 0
	AND ass.Retired = 0

UNION ALL

SELECT ass.SerialNo + ' - ' + 'Past Cycle Count' AS Title
	,DATEADD(MINUTE,1,DATEADD(DAY, ISNULL((
				SELECT DurationDays
				FROM dbo.WarrantyDuration
				WHERE SerialNumberPrefix = LEFT(ass.SerialNo, 7)
				), 5000), CAST('20' + SUBSTRING(ass.SerialNo, 8, 2) + '-' + SUBSTRING(ass.SerialNo, 10, 2) + '-01' AS DATETIME))) AS Start
	,DATEADD(MINUTE, - 1, DATEADD(DAY, ISNULL((
					SELECT DurationDays
					FROM dbo.WarrantyDuration
					WHERE SerialNumberPrefix = LEFT(ass.SerialNo, 7)
					), 5000) + 1, CAST('20' + SUBSTRING(ass.SerialNo, 8, 2) + '-' + SUBSTRING(ass.SerialNo, 10, 2) + '-01' AS DATETIME))) AS [End]
	,ISNULL(NULLIF(dbo.fnGetBatteryCapacity_Int(ass.SerialNo, ass.FullChargeCapacity), 0), 0) AS [Capacity Health]
FROM dbo.Assets ass
LEFT JOIN dbo.Sites s
	ON s.IDSite = ass.SiteID
LEFT JOIN dbo.Customers cus
	ON ass.SiteID = cus.IDCustomer
LEFT JOIN dbo.WarrantyDuration wd
	ON wd.SerialNumberPrefix = LEFT(ass.SerialNo, 7)
WHERE ass.IDAssetType = 5
	AND ass.CycleCount > ass.MaxCycleCount
	AND ass.MaxCycleCount != 0
	AND ass.MaxCycleCount IS NOT NULL
	AND ass.MaxCycleCount <> 0
	AND ass.MaxCycleCount <> 10
	AND ass.FullChargeCapacity <> 22000
	AND ass.CycleCount <> 0
	AND ass.CycleCount IS NOT NULL
	AND dbo.SerialNumberClassification(ass.SerialNo) = 1
	AND ass.FullChargeCapacity > 0
	AND ass.Retired = 0
	AND s.IDSite = @SiteID 




GO
