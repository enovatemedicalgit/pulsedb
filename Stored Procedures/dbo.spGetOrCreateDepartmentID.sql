SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Arlow Farrell
-- Create date: 2011-09-12
-- Description:	Returns the IDDepartment from table
--              Departments which
--              corresponds with the supplied
--              Department description.
--              
--              If the description does not
--              exist for the supplied SiteID,
--              it will be added and the newly
--              created IDDepartment will be returned.
--              
--              If the description IS found,
--              the existing IDDepartment is returned.
--              
-- =============================================
CREATE PROCEDURE [dbo].[spGetOrCreateDepartmentID]
    (
      @SiteID INT ,
      @Description VARCHAR(50)
    )
AS
    BEGIN
        SET NOCOUNT ON;

        IF ( LOWER(RTRIM(COALESCE(@Description, ''))) IN ( '', 'unallocated' ) )
            BEGIN
                SELECT  0 AS IDDepartment;	-- 0 is the global value for Unallocated
                RETURN;
            END;

        IF ( SELECT COUNT(IDDepartment)
             FROM   dbo.Departments
             WHERE  SiteID = @SiteID
                    AND Description = @Description
           ) = 0
            BEGIN
		-- doesn't exist yet. Add the description and return it's new IDDepartment
                INSERT  INTO dbo.Departments
                        ( Description, SiteID )
                VALUES  ( @Description, @SiteID );
		
                SELECT  SCOPE_IDENTITY() AS IDDepartment;
                RETURN;
            END;
        ELSE
            BEGIN
                SELECT TOP 1
                        IDDepartment
                FROM    dbo.Departments
                WHERE   SiteID = @SiteID
                        AND Description = @Description;
            END;

    END;


GO
