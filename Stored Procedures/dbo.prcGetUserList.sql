SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:      <ENOVATE\arlow.farrell,ARLOWFARRELL>
-- Create date: <10/13/2016> <8:31 PM>
-- Description: 
--				Enovate (@enovatemedical.com) Employees   
--				Else Any Employee
-- 
-- =============================================


CREATE PROCEDURE [dbo].[prcGetUserList]
  @UserTypeID INT = NULL ,
  @IsEnovate int = 0
AS
BEGIN
SET NOCOUNT ON;

IF (@IsEnovate = 1)
begin
SELECT U.FirstName + ' ' + U.LastName AS UserFL,u.UserName,u.Email,U.LastLoginDateUTC,ut.Description AS UserLevel,ISNULL(s.SiteName,c.CustomerName) AS 'Site/IDN Name',u.IDSite AS AssignedSiteID FROM dbo.[User] AS U LEFT JOIN dbo.UserType AS UT ON u.UserTypeID=ut.IDUserType LEFT JOIN dbo.Sites AS S ON s.IDSite = u.IDSite LEFT JOIN dbo.Customers AS C ON c.IDCustomer = u.IDSite WHERE U.UserName LIKE '%enovatemedical%' ORDER BY ut.IDUserType desc,U.LastLoginDateUTC DESC
END
ELSE
begin
SELECT U.FirstName + ' ' + U.LastName AS UserFL,u.UserName,u.Email,U.LastLoginDateUTC,ut.Description AS UserLevel,ISNULL(s.SiteName,c.CustomerName) AS 'Site/IDN Name',u.IDSite AS AssignedSiteId FROM dbo.[User] AS U LEFT JOIN dbo.UserType AS UT ON u.UserTypeID=ut.IDUserType LEFT JOIN dbo.Sites AS S ON s.IDSite = u.IDSite LEFT JOIN dbo.Customers AS C ON c.IDCustomer = u.IDSite ORDER BY ut.IDUserType desc,U.LastLoginDateUTC DESC
end
END;


GO
