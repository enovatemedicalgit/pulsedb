SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Arlow Farrell
-- Create date: 2012-02-01
-- Description:	Updates charger revision columns
--              on the Device table using
--              most recent data from
--              NonSessionDataCurrent
--
-- Modified:    2012-06-12 by Arlow Farrell
-- Reason:      Job was taking too long to run.
--              Changed the first where clause
--              from:
--              
--              WHERE CreatedDateUTC > dateadd(hh, -12, getutcdate())
--              and CreatedDateUTC in (...)
--              
--              to:
--              
--              WHERE CreatedDateUTC in (...)
--              
--              (then, to make sure we only use
--               rows from the past hour, I 
--               added the CreatedDateUTC > dateadd(hh, -12, getutcdate())
--               to each WHERE clause in the 
--               various update statements near
--               the bottom of the job)
--
-- Modified:     2012-12-13 by Arlow Farrell
-- Reason:       Temp table needed to include at
--               least one packet from each
--               bay in the charger so it could
--               then have the needed revision
--               data for updating the various
--               columns on the Device table.
--               
-- =============================================
CREATE PROCEDURE [dbo].[spChargerRevisions_Update]
AS
    BEGIN
        SET NOCOUNT ON;

        IF OBJECT_ID(N'tempdb..#tmpChargerRevs', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tmpChargerRevs;
            END;

        SELECT  subq.DeviceSerialNumber ,
                subq.WifiFirmwareRevision ,
                subq.DeviceMAC ,
                subq.BayWirelessRevision ,
                subq.Bay1ChargerRevision ,
                subq.Bay2ChargerRevision ,
                subq.Bay3ChargerRevision ,
                subq.Bay4ChargerRevision ,
                subq.CreatedDateUTC ,
                subq.Source ,
                subq.row_id ,
                subq.Bay
        INTO    #tmpChargerRevs
        FROM    (

		--select DeviceSerialNumber
		--	, WiFiFirmwareRevision
		--	, DeviceMAC
		--	, BayWirelessRevision
		--	, Bay1ChargerRevision
		--	, Bay2ChargerRevision
		--	, Bay3ChargerRevision
		--	, Bay4ChargerRevision
		--	, CreatedDateUTC
		--	, 'NonSessionDataCurrent' as Source
		--from NonSessionDataCurrent nsd
		--where CreatedDateUTC in (
		--	select max(CreatedDateUTC)
		--	from NonSessionDataCurrent b
		--	where CreatedDateUTC > dateadd(n, -1, getutcdate())
		--	and DeviceSerialNumber <> ''
		--	and DeviceType in (select ID from DeviceType where Category = 'Charger')
		--	and b.DeviceSerialNumber = nsd.DeviceSerialNumber
		--)
		--and DeviceSerialNumber <> ''
		--and DeviceType in (select ID from DeviceType where Category = 'Charger')
                  SELECT    nsd.DeviceSerialNumber ,
                            nsd.WifiFirmwareRevision ,
                            nsd.DeviceMAC ,
                            nsd.BayWirelessRevision ,
                            nsd.Bay1ChargerRevision ,
                            nsd.Bay2ChargerRevision ,
                            nsd.Bay3ChargerRevision ,
                            nsd.Bay4ChargerRevision ,
                            nsd.CreatedDateUTC ,
                            'NonSessionDataCurrent' AS Source ,
                            nsd.ROW_ID ,
                            nsd.Bay
                  FROM      dbo.NonSessionDataCurrent nsd
                  WHERE     nsd.CreatedDateUTC IN (
                            SELECT  MAX(b.CreatedDateUTC)
                            FROM    dbo.NonSessionDataCurrent b
                            WHERE   b.CreatedDateUTC > DATEADD(n, -60,
                                                             GETUTCDATE())
                                    AND b.DeviceSerialNumber <> ''
                                    AND b.DeviceType IN (
                                    SELECT  IDAssetType
                                    FROM    dbo.AssetType
                                    WHERE   Category = 'Charger' )
                                    AND b.DeviceSerialNumber = nsd.DeviceSerialNumber
                                    AND b.Bay = 1 )
                            AND nsd.DeviceSerialNumber <> ''
                            AND nsd.DeviceType IN ( SELECT  IDAssetType
                                                FROM    dbo.AssetType
                                                WHERE   Category = 'Charger' )
                            AND nsd.Bay = 1
                  UNION ALL
                  SELECT    nsd.DeviceSerialNumber ,
                            nsd.WifiFirmwareRevision ,
                            nsd.DeviceMAC ,
                            nsd.BayWirelessRevision ,
                            nsd.Bay1ChargerRevision ,
                            nsd.Bay2ChargerRevision ,
                            nsd.Bay3ChargerRevision ,
                            nsd.Bay4ChargerRevision ,
                            nsd.CreatedDateUTC ,
                            'NonSessionDataCurrent' AS Source ,
                            nsd.ROW_ID ,
                            nsd.Bay
                  FROM      dbo.NonSessionDataCurrent nsd
                  WHERE     nsd.CreatedDateUTC IN (
                            SELECT  MAX(b.CreatedDateUTC)
                            FROM    dbo.NonSessionDataCurrent b
                            WHERE   b.CreatedDateUTC > DATEADD(n, -60,
                                                             GETUTCDATE())
                                    AND b.DeviceSerialNumber <> ''
                                    AND b.DeviceType IN (
                                    SELECT  IDAssetType
                                    FROM    dbo.AssetType
                                    WHERE   Category = 'Charger' )
                                    AND b.DeviceSerialNumber = nsd.DeviceSerialNumber
                                    AND b.Bay = 2 )
                            AND nsd.DeviceSerialNumber <> ''
                            AND nsd.DeviceType IN ( SELECT  IDAssetType
                                                FROM    dbo.AssetType
                                                WHERE   Category = 'Charger' )
                            AND nsd.Bay = 2
                  UNION ALL
                  SELECT    nsd.DeviceSerialNumber ,
                            nsd.WifiFirmwareRevision ,
                            nsd.DeviceMAC ,
                            nsd.BayWirelessRevision ,
                            nsd.Bay1ChargerRevision ,
                            nsd.Bay2ChargerRevision ,
                            nsd.Bay3ChargerRevision ,
                            nsd.Bay4ChargerRevision ,
                            nsd.CreatedDateUTC ,
                            'NonSessionDataCurrent' AS Source ,
                            nsd.ROW_ID ,
                            nsd.Bay
                  FROM      dbo.NonSessionDataCurrent nsd
                  WHERE     nsd.CreatedDateUTC IN (
                            SELECT  MAX(b.CreatedDateUTC)
                            FROM    dbo.NonSessionDataCurrent b
                            WHERE   b.CreatedDateUTC > DATEADD(n, -60,
                                                             GETUTCDATE())
                                    AND b.DeviceSerialNumber <> ''
                                    AND b.DeviceType IN (
                                    SELECT  IDAssetType
                                    FROM    dbo.AssetType
                                    WHERE   Category = 'Charger' )
                                    AND b.DeviceSerialNumber = nsd.DeviceSerialNumber
                                    AND b.Bay = 3 )
                            AND nsd.DeviceSerialNumber <> ''
                            AND nsd.DeviceType IN ( SELECT  IDAssetType
                                                FROM    dbo.AssetType
                                                WHERE   Category = 'Charger' )
                            AND nsd.Bay = 3
                  UNION ALL
                  SELECT    nsd.DeviceSerialNumber ,
                            nsd.WifiFirmwareRevision ,
                            nsd.DeviceMAC ,
                            nsd.BayWirelessRevision ,
                            nsd.Bay1ChargerRevision ,
                            nsd.Bay2ChargerRevision ,
                            nsd.Bay3ChargerRevision ,
                            nsd.Bay4ChargerRevision ,
                            nsd.CreatedDateUTC ,
                            'NonSessionDataCurrent' AS Source ,
                            nsd.ROW_ID ,
                            nsd.Bay
                  FROM      dbo.NonSessionDataCurrent nsd
                  WHERE     nsd.CreatedDateUTC IN (
                            SELECT  MAX(b.CreatedDateUTC)
                            FROM    dbo.NonSessionDataCurrent b
                            WHERE   b.CreatedDateUTC > DATEADD(n, -60,
                                                             GETUTCDATE())
                                    AND b.DeviceSerialNumber <> ''
                                    AND b.DeviceType IN (
                                    SELECT  IDAssetType
                                    FROM    dbo.AssetType
                                    WHERE   Category = 'Charger' )
                                    AND b.DeviceSerialNumber = nsd.DeviceSerialNumber
                                    AND b.Bay = 4 )
                            AND nsd.DeviceSerialNumber <> ''
                            AND nsd.DeviceType IN ( SELECT  IDAssetType
                                                FROM    dbo.AssetType
                                                WHERE   Category = 'Charger' )
                            AND nsd.Bay = 4
                ) subq;



        UPDATE  dbo.Assets
        SET     DeviceMAC = t.DeviceMAC
        FROM    ( SELECT DISTINCT
                            DeviceSerialNumber ,
                            DeviceMAC
                  FROM      #tmpChargerRevs
                  WHERE     DeviceMAC LIKE '%:%:%:%:%:%'
                            AND CreatedDateUTC > DATEADD(n, -60, GETUTCDATE())
		--order by ROW_ID desc
                ) t
        WHERE   SerialNo = t.DeviceSerialNumber;


        UPDATE  dbo.Assets
        SET     WiFiFirmwareRevision = t.WiFiFirmwareRevision
        FROM    ( SELECT DISTINCT
                            DeviceSerialNumber ,
                            WifiFirmwareRevision
                  FROM      #tmpChargerRevs
                  WHERE     WifiFirmwareRevision LIKE '%[0-9].[0-9]%'
                            AND CreatedDateUTC > DATEADD(n, -60, GETUTCDATE())
		--order by ROW_ID desc
                ) t
        WHERE   SerialNo = t.DeviceSerialNumber;


        UPDATE  dbo.Assets
        SET     BayWirelessRevision = t.BayWirelessRevision
        FROM    ( SELECT DISTINCT
                            DeviceSerialNumber ,
                            BayWirelessRevision
                  FROM      #tmpChargerRevs
                  WHERE     BayWirelessRevision LIKE '%[0-9].[0-9]%'
                            AND CreatedDateUTC > DATEADD(n, -60, GETUTCDATE())
		--order by ROW_ID desc
                ) t
        WHERE   SerialNo = t.DeviceSerialNumber;


        UPDATE  dbo.Assets
        SET     Bay1ChargerRevision = t.Bay1ChargerRevision
        FROM    ( SELECT DISTINCT
                            DeviceSerialNumber ,
                            Bay1ChargerRevision ,
                            Bay
                  FROM      #tmpChargerRevs
                  WHERE     Bay1ChargerRevision LIKE '%[0-9].[0-9]%'
                            AND CreatedDateUTC > DATEADD(n, -60, GETUTCDATE())
                            AND Bay = 1
		--order by ROW_ID desc
                ) t
        WHERE   SerialNo = t.DeviceSerialNumber
                AND t.Bay = 1;



        UPDATE  dbo.Assets
        SET     Bay2ChargerRevision = t.Bay2ChargerRevision
        FROM    ( SELECT DISTINCT
                            DeviceSerialNumber ,
                            Bay2ChargerRevision ,
                            Bay
                  FROM      #tmpChargerRevs
                  WHERE     Bay2ChargerRevision LIKE '%[0-9].[0-9]%'
                            AND CreatedDateUTC > DATEADD(n, -60, GETUTCDATE())
                            AND Bay = 2
		--order by ROW_ID desc
                ) t
        WHERE   SerialNo = t.DeviceSerialNumber
                AND t.Bay = 2;


        UPDATE  dbo.Assets
        SET     Bay3ChargerRevision = t.Bay3ChargerRevision
        FROM    ( SELECT DISTINCT
                            DeviceSerialNumber ,
                            Bay3ChargerRevision ,
                            Bay
                  FROM      #tmpChargerRevs
                  WHERE     Bay3ChargerRevision LIKE '%[0-9].[0-9]%'
                            AND CreatedDateUTC > DATEADD(n, -60, GETUTCDATE())
                            AND Bay = 3
		--order by ROW_ID desc
                ) t
        WHERE   SerialNo = t.DeviceSerialNumber
                AND t.Bay = 3;


        UPDATE  dbo.Assets
        SET     Bay4ChargerRevision = t.Bay4ChargerRevision
        FROM    ( SELECT DISTINCT
                            DeviceSerialNumber ,
                            Bay4ChargerRevision ,
                            Bay
                  FROM      #tmpChargerRevs
                  WHERE     Bay4ChargerRevision LIKE '%[0-9].[0-9]%'
                            AND CreatedDateUTC > DATEADD(n, -60, GETUTCDATE())
                            AND Bay = 4
		--order by ROW_ID desc
                ) t
        WHERE   SerialNo = t.DeviceSerialNumber
                AND t.Bay = 4;


    END;



GO
