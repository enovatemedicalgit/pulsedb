SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spPurgeOldData_SessionData]
AS
    BEGIN
        SET NOCOUNT ON;

		----------------------------------------------------------------------------------------------------------------
		-- VARIABLES
		----------------------------------------------------------------------------------------------------------------
        DECLARE @dateFloor DATETIME2;
        DECLARE @dateCalc DATETIME2;
        DECLARE @MaxCreatedDateOnHistorical DATETIME2;
		---------------------------------------------------
		-- set this to the number of days you want to keep (ie. -365 for one year, -120, -90, -30, etc. )
		---------------------------------------------------
        DECLARE @daysToKeep INT;
        SET @daysToKeep = 15; --(4 months plus buffer)
		
		----------------------------------------------------------------------------------------------------------------

        SET @dateCalc = DATEADD(DAY, ( @daysToKeep * -1 ), GETUTCDATE());

		-- @dateFloor becomes the date that is the start of the day for the day found above. 
		-- This way we remove records by full days.
        SET @dateFloor = CONVERT(DATETIME, ( CONVERT(VARCHAR(10), @dateCalc, 111) ));
        SET @MaxCreatedDateOnHistorical = ( SELECT  MAX(CreatedDateUTC)
                                            FROM    PulseHistorical.dbo.SessionData
                                          );
      
		--LOOP : this starts looping through and deleting records by how many we set for each pass until we delete all records below the date floor
        WHILE EXISTS ( SELECT   1
                       FROM     dbo.SessionDataCurrent
                       WHERE    CreatedDateUTC < @dateFloor )
            BEGIN
		
			---------------------------------------------------
			-- Do the deletion from the table
			-- Set how many records to delete on each pass using the TOP command
			-- A low number prevents out of control transaction log growth.
			---------------------------------------------------
				SET IDENTITY_INSERT PulseHistorical.dbo.NonSessionData OFF	
					SET IDENTITY_INSERT PulseHistorical.dbo.SessionData ON
		
                INSERT  INTO PulseHistorical.dbo.SessionData
                        ( [DeviceSerialNumber] ,
                          [BatterySerialNumber] ,
                          [DeviceType] ,
                          [Activity] ,
                          [Bay] ,
                          [FullChargeCapacity] ,
                          [Voltage] ,
                          [Amps] ,
                          [Temperature] ,
                          [ChargeLevel] ,
                          [CycleCount] ,
                          [MaxCycleCount] ,
                          [VoltageCell1] ,
                          [VoltageCell2] ,
                          [VoltageCell3] ,
                          [FETStatus] ,
                          [RemainingTime] ,
                          [BatteryName] ,
                          [Efficiency] ,
                          [ControlBoardRevision] ,
                          [LCDRevision] ,
                          [HolsterChargerRevision] ,
                          [DCBoardOneRevision] ,
                          [DCBoardTwoRevision] ,
                          [WifiFirmwareRevision] ,
                          [BayWirelessRevision] ,
                          [Bay1ChargerRevision] ,
                          [Bay2ChargerRevision] ,
                          [Bay3ChargerRevision] ,
                          [Bay4ChargerRevision] ,
                          [MedBoardRevision] ,
                          [BackupBatteryVoltage] ,
                          [BackupBatteryStatus] ,
                          [IsAC] ,
                          [DCUnit1AVolts] ,
                          [DCUnit1ACurrent] ,
                          [DCUnit1BVolts] ,
                          [DCUnit1BCurrent] ,
                          [DCUnit2AVolts] ,
                          [DCUnit2ACurrent] ,
                          [DCUnit2BVolts] ,
                          [DCUnit2BCurrent] ,
                          [XValue] ,
                          [XMax] ,
                          [YValue] ,
                          [YMax] ,
                          [ZValue] ,
                          [ZMax] ,
                          [Move] ,
                          [BatteryStatus] ,
                          [SafetyStatus] ,
                          [PermanentFailureStatus] ,
                          [PermanentFailureAlert] ,
                          [BatteryChargeStatus] ,
                          [BatterySafetyAlert] ,
                          [BatteryOpStatus] ,
                          [BatteryMode] ,
                          [DC1Error] ,
                          [DC1Status] ,
                          [DC2Error] ,
                          [DC2Status] ,
                          [MouseFailureNotification] ,
                          [KeyboardFailureNotification] ,
                          [WindowsShutdownNotification] ,
                          [LinkQuality] ,
                          [IP] ,
                          [DeviceMAC] ,
                          [APMAC] ,
                          [LastTransmissionStatus] ,
                          [AuthType] ,
                          [ChannelNumber] ,
                          [PortNumber] ,
                          [DHCP] ,
                          [WEPKey] ,
                          [PassCode] ,
                          [StaticIP] ,
                          [SSID] ,
                          [SparePinSwitch] ,
                          [ControlBoardSerialNumber] ,
                          [HolsterBoardSerialNumber] ,
                          [LCDBoardSerialNumber] ,
                          [DC1BoardSerialNumber] ,
                          [DC2BoardSerialNumber] ,
                          [MedBoardSerialNumber] ,
                          [BayWirelessBoardSerialNumber] ,
                          [Bay1BoardSerialNumber] ,
                          [Bay2BoardSerialNumber] ,
                          [Bay3BoardSerialNumber] ,
                          [Bay4BoardSerialNumber] ,
                          [MedBoardDrawerOpenTime] ,
                          [MedBoardDrawerCount] ,
                          [MedBoardMotorUp] ,
                          [MedBoardMotorDown] ,
                          [MedBoardUnlockCount] ,
                          [MedBoardAdminPin] ,
                          [MedBoardNarcPin] ,
                          [MedBoardUserPin1] ,
                          [MedBoardUserPin2] ,
                          [MedBoardUserPin3] ,
                          [MedBoardUserPin4] ,
                          [MedBoardUserPin5] ,
                          [GenericError] ,
                          [SessionRecordType] ,
                          [QueryStringID] ,
                          [SessionID] ,
                          [CommandCode] ,
                          [ROW_ID] ,
                          [CreatedDateUTC] ,
                          [ExaminedDateUTC] ,
                          [SourceIPAddress] ,
                          [MeasuredVoltage] ,
                          [BatteryErrorCode] ,
                          [InsertedDateUTC] ,
                          [QueryStringBatchID] ,
                          [CheckedByRX] ,
                          [PacketType]
                        )
                        SELECT TOP 5000 *
                        FROM    dbo.SessionDataCurrent
                        WHERE   CreatedDateUTC < @MaxCreatedDateOnHistorical
                                AND CreatedDateUTC > @dateFloor
                        ORDER BY CreatedDateUTC ASC;
				

                DELETE  FROM dbo.SessionDataCurrent
                WHERE   ROW_ID IN (
                        SELECT TOP 5000
                                ROW_ID
                        FROM    dbo.SessionDataCurrent
                        WHERE   CreatedDateUTC < @dateFloor
                                AND CreatedDateUTC > @MaxCreatedDateOnHistorical
                        ORDER BY CreatedDateUTC ASC );

                DELETE  FROM dbo.SessionData_Alerts
                WHERE   ROW_ID IN (
                        SELECT TOP 5000
                                SDAa.ROW_ID
                        FROM    dbo.SessionData_Alerts AS SDAa
                        WHERE   SDAa.CreatedDateUTC < DATEADD(HOUR,-12,GETUTCDATE())
                        ORDER BY SDAa.CreatedDateUTC ASC );

           

                DELETE  FROM dbo.SessionData_Summary
                WHERE   ROW_ID IN (
                        SELECT TOP 5000
                                SDAs.ROW_ID
                        FROM    dbo.SessionData_Summary AS SDAs
                        WHERE   SDAs.CreatedDateUTC < DATEADD(DAY,10,@dateFloor)
                                AND SDAs.CreatedDateUTC > @MaxCreatedDateOnHistorical
                        ORDER BY SDAs.CreatedDateUTC ASC );
            END;
    END;

GO
