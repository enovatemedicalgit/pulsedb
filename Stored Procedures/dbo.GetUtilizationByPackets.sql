SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GetUtilizationByPackets]
as

WITH BandsByDevice AS
(SELECT 
    DeviceSerialNumber,
    MinCurrent,
    (MinCurrent + (CurrentDifferential*.2)) AS Band20,
    (MinCurrent + (CurrentDifferential*.4)) AS Band40,
    (MinCurrent + (CurrentDifferential*.6)) AS Band60,
    (MinCurrent + (CurrentDifferential*.8)) AS Band80,
    MaxCurrent
FROM        
    (
    SELECT 
	    DeviceSerialNumber,
	    MAX(MinCurrent) AS MinCurrent,
	    MAX(MaxCurrent) AS MaxCurrent,
	    MAX(MaxCurrent) - MAX(MinCurrent) AS CurrentDifferential
    FROM
	   (
	   SELECT 
		  DeviceSerialNumber, 
		  MIN(DCUnit1aCurrent) AS MinCurrent,
		  MAX(DCUnit1aCurrent) AS MaxCurrent,
		  '1a' AS Channel

	   FROM 
		  dbo.SessionDataCurrent
	   WHERE
		  DATEDIFF(day,Sessiondatacurrent.CreatedDateUTC, GETUTCDATE()) <= 2 AND
		  DCUnit1aCurrent >100 AND
		  DeviceType IN (6,3,1)
	   GROUP BY
		  DeviceSerialNumber

	   UNION ALL

	   SELECT 
		  DeviceSerialNumber, 
		  MIN(DCUnit1bCurrent) AS MinCurrent,
		  MAX(DCUnit1BCurrent) AS MaxCurrent,
		  '1b' AS Channel
	   FROM 
		  dbo.SessionDataCurrent
	   WHERE
		  DATEDIFF(day,Sessiondatacurrent.CreatedDateUTC, GETUTCDATE()) <= 2 AND
		  DCUnit1BCurrent >100 AND
		  DeviceType IN (6,3,1)
	   GROUP BY
		  DeviceSerialNumber

	   UNION ALL

	   SELECT 
		  DeviceSerialNumber, 
		  MIN(DCUnit2aCurrent) AS MinCurrent,
		  MAX(DCUnit2aCurrent) AS MaxCurrent,
		  '2a' AS Channel
	   FROM 
		  dbo.SessionDataCurrent
	   WHERE
		  DATEDIFF(day,Sessiondatacurrent.CreatedDateUTC, GETUTCDATE()) <= 2 AND
		  DCUnit2aCurrent >100 AND
		  DeviceType IN (6,3,1)
	   GROUP BY
		  DeviceSerialNumber

	   UNION ALL

	   SELECT 
		  DeviceSerialNumber, 
		  MIN(DCUnit2bCurrent) AS MinCurrent,
		  MAX(DCUnit2bCurrent) AS MaxCurrent,
		  '2b' AS Channel
	   FROM 
		  dbo.SessionDataCurrent
	   WHERE
		  DATEDIFF(day,Sessiondatacurrent.CreatedDateUTC, GETUTCDATE()) <= 2 AND
		  DCUnit2bCurrent >100 AND
		  DeviceType IN (6,3,1)
	   GROUP BY
		  DeviceSerialNumber
	   ) AS u

    GROUP BY 
	   DeviceSerialNumber
    ) b
)
, PacketBandsByDevice as
(
   SELECT 
    DeviceSerialNumber,
    DATEADD(HOUR,DATEDIFF(hour,GETUTCDATE(),GETDATE()), CreatedDateUTC) CreatedDateLocal,
    CurrentBand
    FROM	  
    (
    SELECT  
	   DeviceSerialNumber,
	   CreatedDateUTC,
	   'Band0' AS CurrentBand
    FROM 
	   SessionDataCurrent sd
    WHERE
	   EXISTS (SELECT 1 
			 FROM BandsByDevice b 
			 WHERE b.DeviceSerialNumber = sd.DeviceSerialNumber 
			 AND (
				    sd.DCUnit1ACurrent BETWEEN b.MinCurrent AND b.Band20
				    OR
				    sd.DCUnit1bCurrent BETWEEN b.MinCurrent AND b.Band20
				    OR
				    sd.DCUnit2ACurrent BETWEEN b.MinCurrent AND b.Band20
				    OR
				    sd.DCUnit2bCurrent BETWEEN b.MinCurrent AND b.Band20				   
				)
			 )
    UNION ALL
    SELECT  
	   DeviceSerialNumber,
	   CreatedDateUTC,
	   'Band20' AS CurrentBand
    FROM 
	   SessionDataCurrent sd
    WHERE
	   EXISTS (SELECT 1 
			 FROM BandsByDevice b 
			 WHERE b.DeviceSerialNumber = sd.DeviceSerialNumber 
			 AND (
				    sd.DCUnit1ACurrent BETWEEN b.Band20 +1 AND b.Band40
				    OR
				    sd.DCUnit1bCurrent BETWEEN b.Band20 +1 AND b.Band40
				    OR
				    sd.DCUnit2ACurrent BETWEEN b.Band20 +1 AND b.Band40
				    OR
				    sd.DCUnit2bCurrent BETWEEN b.Band20 +1 AND b.Band40
				)
			 )
    UNION ALL
    SELECT  
	   DeviceSerialNumber,
	   CreatedDateUTC,
	   'Band40' AS CurrentBand
    FROM 
	   SessionDataCurrent sd
    WHERE
	   EXISTS (SELECT 1 
			 FROM BandsByDevice b 
			 WHERE b.DeviceSerialNumber = sd.DeviceSerialNumber 
			 AND (
				    sd.DCUnit1ACurrent BETWEEN b.Band40 +1 AND b.Band60
				    OR
				    sd.DCUnit1bCurrent BETWEEN b.Band40 +1 AND b.Band60
				    OR
				    sd.DCUnit2ACurrent BETWEEN b.Band40 +1 AND b.Band60
				    OR
				    sd.DCUnit2bCurrent BETWEEN b.Band40 +1 AND b.Band60
				)
			 )
    UNION ALL
    SELECT  
	   DeviceSerialNumber,
	   CreatedDateUTC,
	   'Band60' AS CurrentBand
    FROM 
	   SessionDataCurrent sd
    WHERE
	   EXISTS (SELECT 1 
			 FROM BandsByDevice b 
			 WHERE b.DeviceSerialNumber = sd.DeviceSerialNumber 
			 AND (
				    sd.DCUnit1ACurrent BETWEEN b.Band60 +1 AND b.Band80
				    OR
				    sd.DCUnit1bCurrent BETWEEN b.Band60 +1 AND b.Band80
				    OR
				    sd.DCUnit2ACurrent BETWEEN b.Band60 +1 AND b.Band80
				    OR
				    sd.DCUnit2bCurrent BETWEEN b.Band60 +1 AND b.Band80
				)
			 )
    UNION ALL
    SELECT  
	   DeviceSerialNumber,
	   CreatedDateUTC,
	   'Band80' AS CurrentBand
    FROM 
	   SessionDataCurrent sd
    WHERE
	   EXISTS (SELECT 1 
			 FROM BandsByDevice b 
			 WHERE b.DeviceSerialNumber = sd.DeviceSerialNumber 
			 AND (
				    sd.DCUnit1ACurrent BETWEEN b.Band80 +1 AND b.MaxCurrent
				    OR
				    sd.DCUnit1bCurrent BETWEEN b.Band80 +1 AND b.MaxCurrent
				    OR
				    sd.DCUnit2ACurrent BETWEEN b.Band80 +1 AND b.MaxCurrent
				    OR
				    sd.DCUnit2bCurrent BETWEEN b.Band80 +1 AND b.MaxCurrent
				)
			 )
	)AS outerUnion
)
, DateSequence( UBound ) as
(
    Select GETDATE() as UBound
        union all
    Select DATEADD(MINUTE,-30,UBound) 
        from DateSequence
        where Ubound > DATEADD(d,-14,GETDATE())
)
, HalfHourSlices as
(
    SELECT ROW_NUMBER() OVER(ORDER BY UBound DESC) AS Recency, DATEADD(MINUTE,-30,UBound) AS LBound, UBound From DateSequence 
)



SELECT
    *
INTO UtilizationByAmpDraw1
FROM
	PacketBandsByDevice
	INNER JOIN
	HalfHourSlices ON PacketBandsByDevice.CreatedDateLocal BETWEEN HalfHourSlices.Lbound AND HalfHourSlices.UBound
ORDER BY
     PacketBandsByDevice.DeviceSerialNumber DESC,
	    HalfHourSlices.Recency ASC
OPTION (MAXRECURSION 1400)
GO
