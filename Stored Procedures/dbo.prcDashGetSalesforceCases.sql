SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[prcDashGetSalesforceCases]
    @SiteId VARCHAR(20) = 693
AS
    BEGIN
       -- SET @SiteId = 1365;
        BEGIN
             --Select top 3000 isnull(astat.Description,'New, No Response') as "Case Status",am.DeviceSerialNumber,am.BatterySerialNumber,am.Description,am.CreatedDateUTC as "Case Opened Date", ('https://na32.salesforce.com/' + am.SFCID) as "Case Link",vsd.BatteryErrorCode,vsd.VoltageCell1,vsd.VoltageCell2,vsd.VoltageCell3  from AlertMessage am left join AlertStatus astat on am.AlertStatus = astat.ROW_ID  join viewAllSessionData vsd on am.BatterySerialNumber = vsd.BatterySerialNumber and am.DeviceSerialNumber = vsd.DeviceSerialNumber  and SFCID is not null and vsd.CreatedDateUTC  between dateadd(mm,-50,am.createddateutc) and dateadd(mm,50,am.createddateutc) and am.Priority = 911 and SiteID = @SiteID order by am.CreatedDateUTC desc
            SELECT TOP 10
             --                    ISNULL(astat.Description, 'New, No Response') AS "CaseStatus" ,
                    ISNULL(cs.SFCaseStatus, 'New, No Response') AS "CaseStatus" ,
                    --CASE WHEN ac.AppliesTo = 1 THEN 'WKSTN'
                    --     WHEN ac.AppliesTo = 2 THEN 'BATT'
                    --     ELSE 'CHRG'
                    --END AS AssetDesc ,
                    --AM.DeviceSerialNumber ,
                    --AM.BatterySerialNumber ,
                    cs.SFCaseOwnerId ,
                    cs.SFCaseStatus ,
			
					SUBSTRING(cs.Subject,0,55) AS Subject,
                    cs.idsite AS SiteId ,
                    U.FirstName ,
                    U.LastName ,
                    U.FirstName + ' ' + U.LastName AS UserName ,
                   IIF(LEN(u.title)>14,'TS Specialist',	SUBSTRING( U.Title,0,15)) AS Title ,
                    U.ProfileImage ,
                 --   AM.CaseNum ,
                    cs.SFCaseNumber AS SFCaseNum ,
              --      cs.Description ,
                  ISNULL( ISNULL(ISNULL(CONVERT(VARCHAR(24), LastModifiedDate, 100), CONVERT(VARCHAR(24), cs.open_date, 100)),cs.createdate),GETUTCDATE()) AS open_date ,
                  --  AM.CreatedDateUTC AS "CaseOpenedDate" ,
					
                --    ( 'https://na32.salesforce.com/' + AM.SFCID ) AS "CaseLink" ,
                    --CASE WHEN AM.Priority = 211 THEN 'Info'
                    --     WHEN AM.Priority = 911 THEN 'Alert'
                    --     ELSE 'Notification'
                    --END AS PacketType ,
                    cs.Row_ID
            FROM    dbo.CasesMaster  AS cs
               --     LEFT JOIN dbo.AlertMessage AS AM ON cs.SFCaseID  = AM.SFCID
                 --   LEFT JOIN dbo.AlertStatus astat ON AM.AlertStatus = astat.ROW_ID
                 --   LEFT JOIN dbo.Assets ass ON ass.SerialNo = AM.DeviceSerialNumber
                 --   LEFT JOIN dbo.Assets ass2 ON ass.SerialNo = AM.BatterySerialNumber
                --    LEFT JOIN dbo.AlertConditions ac ON AM.AlertConditionID = ac.ROW_ID
                    LEFT JOIN dbo.[User] AS U ON cs.SFCaseOwnerId = U.SFUserId
                 --   LEFT JOIN dbo.Assets assbatt ON AM.BatterySerialNumber = assbatt.SerialNo
            WHERE   cs.IDSite = @SiteId
                   ----   AND am.SFCID IS NOT NULL
                   and  cs.SroNumber IS NOT NULL
                    AND cs.SFCaseNumber IS NOT NULL
					 AND u.FirstName IS NOT null
                    AND cs.SFCaseStatus IS NOT NULL
                  -- AND am.Priority = 911
                 --  AND am.CreatedDateUTC > DATEADD(DAY, -90, GETUTCDATE())
            ORDER BY cs.LastModifiedDate DESC;
        END;
    END;

GO
