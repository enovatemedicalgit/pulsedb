SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================

-- Modified 6/28/2013 
-- added specific order by clause to accomodate
-- for telerik formatting on edits. 

-- Modified added LastPostDate
-- =============================================
CREATE Procedure [dbo].[prcGetWorkstation] 
	--@UserID int,
    @SiteID INT = NULL ,
    @CustomerID INT = NULL
AS
    BEGIN
	
        SET NOCOUNT ON;
	--Variables for testing
 --   declare @UserID int = 813
	--declare @UserIsStinger varchar(5) = 'True'
        IF ( @CustomerID IS  NULL )
            BEGIN
                SELECT 
		--d.IDAsset, 
		--d.Retired, 
                        parentbu.CustomerName AS IDN ,
                        bu.SiteDescription AS SiteName ,
                        d.SerialNo , 
							
 
		--d.PartNumber, 
                        d.AssetNumber , 
		--d.Model, 
                        dt.Description AS DeviceType ,
                        pn.Description , 
		--d.Description,
		--ds.[Description] AS DeviceStatus,
		
				
		--d.DepartmentID, 
		 CASE WHEN d.ManualAllocation <> 0 THEN 'Y'
                             ELSE 'N'
                        END AS ManuallyAssigned ,
                        ISNULL(dept.Description, 'Unallocated') AS DepartmentName ,
                        ISNULL(d.Floor, 'Unallocated') AS AssignedFloor ,
                        ISNULL(d.Wing, 'Unallocated') AS AssignedWing ,
                        ISNULL(d.Other, ' ') AS Other ,
                        CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, d.LastPostDateUTC),
                                                       DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS LastPostDateUTC , 	  
	--	d.IsActive, 
	--d.Retired,
                        CASE WHEN d.Retired <> 0 THEN 'Y'
                             ELSE 'N'
                        END AS NoComm30Days ,
                        d.Notes ,
                        d.APMAC AS 'APMac' ,
		--d.Notes, 
	--	d.IDAssetType, 
	 
	--	d.CreatedDateUTC, 
		
	--	COALESCE(d.LastPostDateUTC, '2011-1-11 15:00:00.000') as LastPostDateUTC,
		--CASE 
		--	WHEN COALESCE(d.ModifiedUserID, 0) = 0 
		--		THEN '' 
		--		ELSE COALESCE(u.FirstName, '') + ' ' + COALESCE(u.LastName, '') 
		--	END AS ModifiedUserFullName, 
		--CASE 
		--	WHEN COALESCE(d.CreatedUserID, 0) = 0 
		--		THEN '' 
		--		ELSE  COALESCE(u2.FirstName, '') + ' ' + COALESCE(u2.LastName, '') 
		--	END AS CreatedUserFullName, 
		
		--ap.MACAddress + CASE 
		--					WHEN COALESCE(ap.[Description], '') NOT IN ('', ap.MACAddress) 
		--						THEN ' (' + ap.[Description] + ')' 
		--						ELSE '' 
		--					END + ' at ' + apbu.SiteDescription AS AccessPointDetail, 
		--ap.BusinessUnitID AS AccessPointBusinessUnitID, 
		--CASE 
		--	WHEN COALESCE(ap.BusinessUnitID, 0) <> COALESCE(d.BusinessUnitID, 0) 
		--		THEN 'Y' 
		--		ELSE 'N' 
		--	END AS LocationFlag, 
                        ap.Floor AS AccessPointFloor ,
                        ap.Wing AS AccessPointWing ,
                        aio.ProductSerialNumber AS AIOSerialNumber ,
                        dcmONitor.ProductSerialNumber AS DCMonitorSerialNumber ,
                        cart.ProductSerialNumber AS CartSerialNumber ,
                        d.InvoiceNumber ,
			--	d.SiteID,

	--	d.ControlBoardRevision, 
	---	d.LCDRevision, 
	--	d.HolsterChargerRevision, 
	--	d.DCBoardOneRevision, 
	--	d.DCBoardTwoRevision, 
                        d.WiFiFirmwareRevision , 
	--	d.MedBoardRevision, 
                        d.IP ,
                        d.SourceIPAddress ,
                        d.DeviceMAC,
				    d.IDAsset --AS DT_RowId
	 --,		
		--Coalesce(Convert(varchar,PM.CreatedDateUTC,101), 'N/A') as PM,
		--Coalesce(PM.Result, 'N/A') as PM_Result,
		--Coalesce(Convert(varchar,RE.CreatedDateUTC,101), 'N/A') as Repair,
		--Coalesce(RE.Result, 'N/A') as Repair_Result,
		--Coalesce(Convert(varchar,CE.CreatedDateUTC,101), 'N/A') as Certification,
		--Coalesce(CE.Result, 'N/A') as Certification_Result
                FROM    dbo.Assets d
                        LEFT JOIN dbo.PartNumber pn ON LEFT(d.SerialNo, 7) = pn.PartNumber 
		--LEFT JOIN 
		--	AssetStatus ds ON d.AssetStatusID = ds.IDAssetStatus 
                        LEFT JOIN dbo.AccessPoint ap ON d.AccessPointId = ap.ROW_ID
                        LEFT JOIN dbo.Sites apbu ON d.SiteID = apbu.IDSite 
		--LEFT JOIN 
		--	[User] u ON d.ModifiedUserID = u.ROW_ID 
		--LEFT JOIN 
		--	[User] u2 ON d.CreatedUserID = u2.ROW_ID 	
		--LEFT JOIN 
		--	QA PM ON PM.DeviceSerialNumber = d.SerialNumber AND PM.Latest = 1 AND PM.QAStationID = 8
		--LEFT JOIN 
		--	QA RE On RE.DeviceSerialNumber = d.SerialNumber AND RE.Latest = 1 AND RE.QAStationID = 9	
		--LEFT JOIN
		--	QA CE On CE.DeviceSerialNumber = d.SerialNumber AND CE.Latest = 1 and CE.QAStationID = 10
                        JOIN dbo.AssetType dt ON d.IDAssetType = dt.IDAssetType
                        LEFT JOIN dbo.Sites bu ON d.SiteID = bu.IDSite
                        LEFT JOIN dbo.Customers parentbu ON bu.CustomerID = parentbu.IDCustomer
                        LEFT JOIN dbo.Departments dept ON d.DepartmentID = dept.IDDepartment
                        LEFT JOIN dbo.vwProductAIO aio ON d.SerialNo = aio.DeviceSerialNo
                        LEFT JOIN dbo.vwProductDCMonitor dcmONitor ON d.SerialNo = dcmONitor.DeviceSerialNo
                        LEFT JOIN dbo.vwProductCart cart ON d.SerialNo = cart.DeviceSerialNo
                WHERE   d.IDAssetType IN ( 1, 3, 6, 8 )
                        AND ( bu.IDSite = @SiteID ) 
				--	OR bu.CustomerID IN (SELECT CustomerID FROM Sites WHERE IDSite = @SiteID) 
					--	OR bu.IDSite IN (SELECT distinct IDSite FROM Sites WHERE @UserIsStinger = 'True'))
                ORDER BY d.IDAsset DESC; 
            END;
        ELSE
            BEGIN
                SELECT 
		--d.IDAsset, 
		--d.Retired, 
                        parentbu.CustomerName AS IDN ,
                        bu.SiteDescription AS SiteName ,
                        d.SerialNo , 
 
		--d.PartNumber, 
                        d.AssetNumber , 
		--d.Model, 
                        dt.Description AS DeviceType ,
                        pn.Description , 
		--d.Description,
		--ds.[Description] AS DeviceStatus,
		
				
		--d.DepartmentID, 
                        ISNULL(dept.Description, 'Unallocated') AS DepartmentName ,
                        ISNULL(d.Floor, 'Unallocated') AS AssignedFloor ,
                        ISNULL(d.Wing, 'Unallocated') AS AssignedWing ,
                        ISNULL(d.Other, ' ') AS Other ,
                        d.LastPostDateUTC ,  
	--	d.IsActive, 
	--d.Retired,
                        CASE WHEN d.Retired <> 0 THEN 'Y'
                             ELSE 'N'
                        END AS IsRetired ,
                        d.Notes , 
		--d.Notes, 
	--	d.IDAssetType, 
	 
	--	d.CreatedDateUTC, 
		
	--	COALESCE(d.LastPostDateUTC, '2011-1-11 15:00:00.000') as LastPostDateUTC,
		--CASE 
		--	WHEN COALESCE(d.ModifiedUserID, 0) = 0 
		--		THEN '' 
		--		ELSE COALESCE(u.FirstName, '') + ' ' + COALESCE(u.LastName, '') 
		--	END AS ModifiedUserFullName, 
		--CASE 
		--	WHEN COALESCE(d.CreatedUserID, 0) = 0 
		--		THEN '' 
		--		ELSE  COALESCE(u2.FirstName, '') + ' ' + COALESCE(u2.LastName, '') 
		--	END AS CreatedUserFullName, 
		
		--ap.MACAddress + CASE 
		--					WHEN COALESCE(ap.[Description], '') NOT IN ('', ap.MACAddress) 
		--						THEN ' (' + ap.[Description] + ')' 
		--						ELSE '' 
		--					END + ' at ' + apbu.SiteDescription AS AccessPointDetail, 
		--ap.BusinessUnitID AS AccessPointBusinessUnitID, 
		--CASE 
		--	WHEN COALESCE(ap.BusinessUnitID, 0) <> COALESCE(d.BusinessUnitID, 0) 
		--		THEN 'Y' 
		--		ELSE 'N' 
		--	END AS LocationFlag, 
                        ap.Floor AS AccessPointFloor ,
                        ap.Wing AS AccessPointWing ,
                        aio.ProductSerialNumber AS AIOSerialNumber ,
                        dcmONitor.ProductSerialNumber AS DCMonitorSerialNumber ,
                        cart.ProductSerialNumber AS CartSerialNumber ,
                        d.InvoiceNumber ,
			--	d.SiteID,

	--	d.ControlBoardRevision, 
	---	d.LCDRevision, 
	--	d.HolsterChargerRevision, 
	--	d.DCBoardOneRevision, 
	--	d.DCBoardTwoRevision, 
                        d.WiFiFirmwareRevision , 
	--	d.MedBoardRevision, 
                        d.IP ,
                        d.SourceIPAddress ,
                        d.DeviceMAC,
				    d.SerialNo AS DT_RowId
	 --,		
		--Coalesce(Convert(varchar,PM.CreatedDateUTC,101), 'N/A') as PM,
		--Coalesce(PM.Result, 'N/A') as PM_Result,
		--Coalesce(Convert(varchar,RE.CreatedDateUTC,101), 'N/A') as Repair,
		--Coalesce(RE.Result, 'N/A') as Repair_Result,
		--Coalesce(Convert(varchar,CE.CreatedDateUTC,101), 'N/A') as Certification,
		--Coalesce(CE.Result, 'N/A') as Certification_Result
                FROM    dbo.Assets d
                        LEFT JOIN dbo.PartNumber pn ON LEFT(d.SerialNo, 7) = pn.PartNumber 
		--LEFT JOIN 
		--	AssetStatus ds ON d.AssetStatusID = ds.IDAssetStatus 
                        LEFT JOIN dbo.AccessPoint ap ON d.AccessPointId = ap.ROW_ID
                        LEFT JOIN dbo.Sites apbu ON d.SiteID = apbu.IDSite 
		--LEFT JOIN 
		--	[User] u ON d.ModifiedUserID = u.ROW_ID 
		--LEFT JOIN 
		--	[User] u2 ON d.CreatedUserID = u2.ROW_ID 	
		--LEFT JOIN 
		--	QA PM ON PM.DeviceSerialNumber = d.SerialNumber AND PM.Latest = 1 AND PM.QAStationID = 8
		--LEFT JOIN 
		--	QA RE On RE.DeviceSerialNumber = d.SerialNumber AND RE.Latest = 1 AND RE.QAStationID = 9	
		--LEFT JOIN
		--	QA CE On CE.DeviceSerialNumber = d.SerialNumber AND CE.Latest = 1 and CE.QAStationID = 10
                        JOIN dbo.AssetType dt ON d.IDAssetType = dt.IDAssetType
                        LEFT JOIN dbo.Sites bu ON d.SiteID = bu.IDSite
                        LEFT JOIN dbo.Customers parentbu ON bu.CustomerID = parentbu.IDCustomer
                        LEFT JOIN dbo.Departments dept ON d.DepartmentID = dept.IDDepartment
                        LEFT JOIN dbo.vwProductAIO aio ON d.SerialNo = aio.DeviceSerialNo
                        LEFT JOIN dbo.vwProductDCMonitor dcmONitor ON d.SerialNo = dcmONitor.DeviceSerialNo
                        LEFT JOIN dbo.vwProductCart cart ON d.SerialNo = cart.DeviceSerialNo
                WHERE   d.IDAssetType IN ( 1, 3, 6, 8 )
                        AND ( bu.IDSite IN ( SELECT IDSite
                                             FROM   dbo.Sites
                                             WHERE  CustomerID = @CustomerID ) 
				--	OR bu.CustomerID IN (SELECT CustomerID FROM Sites WHERE IDSite = @SiteID) 
					--	OR bu.IDSite IN (SELECT distinct IDSite FROM Sites WHERE @UserIsStinger = 'True'))
                                             )
                ORDER BY d.IDAsset DESC; 
            END;
    END;



GO
