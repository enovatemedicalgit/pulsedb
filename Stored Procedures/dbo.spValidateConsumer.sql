SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spValidateConsumer]
	-- Add the parameters for the stored procedure here
    @Consumer AS VARCHAR(50) ,
    @ConsumerKey AS VARCHAR(250)
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        SELECT  *
        FROM    dbo.APIKeys
        WHERE   Consumer = @Consumer
                AND ConsumerKey = @ConsumerKey;
    END;

GO
