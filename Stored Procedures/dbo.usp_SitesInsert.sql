SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC  [dbo].[usp_SitesInsert]
    @SiteName VARCHAR(150) = NULL ,
    @SiteDescription VARCHAR(200) = NULL ,
    @CustomerID VARCHAR(5) ,
    @SfCustomerAcctID VARCHAR(20) = NULL ,
    @SfAccountId VARCHAR(20) = NULL ,
    @SfAcctOwnerId VARCHAR(20) = NULL ,
    @IPAddr VARCHAR(50) = NULL ,
    @IsInternal BIT = NULL ,
    @SiteType INT = NULL ,
    @Status VARCHAR(2) = NULL ,
    @StatusReason VARCHAR(4) = NULL ,
    @RegionID INT = NULL
AS
    SET NOCOUNT ON; 
 
	SET IDENTITY_INSERT dbo.Sites OFF
    --INSERT  INTO dbo.Sites
    --        ( SiteName ,
    --          SiteDescription ,
    --          CustomerID
    --        )
    --VALUES  ( @SiteName ,
    --          @SiteDescription ,
    --          CAST(@CustomerID AS INT)
    --        );
    INSERT  INTO dbo.Sites
            ( [SiteName] ,
              [SiteDescription] ,
              [CustomerID] ,
              [SfCustomerAcctID] ,
              [SfAccountId] ,
              [SfAcctOwnerId] ,
              [IPAddr] ,
              [IsInternal] ,
              [SiteType] ,
              [Status] ,
              [StatusReason] ,
              [RegionID]
            )
            SELECT  @SiteName ,
                    @SiteDescription ,
                    CAST(@CustomerID AS INT) ,
                    @SfCustomerAcctID ,
                    @SfAccountId ,
                    @SfAcctOwnerId ,
                    @IPAddr ,
                    @IsInternal ,
                    @SiteType ,
                    @Status ,
                    @StatusReason ,
                    ISNULL(( SELECT TOP 1 RegionID
                             FROM   dbo.Sites
                             WHERE  SfAcctOwnerId = @SfAcctOwnerId
                           ), 0) AS RegionID;
						   SET IDENTITY_INSERT dbo.Sites ON;

GO
