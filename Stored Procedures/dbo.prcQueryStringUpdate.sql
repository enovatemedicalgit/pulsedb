SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcQueryStringUpdate]
    @ROW_ID INT ,
    @CreatedDateUTC DATETIME = NULL ,
    @DeviceSerialNumber VARCHAR(50) = NULL ,
    @NeedsCaughtUp BIT = NULL ,
    @NineParsed BIT = NULL ,
    @Query VARCHAR(2000) = NULL ,
    @QueryStringBatchID INT ,
    @SourceIPAddress VARCHAR(20) = NULL ,
    @SourceTimestampUTC DATETIME = NULL
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
    BEGIN TRAN;

    UPDATE  dbo.QueryString
    SET     [CreatedDateUTC] = @CreatedDateUTC ,
            [DeviceSerialNumber] = @DeviceSerialNumber ,
            [NeedsCaughtUp] = @NeedsCaughtUp ,
            [NineParsed] = @NineParsed ,
            [Query] = @Query ,
            [QueryStringBatchID] = @QueryStringBatchID ,
            [SourceIPAddress] = @SourceIPAddress ,
            [SourceTimestampUTC] = @SourceTimestampUTC
    WHERE   [ROW_ID] = @ROW_ID;
	
	-- Begin Return Select <- do not remove
    SELECT  [ROW_ID] ,
            [CreatedDateUTC] ,
            [DeviceSerialNumber] ,
            [NeedsCaughtUp] ,
            [NineParsed] ,
            [Query] ,
            [QueryStringBatchID] ,
            [SourceIPAddress] ,
            [SourceTimestampUTC]
    FROM    dbo.QueryString
    WHERE   [ROW_ID] = @ROW_ID;	
	-- End Return Select <- do not remove

    COMMIT;



GO
