SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[spBatteriesPastMCCLimit]
AS
    BEGIN	
        SELECT  s.SiteDescription ,
                ass.SerialNo ,
                'Past Warranty Cycle Count' AS 'Opportunity Type' ,
                wd.DurationYears AS 'Warranty Years' ,
                ass.CycleCount ,
                ass.MaxCycleCount ,
                ISNULL(NULLIF(dbo.fnGetBatteryCapacity_Int(ass.SerialNo,
                                                              ass.FullChargeCapacity),
                              0), 0) AS 'Capacity Health' ,
                ass.FullChargeCapacity ,
                ass.LastPostDateUTC
        FROM    dbo.Assets ass
                LEFT JOIN dbo.Sites s ON s.IDSite = ass.SiteID
                LEFT JOIN dbo.WarrantyDuration wd ON wd.SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
        WHERE   ass.IDAssetType = 5
                AND ass.CycleCount > ass.MaxCycleCount
                AND ass.MaxCycleCount != 0
                AND ass.MaxCycleCount IS NOT NULL
                AND ass.MaxCycleCount <> 0
                AND ass.MaxCycleCount <> 10
                AND ass.CycleCount <> 0
                AND ass.CycleCount IS NOT NULL
                AND s.IDSite NOT IN ( 1125, 28, 4 )
        ORDER BY s.SiteName ,
                ass.MaxCycleCount DESC;	

    END;



GO
