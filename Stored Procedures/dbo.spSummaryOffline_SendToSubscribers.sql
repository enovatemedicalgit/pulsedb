SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Arlow Farrell
-- Create date: 2011-12-20
-- Description:	Sends offline summaries
--              to all subscribers
--
-- Modified:    2012-10-15 by Arlow Farrell
--              Changed body backgroud from #000
--              (black) to #505050 (gray) so
--              text can be read when a user
--              forwards an email containing
--              this content (and types a new
--              message onto the background).
--
--              Also changed hyperlink color
--              from Yellow to #00BE00 (bright 
--              green)

-- Modified 7/25/2013 DC
-- Rebranding changed names, colors, hyperlinks etc...
-- okay, changed the structure for readability a bit. 

-- Modified 8/1/2016 Needs Updated 
-- Need to fix Customer table left joines where they used to be one the same tables as sites, need to replace CAST references with Pulse
-- =============================================
CREATE PROCEDURE [dbo].[spSummaryOffline_SendToSubscribers]
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Date VARCHAR(10);
        SET @Date = ( SELECT    MAX([Date])
                      FROM      dbo.SummaryWeekly
                    );

        DECLARE @SubscriptionKey VARCHAR(10);
        SET @SubscriptionKey = 'OFF';

	-- get email from (address and name), required for inserting into EmailQueue table
        DECLARE @FromAddress VARCHAR(150);
        DECLARE @FromName VARCHAR(150);
	
        SELECT TOP 1
                @FromAddress = a.DefaultFromAddress ,
                @FromName = b.DefaultFromName
        FROM    ( SELECT TOP 1
                            1 AS joinkey ,
                            SettingValue AS DefaultFromAddress
                  FROM      dbo.SystemSettings
                  WHERE     SettingKey = 'EMAIL_NOTIFICATIONS_DEFAULT_FROM_ADDRESS'
                ) a
                JOIN ( SELECT TOP 1
                                1 AS joinkey ,
                                SettingValue AS DefaultFromName
                       FROM     dbo.SystemSettings
                       WHERE    SettingKey = 'EMAIL_NOTIFICATIONS_DEFAULT_FROM_NAME'
                     ) b ON a.joinkey = b.joinkey;
	

	-- only run if/when subscription has not already been sent to recipients
        IF ( SELECT COUNT(*)
             FROM   dbo.Subscription
             WHERE  SubscriptionKey = @SubscriptionKey
                    AND ( LastSentForDate IS NULL
                          OR LastSentForDate < @Date
                        )
           ) > 0
            BEGIN

		-- from SystemSettings …
                DECLARE @Pulse_URL VARCHAR(500);
                DECLARE @CAST_LOGO VARCHAR(500);

                SET @Pulse_URL = ( SELECT TOP 1
                                            SettingValue
                                   FROM     dbo.SystemSettings
                                   WHERE    SettingKey = 'Pulse_URL'
                                 );
                SET @CAST_LOGO = ( SELECT TOP 1
                                            SettingValue
                                   FROM     dbo.SystemSettings
                                   WHERE    SettingKey = 'CAST_LOGO'
                                 );

                DECLARE @disclaimer VARCHAR(MAX);
                SET @disclaimer = ( SELECT TOP 1
                                            SettingValue
                                    FROM    dbo.SystemSettings
                                    WHERE   SettingKey = 'EMAIL_DISCLAIMER'
                                  );


		--------------------     Content header and footer : TOP     --------------------------
                DECLARE @ContentHeaderHTML VARCHAR(MAX);
                DECLARE @ContentFooterHTML VARCHAR(MAX);
                SELECT TOP 1
                        @ContentFooterHTML = ContentFooterHTML ,
                        @ContentHeaderHTML = ContentHeaderHTML
                FROM    dbo.Subscription
                WHERE   SubscriptionKey = @SubscriptionKey;
                IF ( @ContentHeaderHTML IS NOT NULL )
                    BEGIN
                        SET @ContentHeaderHTML = '<div class="ContentHeader">'
                            + @ContentHeaderHTML + '</div>';
                    END;
                ELSE
                    BEGIN
                        SET @ContentHeaderHTML = '';
                    END;
                IF ( @ContentFooterHTML IS NOT NULL )
                    BEGIN
                        SET @ContentFooterHTML = '<div class="ContentFooter">'
                            + @ContentFooterHTML + '</div>';
                    END;
                ELSE
                    BEGIN
                        SET @ContentFooterHTML = '';
                    END;
		--------------------     Content header and footer : END     --------------------------
		
		

                DECLARE @BUType INT;
                DECLARE @SiteID INT;
		
                DECLARE @body_message VARCHAR(MAX);
                DECLARE @body_header VARCHAR(MAX);
                DECLARE @body_workstations VARCHAR(MAX);
                DECLARE @body_chargers VARCHAR(MAX);
		
                DECLARE @RecipientsXML VARCHAR(MAX);
                DECLARE @Recipients VARCHAR(MAX);
                DECLARE @Title VARCHAR(200);


                SET @Title = ( SELECT   Title
                               FROM     dbo.Subscription
                               WHERE    SubscriptionKey = @SubscriptionKey
                             );


		/*	----- create temp table with details from last packet received for each device : TOP  -----	*/
                IF OBJECT_ID(N'tempdb..#LastKnownDetail', N'U') IS NOT NULL
                    BEGIN
                        DROP TABLE #LastKnownDetail;
                    END;

                SELECT  d.SerialNo ,
                        d.LastPostDateUTC ,
                        ap.MACAddress ,
                        CASE WHEN sd.ROW_ID IS NOT NULL THEN sd.LinkQuality
                             WHEN nsd.ROW_ID IS NOT NULL THEN nsd.LinkQuality
                             ELSE NULL
                        END AS LastLinkQuality
                INTO    #LastKnownDetail
                FROM    dbo.Assets d
                        LEFT JOIN dbo.AccessPoint ap ON d.IDAsset = ap.ROW_ID
                        LEFT JOIN dbo.SessionDataCurrent sd ON d.SerialNo = sd.DeviceSerialNumber
                                                              AND d.LastPostDateUTC = sd.CreatedDateUTC
                        LEFT JOIN dbo.NonSessionDataCurrent nsd ON d.SerialNo = nsd.DeviceSerialNumber
                                                              AND d.LastPostDateUTC = nsd.CreatedDateUTC
                WHERE   d.AssetStatusID = 0
                        AND d.Retired = 0
                        AND d.IDAssetType <> 5;

                CREATE NONCLUSTERED INDEX IX_LastKnownDetail ON #LastKnownDetail (SerialNumber, LastPostDateUTC, MACAddress, LastLinkQuality);

		/*	----- create temp table with details from last packet received for each device : END  -----	*/





                DECLARE Site_Cursor CURSOR
                FOR
                    SELECT DISTINCT
                            subscribers.SiteType ,
                            subscribers.IDSite
                    FROM    ( SELECT    s.SubscriptionKey ,
                                        s.UserID ,
                                        u.Email ,
                                        u.FirstName ,
                                        u.LastName ,
                                        bu.SiteType ,
                                        u.IDSite ,
                                        bu.CustomerID ,
                                        bu.SiteDescription ,
                                        bu.IsInternal
                              FROM      dbo.Subscriptions s
                                        JOIN dbo.[User] u ON s.UserID = u.IDUser
								JOIN dbo.UserSites su ON u.IDUser = su.User_Row_Id 
                                        JOIN dbo.Sites bu ON u.IDSite = bu.IDSite
                              WHERE     s.SubscriptionKey = @SubscriptionKey
                                        AND s.UserID IN (
                                        SELECT  u.IDUser
                                        FROM    dbo.UserOptions
                                        WHERE   OptionKey = 'CAN_LOGIN_TO_CAST'
                                                AND OptionValue = 1 )
                            ) subscribers FOR READ ONLY;

                OPEN Site_Cursor;
                FETCH Site_Cursor INTO @BUType, @SiteID;
                WHILE ( @@FETCH_STATUS = 0 )
                    BEGIN
                        SET @RecipientsXML = ( SELECT   CAST(( SELECT
                                                              u.Email
                                                              FROM
                                                              dbo.[User] u
                                                              JOIN dbo.Subscriptions s ON u.IDUser = s.UserID
                                                              WHERE
                                                              u.IDSite = @SiteID
                                                              AND s.SubscriptionKey = @SubscriptionKey
                                                              AND s.UserID IN (
                                                              SELECT
                                                              User_ROW_ID
                                                              FROM
                                                              dbo.UserOptions
                                                              WHERE
                                                              OptionKey = 'CAN_LOGIN_TO_CAST'
                                                              AND OptionValue = 1 )
                                                             FOR
                                                              XML
                                                              PATH('Recipients') ,
                                                              TYPE
                                                             ) AS NVARCHAR(MAX))
                                             );

                        SET @Recipients = ( SELECT  REPLACE(REPLACE(REPLACE(REPLACE(@RecipientsXML,
                                                              '<Recipients>',
                                                              ''),
                                                              '</Recipients>',
                                                              ''), '<Email>',
                                                              ''), '</Email>',
                                                            '; ')
                                          );


                        SET @body_header = N'<style>body {padding: 20px; font-family: segoe ui, Arial, Helvetica, sans-serif;} h4 {margin-bottom: 2px;}'
                            + N'.CASTLogo {text-align: left; display: block;} '
                            + N'.unsubscribe {padding-top: 35px; padding-bottom: 25px; font-size: 9pt;} '
                            + N'.padright {padding-right: 15px;} .Facility {background-color: #e3edf8;} '
                            + N'h5 {margin-top: 20px; margin-bottom: 2px;} '
                            + N'.ContentFooter, .ContentHeader {padding-top: 30px; font-size: smaller; max-width: 800px; min-width: 600px; width: 80%;} blockquote {font-size: smaller; max-width: 800px; min-width: 600px; width: 80%;} .fyi {padding-top: 10px;} .fyisubject {font-weight: bold;} '
                            + N'.nonemsg {margin-top: 5px; margin-bottom: 20px; font-style: italic; font-size: 9pt;} '
                            + N'th {background-color:#79bde8; color:#fff;} '
                            + N'table {font-family: segoe ui, Arial, Helvetica, sans-serif; font-size: 9pt; margin-top: 8px; border-color: #7078A0; border-width: 1px; border-style: none; border-spacing: 0px; border-collapse: collapse;}</style>'
                            + N'<p>If you are unable to view this via email, <a href="https://pulse.myenovate.com/mail.aspx?q=REPLACE_ALTERNATEKEY-REPLACE_ROW_ID">click here</a> to see the content in your browser.</p>'
                            + N'<a href="' + @Pulse_URL
                            + N'" alt="Go to CAST online"><img class="CASTLogo" src="'
                            + @CAST_LOGO
                            + N'" alt="Enovate Medical" border="0"></a>'
                            + N'<h4>' + @Title + N'</h4>' + N'<small>'
                            + CONVERT(VARCHAR(10), CAST(@Date AS DATETIME), 101)
                            + N'</small>' + N' ' + @ContentHeaderHTML
                            + N'<p>As of '
                            + CONVERT(VARCHAR(20), dbo.SiteDateTime(GETUTCDATE(),
                                                              @SiteID), 100)
                            + N', the following workstations and chargers are offline:</p>';
                        IF ( SELECT COUNT(*)
                             FROM   dbo.SummaryWorkstation sw
                                    JOIN Site bu ON sw.SiteID = bu.IDSite
                             WHERE  CASE WHEN @BUType = 1
                                              AND bu.CustomerID = @SiteID
                                         THEN 1
                                         WHEN @BUType = 2
                                              AND sw.SiteID = @SiteID THEN 1
                                         ELSE 0
                                    END = 1
                                    AND sw.Status = 'Offline'
                                    AND sw.Date = @Date
                           ) > 0
                            BEGIN
                                SET @body_workstations = N'<h5>Offline Workstations</h5>'
                                    + N'<table border="1" cellspacing="0" cellpadding="2">'
                                    + N'<tbody><tr valign="top">'
                                    + N'<th align="left" valign="bottom">Facility</th>'
                                    + N'<th align="left" valign="bottom">Department</th>'
                                    + N'<th align="left" valign="bottom">Serial #</th>'
                                    + N'<th align="left" valign="bottom">Asset #</th>'
                                    + N'<th align="center" valign="bottom">Floor</th>'
                                    + N'<th align="center" valign="bottom">Wing</th>'
                                    + N'<th align="center" valign="bottom">Other</th>'
                                    + N'<th align="center" valign="bottom">Last communication</th>'
                                    + N'<th align="center" valign="bottom">Link quality at<br />last communication</th>'
                                    + N'<th align="center" valign="bottom">Last communicated<br />from access point<br />(MAC address)</th></tr>'
                                    + CAST(( SELECT 'HQ padright' AS [td/@class] ,
                                                    td = buS.SiteName ,
                                                    '' ,
                                                    'HQ padright' AS [td/@class] ,
                                                    td = COALESCE(bud.[Description],
                                                              'Unallocated') ,
                                                    '' ,
                                                    'HQ padright' AS [td/@class] ,
                                                    td = DeviceSerialNumber ,
                                                    '' ,
                                                    'HQ' AS [td/@class] ,
                                                    td = sw.AssetNumber ,
                                                    '' ,
                                                    'HQ' AS [td/@class] ,
                                                    'center' AS [td/@align] ,
                                                    td = sw.Floor ,
                                                    '' ,
                                                    'HQ' AS [td/@class] ,
                                                    td = sw.Wing ,
                                                    '' ,
                                                    'HQ' AS [td/@class] ,
                                                    td = sw.Other ,
                                                    '' ,
                                                    'HQ' AS [td/@class] ,
                                                    'center' AS [td/@align] ,
                                                    td = LKD.LastPostDateUTC ,
                                                    '' ,
                                                    'HQ' AS [td/@class] ,
                                                    'center' AS [td/@align] ,
                                                    td = LKD.LastLinkQuality ,
                                                    '' ,
                                                    'HQ' AS [td/@class] ,
                                                    'center' AS [td/@align] ,
                                                    td = LKD.MACAddress ,
                                                    ''
                                             FROM   dbo.SummaryWorkstation sw
                                                    JOIN Site bu ON sw.SiteID = bu.IDSite
                                                    LEFT JOIN SiteDepartment bud ON sw.SiteID = bud.SiteID
                                                              AND sw.SiteDepartmentID = bud.ROW_ID
                                                    LEFT JOIN #LastKnownDetail LKD ON sw.DeviceSerialNumber = LKD.SerialNumber
                                             WHERE  CASE WHEN @BUType = 1
                                                              AND bu.ParentSite = @SiteID
                                                         THEN 1
                                                         WHEN @BUType = 2
                                                              AND sw.SiteID = @SiteID
                                                         THEN 1
                                                         ELSE 0
                                                    END = 1
                                                    AND sw.Status = 'Offline'
                                                    AND sw.Date = @Date
                                             ORDER BY bu.SiteDescription ,
                                                    COALESCE(bud.[Description],
                                                             'Unallocated')
                                           FOR
                                             XML PATH('tr') ,
                                                 TYPE
                                           ) AS NVARCHAR(MAX))
                                    + '</tbody></table>';
                            END;
                        ELSE
                            BEGIN
                                SET @body_workstations = N'<h5>Offline Workstations</h5>'
                                    + N'<p class="nonemsg">None of your workstations are offline at this time</p>';
                            END;
										
					
                        IF ( SELECT COUNT(*)
                             FROM   Device d
                                    JOIN Site bu ON d.SiteID = bu.ROW_ID
                             WHERE  DeviceTypeID IN (
                                    SELECT  [ID]
                                    FROM    DeviceType
                                    WHERE   Category = 'Charger' )
                                    AND CASE WHEN @BUType = 1
                                                  AND bu.ParentSite = @SiteID
                                             THEN 1
                                             WHEN @BUType = 2
                                                  AND d.SiteID = @SiteID
                                             THEN 1
                                             ELSE 0
                                        END = 1
                                    AND DeviceStatusID = 0
                           ) > 0
                            BEGIN
                                SET @body_chargers = N'<h5>Offline Chargers</h5>'
                                    + N'<table border="1" cellspacing="0" cellpadding="2">'
                                    + N'<tbody><tr valign="top">'
                                    + N'<th align="left" valign="bottom">Facility</th>'
                                    + N'<th align="left" valign="bottom">Department</th>'
                                    + N'<th align="left" valign="bottom">Serial #</th>'
                                    + N'<th align="left" valign="bottom">Asset #</th>'
                                    + N'<th align="center" valign="bottom">Floor</th>'
                                    + N'<th align="center" valign="bottom">Wing</th>'
                                    + N'<th align="center" valign="bottom">Other</th>'
                                    + N'<th align="center" valign="bottom">Last communication</th>'
                                    + N'<th align="center" valign="bottom">Link quality at<br />last communication</th>'
                                    + N'<th align="center" valign="bottom">Last communicated<br />from access point<br />(MAC address)</th></tr>'
                                    + CAST(( SELECT 'HQ padright' AS [td/@class] ,
                                                    td = bu.SiteDescription ,
                                                    '' ,
                                                    'HQ padright' AS [td/@class] ,
                                                    td = COALESCE(bud.Description,
                                                              'Unallocated') ,
                                                    '' ,
                                                    'HQ padright' AS [td/@class] ,
                                                    td = d.SerialNumber ,
                                                    '' ,
                                                    'HQ' AS [td/@class] ,
                                                    td = AssetNumber ,
                                                    '' ,
                                                    'HQ' AS [td/@class] ,
                                                    'center' AS [td/@align] ,
                                                    td = [Floor] ,
                                                    '' ,
                                                    'HQ' AS [td/@class] ,
                                                    td = Wing ,
                                                    '' ,
                                                    'HQ' AS [td/@class] ,
                                                    td = Other ,
                                                    '' ,
                                                    'HQ' AS [td/@class] ,
                                                    'center' AS [td/@align] ,
                                                    td = CONVERT(VARCHAR(20), dbo.SiteDateTime(LKD.LastPostDateUTC,
                                                              d.SiteID), 120) ,
                                                    '' ,
                                                    'HQ' AS [td/@class] ,
                                                    'center' AS [td/@align] ,
                                                    td = LKD.LastLinkQuality ,
                                                    '' ,
                                                    'HQ' AS [td/@class] ,
                                                    'center' AS [td/@align] ,
                                                    td = LKD.MACAddress ,
                                                    ''
                                             FROM   Device d
                                                    JOIN Site bu ON d.SiteID = bu.ROW_ID
                                                    LEFT JOIN SiteDepartment bud ON d.SiteDepartmentID = bud.ROW_ID
                                                    LEFT JOIN #LastKnownDetail LKD ON d.SerialNumber = LKD.SerialNumber
                                             WHERE  DeviceTypeID IN (
                                                    SELECT  [ID]
                                                    FROM    DeviceType
                                                    WHERE   Category = 'Charger' )
                                                    AND CASE WHEN @BUType = 1
                                                              AND bu.ParentSite = @SiteID
                                                             THEN 1
                                                             WHEN @BUType = 2
                                                              AND d.SiteID = @SiteID
                                                             THEN 1
                                                             ELSE 0
                                                        END = 1
                                                    AND DeviceStatusID = 0 -- offline
                                             ORDER BY bu.SiteDescription ,
                                                    COALESCE(bud.[Description],
                                                             'Unallocated')
                                           FOR
                                             XML PATH('tr') ,
                                                 TYPE
                                           ) AS NVARCHAR(MAX))
                                    + '</tbody></table>';
                            END;
                        ELSE
                            BEGIN
                                SET @body_chargers = N'<h5>Offline Chargers</h5>'
                                    + N'<p class="nonemsg">None of your chargers are offline at this time</p>';
                            END;
										
                        SET @body_message = @body_header + @body_workstations
                            + @body_chargers + @ContentFooterHTML
                            + N'<div class="unsubscribe"><a href="https://pulse.myenovate.com/unsubscribe.aspx?from='
                            + @SubscriptionKey + N'">Unsubscribe</a></div>'
                            + N'<blockquote><small>' + @disclaimer
                            + '</small></blockquote>';


					--insert into EmailQueue ([Subject], ToAddresses, Body)
					--select @Title, @Recipients, @body_message
                        INSERT  INTO dbo.EmailQueue
                                ( [Subject] ,
                                  ToAddresses ,
                                  Body ,
                                  Attempts ,
                                  FromAddress ,
                                  FromName
                                )
                                SELECT  @Title ,
                                        @Recipients ,
                                        @body_message ,
                                        0 ,
                                        @FromAddress ,
                                        @FromName; -- set attempts to 99 so the standard email processing app won't touch these rows

                        DECLARE @ROW_ID INT;
                        DECLARE @AlternateKey VARCHAR(20);
                        SET @ROW_ID = SCOPE_IDENTITY();
                        SET @AlternateKey = ( SELECT    AlternateKey
                                              FROM      dbo.EmailQueue
                                              WHERE     ROW_ID = @ROW_ID
                                            );
                        UPDATE  dbo.EmailQueue
                        SET     Body = REPLACE(REPLACE(Body,
                                                       'REPLACE_ALTERNATEKEY',
                                                       @AlternateKey),
                                               'REPLACE_ROW_ID', @ROW_ID)
                        WHERE   ROW_ID = @ROW_ID;					

                        FETCH Site_Cursor INTO @BUType, @SiteID;
                    END;

                CLOSE Site_Cursor;
                DEALLOCATE Site_Cursor;

                UPDATE  dbo.Subscription
                SET     LastSentDateUTC = GETUTCDATE() ,
                        LastSentForDate = @Date
                WHERE   SubscriptionKey = @SubscriptionKey;


                IF OBJECT_ID(N'tempdb..#LastKnownDetail', N'U') IS NOT NULL
                    BEGIN
                        DROP TABLE #LastKnownDetail;
                    END;

            END;

    END;


/*******************************************************************************************************************************8
********************************************************************************************************************************8
********************************************************************************************************************************/






GO
