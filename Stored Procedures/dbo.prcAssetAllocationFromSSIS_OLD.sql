SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcAssetAllocationFromSSIS_OLD]
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
	





--delete temp table so it's re-runnable in testing.
    IF OBJECT_ID('tempdb..#tempNSDSelect') IS NOT NULL
        BEGIN
            DROP TABLE #tempNSDSelect;
        END;

    IF OBJECT_ID('tempdb..#NewIPs') IS NOT NULL
        BEGIN
            DROP TABLE #NewIPs;
        END;

    CREATE TABLE #tempNSDSelect
        (
          BatterySerialNumber VARCHAR(50) ,
          Bay1ChargerRevision VARCHAR(50) ,
          Bay2ChargerRevision VARCHAR(50) ,
          Bay3ChargerRevision VARCHAR(50) ,
          Bay4ChargerRevision VARCHAR(50) ,
          WifiFirmwareRevision VARCHAR(50) ,
          BayWirelessRevision VARCHAR(50) ,
          DeviceSerialNumber VARCHAR(50) ,
          MaxCycleCount INT ,
          APMac VARCHAR(50) ,
          CreatedDateUTC DATETIME ,
          ChargeLevel INT ,
          CycleCount INT ,
          FullChargeCapacity INT ,
          Temperature INT ,
          SourceIPAddress VARCHAR(20) ,
          IP VARCHAR(50) ,
          Bay VARCHAR(50) ,
          Activity INT ,
          DeviceType INT ,
          MOVE INT ,
          Amps DECIMAL(18, 2)
        );

--find mismatched devices
    INSERT  INTO #tempNSDSelect
            ( BatterySerialNumber ,
              Bay1ChargerRevision ,
              Bay2ChargerRevision ,
              Bay3ChargerRevision ,
              Bay4ChargerRevision ,
              WifiFirmwareRevision ,
              BayWirelessRevision ,
              DeviceSerialNumber ,
              MaxCycleCount ,
              APMac ,
              CreatedDateUTC ,
              ChargeLevel ,
              CycleCount ,
              FullChargeCapacity ,
              Temperature ,
              SourceIPAddress ,
              IP ,
              Bay ,
              Activity ,
              DeviceType ,
              MOVE ,
              Amps
	        )
            SELECT  MostRecent.BatterySerialNumber ,
                    MostRecent.Bay1ChargerRevision ,
                    MostRecent.Bay2ChargerRevision ,
                    MostRecent.Bay3ChargerRevision ,
                    MostRecent.Bay4ChargerRevision ,
                    MostRecent.WifiFirmwareRevision ,
                    MostRecent.BayWirelessRevision ,
                    MostRecent.DeviceSerialNumber ,
                    MostRecent.MaxCycleCount ,
                    MostRecent.APMAC ,
                    MostRecent.CreatedDateUTC ,
                    MostRecent.ChargeLevel ,
                    MostRecent.CycleCount ,
                    MostRecent.FullChargeCapacity ,
                    MostRecent.Temperature ,
                    MostRecent.SourceIPAddress ,
                    MostRecent.IP ,
                    MostRecent.Bay ,
                    MostRecent.Activity ,
                    MostRecent.DeviceType ,
                    MostRecent.MOVE ,
                    MostRecent.Amps
            FROM    ( SELECT    nsd.BatterySerialNumber ,
                                ISNULL(nsd.Bay1ChargerRevision, '') AS Bay1ChargerRevision ,
                                ISNULL(nsd.Bay2ChargerRevision, '') AS Bay2ChargerRevision ,
                                ISNULL(nsd.Bay3ChargerRevision, '') AS Bay3ChargerRevision ,
                                ISNULL(nsd.Bay4ChargerRevision, '') AS Bay4ChargerRevision ,
                                ISNULL(nsd.WifiFirmwareRevision, '') AS WifiFirmwareRevision ,
                                ISNULL(nsd.BayWirelessRevision, '') AS BayWirelessRevision ,
                                nsd.DeviceSerialNumber ,
                                nsd.MaxCycleCount ,
                                nsd.APMAC ,
                                nsd.CreatedDateUTC ,
                                nsd.ChargeLevel ,
                                nsd.CycleCount ,
                                nsd.FullChargeCapacity ,
                                nsd.Temperature ,
                                nsd.SourceIPAddress AS SourceIPAddress ,
                                nsd.IP AS IP ,
                                nsd.Bay AS Bay ,
                                nsd.Activity AS Activity ,
                                nsd.DeviceType AS DeviceType ,
                                nsd.Move AS MOVE ,
                                nsd.Amps AS Amps ,
                                ROW_NUMBER() OVER ( PARTITION BY nsd.DeviceSerialNumber,
                                                    nsd.BatterySerialNumber ORDER BY nsd.CreatedDateUTC DESC ) AS RN
                      FROM      dbo.viewAllSessionData nsd WITH ( NOLOCK )
                      WHERE     nsd.CreatedDateUTC > DATEADD(MINUTE, -60,
                                                             GETUTCDATE())
	--AND [dbo].[SerialNumberClassification](nsd.BatterySerialNumber) = 1
                    ) MostRecent
            WHERE   MostRecent.RN = 1;

/*
WITH CTE
AS (
	SELECT *
		,RN = ROW_NUMBER() OVER (
			PARTITION BY DeviceSerialNumber
			,BatterySerialNumber ORDER BY CreatedDateUTC DESC
			)
	FROM #tempNSDSelect
	)
DELETE
FROM CTE
WHERE RN > 1
*/



    UPDATE  dbo.Assets
    SET     LastPostDateUTC = t.CreatedDateUTC ,
            Temperature = t.Temperature ,
            AssetStatusID = 1 ,
            IsActive = 1 ,
            Activity = ISNULL(t.Activity, a.Activity) ,
            LastPacketType = IIF(t.DeviceType IN ( 1, 6 ), 1, 2) ,
            ChargeLevel = CASE WHEN t.ChargeLevel IS NOT NULL
                                           AND t.ChargeLevel <> 0
                                      THEN t.ChargeLevel
                                      ELSE a.ChargeLevel
                                 END ,
            FullChargeCapacity = CASE WHEN t.FullChargeCapacity IS NOT NULL
                                                  AND t.ChargeLevel > 90
                                                  AND a.IDAssetType = 2
                                                  AND t.FullChargeCapacity != 22000
                                                  AND t.FullChargeCapacity <> 0
                                             THEN t.FullChargeCapacity
                                             ELSE a.FullChargeCapacity
                                        END ,
            CycleCount = CASE WHEN t.CycleCount IS NOT NULL
                                          AND t.CycleCount <> 0
                                     THEN t.CycleCount
                                     ELSE a.CycleCount
                                END ,
            MaxCycleCount = CASE WHEN t.MaxCycleCount IS NOT NULL
                                             AND t.MaxCycleCount <> 0
                                        THEN t.MaxCycleCount
                                        ELSE a.MaxCycleCount
                                   END ,
            SiteID = ISNULL(( SELECT TOP 1
                                            SiteID
                                     FROM   dbo.Assets
                                     WHERE  SerialNo = t.DeviceSerialNumber
                                   ), a.SiteID) ,
            Retired = 0 ,
            OutOfService = 0 ,
            SourceIPAddress = t.SourceIPAddress ,
            IP = t.IP ,
            APMAC = ISNULL(t.APMac, a.APMAC) ,
            AccessPointId = ISNULL(( SELECT TOP 1
                                                    ROW_ID
                                            FROM    dbo.AccessPoint
                                            WHERE   MACAddress = t.APMac
                                                    AND SiteID = a.SiteID
                                            ORDER BY ModifiedDateUTC DESC ,
                                                    Floor DESC ,
                                                    DeviceCount DESC
                                          ), a.AccessPointId)
    FROM    dbo.Assets a
            INNER JOIN #tempNSDSelect t ON a.SerialNo = t.BatterySerialNumber;

    UPDATE  dbo.Assets
    SET     Temperature = t.Temperature ,
            AssetStatusID = 1 ,
            IsActive = 1 ,
            Bay1LastPostDateUTC = IIF(t.BatterySerialNumber IS NOT NULL
            AND ISNULL(t.Bay, 0) = 1, GETUTCDATE(), a.Bay1LastPostDateUTC) ,
            Bay2LastPostDateUTC = IIF(t.BatterySerialNumber IS NOT NULL
            AND ISNULL(t.Bay, 0) = 2, GETUTCDATE(), a.Bay1LastPostDateUTC) ,
            Bay3LastPostDateUTC = IIF(t.BatterySerialNumber IS NOT NULL
            AND ISNULL(t.Bay, 0) = 3, GETUTCDATE(), a.Bay1LastPostDateUTC) ,
            Bay4LastPostDateUTC = IIF(t.BatterySerialNumber IS NOT NULL
            AND ISNULL(t.Bay, 0) = 4, GETUTCDATE(), a.Bay1LastPostDateUTC) ,
            Bay1ChargerRevision = IIF(t.Bay1ChargerRevision IS NOT NULL
            AND ISNULL(t.Bay, 0) = 1, t.Bay1ChargerRevision, a.Bay1ChargerRevision) ,
            Bay2ChargerRevision = IIF(t.Bay2ChargerRevision IS NOT NULL
            AND ISNULL(t.Bay, 0) = 2, t.Bay2ChargerRevision, a.Bay2ChargerRevision) ,
            Bay3ChargerRevision = IIF(t.Bay3ChargerRevision IS NOT NULL
            AND ISNULL(t.Bay, 0) = 3, t.Bay3ChargerRevision, a.Bay3ChargerRevision) ,
            Bay4ChargerRevision = IIF(t.Bay4ChargerRevision IS NOT NULL
            AND ISNULL(t.Bay, 0) = 4, t.Bay4ChargerRevision, a.Bay4ChargerRevision) ,
            WiFiFirmwareRevision = ISNULL(t.WifiFirmwareRevision,
                                                 a.WiFiFirmwareRevision) ,
            BayWirelessRevision = ISNULL(t.BayWirelessRevision,
                                                a.BayWirelessRevision) ,
            Retired = 0 ,
            BayCount = IIF(t.Bay > 2, 4, a.BayCount) ,
            OutOfService = 0 ,
	--assets.SiteID = isnull((select top 1 siteID from SiteIPSubnet where SourceIPAddressBase =  (left(t.SourceIPAddress, len(t.SourceIPAddress) - charindex('.', reverse(t.SourceIPAddress)))) and LANIPAddressBase =  (left(t.IP, len(t.IP) - charindex('.', reverse(t.IP))))),a.SiteID),a.SiteID),
            SourceIPAddress = t.SourceIPAddress ,
	--assets.DepartmentID = isnull((select top 1 departmentid from AccessPoint where MACAddress = t.APMAC and SiteID = a.SiteID order by DeviceCount desc),a.departmentid),
            LastPostDateUTC = t.CreatedDateUTC ,
            IP = t.IP ,
            APMAC = ISNULL(t.APMac, a.APMAC) ,
            Move = t.MOVE ,
            Amps = ISNULL(t.Amps, a.Amps) ,
            AccessPointId = ISNULL(( SELECT TOP 1
                                                    ROW_ID
                                            FROM    dbo.AccessPoint
                                            WHERE   MACAddress = t.APMac
                                                    AND SiteID = a.SiteID
                                            ORDER BY ModifiedDateUTC DESC ,
                                                    Floor DESC ,
                                                    DeviceCount DESC
                                          ), a.AccessPointId)
    FROM    dbo.Assets a
            INNER JOIN #tempNSDSelect t ON a.SerialNo = t.DeviceSerialNumber;

    UPDATE  dbo.Assets
    SET     SiteID = ISNULL(( SELECT TOP 1
                                            sip.SiteID
                                     FROM   dbo.SiteIPSubnet sip
                                     WHERE  sip.SourceIPAddressBase = ( LEFT(t.SourceIPAddress,
                                                              LEN(t.SourceIPAddress)
                                                              - CHARINDEX('.',
                                                              REVERSE(t.SourceIPAddress))) )
                                            AND sip.LANIPAddressBase = ( LEFT(t.IP,
                                                              LEN(t.IP)
                                                              - CHARINDEX('.',
                                                              REVERSE(t.IP))) )
                                            AND sip.FacilityReliabilityRating > 70
                                   ), a.SiteID)
    FROM    dbo.Assets a
            INNER JOIN #tempNSDSelect t ON a.SerialNo = t.DeviceSerialNumber
                                           AND a.SiteID NOT IN (
                                           SELECT   SiteID
                                           FROM     dbo.SiteAllocationExclusion
                                           WHERE    UseIP = 0 )
                                           AND ISNULL(a.ManualAllocation, 0) = 0;

    UPDATE  dbo.Assets
    SET     SiteID = ISNULL(( SELECT TOP 1
                                            sip.SiteID
                                     FROM   dbo.SiteAPMAC sip
                                     WHERE  sip.APMAC = t.APMAC
                                            AND sip.FacilityReliabilityRating > 70
                                   ), a.SiteID)
    FROM    dbo.Assets a
            INNER JOIN #tempNSDSelect t ON a.SerialNo = t.DeviceSerialNumber
                                           AND a.SiteID NOT IN (
                                           SELECT   SiteID
                                           FROM     dbo.SiteAllocationExclusion
                                           WHERE    UseAP = 0 )
                                           AND ISNULL(a.ManualAllocation, 0) = 0;

    UPDATE  dbo.Assets
    SET     SiteID = ISNULL(( SELECT TOP 1
                                            sip.SiteID
                                     FROM   dbo.AccessPoint sip
                                     WHERE  sip.MACAddress = t.APMac
                                   ), a.SiteID)
    FROM    dbo.Assets a
            INNER JOIN #tempNSDSelect t ON a.SerialNo = t.DeviceSerialNumber
                                           AND ( a.SiteID NOT IN (
                                                 SELECT SiteID
                                                 FROM   dbo.SiteAllocationExclusion
                                                 WHERE  UseIP = 0
                                                        AND UseAP = 0 )
                                                 OR a.SiteID IN ( 4, 1125, 28 )
                                               )
                                           AND ISNULL(a.ManualAllocation, 0) = 0;

    UPDATE  dbo.Assets
    SET     SiteID = ISNULL(( SELECT TOP 1
                                            sip.SiteID
                                     FROM   dbo.AccessPoint sip
                                     WHERE  sip.MACAddress = t.APMac
                                            AND sip.Manual = 1
                                   ), a.SiteID)
    FROM    dbo.Assets a
            INNER JOIN #tempNSDSelect t ON a.SerialNo = t.DeviceSerialNumber
                                           AND ( a.SiteID NOT IN (
                                                 SELECT SiteID
                                                 FROM   dbo.SiteAllocationExclusion
                                                 WHERE  UseIP = 0
                                                        AND UseAP = 0 )
                                                 OR a.SiteID IN ( 4, 1125, 28 )
                                               )
                                           AND ISNULL(a.ManualAllocation, 0) = 0;


--End of Site Allocation checks

--Now do wings
    UPDATE  dbo.Assets
    SET     Wing = ISNULL(( SELECT TOP 1
                                            a.Wing
                                   FROM     dbo.SiteAPMAC sip
                                   WHERE    sip.APMAC = t.APMAC
                                            AND sip.FacilityReliabilityRating > 70
                                 ), a.Wing)
    FROM    dbo.Assets a
            INNER JOIN #tempNSDSelect t ON a.SerialNo = t.DeviceSerialNumber
                                           AND ( a.SiteID NOT IN (
                                                 SELECT SiteID
                                                 FROM   dbo.SiteAllocationExclusion
                                                 WHERE  UseAP = 0 )
                                                 OR a.SiteID IN ( 4, 1125, 28 )
                                               )
                                           AND ISNULL(a.ManualAllocation, 0) = 0;

    UPDATE  dbo.Assets
    SET     Wing = ISNULL(( SELECT TOP 1
                                            sip.Wing
                                   FROM     dbo.AccessPoint sip
                                   WHERE    sip.MACAddress = t.APMac
                                            AND sip.Manual = 1
                                 ), a.Wing)
    FROM    dbo.Assets a
            INNER JOIN #tempNSDSelect t ON a.SerialNo = t.DeviceSerialNumber
                                           AND ( a.SiteID NOT IN (
                                                 SELECT SiteID
                                                 FROM   dbo.SiteAllocationExclusion
                                                 WHERE  UseAP = 0 )
                                                 OR a.SiteID IN ( 4, 1125, 28 )
                                               )
                                           AND ISNULL(a.ManualAllocation, 0) = 0;


--Now do Floors 
    UPDATE  dbo.Assets
    SET     Floor = ISNULL(( SELECT TOP 1
                                            sip.Floor
                                    FROM    dbo.AccessPoint sip
                                    WHERE   sip.MACAddress = t.APMac
                                            AND sip.Manual = 1
                                  ), a.Floor)
    FROM    dbo.Assets a
            INNER JOIN #tempNSDSelect t ON a.SerialNo = t.DeviceSerialNumber
                                           AND ( a.SiteID NOT IN (
                                                 SELECT SiteID
                                                 FROM   dbo.SiteAllocationExclusion
                                                 WHERE  UseAP = 0 )
                                                 OR a.SiteID IN ( 4, 1125, 28 )
                                               )
                                           AND ISNULL(a.ManualAllocation, 0) = 0;


    UPDATE  dbo.Assets
    SET     Floor = ISNULL(( SELECT TOP 1
                                            Floor
                                    FROM    dbo.SiteAPMAC sip
                                    WHERE   sip.APMAC = t.APMAC
                                            AND sip.FacilityReliabilityRating > 70
                                  ), a.Floor)
    FROM    dbo.Assets a
            INNER JOIN #tempNSDSelect t ON a.SerialNo = t.DeviceSerialNumber
                                           AND ( a.SiteID NOT IN (
                                                 SELECT SiteID
                                                 FROM   dbo.SiteAllocationExclusion
                                                 WHERE  UseAP = 0 )
                                                 OR a.SiteID IN ( 4, 1125, 28 )
                                               )
                                           AND ISNULL(a.ManualAllocation, 0) = 0;




--Now Do departments
    UPDATE  dbo.Assets
    SET     DepartmentID = ISNULL(( SELECT TOP 1
                                                    sip.DepartmentID
                                           FROM     dbo.AccessPoint sip
                                           WHERE    sip.MACAddress = t.APMac
                                                    AND sip.Manual = 0
                                         ), a.DepartmentID)
    FROM    dbo.Assets a
            INNER JOIN #tempNSDSelect t ON a.SerialNo = t.DeviceSerialNumber
                                           AND ( a.SiteID NOT IN (
                                                 SELECT SiteID
                                                 FROM   dbo.SiteAllocationExclusion
                                                 WHERE  UseAP = 0 )
                                                 OR a.SiteID IN ( 4, 1125, 28 )
                                               )
                                           AND ISNULL(a.ManualAllocation, 0) = 0;



    UPDATE  dbo.Assets
    SET     DepartmentID = ISNULL(( SELECT TOP 1
                                                    sip.DepartmentID
                                           FROM     dbo.AccessPoint sip
                                           WHERE    sip.MACAddress = t.APMac
                                                    AND sip.Manual = 1
                                         ), a.DepartmentID)
    FROM    dbo.Assets a
            INNER JOIN #tempNSDSelect t ON a.SerialNo = t.DeviceSerialNumber
                                           AND ( a.SiteID NOT IN (
                                                 SELECT SiteID
                                                 FROM   dbo.SiteAllocationExclusion
                                                 WHERE  UseAP = 0 )
                                                 OR a.SiteID IN ( 4, 1125, 28 )
                                               )
                                           AND ISNULL(a.ManualAllocation, 0) = 0;



    UPDATE  dbo.Assets
    SET     DepartmentID = ISNULL(( SELECT TOP 1
                                                    DepartmentID
                                           FROM     dbo.SiteAPMAC sip
                                           WHERE    sip.APMAC = t.APMAC
                                                    AND sip.FacilityReliabilityRating > 70
                                         ), a.DepartmentID)
    FROM    dbo.Assets a
            INNER JOIN #tempNSDSelect t ON a.SerialNo = t.DeviceSerialNumber
                                           AND ( a.SiteID NOT IN (
                                                 SELECT SiteID
                                                 FROM   dbo.SiteAllocationExclusion
                                                 WHERE  UseAP = 0 )
                                                 OR a.SiteID IN ( 4, 1125, 28 )
                                               )
                                           AND ISNULL(a.ManualAllocation, 0) = 0;





--Battery site allocation Check;  this shouldn't do anything
    UPDATE  dbo.Assets
    SET     SiteID = ISNULL(( SELECT TOP 1
                                            SiteID
                                     FROM   dbo.Assets
                                     WHERE  SerialNo = t.DeviceSerialNumber
                                   ), a.SiteID)
    FROM    dbo.Assets a
            INNER JOIN #tempNSDSelect t ON a.SerialNo = t.BatterySerialNumber
    WHERE   ISNULL(a.ManualAllocation, 0) = 0;

		

    UPDATE  dbo.Assets
    SET     Retired = 1
    WHERE   ( LastPostDateUTC < DATEADD(DAY, -29, GETUTCDATE())
              OR LastPostDateUTC IS NULL
            )
            AND CreatedDateUTC < DATEADD(MONTH, -3, GETUTCDATE());


    UPDATE  dbo.Assets
    SET     DepartmentID = ( SELECT IDDepartment
                             FROM   dbo.Departments
                             WHERE  DefaultForSite = 1
                                    AND SiteID = asst.SiteID
                           )
    FROM    dbo.Assets asst
    WHERE   asst.SerialNo IN (
            SELECT  ass.SerialNo
            FROM    dbo.Assets ass
                    LEFT JOIN dbo.Sites s ON s.IDSite = ass.SiteID
                    LEFT JOIN dbo.Departments dep ON ass.DepartmentID = dep.IDDepartment
            WHERE   dep.SiteID <> ass.SiteID
                    AND ass.Retired = 0
                    AND ass.SiteID NOT IN ( 4, 28, 1125 )
                    AND ass.IDAssetType <> 5 );













GO
