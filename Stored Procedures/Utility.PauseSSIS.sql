SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:      <ENOVATE\arlow.farrell,ARLOWFARRELL>
-- Create date: <10/10/2016> <9:30 PM>
-- Description: 
--				Default Description of Stored Proc with Supporting Links, Notes, etc.     
--
-- 
-- =============================================
CREATE PROCEDURE [Utility].[PauseSSIS]
AS
    BEGIN
        SET NOCOUNT ON;
       
    DECLARE @bIndexRunning BIT	=0;
	DECLARE	@bPurgeRunning BIT = 0;
	SET @bIndexRunning = (SELECT SS.SettingValue FROM dbo.SystemSettings AS SS WHERE SS.SettingKey = 'IndexRunning');
	SET @bPurgeRunning = (SELECT ss.SettingValue FROM dbo.SystemSettings AS SS WHERE SS.SettingKey = 'PurgeRunning');

	IF(@bIndexRunning = 1 OR @bPurgeRunning=1)
	BEGIN	
	WAITFOR DELAY '00:10:00';
	END
    END;
GO
