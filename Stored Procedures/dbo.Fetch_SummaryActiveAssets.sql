SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[Fetch_SummaryActiveAssets] ( @SiteID AS INT = 657 )
AS
    WITH    SitesAndDepartments
              AS ( SELECT DISTINCT
                            a.SiteID ,
                            s.SiteName ,
                            a.DepartmentID ,
                            d.Description AS DepartmentName ,
                            COALESCE(a.SiteFloorID, 0) SiteFloorId ,
                            COALESCE(a.SiteWingID, 0) SiteWingID ,
                            GETDATE() AS RecordDate
                   FROM     dbo.Assets a
                            INNER JOIN dbo.Sites s ON a.SiteID = s.IDSite
                            INNER JOIN dbo.Departments d ON a.DepartmentID = d.IDDepartment
                   WHERE    a.Retired = 0
                 ),
            WorkstationDetails
              AS ( SELECT   a.SerialNo AS DeviceSerialNumber ,
                            a.SiteID ,
                            a.DepartmentID ,
                            COALESCE(a.SiteFloorID, 0) SiteFloorId ,
                            COALESCE(a.SiteWingID, 0) SiteWingId ,
                            ast.Description AS AssetStatus ,
                            CONVERT(INT, a.IsActive) AS IsActive ,
                            CASE WHEN a.LastPostDateUTC > DATEADD(hh, -2,
                                                              GETDATE())
                                 THEN 1
                                 ELSE 0
                            END AS IsInService ,
                            CASE a.AssetStatusID
                              WHEN 3 THEN 1
                              ELSE 0
                            END AS IsNotComissioned ,
                            CASE a.AssetStatusID
                              WHEN 1 THEN 1
                              ELSE 0
                            END AS IsAvailable ,
                            CASE WHEN a.LastPostDateUTC BETWEEN DATEADD(d, -7,
                                                              GETDATE())
                                                        AND   DATEADD(d, -2,
                                                              GETDATE())
                                 THEN 1
                                 ELSE 0
                            END AS IsDormant
                   FROM     dbo.Assets a
                            INNER JOIN dbo.AssetType at ON at.IDAssetType = a.IDAssetType
                            INNER JOIN dbo.AssetCategory ac ON ac.IDAssetCategory = at.AssetCategoryId
                            INNER JOIN dbo.AssetStatus ast ON ast.IDAssetStatus = a.AssetStatusID
                   WHERE    ac.Description = 'Workstation'
                 ),
            WorkstationSummary
              AS ( SELECT   WorkstationDetails.SiteID ,
                            WorkstationDetails.DepartmentID ,
                            WorkstationDetails.SiteFloorId ,
                            WorkstationDetails.SiteWingId ,
                            COUNT(WorkstationDetails.DeviceSerialNumber) AS TotalWorkstations ,
                            SUM(WorkstationDetails.IsActive) AS ActiveWorkstations ,
                            SUM(WorkstationDetails.IsInService) AS InServiceWorkstations ,
                            SUM(WorkstationDetails.IsAvailable) AS AvailableWorkstations ,
                            SUM(WorkstationDetails.IsNotComissioned) AS WorkstationsNotCommissioned ,
                            SUM(WorkstationDetails.IsDormant) AS DormantWorkstations
                   FROM     WorkstationDetails
                   GROUP BY WorkstationDetails.SiteID ,
                            WorkstationDetails.DepartmentID ,
                            WorkstationDetails.SiteFloorId ,
                            WorkstationDetails.SiteWingId
                 ),
            ChargerDetails
              AS ( SELECT   a.SerialNo AS DeviceSerialNumber ,
                            a.SiteID ,
                            a.DepartmentID ,
                            COALESCE(a.SiteFloorID, 0) SiteFloorId ,
                            COALESCE(a.SiteWingID, 0) SiteWingId ,
                            ast.Description AS AssetStatus ,
                            CONVERT(INT, a.IsActive) AS IsActive ,
                            CASE WHEN a.LastPostDateUTC > DATEADD(hh, -2,
                                                              GETDATE())
                                 THEN 1
                                 ELSE 0
                            END AS IsInService ,
                            CASE a.AssetStatusID
                              WHEN 3 THEN 1
                              ELSE 0
                            END AS IsNotComissioned ,
                            CASE a.AssetStatusID
                              WHEN 1 THEN 1
                              ELSE 0
                            END AS IsAvailable ,
                            CASE WHEN a.LastPostDateUTC BETWEEN DATEADD(d, -7,
                                                              GETDATE())
                                                        AND   DATEADD(d, -2,
                                                              GETDATE())
                                 THEN 1
                                 ELSE 0
                            END AS IsDormant
                   FROM     dbo.Assets a
                            INNER JOIN dbo.AssetType at ON at.IDAssetType = a.IDAssetType
                            INNER JOIN dbo.AssetCategory ac ON ac.IDAssetCategory = at.AssetCategoryId
                            INNER JOIN dbo.AssetStatus ast ON ast.IDAssetStatus = a.AssetStatusID
                   WHERE    ac.Description = 'Charger'
                 ),
            ChargerSummary
              AS ( SELECT   ChargerDetails.SiteID ,
                            ChargerDetails.DepartmentID ,
                            ChargerDetails.SiteFloorId ,
                            ChargerDetails.SiteWingId ,
                            COUNT(ChargerDetails.DeviceSerialNumber) AS TotalChargers ,
                            SUM(ChargerDetails.IsActive) AS ActiveChargers ,
                            SUM(ChargerDetails.IsInService) AS InServiceChargers ,
                            SUM(ChargerDetails.IsAvailable) AS AvailableChargers ,
                            SUM(ChargerDetails.IsNotComissioned) AS ChargersNotCommissioned ,
                            SUM(ChargerDetails.IsDormant) AS DormantChargers
                   FROM     ChargerDetails
                   GROUP BY ChargerDetails.SiteID ,
                            ChargerDetails.DepartmentID ,
                            ChargerDetails.SiteFloorId ,
                            ChargerDetails.SiteWingId
                 ),
            BatteryDetails
              AS ( SELECT   a.SerialNo AS DeviceSerialNumber ,
                            a.SiteID ,
                            a.DepartmentID ,
                            COALESCE(a.SiteFloorID, 0) SiteFloorId ,
                            COALESCE(a.SiteWingID, 0) SiteWingId ,
                            ast.Description AS AssetStatus ,
                            CONVERT(INT, a.IsActive) AS IsActive ,
                            CASE WHEN a.LastPostDateUTC > DATEADD(hh, -2,
                                                              GETDATE())
                                 THEN 1
                                 ELSE 0
                            END AS IsInService ,
                            CASE a.AssetStatusID
                              WHEN 3 THEN 1
                              ELSE 0
                            END AS IsNotComissioned ,
                            CASE a.AssetStatusID
                              WHEN 1 THEN 1
                              ELSE 0
                            END AS IsAvailable ,
                            CASE WHEN a.LastPostDateUTC BETWEEN DATEADD(d, -7,
                                                              GETDATE())
                                                        AND   DATEADD(d, -2,
                                                              GETDATE())
                                 THEN 1
                                 ELSE 0
                            END AS IsDormant
                   FROM     dbo.Assets a
                            INNER JOIN dbo.AssetType at ON at.IDAssetType = a.IDAssetType
                            INNER JOIN dbo.AssetCategory ac ON ac.IDAssetCategory = at.AssetCategoryId
                            INNER JOIN dbo.AssetStatus ast ON ast.IDAssetStatus = a.AssetStatusID
                   WHERE    ac.Description = 'Battery'
                 ),
            BatterySummary
              AS ( SELECT   BatteryDetails.SiteID ,
                            BatteryDetails.DepartmentID ,
                            BatteryDetails.SiteFloorId ,
                            BatteryDetails.SiteWingId ,
                            COUNT(BatteryDetails.DeviceSerialNumber) AS TotalBatteries ,
                            SUM(BatteryDetails.IsActive) AS ActiveBatteries ,
                            SUM(BatteryDetails.IsInService) AS InServiceBatteries ,
                            SUM(BatteryDetails.IsAvailable) AS AvailableBatteries ,
                            SUM(BatteryDetails.IsNotComissioned) AS BatteriesNotCommissioned ,
                            SUM(BatteryDetails.IsDormant) AS DormantBatteries
                   FROM     BatteryDetails
                   GROUP BY BatteryDetails.SiteID ,
                            BatteryDetails.DepartmentID ,
                            BatteryDetails.SiteFloorId ,
                            BatteryDetails.SiteWingId
                 ),
            CombinedSessionData
              AS ( SELECT   s.SiteID ,
                            COALESCE(d.DepartmentID, 0) AS DepartmentID ,
                            COALESCE(d.SiteFloorID, 0) AS SiteFloorId ,
                            COALESCE(d.SiteWingID, 0) AS SiteWingId ,
                            COUNT(s.ROW_ID) AS SessionCount ,
                            SUM(CASE WHEN CONVERT(DATE, s.StartDateUTC) = CONVERT(DATE, DATEADD(dd,
                                                              -1, GETDATE()))
                                          AND s.StartChargeLevel < 90 THEN 1
                                     ELSE 0
                                END) AS LoChargeInsertsCount ,
                            SUM(CASE WHEN CONVERT(DATE, s.EndDateUTC) = CONVERT(DATE, DATEADD(dd,
                                                              -1, GETDATE()))
                                          AND s.EndChargeLevel > 10 THEN 1
                                     ELSE 0
                                END) AS HiChargeRemovalsCount ,
                            AVG(COALESCE(s.RunRate, 0)) AS AvgRunRate ,
					   MAX(COALESCE(s.RunRate, 0)) AS MaxRunRate ,
					   Min(COALESCE(s.RunRate, 0)) AS MinRunRate ,
					   AVG(COALESCE(s.EstimatedPCUtilization, 0)) AS AvgEstUtilization ,
					   MAX(COALESCE(s.EstimatedPCUtilization, 0)) AS MaxEstUtilization ,
					   MIN(COALESCE(s.EstimatedPCUtilization, 0)) AS MinEstUtilization ,
                            AVG(COALESCE(s.AvgAmpDraw, 0)) AS AvgAmpDraw ,
                            MAX(COALESCE(s.HiAmpDraw, 0)) AS MaxAmpDraw ,
                            MIN(COALESCE(s.LowAmpDraw, 0)) AS MinAmpDraw ,
                            AVG(( COALESCE(s.StartChargeLevel, 1)
                                  + COALESCE(s.EndChargeLevel, 1) ) / 2) AS AvgChargeLevel ,
                            SUM(CASE WHEN CONVERT(DATE, s.StartDateUTC) = CONVERT(DATE, DATEADD(dd,
                                                              -1, GETDATE()))
                                          AND s.EndChargeLevel < 90 THEN 1
                                     ELSE 0
                                END) AS LoChargeRemovalsCount ,
                            SUM(CASE WHEN CONVERT(DATE, s.StartDateUTC) = CONVERT(DATE, DATEADD(dd,
                                                              -1, GETDATE()))
                                          AND s.StartChargeLevel > 90 THEN 1
                                     ELSE 0
                                END) AS HiChargeInsertsCount ,
                            AVG(COALESCE(s.AvgSignalQuality, 0)) AS AvgSignalQuality ,
                            AVG(COALESCE(s.RemainingCapacity, 0)) AS AvgRemainingCapacity ,
                            SUM(COALESCE(s.PacketCount, 0)) AS TotalSessionPackets
                   FROM     dbo.Sessions s
                            JOIN dbo.Assets d ON s.DeviceSerialNumber = d.SerialNo
                   WHERE    s.EndDateUTC >= DATEADD(dd, -1, GETUTCDATE())
                            AND d.OutOfService = 0
                            AND d.Retired = 0
                   GROUP BY s.SiteID ,
                            d.DepartmentID ,
                            COALESCE(d.SiteFloorID, 0) ,
                            COALESCE(d.SiteWingID, 0)
                 ),
            ChargerBayDetails
              AS ( SELECT   a.SerialNo AS DeviceSerialNumber ,
                            a.SiteID ,
                            a.DepartmentID ,
                            COALESCE(a.SiteFloorID, 0) SiteFloorId ,
                            COALESCE(a.SiteWingID, 0) SiteWingId ,
                            a.BayCount AS ChargingBays ,
                            CASE WHEN a.Bay1LastPostDateUTC > DATEADD(MINUTE, -8,
                                                              GETUTCDATE())
                                 THEN 1
                                 ELSE 0
                            END AS Bay1Charging ,
                            CASE WHEN a.Bay2LastPostDateUTC > DATEADD(MINUTE, -8,
                                                              GETUTCDATE())
                                 THEN 1
                                 ELSE 0
                            END AS Bay2Charging ,
                            CASE WHEN a.Bay3LastPostDateUTC > DATEADD(MINUTE, -8,
                                                              GETUTCDATE())
                                 THEN 1
                                 ELSE 0
                            END AS Bay3Charging ,
                            CASE WHEN a.Bay4LastPostDateUTC > DATEADD(MINUTE, -8,
                                                              GETUTCDATE())
                                 THEN 1
                                 ELSE 0
                            END AS Bay4Charging ,
                            CASE WHEN a.Bay1LastPostDateUTC <= DATEADD(MINUTE,
                                                              -8, GETUTCDATE())
                                      AND a.Bay1LastPostDateUTC > DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                 THEN 1
                                 ELSE 0
                            END AS Bay1Vacant ,
                            CASE WHEN a.Bay2LastPostDateUTC <= DATEADD(MINUTE,
                                                              -8, GETUTCDATE())
                                      AND a.Bay2LastPostDateUTC > DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                 THEN 1
                                 ELSE 0
                            END AS Bay2Vacant ,
                            CASE WHEN a.Bay3LastPostDateUTC <= DATEADD(MINUTE,
                                                              -8, GETUTCDATE())
                                      AND a.Bay3LastPostDateUTC > DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                 THEN 1
                                 ELSE 0
                            END AS Bay3Vacant ,
                            CASE WHEN a.Bay4LastPostDateUTC <= DATEADD(MINUTE,
                                                              -8, GETUTCDATE())
                                      AND a.Bay4LastPostDateUTC > DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                 THEN 1
                                 ELSE 0
                            END AS Bay4Vacant ,
                            CASE WHEN a.Bay1LastPostDateUTC <= DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                 THEN 1
                                 ELSE 0
                            END AS Bay1Offline ,
                            CASE WHEN a.Bay2LastPostDateUTC <= DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                 THEN 1
                                 ELSE 0
                            END AS Bay2Offline ,
                            CASE WHEN a.Bay3LastPostDateUTC <= DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                 THEN 1
                                 ELSE 0
                            END AS Bay3Offline ,
                            CASE WHEN a.Bay4LastPostDateUTC <= DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                 THEN 1
                                 ELSE 0
                            END AS Bay4Offline
                   FROM     dbo.Assets a
                            INNER JOIN dbo.AssetType at ON at.IDAssetType = a.IDAssetType
                            INNER JOIN dbo.AssetCategory ac ON ac.IDAssetCategory = at.AssetCategoryId
                            INNER JOIN dbo.AssetStatus ast ON ast.IDAssetStatus = a.AssetStatusID
                   WHERE    ac.Description = 'Charger'
                 ),
            ChargerBaySummary
              AS ( SELECT   d.SiteID ,
                            d.DepartmentID ,
                            d.SiteFloorId ,
                            d.SiteWingId ,
                            SUM(d.Bay1Charging) + SUM(d.Bay2Charging)
                            + SUM(d.Bay3Charging) + SUM(d.Bay4Charging) AS BaysCharging ,
                            SUM(d.Bay1Vacant) + SUM(d.Bay2Vacant)
                            + SUM(d.Bay3Vacant) + SUM(d.Bay4Vacant) AS BaysVacant ,
                            SUM(d.Bay1Offline) + SUM(d.Bay2Offline)
                            + SUM(d.Bay3Offline) + SUM(d.Bay4Offline) AS BaysOffline
                   FROM     ChargerBayDetails d
                   GROUP BY d.SiteID ,
                            d.DepartmentID ,
                            d.SiteFloorId ,
                            d.SiteWingId
                 )
        SELECT  s.SiteID ,
                s.SiteName ,
                s.DepartmentID ,
                s.DepartmentName ,
                s.SiteFloorId ,
                s.SiteWingID ,
                COALESCE(w.TotalWorkstations, 0) AS TotalWorkstations ,
                COALESCE(w.ActiveWorkstations, 0) AS ActiveWorkstations ,
                COALESCE(w.InServiceWorkstations, 0) AS InServiceWorkstations ,
                COALESCE(w.AvailableWorkstations, 0) AS AvailableWorkstations ,
                COALESCE(w.WorkstationsNotCommissioned, 0) AS WorkstationNotCommissioned ,
                COALESCE(w.DormantWorkstations, 0) AS DormantWorkstations ,
                COALESCE(c.TotalChargers, 0) AS TotalChargers ,
                COALESCE(c.ActiveChargers, 0) AS ActiveChargers ,
                COALESCE(c.AvailableChargers, 0) AS AvailableChargers ,
                COALESCE(c.InServiceChargers, 0) AS InServiceChargers ,
                COALESCE(c.ChargersNotCommissioned, 0) AS ChargersNotComissioned ,
                COALESCE(c.DormantChargers, 0) AS DormantChargers ,
                COALESCE(cbs.BaysCharging, 0) AS BaysCharging ,
                COALESCE(cbs.BaysVacant, 0) AS BaysVacant ,
                COALESCE(cbs.BaysOffline, 0) AS BaysOffline ,
                COALESCE(b.TotalBatteries, 0) AS TotalBatteries ,
                COALESCE(b.ActiveBatteries, 0) AS ActiveBatteries ,
                COALESCE(b.InServiceBatteries, 0) AS InserviceBatteries ,
                COALESCE(b.AvailableBatteries, 0) AS AvailableBatteries ,
                COALESCE(b.BatteriesNotCommissioned, 0) AS BatteriesNotCommissioned ,
                COALESCE(b.DormantBatteries, 0) AS DormantBatteries ,
                COALESCE(csd.AvgRunRate, 0) AS AvgRunRate ,
			 COALESCE(csd.MaxRunRate,0)AS MaxRunRate,
			 COALESCE(csd.MinRunRate,0) AS MinRunRate,
			 COALESCE(csd.AvgEstUtilization,0) AS AvgEstUtilization,
			 COALESCE(csd.MaxEstUtilization,0) AS MaxEstUtilization,
			 COALESCE(csd.MinEstUtilization,0) AS MinEstUtilization,
                COALESCE(csd.LoChargeInsertsCount, 0) AS LoChargeInserts ,
                COALESCE(csd.HiChargeRemovalsCount, 0) AS HiChargeRemovals ,
                COALESCE(csd.AvgAmpDraw, 0) AS AvgAmpDraw ,
                COALESCE(csd.MaxAmpDraw, 0) AS MaxAmpDraw ,
                COALESCE(csd.MinAmpDraw, 0) AS MinAmpDraw ,
                COALESCE(csd.AvgChargeLevel, 0) AS AvgChargeLevel ,
                COALESCE(csd.LoChargeRemovalsCount, 0) AS LoChargeRemovals ,
                COALESCE(csd.HiChargeInsertsCount, 0) AS HiChargeInserts ,
                COALESCE(csd.AvgSignalQuality, 0) AS AvgSignalQuality ,
                COALESCE(csd.AvgRemainingCapacity, 0) AS AvgRemainingCapacity ,
                COALESCE(csd.TotalSessionPackets, 0) AS TotalSessionPackets
	   
        FROM    SitesAndDepartments s
                LEFT OUTER JOIN WorkstationSummary w ON s.SiteID = w.SiteID
                                                        AND s.DepartmentID = w.DepartmentID
                                                        AND s.SiteFloorId = w.SiteFloorId
                                                        AND s.SiteWingID = w.SiteWingId
                LEFT OUTER JOIN ChargerSummary c ON s.SiteID = c.SiteID
                                                    AND s.DepartmentID = c.DepartmentID
                                                    AND s.SiteFloorId = c.SiteFloorId
                                                    AND s.SiteWingID = c.SiteWingId
                LEFT OUTER JOIN BatterySummary b ON s.SiteID = b.SiteID
                                                    AND s.DepartmentID = b.DepartmentID
                LEFT OUTER JOIN ChargerBaySummary cbs ON s.SiteID = cbs.SiteID
                                                         AND s.DepartmentID = cbs.DepartmentID
                                                         AND s.SiteFloorId = cbs.SiteFloorId
                                                         AND s.SiteWingID = cbs.SiteWingId
                LEFT OUTER JOIN CombinedSessionData csd ON s.SiteID = csd.SiteID
                                                           AND s.DepartmentID = csd.DepartmentID
                                                           AND s.SiteFloorId = csd.SiteFloorId
                                                           AND s.SiteWingID = csd.SiteWingId
        WHERE   s.SiteID = @SiteID;

GO
