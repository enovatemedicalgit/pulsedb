SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================

-- =============================================
CREATE Procedure [dbo].[prcSaveNetwork]
    @SSID VARCHAR(40) ,
    @Password VARCHAR(40) ,
    @SerialNo VARCHAR(40) = NULL ,
    @Channel VARCHAR(4) = 0 ,
    @NetAuthProtocol VARCHAR(13) = 0 ,
    @SiteID VARCHAR(4) = NULL,
	@Successful INT = 0,
	@IDUser INT = null
AS
    BEGIN
        DECLARE @iChannel INT = ISNULL(CAST(@Channel AS INT), 0);
        DECLARE @iNetAuthProtocol INT = NULL;
        IF ( LEN(@NetAuthProtocol) < 2 )
            BEGIN
                SET @iNetAuthProtocol = ISNULL(CAST(@NetAuthProtocol AS INT),
                                               5);
            END;
        ELSE
            BEGIN
                SET @iNetAuthProtocol = ( SELECT    ROW_ID
                                          FROM      dbo.NetAuthProtocol
                                          WHERE     Description LIKE @NetAuthProtocol
                                        );
            END;
        DECLARE @iSiteID INT = ISNULL(CAST(@SiteID AS INT), NULL);
		IF EXISTS (SELECT * FROM dbo.Network AS N WHERE N.SiteID = @SiteID AND N.SSID = @SSID AND N.WPAPassphrase = @Password )
		BEGIN
		UPDATE dbo.Network SET ModifiedDateUTC = GETUTCDATE() WHERE SiteID = @SiteID AND SSID = @SSID AND WPAPassphrase = @Password AND Successful= @Successful AND modifieddateutc = GETUTCDATE() AND IDUser = @IDUser
        end
        SET NOCOUNT ON;
        IF ( @iNetAuthProtocol = 1
             OR @iNetAuthProtocol = 5
             OR @iNetAuthProtocol = 4
           ) -- WPA
            BEGIN
                INSERT  INTO dbo.Network
                        ( SSID ,
                          WPAPassphrase ,
                          SiteID ,
                          Channel ,
                          CreatedDateUTC ,
                          NetAuthProtocolID ,
                          DHCP,
						  Successful,
						  IDUser
                        )
                VALUES  ( @SSID ,
                          @Password ,
                          ISNULL(@iSiteID,
                                 ISNULL(( SELECT    SiteID
                                          FROM      dbo.Assets
                                          WHERE     SerialNo = @SerialNo
                                        ), 0)) ,
                          @iChannel ,
                          GETUTCDATE() ,
                          @iNetAuthProtocol ,
                          1,
						  @Successful,
						  @IDUser
                        );
            END;
        IF ( @iNetAuthProtocol = 3
             OR @iNetAuthProtocol = 2
           ) -- WEP
            BEGIN
                INSERT  INTO dbo.Network
                        ( SSID ,
                          WEPKey ,
                          SiteID ,
                          Channel ,
                          CreatedDateUTC ,
                          NetAuthProtocolID ,
                          DHCP,
						  Successful,
						  IDUser
                        )
                VALUES  ( @SSID ,
                          @Password ,
                          ISNULL(@iSiteID,
                                 ISNULL(( SELECT    SiteID
                                          FROM      dbo.Assets
                                          WHERE     SerialNo = @SerialNo
                                        ), 0)) ,
                          @iChannel ,
                          GETUTCDATE() ,
                          @iNetAuthProtocol ,
                          1,
						  @Successful,
						  @IDUser
                        );
            END;
    END;



GO
