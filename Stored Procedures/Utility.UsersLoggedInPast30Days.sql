SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Utility].[UsersLoggedInPast30Days]
AS
BEGIN
	    /**************************************************	 Users Logged in Past Month	    **************************************************/

	    SELECT
			 SUM(TotalUsers) AS   TotalUsers
		    , SUM(TotalEmUsers) AS TotalEMUsers
		    , SUM(TotalSites) AS   TotalSites
	    FROM
	    (
		   SELECT
				COUNT(DISTINCT email) AS  TotalUsers
			   , 0 AS                      TotalEmUsers
			   , COUNT(DISTINCT IDSite) AS TotalSites
		   FROM
			   dbo.[User]
		   WHERE   LastLoginDateUTC > DATEADD(MONTH, -1, GETUTCDATE())
				 AND UserTypeId = 0
		   UNION
		   SELECT
				0 AS                      TotalUsers
			   , COUNT(DISTINCT email) AS  TotalEmUsers
			   , COUNT(DISTINCT IDSite) AS TotalSites
		   FROM
			   dbo.[User]
		   WHERE  LastLoginDateUTC > DATEADD(MONTH, -1, GETUTCDATE())
				AND UserTypeId = 1
							   ) AS base;
	END;
GO
