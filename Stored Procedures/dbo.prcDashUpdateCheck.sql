SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[prcDashUpdateCheck]
 @siteId AS INT =657,
 @MaxCaseid AS NVARCHAR(150) =null,
 @MaxAlertid AS INT=0,
 @sMaxLastPostDateUTC AS NVARCHAR(200) =  null
 AS
 BEGIN
  DECLARE @MaxLastPostDateUTC AS DATETIME2 = NULL;
 IF (@sMaxLastPostDateUTC IS NULL) 
 BEGIN
 SET @MaxLastPostDateUTC = GETUTCDATE();

 END
 ELSE
 begin
 SET @MaxLastPostDateUTC =  convert(varchar(30),cast(@sMaxLastPostDateUTC as datetime2),102);

 END

DECLARE	@currMaxCaseid AS NVARCHAR(150) = null;
DECLARE @currMaxAlertid AS INT = 0;
DECLARE @currMaxLastPostDateUTC AS datetime2 = '1910-10-10';

SET  @currMaxCaseid= ( SELECT MAX(Row_ID) FROM cases WHERE SiteID = @siteId)
SET @currMaxAlertid = (SELECT MAX(AM.ROW_ID) FROM dbo.AlertMessage AS AM WHERE SiteID = @siteId);
SET @currMaxLastPostDateUTC= (SELECT MAX(A.LastPostDateUTC) FROM dbo.Assets AS A WHERE A.SiteID = @siteId);

SELECT IIF(@currMaxCaseid <> @MaxCaseid,1,0) AS UpdateCase,ISNULL(@currMaxCaseid,'') AS 'currMaxCaseid',IIF(@currMaxAlertid<>@MaxAlertid,1,0) AS UpdateAlert,@currMaxAlertid AS 'currMaxAlertid', IIF(ISNULL(@currMaxLastPostDateUTC,'1910-10-10') > @MaxLastPostDateUTC,1,0) AS UpdateAsset,ISNULL(@currMaxLastPostDateUTC,'1910-10-10') AS 'MaxLastPostDateUTC' 
   
    END


GO
