SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spInsertMacAddrTest]
    @SiteID VARCHAR(255) ,
    @MacAddr VARCHAR(50) ,
    @AvgLinkQuality INT = 0 ,
    @SSID VARCHAR(255) = ''
AS
    BEGIN
         -- SET NOCOUNT ON added to prevent extra result sets from
         -- interfering with SELECT statements.
        SET NOCOUNT ON;
	    -- mitigate the avglinkquality for now
        SET @AvgLinkQuality = @AvgLinkQuality + 10;
        IF ( @AvgLinkQuality > 100 )
            SET @AvgLinkQuality = 100;
            THROW 60000, 'Timeout testing',1;
    END;

GO
