SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[LostAssetComm]
    @SiteID VARCHAR(20) ,
    @AssetSerialNumber VARCHAR(20) = NULL
AS
    BEGIN
        IF @AssetSerialNumber IS NULL
            BEGIN
                SELECT  *
                FROM    dbo.viewLostAssetCommSimple
                WHERE   IDAssetType != 5
                        AND IDSite = 68
                        AND Retired <> 1
                        AND ( (LastPostDateUTC < DATEADD(HOUR, -24, GETDATE())
                              OR ( IDAssetType IN ( 1, 6 )
                                   AND LastPostDateUTC < DATEADD(DAY, -6,
                                                              GETDATE())
                                 ))
                            )
                ORDER BY LastPostDateUTC DESC;
            END;
        ELSE
            BEGIN
                SELECT TOP 100
                        *
                FROM    dbo.Assets
                WHERE   LastPostDateUTC < DATEADD(WEEK, -1, GETUTCDATE())
                        AND IDAssetType IN ( 1, 2, 3, 4, 6 )
                        AND SiteID IN (
                        SELECT  IDSite
                        FROM    dbo.Sites
                        WHERE   CustomerID IN ( SELECT  IDCustomer
                                                FROM    dbo.Customers
                                                WHERE   SiteID = @SiteID ) ); --and (SerialNo = @AssetSerialNumber or SerialNo = @AssetSerialNumber)
            END;
    END;


GO
