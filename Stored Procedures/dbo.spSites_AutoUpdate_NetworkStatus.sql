SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author: Arlow Farrell

-- Description:	Auto check to set the communication
-- status of business units automatically.
--
-- Calculates percentage of workstations  and chargers online at a facility
-- 0% of workstations and chargers are active = 'Not Yet Set'
-- 1- 59% of workstations and chargers are active = 'Partial'
-- 60% or greater of workstations and chargers are active = 'Live'
-- 90% or greater of workstations and chargers = 'AllActive'
-- Final update that if AllActive is marked, Authenticated
-- is automatically marked as well. 
-- =============================================
CREATE PROCEDURE [dbo].[spSites_AutoUpdate_NetworkStatus]
AS
    BEGIN
	
        DECLARE @Online DECIMAL(18, 0)= 0.00;
        DECLARE @Active INT= 0;
        DECLARE @Total INT= 0;

        DECLARE @BUID INT;
        DECLARE @Stat CHAR(1)= 'N';
        DECLARE @sStatus VARCHAR(20);
        DECLARE @cSiteID AS INT;

        DECLARE @BUIDs TABLE ( IDSite INT );
 
   
			--	UPDATE dbo.sites SET lastlogindateutc = NULL WHERE (onlinedevicespercentage < 40 OR activedevices < 10)

        INSERT  INTO @BUIDs
                SELECT DISTINCT
                        s.IDSite AS IDSite
                FROM    dbo.Sites s
                WHERE  s.IDSite IS NOT NULL and s.IDSite NOT IN ( 1, 4, 28, 1125, 1128,1152 )
                        AND ( s.IDSite IN (
                              SELECT    U.SiteID
                              FROM      dbo.UserSites AS U
                           )
                              OR s.IDSite IN (
                              SELECT    s2.IDSite
                              FROM      dbo.Sites s2
                                        LEFT JOIN dbo.[User] u2 ON s2.CustomerID = u2.IDSite
                             )
                            );

        DECLARE db_cursor2 CURSOR
        FOR
            SELECT  IDSite
            FROM    @BUIDs; 

        OPEN db_cursor2;   
        FETCH NEXT FROM db_cursor2 INTO @cSiteID; 
        WHILE @@FETCH_STATUS = 0
            BEGIN 

                SET @Total = ( SELECT   COUNT(SerialNo)
                               FROM     dbo.Assets
                               WHERE    SiteID = @cSiteID
                                        AND IDAssetType IN ( 1, 2, 3, 4, 6 )
                                        AND LastPostDateUTC > DATEADD(YEAR, -2,
                                                              GETUTCDATE())
                             );
                SET @Active = ( SELECT  COUNT(SerialNo)
                                FROM    dbo.Assets
                                WHERE   SiteID = @cSiteID
                                        AND IDAssetType IN ( 1, 2, 3, 4, 6 )
                                        AND LastPostDateUTC > DATEADD(DAY, -14,
                                                              GETUTCDATE())
                                        AND Retired = 0
                              );
	
		--find standing
                IF ( @Total != 0 )
                    BEGIN
                        SET @Online = CAST(( @Active * 100.0 / @Total ) AS DECIMAL(18,
                                                              2));		
                    END;
                ELSE
                    BEGIN
                        SET @Online = 0;
                            SET @sStatus = 'No Devices Online';
                    END;
		
                IF ( @Online = 0 ) 
                    BEGIN
                        SET @Stat = 'N';
                            SET @sStatus = 'No Devices Online ';
                    END;
                IF ( @Online BETWEEN 1 AND 59 )
                    BEGIN
                        SET @Stat = 'P';
                            SET @sStatus = '' + CAST(@Active AS VARCHAR)
                            + ' of ' + CAST(@Total AS VARCHAR);
                    END;
                IF ( @Online >= 60 )
                    BEGIN 
                        SET @Stat = 'L';
                            SET @sStatus = '' + CAST(@Active AS VARCHAR)
                            + ' of ' + CAST(@Total AS VARCHAR);
                    END;
                IF ( @Online >= 90 )
                    BEGIN 
                        SET @Stat = 'A';
                            SET @sStatus = '' + CAST(@Active AS VARCHAR)
                            + ' of ' + CAST(@Total AS VARCHAR);
                    END;
                BEGIN TRANSACTION;
                UPDATE  dbo.Sites
                SET     [Status] = @Stat ,
                        StatusReason = @sStatus ,
                        ActiveDevices = ISNULL(@Active, 0) ,
                        TotalDevices = ISNULL(@Total, 0) ,
                        OnlineDevicesPercentage = ISNULL(@Online / 100, 0.00)
                WHERE   IDSite = @cSiteID;	
                COMMIT;
			
				DECLARE @csite AS VARCHAR(50);
				SET @csite = 'Done with Site 2 ' + CAST(@cSiteID AS VARCHAR);
				RAISERROR( @csite,0,1) WITH NOWAIT;
				  FETCH NEXT FROM db_cursor2 INTO @cSiteID;
            END;
		  BEGIN TRANSACTION;
                UPDATE  dbo.Customers
                SET     [SitesOnline] = (SELECT ISNULL(COUNT(idsite),0) FROM dbo.sites WHERE Status IN ('A','L') AND customerid = cust.IDCustomer)
                FROM dbo.Customers AS Cust
                COMMIT;
    END;
       
		  
    CLOSE db_cursor2;   
    DEALLOCATE db_cursor2;

    SELECT  C.CustomerName ,
            s.SiteName ,
            --ST.TerritoryName ,
            --SC.BillingState AS State ,
            (s.SfCustomerAcctID + ' (' + s.SfAccountId + ') ')  AS SFDCCustAcctID ,
            --'https://na32.salesforce.com/' + s.SfAccountId
            --+ '?srPos=0&srKp=001' AS SFDCLink ,
            --ISNULL(SR.[Name ], SC.BillingState) AS Region ,
      
            ISNULL(s.LastLoginDateUTC,(SELECT TOP 1  Lastlogindateutc AS Lastlogindateutc FROM sites s WHERE CustomerID = c.idcustomer AND s.LastLoginDateUTC IS NOT null)) AS LastLoginByCustomer ,

			C.SitesOnline,
            s.Status AS StatusCode ,
            --s.StatusReason AS StatusReason ,
			s.IDsite,
			ISNULL(s.OnlineDevicesPercentage, 0.00
			) AS 'Online %' ,
        
            ( SELECT    COUNT(SerialNo)
              FROM      dbo.Assets
              WHERE     SiteID = s.IDSite
                        AND IDAssetType IN ( 1, 2, 3, 4, 6 )
                        AND LastPostDateUTC > DATEADD(DAY, -14, GETUTCDATE())
                        AND Retired = 0
            ) AS 'TotalOnline' ,
            ( SELECT    COUNT(SerialNo)
              FROM      dbo.Assets
              WHERE     SiteID = s.IDSite
                        AND IDAssetType IN ( 1, 2, 3, 4, 6 )
                        AND LastPostDateUTC > DATEADD(YEAR, -2, GETUTCDATE())
            ) AS 'TotalAssigned' ,
            ( SELECT    COUNT(SerialNo)
              FROM      dbo.Assets
              WHERE     SiteID = s.IDSite
                        AND DepartmentID <> ( SELECT    IDDepartment
                                              FROM      dbo.Departments
                                              WHERE     SiteID = s.IDSite
                                                        AND DefaultForSite = 1
                                            )
                        AND IDAssetType IN ( 1, 2, 3, 4, 6 )
                        AND LastPostDateUTC > DATEADD(YEAR, -2, GETUTCDATE())
            ) AS 'Assets with Department Assigned' ,
            ( SELECT    COUNT(SerialNo)
              FROM      dbo.Assets
              WHERE     SiteID = s.IDSite
                        AND Floor <> ''
                        AND Floor IS NOT NULL
                        AND IDAssetType IN ( 1, 2, 3, 4, 6 )
                        AND LastPostDateUTC > DATEADD(YEAR, -2, GETUTCDATE())
            ) AS 'Assets with Floor Assigned' ,
            ( SELECT    COUNT(SerialNo)
              FROM      dbo.Assets
              WHERE     SiteID = s.IDSite
                        AND Wing <> ''
                        AND Wing IS NOT NULL
                        AND IDAssetType IN ( 1, 2, 3, 4, 6 )
                        AND LastPostDateUTC > DATEADD(YEAR, -2, GETUTCDATE())
            ) AS 'Assets with Wing Assigned'
    FROM    dbo.Sites s  
            LEFT JOIN dbo.SalesTerritories AS ST ON s.SfAcctOwnerId = ST.SfAcctOwnerId
            LEFT JOIN dbo.SFCustomer AS SC ON s.IDSite = SC.SiteID
            LEFT JOIN dbo.SalesRegions AS SR ON SR.IDRegion = s.RegionID
            LEFT JOIN dbo.Customers AS C ON s.CustomerID = C.IDCustomer 
                --inner JOIN dbo.Assets ass ON ass.SiteID = s.IDSite
      --  GROUP BY ass.SiteID  
   WHERE s.IDSite NOT IN ( 1, 4, 28, 1125, 1128,1152 ) 
   ORDER BY C.CustomerName ,
            s.SiteName ,
            s.OnlineDevicesPercentage DESC;



--end cleanup 
--Update Sites set Enabled = 'A' where [Status] = 'C'
 


GO
