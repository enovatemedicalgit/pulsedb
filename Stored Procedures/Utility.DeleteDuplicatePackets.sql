SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [Utility].[DeleteDuplicatePackets] @SiteID INT = 1128
AS
	BEGIN
	WITH BandingNSa AS 
(
                SELECT 
                   ROW_NUMBER() OVER(PARTITION BY  DATEADD(Second, round(datediff(Second, '2010-01-01', CreatedDateUTC) / 60.0, 0) * 60, '2010-01-01')  ,DeviceSerialNumber,BatterySerialNumber, VoltageCell1, VoltageCell2, VoltageCell3 ORDER BY ROW_ID) AS OrdinalId,
                   ROW_ID
                FROM
                   dbo.NonSessionData_Alerts 
                WHERE
                   CreatedDateUTC > DATEADD(HOUR, -120, GETUTCDATE())
)
     DELETE
                FROM 
                   dbo.NonSessionData_Alerts 
                WHERE ROW_ID IN (SELECT BandingNSa.ROW_ID FROM BandingNSa WHERE BandingNSa.OrdinalId>1);

WITH BandingNS AS 
(
                SELECT 
                   ROW_NUMBER() OVER(PARTITION BY  DATEADD(Second, round(datediff(Second, '2010-01-01', CreatedDateUTC) / 60.0, 0) * 60, '2010-01-01')  ,DeviceSerialNumber,BatterySerialNumber, VoltageCell1, VoltageCell2, VoltageCell3 ORDER BY ROW_ID) AS OrdinalId,
                   ROW_ID
                FROM
                   dbo.NonSessionDataCurrent  where
                   CreatedDateUTC > DATEADD(HOUR, -120, GETUTCDATE())
)
     DELETE
                FROM 
                   dbo.NonSessionDataCurrent 
                WHERE ROW_ID IN (SELECT BandingNS.ROW_ID FROM BandingNS WHERE BandingNS.OrdinalId>1);


WITH BandingSa AS 
(
                SELECT 
                   ROW_NUMBER() OVER(PARTITION BY  DATEADD(Second, round(datediff(Second, '2010-01-01', CreatedDateUTC) / 60.0, 0) * 60, '2010-01-01')  ,DeviceSerialNumber,BatterySerialNumber, VoltageCell1, VoltageCell2, VoltageCell3 ORDER BY ROW_ID) AS OrdinalId,
                   ROW_ID
                FROM
                   dbo.SessionData_Alerts  
                WHERE
                   CreatedDateUTC > DATEADD(HOUR, -120, GETUTCDATE())
)
 DELETE
                FROM 
                   dbo.SessionData_Alerts
                WHERE 
                   ROW_ID IN (SELECT BandingSa.ROW_ID FROM BandingSa WHERE BandingSa.OrdinalId>1);
				   
WITH BandingS AS 
(
                SELECT 
                   ROW_NUMBER() OVER(PARTITION BY  DATEADD(Second, round(datediff(Second, '2010-01-01', CreatedDateUTC) / 60.0, 0) * 60, '2010-01-01')  ,DeviceSerialNumber,BatterySerialNumber, VoltageCell1, VoltageCell2, VoltageCell3 ORDER BY ROW_ID) AS OrdinalId,
                   ROW_ID
                FROM
                   dbo.SessionDataCurrent 
                WHERE
                   CreatedDateUTC > DATEADD(HOUR, -120, GETUTCDATE())
)
 DELETE
                FROM 
                   dbo.SessionDataCurrent
                WHERE 
                   ROW_ID IN (SELECT BandingS.ROW_ID FROM BandingS WHERE BandingS.OrdinalId>1);
END;
GO
