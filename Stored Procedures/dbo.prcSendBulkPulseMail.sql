SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[prcSendBulkPulseMail]
AS
    BEGIN

        DECLARE @PocEmail VARCHAR(70);
        DECLARE db_cursor CURSOR
        FOR
            SELECT  DISTINCT POCEmail
            FROM    dbo.SFPOCEmails WHERE POCEmail IS NOT null;

        OPEN db_cursor;   
        FETCH NEXT FROM db_cursor INTO @PocEmail;   
        WHILE @@FETCH_STATUS = 0
            BEGIN  
      
declare @Body as nvarchar(max)
declare @Subject as varchar(100) = 'Improvement to Pulse Contact Email'
DECLARE @AccID AS nVARCHAR(30);
DECLARE @gifURL AS nVARCHAR(50);

SET @AccID = (SELECT TOP 1 AccountID FROM dbo.SFPOCEmails WHERE POCEmail = @PocEmail)
SET @gifURL = CONCAT(N'http://pulse.myenovate.com/tracker.php?alertrowid=' , ISNULL((SELECT TOP 1 AccountID FROM dbo.SFPOCEmails WHERE POCEmail = @PocEmail),@PocEmail));
set @Body = '<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml" xmlns="http://www.w3.org/TR/REC-html40"><head><meta http-equiv=Content-Type content="text/html; charset=us-ascii"><meta name=Generator content="Microsoft Word 15 (filtered medium)"><style><!--
/* Font Definitions */
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0in;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
a:link, span.MsoHyperlink
	{mso-style-priority:99;
	color:#0563C1;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-priority:99;
	color:#954F72;
	text-decoration:underline;}
span.EmailStyle17
	{mso-style-type:personal-compose;
	font-family:"Calibri",sans-serif;
	color:windowtext;}
.MsoChpDefault
	{mso-style-type:export-only;
	font-family:"Calibri",sans-serif;}
@page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
--></style><!--[if gte mso 9]><xml>
<o:shapedefaults v:ext="edit" spidmax="1026" />
</xml><![endif]--><!--[if gte mso 9]><xml>
<o:shapelayout v:ext="edit">
<o:idmap v:ext="edit" data="1" />
</o:shapelayout></xml><![endif]--><img src="' + CONCAT(@gifURL,@AccID,' EMAILADDR',@PocEmail)  + '" alt="" width="1" height="1" border="0" /></head><body lang=EN-US link="#0563C1" vlink="#954F72"><div class=WordSection1><p class=MsoNormal>Dear Pulse User, <o:p></o:p></p><p class=MsoNormal>In an effort to provide a more user friendly process, we strongly recommend using an email group or distribution list for communication to your facilities from Pulse. <o:p></o:p></p><p class=MsoNormal>You will need a contact email address for each facility in your hospital system that has devices listed in Pulse.<o:p></o:p></p><p class=MsoNormal><o:p>&nbsp;</o:p></p><p class=MsoNormal>This change will allow you to keep your internal contact(s) for Pulse communication up to date, and give you control over who receives notifications at each facility.<o:p></o:p></p><p class=MsoNormal> &nbsp;<o:p></o:p></p><p class=MsoNormal>This change will prevent issues such as Pulse notifications being delivered to outdated or incorrect email addresses or facilities and allow us to support you much more efficiently. <o:p></o:p></p><p class=MsoNormal>You can manage this contact address, and see the email address you have currently listed, through &nbsp;the &#8220;Manage Users&#8221; screen in Pulse.<o:p></o:p></p><p class=MsoNormal><o:p>&nbsp;</o:p></p><p class=MsoNormal>Thank you in advance,<o:p></o:p></p><p class=MsoNormal><span style=''color:#1F4E79''> Enovate Medical Technical Services Team </span><span style=''color:#262626''></span><o:p></o:p></p><p class=MsoNormal><o:p>&nbsp;</o:p></p></div></body></html>'

exec usp_SendTextEmail 'enovatemedical-com.mail.protection.outlook.com', 'Pulse Notification <Pulse@EnovateMedical.com>',
 @PocEmail, @Subject , @Body, '', '', 0 ,25, 'Pulse Support <arlow.farrell@enovatemedical.com>';

                 
				       FETCH NEXT FROM db_cursor INTO @PocEmail;
                    END;
          
				 CLOSE db_cursor;   
        DEALLOCATE db_cursor;

            END;   

GO
