SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
	CREATE PROCEDURE [dbo].[prcDashGetdevicePacketsPerHourSummary_Alt] ( @SiteID INT = 238)
	
	AS

	SELECT
		 SUM(s.PacketCount) AS PacketCount,
		 DATEDIFF(HOUR,s.EndDateUTC,GETUTCDATE()) AS PacketHour,
		 a.SiteId,
		 a.DepartmentId
	FROM 
		dbo.Sessions s
	INNER JOIN
		dbo.Assets a
	ON 
		a.SerialNo = s.DeviceSerialNumber
	WHERE
		DATEDIFF(HOUR,S.EndDateUTC,GETUTCDATE()) <= 24
		AND
		a.SiteID = @SiteId
	GROUP BY
		a.SiteId,
		a.DepartmentID,
		DATEDIFF(HOUR,s.EndDateUTC,GETUTCDATE())
	ORDER BY
		DATEDIFF(HOUR,s.EndDateUTC,GETUTCDATE()) DESC
		
GO
