SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[prcDashBatteryHealthCalendar]

 AS
 


DECLARE @SerialNo VARCHAR(50) = '311831314050007'

 ;WITH DateCTE as
 (
 SELECT cast(GETUTCDATE() as date) as CalendarDate,
        1 AS DateOrdinal
 
    UNION ALL
 
 SELECT dateadd(day , 1, CalendarDate) AS CalendarDate, DateOrdinal+1 AS DateOrdinal
 FROM DateCTE
 WHERE dateadd (day, 1, CalendarDate) < DATEADD(DAY,180,GETUTCDATE())
 ), 
 BatteryStatus AS 
 (
 SELECT          ass.SerialNo + ' - ' + CAST(ISNULL(NULLIF(dbo.fnGetBatteryCapacity_Int(ass.SerialNo,
                                                              ass.FullChargeCapacity),
                                          0), 0) AS VARCHAR(50)) AS [Title] ,
			  ISNULL(NULLIF(dbo.fnGetBatteryCapacity_Int(ass.SerialNo,
                                                              ass.FullChargeCapacity),0), 0) AS [Capacity Health],
												  ass.SerialNo,
												  ass.FullChargeCapacity,
												  b.LossPerCycle,
												  b.CyclesPerDay,
												  l.CalcAH
                  FROM      dbo.Assets ass
                            LEFT JOIN dbo.Sites s ON s.IDSite = ass.SiteID
                            LEFT JOIN dbo.Customers cus ON ass.SiteID = cus.IDCustomer
					   LEFT JOIN dbo.BatteryCapacityTrending b ON ass.SerialNo = b.BatterySerialNumber
					   LEFT JOIN dbo.BatteryPartList l ON l.PartNumber = LEFT(ass.SerialNo,7)
                  WHERE     
					   ass.SerialNo = @SerialNo
 )


SELECT Title
	,LossPerCycle
	,(DateOrdinal * CyclesPerDay) * LossPerCycle AS CapacityLoss
	,dbo.fnGetBatteryCapacity_Int(SerialNo, FullChargeCapacity - ((DateOrdinal * CyclesPerDay) * LossPerCycle)) AS EstimatedCapacity
	,DATEADD(MINUTE, 1, CONVERT(DATETIME2, CalendarDate)) AS [Start]
	,DATEADD(MINUTE, - 1, DATEADD(DAY, 1, CONVERT(DATETIME2, CalendarDate))) AS [End]
	,Datename(Month,CalendarDate) as MonthName,  Datename(WEEK,CalendarDate) as WeekNumber, CalendarDate,Day(CalendarDate) as Day, Month(CalendarDate) as Month, Left(DATENAME(WEEKDAY,CalendarDate),3) as DateName
FROM BatteryStatus
CROSS JOIN DateCTE
ORDER BY CalendarDate ASC

OPTION (MAXRECURSION 180);
GO
