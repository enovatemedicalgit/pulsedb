SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================

-- =============================================
CREATE PROCEDURE [dbo].[spAssetSiteDepartmentAutoAssociation]
AS
    BEGIN
        SET NOCOUNT ON;
	--delete temp table so it's re-runnable in testing.
        IF OBJECT_ID('tempdb..#tempNSDSelect') IS NOT NULL
            BEGIN
                DROP TABLE #tempNSDSelect;
            END; 

 
        CREATE TABLE #tempNSDSelect
            (
              BatterySerialNumber VARCHAR(50) ,
              DeviceSerialNumber VARCHAR(50) ,
              APMac VARCHAR(50) ,
              CreatedDateUTC DATETIME ,
              SourceIPAddress VARCHAR(20) ,
              IP VARCHAR(20)
            );
 
        INSERT  INTO #tempNSDSelect
                ( BatterySerialNumber ,
                  DeviceSerialNumber ,
                  APMac ,
                  CreatedDateUTC ,
                  SourceIPAddress ,
                  IP
                )
                SELECT  nsd.BatterySerialNumber ,
                        nsd.DeviceSerialNumber ,
                        nsd.APMAC ,
                        nsd.CreatedDateUTC ,
                        nsd.SourceIPAddress AS SourceIP ,
                        nsd.IP AS IP
                FROM    dbo.viewAllSessionData nsd WITH ( NOLOCK )
                WHERE   nsd.CreatedDateUTC > DATEADD(HOUR, -6, GETDATE());
            WITH    CTE
                      AS ( SELECT   * ,
                                    RN = ROW_NUMBER() OVER ( PARTITION BY DeviceSerialNumber,
                                                             BatterySerialNumber ORDER BY CreatedDateUTC DESC )
                           FROM     #tempNSDSelect
                         )
            DELETE  FROM CTE
            WHERE   CTE.RN > 1; 


	--Assign Actual Dept ID (AccessPointID) for Assets 
        UPDATE  dbo.Assets
        SET     --  assets.SiteID = isnull((select top 1 siteID from SiteIPSubnet sip where SourceIPAddressBase =  (left(t.SourceIPAddress, len(t.SourceIPAddress) - charindex('.', reverse(t.SourceIPAddress)))) and LANIPAddressBase =  (left(t.IP, len(t.IP) - charindex('.', reverse(t.IP)))) and sip.FacilityReliabilityRating > 70),a.SiteID),
                AccessPointId = ISNULL(( SELECT TOP 1
                                                        ap.ROW_ID
                                                FROM    dbo.AccessPoint ap
                                                WHERE   ap.MACAddress = t.APMAC
                                                        AND ap.SiteID = a.SiteID
                                                ORDER BY ap.DeviceCount DESC
                                              ), a.AccessPointId)
        FROM    dbo.Assets a
                INNER JOIN #tempNSDSelect t ON a.SerialNo = t.DeviceSerialNumber
                                               AND t.APMac IS NOT NULL;

 	--Assign Original Dept ID for Assets that are set to unallocated Dept ID
        UPDATE  dbo.Assets
        SET     DepartmentID = ISNULL(( SELECT TOP 1
                                                        ap.DepartmentID
                                               FROM     dbo.AccessPoint ap
                                               WHERE    ap.MACAddress = t.APMAC
                                                        AND ap.SiteID = a.SiteID
                                               ORDER BY ap.DeviceCount DESC
                                             ), a.DepartmentID) ,
                AccessPointId = ISNULL(( SELECT TOP 1
                                                        ap.ROW_ID
                                                FROM    dbo.AccessPoint ap
                                                WHERE   ap.MACAddress = t.APMAC
                                                        AND ap.SiteID = a.SiteID
                                                ORDER BY ap.DeviceCount DESC
                                              ), a.AccessPointId)
        FROM    dbo.Assets a
                INNER JOIN #tempNSDSelect t ON a.SerialNo = t.DeviceSerialNumber
                INNER JOIN dbo.Departments dep ON a.DepartmentID = dep.IDDepartment
        WHERE   dep.DefaultForSite = 1
                OR dep.IDDepartment = 0
                AND t.APMac IS NOT NULL;


  	--Assign Actual Dept ID (AccessPointID) for Assets that are set to unallocated
        UPDATE  dbo.Assets
        SET     --  assets.SiteID = isnull((select top 1 siteID from SiteIPSubnet sip where SourceIPAddressBase =  (left(t.SourceIPAddress, len(t.SourceIPAddress) - charindex('.', reverse(t.SourceIPAddress)))) and LANIPAddressBase =  (left(t.IP, len(t.IP) - charindex('.', reverse(t.IP)))) and sip.FacilityReliabilityRating > 70),a.SiteID),
                AccessPointId = ISNULL(( SELECT TOP 1
                                                        ap.ROW_ID
                                                FROM    dbo.AccessPoint ap
                                                WHERE   ap.MACAddress = t.APMAC
                                                        AND ap.SiteID = a.SiteID
                                                ORDER BY ap.DeviceCount DESC
                                              ), a.AccessPointId)
        FROM    dbo.Assets a
                INNER JOIN #tempNSDSelect t ON a.SerialNo = t.BatterySerialNumber
                                               AND t.APMac IS NOT NULL;

 	--Assign Original Dept ID for Assets that are set to unallocated Dept ID
        UPDATE  dbo.Assets
        SET     DepartmentID = ISNULL(( SELECT TOP 1
                                                        ap.DepartmentID
                                               FROM     dbo.AccessPoint ap
                                               WHERE    ap.MACAddress = t.APMAC
                                                        AND ap.SiteID = a.SiteID
                                               ORDER BY ap.DeviceCount DESC
                                             ), a.DepartmentID) ,
                AccessPointId = ISNULL(( SELECT TOP 1
                                                        ap.ROW_ID
                                                FROM    dbo.AccessPoint ap
                                                WHERE   ap.MACAddress = t.APMAC
                                                        AND ap.SiteID = a.SiteID
                                                ORDER BY ap.DeviceCount DESC
                                              ), a.AccessPointId)
        FROM    dbo.Assets a
                INNER JOIN #tempNSDSelect t ON a.SerialNo = t.BatterySerialNumber
                INNER JOIN dbo.Departments dep ON a.DepartmentID = dep.IDDepartment
        WHERE   dep.DefaultForSite = 1
                OR dep.IDDepartment = 0
                AND t.APMac IS NOT NULL;


    END;
GO
