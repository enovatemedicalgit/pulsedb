SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[Fetch_SummaryWorkstations] ( @SiteID AS INT = 657 )
AS
    WITH    WorkstationDetails
              AS ( SELECT   a.SerialNo AS DeviceSerialNumber ,
                            a.SiteID ,
                            a.DepartmentID ,
                            COALESCE(a.SiteFloorID, 0) SiteFloorId ,
                            COALESCE(a.SiteWingID, 0) SiteWingId ,
                            ast.Description AS AssetStatus ,
                            CONVERT(INT, a.IsActive) AS IsActive ,
                            CASE WHEN a.LastPostDateUTC > DATEADD(hh, -2,
                                                              GETDATE())
                                 THEN 1
                                 ELSE 0
                            END AS IsInService ,
                            CASE a.AssetStatusID
                              WHEN 3 THEN 1
                              ELSE 0
                            END AS IsNotComissioned ,
                            CASE a.AssetStatusID
                              WHEN 1 THEN 1
                              ELSE 0
                            END AS IsAvailable ,
                            CASE WHEN a.LastPostDateUTC BETWEEN DATEADD(d, -7,
                                                              GETDATE())
                                                        AND   DATEADD(d, -2,
                                                              GETDATE())
                                 THEN 1
                                 ELSE 0
                            END AS IsDormant
                   FROM     dbo.Assets a
                            INNER JOIN dbo.AssetType at ON at.IDAssetType = a.IDAssetType
                            INNER JOIN dbo.AssetCategory ac ON ac.IDAssetCategory = at.AssetCategoryId
                            INNER JOIN dbo.AssetStatus ast ON ast.IDAssetStatus = a.AssetStatusID
                   WHERE    ac.Description = 'Workstation'
                 ),
            WorkstationSessions
              AS ( SELECT   s.SiteID ,
                            a.SerialNo AS DeviceSerialNumber ,
                            COALESCE(a.DepartmentID, 0) AS DepartmentID ,
                            COALESCE(a.SiteFloorID, 0) AS SiteFloorId ,
                            COALESCE(a.SiteWingID, 0) AS SiteWingId ,
                            COUNT(s.ROW_ID) AS SessionCount ,
                            SUM(CASE WHEN CONVERT(DATE, s.StartDateUTC) = CONVERT(DATE, DATEADD(dd,
                                                              -1, GETDATE()))
                                          AND s.StartChargeLevel < 90 THEN 1
                                     ELSE 0
                                END) AS LoChargeInsertsCount ,
                            SUM(CASE WHEN CONVERT(DATE, s.EndDateUTC) = CONVERT(DATE, DATEADD(dd,
                                                              -1, GETDATE()))
                                          AND s.EndChargeLevel > 10 THEN 1
                                     ELSE 0
                                END) AS HiChargeRemovalsCount ,
                            AVG(COALESCE(s.RunRate, 0)) AS AvgRunRate ,
					   AVG(COALESCE(s.EstimatedPCUtilization,0)) AS AvgEstUtilization,
                            AVG(COALESCE(s.AvgAmpDraw, 0)) AS AvgAmpDraw ,
                            MAX(COALESCE(s.HiAmpDraw, 0)) AS MaxAmpDraw ,
                            MIN(COALESCE(s.LowAmpDraw, 0)) AS MinAmpDraw ,
                            AVG(( COALESCE(s.StartChargeLevel, 1)
                                  + COALESCE(s.EndChargeLevel, 1) ) / 2) AS AvgChargeLevel ,
                            SUM(CASE WHEN CONVERT(DATE, s.StartDateUTC) = CONVERT(DATE, DATEADD(dd,
                                                              -1, GETDATE()))
                                          AND s.EndChargeLevel < 90 THEN 1
                                     ELSE 0
                                END) AS LoChargeRemovalsCount ,
                            SUM(CASE WHEN CONVERT(DATE, s.StartDateUTC) = CONVERT(DATE, DATEADD(dd,
                                                              -1, GETDATE()))
                                          AND s.StartChargeLevel > 90 THEN 1
                                     ELSE 0
                                END) AS HiChargeInsertsCount ,
                            AVG(COALESCE(s.AvgSignalQuality, 0)) AS AvgSignalQuality ,
                            AVG(COALESCE(s.RemainingCapacity, 0)) AS AvgRemainingCapacity ,
                            SUM(COALESCE(s.PacketCount, 0)) AS TotalSessionPackets
                   FROM     dbo.Sessions s
                            INNER JOIN dbo.Assets a ON s.DeviceSerialNumber = a.SerialNo
                            INNER JOIN dbo.AssetType at ON at.IDAssetType = a.IDAssetType
                            INNER JOIN dbo.AssetCategory ac ON ac.IDAssetCategory = at.AssetCategoryId
                   WHERE    s.EndDateUTC >= DATEADD(dd, -1, GETUTCDATE())
                            AND ac.Description = 'Workstation'
                            AND a.OutOfService = 0
                            AND a.Retired = 0
                   GROUP BY s.SiteID ,
                            a.DepartmentID ,
                            a.SerialNo ,
                            COALESCE(a.SiteFloorID, 0) ,
                            COALESCE(a.SiteWingID, 0)
                 )
        SELECT  wd.DeviceSerialNumber ,
                wd.SiteID ,
                wd.DepartmentID ,
                wd.SiteFloorId ,
                wd.SiteWingId ,
                wd.AssetStatus ,
                wd.IsActive ,
                wd.IsInService ,
                wd.IsNotComissioned ,
                wd.IsAvailable ,
                wd.IsDormant ,
                COALESCE(ws.SessionCount, 0) AS SessionCount ,
                COALESCE(ws.LoChargeInsertsCount, 0) LoChargeInsertCount ,
                COALESCE(ws.HiChargeRemovalsCount, 0) HiChargeRemovalCount ,
                COALESCE(ws.AvgRunRate, 0) AvgRunRate ,
			 COALESCE(ws.AvgEstUtilization,0),
                COALESCE(ws.AvgAmpDraw, 0) AvgAmpDraw ,
                COALESCE(ws.MaxAmpDraw, 0) MaxAmpDraw ,
                COALESCE(ws.MinAmpDraw, 0) MinAmpDraw ,
                COALESCE(ws.AvgChargeLevel, 0) AvgChargeLevel ,
                COALESCE(ws.LoChargeRemovalsCount, 0) LoChargeRemovalsCount ,
                COALESCE(ws.HiChargeInsertsCount, 0) HiChargeInsertsCount ,
                COALESCE(ws.AvgSignalQuality, 0) AvgSignalQuality ,
                COALESCE(ws.AvgRemainingCapacity, 0) AvgRemainingCapacity ,
                COALESCE(ws.TotalSessionPackets, 0) TotalSessionPackets
        FROM    WorkstationDetails wd
                LEFT OUTER JOIN WorkstationSessions ws ON wd.DeviceSerialNumber = ws.DeviceSerialNumber;

GO
