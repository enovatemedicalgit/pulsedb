SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[prcGetUsersBySite]
(
	@SiteID int
)
AS
BEGIN
	
	SET NOCOUNT ON;


		 SELECT 
			u.IDUser as 'UserID',
            u.UserName ,
            u.FirstName ,
            u.LastName ,
            u.Title ,
        
            u.PrimaryPhone ,
       
            u.Notes ,
        
            u.LastLoginDateUTC ,
       
            u.ProfileImage ,
     
            u.LoginCount, 
		 case when coalesce(u.CreatedUserID, 0) = 0 then '' 
			else coalesce(u2.FirstName, '') + ' ' + coalesce(u2.LastName, '') end as CreatedUserFullName,
			UO.OPtIONVALUE AS 'Edit Assets',U1.OptionValue AS 'LoginEnabled',U3.OptionValue AS 'Edit Users'
		 from [User] u 
		 LEFT JOIN dbo.UserOptions AS UO ON UO.User_ROW_ID = u.IDUser AND uo.OptionKey = 'CAN_MAINTAIN_DEVICES'
		  LEFT JOIN dbo.UserOptions AS U1 ON U1.User_ROW_ID = u.IDUser AND u1.OptionKey = 'CAN_LOGIN'
		   LEFT JOIN dbo.UserOptions AS U3 ON U3.User_ROW_ID = u.IDUser AND u3.OptionKey = 'CAN_MAINTAIN_USERS'
		 left join [User] u2 on u.CreatedUserID = u2.IDUser 
		 where u.idsite = @siteID 
		 order by u.FirstName, u.LastName
		 
		 
END

GO
