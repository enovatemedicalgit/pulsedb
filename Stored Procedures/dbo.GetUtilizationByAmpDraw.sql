SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/****** Object:  StoredProcedure [dbo].[GetUtilizationByPackets]    Script Date: 8/22/2016 1:27:41 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO


CREATE PROCEDURE [dbo].[GetUtilizationByAmpDraw]
( @SiteId INT = 693)
AS

WITH IncludedDevices as
(
    Select SerialNo as DeviceSerialNumber From Assets
    Where 
    IdAssetType IN (6,3,1)
    AND
    SiteId = @SiteId
),
BandsByDevice AS
(SELECT 
    DeviceSerialNumber,
    MinCurrent,
    (MinCurrent + (CurrentDifferential*.2)) AS Band20,
    (MinCurrent + (CurrentDifferential*.4)) AS Band40,
    (MinCurrent + (CurrentDifferential*.6)) AS Band60,
    (MinCurrent + (CurrentDifferential*.8)) AS Band80,
    MaxCurrent
FROM        
    (
    SELECT 
	    DeviceSerialNumber,
	    MAX(MinCurrent) AS MinCurrent,
	    MAX(MaxCurrent) AS MaxCurrent,
	    MAX(MaxCurrent) - MAX(MinCurrent) AS CurrentDifferential
    FROM
	   (
	   SELECT 
		  DeviceSerialNumber, 
		  MIN(DCUnit1aCurrent) AS MinCurrent,
		  MAX(DCUnit1aCurrent) AS MaxCurrent,
		  '1a' AS Channel

	   FROM 
		  dbo.SessionDataCurrent
	   WHERE
		  DATEDIFF(DAY,Sessiondatacurrent.CreatedDateUTC, GETUTCDATE()) <= 7 AND
		  DCUnit1aCurrent >100 AND
		  DeviceSerialNumber IN (Select DeviceSerialNumber from IncludedDevices)
	   GROUP BY
		  DeviceSerialNumber

	   UNION ALL

	   SELECT 
		  DeviceSerialNumber, 
		  MIN(DCUnit1bCurrent) AS MinCurrent,
		  MAX(DCUnit1BCurrent) AS MaxCurrent,
		  '1b' AS Channel
	   FROM 
		  dbo.SessionDataCurrent
	   WHERE
		  DATEDIFF(DAY,Sessiondatacurrent.CreatedDateUTC, GETUTCDATE()) <= 7 AND
		  DCUnit1BCurrent >100 AND
		  DeviceSerialNumber IN (Select DeviceSerialNumber from IncludedDevices)
	   GROUP BY
		  DeviceSerialNumber

	   UNION ALL

	   SELECT 
		  DeviceSerialNumber, 
		  MIN(DCUnit2aCurrent) AS MinCurrent,
		  MAX(DCUnit2aCurrent) AS MaxCurrent,
		  '2a' AS Channel
	   FROM 
		  dbo.SessionDataCurrent
	   WHERE
		  DATEDIFF(DAY,Sessiondatacurrent.CreatedDateUTC, GETUTCDATE()) <= 7 AND
		  DCUnit2aCurrent >100 AND
		  DeviceSerialNumber IN (Select DeviceSerialNumber from IncludedDevices)
	   GROUP BY
		  DeviceSerialNumber

	   UNION ALL

	   SELECT 
		  DeviceSerialNumber, 
		  MIN(DCUnit2bCurrent) AS MinCurrent,
		  MAX(DCUnit2bCurrent) AS MaxCurrent,
		  '2b' AS Channel
	   FROM 
		  dbo.SessionDataCurrent
	   WHERE
		  DATEDIFF(DAY,Sessiondatacurrent.CreatedDateUTC, GETUTCDATE()) <= 7 AND
		  DCUnit2bCurrent >100 AND
		  DeviceSerialNumber IN (Select DeviceSerialNumber from IncludedDevices)
	   GROUP BY
		  DeviceSerialNumber
	   ) AS u

    GROUP BY 
	   DeviceSerialNumber
    ) b
)
, PacketBandsByDevice AS
(
   SELECT 
    DeviceSerialNumber,
    DATEADD(HOUR,DATEDIFF(HOUR,GETUTCDATE(),GETDATE()), CreatedDateUTC) CreatedDateLocal,
    CurrentBand
    FROM	  
    (
    SELECT  
	   DeviceSerialNumber,
	   CreatedDateUTC,
	   0 AS CurrentBand
    FROM 
	   SessionDataCurrent sd
    WHERE
	   EXISTS (SELECT 1 
			 FROM BandsByDevice b 
			 WHERE b.DeviceSerialNumber = sd.DeviceSerialNumber 
			 AND (
				    sd.DCUnit1ACurrent BETWEEN b.MinCurrent AND b.Band20
				    OR
				    sd.DCUnit1bCurrent BETWEEN b.MinCurrent AND b.Band20
				    OR
				    sd.DCUnit2ACurrent BETWEEN b.MinCurrent AND b.Band20
				    OR
				    sd.DCUnit2bCurrent BETWEEN b.MinCurrent AND b.Band20				   
				)
			 )
    UNION ALL
    SELECT  
	   DeviceSerialNumber,
	   CreatedDateUTC,
	   20 AS CurrentBand
    FROM 
	   SessionDataCurrent sd
    WHERE
	   EXISTS (SELECT 1 
			 FROM BandsByDevice b 
			 WHERE b.DeviceSerialNumber = sd.DeviceSerialNumber 
			 AND (
				    sd.DCUnit1ACurrent BETWEEN b.Band20 +1 AND b.Band40
				    OR
				    sd.DCUnit1bCurrent BETWEEN b.Band20 +1 AND b.Band40
				    OR
				    sd.DCUnit2ACurrent BETWEEN b.Band20 +1 AND b.Band40
				    OR
				    sd.DCUnit2bCurrent BETWEEN b.Band20 +1 AND b.Band40
				)
			 )
    UNION ALL
    SELECT  
	   DeviceSerialNumber,
	   CreatedDateUTC,
	   40 AS CurrentBand
    FROM 
	   SessionDataCurrent sd
    WHERE
	   EXISTS (SELECT 1 
			 FROM BandsByDevice b 
			 WHERE b.DeviceSerialNumber = sd.DeviceSerialNumber 
			 AND (
				    sd.DCUnit1ACurrent BETWEEN b.Band40 +1 AND b.Band60
				    OR
				    sd.DCUnit1bCurrent BETWEEN b.Band40 +1 AND b.Band60
				    OR
				    sd.DCUnit2ACurrent BETWEEN b.Band40 +1 AND b.Band60
				    OR
				    sd.DCUnit2bCurrent BETWEEN b.Band40 +1 AND b.Band60
				)
			 )
    UNION ALL
    SELECT  
	   DeviceSerialNumber,
	   CreatedDateUTC,
	   60 AS CurrentBand
    FROM 
	   SessionDataCurrent sd
    WHERE
	   EXISTS (SELECT 1 
			 FROM BandsByDevice b 
			 WHERE b.DeviceSerialNumber = sd.DeviceSerialNumber 
			 AND (
				    sd.DCUnit1ACurrent BETWEEN b.Band60 +1 AND b.Band80
				    OR
				    sd.DCUnit1bCurrent BETWEEN b.Band60 +1 AND b.Band80
				    OR
				    sd.DCUnit2ACurrent BETWEEN b.Band60 +1 AND b.Band80
				    OR
				    sd.DCUnit2bCurrent BETWEEN b.Band60 +1 AND b.Band80
				)
			 )
    UNION ALL
    SELECT  
	   DeviceSerialNumber,
	   CreatedDateUTC,
	   80 AS CurrentBand
    FROM 
	   SessionDataCurrent sd
    WHERE
	   EXISTS (SELECT 1 
			 FROM BandsByDevice b 
			 WHERE b.DeviceSerialNumber = sd.DeviceSerialNumber 
			 AND (
				    sd.DCUnit1ACurrent BETWEEN b.Band80 +1 AND b.MaxCurrent
				    OR
				    sd.DCUnit1bCurrent BETWEEN b.Band80 +1 AND b.MaxCurrent
				    OR
				    sd.DCUnit2ACurrent BETWEEN b.Band80 +1 AND b.MaxCurrent
				    OR
				    sd.DCUnit2bCurrent BETWEEN b.Band80 +1 AND b.MaxCurrent
				)
			 )
	)AS outerUnion
	WHERE
	   DATEDIFF(DAY,CreatedDateUTC, GETUTCDATE()) <= 2
)
, DateSequence( UBound ) AS
(
    SELECT GETDATE() AS UBound
        UNION ALL
    SELECT DATEADD(MINUTE,-30,UBound) 
        FROM DateSequence
        WHERE Ubound > DATEADD(d,-14,GETDATE())
)
, HalfHourSlices AS
(
    SELECT ROW_NUMBER() OVER(ORDER BY UBound DESC) AS Recency, DATEADD(MINUTE,-30,UBound) AS LBound, UBound FROM DateSequence 
)
, BandCompare AS
(
    SELECT
	   b.DeviceSerialNumber,
	   SUM(ActivePackets) AS ActivePackets, 
	   SUM(InactivePackets) AS InactivePackets,
	   StartTime
    From
    (
    SELECT
	   DeviceSerialNumber,
	   COUNT(CreatedDateLocal) AS ActivePackets,
	   0 AS InactivePackets,
	   LBound AS StartTime
    FROM
	PacketBandsByDevice
	JOIN
	HalfHourSlices ON PacketBandsByDevice.CreatedDateLocal BETWEEN HalfHourSlices.Lbound AND HalfHourSlices.UBound
    WHERE CurrentBand >=40
    GROUP BY
	   DeviceSerialNumber,
	   lBound
    UNION ALL
    SELECT
	   DeviceSerialNumber,
	   0 AS ActivePackets,
	   COUNT(CreatedDateLocal) AS InctivePackets,
	   lBound AS StartTime
    FROM
	PacketBandsByDevice
	JOIN
	HalfHourSlices ON PacketBandsByDevice.CreatedDateLocal BETWEEN HalfHourSlices.Lbound AND HalfHourSlices.UBound
    WHERE CurrentBand <40
    GROUP BY
	   DeviceSerialNumber,
	   LBound
    ) b
    GROUP BY
	   b.DeviceSerialNumber,
	   b.StartTime
)
, Draft AS 
(
SELECT
    DeviceSerialNumber,
    ActivePackets,
    InactivePackets,
    CASE
	   WHEN CAST(ActivePackets AS FLOAT)/CAST((ActivePackets + InactivePackets) AS FLOAT) >= .5 THEN
		  1
	   ELSE
		  0
	   END AS IsUtilized,
    h.LBound as StartTime,
    Recency
FROM
     HalfHourSlices h
	INNER JOIN
     BandCompare b
	ON 
	h.LBound = b.StartTime
)


SELECT     
    SiteId,
    DepartmentId,
    DeviceSerialNumber,
    ActivePackets,
    InactivePackets,
    IsUtilized,
    StartTime,
    Recency 
FROM Draft INNER JOIN Assets on SerialNo = DeviceSerialNumber

OPTION (MAXRECURSION 1400)






GO
