SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [Utility].[Fetch_SummaryActiveAssets]
AS
    DECLARE @temp TABLE
        (
          RowId INT PRIMARY KEY
                    IDENTITY(1, 1) ,
          ForeignKeyName NVARCHAR(200) ,
          ForeignKeyTableSchema NVARCHAR(200) ,
          ForeignKeyTableName NVARCHAR(200) ,
          ForeignKeyColumnName NVARCHAR(200) ,
          PrimaryKeyName NVARCHAR(200) ,
          PrimaryKeyTableSchema NVARCHAR(200) ,
          PrimaryKeyTableName NVARCHAR(200) ,
          PrimaryKeyColumnName NVARCHAR(200)
        );
    INSERT  INTO @temp
            ( ForeignKeyName ,
              ForeignKeyTableSchema ,
              ForeignKeyTableName ,
              ForeignKeyColumnName
            )
            SELECT  CU.CONSTRAINT_NAME ,
                    CU.TABLE_SCHEMA ,
                    CU.TABLE_NAME ,
                    CU.COLUMN_NAME
            FROM    INFORMATION_SCHEMA.KEY_COLUMN_USAGE CU
                    INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS TC ON CU.CONSTRAINT_NAME = TC.CONSTRAINT_NAME
            WHERE   TC.CONSTRAINT_TYPE = 'FOREIGN KEY';
    UPDATE  @temp
    SET     PrimaryKeyName = UNIQUE_CONSTRAINT_NAME
    FROM    @temp T
            INNER JOIN INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS RC ON T.ForeignKeyName = RC.CONSTRAINT_NAME;
    UPDATE  @temp
    SET     PrimaryKeyTableSchema = TABLE_SCHEMA ,
            PrimaryKeyTableName = TABLE_NAME
    FROM    @temp T
            INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS TC ON T.PrimaryKeyName = TC.CONSTRAINT_NAME;
    UPDATE  @temp
    SET     PrimaryKeyColumnName = COLUMN_NAME
    FROM    @temp T
            INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE KCU ON T.PrimaryKeyName = KCU.CONSTRAINT_NAME;
    SELECT  *
    FROM    @temp;

----ADDING CONSTRAINT SCRIPT:
--SELECT
--'ALTER TABLE ' + ForeignKeyTableSchema + '.' 
--+ ForeignKeyTableName + 
--' ADD CONSTRAINT ' + ForeignKeyName + 
--' FOREIGN KEY(' + ForeignKeyColumnName + ') REFERENCES ' + PrimaryKeyTableSchema + '.' + PrimaryKeyTableName + 
--'(' + PrimaryKeyColumnName + ') GO'
--FROM
--@temp
--GO
GO
