SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcErrorLogSelect] @IDError INT
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  

    BEGIN TRAN;

    SELECT  [IDError] ,
            [Description]
    FROM    dbo.ErrorLog
    WHERE   ( [IDError] = @IDError
              OR @IDError IS NULL
            ); 

    COMMIT;



GO
