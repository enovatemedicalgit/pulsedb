SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Arlow Farrell
-- Create date: 2012-08-09
-- Modified:    2012-11-19 by Arlow (if it works properly) and Arlow (if it's broken)
-- Reason:      Inactive devices should not be moved automatically
--
-- Description:	Rewritten to use RadarBUID
--              to better assign the BusinessUnitID
--              for batteries until we can
--              get reliable data from MAS then
--              update the OwnerBUID using
--              MAS data.
--              
--              Once we automate a process to 
--              update the OwnerBUID using MAS,
--              this job may be disabled.
--
--              NOTE: The previous version
--              of this proc was archived 8/9/12
--              by Arlow Farrell

--=====================================================
-- Arlow Farrell - 5/28/2013
-- Modification: Rewritten (previous saved in 
-- comment at bottom) batteries are getting reassinged
-- when they shouldn't and then not be auto discovered
-- In addition to the DT assigning the RadarBUID this
-- sp is called via an Agent job every 15 minutes to 
-- physically move the device assignment depending on 
-- matches with the RadarBUID. Now... network info is
-- on used to determine if the device is reporting within
-- the LAN and then set, then if not found the assignment
-- of the reporting device (batteries can't send packets)
-- is used for the assignment of the battery....
-- =======================================================

--2014-04-23
--Arlow Farrell
--Gutted the logic and changed it to meet new business requirements.

--Find Batteries that are assigned an incorrect BusinessUnitID:
--	1) select Device records with the following:
--		a) DeviceType = 5 (battery)
--		b) most recent battery packet (either session or nonsession data for the given BatterySerialNumber)
--		c) IsActive = 1 (active device)
--		d) SerialNumberClassification = 1 
--		e) Battery.BusinessUnitId  != TransmittingDevice.BusinessUnitId (get buid from transmitting device from most recent battery packet)
	
--	2) Overwrite any that are broadcasting from inside Enovate and are not already set to an Enovate BUID:
--		a) validate SourceIPAddress against PriorityIP table
--		b) where BusinessUnitId not in list from BusinessUnit where IsStinger = 1
		
--	3) Don't change any to an Enovate BUID that are already set to one of our Enovate BUIDs

--	4) Update Battery Device record
--		a) set BusinessUnitId = NewBusinessUnitId calculated
--		a) set RadarBUID = NewBusinessUnitId calculated

--	5) Insert DeviceBusinessUnitChangeLog record


CREATE PROCEDURE [dbo].[spBatteries_AutoUpdateSiteID]
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @ModifiedDateUTC DATETIME;
        SET @ModifiedDateUTC = GETUTCDATE();


	--delete temp table so it's re-runnable in testing.
        IF OBJECT_ID('tempdb..#tempBattRadar') IS NOT NULL
            BEGIN
                DROP TABLE #tempBattRadar;
            END;	
	
	
        CREATE TABLE #tempBattRadar
            (
              RowID INT IDENTITY(1, 1) ,
              BattDeviceID INT ,
              PreviousBusinessUnitID INT ,
              NewBusinessUnitID INT ,
              ModifiedDateUTC DATETIME ,
              SourceIP VARCHAR(20) ,
              BatterySerialNumber VARCHAR(50) ,
              DepartmentID INT
            );
	
	--find mismatched devices
        INSERT  INTO #tempBattRadar
                ( BattDeviceID ,
                  PreviousBusinessUnitID ,
                  NewBusinessUnitID ,
                  ModifiedDateUTC ,
                  SourceIP ,
                  BatterySerialNumber ,
                  DepartmentID
                )
                SELECT  a.IDAsset AS BattDeviceID ,
                        a.SiteID AS PreviousBusinessUnitID ,
                        commdevice.SiteID AS NewBusinessUnitID ,
                        nsd.CreatedDateUTC AS ModifiedDateUTC ,
                        nsd.SourceIPAddress AS SourceIP ,
                        nsd.BatterySerialNumber ,
                        ap.DepartmentID AS DepartmentID
                FROM    dbo.Assets a
                        LEFT JOIN dbo.viewAllSessionData nsd ON a.SerialNo = nsd.BatterySerialNumber
                                                            AND a.LastPostDateUTC = nsd.CreatedDateUTC
                        LEFT JOIN dbo.Assets commdevice ON commdevice.SerialNo = nsd.DeviceSerialNumber
                        JOIN dbo.AccessPoint ap ON ap.MACAddress = nsd.APMAC
                WHERE   a.LastPostDateUTC > DATEADD(DAY, -1, GETDATE())
                        AND a.IDAssetType = 5
                        AND a.IsActive = 1; 
		--and ap.SiteID  is not null
		--AND dbo.serialNumberClassification(a.SerialNo) = 1
	--Select 
	--	d.IDAsset as BattDeviceID, 
	--	d.SiteID as PreviousBusinessUnitID, 
	--	dd.SiteID as NewBusinessUnitID,
	--	@ModifiedDateUTC, 
	--	mrbp.SourceIPAddress as SourceIP
	--From Assets d
	--	CROSS APPLY MostRecentBatteryPacket(d.SerialNo) mrbp
	--	JOIN Assets dd ON dd.SerialNo = mrbp.DeviceSerialNumber
	--Where d.IDAssetType = 5
	--	and d.IsActive = 1
	--	and dd.SiteID  is not null
	--	and isnull(d.SiteID, 0) != dd.SiteId
	--	AND dbo.serialNumberClassification(d.SerialNo) = 1 --and d.LastPostDateUTC = mrbp.CreatedDateUTC
		
	--order by d.CreatedDateUTC desc
        DELETE  FROM #tempBattRadar
        WHERE   BatterySerialNumber IS NULL;



	----overwrite any of those that are reporting from within Enovate and are not already set to an Enovate BUID
        UPDATE  #tempBattRadar
        SET     NewBusinessUnitID = 28
        WHERE   RowID IN (
                SELECT  t.RowID
                FROM    #tempBattRadar t
                        JOIN dbo.PriorityIP ip ON ip.SourceIPAddress_Left = LEFT(t.SourceIP,
                                                              6)
                                              OR ip.SourceIPAddress_Left = LEFT(t.SourceIP,
                                                              8)
                                              OR ip.SourceIPAddress_Left = LEFT(t.SourceIP,
                                                              7)
                WHERE   t.PreviousBusinessUnitID NOT IN ( SELECT
                                                              IDSite
                                                          FROM
                                                              dbo.Sites
                                                          WHERE
                                                              IsInternal = 1 ) );

	-- remove any that are already set to a different enovate buid and don't need to be changed
        DELETE  FROM #tempBattRadar
        WHERE   PreviousBusinessUnitID IN ( SELECT  IDSite
                                            FROM    dbo.Sites
                                            WHERE   IsInternal = 1 )
                AND NewBusinessUnitID IN ( SELECT   IDSite
                                           FROM     dbo.Sites
                                           WHERE    IsInternal = 1 );

	--remove any that are not needing to be changed			
        DELETE  FROM #tempBattRadar
        WHERE   NewBusinessUnitID = PreviousBusinessUnitID;

        DELETE  FROM #tempBattRadar
        WHERE   NewBusinessUnitID IS NULL;

	---- update the device table	
        UPDATE  dbo.Assets
        SET     SiteID = t.NewBusinessUnitID ,
                RadarBUID = t.NewBusinessUnitID
        FROM    dbo.Assets
                INNER JOIN #tempBattRadar t ON IDAsset = t.BattDeviceID; 
		
        UPDATE  dbo.Assets
        SET     DepartmentID = t.DepartmentID
        FROM    dbo.Assets
                INNER JOIN #tempBattRadar t ON IDAsset = t.BattDeviceID
        WHERE   Assets.DepartmentID = 0
                OR Assets.DepartmentID IS NULL;
	
	----log the change
        INSERT  INTO dbo.AssetSiteChangeLog
                ( DeviceID ,
                  PreviousSiteID ,
                  NewSiteID ,
                  ModifiedDateUTC
                )
                SELECT  BattDeviceID ,
                        PreviousBusinessUnitID ,
                        NewBusinessUnitID ,
                        ModifiedDateUTC
                FROM    #tempBattRadar; 
			

	--release resources
        DROP TABLE #tempBattRadar;


    END;
GO
