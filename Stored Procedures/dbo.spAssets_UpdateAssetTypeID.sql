SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Arlow Farrell
-- Create date: 2012-02-23
-- Description:	sets the DeviceTypeID on the Device
--              table equal to the DeviceType
--              on the device's most recent
--              NonSessionDataCurrent packet
--              (if the two device types are different)
-- =============================================
CREATE PROCEDURE [dbo].[spAssets_UpdateAssetTypeID]
AS
    BEGIN
        SET NOCOUNT ON;

        IF OBJECT_ID(N'tempdb..#tmpCurrentDeviceType', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tmpCurrentDeviceType;
            END;

	-- get the devicetype from the most recent NonSessionDataCurrent row for each device
        SELECT  data.DeviceSerialNumber ,
                data.DeviceType
        INTO    #tmpCurrentDeviceType
        FROM    dbo.NonSessionDataCurrent AS [data]
        WHERE   data.CreatedDateUTC = ( SELECT    MAX(CreatedDateUTC)
                                          FROM      dbo.NonSessionDataCurrent
                                          WHERE     NonSessionDataCurrent.ROW_ID = data.ROW_ID
                                                    AND NonSessionDataCurrent.CreatedDateUTC > DATEADD(HOUR,
                                                              -24,
                                                              GETUTCDATE())
                                        )
                AND data.DeviceSerialNumber <> '';


        UPDATE  dbo.Assets
        SET     IDAssetType = a.DeviceType
        FROM    #tmpCurrentDeviceType a
        WHERE   SerialNo = a.DeviceSerialNumber
                AND IDAssetType <> a.DeviceType;

	    
        IF OBJECT_ID(N'tempdb..#tmpCurrentDeviceType', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tmpCurrentDeviceType;
            END;




    END;


GO
