SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spDashboardPulseWeb] 
	  @SiteID int= 633, @DepartmentIDList varchar(200)= NULL
AS
BEGIN
	DECLARE @DepartmentIdTable TABLE
	( 
			 item int
	);
	IF @DepartmentIDList IS NULL OR 
	   LTRIM(RTRIM(@DepartmentIDList)) = ''
	BEGIN
		INSERT INTO @DepartmentIdTable( Item )
			   SELECT DISTINCT 
					  IdDepartment 
			   FROM dbo.Departments
			   WHERE SiteID = @SiteId;
	END;
	IF @DepartmentIDList IS NOT NULL AND 
	   LTRIM(RTRIM(@DepartmentIDList)) <> ''
	BEGIN
		INSERT INTO @DepartmentIdTable( Item )
			   SELECT Item
			   FROM dbo.fnStringList2Table( @DepartmentIDList );
	END;

		

--insert into ErrorLog (Description,Source,CreatedDateUTC) values ( Cast(@SiteID as varchar(30)), 'SPDASHBOARDPULSEWEB PARAMS', GETUTCDATE())  
--- Workstations
        DECLARE @WorkstationsTotal INT;
        DECLARE @WorkstationsInService INT;
        DECLARE @WorkstationsInServiceWeek INT;
        DECLARE @WorkstationsAvailable INT;
        DECLARE @WorkstationsOffline INT;
        DECLARE @WorkstationAvgUtilization FLOAT;
        DECLARE @WorkstationPCAvgUtilization FLOAT;
        DECLARE @WorkstationsMoving INT;

                SET @WorkstationsTotal = ( SELECT   COUNT(IDAsset) AS WorkstationsTotal
                                           FROM     dbo.Assets								   
                                           WHERE    SiteID = @SiteID
                                                        AND IDAssetType IN ( 1,
                                                              3, 6,8,9 )
                                                    AND Retired = 0 AND 
										  DepartmentId IN
										 (
											 SELECT Item
											 FROM @DepartmentIdTable
										 )
                                         );
                SET @WorkstationsInService = ( SELECT   COUNT(IDAsset) AS WorkstationsInService
                                               FROM     dbo.Assets
                                               WHERE    LastPostDateUTC > DATEADD(HOUR,
                                                              -1, GETUTCDATE())
                                                        AND ( Amps < -1000 OR move =1) 
                                                        AND SiteID = @SiteID
                                                        AND IDAssetType IN ( 1,
                                                              3, 6,8,9 )
                                                        AND Retired = 0 AND 
										  DepartmentId IN
										 (
											 SELECT Item
											 FROM @DepartmentIdTable
										 )
                                             );
                SET @WorkstationsInServiceWeek = ( SELECT   COUNT(IDAsset) AS WorkstationsInServiceWeek
                                                   FROM     dbo.Assets
                                                   WHERE    LastPostDateUTC > DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                             AND ( Amps < -1000 OR move =1) 
                                                            AND SiteID = @SiteID
                                                           AND IDAssetType IN ( 1,
                                                              3, 6,8,9 )
                                                            AND Retired = 0 AND 
										  DepartmentId IN
										 (
											 SELECT Item
											 FROM @DepartmentIdTable
										 )
                                                 );
                SET @WorkstationsAvailable = ( SELECT   ( COUNT(IDAsset)
                                                          - ( @WorkstationsInService ) ) AS WorkstationsAvailable
                                               FROM     dbo.Assets
                                               WHERE    LastPostDateUTC > DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                        AND SiteID = @SiteID
													--	  AND ( Amps > -1000 AND move = 0) 
                                                  AND IDAssetType IN ( 1,
                                                              3, 6,8,9 )
                                                        AND Retired = 0 AND 
										  DepartmentId IN
										 (
											 SELECT Item
											 FROM @DepartmentIdTable
										 )
                                             );
                SET @WorkstationsOffline = ( SELECT ( COUNT(IDAsset) ) AS WorkstationsOffline
                                             FROM   dbo.Assets
                                             WHERE  LastPostDateUTC < DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                    AND SiteID = @SiteID
                                               AND IDAssetType IN ( 1,
                                                              3, 6,8,9 )
                                                    AND Retired = 0 AND 
										  DepartmentId IN
										 (
											 SELECT Item
											 FROM @DepartmentIdTable
										 )
                                           ); 
                SET @WorkstationPCAvgUtilization = ( SELECT AVG(s.EstimatedPCUtilization)
                                                            * 100
                                                     FROM   dbo.Sessions AS s
                                                     WHERE  s.EndDateUTC > DATEADD(DAY,
                                                              -3, GETUTCDATE())
                                                            AND s.EndDateUTC < DATEADD(DAY,
                                                              -1, GETUTCDATE())
                                                            AND s.SiteID = @SiteID AND 
										  DepartmentId IN
										 (
											 SELECT Item
											 FROM @DepartmentIdTable
										 )
                                                     GROUP BY s.SiteID
                                                   );


                SET @WorkstationAvgUtilization = ( SELECT   CAST(AVG(s.Utilization) AS FLOAT)
                                                            * 100
                                                   FROM     dbo.SummaryTrending s
                                                   WHERE    s.Date > DATEADD(DAY,
                                                              -3, GETUTCDATE())
                                                            AND s.Date < DATEADD(DAY,
                                                              -1, GETUTCDATE())
                                                            AND s.SiteID = @SiteID 
												AND 
												    DepartmentId IN
												   (
													   SELECT Item
													   FROM @DepartmentIdTable
												   )
                                                   GROUP BY s.SiteID
                                                 );

				   

 
                SET @WorkstationsMoving = ( SELECT  ( COUNT(IDAsset) ) AS WorkstationsMoving
                                            FROM    dbo.Assets
                                            WHERE   LastPostDateUTC > DATEADD(HOUR,
                                                              -1, GETUTCDATE())
                                                    AND SiteID = @SiteID
                                                     AND IDAssetType IN ( 1,
                                                              3, 6,8,9 )
                                                    AND Move = 1
                                                    AND Retired = 0 AND 
										  DepartmentId IN
										 (
											 SELECT Item
											 FROM @DepartmentIdTable
										 )
                                          ); 

--- ChargingBays
        DECLARE @ChargingBaysTotal INT;
        DECLARE @ChargingBaysCharging INT;
        DECLARE @ChargingBaysOccupied INT;
        DECLARE @ChargingBaysVacant INT;
        DECLARE @ChargingBaysOffline INT;



                SET @ChargingBaysTotal = ( SELECT   COUNT(VC.IDAsset) 
                                           FROM     dbo.vwChargers AS VC
                                           WHERE    ( VC.BayCount < 4
                                                      AND VC.BayCount IS NOT NULL
                                                    )
                                                    AND VC.SiteID = @SiteID
                                                    AND VC.Retired = 0 AND VC.Bay1LastPostDateUTC > DATEADD(DAY,-30,GETUTCDATE()) and
										  DepartmentId IN
										 (
											 SELECT Item
											 FROM @DepartmentIdTable
										 )
                                         )
                    + ( SELECT  COUNT(VC.IDAsset) 
                        FROM    dbo.vwChargers AS VC
                        WHERE   ( VC.BayCount < 4
                                  AND VC.BayCount IS NOT NULL
                                )
                                AND VC.SiteID = @SiteID
                                AND VC.Retired = 0 AND VC.Bay2LastPostDateUTC > DATEADD(DAY,-30,GETUTCDATE()) and
										  DepartmentId IN
										 (
											 SELECT Item
											 FROM @DepartmentIdTable
										 )
                      ) + ( SELECT  COUNT(VC.IDAsset) 
                        FROM    dbo.vwChargers AS VC
                        WHERE   ( VC.BayCount = 4
                                  AND VC.BayCount IS NOT NULL
                                )
                                AND VC.SiteID = @SiteID
                                AND VC.Retired = 0 AND VC.Bay3LastPostDateUTC > DATEADD(DAY,-30,GETUTCDATE()) and
										  DepartmentId IN
										 (
											 SELECT Item
											 FROM @DepartmentIdTable
										 )
                      ) + ( SELECT  COUNT(VC.IDAsset) 
                        FROM    dbo.vwChargers AS VC
                        WHERE   ( VC.BayCount = 4
                                  AND VC.BayCount IS NOT NULL
                                )
                                AND VC.SiteID = @SiteID
                                AND VC.Retired = 0 AND VC.Bay4LastPostDateUTC > DATEADD(DAY,-30,GETUTCDATE()) and
										  DepartmentId IN
										 (
											 SELECT Item
											 FROM @DepartmentIdTable
										 )
                      );
                SET @ChargingBaysCharging = ( SELECT    COUNT(CAB.IDAsset)
                                              FROM      dbo.vwChargers AS CAB
                                              WHERE     CAB.Bay1LastPostDateUTC > DATEADD(hour,
                                                              -24, GETUTCDATE())
                                                        AND CAB.SiteID = @SiteID
                                                        AND CAB.Retired = 0 
											 AND 
												DepartmentId IN
												(
												    SELECT Item
												    FROM @DepartmentIdTable
												)
                                                        AND ( CAB.Bay1Amps > 0
                                                              AND CAB.Bay1BSN IS NOT NULL
													
                                                              AND CAB.Bay1BSN  <> '0' and CAB.Bay1ChargeLevel < 99 and CAB.Bay1ChargeLevel is not null 
                                                              AND CAB.Bay1BSN != 'No Batt SN'
                                                              AND CAB.Bay1Amps IS NOT NULL
                                                            )
                                            )
                    + ( SELECT  COUNT(CAB.IDAsset)
                        FROM    dbo.vwChargers AS CAB
                        WHERE   CAB.Bay2LastPostDateUTC > DATEADD(hour,
                                                              -24,
                                                              GETUTCDATE())
                                AND CAB.SiteID = @SiteID
                                AND CAB.Retired = 0 
						  AND 
							 DepartmentId IN
							 (
								SELECT Item
								FROM @DepartmentIdTable
							 )
                                AND ( CAB.Bay2Amps > 0
                                      AND CAB.Bay2BSN IS NOT NULL
									 
                                      AND CAB.Bay2BSN  <> '0' and CAB.Bay2ChargeLevel < 99 and CAB.Bay2ChargeLevel is not null 
                                      AND CAB.Bay2BSN != 'No Batt SN'
                                      AND CAB.Bay2Amps IS NOT NULL
                                    )
                      )
                    + ( SELECT  COUNT(CAB.IDAsset)
                        FROM    dbo.vwChargers AS CAB
                        WHERE   CAB.Bay3LastPostDateUTC > DATEADD(hour,
                                                              -24,
                                                              GETUTCDATE())
                                AND CAB.SiteID = @SiteID
                                AND CAB.Retired = 0 
						  AND 
							 DepartmentId IN
							 (
								SELECT Item
								FROM @DepartmentIdTable
							 )
                                AND ( CAB.Bay3Amps > 0
                                      AND CAB.Bay3BSN IS NOT NULL
									
                                      AND CAB.Bay3BSN  <> '0' and CAB.Bay3ChargeLevel < 99 and CAB.Bay3ChargeLevel is not null 
                                      AND CAB.Bay3BSN != 'No Batt SN'
                                      AND CAB.Bay3Amps IS NOT NULL
                                    )
                      )
                    + ( SELECT  COUNT(CAB.IDAsset)
                        FROM    dbo.vwChargers AS CAB
                        WHERE   CAB.Bay4LastPostDateUTC > DATEADD(hour,
                                                              -24,
                                                              GETUTCDATE())
                                AND CAB.SiteID = @SiteID
                                AND CAB.Retired = 0
						  AND 
							 DepartmentId IN
							 (
								SELECT Item
								FROM @DepartmentIdTable
							 )
                                AND ( CAB.Bay4Amps > 0
                                      AND CAB.Bay4BSN IS NOT NULL
								
                                      AND CAB.Bay4BSN  <> '0' and CAB.Bay4ChargeLevel < 99 and CAB.Bay4ChargeLevel is not null 
                                      AND CAB.Bay4BSN != 'No Batt SN'
                                      AND CAB.Bay4Amps IS NOT NULL
                                    )
                      );

                SET @ChargingBaysOccupied = ( SELECT    COUNT(IDAsset)
                                              FROM      dbo.vwChargers
                                              WHERE     Bay1LastPostDateUTC > DATEADD(hour,
                                                              -24, GETUTCDATE())
                                                        AND LastPostDateUTC > DATEADD(DAY,
                                                              -2, GETUTCDATE())
                                                        AND SiteID = @SiteID
                                                        AND Retired = 0 
											 AND 
												    DepartmentId IN
												   (
													   SELECT Item
													   FROM @DepartmentIdTable
												   )
                                                        AND Bay1LastPostDateUTC IS NOT NULL
                                                        AND ( Bay1BSN IS NOT NULL
                                                              AND Bay1BSN != 'No Batt SN'
                                                              AND LEN(Bay1BSN) > 7
                                                              AND ( Bay1Amps >= 0
                                                 
                                                              )
                                                            )
                                            )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.vwChargers
                        WHERE   Bay2LastPostDateUTC > DATEADD(hour,
                                                              -24,
                                                              GETUTCDATE())
                                AND LastPostDateUTC > DATEADD(DAY, -2,
                                                              GETUTCDATE())
                                AND SiteID = @SiteID
                                AND Retired = 0
						  AND 
							 DepartmentId IN
							 (
								SELECT Item
								FROM @DepartmentIdTable
							 )
                                AND Bay2LastPostDateUTC IS NOT NULL
                                AND ( Bay2BSN IS NOT NULL
                                      AND Bay2BSN != 'No Batt SN'
                                      AND LEN(Bay2BSN) > 7
                                      AND ( Bay2Amps >= 0
                                     
                                          )
                                    )
                      )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.vwChargers
                        WHERE   Bay3LastPostDateUTC > DATEADD(hour,
                                                              -24,
                                                              GETUTCDATE())
                                AND LastPostDateUTC > DATEADD(DAY, -2,
                                                              GETUTCDATE())
                                AND SiteID = @SiteID
                                AND Retired = 0
						  AND 
							 DepartmentId IN
							 (
								SELECT Item
								FROM @DepartmentIdTable
							 )
                                AND BayCount > 2
                                AND Bay3LastPostDateUTC IS NOT NULL
                                AND ( Bay3BSN IS NOT NULL
                                      AND Bay3BSN != 'No Batt SN'
                                      AND LEN(Bay3BSN) > 7
                                      AND ( Bay3Amps >= 0
                                         
                                          )
                                    )
                      )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.vwChargers
                        WHERE   Bay4LastPostDateUTC > DATEADD(hour,
                                                              -24,
                                                              GETUTCDATE())
                                AND LastPostDateUTC > DATEADD(DAY, -1,
                                                              GETUTCDATE())
                                AND SiteID = @SiteID
                                AND Retired = 0
						  AND 
							 DepartmentId IN
							 (
								SELECT Item
								FROM @DepartmentIdTable
							 )
                                AND BayCount > 2
                                AND Bay4LastPostDateUTC IS NOT NULL
                                AND ( Bay4BSN IS NOT NULL
                                      AND Bay4BSN != 'No Batt SN'
                                      AND LEN(Bay4BSN) > 7
                                      AND ( Bay4Amps >= 0
                                     
                                          )
                                    )
                      );

				    

                SET @ChargingBaysVacant = ( SELECT  COUNT(IDAsset)
                                            FROM    dbo.vwChargers
                                            WHERE   LastPostDateUTC > DATEADD(hour,
                                                              -24, GETUTCDATE())
                                                    AND SiteID = @SiteID
                                                    AND Retired = 0
										  AND 
											 DepartmentId IN
											(
												SELECT Item
												FROM @DepartmentIdTable
											)                                                   
                                                    AND ( Bay1Amps <= 0
                                                          OR Bay1Amps IS NULL
                                                        )
                                                    AND ( Bay1BSN IS NULL
                                                          OR LEN(Bay1BSN) < 7
                                                          OR Bay1BSN = 'No Batt SN'
                                                        )
                                          )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.vwChargers
                        WHERE   LastPostDateUTC > DATEADD(hour,
                                                              -24,
                                                          GETUTCDATE())
                                AND SiteID = @SiteID
                                AND Retired = 0
						  AND DepartmentId IN
							 (
								SELECT Item
								FROM @DepartmentIdTable
							 )                                                  
                                AND ( Bay2Amps <= 0
                                      OR Bay2Amps IS NULL
                                    )
                                AND ( Bay2BSN IS NULL
                                      OR LEN(Bay2BSN) < 7
                                      OR Bay2BSN = 'No Batt SN'
                                    )
                      )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.vwChargers
                        WHERE   SiteID = @SiteID
                                AND LastPostDateUTC > DATEADD(hour,
                                                              -24,
                                                              GETUTCDATE())
                                AND Retired = 0
						  AND 
							 DepartmentId IN
							 (
								SELECT Item
								FROM @DepartmentIdTable
							 )
                                AND BayCount > 2
                                AND ( Bay3Amps <= 0
                                      OR Bay3Amps IS NULL
                                    )
                                AND ( Bay3BSN IS NULL
                                      OR LEN(Bay3BSN) < 7
                                      OR Bay3BSN = 'No Batt SN'
                                    )
                      )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.vwChargers
                        WHERE   LastPostDateUTC > DATEADD(hour,
                                                              -24,
                                                          GETUTCDATE())
                                AND SiteID = @SiteID
                                AND Retired = 0
						  AND DepartmentId IN
							 (
								SELECT Item
								FROM @DepartmentIdTable
							 )
                                AND BayCount > 2
                                                    --AND ( Bay4Activity = 3
                                                    --      OR Bay4ChargeLevel = 0 or Bay4ChargeLevel IS NULL)                                                        
                                AND ( Bay4Amps <= 0
                                      OR Bay4Amps IS NULL
                                    )
                                AND ( Bay4BSN IS NULL
                                      OR LEN(Bay4BSN) < 7
                                      OR Bay4BSN = 'No Batt SN'
                                    )
                      ); 
		

                SET @ChargingBaysOffline = ( SELECT COUNT(VC.IDAsset)
                                             FROM   dbo.vwChargers AS VC
                                             WHERE  ( VC.Bay1LastPostDateUTC < DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                      AND VC.Bay1LastPostDateUTC IS NOT NULL
                                                    )
                                                    AND VC.LastPostDateUTC < DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                    AND VC.SiteID = @SiteID
                                                    AND VC.Retired = 0
										  AND DepartmentId IN
											(
												SELECT Item
												FROM @DepartmentIdTable
											)
                                           )
                    + ( SELECT  COUNT(VC.IDAsset)
                        FROM    dbo.vwChargers AS VC
                        WHERE   ( VC.Bay2LastPostDateUTC < DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                  AND VC.Bay2LastPostDateUTC IS NOT NULL
                                )
                                AND VC.LastPostDateUTC < DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                AND VC.SiteID = @SiteID
                                AND VC.Retired = 0
						  AND DepartmentId IN
							 (
								SELECT Item
								FROM @DepartmentIdTable
							 )
                      )
                    + ( SELECT  COUNT(VC.IDAsset)
                        FROM    dbo.vwChargers AS VC
                        WHERE   ( VC.Bay3LastPostDateUTC < DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                  AND VC.Bay3LastPostDateUTC IS NOT NULL
                                )
                                AND VC.LastPostDateUTC < DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                AND VC.BayCount = 4
                                AND VC.SiteID = @SiteID
                                AND VC.Retired = 0
						  AND DepartmentId IN
							 (
								SELECT Item
								FROM @DepartmentIdTable
							 )
                      )
                    + ( SELECT  COUNT(VC.IDAsset)
                        FROM    dbo.vwChargers AS VC
                        WHERE   ( ( VC.Bay4LastPostDateUTC < DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                  AND VC.Bay4LastPostDateUTC IS NOT NULL
                                  )
                                )
                                AND VC.LastPostDateUTC < DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                AND VC.BayCount = 4
                                AND VC.SiteID = @SiteID
                                AND VC.Retired = 0
						  AND DepartmentId IN
							 (
								SELECT Item
								FROM @DepartmentIdTable
							 )
                      );


--- Chargers
        DECLARE @ChargersTotal INT;
        DECLARE @ChargersCharging INT;
        DECLARE @ChargersVacant INT;
        DECLARE @ChargersOccupied INT;
        DECLARE @ChargersOnline INT;
        DECLARE @ChargersOffline INT;
        DECLARE @LowChargeInserts INT;
        DECLARE @HiChargeRemovals INT;

                SET @ChargersTotal = ( SELECT   COUNT(IDAsset) AS ChargersTotal
                                       FROM     dbo.vwChargers
                                       WHERE    SiteID = @SiteID
                                                AND Retired = 0
									   AND DepartmentId IN
										 (
											 SELECT Item
											 FROM @DepartmentIdTable
										 )
                                     );

                SET @ChargersCharging = ( SELECT    COUNT(VC.IDAsset) AS ChargersCharging
                                          FROM      dbo.vwChargers AS VC
                                          WHERE     VC.LastPostDateUTC >= DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                    AND VC.SiteID = @SiteID
                                                    AND VC.Amps > 0
                                                    AND VC.Amps IS NOT NULL
                                                    AND VC.Retired = 0
										  AND DepartmentId IN
											    (
												    SELECT Item
												    FROM @DepartmentIdTable
											    )
                                        );
                SET @ChargersOnline = ( SELECT  COUNT(IDAsset)
                                        FROM    dbo.vwChargers
                                        WHERE   ( LastPostDateUTC >= DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                  AND LastPostDateUTC IS NOT NULL
                                                )
                                                AND SiteID = @SiteID
                                                AND Retired = 0	
									   AND DepartmentId IN
										 (
											 SELECT Item
											 FROM @DepartmentIdTable
										 )
                                      ); 

                SET @ChargersOffline = ( SELECT COUNT(IDAsset)
                                         FROM   dbo.vwChargers
                                         WHERE  ( LastPostDateUTC <= DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                  OR LastPostDateUTC IS NULL
                                                )
                                                AND SiteID = @SiteID
                                                AND Retired = 0
									   AND DepartmentId IN
										 (
											 SELECT Item
											 FROM @DepartmentIdTable
										 )
                                       ); 
                SET @LowChargeInserts = ( SELECT    COUNT(S2.DeviceSerialNumber) AS 'LowChargeInserts'
                                          FROM      dbo.Sessions AS S2
                                          WHERE     S2.StartDateUTC >= DATEADD(DAY,
                                                              -1, GETUTCDATE())
                                                    AND S2.StartChargeLevel <= 90
                                                    AND S2.SiteID = @SiteID
										  AND DepartmentId IN
											    (
												    SELECT Item
												    FROM @DepartmentIdTable
											    )
                                        );
                SET @HiChargeRemovals = ( SELECT    COUNT(S2.DeviceSerialNumber) AS 'HiChargeRemovals'
                                          FROM      dbo.Sessions AS S2
                                          WHERE     S2.StartDateUTC >= DATEADD(DAY,
                                                              -1, GETUTCDATE())
                                                    AND S2.EndChargeLevel >= 20
                                                    AND S2.SiteID = @SiteID
										  AND DepartmentId IN
										 (
											 SELECT Item
											 FROM @DepartmentIdTable
										 )
                                        );


--Batteries
        DECLARE @BatteriesTotal INT  = 0;
        DECLARE @BatteriesAvailable INT  = 0;
        DECLARE @BatteriesInUse INT  = 0;
        DECLARE @BatteriesCharging INT = 0;
        DECLARE @BatteriesDormant INT  = 0;
        DECLARE @BatteriesOffline INT  = 0;
        DECLARE @BatteriesFullyCharged INT  = 0;
        DECLARE @BatteriesAvgCapacity DECIMAL(18, 2) = 0.00;

                SET @BatteriesTotal = (SELECT  COUNT(IDAsset)
                                        FROM    dbo.Assets
                                        WHERE   SiteID = @SiteID
                                                AND IDAssetType IN ( 5 )
                                                AND LastPostDateUTC > DATEADD(DAY,
                                                              -30, GETUTCDATE())
                                                AND Retired = 0
									   AND (DepartmentId IN
										 (
											 SELECT Item
											 FROM @DepartmentIdTable
										 ) OR DepartmentID IS NULL)
                                      );  
                SET @BatteriesInUse = ( SELECT  COUNT(AB.SerialNo)
                                        FROM    dbo.vwBatteries AS AB
                                        WHERE   AB.LastPacketType = 1
                                                AND AB.LastPostDateUTC >= DATEADD(hour,
                                                              -1, GETUTCDATE())
                                                AND AB.SiteID = @SiteID
                                                AND AB.IDAssetType IN ( 5 )
                                                AND AB.Retired = 0 AND amps IS NOT null
                                            
                                                AND AB.Amps < 0
                                                AND AB.LastPostDateUTC IS NOT NULL
									   AND DepartmentId IN
										 (
											 SELECT Item
											 FROM @DepartmentIdTable
										 )
									
                                      );  
                SET @BatteriesAvailable = ( SELECT  COUNT(AB.SerialNo)
                                            FROM    dbo.vwBatteries AS AB
                                            WHERE   
                                                     AB.LastPostDateUTC >= DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                    AND AB.SiteID = @SiteID
                                                    AND AB.IDAssetType IN ( 5 )
													AND  AB.LastPacketType = 2
                                                    AND AB.Retired = 0
                                                    AND (AB.Amps >= 0 or amps IS  NULL)	
                                                    AND AB.LastPostDateUTC IS NOT NULL
										  AND DepartmentId IN
											(
												SELECT Item
												FROM @DepartmentIdTable
											)
                                                 
                                          );  

                SET @BatteriesCharging = (SELECT   COUNT(IDAsset)
                                           FROM     dbo.Assets
                                           WHERE    LastPacketType = 2
                                                    AND LastPostDateUTC >= DATEADD(hour,
                                                              -24,
                                                              GETUTCDATE())
                                                    AND SiteID = @SiteID
                                                    AND IDAssetType IN ( 5 )
                                                    AND Retired = 0
                                                    AND LastPostDateUTC IS NOT NULL
                                                    AND ChargeLevel < 100 
                                                    AND ( Amps > 0 AND Amps IS NOT NULL
                                                      
                                                        )
										  AND ( DepartmentId IN
											(
												SELECT Item
												FROM @DepartmentIdTable
											) OR DepartmentID IS NULL OR DepartmentID = 0)
                                                     
                                         ); 
										 --Batteries havent reported IN 24 hours but NOT offline
                SET @BatteriesDormant = ( SELECT    COUNT(IDAsset)
                                          FROM      dbo.Assets
                                          WHERE     LastPostDateUTC > DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                    AND LastPostDateUTC < DATEADD(HOUR,
                                                              -24,
                                                              GETUTCDATE())
                                                    AND SiteID = @SiteID
                                                    AND IDAssetType IN ( 5 )
                                                    AND Retired = 0
                                                    AND LastPostDateUTC IS NOT NULL
										  AND DepartmentId IN
											    (
												    SELECT Item
												    FROM @DepartmentIdTable
											    )
                                                    AND LastPostDateUTC IS NOT NULL
                                        ); 

                SET @BatteriesOffline = (  SELECT    COUNT(IDAsset)
                                          FROM      dbo.Assets
                                          WHERE     ( LastPostDateUTC <= DATEADD(DAY,
                                                              -7, GETUTCDATE()) )
                                                    AND SiteID = @SiteID
                                                    AND IDAssetType IN ( 5 )
                                                    AND Retired = 0
                                                    AND LastPostDateUTC IS NOT NULL
										  AND DepartmentId IN
											(
												SELECT Item
												FROM @DepartmentIdTable
											)
                                                    AND LastPostDateUTC IS NOT NULL
                                        ); 
                SET @BatteriesFullyCharged = (SELECT   COUNT(IDAsset)
                                               FROM     dbo.Assets
                                               WHERE    ( LastPostDateUTC >= DATEADD(HOUR,-24, GETUTCDATE()) )
                                                        AND SiteID = @SiteID
                                                        AND  ChargeLevel > 94
                                                              AND Amps = 0 AND Amps IS NOT NULL
                                                              
                                                            
                                                        AND LastPacketType = 2
                                                        AND IDAssetType IN ( 5 )
														 AND LEN(SerialNo) > 7
                                                        AND Retired = 0
                                                        AND LastPostDateUTC IS NOT NULL
											 AND DepartmentId IN
											    (
												    SELECT Item
												    FROM @DepartmentIdTable
											    )
                                                        AND LastPostDateUTC IS NOT NULL
                                             ); 
                SET @BatteriesAvgCapacity = ( SELECT    SUM(dbo.fnGetBatteryCapacity_Int(SerialNo,
                                                              FullChargeCapacity))
                                                        / ( @BatteriesTotal - @BatteriesOffline ) AS BatteriesAvgCapacity
                                              FROM      dbo.Assets
                                              WHERE     LastPostDateUTC > DATEADD(DAY,
                                                              -7,
                                                              GETUTCDATE())
                                              
                                                        AND FullChargeCapacity IS NOT NULL
                                                        AND SiteID = @SiteID
                                                        AND IDAssetType IN ( 5 )
                                                        AND Retired = 0
                                                        AND LastPostDateUTC IS NOT NULL
                                                        AND ( FullChargeCapacity IS NOT NULL
                                                              AND FullChargeCapacity > 0
                                                            ) AND DepartmentId IN
											    (
												    SELECT Item
												    FROM @DepartmentIdTable
											    )
                                            );
              
        SELECT  @WorkstationsTotal AS WorkstationsTotal ,
                @WorkstationsInService AS WorkstationsInService ,
                @WorkstationsInServiceWeek AS WorkstationsInServiceWeek ,
                @WorkstationsAvailable AS WorkstationsAvailable ,
                @WorkstationsOffline AS WorkstationsOffline ,
                @WorkstationsMoving AS WorkstationsMoving ,
                @WorkstationAvgUtilization AS WorkstationAvgUtilization ,
                @WorkstationPCAvgUtilization AS WorkstationPCAvgUtilization ,
                @ChargersTotal AS ChargersTotal ,
    --            @ChargersCharging AS ChargersCharging ,
				--@ChargersOccupied AS ChargersOccupied,
                --@ChargersVacant AS ChargersVacant ,
                @ChargersOnline AS ChargersOnline ,
                @ChargersOffline AS ChargersOffline ,
                @ChargingBaysTotal AS ChargingBaysTotal ,
                @ChargingBaysCharging AS ChargingBaysCharging ,
                @ChargingBaysOccupied AS ChargingBaysOccupied ,
                @ChargingBaysVacant AS ChargingBaysVacant ,
                @ChargingBaysOffline AS ChargingBaysOffline ,
                @HiChargeRemovals AS HiChargeRemovals ,
                @LowChargeInserts AS LowChargeInserts ,
                @BatteriesTotal AS BatteriesTotal ,
                @BatteriesInUse AS BatteriesInService ,
                @BatteriesAvailable AS BatteriesAvailable ,
                @BatteriesCharging AS BatteriesCharging ,
                @BatteriesFullyCharged AS BatteriesFullyCharged ,
                @BatteriesDormant AS BatteriesDormant ,
                @BatteriesOffline AS BatteriesOffline ,
                IIF(@BatteriesAvgCapacity < 101, @BatteriesAvgCapacity, 100) AS BatteriesAvgCapacity;


END


GO
