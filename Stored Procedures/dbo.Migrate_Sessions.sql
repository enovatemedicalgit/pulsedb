SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[Migrate_Sessions]
AS
    BEGIN
        SET NOCOUNT ON;

		----------------------------------------------------------------------------------------------------------------
		-- VARIABLES
		----------------------------------------------------------------------------------------------------------------
        DECLARE @dateFloor DATETIME2;
        DECLARE @dateCalc DATETIME2;
        DECLARE @MaxCreatedDateOnHistorical DATETIME2; 
	   DECLARE @MinCreatedDateOnCurrent DATETIME2;
	   DECLARE @BatchEndPoint DATETIME2;
	   DECLARE @BatchStartTime DATETIME2;
	   DECLARE @BatchEndTime DATETIME2;
	   DECLARE @BatchRowCount BIGINT;

		---------------------------------------------------
		-- set this to the number of days you want to keep (ie. -365 for one year, -120, -90, -30, etc. )
		---------------------------------------------------
        DECLARE @daysToKeep INT;
            SET @daysToKeep = 30; --(4 months plus buffer)
		
		----------------------------------------------------------------------------------------------------------------

        SET @dateCalc = DATEADD(DAY, ( @daysToKeep * -1 ), GETUTCDATE());

		-- @dateFloor becomes the date that is the start of the day for the day found above. 
		-- This way we remove records by full days.
        SET @dateFloor = CONVERT(DATETIME, ( CONVERT(VARCHAR(10), @dateCalc, 111) ));
        SET @MaxCreatedDateOnHistorical = ( SELECT  MAX(EndDateUTC)
                                            FROM    PulseHistorical.dbo.Sessions
                                          );
		 
		--LOOP : this starts looping through and deleting records by how many we set for each pass until we delete all records below the date floor
        WHILE EXISTS ( SELECT   1
                       FROM     Pulse.dbo.Sessions
                       WHERE    EndDateUTC < @dateFloor )
            BEGIN

			
			---------------------------------------------------
			-- Do the deletion from the table
			-- Set how many records to delete on each pass using the TOP command
			-- A low number prevents out of control transaction log growth.
			---------------------------------------------------
               BEGIN TRANSACTION 
			Select @MinCreatedDateOnCurrent = Min(sub.EndDateUTC) , @BatchEndPoint = Max(sub.EndDateUTC), @BatchStartTime = GETUTCDATE()
			FROM 
				(SELECT TOP 10000 *
				from Pulse.dbo.Sessions
				WHERE   EndDateUTC > @MaxCreatedDateOnHistorical
				AND EndDateUTC < @dateFloor
				ORDER BY EndDateUTC ASC) sub 
		     
			 
			BEGIN TRY
			INSERT INTO PulseHistorical.[dbo].[Sessions]
			    ([StartDateUTC]
			    ,[EndDateUTC]
			    ,[DeviceType]
			    ,[DeviceSerialNumber]
			    ,[BatterySerialNumber]
			    ,[AvgAmpDraw]
			    ,[NumberOfMoves]
			    ,[SessionLengthMinutes]
			    ,[BatteryName]
			    ,[SiteID]
			    ,[PacketCount]
			    ,[ChargePctConsumed]
			    ,[RunRate]
			    ,[Original_Row_Id]
			    ,[StartChargeLevel]
			    ,[EndChargeLevel]
			    ,[HiAmpDraw]
			    ,[LowAmpDraw]
			    ,[EstimatedPCUtilization]
			    ,[AvgSignalQuality]
			    ,[RemainingCapacity]
			    ,[DepartmentID])
			 SELECT
				   TOP 10000
				   [StartDateUTC]
				  ,[EndDateUTC]
				  ,[DeviceType]
				  ,[DeviceSerialNumber]
				  ,[BatterySerialNumber]
				  ,[AvgAmpDraw]
				  ,[NumberOfMoves]
				  ,[SessionLengthMinutes]
				  ,[BatteryName]
				  ,[SiteID]
				  ,[PacketCount]
				  ,[ChargePctConsumed]
				  ,[RunRate]
				  ,[ROW_ID]
				  ,[StartChargeLevel]
				  ,[EndChargeLevel]
				  ,[HiAmpDraw]
				  ,[LowAmpDraw]
				  ,[EstimatedPCUtilization]
				  ,[AvgSignalQuality]
				  ,[RemainingCapacity]
				  ,[DepartmentID]
			   FROM Pulse.[dbo].[Sessions]
			   WHERE   EndDateUTC > @MaxCreatedDateOnHistorical
				AND EndDateUTC < @dateFloor
				ORDER BY EndDateUTC ASC


			   DELETE  FROM Pulse.dbo.Sessions
			   WHERE   ROW_ID IN (
								SELECT  TOP 10000
									   ROW_ID
								FROM    Pulse.dbo.Sessions
								WHERE   EndDateUTC > @MaxCreatedDateOnHistorical
									   AND EndDateUTC < @dateFloor
								ORDER BY EndDateUTC ASC);

				    SET @BatchRowCount = @@ROWCOUNT
				    SET @BatchEndTime = GETUTCDATE()

			   INSERT INTO PulseHistorical.dbo.MigrationLog (SourceTableName, DestTableName, OldestRecord, NewestRecord, BeginTime, EndTime, NoOfRows)
										VALUES ('Pulse.dbo.Sessions', 'PulseHistorical.dbo.Sessions', @MinCreatedDateOnCurrent, @BatchEndPoint, @BatchStartTime, @BatchEndTime, @BatchRowCount)                              

			 END TRY
			 BEGIN CATCH
			     SELECT   
				ERROR_NUMBER() AS ErrorNumber  
				,ERROR_SEVERITY() AS ErrorSeverity  
				,ERROR_STATE() AS ErrorState  
				,ERROR_PROCEDURE() AS ErrorProcedure  
				,ERROR_LINE() AS ErrorLine  
				,ERROR_MESSAGE() AS ErrorMessage;  
  
			 IF @@TRANCOUNT > 0  
				ROLLBACK TRANSACTION;  
				BREAK;
		  END CATCH;  
		  IF @@TRANCOUNT > 0  		  
			 COMMIT TRANSACTION; 

            END;
		  --ALTER INDEX ALL ON Pulse.dbo.Sessions
		  --REORGANIZE
		  --ALTER INDEX ALL ON PulseHistorical.dbo.Sessions
		  --REORGANIZE
    END;




GO
