SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[spGenerateAlerts]
    (
      --@guid uniqueidentifier, 
      @connectionString NVARCHAR(500)
    )
AS
    BEGIN
        SET NOCOUNT ON;

        IF OBJECT_ID('tempdb..#qsToProcess') IS NOT NULL
            DROP TABLE #qsToProcess;

        IF OBJECT_ID('tempdb..#qsToProcess') IS NULL
            BEGIN
                SELECT --@guid as processId, 
                        qs.*
                INTO    #qsToProcess
                FROM    dbo.GetQuerystringDataToGenerateAlerts(@connectionString) qs
                WHERE   qs.ErrorCodeId IS NULL
                        OR qs.ErrorCodeId < 500;

		-- only including records that are below the 'critical error' threshold. 
		-- these are the only records that get inserted into Session/NonSession Data


                CREATE NONCLUSTERED INDEX idx_tmptbl_qsToProcess_QueryStringId ON #qsToProcess(QueryStringID);


                UPDATE  #qsToProcess
                SET     FixedBatterySerialNumber = ISNULL(BatterySerialNumber,
                                                          '')
                WHERE   LTRIM(RTRIM(UPPER(FixedBatterySerialNumber))) = 'NO BATT SN'
                        OR LTRIM(RTRIM(UPPER(FixedBatterySerialNumber))) = 'NONE';
            END;

        IF OBJECT_ID('tempdb..#tAlerts') IS NOT NULL
            DROP TABLE #tAlerts;
        CREATE TABLE #tAlerts
            (
              AlertConditionId INT ,
              QueryStringId INT
            );

        IF OBJECT_ID('tempdb..#acSumDevOnly') IS NOT NULL
            DROP TABLE #acSumDevOnly;
        CREATE TABLE #acSumDevOnly
            (
              QueryStringId INT ,
              AlertConditionId INT ,
              DeviceSerialNumber VARCHAR(50) ,
              acSum INT ,
              countTotal INT ,
              AlertCountId INT
            );

        IF OBJECT_ID('tempdb..#acSumBattOnly') IS NOT NULL
            DROP TABLE #acSumBattOnly;
        CREATE TABLE #acSumBattOnly
            (
              QueryStringId INT ,
              AlertConditionId INT ,
              BatterySerialNumber VARCHAR(50) ,
              acSum INT ,
              countTotal INT ,
              AlertCountId INT
            );

        IF OBJECT_ID('tempdb..#acSumDevBatt') IS NOT NULL
            DROP TABLE #acSumDevBatt;
        CREATE TABLE #acSumDevBatt
            (
              QueryStringId INT ,
              AlertConditionId INT ,
              DeviceSerialNumber VARCHAR(50) ,
              BatterySerialNumber VARCHAR(50) ,
              acSum INT ,
              countTotal INT ,
              AlertCountId INT
            );

        IF OBJECT_ID('tempdb..#CreateAlertMessage') IS NOT NULL
            DROP TABLE #CreateAlertMessage;
        CREATE TABLE #CreateAlertMessage
            (
              AlertConditionId INT ,
              QueryStringId INT ,
              AlertCountId INT ,
              alertCount INT
            );

        IF OBJECT_ID('tempdb..#UpdateAlertCount') IS NOT NULL
            DROP TABLE #UpdateAlertCount;
        CREATE TABLE #UpdateAlertCount
            (
              AlertConditionId INT ,
              QueryStringId INT ,
              AlertCountId INT ,
              alertCount INT
            );

        DECLARE @sql VARCHAR(2500);
        DECLARE @logic VARCHAR(2000);
        DECLARE @alertConditionId INT;
        DECLARE @conditionAllowance INT; 
        DECLARE @appliesTo INT;
        DECLARE @batterySerialNumber VARCHAR(50);

        DECLARE alertConditions_cursor CURSOR
        FOR
            SELECT  ROW_ID ,
                    Logic ,
                    ConditionAllowance ,
                    AppliesTo
            FROM    dbo.AlertConditions
            WHERE   [Enable] = 1;

        OPEN alertConditions_cursor;

        FETCH NEXT FROM alertConditions_cursor
		INTO @alertConditionId, @logic, @conditionAllowance, @appliesTo;

        WHILE @@FETCH_STATUS = 0
            BEGIN
                SET @sql = 'select ' + ( CONVERT(VARCHAR, @alertConditionId) )
                    + ' as AlertConditionId
						, QueryStringID
					from #qsToProcess 
					where ' + @logic;

                INSERT  INTO #tAlerts
                        EXECUTE ( @sql
                               );

                IF EXISTS ( SELECT  *
                            FROM    #tAlerts )
                    BEGIN
                        IF @appliesTo = 1
                            BEGIN
				-- Alert is Device Only

				/* ***************************************************************************************
					 for this alertCondition; 
					- get count of how many alerts were generated for this device during this batch
					- find those already in AlertCount and rollup the new sum of alerts generated

					--#acSumDevOnly ( QueryStringId int, AlertConditionId int, DeviceSerialNumber varchar(50), acSum int, countTotal int, AlertCountId int);
				 *************************************************************************************** */
				;
                                WITH    alertSum
                                          AS ( SELECT   MAX(q.QueryStringID) queryStringId ,
                                                        q.DeviceSerialNumber ,
                                                        COUNT(*) deviceSum
                                               FROM     #tAlerts a
                                                        JOIN #qsToProcess q ON a.QueryStringId = q.QueryStringID
                                               GROUP BY q.DeviceSerialNumber
                                             )
                                    INSERT  INTO #acSumDevOnly
                                            SELECT  aSum.queryStringId ,
                                                    @alertConditionId AS AlertConditionId ,
                                                    aSum.DeviceSerialNumber ,
                                                    ISNULL(ac.Count, 0) AS acSum ,
                                                    ( ( ISNULL(ac.Count, 0) )
                                                      + aSum.deviceSum ) AS countTotal ,
                                                    ISNULL(ac.ROW_ID, 0) AS AlertCountID
                                            FROM    alertSum aSum
                                                    LEFT JOIN dbo.AlertCount ac ON ac.DeviceSerialNumber = aSum.DeviceSerialNumber
                                                              AND ac.AlertConditionID = @alertConditionId
                                                              AND ac.AlertCreated = 0;
			

				/* ***************************************************************************************
					- generate a list of alerts that should create a record in the AlertMessage table
						- total alerts generated (now + already in AlertCount) should be >= conditionAllowance
						- should not exist in the AlertMessage table alredy by this @appliesTo category
						- get the most current QueryStringId (largest id) so we can find the most recent Session/NonSession Data packet
			
					--#CreateAlertMessage( AlertConditionId int, QueryStringId int, AlertCountId int, alertCount int);
				 *************************************************************************************** */
                                INSERT  INTO #CreateAlertMessage
                                        SELECT  t.AlertConditionId ,
                                                t.QueryStringId ,
                                                t.AlertCountId ,
                                                t.AlertCount
                                        FROM    ( SELECT    @alertConditionId AS AlertConditionId ,
                                                            MAX(ac.QueryStringId) AS QueryStringId ,
                                                            ac.DeviceSerialNumber AS DeviceSerialNumber ,
                                                            ac.AlertCountId AS AlertCountId ,
                                                            MAX(ac.countTotal) AS AlertCount
                                                  FROM      #acSumDevOnly ac
                                                            OUTER APPLY ( SELECT TOP 1
                                                              1 AS AlertMessageFound
                                                              FROM
                                                              dbo.AlertMessage
                                                              WHERE
                                                              AlertStatus <> 20
                                                              AND DeviceSerialNumber = ac.DeviceSerialNumber
                                                              AND AlertConditionID = @alertConditionId
                                                              ) am
                                                  WHERE     ac.countTotal >= @conditionAllowance
                                                            AND ac.SiteID != 688
                                                            AND am.AlertMessageFound IS NULL
                                                  GROUP BY  ac.DeviceSerialNumber ,
                                                            ac.AlertCountId
                                                ) t
                                        ORDER BY t.DeviceSerialNumber;
			

			
				/* ***************************************************************************************
					- generate a list of alerts that should create a record in the AlertCount table
						- alerts not used in the above CreateAlertMessage list
						- should not exist in the AlertMessage table alredy by this @appliesTo category
						- get the most current QueryStringId (largest id) so we can find the most recent Session/NonSession Data packet
			
					-- #UpdateAlertCount ( AlertConditionId int, QueryStringId int, AlertCountId int, alertCount int);
				 *************************************************************************************** */
                                INSERT  INTO #UpdateAlertCount
                                        SELECT  t.AlertConditionId ,
                                                t.QueryStringId ,
                                                t.AlertCountId ,
                                                t.AlertCount
                                        FROM    ( SELECT    @alertConditionId AS AlertConditionId ,
                                                            MAX(s.QueryStringId) AS QueryStringId ,
                                                            s.DeviceSerialNumber AS DeviceSerialNumber ,
                                                            s.AlertCountId AS AlertCountId ,
                                                            COUNT(*) AS AlertCount
                                                  FROM      #acSumDevOnly s
                                                            LEFT JOIN ( SELECT
                                                              qs.DeviceSerialNumber
                                                              FROM
                                                              #CreateAlertMessage cam
                                                              JOIN #qsToProcess qs ON qs.QueryStringID = cam.QueryStringId
                                                              WHERE
                                                              cam.AlertConditionId = @alertConditionId
                                                              ) dev ON dev.DeviceSerialNumber = s.DeviceSerialNumber
                                                            OUTER APPLY ( SELECT TOP 1
                                                              1 AS AlertMessageFound
                                                              FROM
                                                              dbo.AlertMessage
                                                              WHERE
                                                              AlertStatus <> 20
                                                              AND DeviceSerialNumber = s.DeviceSerialNumber
                                                              AND AlertConditionID = @alertConditionId
                                                              ) am
                                                  WHERE     dev.DeviceSerialNumber IS NULL
                                                            AND am.AlertMessageFound IS NULL
                                                  GROUP BY  s.DeviceSerialNumber ,
                                                            s.AlertCountId
                                                ) t
                                        ORDER BY t.DeviceSerialNumber;

						
                            END;
                        ELSE
                            IF @appliesTo = 2
                                BEGIN
				-- Alert is Battery Only
			
				/* ***************************************************************************************
					 for this alertCondition; 
					- get count of how many alerts were generated for this battery during this batch
					- find those already in AlertCount and rollup the new sum of alerts generated

					--#acSumBattOnly ( QueryStringId int, AlertConditionId int, BatterySerialNumber varchar(50), acSum int, countTotal int, AlertCountId int);
				 *************************************************************************************** */
				;
                                    WITH    alertSum
                                              AS ( SELECT   MAX(q.QueryStringID) queryStringId ,
                                                            q.FixedBatterySerialNumber ,
                                                            COUNT(*) deviceSum
                                                   FROM     #tAlerts a
                                                            JOIN #qsToProcess q ON a.QueryStringId = q.QueryStringID
                                                   GROUP BY q.FixedBatterySerialNumber
                                                 )
                                        INSERT  INTO #acSumBattOnly
                                                SELECT  asum.queryStringId ,
                                                        @alertConditionId AS AlertConditionId ,
                                                        asum.FixedBatterySerialNumber ,
                                                        ISNULL(ac.Count, 0) AS acSum ,
                                                        ( ( ISNULL(ac.Count, 0) )
                                                          + asum.deviceSum ) AS countTotal ,
                                                        ISNULL(ac.ROW_ID, 0) AS AlertCountID
                                                FROM    alertSum asum
                                                        LEFT JOIN dbo.AlertCount ac ON ac.BatterySerialNumber = asum.FixedBatterySerialNumber
                                                              AND ac.AlertConditionID = @alertConditionId
                                                              AND ac.AlertCreated = 0;
			

				/* ***************************************************************************************
					- generate a list of alerts that should create a record in the AlertMessage table
						- total alerts generated (now + already in AlertCount) should be >= conditionAllowance
						- should not exist in the AlertMessage table alredy by this @appliesTo category
						- get the most current QueryStringId (largest id) so we can find the most recent Session/NonSession Data packet
			
					--#CreateAlertMessage( AlertConditionId int, QueryStringId int, AlertCountId int, alertCount int);
				 *************************************************************************************** */
                                    INSERT  INTO #CreateAlertMessage
                                            SELECT  t.AlertConditionId ,
                                                    t.QueryStringId ,
                                                    t.AlertCountId ,
                                                    t.AlertCount
                                            FROM    ( SELECT  @alertConditionId AS AlertConditionId ,
                                                              MAX(ac.QueryStringId) AS QueryStringId ,
                                                              ac.BatterySerialNumber AS BatterySerialNumber ,
                                                              ac.AlertCountId AS AlertCountId ,
                                                              MAX(ac.countTotal) AS AlertCount
                                                      FROM    #acSumBattOnly ac
                                                              OUTER APPLY ( SELECT TOP 1
                                                              1 AS AlertMessageFound
                                                              FROM
                                                              dbo.AlertMessage
                                                              WHERE
                                                              AlertStatus <> 20
                                                              AND BatterySerialNumber = ac.BatterySerialNumber
                                                              AND AlertConditionID = @alertConditionId
                                                              ) am
                                                      WHERE   ac.countTotal >= @conditionAllowance
                                                              AND am.AlertMessageFound IS NULL
                                                      GROUP BY ac.BatterySerialNumber ,
                                                              ac.AlertCountId
                                                    ) t
                                            ORDER BY t.BatterySerialNumber;
			

			
				/* ***************************************************************************************
					- generate a list of alerts that should create a record in the AlertCount table
						- alerts not used in the above CreateAlertMessage list
						- should not exist in the AlertMessage table alredy by this @appliesTo category
						- get the most current QueryStringId (largest id) so we can find the most recent Session/NonSession Data packet
			
					-- #UpdateAlertCount ( AlertConditionId int, QueryStringId int, AlertCountId int, alertCount int);
				 *************************************************************************************** */
                                    INSERT  INTO #UpdateAlertCount
                                            SELECT  t.AlertConditionId ,
                                                    t.QueryStringId ,
                                                    t.AlertCountId ,
                                                    t.AlertCount
                                            FROM    ( SELECT  @alertConditionId AS AlertConditionId ,
                                                              MAX(s.QueryStringId) AS QueryStringId ,
                                                              s.BatterySerialNumber AS BatterySerialNumber ,
                                                              s.AlertCountId AS AlertCountId ,
                                                              COUNT(*) AS AlertCount
                                                      FROM    #acSumBattOnly s
                                                              LEFT JOIN ( SELECT
                                                              qs.FixedBatterySerialNumber
                                                              FROM
                                                              #CreateAlertMessage cam
                                                              JOIN #qsToProcess qs ON qs.QueryStringID = cam.QueryStringId
                                                              WHERE
                                                              cam.AlertConditionId = @alertConditionId
                                                              ) dev ON dev.FixedBatterySerialNumber = s.BatterySerialNumber
                                                              OUTER APPLY ( SELECT TOP 1
                                                              1 AS AlertMessageFound
                                                              FROM
                                                              dbo.AlertMessage
                                                              WHERE
                                                              AlertStatus <> 20
                                                              AND BatterySerialNumber = s.BatterySerialNumber
                                                              AND AlertConditionID = @alertConditionId
                                                              ) am
                                                      WHERE   dev.FixedBatterySerialNumber IS NULL
                                                              AND am.AlertMessageFound IS NULL
                                                      GROUP BY s.BatterySerialNumber ,
                                                              s.AlertCountId
                                                    ) t
                                            ORDER BY t.BatterySerialNumber; 
                                END;
                            ELSE
                                BEGIN
				-- Alert is Device and Battery
			
				/* ***************************************************************************************
					 for this alertCondition; 
					- get count of how many alerts were generated for this device during this batch
					- find those already in AlertCount and rollup the new sum of alerts generated

					--#acSumDevBatt ( QueryStringId int, AlertConditionId int, DeviceSerialNumber varchar(50), BatterySerialNumber varchar(50), acSum int, countTotal int, AlertCountId int);
				 *************************************************************************************** */
				;
                                    WITH    alertSum
                                              AS ( SELECT   MAX(q.QueryStringID) queryStringId ,
                                                            q.DeviceSerialNumber ,
                                                            q.FixedBatterySerialNumber ,
                                                            COUNT(*) deviceSum
                                                   FROM     #tAlerts a
                                                            JOIN #qsToProcess q ON a.QueryStringId = q.QueryStringID
                                                   GROUP BY q.DeviceSerialNumber ,
                                                            q.FixedBatterySerialNumber
                                                 )
                                        INSERT  INTO #acSumDevBatt
                                                SELECT  aSum.queryStringId ,
                                                        @alertConditionId AS AlertConditionId ,
                                                        aSum.DeviceSerialNumber ,
                                                        aSum.FixedBatterySerialNumber ,
                                                        ISNULL(ac.Count, 0) AS acSum ,
                                                        ( ( ISNULL(ac.Count, 0) )
                                                          + aSum.deviceSum ) AS countTotal ,
                                                        ISNULL(ac.ROW_ID, 0) AS AlertCountID
                                                FROM    alertSum aSum
                                                        LEFT JOIN dbo.AlertCount ac ON ac.DeviceSerialNumber = aSum.DeviceSerialNumber
                                                              AND ac.BatterySerialNumber = aSum.FixedBatterySerialNumber
                                                              AND ac.AlertConditionID = @alertConditionId
                                                WHERE   ac.AlertCreated = 0;
			

				/* ***************************************************************************************
					- generate a list of alerts that should create a record in the AlertMessage table
						- total alerts generated (now + already in AlertCount) should be >= conditionAllowance
						- should not exist in the AlertMessage table alredy by this @appliesTo category
						- get the most current QueryStringId (largest id) so we can find the most recent Session/NonSession Data packet
			
					--#CreateAlertMessage( AlertConditionId int, QueryStringId int, AlertCountId int, alertCount int);
				 *************************************************************************************** */
                                    INSERT  INTO #CreateAlertMessage
                                            SELECT  t.AlertConditionId ,
                                                    t.QueryStringId ,
                                                    t.AlertCountId ,
                                                    t.AlertCount
                                            FROM    ( SELECT  @alertConditionId AS AlertConditionId ,
                                                              MAX(ac.QueryStringId) AS QueryStringId ,
                                                              ac.DeviceSerialNumber AS DeviceSerialNumber ,
                                                              ac.BatterySerialNumber AS BatterySerialNumber ,
                                                              ac.AlertCountId AS AlertCountId ,
                                                              MAX(ac.countTotal) AS AlertCount
                                                      FROM    #acSumDevBatt ac
                                                              OUTER APPLY ( SELECT TOP 1
                                                              1 AS AlertMessageFound
                                                              FROM
                                                              dbo.AlertMessage
                                                              WHERE
                                                              AlertStatus <> 20
                                                              AND DeviceSerialNumber = ac.DeviceSerialNumber
                                                              AND BatterySerialNumber = ac.BatterySerialNumber
                                                              AND AlertConditionID = @alertConditionId
                                                              ) am
                                                      WHERE   ac.countTotal >= @conditionAllowance
                                                              AND am.AlertMessageFound IS NULL
                                                      GROUP BY ac.DeviceSerialNumber ,
                                                              ac.BatterySerialNumber ,
                                                              ac.AlertCountId
                                                    ) t
                                            ORDER BY t.DeviceSerialNumber ,
                                                    t.BatterySerialNumber;
			

			
				/* ***************************************************************************************
					- generate a list of alerts that should create a record in the AlertCount table
						- alerts not used in the above CreateAlertMessage list
						- should not exist in the AlertMessage table alredy by this @appliesTo category
						- get the most current QueryStringId (largest id) so we can find the most recent Session/NonSession Data packet
			
					-- #UpdateAlertCount ( AlertConditionId int, QueryStringId int, AlertCountId int, alertCount int);
				 *************************************************************************************** */
                                    INSERT  INTO #UpdateAlertCount
                                            SELECT  t.AlertConditionId ,
                                                    t.QueryStringId ,
                                                    t.AlertCountId ,
                                                    t.AlertCount
                                            FROM    ( SELECT  @alertConditionId AS AlertConditionId ,
                                                              MAX(s.QueryStringId) AS QueryStringId ,
                                                              s.DeviceSerialNumber AS DeviceSerialNumber ,
                                                              s.BatterySerialNumber AS BatterySerialNumber ,
                                                              s.AlertCountId AS AlertCountId ,
                                                              COUNT(*) AS AlertCount
                                                      FROM    #acSumDevBatt s
                                                              LEFT JOIN ( SELECT
                                                              qs.DeviceSerialNumber ,
                                                              qs.FixedBatterySerialNumber
                                                              FROM
                                                              #CreateAlertMessage cam
                                                              JOIN #qsToProcess qs ON qs.QueryStringID = cam.QueryStringId
                                                              WHERE
                                                              cam.AlertConditionId = @alertConditionId
                                                              ) dev ON dev.DeviceSerialNumber = s.DeviceSerialNumber
                                                              AND dev.FixedBatterySerialNumber = s.BatterySerialNumber
                                                              OUTER APPLY ( SELECT TOP 1
                                                              1 AS AlertMessageFound
                                                              FROM
                                                              dbo.AlertMessage
                                                              WHERE
                                                              AlertStatus <> 20
                                                              AND DeviceSerialNumber = s.DeviceSerialNumber
                                                              AND BatterySerialNumber = s.BatterySerialNumber
                                                              AND AlertConditionID = @alertConditionId
                                                              ) am
                                                      WHERE   dev.DeviceSerialNumber IS NULL
                                                              AND am.AlertMessageFound IS NULL
                                                      GROUP BY s.DeviceSerialNumber ,
                                                              s.BatterySerialNumber ,
                                                              s.AlertCountId
                                                    ) t
                                            ORDER BY t.DeviceSerialNumber ,
                                                    t.BatterySerialNumber;
                                END;
		
                        TRUNCATE TABLE #tAlerts;
                        TRUNCATE TABLE #acSumDevOnly;
                        TRUNCATE TABLE #acSumBattOnly;
                        TRUNCATE TABLE #acSumDevBatt;
                    END;
	
                FETCH NEXT FROM alertConditions_cursor
		INTO @alertConditionId, @logic, @conditionAllowance, @appliesTo;
            END;
        CLOSE alertConditions_cursor;
        DEALLOCATE alertConditions_cursor;


	--=================================================================
	-- Create AlertMessage records for those that match in SessionData
	--=================================================================
        INSERT  INTO dbo.AlertMessage
                ( [CreatedDateUTC] ,
                  [DeviceSerialNumber] ,
                  [BatterySerialNumber] ,
                  [Description] ,
                  [Notes] ,
                  [SiteID] ,
                  [EmailQueueID] ,
                  [AlertConditionID] ,
                  [AlertStatus] ,
                  [QueuedForEmailUTC] ,
                  [Priority] ,
                  [TablePointer] ,
                  [RowPointer] ,
                  [ModifiedDateUTC] ,
                  [ModifiedUserID] ,
                  [Bay] ,
                  [APMAC] ,
                  [Source] ,
                  [FriendlyDescription]
                )
                SELECT  qs.CreatedDateUTC									-- CreatedDateUTC
                        ,
                        qs.DeviceSerialNumber								-- DeviceSerialNumber
                        ,
                        qs.FixedBatterySerialNumber						-- BatterySerialNumber
                        ,
                        ac.Description									-- Description
                        ,
                        ''												-- Notes
                        ,
                        d.SiteID									-- BusinessUnitID
                        ,
                        0													-- EmailQueueID
                        ,
                        t.AlertConditionId								-- AlertConditionId
                        ,
                        CASE WHEN ac.AlertType = 211 THEN 15
                             ELSE 0
                        END	-- AlertStatus
                        ,
                        '01/01/1900'										-- QueuedForEmailUTC
                        ,
                        ac.AlertType										-- Priority
                        ,
                        'S'												-- TablePointer
                        ,
                        sd.row_id											-- RowPointer
                        ,
                        GETUTCDATE()										-- ModifiedDateUTC
                        ,
                        0													-- ModifiedUserID
                        ,
                        qs.Bay											-- Bay
                        ,
                        qs.APMAC											-- APMAC
                        ,
                        'S'												-- Source
                        ,
                        ac.FriendlyDescription							-- FriendlyDescription
                FROM    #qsToProcess qs
                        JOIN #CreateAlertMessage t ON t.QueryStringId = qs.QueryStringID
                        JOIN dbo.AlertConditions ac ON ac.ROW_ID = t.AlertConditionId
                        JOIN dbo.Assets d ON d.SerialNo = qs.DeviceSerialNumber
                        JOIN SessionData sd ON sd.QueryStringID = qs.QueryStringID
                                               AND sd.QueryStringBatchID = qs.QueryStringBatchID
                WHERE   qs.IsSessionData = 1;
		

	--=================================================================
	-- Create AlertMessage records for those that match in NonSessionData
	--=================================================================
        INSERT  INTO dbo.AlertMessage
                ( [CreatedDateUTC] ,
                  [DeviceSerialNumber] ,
                  [BatterySerialNumber] ,
                  [Description] ,
                  [Notes] ,
                  [SiteID] ,
                  [EmailQueueID] ,
                  [AlertConditionID] ,
                  [AlertStatus] ,
                  [QueuedForEmailUTC] ,
                  [Priority] ,
                  [TablePointer] ,
                  [RowPointer] ,
                  [ModifiedDateUTC] ,
                  [ModifiedUserID] ,
                  [Bay] ,
                  [APMAC] ,
                  [Source] ,
                  [FriendlyDescription]
                )
                SELECT  qs.CreatedDateUTC									-- CreatedDateUTC
                        ,
                        qs.DeviceSerialNumber								-- DeviceSerialNumber
                        ,
                        qs.FixedBatterySerialNumber						-- BatterySerialNumber
                        ,
                        ac.Description									-- Description
                        ,
                        ''												-- Notes
                        ,
                        d.SiteID									-- BusinessUnitID
                        ,
                        0													-- EmailQueueID
                        ,
                        t.AlertConditionId								-- AlertConditionId
                        ,
                        CASE WHEN ac.AlertType = 211 THEN 15
                             ELSE 0
                        END	-- AlertStatus
                        ,
                        '01/01/1900'										-- QueuedForEmailUTC
                        ,
                        ac.AlertType										-- Priority
                        ,
                        'N'												-- TablePointer
                        ,
                        sd.row_id											-- RowPointer
                        ,
                        GETUTCDATE()										-- ModifiedDateUTC
                        ,
                        0													-- ModifiedUserID
                        ,
                        qs.Bay											-- Bay
                        ,
                        qs.APMAC											-- APMAC
                        ,
                        'N'												-- Source
                        ,
                        ac.FriendlyDescription							-- FriendlyDescription
                FROM    #qsToProcess qs
                        JOIN #CreateAlertMessage t ON t.QueryStringId = qs.QueryStringID
                        JOIN dbo.AlertConditions ac ON ac.ROW_ID = t.AlertConditionId
                        JOIN dbo.Assets d ON d.SerialNo = qs.DeviceSerialNumber
                        JOIN NonSessionData sd ON sd.QueryStringID = qs.QueryStringID
                                                  AND sd.QueryStringBatchID = qs.QueryStringBatchID
                WHERE   qs.IsSessionData = 0;

	--=================================================================
	-- Insert New AlertCount records for those alerts that haven't hit their conditionalAllowance yet, and we don't have an AlertCount record yet
	--=================================================================
        INSERT  INTO dbo.AlertCount
                ( [CreatedDate] ,
                  [CreatedDateUTC] ,
                  [DeviceSerialNumber] ,
                  [BatterySerialNumber] ,
                  [AlertConditionID] ,
                  [Count]
                )
                SELECT  GETDATE()						-- CreatedDate
                        ,
                        GETUTCDATE()					-- CreatedDateUTC
                        ,
                        q.DeviceSerialNumber			-- DeviceSerialNumber
                        ,
                        q.FixedBatterySerialNumber	-- BatterySerialNumber
                        ,
                        t.AlertConditionId			-- AlertConditionId
                        ,
                        t.alertCount					-- Count
                FROM    #UpdateAlertCount t
                        JOIN #qsToProcess q ON q.QueryStringID = t.QueryStringId;
			

	--=================================================================
	-- Update AlertCount for those alerts that haven't hit their conditionalAllowance yet
	--=================================================================
        UPDATE  dbo.AlertCount
        SET     [Count] = ac.Count + t.alertCount
        FROM    #UpdateAlertCount t
                JOIN dbo.AlertCount ac ON ac.ROW_ID = t.AlertCountId
        WHERE   t.AlertCountId > 0
                AND ac.AlertCreated = 0;

	--=================================================================
	-- Delete AlertCount records for those that we created alerts for
	--=================================================================
	--DELETE FROM AlertCount 
	--		WHERE ROW_ID IN (select AlertCountId from #CreateAlertMessage where AlertCountId > 0)
			

        UPDATE  dbo.AlertCount
        SET     AlertCreated = 1
        WHERE   ROW_ID IN ( SELECT  AlertCountId
                            FROM    #CreateAlertMessage
                            WHERE   AlertCountId > 0
                                    AND ( AlertCreated = 0
                                          OR AlertCreated IS NULL
                                        ) );


        IF OBJECT_ID('tempdb..#tAlerts') IS NOT NULL
            DROP TABLE #tAlerts;
        IF OBJECT_ID('tempdb..#acSumDevOnly') IS NOT NULL
            DROP TABLE #acSumDevOnly;
        IF OBJECT_ID('tempdb..#acSumBattOnly') IS NOT NULL
            DROP TABLE #acSumBattOnly;
        IF OBJECT_ID('tempdb..#acSumDevBatt') IS NOT NULL
            DROP TABLE #acSumDevBatt;
        IF OBJECT_ID('tempdb..#CreateAlertMessage') IS NOT NULL
            DROP TABLE #CreateAlertMessage;
        IF OBJECT_ID('tempdb..#UpdateAlertCount') IS NOT NULL
            DROP TABLE #UpdateAlertCount;
        IF OBJECT_ID('tempdb..#qsToProcess') IS NOT NULL
            DROP TABLE #qsToProcess;

    END;

GO
