SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[spBatteries_Get_ByCustomer] @CustomerID INT
AS
    BEGIN	
        SET NOCOUNT ON;
		
	--Test Variables
	--declare @UserIsStinger varchar(5) = 'True'
	--declare @UserID int = 813
		
        SELECT DISTINCT
                parentbu.CustomerName AS ParentBusinessUnitName ,
                bu.SiteDescription AS FacilityName ,
                d.Retired ,
                d.SerialNo ,
                d.MaxCycleCount ,
                d.Description ,
                CASE WHEN d.IsActive = 0 THEN 'N'
                     WHEN d.IsActive = 1 THEN 'Y'
                     ELSE 'N'
                END AS IsActive ,
                d.IsActive AS bActive ,
                d.CreatedDateUTC ,
                COALESCE(d.LastPostDateUTC, d.CreatedDateUTC) AS LastPostDateUTC ,
                COALESCE(CONVERT(VARCHAR(5), sd.FullChargeCapacity), 'N/A') AS FCC ,
                COALESCE(CONVERT(VARCHAR(5), d.CycleCount), 'N/A') AS CycleCount ,
                sd.FullChargeCapacity ,
                COALESCE(CAST(( CAST(sd.FullChargeCapacity AS DECIMAL(7, 2))
                                / CAST(22500 AS DECIMAL(7, 2)) * 100 ) AS DECIMAL(7,
                                                              2)), 0) AS CalcCapacity ,
                CASE WHEN w.WarrantyEndCode = 0
                          AND w.ExpirationDate >= GETDATE() THEN 'Y'
                     ELSE 'N'
                END AS InWarranty
        FROM    dbo.Assets d
                LEFT JOIN dbo.viewSessionData sd ON d.SerialNo = sd.BatterySerialNumber
                LEFT JOIN dbo.AccessPoint ap ON d.AccessPointId = ap.ROW_ID
                LEFT JOIN dbo.Warranty w ON w.SerialNumber = d.SerialNo
                                        AND w.ROW_ID = ( SELECT
                                                              ROW_ID
                                                         FROM dbo.MostRecentPONumberBySerialNo(d.SerialNo)
                                                       )
                LEFT JOIN dbo.Sites apbu ON ap.SiteID = apbu.IDSite
                LEFT JOIN dbo.Sites bu ON d.SiteID = bu.IDSite
                LEFT JOIN dbo.Customers parentbu ON bu.CustomerID = parentbu.IDCustomer
                LEFT JOIN dbo.Departments dept ON d.DepartmentID = dept.IDDepartment
        WHERE   d.IDAssetType = 5
                AND ( parentbu.IDCustomer = @CustomerID
                      OR bu.CustomerID = @CustomerID
                    )
        ORDER BY FacilityName ,
                d.SerialNo;
    END;




GO
