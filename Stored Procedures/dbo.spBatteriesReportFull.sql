SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[spBatteriesReportFull] @SiteID INT = NULL
AS
    BEGIN

--select max(account) as Account,SerialNo,count(serialno) as cnt,max([Warranty Years]) as 'Warranty Years',max([Expiration Date]) as 'Expiration Date',max(CycleCount) as 'CycleCount',max(MaxCycleCount) as 'MaxCycleCount',max([Capacity Health]) as 'Capacity Health %',max(FullChargeCapacity) as 'FullChargeCapacity' ,max(lastpostdateutc) as 'LastPostDateUTC' INTO #tfullrpt 
--from (
-- select isnull(s.SiteName,cus.CustomerName) as Account,ass.SerialNo,wd.DurationYears as 'Warranty Years', Convert(Date, dateadd(day,isnull( (select durationdays  from warrantyduration where serialnumberprefix = left(ass.SerialNo,7)),5000),cast('20' + Substring(ass.Serialno,8,2) + '-' + Substring(ass.SerialNo,10,2) + '-01' as datetime) )) as 'Expiration Date',ass.CycleCount,ass.MaxCycleCount,isnull(nullif([dbo].[fnGetBatteryCapacity_Int] (ass.SerialNo,ass.FullChargeCapacity),0),0) as 'Capacity Health', ass.FullChargeCapacity,ass.LastPostDateUTC from assets ass left join sites s on s.idsite = ass.siteid left join Customers cus on ass.SiteID = cus.idcustomer left join warrantyduration wd on wd.SerialNumberPrefix = left(ass.serialno,7) where idassettype = 5 and 
--  [dbo].[SerialNumberClassification](ass.serialno) = 1 and ass.MaxCycleCount <> 0 and ass.MaxCycleCount <> 10 and SiteID not in (4,28,1125) and fullchargecapacity > 0 ) 
-- fullrpt group by SerialNo -- order by fullrpt.Account asc,SerialNo desc 

-- select Account,Serialno as 'SerialNumber', isnull((select Generation from  BatteryPartlist where PartNumber = left(#tfullrpt.SerialNo,7)),'3.5') as Generation,[Warranty Years],[Expiration Date],CycleCount,MaxCycleCount,[Capacity Health %],FullChargeCapacity,LastPostDateUTC as 'Last Reported To Pulse', (Select count(idasset) from assets where IDAssetType = 5 and siteid = (select siteid from assets where serialno = #tfullrpt.SerialNo)) as 'Total Batteries at Facility', cast(cast(#tfullrpt.[FullChargeCapacity] as decimal(18,2))/cast(2400.00 as decimal(18,2)) *0.85 as decimal(18,2)) as 'Estimated Run Hours'  , isnull((select top 1 CustomerAcct from SFCustomer where SiteID = (select siteid from assets where serialno = #tfullrpt.SerialNo)),('NA')) as 'SF Customer Acct #' , isnull(('http://ipaddress.is/' + (Select top 1 SourceIPAddress from assets where SiteID = (select siteid from Assets where serialno=  #tfullrpt.SerialNo) and SourceIPAddress is not null)),('http://ipaddress.is/' + (select top 1 (SourceIPaddressBase + '222') from SiteIPSubnet where siteid = (select top 1 siteid from assets where serialno = #tfullrpt.SerialNo) ))) as 'Geographical Info Link (St,Zip,Area)' from #tfullrpt order by Account


      --  DROP TABLE #tfullrpt2;
        SELECT  MAX(fullrpt.Account) AS Account ,
                fullrpt.SerialNo ,
                COUNT(fullrpt.SerialNo) AS cnt ,
                MAX(fullrpt.[Warranty Years]) AS 'Warranty Years' ,
                MAX(fullrpt.[Expiration Date]) AS 'Expiration Date' ,
                MAX(fullrpt.CycleCount) AS 'CycleCount' ,
                MAX(fullrpt.MaxCycleCount) AS 'MaxCycleCount' ,
                MAX(fullrpt.[Capacity Health]) AS 'Capacity Health %' ,
                MAX(ISNULL(fullrpt.FullChargeCapacity,0)) AS 'FullChargeCapacity' ,
                MAX(fullrpt.LastPostDateUTC) AS 'LastPostDateUTC'
        INTO    #tfullrpt2
        FROM    ( SELECT    ISNULL(s.SiteName, cus.CustomerName) AS Account ,
                            ass.SerialNo ,
                            wd.DurationYears AS 'Warranty Years' ,
                            CONVERT(DATE, DATEADD(DAY,
                                                  ISNULL(( SELECT
                                                              DurationDays
                                                           FROM
                                                              dbo.WarrantyDuration
                                                           WHERE
                                                              SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
                                                         ), 5000),
                                                  CAST('20'
                                                  + SUBSTRING(ass.SerialNo, 8,
                                                              2) + '-'
                                                  + SUBSTRING(ass.SerialNo, 10,
                                                              2) + '-01' AS DATETIME))) AS 'Expiration Date' ,
                            ass.CycleCount ,
                            ass.MaxCycleCount ,
                            ISNULL(NULLIF(dbo.fnGetBatteryCapacity_Int(ass.SerialNo,
                                                              ass.FullChargeCapacity),
                                          0), 0) AS 'Capacity Health' ,
                            ass.FullChargeCapacity ,
                            ass.LastPostDateUTC
                  FROM      dbo.Assets ass
                            LEFT JOIN dbo.Sites s ON s.IDSite = ass.SiteID
                            LEFT JOIN dbo.Customers cus ON ass.SiteID = cus.IDCustomer
                            LEFT JOIN dbo.WarrantyDuration wd ON wd.SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
                  WHERE     ass.IDAssetType = 5
                            AND dbo.SerialNumberClassification(ass.SerialNo) = 1
                            AND ass.Retired = 0
  --and ass.MaxCycleCount <> 0 and ass.MaxCycleCount <> 10 
                            AND ass.SiteID NOT IN ( 4, 28, 1125 )
                         --   AND ass.SiteID = @SiteID
  -- and fullchargecapacity > 0 
                ) fullrpt
        GROUP BY fullrpt.SerialNo; -- order by fullrpt.Account asc,SerialNo desc 

        SELECT  Account ,
                SerialNo AS 'SerialNumber' ,
                ISNULL(( SELECT Generation
                         FROM   dbo.BatteryPartList
                         WHERE  PartNumber = LEFT(#tfullrpt2.SerialNo, 7)
                       ), '3.5') AS Generation ,
                [Warranty Years] ,
                [Expiration Date] ,
                CycleCount ,
                MaxCycleCount ,
                [Capacity Health %] ,
                FullChargeCapacity ,
                IIF(LastPostDateUTC < DATEADD(MONTH, -1, GETDATE()), 'No', 'Yes') AS 'Recently Reported to Pulse' ,
                LastPostDateUTC AS 'Last Reported To Pulse' ,
                ( SELECT    COUNT(IDAsset)
                  FROM      dbo.Assets
                  WHERE     IDAssetType = 5
                            AND SiteID = ( SELECT   SiteID
                                           FROM     dbo.Assets
                                           WHERE    SerialNo = #tfullrpt2.SerialNo
                                         )
                ) AS 'Total Batteries at Facility' ,
                CAST(CAST(FullChargeCapacity AS DECIMAL(18, 2))
                / CAST(2400.00 AS DECIMAL(18, 2)) * 0.85 AS DECIMAL(18, 2)) AS 'Estimated Run Hours' ,
                ISNULL(( SELECT TOP 1
                                ISNULL(sfc.CustomerAcct, sfa.CustomerAcct)
                         FROM   dbo.SFCustomer sfc
                                RIGHT JOIN dbo.SFAccounts sfa ON sfc.CustomerAcct = sfa.CustomerAcct
                         WHERE  sfc.SiteID = ( SELECT   SiteID
                                               FROM     dbo.Assets
                                               WHERE    SerialNo = #tfullrpt2.SerialNo
                                             )
                       ),
                       ( SELECT SfCustomerAcctID
                         FROM   dbo.Sites
                         WHERE  IDSite = ( SELECT   SiteID
                                           FROM     dbo.Assets
                                           WHERE    SerialNo = #tfullrpt2.SerialNo
                                         )
                       )) AS 'SF Customer Acct #' ,
                ISNULL(( 'http://ipaddress.is/'
                         + ( SELECT TOP 1
                                    SourceIPAddress
                             FROM   dbo.Assets
                             WHERE  SiteID = ( SELECT   SiteID
                                               FROM     dbo.Assets
                                               WHERE    SerialNo = #tfullrpt2.SerialNo
                                             )
                                    AND SourceIPAddress IS NOT NULL
                           ) ),
                       ( 'http://ipaddress.is/'
                         + ( SELECT TOP 1
                                    ( SourceIPAddressBase + '222' )
                             FROM   dbo.SiteIPSubnet
                             WHERE  SiteID = ( SELECT TOP 1
                                                        SiteID
                                               FROM     dbo.Assets
                                               WHERE    SerialNo = #tfullrpt2.SerialNo
                                             )
                           ) )) AS 'Geographical Info Link (St,Zip,Area)'
        FROM    #tfullrpt2
        ORDER BY Account;

    END;
GO
