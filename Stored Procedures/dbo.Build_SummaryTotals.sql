SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*################################################################################################
 Name				: Build_SummaryTotals
 Date				: 
 Author				: christopher.stewart
 Company				: enovate 
 Purpose				: Build SummaryTotals
 Usage				:
 Impact				:
 Required Perm			:   
 Called by			:   
##################################################################################################
 ver		author					date			change  
 1.0		Arlow.Farrell			Unknown			initial- part of [spSummarySiteDepartment_RollupNow]
 1.1		Christopher.Stewart		09302016		Optimizition and Comments
#################################################################################################*/

/*##################################################################################################
 STEPS	  Description						 
 1		  
 2
 3
 4
 5


#################################################################################################*/

CREATE PROCEDURE [dbo].[Build_SummaryTotals]
AS

DECLARE @Yesterdate VARCHAR(10);
DECLARE @LapTimer DATETIME2;

SET @Yesterdate = CONVERT(VARCHAR(10), DATEADD(DAY, -1, GETDATE()), 111);
PRINT @Yesterdate;


PRINT dbo.GetMessageBorder();
/*################################################################################################*/
    Step_1:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 1'; 
    RAISERROR('Delete any existing records from same day from SummaryTotals',0,1) WITH NOWAIT;
							 
/*################################################################################################*/


        DELETE  FROM dbo.SummaryTotals
        WHERE   SummaryDate = @Yesterdate;

PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_2:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 2';
    RAISERROR('Populate SummaryTotals for individual sites',0,1) WITH NOWAIT;

/*################################################################################################*/


            INSERT  INTO dbo.SummaryTotals
                ( SummaryDate ,
                  SortKey ,
                  HQID ,
                  SiteID ,
                  BatteryCount_Purchased ,
                  BatteryCount_Active ,
                  ChargerCount_Purchased ,
                  ChargerCount_Active ,
                  WorkstationCount_Active ,
                  WorkstationCount_Purchased
                )
                SELECT  @Yesterdate AS SummaryDate ,
                        1 AS SortKey ,
                        bu.CustomerID AS HQID ,
                        bu.IDSite AS SiteID ,
                        SUM(COALESCE(s.BatteryCount, 0)) AS BatteryCount ,
                        SUM(COALESCE(s.ActiveBatteryCount, 0)) AS ActiveBatteryCount ,
                        SUM(COALESCE(s.ChargerCount, 0)) AS ChargerCount ,
                        SUM(COALESCE(s.ActiveChargerCount, 0)) AS ActiveChargerCount ,
                        0 AS WorkstationCount_Active ,
                        0 AS WorkstationCount_Purchased
                FROM    dbo.Sites bu
                        LEFT JOIN dbo.SummarySiteAssets s ON s.IDSite = bu.IDSite
                WHERE   bu.CustomerID <> 0
                GROUP BY bu.CustomerID ,
                        bu.SiteDescription ,
                        bu.IDSite;

PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_3:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 3';
    RAISERROR('Populate SummaryTotals for HQs',0,1) WITH NOWAIT;

/*################################################################################################*/
        INSERT  INTO dbo.SummaryTotals
                ( SummaryDate ,
                  SortKey ,
                  HQID ,
                  BatteryCount_Purchased ,
                  BatteryCount_Active ,
                  ChargerCount_Purchased ,
                  ChargerCount_Active ,
                  WorkstationCount_Active ,
                  WorkstationCount_Purchased
                )
                SELECT  @Yesterdate AS SummaryDate ,
                        0 AS SortKey ,
                        HQID ,
                        SUM(COALESCE(BatteryCount_Purchased, 0)) AS BatteryCount_Purchased ,
                        SUM(COALESCE(BatteryCount_Active, 0)) AS BatteryCount_Active ,
                        SUM(COALESCE(ChargerCount_Purchased, 0)) AS ChargerCount_Purchased ,
                        SUM(COALESCE(ChargerCount_Active, 0)) AS ChargerCount_Active ,
                        0 AS WorkstationCount_Active ,
                        0 AS WorkstationCount_Purchased
                FROM    dbo.SummaryTotals
                WHERE   SummaryDate = @Yesterdate
                GROUP BY HQID;


PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_4:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 4';
    RAISERROR('Update Workstation Counts for HQs',0,1) WITH NOWAIT;

/*################################################################################################*/




	-- now get workstation counts from SummaryBusinessUnitDepartment

	--   first for HQs
        UPDATE  dbo.SummaryTotals
        SET     WorkstationCount_Active = a.ActiveWorkstationCount ,
                WorkstationCount_Purchased = COALESCE(a.ActiveWorkstationCount
                                                      + a.AvailableWorkstationCount
                                                      + a.OfflineWorkstationCount,
                                                      0)
        FROM    ( SELECT    [Date] AS SummaryDate ,
                            IDCustomer AS HQID ,
                            NULL AS SiteID ,
                            COALESCE(SUM(ActiveWorkstationCount), 0) AS ActiveWorkstationCount ,
                            COALESCE(SUM(AvailableWorkstationCount), 0) AS AvailableWorkstationCount ,
                            COALESCE(SUM(OfflineWorkstationCount), 0) AS OfflineWorkstationCount
                  FROM      dbo.SummarySiteDepartment
                  WHERE     [Date] = @Yesterdate
                            AND IDCustomer <> 0
                  GROUP BY  [Date] ,
                            IDCustomer
                ) a
        WHERE   SummaryTotals.SummaryDate = a.SummaryDate
                AND SummaryTotals.HQID = a.HQID
                AND SummaryTotals.SiteID IS NULL;


PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_5:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 5';
    RAISERROR('Update Workstation Counts for individual sites',0,1) WITH NOWAIT;

/*################################################################################################*/


        UPDATE  dbo.SummaryTotals
        SET     WorkstationCount_Active = a.ActiveWorkstationCount ,
                WorkstationCount_Purchased = COALESCE(a.ActiveWorkstationCount
                                                      + a.AvailableWorkstationCount
                                                      + a.OfflineWorkstationCount,
                                                      0)
        FROM    ( SELECT    [Date] AS SummaryDate ,
                            IDCustomer AS HQID ,
                            IDSite ,
                            COALESCE(SUM(ActiveWorkstationCount), 0) AS ActiveWorkstationCount ,
                            COALESCE(SUM(AvailableWorkstationCount), 0) AS AvailableWorkstationCount ,
                            COALESCE(SUM(OfflineWorkstationCount), 0) AS OfflineWorkstationCount
                  FROM      dbo.SummarySiteDepartment
                  WHERE     [Date] = @Yesterdate
                            AND IDCustomer <> 0
                  GROUP BY  [Date] ,
                            IDCustomer ,
                            IDSite
                ) a
        WHERE   SummaryTotals.SummaryDate = a.SummaryDate
                AND SummaryTotals.HQID = a.HQID
                AND SiteID = a.IDSite;
	

PRINT dbo.GetLapTime(@LapTimer); 
GO
