SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Arlow Farrell
-- Create date: 2012-07-17
-- Description:	Appends run rate data to
--              SummaryBatteryRunRate, summarizing
--              any data accumulated since this
--              routine last ran.
--
--              Updates Job table to keep record
--              of when this job last ran.
--              Note: The date value is actually
--              the '@ToDate' value used when
--              this routine last ran.
-- =============================================
CREATE PROCEDURE [dbo].[spSummary_BatteryRunRate_Append]
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        DECLARE @FromDate DATETIME;
        DECLARE @ToDate DATETIME;
        DECLARE @LastRanTimestampUTC DATETIME;
        DECLARE @JobCode VARCHAR(10);

	-- FromDate should be the day after the ToDate which was used when this job last ran
	-- ToDate should be the most recently completed day, prior to today's date
	--        and the ToDate should NOT be prior to the FromDate
	--        If the ToDate is the same as the FromDate, apply the time 23:59:59 to include all rows received on that day

        SET @JobCode = 'BATRUNRT';

        SET @LastRanTimestampUTC = ( SELECT TOP 1
                                            CONVERT(VARCHAR(10), LastRanTimestampUTC, 101) AS LastRanTimestampUTC
                                     FROM   dbo.Job
                                     WHERE  JobCode = @JobCode
                                   );

        SET @ToDate = CONVERT(VARCHAR(10), DATEADD(DAY, -1, GETUTCDATE()), 101);

        IF @LastRanTimestampUTC >= @ToDate
            BEGIN
		-- no need to run again within same date range. If you need to refresh data on SummaryBatteryRunRate, change the date on Job.LastRanTimestampUTC where JobCode = 'BATRUNRT', then execute this code
                RETURN;
            END;

        SET @FromDate = @LastRanTimestampUTC;

        INSERT  INTO dbo.SummaryBatteryRunRate
                ( SiteID ,
                  [Date] ,
                  BatterySerialNumber ,
                  AvgRunRate
                )
                SELECT  SiteID ,
                        CAST(CONVERT(VARCHAR(10), EndDateUTC, 101) AS DATE) AS [Date] ,
                        BatterySerialNumber ,
                        COALESCE(AVG(RunRate), 0) AS AvgRunRate
                FROM    dbo.Sessions
                WHERE   ( ( EndDateUTC BETWEEN @FromDate AND @ToDate )
                          OR ( StartDateUTC BETWEEN @FromDate AND @ToDate )
                          OR ( StartDateUTC < @FromDate
                               AND EndDateUTC > @ToDate
                             )
                        )
                        AND SiteID IS NOT NULL
                        AND RunRate IS NOT NULL
	--and dbo.SerialNumberClassification(BatterySerialNumber) != 0 
                GROUP BY SiteID ,
                        CONVERT(VARCHAR(10), EndDateUTC, 101) ,
                        BatterySerialNumber; 

        UPDATE  dbo.Job
        SET     LastRanTimestampUTC = @ToDate
        WHERE   JobCode = @JobCode;

    END;


GO
