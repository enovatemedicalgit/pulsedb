SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:      <ENOVATE\arlow.farrell,ARLOWFARRELL>
-- Create date: <10/4/2016> <3:41 PM>
-- Description: 
--				Default Description of Stored Proc with Supporting Links, Notes, etc.     
--
-- 
-- =============================================
CREATE PROCEDURE [dbo].[prcGetChargerBaysLastPacket] @SiteID INT = 657
AS
    BEGIN
        SET NOCOUNT ON;
    
 
 
  SELECT MostRecent.DeviceSerialNumber ,
         MostRecent.Activity ,
         MostRecent.ChargeLevel ,
         MostRecent.CreatedDateUTC ,
         MostRecent.Bay ,
		 MostRecent.Amps,
         MostRecent.RN FROM (SELECT deviceserialnumber,amps,activity,chargelevel,NSDC.CreatedDateUTC,nsdc.Bay,ROW_NUMBER() OVER (PARTITION BY NSDC.DeviceSerialNumber,NSDC.Bay ORDER BY CreatedDateUTC DESC) as RN FROM dbo.NonSessionDataCurrent AS NSDC WHERE NSDC.DeviceSerialNumber IN (SELECT SerialNo FROM dbo.Assets ass WHERE siteid = @SiteID AND IDAssetType IN (2,4) AND ass.Retired = 0 AND lastpostdateutc > DATEADD(DAY,-8,GETUTCDATE()))) MostRecent where RN=1 ORDER BY 
    MostRecent.DeviceSerialNumber,MostRecent.Bay,CreatedDateUTC DESC
 
    END;
GO
