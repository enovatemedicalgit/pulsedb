SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcAssetTypeInsert]
    @Category NVARCHAR(50) = NULL ,
    @Description VARCHAR(50) = NULL ,
    @IDSite NVARCHAR(50) = NULL
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
    BEGIN TRAN;
	
    INSERT  INTO dbo.AssetType
            ( [Category] ,
              [Description] 
         
            )
            SELECT  @Category ,
                    @Description 
                
	
	-- Begin Return Select <- do not remove
    SELECT  [IDAssetType] ,
            [Category] ,
            [Description] 
      
    FROM    dbo.AssetType
    WHERE   [IDAssetType] = SCOPE_IDENTITY();
	-- End Return Select <- do not remove
               
    COMMIT;




GO
