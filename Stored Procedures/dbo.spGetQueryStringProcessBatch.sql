SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[spGetQueryStringProcessBatch]
    (
      @rowsToGet INT ,
      @guid UNIQUEIDENTIFIER OUTPUT
    )
AS
    BEGIN
        SET @guid = NEWID();

        UPDATE  dbo.QueryString
        SET     ProcessID = @guid
        WHERE   ROW_ID IN (
                SELECT TOP ( @rowsToGet )
                        qs.ROW_ID
                FROM    dbo.QueryString qs
                        LEFT JOIN dbo.PriorityIP ip ON ( LEFT(qs.SourceIPAddress,
                                                          6) = ip.SourceIPAddress_Left )
                                                   OR ( LEFT(qs.SourceIPAddress,
                                                             8) = ip.SourceIPAddress_Left )
                WHERE   qs.NineParsed = 0
                        AND qs.ProcessID IS NULL
                ORDER BY ISNULL(( SELECT    0
                                  WHERE     ISNULL(ip.SourceIPAddress_Left,
                                                   '0') = '0'
                                ), 1) DESC ,
                        qs.CreatedDateUTC ASC );
	
    END;
GO
