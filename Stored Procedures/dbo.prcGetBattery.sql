SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================

-- Modified 6/28/2013 
-- added specific order by clause to accomodate
-- for telerik formatting on edits. 

-- Modified added LastPostDate
-- =============================================
CREATE Procedure [dbo].[prcGetBattery] 
	--@UserID int,
    @SiteID INT = NULL ,
    @CustomerID INT = NULL,
		@UserTypeID INT = 0
AS
    BEGIN
	
        SET NOCOUNT ON;
	--Variables for testing
 --   declare @UserID int = 813
	--declare @UserIsStinger varchar(5) = 'True'
	
	--SELECT 
	--	--d.IDAsset, 
	--	--d.Retired, 
	--	d.SerialNo, 
	--	d.InvoiceNumber, 
	--	--d.PartNumber, 
	--	d.AssetNumber, 
	--	--d.Model, 
	--	--pn.[Description], 
	--	d.Description,
	--	d.SiteID, 
	--	bu.SiteDescription AS SiteName, 
	--	parentbu.CustomerName AS IDN, 		
	--	--d.DepartmentID, 
	--	isnull(dept.[Description],'Unallocated') AS DepartmentName, 
	--	d.[Floor] AS AssignedFloor, 
	--	d.Wing AS AssignedWing, 
	--	d.Other as AssignedOther, 
	----	d.IsActive, 
	----d.Retired,
	--	CASE 
	--		WHEN d.Retired <> 0  
	--			THEN 'Y' 
	--			ELSE 'N' 
	--		END AS IsRetired, 
	--	d.Notes, 
	--	--d.Notes, 
	----	d.IDAssetType, 
	--	--dt.[Description] AS DeviceType, 
	----	d.CreatedDateUTC, 
	--	d.LastPostDateUTC, 
	----	COALESCE(d.LastPostDateUTC, '2011-1-11 15:00:00.000') as LastPostDateUTC,
	--	--CASE 
	--	--	WHEN COALESCE(d.ModifiedUserID, 0) = 0 
	--	--		THEN '' 
	--	--		ELSE COALESCE(u.FirstName, '') + ' ' + COALESCE(u.LastName, '') 
	--	--	END AS ModifiedUserFullName, 
	--	--CASE 
	--	--	WHEN COALESCE(d.CreatedUserID, 0) = 0 
	--	--		THEN '' 
	--	--		ELSE  COALESCE(u2.FirstName, '') + ' ' + COALESCE(u2.LastName, '') 
	--	--	END AS CreatedUserFullName, 
		
	--	--ap.MACAddress + CASE 
	--	--					WHEN COALESCE(ap.[Description], '') NOT IN ('', ap.MACAddress) 
	--	--						THEN ' (' + ap.[Description] + ')' 
	--	--						ELSE '' 
	--	--					END + ' at ' + apbu.SiteDescription AS AccessPointDetail, 
	--	--ap.BusinessUnitID AS AccessPointBusinessUnitID, 
	--	--CASE 
	--	--	WHEN COALESCE(ap.BusinessUnitID, 0) <> COALESCE(d.BusinessUnitID, 0) 
	--	--		THEN 'Y' 
	--	--		ELSE 'N' 
	--	--	END AS LocationFlag, 
	--	ap.[Floor] AS AccessPointFloor, 
	----	ap.Wing AS AccessPointWing,  

	----	d.ControlBoardRevision, 
	-----	d.LCDRevision, 
	----	d.HolsterChargerRevision, 
	----	d.DCBoardOneRevision, 
	----	d.DCBoardTwoRevision, 
	----	d.WiFiFirmwareRevision, 
	----	d.MedBoardRevision, 
	--	d.DeviceMAC, 
	--	ds.[Description] AS DeviceStatus --,		
	--	--Coalesce(Convert(varchar,PM.CreatedDateUTC,101), 'N/A') as PM,
	--	--Coalesce(PM.Result, 'N/A') as PM_Result,
	--	--Coalesce(Convert(varchar,RE.CreatedDateUTC,101), 'N/A') as Repair,
	--	--Coalesce(RE.Result, 'N/A') as Repair_Result,
	--	--Coalesce(Convert(varchar,CE.CreatedDateUTC,101), 'N/A') as Certification,
	--	--Coalesce(CE.Result, 'N/A') as Certification_Result
		

	--FROM Assets d 
	--	--LEFT JOIN 
	--	--	PartNumber pn ON left(d.SerialNumber, 7) = pn.PartNumber 
	--	LEFT JOIN 
	--		AssetStatus ds ON d.AssetStatusID = ds.IDAssetStatus 
	--	LEFT JOIN 
	--		AccessPoint ap ON d.AccessPointID = ap.ROW_ID 
	--	LEFT JOIN 
	--		Sites apbu ON d.SiteID = apbu.IDSite 
	--	--LEFT JOIN 
	--	--	[User] u ON d.ModifiedUserID = u.ROW_ID 
	--	--LEFT JOIN 
	--	--	[User] u2 ON d.CreatedUserID = u2.ROW_ID 	
	--	--LEFT JOIN 
	--	--	QA PM ON PM.DeviceSerialNumber = d.SerialNumber AND PM.Latest = 1 AND PM.QAStationID = 8
	--	--LEFT JOIN 
	--	--	QA RE On RE.DeviceSerialNumber = d.SerialNumber AND RE.Latest = 1 AND RE.QAStationID = 9	
	--	--LEFT JOIN
	--	--	QA CE On CE.DeviceSerialNumber = d.SerialNumber AND CE.Latest = 1 and CE.QAStationID = 10
	--	JOIN 
	--		AssetType dt ON d.IDAssetType = dt.IDAssetType 
	--	LEFT JOIN 
	--		Sites bu ON d.SiteID = bu.IDSite 
	--	LEFT JOIN 
	--		Customers parentbu ON bu.CustomerID = parentbu.IDCustomer 
	--	LEFT JOIN 
	--		Departments dept ON d.DepartmentID = dept.IDDepartment 
	
			
	--	WHERE 
	--		d.IDAssetType IN (5,9,14,15) 
	--			AND (bu.IDSite IN ( SELECT IDSite FROM [Sites] WHERE IDSite = @SiteID) 
	--			--	OR bu.CustomerID IN (SELECT CustomerID FROM Sites WHERE IDSite = @SiteID) 
	--				--	OR bu.IDSite IN (SELECT distinct IDSite FROM Sites WHERE @UserIsStinger = 'True'))
	--					)
	--	Order by d.IDAsset desc 






        IF OBJECT_ID(N'tempdb..#tfullrpt', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tfullrpt;
            END;
        IF OBJECT_ID(N'tempdb..#tfullrpt2', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tfullrpt2;
            END;
        IF ( @CustomerID IS NULL )
            BEGIN
                SELECT  MAX(fullrpt.SiteName) AS SiteName ,
                        MAX(fullrpt.IDN) AS IDN ,
                        fullrpt.SerialNo ,
						MAX(fullrpt.AssetNumber) AS AssetNumber,

                        CASE WHEN MAX(CAST(fullrpt.RemoveFromField AS INT)) <> 0
                             THEN 'Y'
                             ELSE 'N'
                        END AS 'BRPorShutdown' ,
                        COUNT(fullrpt.SerialNo) AS cnt ,
                        MAX(fullrpt.[Warranty Years]) AS 'Warranty Years' ,
                        MAX(fullrpt.[Expiration Date]) AS 'Expiration Date' ,
                        MAX(fullrpt.CycleCount) AS 'CycleCount' ,
                        MAX(fullrpt.MaxCycleCount) AS 'MaxCycleCount' ,
                        MAX(fullrpt.[Capacity Health]) AS 'Capacity Health %' ,
                        MAX(fullrpt.FullChargeCapacity) AS 'FullChargeCapacity' ,
                        MAX(fullrpt.LastPostDateUTC) AS 'LastPostDateUTC',
						MAX(fullrpt.IDAsset) AS 'IDAsset'
                INTO    #tfullrpt
                FROM    ( SELECT    ISNULL(s.SiteName, cus.CustomerName) AS SiteName ,
                                    ISNULL(cus2.CustomerName, s.SiteName) AS IDN ,
                                    ass.SerialNo ,
									ass.AssetNumber AS 'AssetNumber',
                                    ass.RemoveFromField AS RemoveFromField ,
                                    wd.DurationYears AS 'Warranty Years' ,
                                    CONVERT(DATE, DATEADD(DAY,
                                                          ISNULL(( SELECT
                                                              DurationDays
                                                              FROM
                                                              dbo.WarrantyDuration
                                                              WHERE
                                                              SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
                                                              ), 5000),
                                                          CAST('20'
                                                          + SUBSTRING(ass.SerialNo,
                                                              8, 2) + '-'
                                                          + SUBSTRING(ass.SerialNo,
                                                              10, 2) + '-01' AS DATETIME))) AS 'Expiration Date' ,
                                    ass.CycleCount ,
                                    ass.MaxCycleCount ,
                                    ISNULL(NULLIF(dbo.fnGetBatteryCapacity_Int(ass.SerialNo,
                                                              ass.FullChargeCapacity),
                                                  0), 0) AS 'Capacity Health' ,
                                    ass.FullChargeCapacity ,
                                    ass.LastPostDateUTC,
									ass.IDAsset
                          FROM      dbo.Assets ass
                                    LEFT JOIN dbo.Sites s ON s.IDSite = ass.SiteID
                                    LEFT JOIN dbo.Customers cus ON ass.SiteID = cus.IDCustomer
                                    LEFT JOIN dbo.Customers cus2 ON s.CustomerID = cus2.IDCustomer
                                    LEFT JOIN dbo.WarrantyDuration wd ON wd.SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
                          WHERE     ass.IDAssetType = 5 --and 
 -- [dbo].[SerialNumberClassification](ass.serialno) = 1 
   --and Retired = 0
  --and ass.MaxCycleCount <> 0 and ass.MaxCycleCount <> 10 
                                --    AND ass.SiteID NOT IN ( 4, 28, 1125 )
                                    AND ass.SiteID = @SiteID
  -- and fullchargecapacity > 0 
                        ) fullrpt
                GROUP BY fullrpt.SerialNo; -- order by fullrpt.Account asc,SerialNo desc 

                SELECT  IDN ,
                        SiteName ,
                        SerialNo AS 'SerialNumber' ,
						AssetNumber AS AssetNumber,
                        ISNULL(( SELECT Generation
                                 FROM   dbo.BatteryPartList
                                 WHERE  PartNumber = LEFT(#tfullrpt.SerialNo,
                                                          7)
                               ), '3.5') AS Generation ,
                        [Warranty Years] ,
                        [Expiration Date] ,
                        CycleCount ,
                        MaxCycleCount ,
                        [Capacity Health %] ,
                        FullChargeCapacity ,
                        IIF(LastPostDateUTC < DATEADD(WEEK, -1, GETDATE()), 'No', 'Yes') AS 'NoComm7Days' ,
                        LastPostDateUTC AS 'Last Reported To Pulse' ,

                        CAST(CAST(FullChargeCapacity AS DECIMAL(18,
                                                              2))
                        / CAST(2400.00 AS DECIMAL(18, 2)) * 0.85 AS DECIMAL(18,
                                                              2)) AS 'Estimated Run Hours' ,
                        BRPorShutdown AS 'Shutdown or BRP' ,
                        ISNULL(( SELECT TOP 1
                                        ISNULL(sfc.CustomerAcct,
                                               sfa.CustomerAcct)
                                 FROM   dbo.SFCustomer sfc
                                        RIGHT JOIN dbo.SFAccounts sfa ON sfc.CustomerAcct = sfa.CustomerAcct
                                 WHERE  sfc.SiteID = ( SELECT SiteID
                                                       FROM   dbo.Assets
                                                       WHERE  SerialNo = #tfullrpt.SerialNo
                                                     )
                               ),
                               ( SELECT SfCustomerAcctID
                                 FROM   dbo.Sites
                                 WHERE  IDSite = ( SELECT   SiteID
                                                   FROM     dbo.Assets
                                                   WHERE    SerialNo = #tfullrpt.SerialNo
                                                 )
                               )) AS 'SF Customer Acct #' ,
                        ISNULL(( 'http://ipaddress.is/'
                                 + ( SELECT TOP 1
                                            SourceIPAddress
                                     FROM   dbo.Assets
                                     WHERE  SiteID = ( SELECT SiteID
                                                       FROM   dbo.Assets
                                                       WHERE  SerialNo = #tfullrpt.SerialNo
                                                     )
                                            AND SourceIPAddress IS NOT NULL
                                   ) ),
                               ( 'http://ipaddress.is/'
                                 + ( SELECT TOP 1
                                            ( SourceIPAddressBase + '222' )
                                     FROM   dbo.SiteIPSubnet
                                     WHERE  SiteID = ( SELECT TOP 1
                                                              SiteID
                                                       FROM   dbo.Assets
                                                       WHERE  SerialNo = #tfullrpt.SerialNo
                                                     )
                                   ) )) AS 'Geographical Info Link (St,Zip,Area)',
								   IDAsset
                FROM    #tfullrpt
                ORDER BY SiteName;
            END;
        ELSE
            BEGIN
                SELECT  MAX(fullrpt.SiteName) AS SiteName ,
                        MAX(fullrpt.IDN) AS IDN ,
                        fullrpt.SerialNo ,
						MAX(fullrpt.AssetNumber) AS AssetNumber,
                        CASE WHEN MAX(CAST(fullrpt.RemoveFromField AS INT)) <> 0
                             THEN 'Y'
                             ELSE 'N'
                        END AS 'BRPorShutdown' ,
						
                        COUNT(fullrpt.SerialNo) AS cnt ,
                        MAX(fullrpt.[Warranty Years]) AS 'Warranty Years' ,
                        MAX(fullrpt.[Expiration Date]) AS 'Expiration Date' ,
                        MAX(fullrpt.CycleCount) AS 'CycleCount' ,
                        MAX(fullrpt.MaxCycleCount) AS 'MaxCycleCount' ,
                        MAX(fullrpt.[Capacity Health]) AS 'Capacity Health %' ,
                        MAX(fullrpt.FullChargeCapacity) AS 'FullChargeCapacity' ,
                        MAX(fullrpt.LastPostDateUTC) AS 'LastPostDateUTC',
						MAX(fullrpt.IDAsset) AS 'IDAsset'
                INTO    #tfullrpt2
                FROM    ( SELECT    ISNULL(s.SiteName, cus.CustomerName) AS SiteName ,
                                    ISNULL(cus2.CustomerName, s.SiteName) AS IDN ,
									isnull(ass.AssetNumber,'') AS AssetNumber,
                                    ass.SerialNo ,
                                    ass.RemoveFromField AS RemoveFromField ,
                                    wd.DurationYears AS 'Warranty Years' ,
                                    CONVERT(DATE, DATEADD(DAY,
                                                          ISNULL(( SELECT
                                                              DurationDays
                                                              FROM
                                                              dbo.WarrantyDuration
                                                              WHERE
                                                              SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
                                                              ), 5000),
                                                          CAST('20'
                                                          + SUBSTRING(ass.SerialNo,
                                                              8, 2) + '-'
                                                          + SUBSTRING(ass.SerialNo,
                                                              10, 2) + '-01' AS DATETIME))) AS 'Expiration Date' ,
                                    ass.CycleCount ,
                                    ass.MaxCycleCount ,
                                    ISNULL(NULLIF(dbo.fnGetBatteryCapacity_Int(ass.SerialNo,
                                                              ass.FullChargeCapacity),
                                                  0), 0) AS 'Capacity Health' ,
                                    ass.FullChargeCapacity ,
                                    ass.LastPostDateUTC,
									ass.IDAsset
                          FROM      dbo.Assets ass
                                    LEFT JOIN dbo.Sites s ON s.IDSite = ass.SiteID
                                    LEFT JOIN dbo.Customers cus ON ass.SiteID = cus.IDCustomer
                                    LEFT JOIN dbo.Customers cus2 ON s.CustomerID = cus2.IDCustomer
                                    LEFT JOIN dbo.WarrantyDuration wd ON wd.SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
                          WHERE     ass.IDAssetType = 5
                                    AND dbo.SerialNumberClassification(ass.SerialNo) = 1
                                    AND ass.SiteID IN (
                                    SELECT  IDSite
                                    FROM    dbo.Sites
                                    WHERE   CustomerID = @CustomerID )
                        ) fullrpt
                GROUP BY fullrpt.SerialNo; -- order by fullrpt.Account asc,SerialNo desc 

                SELECT  IDN ,
                        SiteName ,
                        SerialNo AS 'SerialNumber' ,
                        ISNULL(( SELECT Generation
                                 FROM   dbo.BatteryPartList
                                 WHERE  PartNumber = LEFT(#tfullrpt2.SerialNo,
                                                          7)
                               ), '3.5') AS Generation ,
                        [Warranty Years] ,
                        [Expiration Date] ,
                        CycleCount ,
                        MaxCycleCount ,
                        [Capacity Health %] ,
                        FullChargeCapacity ,
                        IIF(LastPostDateUTC < DATEADD(WEEK, -1, GETDATE()), 'No', 'Yes') AS 'Recently Reported to Pulse' ,
                        CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, LastPostDateUTC),
                                                       DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS 'Last Reported To Pulse' ,
                        CAST(CAST(FullChargeCapacity AS DECIMAL(18,
                                                              2))
                        / CAST(2400.00 AS DECIMAL(18, 2)) * 0.85 AS DECIMAL(18,
                                                              2)) AS 'Estimated Run Hours' ,
                        BRPorShutdown AS 'Shutdown or BRP' ,
                        ISNULL(( SELECT TOP 1
                                        ISNULL(sfc.CustomerAcct,
                                               sfa.CustomerAcct)
                                 FROM   dbo.SFCustomer sfc
                                        RIGHT JOIN dbo.SFAccounts sfa ON sfc.CustomerAcct = sfa.CustomerAcct
                                 WHERE  sfc.SiteID = ( SELECT SiteID
                                                       FROM   dbo.Assets
                                                       WHERE  SerialNo = #tfullrpt2.SerialNo
                                                     )
                               ),
                               ( SELECT SfCustomerAcctID
                                 FROM   dbo.Sites
                                 WHERE  IDSite = ( SELECT   SiteID
                                                   FROM     dbo.Assets
                                                   WHERE    SerialNo = #tfullrpt2.SerialNo
                                                 )
                               )) AS 'SF Customer Acct #' ,
                        ISNULL(( 'http://ipaddress.is/'
                                 + ( SELECT TOP 1
                                            SourceIPAddress
                                     FROM   dbo.Assets
                                     WHERE  SiteID = ( SELECT SiteID
                                                       FROM   dbo.Assets
                                                       WHERE  SerialNo = #tfullrpt2.SerialNo
                                                     )
                                            AND SourceIPAddress IS NOT NULL
                                   ) ),
                               ( 'http://ipaddress.is/'
                                 + ( SELECT TOP 1
                                            ( SourceIPAddressBase + '222' )
                                     FROM   dbo.SiteIPSubnet
                                     WHERE  SiteID = ( SELECT TOP 1
                                                              SiteID
                                                       FROM   dbo.Assets
                                                       WHERE  SerialNo = #tfullrpt2.SerialNo
                                                     )
                                   ) )) AS 'Geographical Info Link (St,Zip,Area)',
								   IDAsset
                FROM    #tfullrpt2
                ORDER BY SiteName ,
                        BRPorShutdown DESC ,
                        LastPostDateUTC DESC;

            END;
    END;



GO
