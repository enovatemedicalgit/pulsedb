SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


create PROC  [dbo].[spDashboardBatteryChargeLevels]
    @SiteID INT = 212 ,
    @Department INT = NULL
AS
    SET NOCOUNT ON;
	 --INSERT INTO Errorlog
  --               (description,
  --                [source],
  --                CreatedDateUTC
  --               )
  --               VALUES
  --               ('Called Battery HeatMap SP :' +  CAST(@SiteID as varchar) , 'spDashboardBatteryChargeLevels_Test',
  --                GETutcDATE()
  --               );
	 --  SELECT    x,
		--	   y,
		--	   heat,
		--	   symbol
	 --  FROM
	 --  (
		--  SELECT((ROW_NUMBER() OVER(ORDER BY CONVERT(FLOAT,ChargeLevel) DESC)-1) / 18) + 1 AS y,
		--	   ((ROW_NUMBER() OVER(ORDER BY CONVERT(FLOAT,ChargeLevel) DESC)-1) % 18) + 1 AS x,
		--	   SerialNo as symbol,
		--	   convert(float,ChargeLevel)/100 as heat
		--  FROM  dbo.Assets
		--  WHERE idassettype = 5 AND Retired = 0
		--	   AND siteid = @SiteID 
		--   AND (DepartmentID  = @Department or @Department is null)
	 --  ) AS base
	 --  ORDER BY  heat desc,x,y
    DECLARE @cntBatt AS INT;
    SET @cntBatt = ( SELECT COUNT(IDAsset)
                     FROM   dbo.Assets
                     WHERE  IDAssetType = 5
                            AND SiteID = @SiteID
                            AND  CONVERT(float,ChargeLevel) > 0
                            AND Retired = 0
                            AND ( DepartmentID = @Department
                                  OR @Department IS NULL
                                )
                   );

    SELECT  base.x ,
            base.y ,
            base.heat ,
            base.symbol ,
            @cntBatt
    FROM    ( SELECT    ( CAST(( ROW_NUMBER() OVER ( ORDER BY CONVERT(FLOAT, ChargeLevel) DESC )
                                 - 1 ) / ( @cntBatt / 3.27 ) AS INT) ) + 1 AS y ,
                        ( CAST(( ROW_NUMBER() OVER ( ORDER BY CONVERT(FLOAT, ChargeLevel) DESC )
                                 - 1 ) % ( @cntBatt / 3.27 ) AS INT) ) + 1 AS x ,
                        SerialNo AS symbol ,
                        CONVERT(FLOAT, ChargeLevel) / 100 AS heat
             FROM      dbo.vwBatteries AS VB
              WHERE     IDAssetType = 5
                        AND SiteID = @SiteID
                        AND CONVERT(float,ChargeLevel) > 0
                        AND Retired = 0
                        AND ( DepartmentID = @Department
                              OR @Department IS NULL
                            )
            ) AS base
    ORDER BY base.heat, base.x ,
            base.y;
	



GO
