SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Description:	Gets workstation detail rows
--              for workstationsummary.aspx page
--              in CAST. These are the rows
--              that display when the user
--              expands the department row.
--
--              Date must be formatted as follows:
--              2011/12/13
--
-- =============================================
CREATE PROCEDURE [dbo].[spSummary_GetWorkstationDetail]
    (
      @Date VARCHAR(10) ,
      @SiteID INT ,
      @DepartmentID INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT  SerialNo ,
                AssetNumber ,
                IDSite ,
                IDDepartment ,
                [Floor] ,
                Wing ,
                Other ,
                [Status] ,
                Utilization ,
                AvgRunRate ,
                SessionCount ,
                LoChargeInsertsCount ,
                HiChargeRemovalsCount ,
                AvgAmpDraw ,
                AvgEstimatedPCUtilization
        FROM    dbo.SummaryWorkstation
        WHERE   IDSite = @SiteID
                AND IDDepartment = @DepartmentID
                AND [Date] = @Date;

    END;


GO
