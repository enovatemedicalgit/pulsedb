SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcSiteFloorsSelectBySite] @SiteID INT
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  

    BEGIN TRAN;

    SELECT  IdSiteFloor ,
            [Description] ,
            [SiteId] ,
            [DefaultForSite]
    FROM    dbo.SiteFloors
    WHERE   ( [SiteId] = @SiteID ); 

    COMMIT;



GO
