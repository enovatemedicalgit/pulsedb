SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:      <ENOVATE\bill.murray,BILLMURRAY>
-- Create date: <10/19/2016> <12:20 PM>
-- Description: 
--				Default Description of Stored Proc with Supporting Links, Notes, etc.     
--
-- 
-- =============================================
CREATE PROCEDURE [dbo].[prcDashGetFullyChargedBatteries]
    @SiteId INT = 657,
    @DepartmentId INT = null
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT,
        QUOTED_IDENTIFIER,
        ANSI_NULLS,
        ANSI_PADDING,
        ANSI_WARNINGS,
        ARITHABORT,
        CONCAT_NULL_YIELDS_NULL ON;
        SET NUMERIC_ROUNDABORT OFF;
 
        DECLARE @localTran BIT;
 
        BEGIN TRY
        
select 90 AS FullyChargedBatteries, 201 AS FullyChargedCnt; 
          
 
        END TRY
        BEGIN CATCH
 
            DECLARE @ErrorMessage NVARCHAR(4000);
            DECLARE @ErrorSeverity INT;
            DECLARE @ErrorState INT;
 
            SELECT  @ErrorMessage = ERROR_MESSAGE() ,
                    @ErrorSeverity = ERROR_SEVERITY() ,
                    @ErrorState = ERROR_STATE();
 
            IF @localTran = 1
                AND XACT_STATE() <> 0
                ROLLBACK TRAN;
 
            RAISERROR ( @ErrorMessage, @ErrorSeverity, @ErrorState);
            INSERT  INTO dbo.ErrorLog 
                    ( [Description] ,
                      [Source] ,
                      [CreatedDateUTC]
                    )
            VALUES  ( @ErrorMessage + ' ' + @ErrorSeverity + ' ' + @ErrorState ,
                      ' prcDashGetFullyChargedBatteries ' ,
                      GETUTCDATE()
                    );
 		
        END CATCH;
 
    END;
GO
