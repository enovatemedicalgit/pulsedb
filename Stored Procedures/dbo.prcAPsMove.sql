SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcAPsMove]
    @APMACList NVARCHAR(3000) ,
    @SiteID INT,
	@UserID INT = 0
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
    BEGIN TRAN;
    DECLARE @SQLs NVARCHAR(MAX);
    DECLARE @SQLSelect NVARCHAR(MAX);
    SET @SQLs = 'update [dbo].[AccessPoint] SET SiteID ='
        + CAST(@SiteID AS NVARCHAR) + ', InsertedBy =' + CAST(@UserID AS NVARCHAR) +  ' , Manual = 1  WHERE MacAddress in (' + @APMACList
        + ')';
    PRINT @SQLs;

	--UPDATE [dbo].[Assets]
	--SET   SiteID = 1125
	--WHERE  serialno in (@APMACList)
    EXEC sys.sp_executesql @SQLs;
    COMMIT;
    SELECT  @SQLs; 



GO
