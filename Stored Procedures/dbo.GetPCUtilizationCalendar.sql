SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[GetPCUtilizationCalendar](@SiteID INT = 1294, @BeginDate DATE = NULL, @EndDate DATE = NULL)
AS

IF @BeginDate IS NULL
    SET @BeginDate = DATEADD(month,-1,GETDATE())
IF @EndDate IS NULL
    SET @EndDate = GETDATE()



SELECT  Week, 
[Sunday], [Monday], [Tuesday], [Wednesday], [Thursday], [Friday], [Saturday]
FROM
(

        SELECT  dd.DayName,
                                                DATEDIFF(Week,s.date, GETDATE()) as Week,
                IIF(s.AvgEstimatedPCUtilization > 1, 1,s.AvgEstimatedPCUtilization)  AS  'AvgEstPCUtil'
        FROM    dbo.SummaryWorkstation s
                                                JOIN
                                                dbo.DimDate dd
                                                                ON CONVERT(date,s.Date) = dd.Date
        WHERE   s.IDSite = @SiteId
                AND s.AvgEstimatedPcUtilization >.01
                AND CONVERT(date,s.[Date]) BETWEEN @BeginDate AND @EndDate
                                               

) AS SourceTable
PIVOT
(
AVG(AvgEstPcUtil)
FOR DayName IN ([Sunday], [Monday], [Tuesday], [Wednesday], [Thursday], [Friday], [Saturday])
) AS PivotTable
ORDER BY Week DESC
GO
