SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcUpdateAssets]
    @SerialNo VARCHAR(50) = NULL ,
    @AssetStatusID INT = NULL ,
    @Temperature INT = NULL ,
    @FullChargeCapacity INT = NULL
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
    BEGIN TRAN;

    UPDATE  dbo.Assets
    SET     [AssetStatusID] = @AssetStatusID ,
            FullChargeCapacity = @FullChargeCapacity ,
            Temperature = @Temperature
    WHERE   SerialNo = @SerialNo;
	
	-- Begin Return Select <- do not remove
    SELECT  *
    FROM    dbo.Assets
    WHERE   SerialNo = @SerialNo;
	-- End Return Select <- do not remove

    COMMIT;



GO
