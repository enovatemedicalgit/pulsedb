SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================

-- Modified 6/28/2013 
-- added specific order by clause to accomodate
-- for telerik formatting on edits. 

-- Modified added LastPostDate
-- =============================================
CREATE PROCEDURE [dbo].[prcDashGetBattery] 
	--@UserID int,
    @SiteID INT = 657 ,
    @CustomerID INT = NULL ,
    @UserTypeID INT = 0
AS
    BEGIN
	
        SET NOCOUNT ON;

        IF OBJECT_ID(N'tempdb..#tfullrpt', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tfullrpt;
            END;
        IF OBJECT_ID(N'tempdb..#tfullrpt2', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tfullrpt2;
            END;
        
        SELECT 
               
                fullrpt.SerialNumber AS SerialNumber ,
                MAX(fullrpt.AssetNumber) AS Asset ,
                CASE WHEN MAX(CAST(fullrpt.RMA AS INT)) <> 0 THEN 'Y'
                     ELSE 'N'
                END AS 'RMA' ,
                MAX(fullrpt.WarrantyYears) AS 'WarrantyYears' ,
                MAX(fullrpt.ExpirationDate) AS 'ExpirationDate' ,
                MAX(fullrpt.CycleCount) AS 'CycleCount' ,
                MAX(fullrpt.CapacityHealth) AS 'CapacityHealth' ,
				    MAX(fullrpt.ChargeLevel) as 'ChargeLevel' ,
                MAX(fullrpt.FullChargeCapacity) AS 'FullChargeCapacity' ,
                MAX(fullrpt.LastPostDateUTC) AS 'LastPostDateUTC' ,
                MAX(fullrpt.IDAsset) AS 'IDAsset'
        INTO    #tfullrpt
        FROM    ( SELECT    
                            ass.SerialNo AS SerialNumber ,
                            ass.AssetNumber AS 'AssetNumber' ,
                            ass.RemoveFromField AS RMA ,
                            wd.DurationYears AS 'WarrantyYears' ,
                            CONVERT(DATE, DATEADD(DAY,
                                                  ISNULL(( SELECT
                                                              DurationDays
                                                           FROM
                                                              dbo.WarrantyDuration
                                                           WHERE
                                                              SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
                                                         ), 5000),
                                                  CAST('20'
                                                  + SUBSTRING(ass.SerialNo, 8,
                                                              2) + '-'
                                                  + SUBSTRING(ass.SerialNo, 10,
                                                              2) + '-01' AS DATETIME))) AS 'ExpirationDate' ,
                            ISNULL(ass.CycleCount, 0) AS CycleCount ,
							   ISNULL(ass.ChargeLevel,0) AS 'ChargeLevel' ,
                            ISNULL(isnull(dbo.fnGetBatteryCapacity_Int(ass.SerialNo,
                                                              ass.FullChargeCapacity),
                                          0), 100) AS 'CapacityHealth' ,
                            ISNULL(ass.FullChargeCapacity, 0) AS FullChargeCapacity ,
                            ass.LastPostDateUTC ,
                            ass.IDAsset
                  FROM      dbo.Assets ass
                            LEFT JOIN dbo.Sites s ON s.IDSite = ass.SiteID
                            LEFT JOIN dbo.Customers cus ON ass.SiteID = cus.IDCustomer
                            LEFT JOIN dbo.Customers cus2 ON s.CustomerID = cus2.IDCustomer
                            LEFT JOIN dbo.WarrantyDuration wd ON wd.SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
                  WHERE     ass.IDAssetType = 5 
                            AND ass.SiteID NOT IN ( 4, 28, 1125 )
                            AND ass.SiteID = @SiteID AND (ass.retired = 0 or ass.createddateutc > DATEADD(month,-6, GETUTCDATE()))

                ) fullrpt
        GROUP BY fullrpt.SerialNumber; -- order by fullrpt.Account asc,SerialNo desc 

        SELECT  
           
                SerialNumber AS 'Serial' ,
			
                ISNULL(( SELECT Generation
                         FROM   dbo.BatteryPartList
                         WHERE  PartNumber = LEFT(#tfullrpt.SerialNumber, 7)
                       ), '3.5') AS Generation ,   				           
						#tfullrpt.LastPostDateUTC,
			
			       CONVERT(VARCHAR,CAST(CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, #tfullrpt.LastPostDateUTC),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS DATETIME), 100) AS 'Last Used' ,
															  	ChargeLevel AS 'ChargeLevel',
																 [CapacityHealth] ,
																 
															  CAST(CAST(FullChargeCapacity AS DECIMAL(18, 2))
                / CAST(2400.00 AS DECIMAL(18, 2)) * 0.85 AS DECIMAL(18, 2)) AS 'Est Run Time' ,
				           CycleCount ,
                [WarrantyYears] ,
				      CONVERT(VARCHAR,CAST(CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, [ExpirationDate]),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS DATETIME), 107)
               AS "Warranty Expires" ,
     
               
      
               -- IIF(LastPostDateUTC < DATEADD(WEEK, -1, GETDATE()), 'No', 'Yes') AS 'Recently Used' ,
        
		

                IDAsset
        FROM    #tfullrpt
        ORDER BY LastPostDateUTC desc;
    END;





GO
