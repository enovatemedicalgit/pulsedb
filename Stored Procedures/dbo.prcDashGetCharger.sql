SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================

-- Modified 6/28/2013 
-- added specific order by clause to accomodate
-- for telerik formatting on edits. 

-- Modified added LastPostDate
-- =============================================
CREATE Procedure [dbo].[prcDashGetCharger] 
	
    @SiteID INT  = 657 
AS
    BEGIN
        SET NOCOUNT ON;
            BEGIN
                SELECT 
                     
                        d.SerialNo AS 'Charger', 
                        d.AssetNumber AS 'Asset' , 
						dept.Description AS 'Department',
						SF.Description AS 'Floor',
						SW.Description AS 'Wing',
                        CAST(CAST(d.BayCount AS VARCHAR(4)) + ' Bay' AS NVARCHAR(20)) AS 'Type',
						IIF(ISNULL(d.Bay1BSN,'No Batt SN') = 'No Batt SN','Empty',d.Bay1BSN) AS 'Bay 1',						
						d.Bay1ChargeLevel AS "Charge1",
						IIF(ISNULL(d.Bay2BSN,'No Batt SN') = 'No Batt SN','Empty',d.Bay2BSN) AS 'Bay 2',
						d.Bay2ChargeLevel AS "Charge2",
						IIF(d.baycount < 3,'NA',IIF(ISNULL(d.Bay3BSN,'No Batt SN') = 'No Batt SN','Empty',d.Bay3BSN)) AS 'Bay 3',
						d.Bay3ChargeLevel AS "Charge3",
						IIF(d.baycount < 4,'NA',IIF(ISNULL(d.Bay4BSN,'No Batt SN') = 'No Batt SN','Empty',d.Bay4BSN)) AS 'Bay 4',
						d.Bay4ChargeLevel AS "Charge4",
					     CONVERT(VARCHAR,CAST(CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, d.LastPostDateUTC),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS DATETIME), 100) AS 'Last Reported' , 

						d.LastPostDateUTC,
						d.IDAsset
                   
                FROM    dbo.vwChargers AS  d --LEFT JOIN 
                        LEFT JOIN dbo.Sites bu ON d.SiteID = bu.IDSite
                        LEFT JOIN dbo.Customers parentbu ON bu.CustomerID = parentbu.IDCustomer
                        LEFT JOIN dbo.Departments dept ON d.DepartmentID = dept.IDDepartment
						  LEFT JOIN dbo.SiteFloors AS SF  ON d.SiteFloorID = sf.IdSiteFloor
						    LEFT JOIN dbo.SiteWings AS SW  ON d.SiteWingID = sw.IdSiteWing
                WHERE   bu.IDSite = @SiteID 
                                            
                ORDER BY  floor DESC, wing DESC,Department DESC,DATEPART(DAY,d.LastPostDateUTC) DESC; 
            END;
        
      
    END;





GO
