SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcSiteMove]
    @CustomerID VARCHAR(10) ,
    @SiteID VARCHAR(10)
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
    BEGIN TRAN;
    UPDATE  dbo.Sites
    SET     CustomerID = CAST(@CustomerID AS INT)
    WHERE   IDSite = CAST(@SiteID AS INT);
    COMMIT;




GO
