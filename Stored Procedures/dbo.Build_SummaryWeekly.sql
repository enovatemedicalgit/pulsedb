SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*################################################################################################
 Name				: Build_SummaryWeekly
 Date				: 
 Author				: christopher.stewart
 Company				: enovate 
 Purpose				: Build SummaryWeekly table
 Usage				:
 Impact				:
 Required Perm			:   
 Called by			:   
##################################################################################################
 ver		author					date			change  
 1.0		Arlow.Farrell			12142011			initial [spSummaryWeekly_RollupNow]
 1.1		Christopher.Stewart		06212016		Optimizition and Comments
#################################################################################################*/

/*##################################################################################################
 STEPS	  Description						 
 1		  SummaryActiveAssets_RollupNow

#################################################################################################*/

CREATE PROCEDURE [dbo].[Build_SummaryWeekly]
AS
    DECLARE @LapTimer DATETIME2;
    DECLARE @ReportDate DATE;
    SET @ReportDate = CONVERT(DATE, DATEADD(DAY, -1, GETUTCDATE()), 111);

 PRINT dbo.GetMessageBorder();
/*################################################################################################*/
    Step_1:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 1'; 
    RAISERROR('Delete any existing records for the current date',0,1) WITH NOWAIT;
							 
/*################################################################################################*/

        DELETE  FROM dbo.SummaryWeekly
        WHERE   [Date] = @ReportDate;

PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_2:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 2';
    RAISERROR('Populate SummaryWeekly',0,1) WITH NOWAIT;

/*################################################################################################*/

     INSERT  INTO dbo.SummaryWeekly
                ( [Date] ,
                  [Description] ,
                  HQID ,
                  SiteID ,
                  DepartmentID ,
                  RowType ,
                  ActiveWorkstationCount ,
                  AvailableWorkstationCount ,
                  OfflineWorkstationCount
                )
                SELECT  @ReportDate AS [Date] ,
                        bu.CustomerName AS [Description] ,
                        sbud.IDCustomer AS HQID ,
                        CASE WHEN bu.IDCustomer = 0 THEN 1
                             ELSE 0
                        END AS BusinessUnitID ,
                        CASE WHEN bu.IDCustomer = 0 THEN 1
                             ELSE 0
                        END AS BusinessUnitDepartmentID ,
                        CASE WHEN bu.IDCustomer = 0 THEN ''
                             ELSE 'HQ'
                        END AS RowType ,
                        SUM(sbud.ActiveWorkstationCount) AS ActiveWorkstationCount ,
                        SUM(sbud.AvailableWorkstationCount) AS AvailableWorkstationCount ,
                        SUM(sbud.OfflineWorkstationCount) AS OfflineWorkstationCount
                FROM    dbo.SummarySiteDepartment sbud
                        JOIN dbo.Customers bu ON sbud.IDCustomer = bu.IDCustomer
                WHERE   sbud.Date = @ReportDate
                GROUP BY bu.CustomerName ,
                        sbud.IDCustomer ,
                        CASE WHEN bu.IDCustomer = 0 THEN 1
                             ELSE 0
                        END ,
                        CASE WHEN bu.IDCustomer = 0 THEN 1
                             ELSE 0
                        END ,
                        CASE WHEN bu.IDCustomer = 0 THEN ''
                             ELSE 'HQ'
                        END
                UNION
                SELECT  @ReportDate AS [Date] ,
                        bu.SiteDescription AS [Description] ,
                        sbud.IDCustomer AS HQID ,
                        bu.IDSite ,
                        CASE WHEN bu.IDSite = 0 THEN 1
                             ELSE 0
                        END AS BusinessUnitDepartmentID ,
                        CASE WHEN bu.IDSite = 0 THEN ''
                             ELSE 'Facility'
                        END AS RowType ,
                        SUM(sbud.ActiveWorkstationCount) AS ActiveWorkstationCount ,
                        SUM(sbud.AvailableWorkstationCount) AS AvailableWorkstationCount ,
                        SUM(sbud.OfflineWorkstationCount) AS OfflineWorkstationCount
                FROM    dbo.SummarySiteDepartment sbud
                        JOIN dbo.Sites bu ON sbud.IDSite = bu.IDSite
                WHERE   sbud.Date = @ReportDate
                GROUP BY bu.SiteDescription ,
                        sbud.IDCustomer ,
                        bu.IDSite ,
                        CASE WHEN bu.IDSite = 0 THEN 1
                             ELSE 0
                        END ,
                        CASE WHEN bu.IDSite = 0 THEN ''
                             ELSE 'Facility'
                        END
                UNION
                SELECT  @ReportDate AS [Date] ,
                        bud.Description ,
                        sbud.IDCustomer AS HQID ,
                        sbud.IDSite ,
                        sbud.IDDepartment ,
                        CASE WHEN bud.IDDepartment = -1 THEN ''
                             ELSE 'Department'
                        END AS RowType ,
                        SUM(sbud.ActiveWorkstationCount) AS ActiveWorkstationCount ,
                        SUM(sbud.AvailableWorkstationCount) AS AvailableWorkstationCount ,
                        SUM(sbud.OfflineWorkstationCount) AS OfflineWorkstationCount
                FROM    dbo.SummarySiteDepartment sbud
                        JOIN dbo.Departments bud ON sbud.IDDepartment = bud.IDDepartment
                WHERE   sbud.Date = @ReportDate
                GROUP BY bud.Description ,
                        sbud.IDCustomer ,
                        sbud.IDSite ,
                        sbud.IDDepartment ,
                        CASE WHEN bud.IDDepartment = -1 THEN ''
                             ELSE 'Department'
                        END;


PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_3:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 3';
    RAISERROR('Update RunRate & AvgAmpDraw this (past) week : HQ level',0,1) WITH NOWAIT;

/*################################################################################################*/

        UPDATE  dbo.SummaryWeekly
        SET     RunRateTW = COALESCE(a.AvgRunRateTW, 0) ,
                AvgAmpDrawTW = COALESCE(a.AvgAmpDrawTW, 0)
        FROM    ( SELECT    bu.IDCustomer AS HQID ,
                            AVG(sbud.AvgRunRate) AS AvgRunRateTW ,
                            AVG(sbud.AvgAmpDraw) AS AvgAmpDrawTW
                  FROM      dbo.SummarySiteDepartment sbud
                            JOIN dbo.Customers bu ON sbud.IDCustomer = bu.IDCustomer
                  WHERE     sbud.Date BETWEEN DATEADD(DAY, -7, @ReportDate)
                                      AND     CAST(@ReportDate AS DATETIME)
                  GROUP BY  bu.IDCustomer
                ) a
        WHERE   SummaryWeekly.Date = @ReportDate
                AND SummaryWeekly.HQID = a.HQID
                AND RowType = 'HQ';

PRINT dbo.GetLapTime(@LapTimer);
/*################################################################################################*/
    Step_4:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 4';
    RAISERROR('Update RunRate & AvgAmpDraw this (last) week : HQ level',0,1) WITH NOWAIT;

/*################################################################################################*/
        UPDATE  dbo.SummaryWeekly
        SET     RunRateTW = COALESCE(a.AvgRunRateLW, 0) ,
                AvgAmpDrawTW = COALESCE(a.AvgAmpDrawLW, 0)
        FROM    ( SELECT    sbud.IDCustomer AS HQID ,
                            AVG(sbud.AvgRunRate) AS AvgRunRateLW ,
                            AVG(sbud.AvgAmpDraw) AS AvgAmpDrawLW
                  FROM      dbo.SummarySiteDepartment sbud
                            JOIN dbo.Customers bu ON sbud.IDCustomer = bu.IDCustomer
                  WHERE     sbud.Date BETWEEN DATEADD(DAY, -14, @ReportDate)
                                      AND     DATEADD(DAY, -8, @ReportDate)
                  GROUP BY  sbud.IDCustomer
                ) a
        WHERE   SummaryWeekly.Date = @ReportDate
                AND SummaryWeekly.HQID = a.HQID
                AND RowType = 'HQ';

PRINT dbo.GetLapTime(@LapTimer);
/*################################################################################################*/
    Step_5:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 5';
    RAISERROR('Update RunRate and AvgAmpDraw : this (past) week : business unit level',0,1) WITH NOWAIT;

/*################################################################################################*/


        UPDATE  dbo.SummaryWeekly
        SET     RunRateTW = COALESCE(a.AvgRunRateTW, 0) ,
                AvgAmpDrawTW = COALESCE(a.AvgAmpDrawTW, 0)
        FROM    ( SELECT    IDSite ,
                            AVG(AvgRunRate) AS AvgRunRateTW ,
                            AVG(AvgAmpDraw) AS AvgAmpDrawTW
                  FROM      dbo.SummarySiteDepartment
                  WHERE     [Date] BETWEEN DATEADD(DAY, -7, @ReportDate)
                                   AND     CAST(@ReportDate AS DATETIME)
                  GROUP BY  IDSite
                ) a
        WHERE   SummaryWeekly.Date = @ReportDate
                AND SiteID = a.IDSite
                AND RowType = 'Facility';

PRINT dbo.GetLapTime(@LapTimer);
/*################################################################################################*/
    Step_6:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 6';
    RAISERROR('Update RunRate and AvgAmpDraw : last week : business unit level',0,1) WITH NOWAIT;

/*################################################################################################*/


        UPDATE  dbo.SummaryWeekly
        SET     RunRateLW = COALESCE(a.AvgRunRateLW, 0) ,
                AvgAmpDrawLW = COALESCE(a.AvgAmpDrawLW, 0)
        FROM    ( SELECT    IDSite ,
                            AVG(AvgRunRate) AS AvgRunRateLW ,
                            AVG(AvgAmpDraw) AS AvgAmpDrawLW
                  FROM      dbo.SummarySiteDepartment
                  WHERE     [Date] BETWEEN DATEADD(DAY, -14, @ReportDate)
                                   AND     DATEADD(DAY, -8, @ReportDate)
                  GROUP BY  IDSite
                ) a
        WHERE   SummaryWeekly.Date = @ReportDate
                AND SiteID = a.IDSite
                AND RowType = 'Facility';

PRINT dbo.GetLapTime(@LapTimer);
/*################################################################################################*/
    Step_7:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 7';
    RAISERROR('Update RunRate and AvgAmpDraw : this (past) week : Department level',0,1) WITH NOWAIT;

/*################################################################################################*/

        UPDATE  dbo.SummaryWeekly
        SET     RunRateTW = COALESCE(a.AvgRunRateTW, 0) ,
                AvgAmpDrawTW = COALESCE(a.AvgAmpDrawTW, 0)
        FROM    ( SELECT    IDSite ,
                            IDDepartment ,
                            AVG(AvgRunRate) AS AvgRunRateTW ,
                            AVG(AvgAmpDraw) AS AvgAmpDrawTW
                  FROM      dbo.SummarySiteDepartment
                  WHERE     [Date] BETWEEN DATEADD(DAY, -7, @ReportDate)
                                   AND     CAST(@ReportDate AS DATETIME)
                  GROUP BY  IDSite ,
                            IDDepartment
                ) a
        WHERE   SummaryWeekly.Date = @ReportDate
                AND SiteID = a.IDSite
                AND DepartmentID = a.IDDepartment
                AND RowType = 'Department';

PRINT dbo.GetLapTime(@LapTimer);
/*################################################################################################*/
    Step_8:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 8';
    RAISERROR('Update RunRate and AvgAmpDraw : last week : Department level',0,1) WITH NOWAIT;

/*################################################################################################*/

        UPDATE  dbo.SummaryWeekly
        SET     RunRateLW = COALESCE(a.AvgRunRateLW, 0) ,
                AvgAmpDrawLW = COALESCE(a.AvgAmpDrawLW, 0)
        FROM    ( SELECT    IDSite ,
                            IDDepartment ,
                            AVG(AvgRunRate) AS AvgRunRateLW ,
                            AVG(AvgAmpDraw) AS AvgAmpDrawLW
                  FROM      dbo.SummarySiteDepartment
                  WHERE     [Date] BETWEEN DATEADD(DAY, -14, @ReportDate)
                                   AND     DATEADD(DAY, -8, @ReportDate)
                  GROUP BY  IDSite ,
                            IDDepartment
                ) a
        WHERE   SummaryWeekly.Date = @ReportDate
                AND SiteID = a.IDSite
                AND DepartmentID = a.IDDepartment
                AND RowType = 'Department';


PRINT dbo.GetLapTime(@LapTimer);
/*################################################################################################*/
    Step_9:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 9';
    RAISERROR('Update Utilization : this (past) week : HQ level',0,1) WITH NOWAIT;

/*################################################################################################*/


	/*

		Getting utilization from SummaryTrending because SummaryBusinessUnitDepartment
		is already averaged, which may skew the numbers more than those we can get
		from SummaryTrending

	*/

        UPDATE  dbo.SummaryWeekly
        SET     UtilizationTW = COALESCE(a.AvgUtilizationTW, 0)
        FROM    ( SELECT    st.IDCustomer AS HQID ,
                            AVG(st.Utilization) AS AvgUtilizationTW
                  FROM      dbo.SummarySiteDepartment st
                            JOIN dbo.Customers bu ON st.IDCustomer = bu.IDCustomer
                  WHERE     st.Date BETWEEN DATEADD(DAY, -7, @ReportDate)
                                    AND     CAST(@ReportDate AS DATETIME)
                            AND st.Utilization <> 0
                  GROUP BY  st.IDCustomer
                ) a
        WHERE   SummaryWeekly.Date = @ReportDate
                AND SummaryWeekly.HQID = a.HQID
                AND RowType = 'HQ';


PRINT dbo.GetLapTime(@LapTimer);
/*################################################################################################*/
    Step_10:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 10';
    RAISERROR('Update Utilization : last week : HQ level',0,1) WITH NOWAIT;

/*################################################################################################*/

        UPDATE  dbo.SummaryWeekly
        SET     UtilizationLW = COALESCE(a.AvgUtilizationLW, 0)
        FROM    ( SELECT    st.IDCustomer AS HQID ,
                            AVG(st.Utilization) AS AvgUtilizationLW
                  FROM      dbo.SummarySiteDepartment st
                            JOIN dbo.Customers bu ON st.IDCustomer = bu.IDCustomer
                  WHERE     st.Date BETWEEN DATEADD(DAY, -14, @ReportDate)
                                    AND     DATEADD(DAY, -8, @ReportDate)
                            AND st.Utilization <> 0
                  GROUP BY  st.IDCustomer
                ) a
        WHERE   SummaryWeekly.Date = @ReportDate
                AND SummaryWeekly.HQID = a.HQID
                AND RowType = 'HQ';

PRINT dbo.GetLapTime(@LapTimer);
/*################################################################################################*/
    Step_11:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 11';
    RAISERROR('Update Utilization : this (past) week : business unit level',0,1) WITH NOWAIT;

/*################################################################################################*/


        UPDATE  dbo.SummaryWeekly
        SET     UtilizationTW = COALESCE(a.AvgUtilizationTW, 0)
        FROM    ( SELECT    SiteID ,
                            AVG(Utilization) AS AvgUtilizationTW
                  FROM      dbo.SummaryTrending
                  WHERE     [Date] BETWEEN DATEADD(DAY, -7, @ReportDate)
                                   AND     CAST(@ReportDate AS DATETIME)
                            AND Utilization <> 0
                  GROUP BY  SiteID
                ) a
        WHERE   SummaryWeekly.Date = @ReportDate
                AND SummaryWeekly.SiteID = a.SiteID
                AND RowType = 'Facility';

PRINT dbo.GetLapTime(@LapTimer);
/*################################################################################################*/
    Step_12:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 12';
    RAISERROR('Update Utilization : last week : business unit level',0,1) WITH NOWAIT;

/*################################################################################################*/

        UPDATE  dbo.SummaryWeekly
        SET     UtilizationLW = a.AvgUtilizationLW
        FROM    ( SELECT    SiteID ,
                            AVG(Utilization) AS AvgUtilizationLW
                  FROM      dbo.SummaryTrending
                  WHERE     [Date] BETWEEN DATEADD(DAY, -14, @ReportDate)
                                   AND     DATEADD(DAY, -8, @ReportDate)
                            AND Utilization <> 0
                  GROUP BY  SiteID
                ) a
        WHERE   SummaryWeekly.Date = @ReportDate
                AND SummaryWeekly.SiteID = a.SiteID
                AND RowType = 'Facility';

PRINT dbo.GetLapTime(@LapTimer);
/*################################################################################################*/
    Step_13:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 13';
    RAISERROR('Update Utilization : this (past) week : department level',0,1) WITH NOWAIT;

/*################################################################################################*/


        UPDATE  dbo.SummaryWeekly
        SET     UtilizationTW = a.AvgUtilizationTW
        FROM    ( SELECT    SiteID ,
                            DepartmentID ,
                            AVG(Utilization) AS AvgUtilizationTW
                  FROM      dbo.SummaryTrending
                  WHERE     [Date] BETWEEN DATEADD(DAY, -7, @ReportDate)
                                   AND     CAST(@ReportDate AS DATETIME)
                            AND Utilization <> 0
                  GROUP BY  SiteID ,
                            DepartmentID
                ) a
        WHERE   SummaryWeekly.Date = @ReportDate
                AND SummaryWeekly.DepartmentID = a.DepartmentID
                AND RowType = 'Department';

PRINT dbo.GetLapTime(@LapTimer);
/*################################################################################################*/
    Step_14:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 14';
    RAISERROR('Update Utilization : last week : department level',0,1) WITH NOWAIT;

/*################################################################################################*/

        UPDATE  dbo.SummaryWeekly
        SET     UtilizationLW = COALESCE(a.AvgUtilizationLW, 0)
        FROM    ( SELECT    SiteID ,
                            DepartmentID ,
                            AVG(Utilization) AS AvgUtilizationLW
                  FROM      dbo.SummaryTrending
                  WHERE     [Date] BETWEEN DATEADD(DAY, -14, @ReportDate)
                                   AND     DATEADD(DAY, -8, @ReportDate)
                            AND Utilization <> 0
                  GROUP BY  SiteID ,
                            DepartmentID
                ) a
        WHERE   SummaryWeekly.Date = @ReportDate
                AND SummaryWeekly.DepartmentID = a.DepartmentID
                AND RowType = 'Department';

PRINT dbo.GetLapTime(@LapTimer);




GO
