SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[Purge_SupplementalPacketTables]
(@BatchSize INT = NULL)
AS        
	   DECLARE @dateFloor DATETIME2;
        DECLARE @dateCalc DATETIME2;

	   IF @BatchSize IS NULL
		  Set @BatchSize = 50000;

		---------------------------------------------------
		-- set this to the number of days you want to keep (ie. -365 for one year, -120, -90, -30, etc. )
		---------------------------------------------------
        DECLARE @daysToKeep INT;
            SET @daysToKeep = 14; --(4 months plus buffer)
		
		----------------------------------------------------------------------------------------------------------------

        SET @dateCalc = DATEADD(DAY, ( @daysToKeep * -1 ), GETUTCDATE());

		-- @dateFloor becomes the date that is the start of the day for the day found above. 
		-- This way we remove records by full days.
        SET @dateFloor = CONVERT(DATETIME, ( CONVERT(VARCHAR(10), @dateCalc, 111) ));
    
		 
		--LOOP : this starts looping through and deleting records by how many we set for each pass until we delete all records below the date floor
        WHILE EXISTS ( SELECT   1
                       FROM     Pulse.dbo.SessionData_Alerts
                       WHERE    CreatedDateUTC < @dateFloor 
				  )
	   BEGIN
		  BEGIN TRANSACTION
		  BEGIN TRY
			 DELETE  FROM Pulse.dbo.SessionData_Alerts
			 WHERE   ROW_ID IN (
				SELECT  Top (@BatchSize)
					   ROW_ID
				FROM    Pulse.dbo.SessionData_Alerts
				WHERE  CreatedDateUTC < @dateFloor
				ORDER BY CreatedDateUTC ASC);

		  	 END TRY
			 BEGIN CATCH
			     SELECT   
				ERROR_NUMBER() AS ErrorNumber  
				,ERROR_SEVERITY() AS ErrorSeverity  
				,ERROR_STATE() AS ErrorState  
				,ERROR_PROCEDURE() AS ErrorProcedure  
				,ERROR_LINE() AS ErrorLine  
				,ERROR_MESSAGE() AS ErrorMessage;  
  
			 IF @@TRANCOUNT > 0  
				ROLLBACK TRANSACTION;  
				BREAK;
			 END CATCH;  
		  IF @@TRANCOUNT > 0  		  
			 COMMIT TRANSACTION;
            END;

		  WHILE EXISTS ( SELECT   1
                       FROM     Pulse.dbo.SessionData_Summary
                       WHERE    CreatedDateUTC < @dateFloor 
				  )
		  BEGIN
		  BEGIN TRANSACTION
		  BEGIN TRY
			 DELETE  FROM Pulse.dbo.SessionData_Summary
			 WHERE   ROW_ID IN (
				SELECT  Top (@BatchSize)
					   ROW_ID
				FROM    Pulse.dbo.SessionData_Summary
				WHERE   CreatedDateUTC < @dateFloor
				ORDER BY CreatedDateUTC ASC);
		  END TRY
			 BEGIN CATCH
			     SELECT   
				ERROR_NUMBER() AS ErrorNumber  
				,ERROR_SEVERITY() AS ErrorSeverity  
				,ERROR_STATE() AS ErrorState  
				,ERROR_PROCEDURE() AS ErrorProcedure  
				,ERROR_LINE() AS ErrorLine  
				,ERROR_MESSAGE() AS ErrorMessage;  
  
			 IF @@TRANCOUNT > 0  
				ROLLBACK TRANSACTION;  
				BREAK;
			 END CATCH;  
		  IF @@TRANCOUNT > 0  		  
			 COMMIT TRANSACTION;
            END;


        WHILE EXISTS ( SELECT   1
                       FROM     Pulse.dbo.NonSessionData_Alerts
                       WHERE    CreatedDateUTC < @dateFloor 
				  )
	   BEGIN
		  BEGIN TRANSACTION
		  BEGIN TRY
			 DELETE  FROM Pulse.dbo.NonSessionData_Alerts
			 WHERE   ROW_ID IN (
				SELECT  Top (@BatchSize)
					   ROW_ID
				FROM    Pulse.dbo.NonSessionData_Alerts
				WHERE   CreatedDateUTC < @dateFloor
				ORDER BY CreatedDateUTC ASC);

		  	 END TRY
			 BEGIN CATCH
			     SELECT   
				ERROR_NUMBER() AS ErrorNumber  
				,ERROR_SEVERITY() AS ErrorSeverity  
				,ERROR_STATE() AS ErrorState  
				,ERROR_PROCEDURE() AS ErrorProcedure  
				,ERROR_LINE() AS ErrorLine  
				,ERROR_MESSAGE() AS ErrorMessage;  
  
			 IF @@TRANCOUNT > 0  
				ROLLBACK TRANSACTION;  
				BREAK;
			 END CATCH;  
		  IF @@TRANCOUNT > 0  		  
			 COMMIT TRANSACTION;
            END;

		  WHILE EXISTS ( SELECT   1
                       FROM     Pulse.dbo.NonSessionData_Summary
                       WHERE    CreatedDateUTC < @dateFloor 
				  )
		  BEGIN
		  BEGIN TRANSACTION
		  BEGIN TRY
			 DELETE  FROM Pulse.dbo.NonSessionData_Summary
			 WHERE   ROW_ID IN (
				SELECT  Top (@BatchSize)
					   ROW_ID
				FROM    Pulse.dbo.NonSessionData_Summary
				WHERE   CreatedDateUTC < @dateFloor
				ORDER BY CreatedDateUTC ASC);
		  END TRY
			 BEGIN CATCH
			     SELECT   
				ERROR_NUMBER() AS ErrorNumber  
				,ERROR_SEVERITY() AS ErrorSeverity  
				,ERROR_STATE() AS ErrorState  
				,ERROR_PROCEDURE() AS ErrorProcedure  
				,ERROR_LINE() AS ErrorLine  
				,ERROR_MESSAGE() AS ErrorMessage;  
  
			 IF @@TRANCOUNT > 0  
				ROLLBACK TRANSACTION;  
				BREAK;
			 END CATCH;  
		  IF @@TRANCOUNT > 0  		  
			 COMMIT TRANSACTION;
            END;


GO
