SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[prcDashGetWorkstations] 
    @SiteID INT = NULL ,
    @CustomerID INT = NULL
AS
    BEGIN
	
        SET NOCOUNT ON;
            BEGIN
                SELECT 
		
                        d.SerialNo AS Workstation , 
                            
                    
					    d.AssetNumber , 
                        pn.FriendlyDescription AS Description , 
		
                       
						  ISNULL(dept.Description, 'Unallocated') AS 'Department Assigned' ,
						  ISNULL(ap.Description,dept.Description) AS 'Department Reporting' ,					 
                        ISNULL(d.Floor,'Unallocated') AS 'Floor Assigned'  ,  
						    ISNULL(ap.Floor,d.Floor) AS 'Floor Reporting' ,
                        ISNULL(d.Wing,'Unallocated') AS 'Wing Assigned'  ,
						     ISNULL(ap.Wing,d.Wing) AS 'Wing Reporting' ,
    
                  
                 
					
                        aio.ProductSerialNumber  AS   'PC Serial' ,
                        cart.ProductSerialNumber AS 'Cart Serial' ,
       CONVERT(VARCHAR,CAST(CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, d.LastPostDateUTC),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS DATETIME), 100) AS 'Last Reported' ,

                        (ISNULL(d.Other, ' ') + ISNULL(d.Notes, ' ')) AS 'Notes', 
			--		ISNULL(d.Other, ' ') AS Other ,
                        d.IP ,
               
                        d.DeviceMAC,
						              --CASE WHEN d.Retired <> 0 THEN 'Y'
                    --         ELSE 'N'
                    --    END AS 'Offline Inventory' ,
	 d.LastPostDateUTC
                FROM    dbo.vwWorkstations AS d
                        LEFT JOIN dbo.PartNumber pn ON LEFT(d.SerialNo, 7) = pn.PartNumber 

                        LEFT JOIN dbo.AccessPoint ap ON d.AccessPointId = ap.ROW_ID
                        LEFT JOIN dbo.Sites apbu ON d.SiteID = apbu.IDSite 
                        JOIN dbo.AssetType dt ON d.IDAssetType = dt.IDAssetType
                        LEFT JOIN dbo.Sites bu ON d.SiteID = bu.IDSite
                        LEFT JOIN dbo.Customers parentbu ON bu.CustomerID = parentbu.IDCustomer
                        LEFT JOIN dbo.Departments dept ON d.DepartmentID = dept.IDDepartment
                        LEFT JOIN dbo.vwProductAIO aio ON d.SerialNo = aio.DeviceSerialNo
                        LEFT JOIN dbo.vwProductDCMonitor dcmONitor ON d.SerialNo = dcmONitor.DeviceSerialNo
                        LEFT JOIN dbo.vwProductCart cart ON d.SerialNo = cart.DeviceSerialNo
                WHERE   d.SiteID  = @SiteID 
		
                ORDER BY d.LastPostDateUTC desc, d.DepartmentID DESC; 
            END;
    END;



GO
