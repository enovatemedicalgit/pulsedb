SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE Procedure [dbo].[prcAssetRpt]
    @SiteID VARCHAR(20) = NULL ,
    @DeviceType INT = 99
AS
    BEGIN
        IF @DeviceType = 5
            OR @DeviceType = 9
            OR @DeviceType = 15
            BEGIN
                SELECT  *
                FROM    dbo.viewBatteryRpt
                WHERE   SiteID = @SiteID
                ORDER BY ChargeLevel;
            END;
        IF @DeviceType = 2
            OR @DeviceType = 4
            BEGIN
                SELECT  *
                FROM    dbo.viewChargerRpt
                WHERE   SiteID = @SiteID;
            END;
        IF @DeviceType = 1
            OR @DeviceType = 3
            OR @DeviceType = 6
            BEGIN
                SELECT  *
                FROM    dbo.viewWorkstationRpt
                WHERE   SiteID = @SiteID
                ORDER BY AssetStatusID DESC;
            END;
        IF @DeviceType = 99
            BEGIN
                SELECT  *
                FROM    dbo.Assets
                WHERE   SiteID = @SiteID
                        AND Retired = 0
                ORDER BY IDAssetType ,
                        AssetStatusID DESC;
            END;

    END;

--IDAssetType	Description
--1	Workstation
--2	Charger
--3	Mobius 2 Workstation
--4	Mobius 2 Charger
--5	Battery
--6	Med Workstation
--7	Patient
--8	Employee
--9	Empower Battery
--14	3.5 Battery
--15	4.0 Battery



GO
