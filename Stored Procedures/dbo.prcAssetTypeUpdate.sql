SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcAssetTypeUpdate]
    @IDAssetType INT ,
    @Category NVARCHAR(50) = NULL ,
    @Description VARCHAR(50) = NULL ,
    @IDSite NVARCHAR(50) = NULL
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
    BEGIN TRAN;

    UPDATE  dbo.AssetType
    SET     [Category] = @Category ,
            [Description] = @Description 
        
    WHERE   [IDAssetType] = @IDAssetType;
	
	-- Begin Return Select <- do not remove
    SELECT  [IDAssetType] ,
            [Category] ,
            [Description]
    FROM    dbo.AssetType
    WHERE   [IDAssetType] = @IDAssetType;	
	-- End Return Select <- do not remove

    COMMIT;




GO
