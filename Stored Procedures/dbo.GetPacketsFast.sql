SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[GetPacketsFast]
    @SiteID VARCHAR(20) = NULL ,
    @AssetSerialNumber VARCHAR(20) = NULL ,
    @RowLimit INT = 5000 ,
    @StartDate DATETIME = NULL ,
    @EndDate DATETIME = NULL ,
    @IsAdmin INT = 0 ,
    @UserTypeID INT = 0
AS
    BEGIN
        DECLARE @AssetType INT;
        IF ( @IsAdmin = 1 )
            BEGIN
                SET @RowLimit = 50000;
            END;


        
        SET @AssetType = ( SELECT   IDAssetType
                           FROM     dbo.Assets
                           WHERE    SerialNo = @AssetSerialNumber
                         );

        IF ( @StartDate IS NULL )
            BEGIN
                SET @StartDate = DATEADD(HOUR, -48, GETUTCDATE());
                SET @EndDate = DATEADD(HOUR, 6, GETUTCDATE());
            END;

        IF ( @AssetSerialNumber IS NOT NULL
             AND LEN(@AssetSerialNumber) > 1
           )
            BEGIN
                DECLARE @LastPacketDate DATETIME;

                SET @LastPacketDate = ( SELECT  LastPostDateUTC
                                        FROM    dbo.Assets
                                        WHERE   SerialNo = @AssetSerialNumber
                                      );
                SET @EndDate = DATEADD(HOUR, 6, @LastPacketDate);
                SET @StartDate = DATEADD(HOUR, -48, @LastPacketDate);
				--Here we do a lot of stuff based on Admin (More Rows/Packets) or Regular (most people)
                IF ( @IsAdmin = 1 )
                    BEGIN
                        BEGIN
                            IF ( @LastPacketDate < DATEADD(DAY, -13,
                                                           GETUTCDATE()) )
                                BEGIN
                                    IF ( @AssetType = 2
                                         OR @AssetType = 4
                                       )
                                        BEGIN
                                            PRINT 'ReturnSet2 - Charger Packets, Recent';
                                            SELECT TOP ( @RowLimit )
                                                    dt.Description AS AssetTypeDescription ,
                                                    CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, sd.CreatedDateUTC),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS 'Local TimeStamp' ,
                                                    bu.SiteDescription ,
                                                    cust.CustomerName AS IDN ,
                                                    d.AssetNumber ,
                                                    bud.Description AS AssignedDepartment ,
                                                    budActual.Description AS CommunicatingDepartment ,
                                                    sd.DeviceSerialNumber ,
                                                    sd.BatterySerialNumber ,
                                                    a.ActivityDescription ,
                                                    ap.Description AS AccessPointDescription ,
                                                    d.Floor ,
                                                    d.Wing ,
                                                    d.Other ,
                                                    sd.Bay ,
                                                    sd.FullChargeCapacity ,
                                                    sd.ChargeLevel ,
														sd.MaxCycleCount,
                                                    sd.CycleCount ,
                                                    sd.WifiFirmwareRevision ,
                                                    sd.BayWirelessRevision ,
                                                    sd.Bay1ChargerRevision ,
                                                    sd.Bay2ChargerRevision ,
                                                    sd.Bay3ChargerRevision ,
                                                    sd.Bay4ChargerRevision ,
                                                    sd.LinkQuality ,
                                                    sd.IP ,
                                                    sd.DeviceMAC ,
                                                    sd.APMAC ,
													sd.DC1BoardSerialNumber,
													sd.DC1Error,
													sd.DC1Status,
													sd.DCUnit1AVolts,
													sd.DCUnit2ACurrent,
													sd.DCUnit2BVolts,
													sd.DCUnit2BCurrent,
												    sd.dc2BoardSerialNumber,
													sd.dc2Error,
													sd.dc2Status,
													sd.DCUnit1AVolts,
													sd.DCUnit2ACurrent,
													sd.DCUnit2BVolts,
													sd.DCUnit2BCurrent,
                                                    sd.SourceIPAddress
                                            FROM    dbo.viewAllSessionDataHistory sd ( NOLOCK )
                                                    INNER JOIN dbo.Assets (NOLOCK) d ON sd.DeviceSerialNumber = d.SerialNo
                                                    LEFT JOIN dbo.Departments bud ON d.DepartmentID = bud.IDDepartment
                                                    LEFT JOIN dbo.Sites (NOLOCK) bu ON d.SiteID = bu.IDSite
                                                    LEFT JOIN dbo.AssetType (NOLOCK) dt ON sd.DeviceType = dt.IDAssetType
                                                    LEFT JOIN dbo.vwActivity (NOLOCK) a ON sd.Activity = a.ActivityCode
                                                    LEFT JOIN dbo.AccessPoint (NOLOCK) ap ON d.AccessPointId = ap.ROW_ID
                                                    LEFT JOIN dbo.Departments budActual ON ap.DepartmentID = budActual.IDDepartment
                                                    LEFT JOIN dbo.BUBStatus (NOLOCK) bub ON d.IDAssetType = bub.AssetTypeID
                                                              AND sd.BackupBatteryStatus = bub.BUBStatusCode
                                                              AND sd.ControlBoardRevision BETWEEN bub.RevisionLow
                                                              AND
                                                              bub.RevisionHigh
                                                    LEFT JOIN dbo.vwProductAIO (NOLOCK) aio ON d.SerialNo = aio.DeviceSerialNo
                                                    LEFT JOIN dbo.vwProductDCMonitor (NOLOCK) dcmonitor ON d.SerialNo = dcmonitor.DeviceSerialNo
                                                    LEFT JOIN dbo.vwProductCart (NOLOCK) cart ON d.SerialNo = cart.DeviceSerialNo
                                                    LEFT JOIN dbo.Sites buAccessPoint ON ap.SiteID = buAccessPoint.IDSite
                                                    LEFT JOIN dbo.Customers cust ON bu.CustomerID = cust.IDCustomer
                                            WHERE   sd.BatterySerialNumber = @AssetSerialNumber
                                                    OR sd.DeviceSerialNumber = @AssetSerialNumber
                                                    AND sd.CreatedDateUTC BETWEEN @StartDate AND @EndDate
                                            ORDER BY sd.CreatedDateUTC DESC;
                                        END;
                                    IF ( @AssetType = 5 )
                                        BEGIN
                                            PRINT 'ReturnSet3 - Battery Packets, Recent';
                                            SELECT TOP ( @RowLimit )
                                                    dt.Description AS AssetTypeDescription ,
                                                    CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, sd.CreatedDateUTC),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS 'Local TimeStamp' ,
                                                    bu.SiteDescription ,
                                                    cust.CustomerName AS IDN ,
                                                    d.AssetNumber ,
                                                    bud.Description AS AssignedDepartment ,
                                                    budActual.Description AS CommunicatingDepartment ,
                                                    sd.DeviceSerialNumber ,
                                                    sd.BatterySerialNumber ,
                                                    a.ActivityDescription ,
                                                    ap.Description AS AccessPointDescription ,
                                                    d.Floor ,
                                                    d.Wing ,
                                                    d.Other ,
                                                    sd.Bay ,
                                                    sd.FullChargeCapacity ,
                                                    sd.ChargeLevel ,
											
                                                    sd.CycleCount ,
															sd.MaxCycleCount,
                                                    sd.VoltageCell1 ,
                                                    sd.VoltageCell2 ,
                                                    sd.VoltageCell3 ,
                                                    sd.WifiFirmwareRevision ,
                                                    sd.BayWirelessRevision ,
                                                    sd.Bay1ChargerRevision ,
                                                    sd.Bay2ChargerRevision ,
                                                    sd.Bay3ChargerRevision ,
                                                    sd.Bay4ChargerRevision ,
                                                    sd.LinkQuality ,
                                                    sd.IP ,
                                                    sd.DeviceMAC ,
                                                    sd.APMAC ,
														sd.DC1BoardSerialNumber,
													sd.DC1Error,
													sd.DC1Status,
													sd.DCUnit1AVolts,
													sd.DCUnit2ACurrent,
													sd.DCUnit2BVolts,
													sd.DCUnit2BCurrent,
												    sd.dc2BoardSerialNumber,
													sd.dc2Error,
													sd.dc2Status,
													sd.DCUnit1AVolts,
													sd.DCUnit2ACurrent,
													sd.DCUnit2BVolts,
													sd.DCUnit2BCurrent,
                                                    sd.SourceIPAddress
                                            FROM    dbo.viewAllSessionDataHistory sd ( NOLOCK )
                                                    INNER JOIN dbo.Assets (NOLOCK) d ON sd.BatterySerialNumber = d.SerialNo
                                                    LEFT JOIN dbo.Departments bud ON d.DepartmentID = bud.IDDepartment
                                                    LEFT JOIN dbo.Sites (NOLOCK) bu ON d.SiteID = bu.IDSite
                                                    LEFT JOIN dbo.AssetType (NOLOCK) dt ON sd.DeviceType = dt.IDAssetType
                                                    LEFT JOIN dbo.vwActivity (NOLOCK) a ON sd.Activity = a.ActivityCode
                                                    LEFT JOIN dbo.AccessPoint (NOLOCK) ap ON d.AccessPointId = ap.ROW_ID
                                                    LEFT JOIN dbo.Departments budActual ON ap.DepartmentID = budActual.IDDepartment
                                                    LEFT JOIN dbo.BUBStatus (NOLOCK) bub ON d.IDAssetType = bub.AssetTypeID
                                                              AND sd.BackupBatteryStatus = bub.BUBStatusCode
                                                              AND sd.ControlBoardRevision BETWEEN bub.RevisionLow
                                                              AND
                                                              bub.RevisionHigh
                                                    LEFT JOIN dbo.vwProductAIO (NOLOCK) aio ON d.SerialNo = aio.DeviceSerialNo
                                                    LEFT JOIN dbo.vwProductDCMonitor (NOLOCK) dcmonitor ON d.SerialNo = dcmonitor.DeviceSerialNo
                                                    LEFT JOIN dbo.vwProductCart (NOLOCK) cart ON d.SerialNo = cart.DeviceSerialNo
                                                    LEFT JOIN dbo.Sites buAccessPoint ON ap.SiteID = buAccessPoint.IDSite
                                                    LEFT JOIN dbo.Customers cust ON bu.CustomerID = cust.IDCustomer
                                            WHERE   sd.BatterySerialNumber = @AssetSerialNumber
                                                    OR sd.DeviceSerialNumber = @AssetSerialNumber
                                                    AND sd.CreatedDateUTC BETWEEN @StartDate AND @EndDate
                                            ORDER BY sd.CreatedDateUTC DESC;
                                        END;
                                    ELSE
                                        BEGIN
                                            PRINT 'ReturnSet2 - Workstation Packets, Recent';
                                            SELECT TOP ( @RowLimit )
                                                    dt.Description AS AssetTypeDescription ,
                                                    CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, sd.CreatedDateUTC),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS 'Local TimeStamp' ,
                                                    bu.SiteDescription ,
                                                    cust.CustomerName AS IDN ,
                                                    d.AssetNumber ,
                                                    bud.Description AS AssignedDepartment ,
                                                    budActual.Description AS CommunicatingDepartment ,
                                                    sd.DeviceSerialNumber ,
                                                    sd.BatterySerialNumber ,
                                                    a.ActivityDescription ,
                                                    ap.Description AS AccessPointDescription ,
                                                    d.Floor ,
                                                    d.Wing ,
                                                    d.Other ,
                                                    sd.FullChargeCapacity ,
                                                    sd.ChargeLevel ,
                                                    sd.CycleCount ,
														sd.MaxCycleCount,
                                                    sd.BackupBatteryVoltage ,
                                                    sd.WifiFirmwareRevision ,
                                                    sd.HolsterChargerRevision ,
                                                    sd.DCBoardOneRevision ,
                                                    sd.DCBoardTwoRevision ,
                                                    sd.LCDRevision ,
                                                    sd.MedBoardRevision ,
                                                    sd.ControlBoardRevision ,
                                                    sd.LinkQuality ,
                                                    sd.IP ,
                                                    sd.DeviceMAC ,
                                                    sd.APMAC ,
                                                    cart.ProductSerialNumber AS CartSerialNumber ,
														sd.DC1BoardSerialNumber,
													sd.DC1Error,
													sd.DC1Status,
													sd.DCUnit1AVolts,
													sd.DCUnit2ACurrent,
													sd.DCUnit2BVolts,
													sd.DCUnit2BCurrent,
												    sd.dc2BoardSerialNumber,
													sd.dc2Error,
													sd.dc2Status,
													sd.DCUnit1AVolts,
													sd.DCUnit2ACurrent,
													sd.DCUnit2BVolts,
													sd.DCUnit2BCurrent,
                                                    sd.SourceIPAddress
                                            FROM    dbo.viewAllSessionDataHistory sd ( NOLOCK )
                                                    INNER JOIN dbo.Assets (NOLOCK) d ON sd.DeviceSerialNumber = d.SerialNo
                                                    LEFT JOIN dbo.Departments bud ON d.DepartmentID = bud.IDDepartment
                                                    LEFT JOIN dbo.Sites (NOLOCK) bu ON d.SiteID = bu.IDSite
                                                    LEFT JOIN dbo.AssetType (NOLOCK) dt ON sd.DeviceType = dt.IDAssetType
                                                    LEFT JOIN dbo.vwActivity (NOLOCK) a ON sd.Activity = a.ActivityCode
                                                    LEFT JOIN dbo.AccessPoint (NOLOCK) ap ON d.AccessPointId = ap.ROW_ID
                                                    LEFT JOIN dbo.Departments budActual ON ap.DepartmentID = budActual.IDDepartment
                                                    LEFT JOIN dbo.BUBStatus (NOLOCK) bub ON d.IDAssetType = bub.AssetTypeID
                                                              AND sd.BackupBatteryStatus = bub.BUBStatusCode
                                                              AND sd.ControlBoardRevision BETWEEN bub.RevisionLow
                                                              AND
                                                              bub.RevisionHigh
                                                    LEFT JOIN dbo.vwProductAIO (NOLOCK) aio ON d.SerialNo = aio.DeviceSerialNo
                                                    LEFT JOIN dbo.vwProductDCMonitor (NOLOCK) dcmonitor ON d.SerialNo = dcmonitor.DeviceSerialNo
                                                    LEFT JOIN dbo.vwProductCart (NOLOCK) cart ON d.SerialNo = cart.DeviceSerialNo
                                                    LEFT JOIN dbo.Sites buAccessPoint ON ap.SiteID = buAccessPoint.IDSite
                                                    LEFT JOIN dbo.Customers cust ON bu.CustomerID = cust.IDCustomer
                                            WHERE   sd.BatterySerialNumber = @AssetSerialNumber
                                                    OR sd.DeviceSerialNumber = @AssetSerialNumber
                                                    AND sd.CreatedDateUTC BETWEEN @StartDate AND @EndDate; --order by CreatedDateUTC desc
                                        END;
                                END;
                            ELSE
                                BEGIN
                                    IF ( @AssetType = 2
                                         OR @AssetType = 4
                                       )
                                        BEGIN
                                            PRINT 'ReturnSet2 - Charger Packets, Recent';
                                            SELECT TOP ( @RowLimit )
                                                    dt.Description AS AssetTypeDescription ,
                                                    CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, sd.CreatedDateUTC),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS 'Local TimeStamp' ,
                                                    bu.SiteDescription ,
                                                    cust.CustomerName AS IDN ,
                                                    d.AssetNumber ,
                                                    bud.Description AS AssignedDepartment ,
                                                    budActual.Description AS CommunicatingDepartment ,
                                                    sd.DeviceSerialNumber ,
                                                    sd.BatterySerialNumber ,
                                                    a.ActivityDescription ,
                                                    ap.Description AS AccessPointDescription ,
                                                    d.Floor ,
                                                    d.Wing ,
                                                    d.Other ,
                                                    sd.Bay ,
                                                    sd.FullChargeCapacity ,
                                                    sd.ChargeLevel ,
													
                                                    sd.CycleCount ,
														sd.MaxCycleCount,
                                                    sd.WifiFirmwareRevision ,
                                                    sd.BayWirelessRevision ,
                                                    sd.Bay1ChargerRevision ,
                                                    sd.Bay2ChargerRevision ,
                                                    sd.Bay3ChargerRevision ,
                                                    sd.Bay4ChargerRevision ,
                                                    sd.LinkQuality ,
                                                    sd.IP ,
                                                    sd.DeviceMAC ,
                                                    sd.APMAC ,
														sd.DC1BoardSerialNumber,
													sd.DC1Error,
													sd.DC1Status,
													sd.DCUnit1AVolts,
													sd.DCUnit2ACurrent,
													sd.DCUnit2BVolts,
													sd.DCUnit2BCurrent,
												    sd.dc2BoardSerialNumber,
													sd.dc2Error,
													sd.dc2Status,
													sd.DCUnit1AVolts,
													sd.DCUnit2ACurrent,
													sd.DCUnit2BVolts,
													sd.DCUnit2BCurrent,
                                                    sd.SourceIPAddress
                                            FROM    dbo.viewAllSessionData sd ( NOLOCK )
                                                    INNER JOIN dbo.Assets (NOLOCK) d ON sd.DeviceSerialNumber = d.SerialNo
                                                    LEFT JOIN dbo.Departments bud ON d.DepartmentID = bud.IDDepartment
                                                    LEFT JOIN dbo.Sites (NOLOCK) bu ON d.SiteID = bu.IDSite
                                                    LEFT JOIN dbo.AssetType (NOLOCK) dt ON sd.DeviceType = dt.IDAssetType
                                                    LEFT JOIN dbo.vwActivity (NOLOCK) a ON sd.Activity = a.ActivityCode
                                                    LEFT JOIN dbo.AccessPoint (NOLOCK) ap ON d.AccessPointId = ap.ROW_ID
                                                    LEFT JOIN dbo.Departments budActual ON ap.DepartmentID = budActual.IDDepartment
                                                    LEFT JOIN dbo.BUBStatus (NOLOCK) bub ON d.IDAssetType = bub.AssetTypeID
                                                              AND sd.BackupBatteryStatus = bub.BUBStatusCode
                                                              AND sd.ControlBoardRevision BETWEEN bub.RevisionLow
                                                              AND
                                                              bub.RevisionHigh
                                                    LEFT JOIN dbo.vwProductAIO (NOLOCK) aio ON d.SerialNo = aio.DeviceSerialNo
                                                    LEFT JOIN dbo.vwProductDCMonitor (NOLOCK) dcmonitor ON d.SerialNo = dcmonitor.DeviceSerialNo
                                                    LEFT JOIN dbo.vwProductCart (NOLOCK) cart ON d.SerialNo = cart.DeviceSerialNo
                                                    LEFT JOIN dbo.Sites buAccessPoint ON ap.SiteID = buAccessPoint.IDSite
                                                    LEFT JOIN dbo.Customers cust ON bu.CustomerID = cust.IDCustomer
                                            WHERE   sd.BatterySerialNumber = @AssetSerialNumber
                                                    OR sd.DeviceSerialNumber = @AssetSerialNumber
                                                    AND sd.CreatedDateUTC BETWEEN @StartDate AND @EndDate
                                            ORDER BY sd.CreatedDateUTC DESC;
                                        END;
                                    IF ( @AssetType = 5 )
                                        BEGIN
                                            PRINT 'ReturnSet3 - Battery Packets, Recent';
                                            SELECT TOP ( @RowLimit )
                                                    dt.Description AS AssetTypeDescription ,
                                                    CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, sd.CreatedDateUTC),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS 'Local TimeStamp' ,
                                                    bu.SiteDescription ,
                                                    cust.CustomerName AS IDN ,
                                                    d.AssetNumber ,
                                                    bud.Description AS AssignedDepartment ,
                                                    budActual.Description AS CommunicatingDepartment ,
                                                    sd.DeviceSerialNumber ,
                                                    sd.BatterySerialNumber ,
                                                    a.ActivityDescription ,
                                                    ap.Description AS AccessPointDescription ,
                                                    d.Floor ,
                                                    d.Wing ,
                                                    d.Other ,
                                                    sd.Bay ,
                                                    sd.FullChargeCapacity ,
                                                    sd.ChargeLevel ,
                                                    sd.CycleCount ,
														sd.MaxCycleCount,
                                                    sd.VoltageCell1 ,
                                                    sd.VoltageCell2 ,
                                                    sd.VoltageCell3 ,
													sd.BatteryErrorCode,
													sd.FETStatus,
													sd.SafetyStatus,
													sd.PermanentFailureStatus,
													sd.PermanentFailureAlert,
													sd.Temperature,
                                     
                                                    sd.LinkQuality ,
                                                    sd.IP ,
                                                    sd.DeviceMAC ,
                                                    sd.APMAC ,
                                                    sd.SourceIPAddress
                                            FROM    dbo.viewAllSessionData sd ( NOLOCK )
                                                    INNER JOIN dbo.Assets (NOLOCK) d ON sd.BatterySerialNumber = d.SerialNo
                                                    LEFT JOIN dbo.Departments bud ON d.DepartmentID = bud.IDDepartment
                                                    LEFT JOIN dbo.Sites (NOLOCK) bu ON d.SiteID = bu.IDSite
                                                    LEFT JOIN dbo.AssetType (NOLOCK) dt ON sd.DeviceType = dt.IDAssetType
                                                    LEFT JOIN dbo.vwActivity (NOLOCK) a ON sd.Activity = a.ActivityCode
                                                    LEFT JOIN dbo.AccessPoint (NOLOCK) ap ON d.AccessPointId = ap.ROW_ID
                                                    LEFT JOIN dbo.Departments budActual ON ap.DepartmentID = budActual.IDDepartment
                                                    LEFT JOIN dbo.BUBStatus (NOLOCK) bub ON d.IDAssetType = bub.AssetTypeID
                                                              AND sd.BackupBatteryStatus = bub.BUBStatusCode
                                                              AND sd.ControlBoardRevision BETWEEN bub.RevisionLow
                                                              AND
                                                              bub.RevisionHigh
                                                    LEFT JOIN dbo.vwProductAIO (NOLOCK) aio ON d.SerialNo = aio.DeviceSerialNo
                                                    LEFT JOIN dbo.vwProductDCMonitor (NOLOCK) dcmonitor ON d.SerialNo = dcmonitor.DeviceSerialNo
                                                    LEFT JOIN dbo.vwProductCart (NOLOCK) cart ON d.SerialNo = cart.DeviceSerialNo
                                                    LEFT JOIN dbo.Sites buAccessPoint ON ap.SiteID = buAccessPoint.IDSite
                                                    LEFT JOIN dbo.Customers cust ON bu.CustomerID = cust.IDCustomer
                                            WHERE   sd.BatterySerialNumber = @AssetSerialNumber
                                                    OR sd.DeviceSerialNumber = @AssetSerialNumber
                                                    AND sd.CreatedDateUTC BETWEEN @StartDate AND @EndDate
                                            ORDER BY sd.CreatedDateUTC DESC;
                                        END;
                                    ELSE
                                        BEGIN
                                            PRINT 'ReturnSet2 - Workstation Packets, Recent';
                                            SELECT TOP ( @RowLimit )
                                                    dt.Description AS AssetTypeDescription ,
                                                    CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, sd.CreatedDateUTC),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS 'Local TimeStamp' ,
                                                    bu.SiteDescription ,
                                                    cust.CustomerName AS IDN ,
                                                    d.AssetNumber ,
                                                    bud.Description AS AssignedDepartment ,
                                                    budActual.Description AS CommunicatingDepartment ,
                                                    sd.DeviceSerialNumber ,
                                                    sd.BatterySerialNumber ,
                                                    a.ActivityDescription ,
                                                    ap.Description AS AccessPointDescription ,
                                                    d.Floor ,
                                                    d.Wing ,
                                                    d.Other ,
                                                    sd.FullChargeCapacity ,
                                                    sd.ChargeLevel ,
                                                    sd.CycleCount ,
														sd.MaxCycleCount,
                                                    sd.BackupBatteryVoltage ,
                                                    sd.WifiFirmwareRevision ,
                                                    sd.HolsterChargerRevision ,
                                                    sd.DCBoardOneRevision ,
                                                    sd.DCBoardTwoRevision ,
                                                    sd.LCDRevision ,
                                                    sd.MedBoardRevision ,
                                                    sd.ControlBoardRevision ,
                                                    sd.LinkQuality ,
                                                    sd.IP ,
                                                    sd.DeviceMAC ,
                                                    sd.APMAC ,
                                                    cart.ProductSerialNumber AS CartSerialNumber ,
														sd.DC1BoardSerialNumber,
													sd.DC1Error,
													sd.DC1Status,
													sd.DCUnit1AVolts,
													sd.DCUnit2ACurrent,
													sd.DCUnit2BVolts,
													sd.DCUnit2BCurrent,
												    sd.dc2BoardSerialNumber,
													sd.dc2Error,
													sd.dc2Status,
													sd.DCUnit1AVolts,
													sd.DCUnit2ACurrent,
													sd.DCUnit2BVolts,
													sd.DCUnit2BCurrent,
                                                    sd.SourceIPAddress
                                            FROM    dbo.viewAllSessionData sd ( NOLOCK )
                                                    INNER JOIN dbo.Assets (NOLOCK) d ON sd.DeviceSerialNumber = d.SerialNo
                                                    LEFT JOIN dbo.Departments bud ON d.DepartmentID = bud.IDDepartment
                                                    LEFT JOIN dbo.Sites (NOLOCK) bu ON d.SiteID = bu.IDSite
                                                    LEFT JOIN dbo.AssetType (NOLOCK) dt ON sd.DeviceType = dt.IDAssetType
                                                    LEFT JOIN dbo.vwActivity (NOLOCK) a ON sd.Activity = a.ActivityCode
                                                    LEFT JOIN dbo.AccessPoint (NOLOCK) ap ON d.AccessPointId = ap.ROW_ID
                                                    LEFT JOIN dbo.Departments budActual ON ap.DepartmentID = budActual.IDDepartment
                                                    LEFT JOIN dbo.BUBStatus (NOLOCK) bub ON d.IDAssetType = bub.AssetTypeID
                                                              AND sd.BackupBatteryStatus = bub.BUBStatusCode
                                                              AND sd.ControlBoardRevision BETWEEN bub.RevisionLow
                                                              AND
                                                              bub.RevisionHigh
                                                    LEFT JOIN dbo.vwProductAIO (NOLOCK) aio ON d.SerialNo = aio.DeviceSerialNo
                                                    LEFT JOIN dbo.vwProductDCMonitor (NOLOCK) dcmonitor ON d.SerialNo = dcmonitor.DeviceSerialNo
                                                    LEFT JOIN dbo.vwProductCart (NOLOCK) cart ON d.SerialNo = cart.DeviceSerialNo
                                                    LEFT JOIN dbo.Sites buAccessPoint ON ap.SiteID = buAccessPoint.IDSite
                                                    LEFT JOIN dbo.Customers cust ON bu.CustomerID = cust.IDCustomer
                                            WHERE   sd.BatterySerialNumber = @AssetSerialNumber
                                                    OR sd.DeviceSerialNumber = @AssetSerialNumber
                                                    AND sd.CreatedDateUTC BETWEEN @StartDate AND @EndDate; --order by CreatedDateUTC desc
                                        END;
                                END;
                        END;
                    END;
                ELSE
                    BEGIN
                        IF ( @LastPacketDate < DATEADD(DAY, -13, GETUTCDATE()) )
                            BEGIN
                                IF ( @AssetType = 2
                                     OR @AssetType = 4
                                   )
                                    BEGIN
                                        PRINT 'ReturnSet2 - Charger Packets, Recent';
                                        SELECT TOP ( @RowLimit )
                                                dt.Description AS AssetTypeDescription ,
                                                CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, sd.CreatedDateUTC),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS 'Local TimeStamp' ,
                                                bu.SiteDescription ,
                                                cust.CustomerName AS IDN ,
                                                d.AssetNumber ,
                                                bud.Description AS AssignedDepartment ,
                                                budActual.Description AS CommunicatingDepartment ,
                                                sd.DeviceSerialNumber ,
                                                sd.BatterySerialNumber ,
                                                a.ActivityDescription ,
                                                ap.Description AS AccessPointDescription ,
                                                d.Floor ,
                                                d.Wing ,
                                                d.Other ,
                                                sd.Bay ,
                                                sd.FullChargeCapacity ,
                                                sd.ChargeLevel ,
                                                sd.CycleCount ,
													sd.MaxCycleCount,
                                                sd.WifiFirmwareRevision ,
                                                sd.BayWirelessRevision ,
                                                sd.Bay1ChargerRevision ,
                                                sd.Bay2ChargerRevision ,
                                                sd.Bay3ChargerRevision ,
                                                sd.Bay4ChargerRevision ,
                                                sd.LinkQuality ,
                                                sd.IP ,
                                                sd.DeviceMAC ,
                                                sd.APMAC ,
                                                sd.SourceIPAddress
                                        FROM    dbo.viewAllSessionDataHistory sd ( NOLOCK )
                                                INNER JOIN dbo.Assets (NOLOCK) d ON sd.DeviceSerialNumber = d.SerialNo
                                                LEFT JOIN dbo.Departments bud ON d.DepartmentID = bud.IDDepartment
                                                LEFT JOIN dbo.Sites (NOLOCK) bu ON d.SiteID = bu.IDSite
                                                LEFT JOIN dbo.AssetType (NOLOCK) dt ON sd.DeviceType = dt.IDAssetType
                                                LEFT JOIN dbo.vwActivity (NOLOCK) a ON sd.Activity = a.ActivityCode
                                                LEFT JOIN dbo.AccessPoint (NOLOCK) ap ON d.AccessPointId = ap.ROW_ID
                                                LEFT JOIN dbo.Departments budActual ON ap.DepartmentID = budActual.IDDepartment
                                                LEFT JOIN dbo.BUBStatus (NOLOCK) bub ON d.IDAssetType = bub.AssetTypeID
                                                              AND sd.BackupBatteryStatus = bub.BUBStatusCode
                                                              AND sd.ControlBoardRevision BETWEEN bub.RevisionLow
                                                              AND
                                                              bub.RevisionHigh
                                                LEFT JOIN dbo.vwProductAIO (NOLOCK) aio ON d.SerialNo = aio.DeviceSerialNo
                                                LEFT JOIN dbo.vwProductDCMonitor (NOLOCK) dcmonitor ON d.SerialNo = dcmonitor.DeviceSerialNo
                                                LEFT JOIN dbo.vwProductCart (NOLOCK) cart ON d.SerialNo = cart.DeviceSerialNo
                                                LEFT JOIN dbo.Sites buAccessPoint ON ap.SiteID = buAccessPoint.IDSite
                                                LEFT JOIN dbo.Customers cust ON bu.CustomerID = cust.IDCustomer
                                        WHERE   sd.BatterySerialNumber = @AssetSerialNumber
                                                OR sd.DeviceSerialNumber = @AssetSerialNumber
                                                AND sd.CreatedDateUTC BETWEEN @StartDate AND @EndDate
                                        ORDER BY sd.CreatedDateUTC DESC;
                                    END;
                                IF ( @AssetType = 5 )
                                    BEGIN
                                        PRINT 'ReturnSet3 - Battery Packets, Recent';
                                        SELECT TOP ( @RowLimit )
                                                dt.Description AS AssetTypeDescription ,
                                                CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, sd.CreatedDateUTC),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS 'Local TimeStamp' ,
                                                bu.SiteDescription ,
                                                cust.CustomerName AS IDN ,
                                                d.AssetNumber ,
                                                bud.Description AS AssignedDepartment ,
                                                budActual.Description AS CommunicatingDepartment ,
                                                sd.DeviceSerialNumber ,
                                                sd.BatterySerialNumber ,
                                                a.ActivityDescription ,
                                                ap.Description AS AccessPointDescription ,
                                                d.Floor ,
                                                d.Wing ,
                                                d.Other ,
                                                sd.Bay ,
                                                sd.FullChargeCapacity ,
                                                sd.ChargeLevel ,
                                                sd.CycleCount ,
													sd.MaxCycleCount,
                                                sd.VoltageCell1 ,
                                                sd.VoltageCell2 ,
                                                sd.VoltageCell3 ,
                                                sd.WifiFirmwareRevision ,
                                                sd.BayWirelessRevision ,
                                                sd.Bay1ChargerRevision ,
                                                sd.Bay2ChargerRevision ,
                                                sd.Bay3ChargerRevision ,
                                                sd.Bay4ChargerRevision ,
                                                sd.LinkQuality ,
                                                sd.IP ,
                                                sd.DeviceMAC ,
                                                sd.APMAC ,
                                                sd.SourceIPAddress
                                        FROM    dbo.viewAllSessionDataHistory sd ( NOLOCK )
                                                INNER JOIN dbo.Assets (NOLOCK) d ON sd.BatterySerialNumber = d.SerialNo
                                                LEFT JOIN dbo.Departments bud ON d.DepartmentID = bud.IDDepartment
                                                LEFT JOIN dbo.Sites (NOLOCK) bu ON d.SiteID = bu.IDSite
                                                LEFT JOIN dbo.AssetType (NOLOCK) dt ON sd.DeviceType = dt.IDAssetType
                                                LEFT JOIN dbo.vwActivity (NOLOCK) a ON sd.Activity = a.ActivityCode
                                                LEFT JOIN dbo.AccessPoint (NOLOCK) ap ON d.AccessPointId = ap.ROW_ID
                                                LEFT JOIN dbo.Departments budActual ON ap.DepartmentID = budActual.IDDepartment
                                                LEFT JOIN dbo.BUBStatus (NOLOCK) bub ON d.IDAssetType = bub.AssetTypeID
                                                              AND sd.BackupBatteryStatus = bub.BUBStatusCode
                                                              AND sd.ControlBoardRevision BETWEEN bub.RevisionLow
                                                              AND
                                                              bub.RevisionHigh
                                                LEFT JOIN dbo.vwProductAIO (NOLOCK) aio ON d.SerialNo = aio.DeviceSerialNo
                                                LEFT JOIN dbo.vwProductDCMonitor (NOLOCK) dcmonitor ON d.SerialNo = dcmonitor.DeviceSerialNo
                                                LEFT JOIN dbo.vwProductCart (NOLOCK) cart ON d.SerialNo = cart.DeviceSerialNo
                                                LEFT JOIN dbo.Sites buAccessPoint ON ap.SiteID = buAccessPoint.IDSite
                                                LEFT JOIN dbo.Customers cust ON bu.CustomerID = cust.IDCustomer
                                        WHERE   sd.BatterySerialNumber = @AssetSerialNumber
                                                OR sd.DeviceSerialNumber = @AssetSerialNumber
                                                AND sd.CreatedDateUTC BETWEEN @StartDate AND @EndDate
                                        ORDER BY sd.CreatedDateUTC DESC;
                                    END;
                                ELSE
                                    BEGIN
                                        PRINT 'ReturnSet2 - Workstation Packets, Recent';
                                        SELECT TOP ( @RowLimit )
                                                dt.Description AS AssetTypeDescription ,
                                                CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, sd.CreatedDateUTC),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS 'Local TimeStamp' ,
                                                bu.SiteDescription ,
                                                cust.CustomerName AS IDN ,
                                                d.AssetNumber ,
                                                bud.Description AS AssignedDepartment ,
                                                budActual.Description AS CommunicatingDepartment ,
                                                sd.DeviceSerialNumber ,
                                                sd.BatterySerialNumber ,
                                                a.ActivityDescription ,
                                                ap.Description AS AccessPointDescription ,
                                                d.Floor ,
                                                d.Wing ,
                                                d.Other ,
                                                sd.FullChargeCapacity ,
                                                sd.ChargeLevel ,
                                                sd.CycleCount ,
													sd.MaxCycleCount,
                                                sd.BackupBatteryVoltage ,
                                                sd.WifiFirmwareRevision ,
                                                sd.HolsterChargerRevision ,
                                                sd.DCBoardOneRevision ,
                                                sd.DCBoardTwoRevision ,
                                                sd.LCDRevision ,
                                                sd.MedBoardRevision ,
                                                sd.ControlBoardRevision ,
                                                sd.LinkQuality ,
                                                sd.IP ,
                                                sd.DeviceMAC ,
                                                sd.APMAC ,
                                                cart.ProductSerialNumber AS CartSerialNumber ,
                                                sd.SourceIPAddress
                                        FROM    dbo.viewAllSessionDataHistory sd ( NOLOCK )
                                                INNER JOIN dbo.Assets (NOLOCK) d ON sd.DeviceSerialNumber = d.SerialNo
                                                LEFT JOIN dbo.Departments bud ON d.DepartmentID = bud.IDDepartment
                                                LEFT JOIN dbo.Sites (NOLOCK) bu ON d.SiteID = bu.IDSite
                                                LEFT JOIN dbo.AssetType (NOLOCK) dt ON sd.DeviceType = dt.IDAssetType
                                                LEFT JOIN dbo.vwActivity (NOLOCK) a ON sd.Activity = a.ActivityCode
                                                LEFT JOIN dbo.AccessPoint (NOLOCK) ap ON d.AccessPointId = ap.ROW_ID
                                                LEFT JOIN dbo.Departments budActual ON ap.DepartmentID = budActual.IDDepartment
                                                LEFT JOIN dbo.BUBStatus (NOLOCK) bub ON d.IDAssetType = bub.AssetTypeID
                                                              AND sd.BackupBatteryStatus = bub.BUBStatusCode
                                                              AND sd.ControlBoardRevision BETWEEN bub.RevisionLow
                                                              AND
                                                              bub.RevisionHigh
                                                LEFT JOIN dbo.vwProductAIO (NOLOCK) aio ON d.SerialNo = aio.DeviceSerialNo
                                                LEFT JOIN dbo.vwProductDCMonitor (NOLOCK) dcmonitor ON d.SerialNo = dcmonitor.DeviceSerialNo
                                                LEFT JOIN dbo.vwProductCart (NOLOCK) cart ON d.SerialNo = cart.DeviceSerialNo
                                                LEFT JOIN dbo.Sites buAccessPoint ON ap.SiteID = buAccessPoint.IDSite
                                                LEFT JOIN dbo.Customers cust ON bu.CustomerID = cust.IDCustomer
                                        WHERE   sd.BatterySerialNumber = @AssetSerialNumber
                                                OR sd.DeviceSerialNumber = @AssetSerialNumber
                                                AND sd.CreatedDateUTC BETWEEN @StartDate AND @EndDate; --order by CreatedDateUTC desc
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF ( @AssetType = 2
                                     OR @AssetType = 4
                                   )
                                    BEGIN
                                        PRINT 'ReturnSet2 - Charger Packets, Recent';
                                        SELECT TOP ( @RowLimit )
                                                dt.Description AS AssetTypeDescription ,
                                                CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, sd.CreatedDateUTC),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS 'Local TimeStamp' ,
                                                bu.SiteDescription ,
                                                cust.CustomerName AS IDN ,
                                                d.AssetNumber ,
                                                bud.Description AS AssignedDepartment ,
                                                budActual.Description AS CommunicatingDepartment ,
                                                sd.DeviceSerialNumber ,
                                                sd.BatterySerialNumber ,
                                                a.ActivityDescription ,
                                                ap.Description AS AccessPointDescription ,
                                                d.Floor ,
                                                d.Wing ,
                                                d.Other ,
                                                sd.Bay ,
                                                sd.FullChargeCapacity ,
                                                sd.ChargeLevel ,
                                                sd.CycleCount ,
													sd.MaxCycleCount,
                                                sd.WifiFirmwareRevision ,
                                                sd.BayWirelessRevision ,
                                                sd.Bay1ChargerRevision ,
                                                sd.Bay2ChargerRevision ,
                                                sd.Bay3ChargerRevision ,
                                                sd.Bay4ChargerRevision ,
                                                sd.LinkQuality ,
                                                sd.IP ,
                                                sd.DeviceMAC ,
                                                sd.APMAC ,
                                                sd.SourceIPAddress
                                        FROM    dbo.viewAllSessionData sd ( NOLOCK )
                                                INNER JOIN dbo.Assets (NOLOCK) d ON sd.DeviceSerialNumber = d.SerialNo
                                                LEFT JOIN dbo.Departments bud ON d.DepartmentID = bud.IDDepartment
                                                LEFT JOIN dbo.Sites (NOLOCK) bu ON d.SiteID = bu.IDSite
                                                LEFT JOIN dbo.AssetType (NOLOCK) dt ON sd.DeviceType = dt.IDAssetType
                                                LEFT JOIN dbo.vwActivity (NOLOCK) a ON sd.Activity = a.ActivityCode
                                                LEFT JOIN dbo.AccessPoint (NOLOCK) ap ON d.AccessPointId = ap.ROW_ID
                                                LEFT JOIN dbo.Departments budActual ON ap.DepartmentID = budActual.IDDepartment
                                                LEFT JOIN dbo.BUBStatus (NOLOCK) bub ON d.IDAssetType = bub.AssetTypeID
                                                              AND sd.BackupBatteryStatus = bub.BUBStatusCode
                                                              AND sd.ControlBoardRevision BETWEEN bub.RevisionLow
                                                              AND
                                                              bub.RevisionHigh
                                                LEFT JOIN dbo.vwProductAIO (NOLOCK) aio ON d.SerialNo = aio.DeviceSerialNo
                                                LEFT JOIN dbo.vwProductDCMonitor (NOLOCK) dcmonitor ON d.SerialNo = dcmonitor.DeviceSerialNo
                                                LEFT JOIN dbo.vwProductCart (NOLOCK) cart ON d.SerialNo = cart.DeviceSerialNo
                                                LEFT JOIN dbo.Sites buAccessPoint ON ap.SiteID = buAccessPoint.IDSite
                                                LEFT JOIN dbo.Customers cust ON bu.CustomerID = cust.IDCustomer
                                        WHERE   sd.BatterySerialNumber = @AssetSerialNumber
                                                OR sd.DeviceSerialNumber = @AssetSerialNumber
                                                AND sd.CreatedDateUTC BETWEEN @StartDate AND @EndDate
                                        ORDER BY sd.CreatedDateUTC DESC;
                                    END;
                                IF ( @AssetType = 5 )
                                    BEGIN
                                        PRINT 'ReturnSet3 - Battery Packets, Recent';
                                        SELECT TOP ( @RowLimit )
                                                dt.Description AS AssetTypeDescription ,
                                                CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, sd.CreatedDateUTC),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS 'Local TimeStamp' ,
                                                bu.SiteDescription ,
                                                cust.CustomerName AS IDN ,
                                                d.AssetNumber ,
                                                bud.Description AS AssignedDepartment ,
                                                budActual.Description AS CommunicatingDepartment ,
                                                sd.DeviceSerialNumber ,
                                                sd.BatterySerialNumber ,
                                                a.ActivityDescription ,
                                                ap.Description AS AccessPointDescription ,
                                                d.Floor ,
                                                d.Wing ,
                                                d.Other ,
                                                sd.Bay ,
                                                sd.FullChargeCapacity ,
                                                sd.ChargeLevel ,
                                                sd.CycleCount ,
													sd.MaxCycleCount,
                                                sd.VoltageCell1 ,
                                                sd.VoltageCell2 ,
                                                sd.VoltageCell3 ,
                                                sd.WifiFirmwareRevision ,
                                                sd.BayWirelessRevision ,
                                                sd.Bay1ChargerRevision ,
                                                sd.Bay2ChargerRevision ,
                                                sd.Bay3ChargerRevision ,
                                                sd.Bay4ChargerRevision ,
                                                sd.LinkQuality ,
                                                sd.IP ,
                                                sd.DeviceMAC ,
                                                sd.APMAC ,
                                                sd.SourceIPAddress
                                        FROM    dbo.viewAllSessionData sd ( NOLOCK )
                                                INNER JOIN dbo.Assets (NOLOCK) d ON sd.BatterySerialNumber = d.SerialNo
                                                LEFT JOIN dbo.Departments bud ON d.DepartmentID = bud.IDDepartment
                                                LEFT JOIN dbo.Sites (NOLOCK) bu ON d.SiteID = bu.IDSite
                                                LEFT JOIN dbo.AssetType (NOLOCK) dt ON sd.DeviceType = dt.IDAssetType
                                                LEFT JOIN dbo.vwActivity (NOLOCK) a ON sd.Activity = a.ActivityCode
                                                LEFT JOIN dbo.AccessPoint (NOLOCK) ap ON d.AccessPointId = ap.ROW_ID
                                                LEFT JOIN dbo.Departments budActual ON ap.DepartmentID = budActual.IDDepartment
                                                LEFT JOIN dbo.BUBStatus (NOLOCK) bub ON d.IDAssetType = bub.AssetTypeID
                                                              AND sd.BackupBatteryStatus = bub.BUBStatusCode
                                                              AND sd.ControlBoardRevision BETWEEN bub.RevisionLow
                                                              AND
                                                              bub.RevisionHigh
                                                LEFT JOIN dbo.vwProductAIO (NOLOCK) aio ON d.SerialNo = aio.DeviceSerialNo
                                                LEFT JOIN dbo.vwProductDCMonitor (NOLOCK) dcmonitor ON d.SerialNo = dcmonitor.DeviceSerialNo
                                                LEFT JOIN dbo.vwProductCart (NOLOCK) cart ON d.SerialNo = cart.DeviceSerialNo
                                                LEFT JOIN dbo.Sites buAccessPoint ON ap.SiteID = buAccessPoint.IDSite
                                                LEFT JOIN dbo.Customers cust ON bu.CustomerID = cust.IDCustomer
                                        WHERE   sd.BatterySerialNumber = @AssetSerialNumber
                                                OR sd.DeviceSerialNumber = @AssetSerialNumber
                                                AND sd.CreatedDateUTC BETWEEN @StartDate AND @EndDate
                                        ORDER BY sd.CreatedDateUTC DESC;
                                    END;
                                ELSE
                                    BEGIN
                                        PRINT 'ReturnSet2 - Workstation Packets, Recent';
                                        SELECT TOP ( @RowLimit )
                                                dt.Description AS AssetTypeDescription ,
                                                CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, sd.CreatedDateUTC),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS 'Local TimeStamp' ,
                                                bu.SiteDescription ,
                                                cust.CustomerName AS IDN ,
                                                d.AssetNumber ,
                                                bud.Description AS AssignedDepartment ,
                                                budActual.Description AS CommunicatingDepartment ,
                                                sd.DeviceSerialNumber ,
                                                sd.BatterySerialNumber ,
                                                a.ActivityDescription ,
                                                ap.Description AS AccessPointDescription ,
                                                d.Floor ,
                                                d.Wing ,
                                                d.Other ,
                                                sd.FullChargeCapacity ,
                                                sd.ChargeLevel ,
                                                sd.CycleCount ,
													sd.MaxCycleCount,
                                                sd.BackupBatteryVoltage ,
                                                sd.WifiFirmwareRevision ,
                                                sd.HolsterChargerRevision ,
                                                sd.DCBoardOneRevision ,
                                                sd.DCBoardTwoRevision ,
                                                sd.LCDRevision ,
                                                sd.MedBoardRevision ,
                                                sd.ControlBoardRevision ,
                                                sd.LinkQuality ,
                                                sd.IP ,
                                                sd.DeviceMAC ,
                                                sd.APMAC ,
                                                cart.ProductSerialNumber AS CartSerialNumber ,
                                                sd.SourceIPAddress
                                        FROM    dbo.viewAllSessionData sd ( NOLOCK )
                                                INNER JOIN dbo.Assets (NOLOCK) d ON sd.DeviceSerialNumber = d.SerialNo
                                                LEFT JOIN dbo.Departments bud ON d.DepartmentID = bud.IDDepartment
                                                LEFT JOIN dbo.Sites (NOLOCK) bu ON d.SiteID = bu.IDSite
                                                LEFT JOIN dbo.AssetType (NOLOCK) dt ON sd.DeviceType = dt.IDAssetType
                                                LEFT JOIN dbo.vwActivity (NOLOCK) a ON sd.Activity = a.ActivityCode
                                                LEFT JOIN dbo.AccessPoint (NOLOCK) ap ON d.AccessPointId = ap.ROW_ID
                                                LEFT JOIN dbo.Departments budActual ON ap.DepartmentID = budActual.IDDepartment
                                                LEFT JOIN dbo.BUBStatus (NOLOCK) bub ON d.IDAssetType = bub.AssetTypeID
                                                              AND sd.BackupBatteryStatus = bub.BUBStatusCode
                                                              AND sd.ControlBoardRevision BETWEEN bub.RevisionLow
                                                              AND
                                                              bub.RevisionHigh
                                                LEFT JOIN dbo.vwProductAIO (NOLOCK) aio ON d.SerialNo = aio.DeviceSerialNo
                                                LEFT JOIN dbo.vwProductDCMonitor (NOLOCK) dcmonitor ON d.SerialNo = dcmonitor.DeviceSerialNo
                                                LEFT JOIN dbo.vwProductCart (NOLOCK) cart ON d.SerialNo = cart.DeviceSerialNo
                                                LEFT JOIN dbo.Sites buAccessPoint ON ap.SiteID = buAccessPoint.IDSite
                                                LEFT JOIN dbo.Customers cust ON bu.CustomerID = cust.IDCustomer
                                        WHERE   sd.BatterySerialNumber = @AssetSerialNumber
                                                OR sd.DeviceSerialNumber = @AssetSerialNumber
                                                AND sd.CreatedDateUTC BETWEEN @StartDate AND @EndDate; --order by CreatedDateUTC desc
                                    END;
                            END;
                    END;
            END;
        ELSE
            BEGIN
                --PRINT 'ReturnSet3 - Site or Customer Packets, Recent';
                SELECT TOP ( @RowLimit )
                        dt.Description AS AssetTypeDescription ,
                        CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, sd.CreatedDateUTC),
                                                       DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS 'Local TimeStamp' ,
                        bu.SiteDescription ,
                        cust.CustomerName AS IDN ,
                        d.AssetNumber ,
                        bud.Description AS Department ,
                        sd.DeviceSerialNumber ,
                        sd.BatterySerialNumber ,
                        a.ActivityDescription ,
                        ap.Description AS AccessPointDescription ,
                        d.Floor ,
                        d.Wing ,
                        d.Other ,
                        sd.Bay ,
                        sd.FullChargeCapacity ,
                        sd.ChargeLevel ,
                        sd.CycleCount ,
							sd.MaxCycleCount,
                        sd.WifiFirmwareRevision ,
                        sd.BayWirelessRevision ,
                        sd.Bay1ChargerRevision ,
                        sd.Bay2ChargerRevision ,
                        sd.Bay3ChargerRevision ,
                        sd.Bay4ChargerRevision ,
                        sd.LinkQuality ,
                        sd.IP ,
                        sd.DeviceMAC ,
                        sd.APMAC ,
                        sd.ControlBoardSerialNumber ,
                        sd.HolsterBoardSerialNumber ,
                        cart.ProductSerialNumber AS CartSerialNumber ,
                        sd.SourceIPAddress
                FROM    dbo.viewAllSessionData sd ( NOLOCK )
                        INNER JOIN dbo.Assets (NOLOCK) d ON sd.DeviceSerialNumber = d.SerialNo
                        LEFT JOIN dbo.Departments bud ON d.DepartmentID = bud.IDDepartment
                        LEFT JOIN dbo.Sites (NOLOCK) bu ON d.SiteID = bu.IDSite
                        LEFT JOIN dbo.AssetType (NOLOCK) dt ON sd.DeviceType = dt.IDAssetType
                        LEFT JOIN dbo.vwActivity (NOLOCK) a ON sd.Activity = a.ActivityCode
                        LEFT JOIN dbo.AccessPoint (NOLOCK) ap ON d.AccessPointId = ap.ROW_ID
                        LEFT JOIN dbo.BUBStatus (NOLOCK) bub ON d.IDAssetType = bub.AssetTypeID
                                                              AND sd.BackupBatteryStatus = bub.BUBStatusCode
                                                              AND sd.ControlBoardRevision BETWEEN bub.RevisionLow
                                                              AND
                                                              bub.RevisionHigh
                        LEFT JOIN dbo.vwProductAIO (NOLOCK) aio ON d.SerialNo = aio.DeviceSerialNo
                        LEFT JOIN dbo.vwProductDCMonitor (NOLOCK) dcmonitor ON d.SerialNo = dcmonitor.DeviceSerialNo
                        LEFT JOIN dbo.vwProductCart (NOLOCK) cart ON d.SerialNo = cart.DeviceSerialNo
                        LEFT JOIN dbo.Sites buAccessPoint ON ap.SiteID = buAccessPoint.IDSite
                        LEFT JOIN dbo.Customers cust ON bu.CustomerID = cust.IDCustomer
                WHERE   bu.IDSite IN (
                        SELECT  IDSite
                        FROM    dbo.Sites
                        WHERE   bu.IDSite = @SiteID); --order by CreatedDateUTC desc
            END;
			 
			
    END;
GO
