SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Description:	Gets list of facility networks
-- =============================================
CREATE PROCEDURE [dbo].[spFacilityNetworks_GetList]
AS
    BEGIN
        SET NOCOUNT ON;


        BEGIN
		--declare @BusinessUnitID int
		--set @BusinessUnitID = (select top 1 BusinessUnitID from [User] where ROW_ID = @UserID)

            SELECT  n.SiteID ,
                    bu.SiteDescription AS Facility ,
                    n.SSID ,
                    n.Channel ,
                    n.NetAuthProtocolID ,
                    nap.Description AS NetAuthProtocolDescription ,
                    n.WEPKey ,
                    n.WPAPassphrase ,
                    n.DHCP ,
                    n.IPAddress ,
                    n.SubnetMask ,
                    n.DNSServer ,
                    n.DefaultGateway ,
                    n.CreatedDateUTC ,
                    n.CreatedUserID ,
                    n.ModifiedDateUTC ,
                    n.ModifiedUserID ,
                    n.ROW_ID ,
                    CASE WHEN COALESCE(n.ModifiedUserID, 0) = 0 THEN ''
                         ELSE COALESCE(u.FirstName, '') + ' '
                              + COALESCE(u.LastName, '')
                    END AS ModifiedUserFullName ,
                    CASE WHEN COALESCE(n.CreatedUserID, 0) = 0 THEN ''
                         ELSE COALESCE(u2.FirstName, '') + ' '
                              + COALESCE(u2.LastName, '')
                    END AS CreatedUserFullName
            FROM    dbo.Network n
                    LEFT JOIN dbo.NetAuthProtocol nap ON n.NetAuthProtocolID = nap.ROW_ID
                    LEFT JOIN dbo.Sites bu ON n.SiteID = bu.IDSite
                    LEFT JOIN dbo.[User] u ON n.ModifiedUserID = u.IDSite
                    LEFT JOIN dbo.[User] u2 ON n.CreatedUserID = u2.IDSite
            WHERE   bu.IDSite IN ( SELECT   IDSite
                                   FROM     dbo.Sites )
            ORDER BY bu.SiteDescription ,
                    n.SSID;
        END;

    END;

GO
