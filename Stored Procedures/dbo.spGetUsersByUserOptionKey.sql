SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spGetUsersByUserOptionKey] (@OptionKey varchar(50))
AS
BEGIN
	select u.IDUser as UserID, u.Email, u.FirstName, u.LastName, u.FirstName + ' ' + u.LastName as FullName
	from UserOptions uo
	join [User] u
	on uo.User_ROW_ID = u.IDUser
	where OptionKey = @OptionKey
	and OptionValue = 1
END

GO
