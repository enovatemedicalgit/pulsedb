SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*################################################################################################
 Name				: Build_SummarySiteDepartment
 Date				: 
 Author				: christopher.stewart
 Company				: enovate 
 Purpose				: Build SummarySiteDepartment
 Usage				:
 Impact				:
 Required Perm			:   
 Called by			:   
##################################################################################################
 ver		author					date			change  
 1.0		Arlow.Farrell			Unknown			initial- part of [spSummarySiteDepartment_RollupNow]
 1.1		Christopher.Stewart		09302016		Optimizition and Comments
#################################################################################################*/

/*##################################################################################################
 STEPS	  Description						 
 1		  
 2
 3
 4
 5

#################################################################################################*/

CREATE PROCEDURE [dbo].[Build_SummarySiteDepartment]
AS

DECLARE @Yesterdate VARCHAR(10);
DECLARE @LapTimer DATETIME2;

SET @Yesterdate = CONVERT(VARCHAR(10), DATEADD(DAY, -1, GETDATE()), 111);
PRINT @Yesterdate;


PRINT dbo.GetMessageBorder();
/*################################################################################################*/
    Step_1:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 1'; 
    RAISERROR('Delete any existing records from same day from SummarySiteDepartment',0,1) WITH NOWAIT;
							 
/*################################################################################################*/


 
        DELETE  FROM dbo.SummarySiteDepartment
        WHERE   [Date] = @Yesterdate;

PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_2:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 2';
    RAISERROR('Populate SummarySiteDepartment',0,1) WITH NOWAIT;

/*################################################################################################*/


        INSERT  INTO dbo.SummarySiteDepartment
                ( [Date] ,
                  IDCustomer ,
                  IDSite ,
                  IDDepartment ,
                  ActiveWorkstationCount ,
                  AvailableWorkstationCount ,
                  OfflineWorkstationCount
                )
                SELECT  @Yesterdate ,
                        bu.CustomerID ,
                        d.SiteID ,
                        d.DepartmentID ,
                        SUM(CASE WHEN d.AssetStatusID = 2 THEN 1
                                 ELSE 0
                            END) --as ActiveWorkstations
                        ,
                        SUM(CASE WHEN d.AssetStatusID = 1 THEN 1
                                 ELSE 0
                            END) --as AvailableWorkstations
                        ,
                        SUM(CASE WHEN d.AssetStatusID = 0 THEN 1
                                 ELSE 0
                            END) --as OfflineWorkstations
                FROM    dbo.Assets d
                        JOIN dbo.Sites bu ON d.SiteID = bu.IDSite
                WHERE   d.IDAssetType IN ( 1, 3, 6 )
                        AND d.Retired = 0
                        AND d.OutOfService = 0
                GROUP BY bu.CustomerID ,
                        d.SiteID ,
                        d.DepartmentID ,
                        bu.SiteDescription;


PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_3:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 3';
    RAISERROR('Update SummarySiteDepartment with data from Sessions table',0,1) WITH NOWAIT;

/*################################################################################################*/





        UPDATE  dbo.SummarySiteDepartment
        SET     LoChargeInsertsCount = counts.LoChargeInsertsCount ,
                HiChargeRemovalsCount = counts.HiChargeRemovalsCount ,
                SessionCount = counts.SessionCount ,
                AvgAmpDraw = counts.AvgAmpDraw ,
                AvgChargeLevel = counts.AvgChargeLevel ,
                HiChargeInserts = counts.HiChargeInsertsCount ,
                LoChargeRemovals = counts.LoChargeRemovalsCount ,
                AvgSignalQuality = counts.AvgSignal ,
                RemainingCapacity = counts.RemainingCapacity
        FROM    ( SELECT    s.SiteID ,
                            COALESCE(d.DepartmentID, 0) AS DepartmentID ,
                            COUNT(s.ROW_ID) AS SessionCount ,
                            SUM(CASE WHEN CONVERT(VARCHAR(10), s.StartDateUTC, 111) = @Yesterdate
                                          AND s.StartChargeLevel < 90 THEN 1
                                     ELSE 0
                                END) AS LoChargeInsertsCount ,
                            SUM(CASE WHEN CONVERT(VARCHAR(10), s.EndDateUTC, 111) = @Yesterdate
                                          AND s.EndChargeLevel > 10 THEN 1
                                     ELSE 0
                                END) AS HiChargeRemovalsCount ,
                            AVG(COALESCE(s.AvgAmpDraw, 0)) AS AvgAmpDraw ,
                            AVG(( COALESCE(s.StartChargeLevel, 1)
                                  + COALESCE(s.EndChargeLevel, 1) ) / 2) AS AvgChargeLevel ,
                            SUM(CASE WHEN CONVERT(VARCHAR(10), s.StartDateUTC, 111) = @Yesterdate
                                          AND s.EndChargeLevel < 90 THEN 1
                                     ELSE 0
                                END) AS LoChargeRemovalsCount ,
                            SUM(CASE WHEN CONVERT(VARCHAR(10), s.StartDateUTC, 111) = @Yesterdate
                                          AND s.StartChargeLevel > 90 THEN 1
                                     ELSE 0
                                END) AS HiChargeInsertsCount ,
                            AVG(COALESCE(s.AvgSignalQuality, 0)) AS AvgSignal ,
                            AVG(COALESCE(s.RemainingCapacity, 0)) AS RemainingCapacity
                  FROM      dbo.Sessions s
                            JOIN dbo.Assets d ON s.DeviceSerialNumber = d.SerialNo
                  WHERE     @Yesterdate BETWEEN CONVERT(VARCHAR(10), s.StartDateUTC, 111)
                                        AND     CONVERT(VARCHAR(10), s.EndDateUTC, 111)
                            AND d.OutOfService = 0
                            AND d.Retired = 0
                  GROUP BY  s.SiteID ,
                            d.DepartmentID
                ) counts
        WHERE   IDSite = counts.SiteID
                AND IDDepartment = counts.DepartmentID
                AND Date = @Yesterdate;

PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_4:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 4';
    RAISERROR('Update SummarySiteDepartment with PCUtilization',0,1) WITH NOWAIT;

/*################################################################################################*/


	-- update AvgEstimatedPCUtilization column
        UPDATE  dbo.SummarySiteDepartment
        SET     AvgEstimatedPCUtilization = counts.AvgEstimatedPCUtilization
        FROM    ( SELECT    s.SiteID ,
                            COALESCE(d.DepartmentID, 0) AS DepartmentID ,
                            AVG(COALESCE(s.EstimatedPCUtilization, 0)) AS AvgEstimatedPCUtilization
                  FROM      dbo.Sessions s
                            JOIN dbo.Assets d ON s.DeviceSerialNumber = d.SerialNo
                  WHERE     @Yesterdate BETWEEN CONVERT(VARCHAR(10), s.StartDateUTC, 111)
                                        AND     CONVERT(VARCHAR(10), s.EndDateUTC, 111)
                            AND d.OutOfService = 0
                            AND d.Retired = 0
                            AND s.EstimatedPCUtilization IS NOT NULL -- This will only average sessions which have a value (nulls will not impact the average)
                  GROUP BY  s.SiteID ,
                            d.DepartmentID
                ) counts
        WHERE   IDSite = counts.SiteID
                AND IDDepartment = counts.DepartmentID
                AND Date = @Yesterdate; 


PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_5:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 5';
    RAISERROR('Update SummarySiteDepartment with Utilization and AvgRunRate',0,1) WITH NOWAIT;

/*################################################################################################*/


	--select * from summarysitedepartment where idsite=238 order by date desc, iddepartment
	--select * from summarytrending where siteid=238 order by date desc, departmentid
        UPDATE  dbo.SummarySiteDepartment
        SET     Utilization = st.Utilization ,
                AvgRunRate = COALESCE(st.AvgRunRate, 0)
        FROM    dbo.SummaryTrending st
        WHERE   IDSite = st.SiteID
                AND IDDepartment = st.DepartmentID
                AND SummarySiteDepartment.Date = @Yesterdate
	---and st.avgrunrate is not null
                AND CONVERT(VARCHAR(10), st.Date, 111) = @Yesterdate;	
	-- temp fix for problem with sessions table that has rows for previous
	--Update SummarySiteDepartment set
	--	  HiChargeRemovalsCount = 0
	--	, SessionCount =0
	--	, AvgAmpDraw = 0
	--	, AvgEstimatedPCUtilization = 0
	--	, AvgChargeLevel = 0
	--	, HiChargeInserts = 0
	--	, LoChargeRemovals = 0
	--	, AvgSignalQuality = 0
	--	,RemainingCapacity = 0 where AvgRunRate < 1 and SessionCount >0


 PRINT dbo.GetLapTime(@LapTimer); 


GO
