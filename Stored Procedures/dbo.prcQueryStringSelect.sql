SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcQueryStringSelect] @ROW_ID INT
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  

    BEGIN TRAN;

    SELECT  [ROW_ID] ,
            [CreatedDateUTC] ,
            [DeviceSerialNumber] ,
            [NeedsCaughtUp] ,
            [NineParsed] ,
            [Query] ,
            [QueryStringBatchID] ,
            [SourceIPAddress] ,
            [SourceTimestampUTC]
    FROM    dbo.QueryString
    WHERE   ( [ROW_ID] = @ROW_ID
              OR @ROW_ID IS NULL
            ); 

    COMMIT;



GO
