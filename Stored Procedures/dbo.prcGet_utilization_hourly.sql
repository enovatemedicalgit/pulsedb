SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[prcGet_utilization_hourly] (@SerialNumber varchar(20)= null, @FromDate DATETIME =null, @ToDate  DATETIME=null, @SiteID AS INT = 0)
AS
BEGIN
	SET NOCOUNT ON;
	IF (@SerialNumber IS NOT NULL)
	begin
	declare @BUID int
	set @BUID = (select SiteID from assets where SerialNo = @SerialNumber)
	END
    IF (@FromDate IS NULL)
	BEGIN
   SET @FromDate=  dateadd(month,-1,GETUTCDATE())
	END
    
	    IF (@ToDate IS NULL)
	BEGIN
   SET @ToDate=  GETUTCDATE()
	end
	SELECT serialno, [Date]
		,  [DateAndHour]
		,  Sum( Moves) as [Moves] 
	FROM fnHourlyMovesByWorkstation(@FromDate, @ToDate, @BUID) 
	WHERE @SiteID = @SiteID 
	AND [Date] BETWEEN @FromDate AND @ToDate  
	--AND DeviceSerialNumber = @DeviceSerialNumber  
	GROUP BY serialno,[Date] , [DateAndHour] 
  
END




GO
