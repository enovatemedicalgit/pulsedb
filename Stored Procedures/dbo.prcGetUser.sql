SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[prcGetUser]
    @Username VARCHAR(70) ,
    @Password VARCHAR(77)
AS
    SET NOCOUNT ON; 

	    BEGIN
        SELECT  ISNULL(s.IDSite, C.IDCustomer) AS SiteId  ,
		 ISNULL(s.IDSite, C.IDCustomer) AS IDSite  ,
                u.CreatedDate ,
                u.CreatedDateUTC ,
                u.UserName ,
                u.Password ,
                u.FirstName ,
                u.LastName ,
                u.Email ,
                u.Title ,    
                u.PrimaryPhone ,
                u.OtherPhone ,
				u.LoginCount,
                u.Notes ,
                u.UTC_Offset ,
                u.CreatedUserID ,
                u.ModifiedDateUTC ,
                u.IDUser ,
                u.LastLoginDateUTC ,
                u.UserTypeID ,
                s.RegionID ,
                s.SiteName ,
                s.SiteDescription ,
                s.CustomerID ,
                s.IsInternal ,
                s.SiteType ,
                s.Status ,
                s.StatusReason,
				 u.ProfileImage,
				 UO.OPtIONVALUE AS 'Edit Assets',U1.OptionValue AS 'LoginEnabled',U3.OptionValue AS 'Edit Users'

        INTO    #usr
        FROM    dbo.[User] u
                LEFT JOIN UserSites us ON us.User_Row_Id = u.IDUser
			 LEFT JOIN dbo.Sites s ON s.idSite = u.IDSite
                LEFT JOIN dbo.Customers AS C ON C.IDCustomer = u.IDSite
		LEFT JOIN dbo.UserOptions AS UO ON UO.User_ROW_ID = u.IDUser AND uo.OptionKey = 'CAN_MAINTAIN_DEVICES'
		  LEFT JOIN dbo.UserOptions AS U1 ON U1.User_ROW_ID = u.IDUser AND u1.OptionKey = 'CAN_LOGIN'
		   LEFT JOIN dbo.UserOptions AS U3 ON U3.User_ROW_ID = u.IDUser AND u3.OptionKey = 'CAN_MAINTAIN_USERS'
        WHERE   u.UserName = @Username
                AND u.Password = @Password;
        DECLARE @iusr INT; 
        SET @iusr = ( SELECT TOP 1
                                U.IDUser
                      FROM      #usr AS U
                    );
			  
        IF ( @@ROWCOUNT > 0 )
            BEGIN	
                DECLARE @cnttbl AS INT;
                SET @cnttbl = ( SELECT  COUNT(US.SessionKey)
                                FROM    dbo.UserSession AS US
                                WHERE   US.UserId = @iusr
                                        AND US.Active = 1
                              );
                IF ( @cnttbl > 0 )
                    BEGIN
                        UPDATE  dbo.UserSession
                        SET     Active = 1 ,
                                ExpirationDateUTC = DATEADD(MINUTE, 45,
                                                            ExpirationDateUTC)
                        WHERE   UserId = @iusr
                                AND Active = 1;
                    END;
                ELSE
                    BEGIN
                        INSERT  INTO dbo.UserSession
                                ( SessionKey ,
                                  UserId ,
                                  CreatedDateUTC ,
                                  Active
			                    )
                        VALUES  ( ( SELECT  NEWID()
                                  ) , -- SessionKey - uniqueidentifier
                                  ( SELECT  U.IDUser
                                    FROM    dbo.[User] AS U
                                    WHERE   U.UserName = @Username
                                            AND U.Password = @Password
                                  ) , -- UserId - int
                                  GETUTCDATE() , -- CreatedDateUTC - datetime
                                  1  -- Active - bit
			                    );
                    END;
					UPDATE dbo.[User] SET LastLoginDateUTC = GETUTCDATE(), logincount = 
					( LoginCount + 1) WHERE IDUser = @iusr;
            END;
			
        SELECT TOP 1 tusr.SiteId ,
                     tusr.IDSite ,
                     tusr.CreatedDate ,
                     tusr.CreatedDateUTC ,
                     tusr.UserName ,
                     tusr.Password ,
                     tusr.FirstName ,
                     tusr.LastName ,
                     tusr.Email ,
                     tusr.Title ,
                     tusr.PrimaryPhone ,
                     tusr.OtherPhone ,
                     tusr.LoginCount ,
                     tusr.Notes ,
                     tusr.UTC_Offset ,
                     tusr.CreatedUserID ,
                     tusr.ModifiedDateUTC ,
                     tusr.IDUser ,
                     tusr.LastLoginDateUTC ,
                     tusr.UserTypeID ,
                     tusr.RegionID ,
                     tusr.SiteName ,
                     tusr.SiteDescription ,
                     tusr.CustomerID ,
                     tusr.IsInternal ,
                     tusr.SiteType ,
                     tusr.Status ,
                     tusr.StatusReason ,
                     tusr.ProfileImage ,
                     tusr.[Edit Assets] ,
                     tusr.LoginEnabled ,
                     tusr.[Edit Users]	 ,
                US.SessionKey, us.ExpirationDateUTC
        FROM    #usr tusr
                LEFT JOIN dbo.UserSession AS US ON tusr.IDUser = US.UserId WHERE tusr.UserName = @Username AND us.Active = 1  ORDER BY us.ExpirationDateUTC DESC, US.CreatedDateUTC desc;
	
    END;



GO
