SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Description:	Uses business logic to resolve
--              the business unit id for the
--              given SourceIPAddress and APMAC
--
--              NOTE: APMAC is required so we
--              may eventually use it for
--              resolving the business unit id
--              in case the SourceIPAddress 
--              is not enough criteria, alone.
--              Requiring it from the beginning
--              will mean the code can be used
--              in compiled apps (which will 
--              provide the APMAC) without
--              requiring them to be recompiled
--              if/when we decide to use APMAC
--              for improved accuracy.


-- Modified - 1/28/2016
-- Also added the check for 192.168 to reference
-- devices reporting from within Enovate's internal
-- network. 
-- =============================================
CREATE PROCEDURE [dbo].[spRadar_GetSiteID]
    (
      @SourceIPAddress VARCHAR(20) ,
      @APMAC VARCHAR(20) = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

	--if(@SourceIPAddress like '10.51.%')
	--begin
	--	declare @StingerSiteID int
	--	set @StingerSiteID = 28 
	--	Select @StingerSiteID as SiteID
	--	return
	--end
	
	--if(@SourceIPAddress like '192.168.%')
	--begin
	--	set @StingerSiteID = 28
	--	Select @StingerSiteID as SiteID
	--	return
	--end
		
        DECLARE @SiteID INT;

        SET @SiteID = ( SELECT TOP 1
                                [SiteID]
                        FROM    dbo.SiteIPSubnet
                        WHERE   FacilityReliabilityRating > 90
                                AND @SourceIPAddress LIKE IPBase + '%'
                      );
  
        IF ( @SiteID IS NULL
             AND @APMAC IS NOT NULL
           )
            BEGIN
                SET @SiteID = ( SELECT TOP 1
                                        [SiteID]
                                FROM    dbo.SiteAPMAC
                                WHERE   APMAC = @APMAC
                                        AND FacilityReliabilityRating >= 90
                              );
            END;
        IF ( @SiteID IS NULL )
            BEGIN
                SET @SiteID = ( SELECT TOP 1
                                        [SiteID]
                                FROM    dbo.SiteAPMAC
                                WHERE   FacilityReliabilityRating >= 90
                              );
            END;



        IF ( @SiteID IS NULL )
            BEGIN	
                SET @SiteID = 28;
            END;
	
        IF ( @SourceIPAddress LIKE '10.51.%' )
            BEGIN
                SET @SiteID = 28;
            END;
	
        IF ( @SourceIPAddress LIKE '192.168.%' )
            BEGIN
                SET @SiteID = 28;
            END;

        SELECT  @SiteID AS SiteID;

    END;



GO
