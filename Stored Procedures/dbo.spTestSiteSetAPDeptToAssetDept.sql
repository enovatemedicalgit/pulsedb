SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[spTestSiteSetAPDeptToAssetDept] @IDSite INT = NULL
AS
    BEGIN

        WITH    CurrentAP
                  AS -----Current Access Points By Matching MacAddress
( SELECT    ap.ROW_ID AS APId
	--	,ass.DepartmentID AS DepartmentId
            ,
            ISNULL(ISNULL(ass.DepartmentID,
                          ( SELECT  IDDepartment
                            FROM    dbo.Departments
                            WHERE   DefaultForSite = 1
                                    AND SiteID = ass.SiteID
                          )), 0) AS DepartmentId ,
            ass.SiteID AS SiteID ,
            ass.APMAC ,
            COUNT(ass.IDAsset) AS DeviceCount
  FROM      dbo.AccessPoint ap
            LEFT JOIN dbo.Assets ass ON ap.MACAddress = ass.APMAC
			  --AND ap.DepartmentID = ass.DepartmentId
			 -- AND ap.SiteID = ass.SiteID
  WHERE     ass.LastPostDateUTC > DATEADD(DAY, -24, GETUTCDATE())
            AND ass.IDAssetType NOT IN ( 1, 3, 5, 6 )
            AND ap.Manual = 0
			AND ap.MACAddress != '00:00:00:00:00:00'
            AND ass.SiteID = @IDSite
		--AND ROW_ID IS NOT NULL
  GROUP BY  ap.ROW_ID ,
            ass.DepartmentID ,
            ass.SiteID ,
            ass.APMAC
)
            UPDATE  dbo.AccessPoint		-- Update the access point table with the last reported information from assets table where not manually scanned
            SET     DepartmentID = CurrentAP.DepartmentId ,
                    SiteID = CurrentAP.SiteID ,
                    DeviceCount = CurrentAP.DeviceCount ,
                    LastPostDateUTC = GETUTCDATE() ,
                    ModifiedDateUTC = GETUTCDATE()
--Select * 
            FROM    dbo.AccessPoint ap
                    INNER JOIN CurrentAP ON ap.ROW_ID = CurrentAP.APId
                                            AND ap.DepartmentID = CurrentAP.DepartmentId
                                            AND ap.SiteID = CurrentAP.SiteID;

        WITH    CurrentAP4
                  AS -----Current Access Points By Matching MacAddress
( SELECT    ap.ROW_ID AS APId ,
            ISNULL(ISNULL(ass.DepartmentID,
                          ( SELECT  IDDepartment
                            FROM    dbo.Departments
                            WHERE   DefaultForSite = 1
                                    AND SiteID = ass.SiteID
                          )), 0) AS DepartmentId ,
            ass.SiteID AS SiteID ,
            MAX(ISNULL(ass.APMAC, '')) AS APMAC ,
            MAX(ISNULL(ass.Floor, '')) AS Floor ,
            MAX(ISNULL(ass.Wing, '')) AS Wing ,
            COUNT(ass.IDAsset) AS DeviceCount
  FROM      dbo.AccessPoint ap
            LEFT JOIN dbo.Assets ass ON ap.MACAddress = ass.APMAC
			  --AND ap.DepartmentID = ass.DepartmentId
			 -- AND ap.SiteID = ass.SiteID
  WHERE     ass.LastPostDateUTC > DATEADD(DAY, -24, GETUTCDATE())
            AND ap.Manual = 0
            AND ass.ManualAllocation = 1
            AND ass.SiteID = @IDSite
		--AND ROW_ID IS NOT NULL
  GROUP BY  ap.ROW_ID ,
            ass.DepartmentID ,
            ass.SiteID ,
            ass.APMAC
)
            UPDATE  dbo.AccessPoint		-- Update the access point table with the last reported information from assets table where not manually scanned
            SET     DepartmentID = CurrentAP4.DepartmentId ,
                    SiteID = CurrentAP4.SiteID ,
                    DeviceCount = CurrentAP4.DeviceCount ,
                    LastPostDateUTC = GETUTCDATE() ,
                    ModifiedDateUTC = GETUTCDATE()
--Select * 
            FROM    dbo.AccessPoint ap
                    INNER JOIN CurrentAP4 ON ap.ROW_ID = CurrentAP4.APId
                                             AND ap.DepartmentID = CurrentAP4.DepartmentId
                                             AND ap.SiteID = CurrentAP4.SiteID;


        WITH    CurrentAP2
                  AS -----Current Access Points By Matching MacAddress
( SELECT    ap.ROW_ID AS APId
	--	,ass.DepartmentID AS DepartmentId
            ,
            ISNULL(ISNULL(ass.DepartmentID,
                          ( SELECT  IDDepartment
                            FROM    dbo.Departments
                            WHERE   DefaultForSite = 1
                                    AND SiteID = ass.SiteID
                          )), 0) AS DepartmentId ,
            ass.SiteID AS SiteID ,
            ass.APMAC ,
            MAX(ass.Floor) AS Floor ,
            MAX(ass.Wing) AS Wing ,
            COUNT(ass.IDAsset) AS DeviceCount
  FROM      dbo.AccessPoint ap
            LEFT JOIN dbo.Assets ass ON ap.MACAddress = ass.APMAC
			  --AND ap.DepartmentID = ass.DepartmentId
			 -- AND ap.SiteID = ass.SiteID
  WHERE     ass.LastPostDateUTC > DATEADD(DAY, -12, GETUTCDATE())
            AND ap.Manual = 1
            AND ap.SiteID = ass.SiteID
            AND ass.SiteID = @IDSite
		--AND ROW_ID IS NOT NULL
  GROUP BY  ap.ROW_ID ,
            ass.DepartmentID ,
            ass.SiteID ,
            ass.APMAC
)
            UPDATE  dbo.AccessPoint		-- Update the manual scans, but only dep/floor/wing; No site!
            SET     DepartmentID = CurrentAP2.DepartmentId ,
                    Floor = CurrentAP2.Floor ,
                    Wing = CurrentAP2.Wing ,
                    DeviceCount = CurrentAP2.DeviceCount ,
                    LastPostDateUTC = GETUTCDATE() ,
                    ModifiedDateUTC = GETUTCDATE()
--Select * 
            FROM    dbo.AccessPoint ap
                    INNER JOIN CurrentAP2 ON ap.ROW_ID = CurrentAP2.APId
                                             AND ap.DepartmentID = CurrentAP2.DepartmentId
                                             AND ap.SiteID = CurrentAP2.SiteID
                                             AND ap.SiteID = @IDSite;


        WITH    CurrentAP9
                  AS -----Current Access Points By Matching MacAddress, Same thing, but just chargers to weight it towards chargers
( SELECT    ap.ROW_ID AS APId
	--	,ass.DepartmentID AS DepartmentId
            ,
            ISNULL(ISNULL(ass.DepartmentID,
                          ( SELECT  IDDepartment
                            FROM    dbo.Departments
                            WHERE   DefaultForSite = 1
                                    AND SiteID = ass.SiteID
                          )), 0) AS DepartmentId ,
            ass.SiteID AS SiteID ,
            ass.APMAC ,
            MAX(ass.Floor) AS Floor ,
            MAX(ass.Wing) AS Wing ,
            COUNT(ass.IDAsset) AS DeviceCount
  FROM      dbo.AccessPoint ap
            LEFT JOIN dbo.Assets ass ON ap.MACAddress = ass.APMAC
			  --AND ap.DepartmentID = ass.DepartmentId
			 -- AND ap.SiteID = ass.SiteID
  WHERE     ass.LastPostDateUTC > DATEADD(DAY, -12, GETUTCDATE())
            AND ass.IDAssetType IN ( 2, 4 )
            AND ass.DepartmentID != ( SELECT    IDDepartment
                                      FROM      dbo.Departments
                                      WHERE     DefaultForSite = 1
                                                AND SiteID = ass.SiteID
                                    )
            AND ap.Manual = 1
            AND ap.SiteID = ass.SiteID
            AND ass.SiteID = @IDSite
		--AND ROW_ID IS NOT NULL
  GROUP BY  ap.ROW_ID ,
            ass.DepartmentID ,
            ass.SiteID ,
            ass.APMAC
)
            UPDATE  dbo.AccessPoint		-- Update the manual scans, but only dep/floor/wing; No site!
            SET     DepartmentID = CurrentAP9.DepartmentId ,
                    Floor = CurrentAP9.Floor ,
                    Wing = CurrentAP9.Wing ,
                    DeviceCount = CurrentAP9.DeviceCount ,
                    LastPostDateUTC = GETUTCDATE() ,
                    ModifiedDateUTC = GETUTCDATE()
--Select * 
            FROM    dbo.AccessPoint ap
                    INNER JOIN CurrentAP9 ON ap.ROW_ID = CurrentAP9.APId
                                             AND ap.DepartmentID = CurrentAP9.DepartmentId
                                             AND ap.SiteID = CurrentAP9.SiteID
                                             AND ap.SiteID = @IDSite;




        INSERT  INTO dbo.AccessPoint
                ( MACAddress ,
                  SiteID ,
                  DepartmentID ,
                  Description ,
                  DeviceCount ,
                  Wing ,
                  Floor ,
                  LastPostDateUTC ,
                  CreatedDateUTC ,
                  ModifiedDateUTC
						
                )
                SELECT  ass.APMAC AS MacAddress ,
                        ass.SiteID AS SiteID ,
                        ISNULL(ISNULL(ass.DepartmentID,
                                      ( SELECT  IDDepartment
                                        FROM    dbo.Departments
                                        WHERE   DefaultForSite = 1
                                                AND SiteID = ass.SiteID
                                      )), 0) AS DepartmentId ,
                        ISNULL(MAX(ass.Wing), '') + ' ' + ISNULL(MAX(ass.Floor),
                                                              '') AS Description ,
                        COUNT(ass.IDAsset) AS DeviceCount ,
                        MAX(ISNULL(ass.Wing, '')) ,
                        MAX(ISNULL(ass.Floor, '')) ,
                        GETUTCDATE() AS LastPostDateUTC ,
                        GETUTCDATE() AS CreatedDateUTC ,
                        GETUTCDATE() AS ModifiedDateUTC
                FROM    --	AccessPoint ap
	--RIGHT JOIN
	--	Assets ass 
	--	ON ap.MACAddress = ass.APMAC
                        dbo.Assets ass
                        LEFT JOIN dbo.Departments dept ON ass.DepartmentID = dept.IDDepartment
                WHERE   ass.LastPostDateUTC > DATEADD(HOUR, -1, GETUTCDATE())
                        AND ass.IDAssetType NOT IN ( 5 )
						AND ass.APMAC != '00:00:00:00:00:00'
                        AND NOT EXISTS ( SELECT *
                                         FROM   dbo.AccessPoint ap
                                         WHERE  ap.MACAddress = ass.APMAC
                                                AND ap.SiteID = ass.SiteID )
                        AND ass.SiteID IS NOT NULL
                        AND ass.SiteID NOT IN ( 4, 28, 1125 )
                        AND ass.APMAC IS NOT NULL
                        AND ass.DepartmentID <> 0
                        AND ( ass.DepartmentID IS NOT NULL
                              OR dept.IDDepartment IN (
                              SELECT    IDDepartment
                              FROM      dbo.Departments
                              WHERE     DefaultForSite = 1
                                        AND SiteID = ass.SiteID )
                            )
                GROUP BY ass.APMAC ,
                        ass.SiteID ,
                        ass.DepartmentID;	
    END;


GO
