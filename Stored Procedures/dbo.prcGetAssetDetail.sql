SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcGetAssetDetail] @AssetID INT
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  

    BEGIN TRAN;

    SELECT  [IDAssetDetail] ,
            [AssetID] ,
            [DetailDescription] ,
            [DetailName] ,
            [DetailValue]
    FROM    dbo.AssetDetail
    WHERE   ( [AssetID] = @AssetID ); 

    COMMIT;



GO
