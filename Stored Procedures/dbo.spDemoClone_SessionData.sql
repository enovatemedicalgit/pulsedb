SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Description:	Copies SessionData rows from the devices
--              identified on the DemoClone table
-- =============================================
CREATE PROCEDURE [dbo].[spDemoClone_SessionData]
    (
      @FromDateUTC DATETIME
    )
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        DECLARE @Row_ID AS INT;
        SET @Row_ID = ( SELECT  ( MAX(ROW_ID) + 1 )
                        FROM    dbo.SessionDataCurrent
                      );

        INSERT  INTO pulse.dbo.SessionDataCurrent
                ( [DeviceSerialNumber] ,
                  [BatterySerialNumber] ,
                  [DeviceType] ,
                  [Activity] ,
                  [Bay] ,
                  [FullChargeCapacity] ,
                  [Voltage] ,
                  [Amps] ,
                  [Temperature] ,
                  [ChargeLevel] ,
                  [CycleCount] ,
                  [MaxCycleCount] ,
                  [VoltageCell1] ,
                  [VoltageCell2] ,
                  [VoltageCell3] ,
                  [FETStatus] ,
                  [RemainingTime] ,
                  [BatteryName] ,
                  [Efficiency] ,
                  [ControlBoardRevision] ,
                  [LCDRevision] ,
                  [HolsterChargerRevision] ,
                  [DCBoardOneRevision] ,
                  [DCBoardTwoRevision] ,
                  [WifiFirmwareRevision] ,
                  [BayWirelessRevision] ,
                  [Bay1ChargerRevision] ,
                  [Bay2ChargerRevision] ,
                  [Bay3ChargerRevision] ,
                  [Bay4ChargerRevision] ,
                  [MedBoardRevision] ,
                  [BackupBatteryVoltage] ,
                  [BackupBatteryStatus] ,
                  [IsAC] ,
                  [DCUnit1AVolts] ,
                  [DCUnit1ACurrent] ,
                  [DCUnit1BVolts] ,
                  [DCUnit1BCurrent] ,
                  [DCUnit2AVolts] ,
                  [DCUnit2ACurrent] ,
                  [DCUnit2BVolts] ,
                  [DCUnit2BCurrent] ,
                  [XValue] ,
                  [XMax] ,
                  [YValue] ,
                  [YMax] ,
                  [ZValue] ,
                  [ZMax] ,
                  [Move] ,
                  [BatteryStatus] ,
                  [SafetyStatus] ,
                  [PermanentFailureStatus] ,
                  [PermanentFailureAlert] ,
                  [BatteryChargeStatus] ,
                  [BatterySafetyAlert] ,
                  [BatteryOpStatus] ,
                  [BatteryMode] ,
                  [DC1Error] ,
                  [DC1Status] ,
                  [DC2Error] ,
                  [DC2Status] ,
                  [MouseFailureNotification] ,
                  [KeyboardFailureNotification] ,
                  [WindowsShutdownNotification] ,
                  [LinkQuality] ,
                  [IP] ,
                  [DeviceMAC] ,
                  [APMAC] ,
                  [LastTransmissionStatus] ,
                  [AuthType] ,
                  [ChannelNumber] ,
                  [PortNumber] ,
                  [DHCP] ,
                  [WEPKey] ,
                  [PassCode] ,
                  [StaticIP] ,
                  [SSID] ,
                  [SparePinSwitch] ,
                  [ControlBoardSerialNumber] ,
                  [HolsterBoardSerialNumber] ,
                  [LCDBoardSerialNumber] ,
                  [DC1BoardSerialNumber] ,
                  [DC2BoardSerialNumber] ,
                  [MedBoardSerialNumber] ,
                  [BayWirelessBoardSerialNumber] ,
                  [Bay1BoardSerialNumber] ,
                  [Bay2BoardSerialNumber] ,
                  [Bay3BoardSerialNumber] ,
                  [Bay4BoardSerialNumber] ,
                  [MedBoardDrawerOpenTime] ,
                  [MedBoardDrawerCount] ,
                  [MedBoardMotorUp] ,
                  [MedBoardMotorDown] ,
                  [MedBoardUnlockCount] ,
                  [MedBoardAdminPin] ,
                  [MedBoardNarcPin] ,
                  [MedBoardUserPin1] ,
                  [MedBoardUserPin2] ,
                  [MedBoardUserPin3] ,
                  [MedBoardUserPin4] ,
                  [MedBoardUserPin5] ,
                  [GenericError] ,
                  [SessionRecordType] ,
                  [QueryStringID] ,
                  [SessionID] ,
                  [CommandCode] ,
                  [CreatedDateUTC] ,
                  [ExaminedDateUTC]
                )
                SELECT  dc.CloneSerialNumber AS [DeviceSerialNumber] ,
                        '555' + SUBSTRING(s.BatterySerialNumber, 4, 20) AS [BatterySerialNumber] ,
                        s.DeviceType ,
                        s.Activity ,
                        s.Bay ,
                        s.FullChargeCapacity ,
                        s.Voltage ,
                        s.Amps ,
                        s.Temperature ,
                        s.ChargeLevel ,
                        s.CycleCount ,
                        s.MaxCycleCount ,
                        s.VoltageCell1 ,
                        s.VoltageCell2 ,
                        s.VoltageCell3 ,
                        s.FETStatus ,
                        s.RemainingTime ,
                        s.BatteryName ,
                        s.Efficiency ,
                        s.ControlBoardRevision ,
                        s.LCDRevision ,
                        s.HolsterChargerRevision ,
                        s.DCBoardOneRevision ,
                        s.DCBoardTwoRevision ,
                        s.WifiFirmwareRevision ,
                        s.BayWirelessRevision ,
                        s.Bay1ChargerRevision ,
                        s.Bay2ChargerRevision ,
                        s.Bay3ChargerRevision ,
                        s.Bay4ChargerRevision ,
                        s.MedBoardRevision ,
                        s.BackupBatteryVoltage ,
                        s.BackupBatteryStatus ,
                        s.IsAC ,
                        s.DCUnit1AVolts ,
                        s.DCUnit1ACurrent ,
                        s.DCUnit1BVolts ,
                        s.DCUnit1BCurrent ,
                        s.DCUnit2AVolts ,
                        s.DCUnit2ACurrent ,
                        s.DCUnit2BVolts ,
                        s.DCUnit2BCurrent ,
                        s.XValue ,
                        s.XMax ,
                        s.YValue ,
                        s.YMax ,
                        s.ZValue ,
                        s.ZMax ,
                        s.Move ,
                        s.BatteryStatus ,
                        s.SafetyStatus ,
                        s.PermanentFailureStatus ,
                        s.PermanentFailureAlert ,
                        s.BatteryChargeStatus ,
                        s.BatterySafetyAlert ,
                        s.BatteryOpStatus ,
                        s.BatteryMode ,
                        s.DC1Error ,
                        s.DC1Status ,
                        s.DC2Error ,
                        s.DC2Status ,
                        s.MouseFailureNotification ,
                        s.KeyboardFailureNotification ,
                        s.WindowsShutdownNotification ,
                        s.LinkQuality ,
                        '555' + SUBSTRING(s.IP, CHARINDEX('.', s.IP), 20) AS [IP] ,
                        '55:5' + SUBSTRING(s.DeviceMAC,
                                           CHARINDEX(':', s.DeviceMAC) + 2, 20) AS [DeviceMAC] ,
                        '55:5' + SUBSTRING(s.APMAC,
                                           CHARINDEX(':', s.APMAC) + 2, 20) AS [APMAC] ,
                        s.LastTransmissionStatus ,
                        s.AuthType ,
                        s.ChannelNumber ,
                        s.PortNumber ,
                        s.DHCP ,
                        '' AS [WEPKey] ,
                        '' AS [PassCode] ,
                        s.StaticIP ,
                        s.SSID ,
                        s.SparePinSwitch ,
                        s.ControlBoardSerialNumber ,
                        s.HolsterBoardSerialNumber ,
                        s.LCDBoardSerialNumber ,
                        s.DC1BoardSerialNumber ,
                        s.DC2BoardSerialNumber ,
                        s.MedBoardSerialNumber ,
                        s.BayWirelessBoardSerialNumber ,
                        s.Bay1BoardSerialNumber ,
                        s.Bay2BoardSerialNumber ,
                        s.Bay3BoardSerialNumber ,
                        s.Bay4BoardSerialNumber ,
                        s.MedBoardDrawerOpenTime ,
                        s.MedBoardDrawerCount ,
                        s.MedBoardMotorUp ,
                        s.MedBoardMotorDown ,
                        s.MedBoardUnlockCount ,
                        s.MedBoardAdminPin ,
                        s.MedBoardNarcPin ,
                        s.MedBoardUserPin1 ,
                        s.MedBoardUserPin2 ,
                        s.MedBoardUserPin3 ,
                        s.MedBoardUserPin4 ,
                        s.MedBoardUserPin5 ,
                        s.GenericError ,
                        s.SessionRecordType ,
                        0 AS [QueryStringID] ,
                        0 AS [SessionID] ,
                        s.CommandCode ,
                        s.CreatedDateUTC ,
                        s.ExaminedDateUTC
                FROM    dbo.DemoClone dc
                        JOIN dbo.SessionDataCurrent s ON dc.SourceSerialNumber = s.DeviceSerialNumber
                WHERE   s.CreatedDateUTC > @FromDateUTC; 



    END;

GO
