SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcGetAPMacByAssetSerialNo] @SerialNo VARCHAR(20)
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  



    SELECT  [APMAC]
    FROM    dbo.Assets
    WHERE   ( [SerialNo] = @SerialNo ); 




GO
