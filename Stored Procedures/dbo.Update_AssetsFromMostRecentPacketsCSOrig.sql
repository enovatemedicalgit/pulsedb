SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*################################################################################################
 Name				: Update_AssetsFromMostRecentPackets
 Date				: 10-03-2016
 Author				: christopher.stewart
 Company				: enovate 
 Purpose				: Update the assets table using information from incoming packets
					  Replaces inline script from IsPac
 Usage				:
 Impact				:
 Required Perm			:   
 Called by			:   
##################################################################################################
 ver		author					date			change  
 1.0		Christopher.Stewart			10032016		Replaces inline script from SSIS Package
#################################################################################################*/

/*##################################################################################################
 STEPS	  Description						 
 1		  Insert Most Recent packets into tempDB
 2		  Update statistics for battery type devices
 3		  Update statistics for workstation and charger type devices
 4		  Update SiteId for all assets, except those where UseIp = 0
 5		  Update SiteId for all assets, except those with UseAp = 0
 6		  Update SiteId enovate assets, and those with UseIP and UseAp Both = 0
 7		  Update Wing, Floor, SiteWingId and SiteFloorID
 8		  Update Department
 9		  Update Department	  
 10		  Update SiteId For Batteries
 11		  Update Retired Flag
 12		  Update Department for
 13		  Clean UP TempDB	  
#################################################################################################*/

CREATE PROCEDURE [dbo].[Update_AssetsFromMostRecentPacketsCSOrig]
AS

--DECLARE @LapTimer AS DATETIME2
--DECLARE @ProcessTimer AS DATETIME2 = GETDATE();


IF OBJECT_ID('tempdb..#MostRecentPacketByDeviceByBattery') IS NOT NULL
BEGIN
	DROP TABLE #MostRecentPacketByDeviceByBattery
END



--PRINT dbo.GetMessageBorder();
--/*################################################################################################*/
--Step_1:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 1'; 
--		  RAISERROR('Insert Most Recent packets into tempDB',0,1) WITH NOWAIT;
							 
--/*################################################################################################*/


CREATE TABLE #MostRecentPacketByDeviceByBattery
(
		   BatterySerialNumber  VARCHAR(50),
		   Bay1ChargerRevision  VARCHAR(50),
		   Bay2ChargerRevision  VARCHAR(50),
		   Bay3ChargerRevision  VARCHAR(50),
		   Bay4ChargerRevision  VARCHAR(50),
		   WifiFirmwareRevision VARCHAR(50),
		   BayWirelessRevision  VARCHAR(50),
		   DeviceSerialNumber   VARCHAR(50),
		   MaxCycleCount        INT,
		   APMac                VARCHAR(50),
		   CreatedDateUTC       DATETIME,
		   ChargeLevel          INT,
		   CycleCount           INT,
		   FullChargeCapacity   INT,
		   Temperature          INT,
		   SourceIPAddress      VARCHAR(20),
		   IP                   VARCHAR(50),
		   Bay                  VARCHAR(50),
		   Activity             INT,
		   DeviceType           INT,
		   MOVE                 INT,
		   Amps                 DECIMAL(18, 2)
)

--find mismatched devices
INSERT INTO #MostRecentPacketByDeviceByBattery 
    (
	    BatterySerialNumber
	    ,Bay1ChargerRevision
	    ,Bay2ChargerRevision
	    ,Bay3ChargerRevision
	    ,Bay4ChargerRevision
	    ,WifiFirmwareRevision
	    ,BayWirelessRevision
	    ,DeviceSerialNumber
	    ,MaxCycleCount
	    ,APMac
	    ,CreatedDateUTC
	    ,ChargeLevel
	    ,CycleCount
	    ,FullChargeCapacity
	    ,Temperature
	    ,SourceIPAddress
	    ,IP
	    ,Bay
	    ,Activity
	    ,DeviceType
	    ,MOVE
	    ,Amps
	)
    SELECT 	
		BatterySerialNumber
	    ,Bay1ChargerRevision
	    ,Bay2ChargerRevision
	    ,Bay3ChargerRevision
	    ,Bay4ChargerRevision
	    ,WifiFirmwareRevision
	    ,BayWirelessRevision
	    ,DeviceSerialNumber
	    ,MaxCycleCount
	    ,APMac
	    ,CreatedDateUTC
	    ,ChargeLevel
	    ,CycleCount
	    ,FullChargeCapacity
	    ,Temperature
	    ,SourceIPAddress
	    ,IP
	    ,Bay
	    ,Activity
	    ,DeviceType
	    ,MOVE
	    ,Amps
    FROM
(
    SELECT nsd.BatterySerialNumber
	    ,isnull(nsd.Bay1ChargerRevision, '') as Bay1ChargerRevision
	    ,isnull(nsd.Bay2ChargerRevision, '') as Bay2ChargerRevision
	    ,isnull(nsd.Bay3ChargerRevision, '')as Bay3ChargerRevision
	    ,isnull(nsd.Bay4ChargerRevision, '') as Bay4ChargerRevision
	    ,isnull(nsd.WifiFirmwareRevision, '')as WifiFirmwareRevision
	    ,isnull(nsd.BayWirelessRevision, '') as BayWirelessRevision
	    ,nsd.DeviceSerialNumber
	    ,nsd.MaxCycleCount
	    ,nsd.APMac
	    ,nsd.CreatedDateUTC
	    ,nsd.ChargeLevel
	    ,nsd.CycleCount
	    ,nsd.FullChargeCapacity
	    ,nsd.Temperature
	    ,nsd.SourceIPAddress AS SourceIPAddress
	    ,nsd.IP AS IP
	    ,nsd.Bay AS Bay
	    ,nsd.Activity AS Activity
	    ,nsd.DeviceType AS DeviceType
	    ,nsd.MOVE AS MOVE
	    ,nsd.Amps AS Amps
	    ,ROW_NUMBER() OVER (PARTITION BY DeviceSerialNumber,BatterySerialNumber ORDER BY CreatedDateUTC DESC) as RN
    FROM viewAllSessionData nsd WITH (NOLOCK)
    WHERE nsd.createddateutc > dateadd(Minute, -50, getutcdate())
) MostRecent
WHERE 
	RN = 1
ORDER BY 
    CreatedDateUTC DESC

--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_2:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 2';
--		  RAISERROR('Update statistics for battery type devices',0,1) WITH NOWAIT;

--/*################################################################################################*/

UPDATE Assets
SET
	Assets.LastPostDateUTC = t.CreatedDateUTC,
	Assets.Temperature = t.Temperature,
	Assets.AssetStatusID = 1,
	assets.IsActive = 1,
	assets.Activity = ISNULL(t.Activity, a.Activity),
	assets.LastPacketType = IIF(t.DeviceType IN(1, 6), 1, 2),
	assets.ChargeLevel = CASE
						WHEN t.ChargeLevel IS NOT NULL
							AND t.ChargeLevel <> 0
						THEN t.ChargeLevel
						ELSE a.ChargeLevel
					 END,
	assets.FullChargeCapacity = CASE
							  WHEN t.FullChargeCapacity IS NOT NULL
								  AND t.ChargeLevel > 90
								  AND a.IDAssetType = 2
								  AND t.FullChargeCapacity != 22000
								  AND t.FullChargeCapacity <> 0
							  THEN t.FullChargeCapacity
							  ELSE a.FullChargeCapacity
						   END,
	assets.CycleCount = CASE
					    WHEN t.CycleCount IS NOT NULL
						    AND t.CycleCount <> 0
					    THEN t.CycleCount
					    ELSE a.CycleCount
					END,
	assets.MaxCycleCount = CASE
						  WHEN t.MaxCycleCount IS NOT NULL
							  AND t.MaxCycleCount <> 0
						  THEN t.MaxCycleCount
						  ELSE a.MaxCycleCount
					   END,
	assets.SiteID = ISNULL(
					  (
						 SELECT TOP 1
							   siteID
						 FROM
							 assets
						 WHERE  serialno = t.DeviceSerialNumber
					  ), a.SiteID),
	assets.Retired = 0,
	assets.OutOfService = 0,
	assets.SourceIPAddress = t.SourceIPAddress,
	assets.IP = t.IP,
	assets.APMac = ISNULL(t.APMac, a.APMac),
	assets.AccessPointId = ISNULL(
						    (
							   SELECT TOP 1
									row_id
							   FROM
								   AccessPoint
							   WHERE  MACAddress = t.APMAC
									AND SiteID = a.SiteID
							   ORDER BY
									  ModifiedDateUTC DESC,
									  Floor DESC,
									  DeviceCount DESC
						    ), a.AccessPointId)
FROM Assets a
	INNER JOIN
	#MostRecentPacketByDeviceByBattery t ON a.SerialNo = t.BatterySerialNumber


--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_3:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 3';
--		  RAISERROR('Update statistics for workstation and charger type devices',0,1) WITH NOWAIT;

--/*################################################################################################*/


UPDATE assets
SET 
	 assets.Temperature = t.Temperature
	,assets.AssetStatusID = 1
	,assets.IsActive = 1
	,assets.Bay1LastPostDateUTC = iif(t.BatterySerialNumber IS NOT NULL
		AND isnull(t.Bay, 0) = 1, getutcdate(), a.Bay1LastPostDateUTC)
	,assets.Bay2LastPostDateUTC = iif(t.BatterySerialNumber IS NOT NULL
		AND isnull(t.Bay, 0) = 2, getutcdate(), a.Bay1LastPostDateUTC)
	,assets.Bay3LastPostDateUTC = iif(t.BatterySerialNumber IS NOT NULL
		AND isnull(t.Bay, 0) = 3, getutcdate(), a.Bay1LastPostDateUTC)
	,assets.Bay4LastPostDateUTC = iif(t.BatterySerialNumber IS NOT NULL
		AND isnull(t.Bay, 0) = 4, getutcdate(), a.Bay1LastPostDateUTC)
	,assets.Bay1ChargerRevision = iif(t.Bay1ChargerRevision IS NOT NULL
		AND isnull(t.Bay, 0) = 1, t.Bay1ChargerRevision, a.Bay1ChargerRevision)
	,assets.Bay2ChargerRevision = iif(t.Bay2ChargerRevision IS NOT NULL
		AND isnull(t.Bay, 0) = 2, t.Bay2ChargerRevision, a.Bay2ChargerRevision)
	,assets.Bay3ChargerRevision = iif(t.Bay3ChargerRevision IS NOT NULL
		AND isnull(t.Bay, 0) = 3, t.Bay3ChargerRevision, a.Bay3ChargerRevision)
	,assets.Bay4ChargerRevision = iif(t.Bay4ChargerRevision IS NOT NULL
		AND isnull(t.Bay, 0) = 4, t.Bay4ChargerRevision, a.Bay4ChargerRevision)
	,assets.WifiFirmwareRevision = isnull(t.WifiFirmwareRevision, a.WifiFirmwareRevision)
	,assets.BayWirelessRevision = isnull(t.BayWirelessRevision, a.BayWirelessRevision)
	,assets.Retired = 0
	,assets.BayCount = IIF(t.Bay > 2, 4, a.BayCount)
	,assets.OutOfService = 0
	,assets.SourceIPAddress = t.SourceIPAddress
	,assets.LastPostDateUTC = t.createddateutc
	,assets.IP = t.IP
	,assets.APMac = isnull(t.APMac, a.APMac)
	,assets.MOVE = t.MOVE
	,assets.Amps = isnUll(t.amps, a.amps)
	,assets.Activity = isnUll(t.Activity, a.Activity)
	,assets.ChargeLevel = isnUll(t.ChargeLevel, a.ChargeLevel)
	,assets.AccessPointId = isnull((
			SELECT TOP 1 row_id
			FROM AccessPoint
			WHERE MACAddress = t.APMAC
				AND SiteID = a.SiteID
			ORDER BY  ModifiedDateUTC desc, Floor desc, DeviceCount DESC
			), a.AccessPointId)
FROM Assets a
INNER JOIN #MostRecentPacketByDeviceByBattery t
	ON a.SerialNo = t.DeviceSerialNumber



--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_4:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 4';
--		  RAISERROR('Update SiteId for all assets, except those where UseIp = 0',0,1) WITH NOWAIT;

--/*################################################################################################*/


UPDATE Assets
SET
    assets.SiteID=ISNULL(
                        (
                          SELECT TOP 1 siteID
                          FROM SiteIPSubnet sip
                          WHERE SourceIPAddressBase=LEFT( t.SourceIPAddress, LEN( t.SourceIPAddress )-CHARINDEX( '.', REVERSE( t.SourceIPAddress ) ) )
                                AND LANIPAddressBase=LEFT( t.IP, LEN( t.IP )-CHARINDEX( '.', REVERSE( t.IP ) ) )
                                AND sip.FacilityReliabilityRating>70
                        ), a.SiteID )
FROM Assets a
INNER JOIN
#MostRecentPacketByDeviceByBattery t
ON a.SerialNo=t.DeviceSerialNumber
   AND a.SiteID NOT IN
(
  SELECT SiteID
  FROM SiteAllocationExclusion
  WHERE useip=0
)
   AND ISNULL( a.ManualAllocation, 0 )=0;


--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_5:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 5';
--		  RAISERROR('Update SiteId for all assets, except those with UseAp = 0',0,1) WITH NOWAIT;

--/*################################################################################################*/

UPDATE Assets
SET
    assets.SiteID=ISNULL(
                        (
                          SELECT TOP 1 siteID
                          FROM SiteAPMAC sip
                          WHERE APMAC=t.APMac
                                AND sip.FacilityReliabilityRating>70
                        ), a.SiteID )
FROM Assets a
INNER JOIN
#MostRecentPacketByDeviceByBattery t
ON a.SerialNo=t.DeviceSerialNumber
   AND a.SiteID NOT IN
(
  SELECT SiteID
  FROM SiteAllocationExclusion
  WHERE useap=0
)
   AND ISNULL( a.ManualAllocation, 0 )=0;


--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_6:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 6';
--		  RAISERROR('Update SiteId enovate assets, and those with UseIP and UseAp Both = 0',0,1) WITH NOWAIT;

--/*################################################################################################*/


UPDATE Assets
SET assets.SiteID = isnull((
			SELECT TOP 1 siteID
			FROM AccessPoint sip
			WHERE MacAddress = t.APMac	
			), a.SiteID)
FROM Assets a
INNER JOIN #MostRecentPacketByDeviceByBattery t
	ON a.SerialNo = t.DeviceSerialNumber
		AND (
			a.SiteID not IN (
				SELECT SiteID
				FROM SiteAllocationExclusion
				WHERE useip = 0  and useap = 0
				)
			OR a.SiteID in (1125,28)
			)  and ISNULL(a.ManualAllocation,0) = 0


--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_7:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 7';
--		  RAISERROR('Update Wing',0,1) WITH NOWAIT;

--/*################################################################################################*/



--End of Site Allocation checks

--Now do wings

UPDATE Assets
SET
    assets.Wing=ISNULL(
                      (
                        SELECT TOP 1 sip.Wing
                        FROM AccessPoint
                        AS sip
                        WHERE MacAddress=t.APMac
                              AND sip.Wing<>''
                              AND sip.Manual=1
                        ORDER BY sip.DeviceCount DESC
                      ), a.Wing ),
    assets.Floor=ISNULL(
                       (
                         SELECT TOP 1 sip.Floor
                         FROM AccessPoint
                         AS sip
                         WHERE MacAddress=t.APMac
                               AND sip.Manual=1
                               AND sip.floor<>''
                         ORDER BY sip.Floor DESC
                       ), a.Floor ),
    assets.SiteWingId=ISNULL(
                            (
                              SELECT TOP 1 sip.SiteWingId
                              FROM AccessPoint
                              AS sip
                              WHERE MacAddress=t.APMac
                                    AND sip.SiteWingId IS NOT NULL
                                    AND sip.Manual=1
                              ORDER BY sip.DeviceCount DESC
                            ), a.SiteWingId ),
    assets.SiteFloorId=ISNULL(
                             (
                               SELECT TOP 1 sip.SiteFloorID
                               FROM AccessPoint
                               AS sip
                               WHERE MacAddress=t.APMac
                                     AND sip.Manual=1
                                     AND sip.SitefloorId IS NOT NULL
                               ORDER BY sip.Floor DESC
                             ), a.SiteFloorId )
FROM Assets a
INNER JOIN
#MostRecentPacketByDeviceByBattery t
ON a.SerialNo=t.DeviceSerialNumber
   AND (a.SiteID NOT IN
       (
         SELECT SiteID
         FROM SiteAllocationExclusion
         WHERE useap=0
       )
        OR a.SiteID IN
       (1125, 28))
   AND ISNULL( a.ManualAllocation, 0 )=0;


--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_8:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 8';
--		  RAISERROR('Update Department based access points from manual scan',0,1) WITH NOWAIT;

--/*################################################################################################*/


UPDATE Assets
SET
    assets.DepartmentID=ISNULL(
                              (
                                SELECT TOP 1 DepartmentID
                                FROM AccessPoint sip
                                WHERE MacAddress=t.APMac
                                      AND sip.Manual=1
                                      AND sip.DepartmentID!=''
                                      AND sip.DepartmentID NOT IN
                                (
                                  SELECT IdDepartment
                                  FROM dbo.Departments d
                                  WHERE d.SiteID=a.SiteID
                                        AND d.DefaultForSite=1
                                )
                                ORDER BY sip.DeviceCount DESC
                              ), a.DepartmentID )
FROM Assets a
INNER JOIN
#MostRecentPacketByDeviceByBattery t
ON a.SerialNo=t.DeviceSerialNumber
   AND (a.SiteID NOT IN
       (
         SELECT SiteID
         FROM SiteAllocationExclusion
         WHERE useap=0
       )
        OR a.SiteID IN
       (1125, 28))
   AND ISNULL( a.ManualAllocation, 0 )=0
   AND (a.IDAssetType NOT IN
       (2, 4)
        OR a.DepartmentID IN
       (
         SELECT IdDepartment
         FROM dbo.Departments d
         WHERE d.SiteID=a.SiteID
               AND d.DefaultForSite=1
       ));

--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_9:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 9';
--		  RAISERROR('Update Department based on access points without manual scan',0,1) WITH NOWAIT;

--/*################################################################################################*/

UPDATE Assets
SET
    assets.DepartmentID=ISNULL(
                              (
                                SELECT TOP 1 sip.DepartmentID
                                FROM AccessPoint sip
                                WHERE MacAddress=t.APMac
                                      AND sip.Manual=0
                                      AND sip.DepartmentID!=''
                                      AND sip.DepartmentID NOT IN
                                (
                                  SELECT TOP 1 IdDepartment
                                  FROM dbo.Departments d
                                  WHERE d.SiteID=a.SiteID
                                        AND d.DefaultForSite=1
                                  ORDER BY sip.DeviceCount DESC
                                )
                              ), a.DepartmentID )
FROM Assets a
INNER JOIN
#MostRecentPacketByDeviceByBattery t
ON a.SerialNo=t.DeviceSerialNumber
   AND (a.SiteID NOT IN
       (
         SELECT SiteID
         FROM SiteAllocationExclusion
         WHERE useap=0
       )
        OR a.SiteID IN
       (1125, 28))
   AND ISNULL( a.ManualAllocation, 0 )=0
   AND (a.IDAssetType NOT IN
       (2, 4)
        OR a.DepartmentID IN
       (
         SELECT TOP 1 IdDepartment
         FROM dbo.Departments d
         WHERE d.SiteID=a.SiteID
               AND d.DefaultForSite=1
       ));

--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_10:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 10';
--		  RAISERROR('Update SiteId For Batteries ',0,1) WITH NOWAIT;

--/*################################################################################################*/



UPDATE Assets
SET assets.SiteID = isnull(( SELECT TOP 1 siteid
			FROM assets 
			WHERE serialno = t.DeviceSerialNumber)
			, a.SiteID)
FROM Assets a
INNER JOIN #MostRecentPacketByDeviceByBattery t
	ON a.SerialNo = t.BatterySerialNumber
where ISNULL(a.ManualAllocation,0) = 0

		

--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_11:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 11';
--		  RAISERROR('Update Retired Flag ',0,1) WITH NOWAIT;

--/*################################################################################################*/



UPDATE assets
SET Retired = 1
WHERE (
		lastpostdateutc < dateadd(day, - 29, getutcdate())
		OR lastpostdateutc IS NULL
		)
	AND CreatedDateUTC < dateadd(month, - 3, getutcdate())


--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_12:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 12';
--		  RAISERROR('Update to Default Department',0,1) WITH NOWAIT;

--/*################################################################################################*/
UPDATE assets
SET
    DepartmentID=
(
  SELECT iddepartment
  FROM departments
  WHERE DefaultForSite=1
        AND siteid=asst.siteid
)
FROM assets asst
WHERE asst.serialno IN
(
  SELECT serialno
  FROM assets ass
  LEFT JOIN
  dbo.Sites s
  ON s.IDSite=ass.SiteID
      LEFT JOIN
      dbo.Departments dep
      ON ass.DepartmentID=dep.IDDepartment
  WHERE dep.SiteID<>ass.SiteID
        AND ass.siteid NOT IN
  (1125,
   28)
        AND ass.IDAssetType<>5
);



--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_13:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 13';
--		  RAISERROR('Clean up TempDB',0,1) WITH NOWAIT;

--/*################################################################################################*/

IF OBJECT_ID('tempdb..#MostRecentPacketByDeviceByBattery') IS NOT NULL
BEGIN
	DROP TABLE #MostRecentPacketByDeviceByBattery
END

--PRINT dbo.GetLapTime(@LapTimer); 
--PRINT 'Combined Steps ' + dbo.GetLapTime(@ProcessTimer);
GO
