SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[spGetSSISQSBatch]
    (
      @GuidVar AS UNIQUEIDENTIFIER
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @qs TABLE
            (
              [QueryStringID] [INT] ,
              [QueryStringBatchID] [INT] ,
              [CreatedDateUTC] [DATETIME] ,
              [SourceTimestampUTC] [DATETIME] ,
              [IsSessionData] [BIT] ,
              [DeviceSerialNumber] [VARCHAR](50) ,
              [BatterySerialNumber] [VARCHAR](50) ,
              [FixedBatterySerialNumber] [VARCHAR](50) ,
              [DeviceType] [INT] ,
              [Activity] [INT] ,
              [Bay] [VARCHAR](50) ,
              [FullChargeCapacity] [INT] ,
              [Voltage] [DECIMAL](18, 2) ,
              [Amps] [DECIMAL](18, 2) ,
              [Temperature] [INT] ,
              [ChargeLevel] [INT] ,
              [CycleCount] [INT] ,
              [MaxCycleCount] [INT] ,
              [VoltageCell1] [DECIMAL](18, 2) ,
              [VoltageCell2] [DECIMAL](18, 2) ,
              [VoltageCell3] [DECIMAL](18, 2) ,
              [FETStatus] [INT] ,
              [RemainingTime] [INT] ,
              [BatteryName] [VARCHAR](50) ,
              [Efficiency] [DECIMAL](18, 2) ,
              [ControlBoardRevision] [VARCHAR](50) ,
              [LCDRevision] [VARCHAR](50) ,
              [HolsterChargerRevision] [VARCHAR](50) ,
              [DCBoardOneRevision] [VARCHAR](50) ,
              [DCBoardTwoRevision] [VARCHAR](50) ,
              [WifiFirmwareRevision] [VARCHAR](50) ,
              [BayWirelessRevision] [VARCHAR](50) ,
              [Bay1ChargerRevision] [VARCHAR](50) ,
              [Bay2ChargerRevision] [VARCHAR](50) ,
              [Bay3ChargerRevision] [VARCHAR](50) ,
              [Bay4ChargerRevision] [VARCHAR](50) ,
              [MedBoardRevision] [VARCHAR](50) ,
              [BackupBatteryVoltage] [DECIMAL](18, 2) ,
              [BackupBatteryStatus] [INT] ,
              [IsAC] [BIT] ,
              [DCUnit1AVolts] [DECIMAL](18, 2) ,
              [DCUnit1ACurrent] [DECIMAL](18, 2) ,
              [DCUnit1BVolts] [DECIMAL](18, 2) ,
              [DCUnit1BCurrent] [DECIMAL](18, 2) ,
              [DCUnit2AVolts] [DECIMAL](18, 2) ,
              [DCUnit2ACurrent] [DECIMAL](18, 2) ,
              [DCUnit2BVolts] [DECIMAL](18, 2) ,
              [DCUnit2BCurrent] [DECIMAL](18, 2) ,
              [XValue] [INT] ,
              [XMax] [INT] ,
              [YValue] [INT] ,
              [YMax] [INT] ,
              [ZValue] [INT] ,
              [ZMax] [INT] ,
              [Move] [INT] ,
              [BatteryStatus] [INT] ,
              [SafetyStatus] [INT] ,
              [PermanentFailureStatus] [INT] ,
              [PermanentFailureAlert] [INT] ,
              [BatteryChargeStatus] [INT] ,
              [BatterySafetyAlert] [INT] ,
              [BatteryOpStatus] [INT] ,
              [BatteryMode] [INT] ,
              [DC1Error] [INT] ,
              [DC1Status] [INT] ,
              [DC2Error] [INT] ,
              [DC2Status] [INT] ,
              [MouseFailureNotification] [INT] ,
              [KeyboardFailureNotification] [INT] ,
              [WindowsShutdownNotification] [INT] ,
              [LinkQuality] [VARCHAR](50) ,
              [IP] [VARCHAR](50) ,
              [DeviceMAC] [VARCHAR](50) ,
              [APMAC] [VARCHAR](50) ,
              [LastTransmissionStatus] [BIT] ,
              [AuthType] [INT] ,
              [ChannelNumber] [INT] ,
              [PortNumber] [INT] ,
              [DHCP] [INT] ,
              [WEPKey] [VARCHAR](50) ,
              [PassCode] [VARCHAR](100) ,
              [StaticIP] [VARCHAR](50) ,
              [SSID] [VARCHAR](50) ,
              [SparePinSwitch] [INT] ,
              [ControlBoardSerialNumber] [VARCHAR](50) ,
              [HolsterBoardSerialNumber] [VARCHAR](50) ,
              [LCDBoardSerialNumber] [VARCHAR](50) ,
              [DC1BoardSerialNumber] [VARCHAR](50) ,
              [DC2BoardSerialNumber] [VARCHAR](50) ,
              [MedBoardSerialNumber] [VARCHAR](50) ,
              [BayWirelessBoardSerialNumber] [VARCHAR](50) ,
              [Bay1BoardSerialNumber] [VARCHAR](50) ,
              [Bay2BoardSerialNumber] [VARCHAR](50) ,
              [Bay3BoardSerialNumber] [VARCHAR](50) ,
              [Bay4BoardSerialNumber] [VARCHAR](50) ,
              [MedBoardDrawerOpenTime] [INT] ,
              [MedBoardDrawerCount] [INT] ,
              [MedBoardMotorUp] [INT] ,
              [MedBoardMotorDown] [INT] ,
              [MedBoardUnlockCount] [INT] ,
              [MedBoardAdminPin] [VARCHAR](50) ,
              [MedBoardNarcPin] [VARCHAR](50) ,
              [MedBoardUserPin1] [VARCHAR](50) ,
              [MedBoardUserPin2] [VARCHAR](50) ,
              [MedBoardUserPin3] [VARCHAR](50) ,
              [MedBoardUserPin4] [VARCHAR](50) ,
              [MedBoardUserPin5] [VARCHAR](50) ,
              [GenericError] [INT] ,
              [CommandCode] [VARCHAR](20) ,
              [SourceIPAddress] [VARCHAR](20) ,
              [MeasuredVoltage] [DECIMAL](18, 2) ,
              [BatteryErrorCode] [INT] ,
              [HasError] [BIT] ,
              [ErrorCodeId] [INT] ,
              [ErrorMessage] [VARCHAR](4000) ,
              [NewRowID] [INT]
            );

	--select top 10 * from capture.dbo.querystring where processid is not null

	/* *********************************************************
	*** Get querystring data for the batch and put in temp table
	 ********************************************************* */

        INSERT  INTO @qs
                SELECT  [QueryStringID] ,
                        [QueryStringBatchID] ,
                        [CreatedDateUTC] ,
                        [SourceTimestampUTC] ,
                        [IsSessionData] ,
                        CONVERT(VARCHAR(50), [DeviceSerialNumber]) AS DeviceSerialNumber ,
                        CONVERT(VARCHAR(50), [BatterySerialNumber]) AS BatterySerialNumber ,
                        CONVERT(VARCHAR(50), [FixedBatterySerialNumber]) AS FixedBatterySerialNumber ,
                        [DeviceType] ,
                        [Activity] ,
                        CONVERT(VARCHAR(50), [Bay]) AS Bay ,
                        [FullChargeCapacity] ,
                        [Voltage] ,
                        [Amps] ,
                        [Temperature] ,
                        [ChargeLevel] ,
                        [CycleCount] ,
                        [MaxCycleCount] ,
                        [VoltageCell1] ,
                        [VoltageCell2] ,
                        [VoltageCell3] ,
                        [FETStatus] ,
                        [RemainingTime] ,
                        CONVERT(VARCHAR(50), [BatteryName]) AS BatteryName ,
                        [Efficiency] ,
                        CONVERT(VARCHAR(50), [ControlBoardRevision]) AS ControlBoardRevision ,
                        CONVERT(VARCHAR(50), [LCDRevision]) AS LCDRevision ,
                        CONVERT(VARCHAR(50), [HolsterChargerRevision]) AS HolsterChargerRevision ,
                        CONVERT(VARCHAR(50), [DCBoardOneRevision]) AS DCBoardOneRevision ,
                        CONVERT(VARCHAR(50), [DCBoardTwoRevision]) AS DCBoardTwoRevision ,
                        CONVERT(VARCHAR(50), [WifiFirmwareRevision]) AS WifiFirmwareRevision ,
                        CONVERT(VARCHAR(50), [BayWirelessRevision]) AS BayWirelessRevision ,
                        CONVERT(VARCHAR(50), [Bay1ChargerRevision]) AS Bay1ChargerRevision ,
                        CONVERT(VARCHAR(50), [Bay2ChargerRevision]) AS Bay2ChargerRevision ,
                        CONVERT(VARCHAR(50), [Bay3ChargerRevision]) AS Bay3ChargerRevision ,
                        CONVERT(VARCHAR(50), [Bay4ChargerRevision]) AS Bay4ChargerRevision ,
                        CONVERT(VARCHAR(50), [MedBoardRevision]) AS MedBoardRevision ,
                        [BackupBatteryVoltage] ,
                        [BackupBatteryStatus] ,
                        [IsAC] ,
                        [DCUnit1AVolts] ,
                        [DCUnit1ACurrent] ,
                        [DCUnit1BVolts] ,
                        [DCUnit1BCurrent] ,
                        [DCUnit2AVolts] ,
                        [DCUnit2ACurrent] ,
                        [DCUnit2BVolts] ,
                        [DCUnit2BCurrent] ,
                        [XValue] ,
                        [XMax] ,
                        [YValue] ,
                        [YMax] ,
                        [ZValue] ,
                        [ZMax] ,
                        [Move] ,
                        [BatteryStatus] ,
                        [SafetyStatus] ,
                        [PermanentFailureStatus] ,
                        [PermanentFailureAlert] ,
                        [BatteryChargeStatus] ,
                        [BatterySafetyAlert] ,
                        [BatteryOpStatus] ,
                        [BatteryMode] ,
                        [DC1Error] ,
                        [DC1Status] ,
                        [DC2Error] ,
                        [DC2Status] ,
                        [MouseFailureNotification] ,
                        [KeyboardFailureNotification] ,
                        [WindowsShutdownNotification] ,
                        CONVERT(VARCHAR(50), [LinkQuality]) AS LinkQuality ,
                        CONVERT(VARCHAR(50), [IP]) AS IP ,
                        CONVERT(VARCHAR(50), [DeviceMAC]) AS DeviceMAC ,
                        CONVERT(VARCHAR(50), [APMAC]) AS APMAC ,
                        [LastTransmissionStatus] ,
                        [AuthType] ,
                        [ChannelNumber] ,
                        [PortNumber] ,
                        [DHCP] ,
                        CONVERT(VARCHAR(50), [WEPKey]) AS WEPKey ,
                        CONVERT(VARCHAR(50), [PassCode]) AS PassCode ,
                        CONVERT(VARCHAR(50), [StaticIP]) AS StaticIP ,
                        CONVERT(VARCHAR(50), [SSID]) AS SSID ,
                        [SparePinSwitch] ,
                        CONVERT(VARCHAR(50), [ControlBoardSerialNumber]) AS ControlBoardSerialNumber ,
                        CONVERT(VARCHAR(50), [HolsterBoardSerialNumber]) AS HolsterBoardSerialNumber ,
                        CONVERT(VARCHAR(50), [LCDBoardSerialNumber]) AS LCDBoardSerialNumber ,
                        CONVERT(VARCHAR(50), [DC1BoardSerialNumber]) AS DC1BoardSerialNumber ,
                        CONVERT(VARCHAR(50), [DC2BoardSerialNumber]) AS DC2BoardSerialNumber ,
                        CONVERT(VARCHAR(50), [MedBoardSerialNumber]) AS MedBoardSerialNumber ,
                        CONVERT(VARCHAR(50), [BayWirelessBoardSerialNumber]) AS BayWirelessBoardSerialNumber ,
                        CONVERT(VARCHAR(50), [Bay1BoardSerialNumber]) AS Bay1BoardSerialNumber ,
                        CONVERT(VARCHAR(50), [Bay2BoardSerialNumber]) AS Bay2BoardSerialNumber ,
                        CONVERT(VARCHAR(50), [Bay3BoardSerialNumber]) AS Bay3BoardSerialNumber ,
                        CONVERT(VARCHAR(50), [Bay4BoardSerialNumber]) AS Bay4BoardSerialNumber ,
                        [MedBoardDrawerOpenTime] ,
                        [MedBoardDrawerCount] ,
                        [MedBoardMotorUp] ,
                        [MedBoardMotorDown] ,
                        [MedBoardUnlockCount] ,
                        CONVERT(VARCHAR(50), [MedBoardAdminPin]) AS MedBoardAdminPin ,
                        CONVERT(VARCHAR(50), [MedBoardNarcPin]) AS MedBoardNarcPin ,
                        CONVERT(VARCHAR(50), [MedBoardUserPin1]) AS MedBoardUserPin1 ,
                        CONVERT(VARCHAR(50), [MedBoardUserPin2]) AS MedBoardUserPin2 ,
                        CONVERT(VARCHAR(50), [MedBoardUserPin3]) AS MedBoardUserPin3 ,
                        CONVERT(VARCHAR(50), [MedBoardUserPin4]) AS MedBoardUserPin4 ,
                        CONVERT(VARCHAR(50), [MedBoardUserPin5]) AS MedBoardUserPin5 ,
                        [GenericError] ,
                        CONVERT(VARCHAR(50), [CommandCode]) AS CommandCode ,
                        CONVERT(VARCHAR(50), [SourceIPAddress]) AS SourceIPAddress ,
                        [MeasuredVoltage] ,
                        [BatteryErrorCode] ,
                        [HasError] ,
                        [ErrorCodeId] ,
                        CONVERT(VARCHAR(50), [ErrorMessage]) AS ErrorMessage ,
                        CONVERT(INT, 0) AS NewRowID
                FROM    Pulse.dbo.clrFnGetQuerystringDataToProcess(@GuidVar,
                                                              N'');


	/* *********************************************************
	*** Generate row ids for SessionData Rows
	 ********************************************************* */
        DECLARE @sdCount INT;
        SELECT  @sdCount = COUNT(*)
        FROM    @qs
        WHERE   IsSessionData = 1;

        DECLARE @sdTbl TABLE ( id INT );
        DECLARE @minSDrowId INT;
        UPDATE  pulse.dbo.RowIdGenerator
        SET     SessionData_RowId = SessionData_RowId + @sdCount
        OUTPUT  INSERTED.SessionData_RowId
                INTO @sdTbl
        WHERE   row_id = 1;
        SELECT  @minSDrowId = id - @sdCount
        FROM    @sdTbl;
            WITH    sd
                      AS ( SELECT   q.* ,
                                    ROW_NUMBER() OVER ( ORDER BY q.CreatedDateUTC ASC ) AS rowNum
                           FROM     @qs q
                           WHERE    q.IsSessionData = 1
                         )
            UPDATE  @qs
            SET     NewRowID = sd.rowNum + @minSDrowId
            FROM    @qs qs
                    JOIN sd ON sd.QueryStringID = qs.QueryStringID;



	/* *********************************************************
	*** Generate row ids for NonSessionData Rows
	 ********************************************************* */
        DECLARE @nsdCount INT;
        SELECT  @nsdCount = COUNT(*)
        FROM    @qs
        WHERE   IsSessionData = 0;
        DECLARE @nsdTbl TABLE ( id INT );
        DECLARE @minNSDrowId INT;
        UPDATE  pulse.dbo.RowIdGenerator
        SET     NonSessionData_RowId = NonSessionData_RowId + @sdCount
        OUTPUT  INSERTED.NonSessionData_RowId
                INTO @nsdTbl
        WHERE   row_id = 1;
        SELECT  @minNSDrowId = id - @nsdCount
        FROM    @nsdTbl;
            WITH    nsd
                      AS ( SELECT   q.* ,
                                    ROW_NUMBER() OVER ( ORDER BY q.CreatedDateUTC ASC ) AS rowNum
                           FROM     @qs q
                           WHERE    q.IsSessionData = 0
                         )
            UPDATE  @qs
            SET     NewRowID = nsd.rowNum + @minSDrowId
            FROM    @qs qs
                    JOIN nsd ON nsd.QueryStringID = qs.QueryStringID;


	/* *********************************************************
	*** Pass back our data
	 ********************************************************* */
        SELECT  *
        FROM    @qs;
    END;



GO
