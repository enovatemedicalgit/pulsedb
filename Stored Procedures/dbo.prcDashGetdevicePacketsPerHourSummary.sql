SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
	CREATE PROCEDURE [dbo].[prcDashGetdevicePacketsPerHourSummary] ( @SiteID INT = 238, @PacketHours INT = 24)
	
	AS

	SELECT
		 COUNT(ROW_ID) AS PacketCount,
		 DATEDIFF(HOUR,asd.CreatedDateUTC,GETUTCDATE()) AS PacketHour,
		 a.SiteId,
		 a.DepartmentId
	FROM 
		dbo.viewAllSessionData asd
	INNER JOIN
		dbo.Assets a
	ON 
		a.SerialNo = asd.DeviceSerialNumber
	WHERE
		asd.CreatedDateUTC > DATEADD(HOUR,-1* (@PacketHours - 1), GETUTCDATE()) 
		AND
		a.SiteID = @SiteId
	GROUP BY
		SiteId,
		a.DepartmentID,
		DATEDIFF(HOUR,asd.CreatedDateUTC,GETUTCDATE())
	ORDER BY
		DATEDIFF(HOUR,asd.CreatedDateUTC,GETUTCDATE()) DESC
		
GO
