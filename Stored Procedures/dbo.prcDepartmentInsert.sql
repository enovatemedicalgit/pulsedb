SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcDepartmentInsert]
    @Description NVARCHAR(50) ,
    @SiteID INT
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
    BEGIN TRAN;
	
    INSERT  INTO dbo.Departments
            ( Description, SiteID )
    VALUES  ( @Description, @SiteID );
    SELECT  SCOPE_IDENTITY() AS IdDepartment;
 
               
    COMMIT;



GO
