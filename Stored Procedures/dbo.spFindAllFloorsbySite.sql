SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Bill Murray
-- Create date:	7/6/2016
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spFindAllFloorsbySite] @SiteId INT
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT  *
        FROM    dbo.SiteFloors
        WHERE   SiteId = @SiteId
        ORDER BY Description;
    END;

GO
