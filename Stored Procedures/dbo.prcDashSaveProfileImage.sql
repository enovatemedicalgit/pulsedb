SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:      <ENOVATE\bill.murray,BILLMURRAY>
-- Create date: <10/4/2016> <10:47 AM>
-- Description: Update user table with uploaded profile image
--				     
--
-- 
-- =============================================
CREATE PROCEDURE [dbo].[prcDashSaveProfileImage]
    @UserID AS INT,
    @ImageData AS VARBINARY(max)
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT,
        QUOTED_IDENTIFIER,
        ANSI_NULLS,
        ANSI_PADDING,
        ANSI_WARNINGS,
        ARITHABORT,
        CONCAT_NULL_YIELDS_NULL ON;
        SET NUMERIC_ROUNDABORT OFF;
 
        DECLARE @localTran BIT;
        IF @@TRANCOUNT = 0
            BEGIN
                SET @localTran = 1;
                BEGIN TRANSACTION LocalTran;
            END;
 
        BEGIN TRY
        
 		UPDATE dbo.[USER] SET ProfileImage = @ImageData 
		WHERE IDuser = @UserID
 
            IF @localTran = 1
                AND XACT_STATE() = 1
                COMMIT TRAN LocalTran;
 
        END TRY
        BEGIN CATCH
 
            DECLARE @ErrorMessage NVARCHAR(4000);
            DECLARE @ErrorSeverity INT;
            DECLARE @ErrorState INT;
 
            SELECT  @ErrorMessage = ERROR_MESSAGE() ,
                    @ErrorSeverity = ERROR_SEVERITY() ,
                    @ErrorState = ERROR_STATE();
 
            IF @localTran = 1
                AND XACT_STATE() <> 0
                ROLLBACK TRAN;
 
            RAISERROR ( @ErrorMessage, @ErrorSeverity, @ErrorState);
            INSERT  INTO dbo.ErrorLog 
                    ( [Description] ,
                      [Source] ,
                      [CreatedDateUTC]
                    )
            VALUES  ( @ErrorMessage + ' ' + @ErrorSeverity + ' ' + @ErrorState ,
                      ' prcDashSaveProfileImage ' ,
                      GETUTCDATE()
                    );
 		
        END CATCH;
 
    END;
GO
