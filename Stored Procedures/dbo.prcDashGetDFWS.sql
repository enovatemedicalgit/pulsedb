SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:      <ENOVATE\arlow.farrell,ARLOWFARRELL>
-- Create date: <10/31/2016> <1:02 AM>
-- Description: 
--				Gets Dash Dep,Floor,Wings, descriptions, and Normalized Name     
--
-- 
-- =============================================
CREATE PROCEDURE [dbo].[prcDashGetDFWS] @SiteId AS INT = 657,@IdUser AS INT = null
AS
    BEGIN
	IF(@IdUser IS not NULL)
	BEGIN
	SET @SiteId = (SELECT TOP 1 IDSite FROM dbo.[User] AS U WHERE u.IDUser = @IdUser)
    end
        SET NOCOUNT ON;
       
	   SELECT IDDepartment AS DLFId,Description,'asd' AS StdDescription,'D' AS DFLType,DefaultForSite FROM dbo.Departments WHERE (SiteId = @SiteId OR siteid IN (SELECT siteid FROM usersites WHERE user_row_id = @IdUser))
	   UNION all
	   SELECT IdSiteWing AS DLFId,Description,'asd' AS StdDescription,'W' AS DFLType,DefaultForSite FROM  dbo.SiteWings WHERE (SiteId = @SiteId OR siteid IN (SELECT siteid FROM usersites WHERE user_row_id = @IdUser))
	   UNION all
       SELECT IdSiteFloor AS DLFId,Description,'asd' AS StdDescription,'F' AS DFLType,DefaultForSite FROM dbo.SiteFloors WHERE (SiteId = @SiteId OR siteid IN (SELECT siteid FROM usersites WHERE user_row_id = @IdUser))
		  UNION all
	SELECT IDsite AS DLFId,SiteName AS 'Description','asd' AS StdDescription,'S' AS DFLType,0 AS 'DefaultForSite' FROM  dbo.usersites AS usrs LEFT JOIN sites s ON s.IDSite = ISNULL(usrs.siteid,@SiteId)  WHERE (usrs.siteid = @SiteId OR usrs.user_row_id = @IdUser) 
	
	   
    END;


GO
