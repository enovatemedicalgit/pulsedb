SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROC [dbo].[prcDashDurationReport]
    @BeginDate DATETIME = NULL ,
    @EndDate DATETIME = NULL ,
    @SiteID INT = 693,
	@RowCount INT = 3000
AS

    DECLARE @StartDate DATETIME2 = NULL
    DECLARE @StopDate DATETIME2 = NULL

    SET NOCOUNT ON; 
    IF ( @BeginDate IS NULL )
            SET @StartDate = DATEADD(day, -8, GETUTCDATE());
    ELSE
		  SET @StartDate = @BeginDate;
	   
    IF ( @EndDate IS NULL )
            SET @StopDate = GETUTCDATE();
    ELSE
		  SET @StopDate = @EndDate

    BEGIN 
        SELECT TOP (@RowCount)
             
                sd.DeviceSerialNumber AS 'Workstation' , 
				sd.BatterySerialNumber AS Battery , 
				CONVERT(VARCHAR,CAST(CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, sd.StartDateUTC),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS DATETIME), 100) AS 'Session Start' ,
               CONVERT(VARCHAR,CAST(CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, sd.EndDateUTC),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS DATETIME), 100) AS 'Session End'  ,
               ( sd.SessionLengthMinutes / 60 ) AS Hours ,
                d.AssetNumber ,
				
                  sd.StartChargeLevel AS 'Inserted Charge Level',
                sd.EndChargeLevel  AS 'Removed Charge Level',
                sd.AvgAmpDraw ,
                sd.NumberOfMoves ,
           
   bud.Description AS Department,
                d.Wing ,
                d.Floor ,
                d.Other ,
         
                sd.PacketCount ,
             
                aio.ProductSerialNumber AS   'PC Serial',
  
                cart.ProductSerialNumber AS CartSerialNumber 
             
        FROM    dbo.Sessions (NOLOCK) sd
                LEFT JOIN dbo.Assets (NOLOCK) d ON sd.DeviceSerialNumber = d.SerialNo
                LEFT JOIN dbo.Sites (NOLOCK) bu ON d.SiteID = bu.IDSite
                LEFT JOIN dbo.AssetType (NOLOCK) dt ON sd.DeviceType = dt.IDAssetType
                LEFT JOIN dbo.Departments bud ON d.DepartmentID = bud.IDDepartment
                LEFT JOIN dbo.vwProductAIO (NOLOCK) aio ON d.SerialNo = aio.DeviceSerialNo
                LEFT JOIN dbo.vwProductCart (NOLOCK) cart ON d.SerialNo = cart.DeviceSerialNo
        WHERE   ( sd.SiteID = @SiteID )
                AND ( ( ( @StartDate BETWEEN sd.StartDateUTC AND sd.EndDateUTC )
                        OR ( @StopDate BETWEEN sd.StartDateUTC AND sd.EndDateUTC )
                      )
                      OR ( ( sd.StartDateUTC BETWEEN @StartDate AND @StopDate )
                           OR ( sd.EndDateUTC BETWEEN @StartDate AND @StopDate )
                         )
                    )
        ORDER BY sd.EndDateUTC DESC;
    END;





GO
