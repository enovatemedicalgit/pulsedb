SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcVendorsDelete] @IDVendor INT
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
    BEGIN TRAN;

    DELETE  FROM dbo.Vendors
    WHERE   [IDVendor] = @IDVendor;

    COMMIT;



GO
