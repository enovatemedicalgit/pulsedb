SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcAssetAuditLogDelete] @IDAssetAudit INT
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
    BEGIN TRAN;

    DELETE  FROM dbo.AssetAuditLog
    WHERE   [IDAssetAudit] = @IDAssetAudit;

    COMMIT;



GO
