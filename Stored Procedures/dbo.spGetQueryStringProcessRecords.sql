SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[spGetQueryStringProcessRecords]
    (
      @processId UNIQUEIDENTIFIER
    )
AS
    BEGIN
        SELECT  qs.ROW_ID ,
                qs.QueryStringBatchID ,
                qs.DeviceSerialNumber ,
                qs.Query ,
                qs.CreatedDateUTC ,
                qs.SourceIPAddress ,
                qs.SourceTimestampUTC
        FROM    dbo.QueryString qs
                LEFT JOIN dbo.PriorityIP ip ON ( LEFT(qs.SourceIPAddress, 6) = ip.SourceIPAddress_Left )
                                           OR ( LEFT(qs.SourceIPAddress, 8) = ip.SourceIPAddress_Left )
        WHERE   qs.ProcessID = @processId
        ORDER BY ISNULL(( SELECT    0
                          WHERE     ISNULL(ip.SourceIPAddress_Left, '0') = '0'
                        ), 1) DESC ,
                qs.CreatedDateUTC ASC;
    END;

GO
