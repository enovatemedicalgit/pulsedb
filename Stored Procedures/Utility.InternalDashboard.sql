SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Utility].[InternalDashboard]
AS
	BEGIN

	    /**************************************************	Unprocessed Packets By Source	    **************************************************/

	    SELECT
			 t.Source
		    , ISNULL(t2.UnProcessed, 0) AS UnProcessed
	    FROM
	    (
		   SELECT
				'qsW' AS Source
		   UNION ALL
		   SELECT
				'qsE'
					) AS t
	    LEFT JOIN
	    (
		   SELECT
				CASE
				    WHEN ISNULL(ip.SourceIPAddress_Left, '0') = '0'
				    THEN 'qsW'
				    ELSE 'qsE'
				END AS      Source
			   , COUNT(*) AS UnProcessed
		   FROM
			   dbo.QueryString AS qs WITH (nolock)
		   LEFT JOIN
			   dbo.PriorityIP AS ip WITH (nolock)
			   ON LEFT(qs.SourceIPAddress, 6) = ip.SourceIPAddress_Left
				 OR LEFT(qs.SourceIPAddress, 8) = ip.SourceIPAddress_Left
		   WHERE  NineParsed = 0
		   GROUP BY
				  CASE
					 WHEN ISNULL(ip.SourceIPAddress_Left, '0') = '0'
					 THEN 'qsW'
					 ELSE 'qsE'
				  END
					) AS t2
	    ON t.Source = t2.Source
	    UNION ALL
	    SELECT
			 j.Source
		    , ISNULL(j2.UnProcessed, 0) AS UnProcessed
	    FROM
	    (
		   SELECT
				'diW' AS Source
		   UNION ALL
		   SELECT
				'diE'
					) AS j
	    LEFT JOIN
	    (
		   SELECT
				CASE
				    WHEN ISNULL(ip.SourceIPAddress_Left, '0') = '0'
				    THEN 'diW'
				    ELSE 'diE'
				END AS      Source
			   , COUNT(*) AS UnProcessed
		   FROM
			   PULSE.dbo.DataImport AS qs WITH (nolock)
		   LEFT JOIN
			   dbo.PriorityIP AS ip WITH (nolock)
			   ON LEFT(qs.SourceIPAddress, 6) = ip.SourceIPAddress_Left
				 OR LEFT(qs.SourceIPAddress, 8) = ip.SourceIPAddress_Left
		   GROUP BY
				  CASE
					 WHEN ISNULL(ip.SourceIPAddress_Left, '0') = '0'
					 THEN 'diW'
					 ELSE 'diE'
				  END
					) AS j2
	    ON j.Source = j2.Source
	    ORDER BY
			   Source;

	    /**************************************************	Packets Processed By Minute and Second	    **************************************************/

	    --*******************************************************************
	    -- Average number of records per second in Querystring
	    --*******************************************************************
	    IF OBJECT_ID('tempdb..#qs') IS NOT NULL
		   BEGIN
			  DROP TABLE #qs;
		   END;
	    DECLARE @time DATETIME;
	    SET @time = GETUTCDATE();
	    SELECT TOP 100000
			 CONVERT( SMALLDATETIME, DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), qs.CreatedDateUTC)) AS CreatedTime
		    , qs.CreatedDateUTC
	    INTO
		    #qs
	    FROM
		    dbo.QueryString AS qs WITH (nolock)
	    ORDER BY
			   qs.ROW_ID DESC;
	    CREATE NONCLUSTERED INDEX ix_tmptbl_qs_CreatedTime ON #qs(CreatedTime);
	    CREATE NONCLUSTERED INDEX ix_tmptbl_qs_CreatedDateUTC ON #qs(CreatedDateUTC);
	    --Last 10 actual record counts per each second interval (shows spikes and flow rate of data)
	    SELECT TOP 11
			 CreatedTime
		    , COUNT(*) AS      recordsPerMin
		    , COUNT(*) / 60 AS avgRecordsPerSecond
	    FROM
		    #qs
	    GROUP BY
			   CreatedTime
	    ORDER BY
			   CreatedTime DESC;
	    --*******************************************************************
	    -- Average number of records per second in Session/NonSession Data
	    --*******************************************************************
	    IF OBJECT_ID('tempdb..#nsd') IS NOT NULL
		   BEGIN
			  DROP TABLE #nsd;
		   END;
	    SELECT TOP 50000
			 CONVERT( SMALLDATETIME, DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), nsd.InsertedDateUTC)) AS InsertedTime
		    , nsd.InsertedDateUTC
		    , nsd.QueryStringID
	    INTO
		    #nsd
	    FROM
		    NonSessionData_Summary AS nsd
	    ORDER BY
			   nsd.ROW_ID DESC;
	    INSERT INTO #nsd
	    SELECT TOP 50000
			 CONVERT( SMALLDATETIME, DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), sd.InsertedDateUTC)) AS InsertedTime
		    , sd.InsertedDateUTC
		    , sd.QueryStringID
	    FROM
		    SessionData_Summary AS sd
	    ORDER BY
			   sd.ROW_ID DESC;
	    CREATE NONCLUSTERED INDEX ix_tmptbl_nsd_insertedDateUTC ON #nsd(InsertedTime);
	    CREATE NONCLUSTERED INDEX idx_tmptbl_sd_QueryStringId ON #nsd
	    (QueryStringId
	    )
			 INCLUDE(InsertedTime, InsertedDateUTC);
	    --Last 10 actual record counts per each second interval (shows spikes and flow rate of data)
	    SELECT TOP 11
			 InsertedTime
		    , COUNT(*) AS      recordsPerMin
		    , COUNT(*) / 60 AS avgRecordsPerSecond
	    FROM
		    #nsd
	    GROUP BY
			   InsertedTime
	    ORDER BY
			   InsertedTime DESC;
	    --*******************************************************************
	    -- Average speed from insert into Querystring to insert into Session/NonSession Data
	    --*******************************************************************
	    SELECT
			 AVG(DATEPART(MILLISECOND, sd.InsertedDateUTC - qs.CreatedDateUTC)) AS                                                         avgTimeThroughSystemInMs
		    , CONVERT( DECIMAL(18, 2), AVG(DATEPART(MILLISECOND, sd.InsertedDateUTC - qs.CreatedDateUTC)) / CONVERT(DECIMAL(18, 2), 60)) AS avgTimeThroughSystemSec
	    FROM
		    #nsd AS sd
	    JOIN
		    dbo.QueryString AS qs
		    ON qs.ROW_ID = sd.QueryStringID
	    WHERE  sd.InsertedDateUTC > DATEADD(second, -60, @time);

	    /**************************************************	Active Assets by Type	    **************************************************/

	    SELECT
			 dt.Description
		    , COUNT(*) AS Count
	    FROM
		    Assets AS d
	    JOIN
		    AssetType AS dt
		    ON dt.IdAssetType = d.IDAssetType
	    WHERE  d.IsActive = 1
	    GROUP BY
			   dt.Description
	    ORDER BY
			   dt.Description;

	    /**************************************************	Communication Over Last Hour	    **************************************************/

	    SELECT
			 COUNT(DISTINCT ROW_ID) AS             PacketsProcessed
		    , COUNT(DISTINCT DeviceSerialNumber) AS DevicesCommunicating
		    , COUNT(DISTINCT SiteId) AS             SitesCommunicating
		    , COUNT(DISTINCT CustomerID) AS         CustomersCommunicating
	    FROM
	    (
		   SELECT
				*
		   FROM
			   dbo.SessionData_Summary
		   UNION ALL
		   SELECT
				*
		   FROM
			   dbo.NonSessionData_Summary
								    ) AS composite
	    INNER JOIN
		    dbo.Assets
	    ON composite.DeviceSerialNumber = assets.SerialNo
	    INNER JOIN
		    dbo.Sites
	    ON Assets.SiteId = Sites.IDSite
	    WHERE  composite.CreatedDateUTC >= DATEADD(HOUR, -1, GETUTCDATE());

	    /**************************************************	Communication Over Last 24 Hours	    **************************************************/

	    SELECT
			 COUNT(DISTINCT ROW_ID) AS             PacketsProcessed
		    , COUNT(DISTINCT DeviceSerialNumber) AS DevicesCommunicating
		    , COUNT(DISTINCT SiteId) AS             SitesCommunicating
		    , COUNT(DISTINCT CustomerID) AS         CustomersCommunicating
	    FROM
	    (
		   SELECT
				*
		   FROM
			   dbo.SessionData_Summary
		   UNION ALL
		   SELECT
				*
		   FROM
			   dbo.NonSessionData_Summary
								    ) AS composite
	    INNER JOIN
		    dbo.Assets
	    ON composite.DeviceSerialNumber = assets.SerialNo
	    INNER JOIN
		    dbo.Sites
	    ON Assets.SiteId = Sites.IDSite
	    WHERE  composite.CreatedDateUTC >= DATEADD(HOUR, -24, GETUTCDATE());

	    /**************************************************	Communication Over Last 24 By State	    **************************************************/

	    SELECT
			 COUNT(DISTINCT ROW_ID) AS             PacketsProcessed
		    , COUNT(DISTINCT DeviceSerialNumber) AS DevicesCommunicating
		    , COUNT(DISTINCT SiteId) AS             SitesCommunicating
		    , COUNT(DISTINCT CustomerID) AS         CustomersCommunicating --, Sites.State AS StateAbbrv
	    FROM
	    (
		   SELECT
				*
		   FROM
			   dbo.SessionData_Summary
		   UNION ALL
		   SELECT
				*
		   FROM
			   dbo.NonSessionData_Summary
								    ) AS composite
	    INNER JOIN
		    dbo.Assets
	    ON composite.DeviceSerialNumber = assets.SerialNo
	    INNER JOIN
		    dbo.Sites
	    ON Assets.SiteId = Sites.IDSite
	    WHERE  composite.CreatedDateUTC >= DATEADD(HOUR, -24, GETUTCDATE());

	    /**************************************************	 Users Logged in Past 24Hours	    **************************************************/

	    SELECT
			 SUM(TotalUsers) AS   TotalUsers
		    , SUM(TotalEmUsers) AS TotalEMUsers
		    , SUM(TotalSites) AS   TotalSites
	    FROM
	    (
		   SELECT
				COUNT(DISTINCT email) AS  TotalUsers
			   , 0 AS                      TotalEmUsers
			   , COUNT(DISTINCT IDSite) AS TotalSites
		   FROM
			   dbo.[User]
		   WHERE   LastLoginDateUTC > DATEADD(HOUR, -6, GETUTCDATE())
				 AND UserTypeId = 0
		   UNION
		   SELECT
				0 AS                      TotalUsers
			   , COUNT(DISTINCT email) AS  TotalEmUsers
			   , COUNT(DISTINCT IDSite) AS TotalSites
		   FROM
			   dbo.[User]
		   WHERE  LastLoginDateUTC > DATEADD(HOUR, -6, GETUTCDATE())
				AND UserTypeId = 1
							   ) AS base;

	    /**************************************************	 Users Logged in Past 24Hours	    **************************************************/

	    SELECT
			 SUM(TotalUsers) AS   TotalUsers
		    , SUM(TotalEmUsers) AS TotalEMUsers
		    , SUM(TotalSites) AS   TotalSites
	    FROM
	    (
		   SELECT
				COUNT(DISTINCT email) AS  TotalUsers
			   , 0 AS                      TotalEmUsers
			   , COUNT(DISTINCT IDSite) AS TotalSites
		   FROM
			   dbo.[User]
		   WHERE   LastLoginDateUTC > DATEADD(HOUR, -24, GETUTCDATE())
				 AND UserTypeId = 0
		   UNION
		   SELECT
				0 AS                      TotalUsers
			   , COUNT(DISTINCT email) AS  TotalEmUsers
			   , COUNT(DISTINCT IDSite) AS TotalSites
		   FROM
			   dbo.[User]
		   WHERE  LastLoginDateUTC > DATEADD(HOUR, -24, GETUTCDATE())
				AND UserTypeId = 1
							   ) AS base;

	    /**************************************************	 Users Logged in Past Month	    **************************************************/

	    SELECT
			 SUM(TotalUsers) AS   TotalUsers
		    , SUM(TotalEmUsers) AS TotalEMUsers
		    , SUM(TotalSites) AS   TotalSites
	    FROM
	    (
		   SELECT
				COUNT(DISTINCT email) AS  TotalUsers
			   , 0 AS                      TotalEmUsers
			   , COUNT(DISTINCT IDSite) AS TotalSites
		   FROM
			   dbo.[User]
		   WHERE   LastLoginDateUTC > DATEADD(MONTH, -1, GETUTCDATE())
				 AND UserTypeId = 0
		   UNION
		   SELECT
				0 AS                      TotalUsers
			   , COUNT(DISTINCT email) AS  TotalEmUsers
			   , COUNT(DISTINCT IDSite) AS TotalSites
		   FROM
			   dbo.[User]
		   WHERE  LastLoginDateUTC > DATEADD(MONTH, -1, GETUTCDATE())
				AND UserTypeId = 1
							   ) AS base;
	END;
GO
