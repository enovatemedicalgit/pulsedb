SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcAssetsUpdate]
    @SerialList VARCHAR(3000)
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
    BEGIN TRAN;
    DECLARE @SQLs VARCHAR(MAX);
    SET @SQLs = 'update [dbo].[Assets] SET   SiteID = 1125 WHERE  serialno in ('
        + @SerialList + ')';

    UPDATE  dbo.Assets
    SET     SiteID = 1125
    WHERE   SerialNo IN ( @SerialList );
	
    COMMIT;




GO
