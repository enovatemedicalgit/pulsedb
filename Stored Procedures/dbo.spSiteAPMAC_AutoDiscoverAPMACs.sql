SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Description:	Populates the table
--              SiteAPMac which is
--              used for logically resolving 
--           Site ids for a 
--              given APMAC
-- =============================================
CREATE PROCEDURE [dbo].[spSiteAPMAC_AutoDiscoverAPMACs] @IDSite AS INT = NULL
AS
    BEGIN
        SET NOCOUNT ON;
        IF OBJECT_ID(N'tempdb..#a', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #a;
            END;
        IF OBJECT_ID(N'tempdb..#DeviceAPMACDetail', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #DeviceAPMACDetail;
            END;
        IF OBJECT_ID(N'tempdb..#pbuAPMACCounts', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #pbuAPMACCounts;
            END;
        IF OBJECT_ID(N'tempdb..#buAPMACCounts', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #buAPMACCounts;
            END;

        RAISERROR ('Updated Current APs and Added Newly Scanned' , 0, 1) WITH NOWAIT;

        -- grab data from NonSessionData
        SELECT  ast.SerialNo ,
                ast.APMAC ,
                ast.SiteID ,
                bu.CustomerID
        INTO    #a
        FROM    dbo.Assets ast
                JOIN dbo.Sites bu ON ast.SiteID = bu.IDSite
        WHERE   ast.LastPostDateUTC > DATEADD(HOUR, -12, GETDATE())
                AND ast.APMAC IS NOT NULL
                AND ast.IDAssetType IN ( 1,2, 4,6 );
	
   
        SELECT  SerialNo ,
                APMAC ,
                SiteID ,
                CustomerID ,
                'Y' AS Usable -- any rows with more than one ParentBusinessUnit will be updated to 'N' (further below)
        INTO    #DeviceAPMACDetail
        FROM    #a
        WHERE   APMAC LIKE '%:%'
        ORDER BY SiteID ,
                CustomerID ,
                SerialNo ,
                APMAC;
        CREATE NONCLUSTERED INDEX IX_DeviceAPMACDetail ON #DeviceAPMACDetail( SerialNo, APMAC, SiteID, CustomerID, Usable );
        DELETE  FROM #DeviceAPMACDetail
        WHERE   CustomerID IN ( 4, 394 ); -- exclude Stinger 
        -- count distinct IPBase values at parent business unit (IDN) level
        -- this is used for determining whether devices can be moved from one business unit to another (within the same parent business unit)

	   
        SELECT  CustomerID ,
                COUNT(DISTINCT APMAC) AS APMACCount_ParentBusinessUnit
        INTO    #pbuAPMACCounts
        FROM    #DeviceAPMACDetail
        GROUP BY CustomerID
        ORDER BY CustomerID;

        -- count distinct IPBase values at business unit (facility) level
        -- this is used for determining whether devices can be moved from a given business unit
        SELECT  SiteID ,
                COUNT(DISTINCT APMAC) AS APMACCount_BusinessUnit
        INTO    #buAPMACCounts
        FROM    #DeviceAPMACDetail
        GROUP BY SiteID
        ORDER BY SiteID;


        -- if any IPBase is associated with multiple ParentBusinessUnits, do not allow it to be used for automatically assigning the business unit
        UPDATE  #DeviceAPMACDetail
        SET     [Usable] = 'N'
        WHERE   APMAC IN ( SELECT   APMAC
                           FROM     #DeviceAPMACDetail
                           GROUP BY APMAC
                           HAVING   COUNT(DISTINCT CustomerID) > 1 );
        DELETE  FROM dbo.SiteAPMAC;
        INSERT  INTO dbo.SiteAPMAC
                ( CustomerID ,
                  SiteID ,
                  APMAC ,
                  APMACTotalDeviceCount ,
                  DeviceCount ,
                  FacilityReliabilityRating ,
                  OnlyAPMACForHQ ,
                  OnlyAPMACForFacility
                )
                SELECT  a.CustomerID ,
                        a.SiteID ,
                        a.APMAC ,
                        b.APMACTotalDeviceCount ,
                        COUNT(DISTINCT a.SerialNo) AS DeviceCount ,
                        ( COUNT(DISTINCT a.SerialNo) * 100 )
                        / b.APMACTotalDeviceCount AS FacilityReliabilityRating ,
                        CASE WHEN pbu.APMACCount_ParentBusinessUnit = 1 THEN 1
                             ELSE 0
                        END AS OnlyAPMACForHQ ,
                        CASE WHEN bu.APMACCount_BusinessUnit = 1 THEN 1
                             ELSE 0
                        END AS OnlyAPMACForFacility
                FROM    #DeviceAPMACDetail a
                        JOIN ( SELECT   CustomerID ,
                                        APMAC ,
                                        COUNT(DISTINCT SerialNo) AS APMACTotalDeviceCount
                               FROM     #DeviceAPMACDetail
                               WHERE    CustomerID <> 0
                                        AND Usable = 'Y'
                               GROUP BY CustomerID ,
                                        APMAC
                             ) b ON a.CustomerID = b.CustomerID
                                    AND a.APMAC = b.APMAC
                        JOIN #pbuAPMACCounts pbu ON a.CustomerID = pbu.CustomerID
                        JOIN #buAPMACCounts bu ON a.SiteID = bu.SiteID
                WHERE   a.CustomerID <> 0
                        AND a.Usable = 'Y'
               --and a.IPBase <> '0.0.0'
                GROUP BY a.CustomerID ,
                        a.SiteID ,
                        a.APMAC ,
                        b.APMACTotalDeviceCount ,
                        CASE WHEN pbu.APMACCount_ParentBusinessUnit = 1 THEN 1
                             ELSE 0
                        END ,
                        CASE WHEN bu.APMACCount_BusinessUnit = 1 THEN 1
                             ELSE 0
                        END
                ORDER BY a.CustomerID ,
                        a.APMAC ,
                        a.SiteID;
        RAISERROR ('Updated SiteAPMac Table and FacilityReliability Rating' , 0, 1) WITH NOWAIT;

        DELETE  FROM #DeviceAPMACDetail
        WHERE   Usable = 'Y';
--select * into #tmp3 from (select * from SiteAPMAC) sapmac
        DECLARE @cntE INT;
        DECLARE @HTML1 NVARCHAR(MAX);
        DECLARE @HTML2 NVARCHAR(MAX);
		 EXEC dbo.CustomTable2HTMLv3 '#DeviceAPMACDetail', @HTML1 OUTPUT,
            'class="horizontal"', 0;

 SET @cntE = ( SELECT    COUNT(Subject) AS cntE
                      FROM      dbo.EmailQueue
                      WHERE     Subject = 'AP Conflict Report'
                    );
        IF ( @cntE < 1 )
            BEGIN
                INSERT  INTO dbo.EmailQueue
                        ( Subject ,
                          ToAddresses ,
                          Body ,
                          DateCreatedUTC ,
                          FromAddress ,
                          FromName
                        )
                VALUES  ( 'AP Conflict Report' ,
                          'arlow.farrell@enovatemedical.com' ,
                          @HTML1 ,
                          GETUTCDATE() ,
                          'Pulse@EnovateMedical.com' ,
                          'Pulse Notification'
                        );

                INSERT  INTO dbo.EmailQueue
                        ( Subject ,
                          ToAddresses ,
                          Body ,
                          DateCreatedUTC ,
                          FromAddress ,
                          FromName
                        )
                VALUES  ( 'AP Conflict Report' ,
                          'jason.batts@enovatemedical.com' ,
                          @HTML1 ,
                          GETUTCDATE() ,
                          'Pulse@EnovateMedical.com' ,
                          'Pulse Notification'
                        );
            END;
        RAISERROR ('Inserted into SiteAPMac Table' , 0, 1) WITH NOWAIT;
    END;
GO
