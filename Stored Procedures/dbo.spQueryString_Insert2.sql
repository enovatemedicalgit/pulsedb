SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[spQueryString_Insert2]
    (
      @DEVICESERIALNUMBER VARCHAR(90) ,
      @QUERY VARCHAR(5000) ,
      @NINEPARSED BIT ,
      @SOURCEIPADDRESS VARCHAR(80) ,
      @SOURCETIMESTAMPUTC VARCHAR(50)
    )
AS
    BEGIN
        SET NOCOUNT ON;

        INSERT  INTO dbo.QueryString
                ( DeviceSerialNumber ,
                  Query ,
                  NineParsed ,
                  SourceIPAddress ,
                  SourceTimestampUTC
                )
	--INSERT INTO QueryStringHolding (DeviceSerialNumber, Query, NineParsed, SourceIPAddress, SourceTimestampUTC)
        VALUES  ( @DEVICESERIALNUMBER ,
                  @QUERY ,
                  @NINEPARSED ,
                  @SOURCEIPADDRESS ,
                  @SOURCETIMESTAMPUTC
                );

    END;







GO
