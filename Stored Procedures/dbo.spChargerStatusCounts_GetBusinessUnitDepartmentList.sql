SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Arlow Farrell
-- Create date: 2012-09-12
-- Description:	Returns a list of departments
--              for the supplied BusinessUnitID
--              and shows counts of chargers that
--              are Communicating vs Offline
-- =============================================
CREATE PROCEDURE [dbo].[spChargerStatusCounts_GetBusinessUnitDepartmentList] ( @BusinessUnitID INT )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT  bud.Description AS Department ,
                SUM(CASE WHEN d.AssetStatusID IN ( 1, 2 ) THEN 1
                         ELSE 0
                    END) AS Communicating ,
                SUM(CASE WHEN d.AssetStatusID NOT IN ( 1, 2 ) THEN 1
                         ELSE 0
                    END) AS Offline ,
                SUM(CASE WHEN d.AssetStatusID IN ( 1 ) THEN 1
                         ELSE 0
                    END) AS Available ,
                SUM(CASE WHEN d.AssetStatusID IN ( 2 ) THEN 1
                         ELSE 0
                    END) AS InService
        FROM    dbo.Assets d
                JOIN dbo.AssetStatus ds ON d.AssetStatusID = ds.IDAssetStatus
                JOIN dbo.Departments bud ON d.DepartmentID = bud.IDDepartment
        WHERE   d.SiteID = @BusinessUnitID
                AND d.IDAssetType IN ( SELECT   IDAssetType
                                       FROM     dbo.AssetType
                                       WHERE    Description LIKE '%Charger%' )
                AND d.Retired = 0
        GROUP BY bud.Description;

    END;



GO
