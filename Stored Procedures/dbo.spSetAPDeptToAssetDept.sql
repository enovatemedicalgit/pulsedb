SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[spSetAPDeptToAssetDept] @IDSite INT = NULL
AS
    BEGIN

        WITH    CurrentAPDepartment
                  AS -----Current Access Points By Matching MacAddress
( SELECT    ap.ROW_ID AS APId
	--	,ass.DepartmentID AS DepartmentId
            ,
            ISNULL(ISNULL(ass.DepartmentID,
                          ( SELECT  IDDepartment
                            FROM    dbo.Departments
                            WHERE   DefaultForSite = 1
                                    AND SiteID = ass.SiteID
                          )), 0) AS DepartmentId ,
            ass.SiteID AS SiteID ,
            ass.APMAC ,
            COUNT(ass.IDAsset) AS DeviceCount
  FROM      dbo.AccessPoint ap
            LEFT JOIN dbo.Assets ass ON ap.MACAddress = ass.APMAC
			  --AND ap.DepartmentID = ass.DepartmentId
			 -- AND ap.SiteID = ass.SiteID
  WHERE     ass.LastPostDateUTC > DATEADD(DAY, -24, GETUTCDATE())
            AND ass.IDAssetType NOT IN ( 1, 3, 5, 6 )
            AND ap.Manual = 0
            AND ass.DepartmentID NOT IN ( SELECT    IDDepartment
                                          FROM      dbo.Departments
                                          WHERE     DefaultForSite = 1
                                                    AND SiteID = ass.SiteID )
            AND ass.DepartmentID IS NOT NULL
		--AND ROW_ID IS NOT NULL
  GROUP BY  ap.ROW_ID ,
            ass.DepartmentID ,
            ass.SiteID ,
            ass.APMAC
)
            UPDATE  dbo.AccessPoint		-- Update the access point table with the last reported information from assets table where not manually scanned
            SET     DepartmentID = CurrentAPDepartment.DepartmentId ,
                    SiteID = CurrentAPDepartment.SiteID ,
                    DeviceCount = CurrentAPDepartment.DeviceCount ,
                    LastPostDateUTC = GETUTCDATE() ,
                    ModifiedDateUTC = GETUTCDATE()
--Select * 
            FROM    dbo.AccessPoint ap
                    INNER JOIN CurrentAPDepartment ON ap.ROW_ID = CurrentAPDepartment.APId
                                                      AND ap.DepartmentID = CurrentAPDepartment.DepartmentId
                                                      AND ap.SiteID = CurrentAPDepartment.SiteID;



        WITH    CurrentAPFloorWing
                  AS -----Current Access Points By Matching MacAddress
( SELECT    ap.ROW_ID AS APId
	--	,ass.DepartmentID AS DepartmentId
            ,
            MAX(ISNULL(ass.Floor, '')) AS Floor ,
            MAX(ISNULL(ass.Wing, '')) AS Wing ,
            ass.SiteID AS SiteID ,
            ass.APMAC ,
            COUNT(ass.IDAsset) AS DeviceCount
  FROM      dbo.AccessPoint ap
            LEFT JOIN dbo.Assets ass ON ap.MACAddress = ass.APMAC
			  --AND ap.DepartmentID = ass.DepartmentId
			 -- AND ap.SiteID = ass.SiteID
  WHERE     ass.LastPostDateUTC > DATEADD(DAY, -24, GETUTCDATE())
            AND ass.IDAssetType NOT IN ( 1, 3, 5, 6 )
            AND ap.Manual = 0
            AND ( ass.Floor IS NOT NULL
                  OR ass.Wing IS NOT NULL
                )
		--AND ROW_ID IS NOT NULL
  GROUP BY  ap.ROW_ID ,
            ass.Floor ,
            ass.SiteID ,
            ass.APMAC
)
            UPDATE  dbo.AccessPoint		-- Update the access point table with the last reported information from assets table where not manually scanned
            SET     Floor = CurrentAPFloorWing.Floor ,
                    Wing = CurrentAPFloorWing.Wing ,
                    SiteID = CurrentAPFloorWing.SiteID ,
                    DeviceCount = CurrentAPFloorWing.DeviceCount ,
                    LastPostDateUTC = GETUTCDATE() ,
                    ModifiedDateUTC = GETUTCDATE()
--Select * 
            FROM    dbo.AccessPoint ap
                    INNER JOIN CurrentAPFloorWing ON ap.ROW_ID = CurrentAPFloorWing.APId
                                                     AND ap.Floor = CurrentAPFloorWing.Floor
                                                     AND ap.SiteID = CurrentAPFloorWing.SiteID;



        WITH    CurrentAP4FloorWing
                  AS -----Current Access Points By Matching MacAddress
( SELECT    ap.ROW_ID AS APId ,
            ass.SiteID AS SiteID ,
            MAX(ISNULL(ass.APMAC, '')) AS APMAC ,
            MAX(ISNULL(ass.Floor, '')) AS Floor ,
            MAX(ISNULL(ass.Wing, '')) AS Wing ,
            COUNT(ass.IDAsset) AS DeviceCount
  FROM      dbo.AccessPoint ap
            LEFT JOIN dbo.Assets ass ON ap.MACAddress = ass.APMAC
			  --AND ap.DepartmentID = ass.DepartmentId
			 -- AND ap.SiteID = ass.SiteID
  WHERE     ass.LastPostDateUTC > DATEADD(DAY, -24, GETUTCDATE())
            AND ass.IDAssetType NOT IN ( 5 )
            AND ap.Manual = 0
            AND ass.ManualAllocation = 1
            AND ( ass.Floor IS NOT NULL
                  OR ass.Wing IS NOT NULL
                )
		--AND ROW_ID IS NOT NULL
  GROUP BY  ap.ROW_ID ,
            ass.Floor ,
            ass.SiteID ,
            ass.APMAC
)
            UPDATE  dbo.AccessPoint		-- Update the access point table with the last reported information from assets table where not manually scanned
            SET     Floor = CurrentAP4FloorWing.Floor ,
                    Wing = CurrentAP4FloorWing.Wing ,
                    SiteID = CurrentAP4FloorWing.SiteID ,
                    DeviceCount = CurrentAP4FloorWing.DeviceCount ,
                    LastPostDateUTC = GETUTCDATE() ,
                    ModifiedDateUTC = GETUTCDATE()
--Select * 
            FROM    dbo.AccessPoint ap
                    INNER JOIN CurrentAP4FloorWing ON ap.ROW_ID = CurrentAP4FloorWing.APId
                                                      AND ap.Floor = CurrentAP4FloorWing.Floor
                                                      AND ap.Wing = CurrentAP4FloorWing.Wing
                                                      AND ap.SiteID = CurrentAP4FloorWing.SiteID;


        WITH    CurrentAP4Department
                  AS -----Current Access Points By Matching MacAddress
( SELECT    ap.ROW_ID AS APId ,
            ISNULL(ISNULL(ass.DepartmentID,
                          ( SELECT  IDDepartment
                            FROM    dbo.Departments
                            WHERE   DefaultForSite = 1
                                    AND SiteID = ass.SiteID
                          )), 0) AS DepartmentId ,
            ass.SiteID AS SiteID ,
            MAX(ISNULL(ass.APMAC, '')) AS APMAC ,
            COUNT(ass.IDAsset) AS DeviceCount
  FROM      dbo.AccessPoint ap
            LEFT JOIN dbo.Assets ass ON ap.MACAddress = ass.APMAC
			  --AND ap.DepartmentID = ass.DepartmentId
			 -- AND ap.SiteID = ass.SiteID
  WHERE     ass.LastPostDateUTC > DATEADD(DAY, -24, GETUTCDATE())
            AND ass.IDAssetType NOT IN ( 5 )
            AND ap.Manual = 0
            AND ass.ManualAllocation = 1
            AND ass.DepartmentID NOT IN ( SELECT    IDDepartment
                                          FROM      dbo.Departments
                                          WHERE     DefaultForSite = 1
                                                    AND SiteID = ass.SiteID )
            AND ass.DepartmentID IS NOT NULL
		--AND ROW_ID IS NOT NULL
  GROUP BY  ap.ROW_ID ,
            ass.DepartmentID ,
            ass.SiteID ,
            ass.APMAC
)
            UPDATE  dbo.AccessPoint		-- Update the access point table with the last reported information from assets table where not manually scanned
            SET     DepartmentID = CurrentAP4Department.DepartmentId ,
                    SiteID = CurrentAP4Department.SiteID ,
                    DeviceCount = CurrentAP4Department.DeviceCount ,
                    LastPostDateUTC = GETUTCDATE() ,
                    ModifiedDateUTC = GETUTCDATE()
--Select * 
            FROM    dbo.AccessPoint ap
                    INNER JOIN CurrentAP4Department ON ap.ROW_ID = CurrentAP4Department.APId
                                                       AND ap.DepartmentID = CurrentAP4Department.DepartmentId
                                                       AND ap.SiteID = CurrentAP4Department.SiteID;


        WITH    CurrentAP2Department
                  AS -----Current Access Points By Matching MacAddress Get Set Department
( SELECT    ap.ROW_ID AS APId
	--	,ass.DepartmentID AS DepartmentId
            ,
            ISNULL(ISNULL(ass.DepartmentID,
                          ( SELECT  IDDepartment
                            FROM    dbo.Departments
                            WHERE   DefaultForSite = 1
                                    AND SiteID = ass.SiteID
                          )), 0) AS DepartmentId ,
            ass.SiteID AS SiteID ,
            ass.APMAC ,
            COUNT(ass.IDAsset) AS DeviceCount
  FROM      dbo.AccessPoint ap
            LEFT JOIN dbo.Assets ass ON ap.MACAddress = ass.APMAC
			  --AND ap.DepartmentID = ass.DepartmentId
			 -- AND ap.SiteID = ass.SiteID
  WHERE     ass.LastPostDateUTC > DATEADD(DAY, -24, GETUTCDATE())
            AND ass.IDAssetType NOT IN ( 5 )
            AND ap.Manual = 1
            AND ap.SiteID = ass.SiteID
            AND ass.DepartmentID NOT IN ( SELECT    IDDepartment
                                          FROM      dbo.Departments
                                          WHERE     DefaultForSite = 1
                                                    AND SiteID = ass.SiteID )
            AND ass.DepartmentID IS NOT NULL
        
		--AND ROW_ID IS NOT NULL
  GROUP BY  ap.ROW_ID ,
            ass.DepartmentID ,
            ass.SiteID ,
            ass.APMAC
)
            UPDATE  dbo.AccessPoint		-- Update the manual scans, but only dep/floor/wing; No site!
            SET     DepartmentID = CurrentAP2Department.DepartmentId ,
                    DeviceCount = CurrentAP2Department.DeviceCount ,
                    LastPostDateUTC = GETUTCDATE() ,
                    ModifiedDateUTC = GETUTCDATE()
--Select * 
            FROM    dbo.AccessPoint ap
                    INNER JOIN CurrentAP2Department ON ap.ROW_ID = CurrentAP2Department.APId
                                                       AND ap.DepartmentID = CurrentAP2Department.DepartmentId
                                                       AND ap.SiteID = CurrentAP2Department.SiteID;


        WITH    CurrentAPFloorWing
                  AS -----Current Access Points By Matching MacAddress, Get and update floor/wing
( SELECT    ap.ROW_ID AS APId
	--	,ass.DepartmentID AS DepartmentId
            ,
            ass.SiteID AS SiteID ,
            ass.APMAC ,
            ISNULL(MAX(ass.Floor), MAX(ap.Floor)) AS Floor ,
            ISNULL(MAX(ass.Wing), MAX(ap.Wing)) AS Wing ,
            COUNT(ass.IDAsset) AS DeviceCount
  FROM      dbo.AccessPoint ap
            LEFT JOIN dbo.Assets ass ON ap.MACAddress = ass.APMAC
			  --AND ap.DepartmentID = ass.DepartmentId
			 -- AND ap.SiteID = ass.SiteID
  WHERE     ass.LastPostDateUTC > DATEADD(DAY, -24, GETUTCDATE())
            AND ass.IDAssetType NOT IN ( 5 )
            AND ap.Manual = 1
            AND ap.SiteID = ass.SiteID
            AND ( ass.Floor IS NOT NULL
                  OR ass.Wing IS NOT NULL
                )
        
		--AND ROW_ID IS NOT NULL
  GROUP BY  ap.ROW_ID ,
            ass.Floor ,
            ass.SiteID ,
            ass.APMAC
)
            UPDATE  dbo.AccessPoint		-- Update the manual scans, but only dep/floor/wing; No site!
            SET     Floor = CurrentAPFloorWing.Floor ,
                    Wing = CurrentAPFloorWing.Wing ,
                    DeviceCount = CurrentAPFloorWing.DeviceCount ,
                    LastPostDateUTC = GETUTCDATE() ,
                    ModifiedDateUTC = GETUTCDATE()
--Select * 
            FROM    dbo.AccessPoint ap
                    INNER JOIN CurrentAPFloorWing ON ap.ROW_ID = CurrentAPFloorWing.APId
                                                     AND ap.Floor = CurrentAPFloorWing.Floor
                                                     AND ap.SiteID = CurrentAPFloorWing.SiteID;


--- Now insert AP's that aren't in the table from assets that dont have null departments or don't have null wing/floors

        INSERT  INTO dbo.AccessPoint
                ( MACAddress ,
                  SiteID ,
                  DepartmentID ,
                  Description ,
                  DeviceCount ,
                  Wing ,
                  Floor ,
                  LastPostDateUTC ,
                  CreatedDateUTC ,
                  ModifiedDateUTC
						
                )
                SELECT  ass.APMAC AS MacAddress ,
                        ass.SiteID AS SiteID ,
                        ISNULL(ISNULL(ass.DepartmentID,
                                      ( SELECT  IDDepartment
                                        FROM    dbo.Departments
                                        WHERE   DefaultForSite = 1
                                                AND SiteID = ass.SiteID
                                      )), 0) AS DepartmentId ,
                        ISNULL(MAX(ass.Wing), '') + ' ' + ISNULL(MAX(ass.Floor),
                                                              '') AS Description ,
                        COUNT(ass.IDAsset) AS DeviceCount ,
                        MAX(ISNULL(ass.Wing, '')) ,
                        MAX(ISNULL(ass.Floor, '')) ,
                        GETUTCDATE() AS LastPostDateUTC ,
                        GETUTCDATE() AS CreatedDateUTC ,
                        GETUTCDATE() AS ModifiedDateUTC
                FROM    --	AccessPoint ap
	--RIGHT JOIN
	--	Assets ass 
	--	ON ap.MACAddress = ass.APMAC
                        dbo.Assets ass
                        LEFT JOIN dbo.Departments dept ON ass.DepartmentID = dept.IDDepartment
                WHERE   ass.LastPostDateUTC > DATEADD(HOUR, -12, GETUTCDATE())
                        AND ass.IDAssetType NOT IN ( 5 )
                        AND NOT EXISTS ( SELECT *
                                         FROM   dbo.AccessPoint ap
                                         WHERE  ap.MACAddress = ass.APMAC
                                                AND ap.DepartmentID = ass.DepartmentID
                                                AND ap.SiteID = ass.SiteID )
                        AND NOT EXISTS ( SELECT *
                                         FROM   dbo.AccessPoint ap
                                         WHERE  ap.MACAddress = ass.APMAC
                                                AND ap.Floor = ass.Floor
                                                AND ap.SiteID = ass.SiteID )
                        AND ass.SiteID IS NOT NULL
                        AND ass.SiteID NOT IN ( 4, 28, 1125 )
                        AND ass.APMAC IS NOT NULL
                        AND ass.DepartmentID <> 0
                        AND ( (ass.DepartmentID IS NOT NULL
                              AND dept.IDDepartment NOT IN (
                              SELECT    IDDepartment
                              FROM      dbo.Departments
                              WHERE     DefaultForSite = 1
                                        AND SiteID = ass.SiteID )
                              OR ( ass.Floor IS NOT NULL
                                   AND ass.Floor != ''
                                 ) )
                            )
                GROUP BY ass.APMAC ,
                        ass.SiteID ,
                        ass.DepartmentID;	
    END;


GO
