SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [Utility].[ActiveAssetsByType]
AS
BEGIN


	    /**************************************************	Active Assets by Type	    **************************************************/

	    SELECT
			 dt.Description
		    , COUNT(*) AS Count
	    FROM
		    Assets AS d
	    JOIN
		    AssetType AS dt
		    ON dt.IdAssetType = d.IDAssetType
	    WHERE  d.IsActive = 1
	    GROUP BY
			   dt.Description
	    ORDER BY
			   dt.Description;
END

GO
