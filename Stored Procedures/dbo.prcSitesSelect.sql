SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcSitesSelect]
    @IDSite INT = NULL ,
    @IPAddress VARCHAR(25) = NULL
AS
    SET NOCOUNT ON; 
   
	 BEGIN
	 IF (@IDSite IS NULL)
		BEGIN	
		RETURN NULL;	
		END	
    IF ( @IPAddress IS NOT NULL )
        BEGIN	
		
	--select s.SiteName from SiteIPSubnet sip left join sites s on s.idsite = sip.SiteID where Left(SourceIPAddressBase,11) like '%' + left(@IPAddress,10) + '%'
            SELECT TOP 1
                    s.SiteName AS SiteName ,
                    s.IDSite ,
                    s.CustomerID
            FROM    dbo.Assets ass
                    LEFT JOIN dbo.Sites s ON s.IDSite = ass.SiteID
            WHERE   LEFT(ass.SourceIPAddress, 12) LIKE '%' + LEFT(@IPAddress,
                                                              12) + '%';
        END;
    IF ( @IPAddress IS NULL )
        BEGIN
            IF ( @IDSite = 4 )
                BEGIN
                    SELECT  [IDSite] ,
                            [CustomerID] ,
                            [IPAddr] ,
                            [SiteDescription] ,
                            [SiteName]
                    FROM    dbo.Sites
                    ORDER BY SiteDescription;
                END;
            ELSE
                BEGIN 
                    SELECT  [IDSite] ,
                            [CustomerID] ,
                            [IPAddr] ,
                            [SiteDescription] ,
                            [SiteName]
                    FROM    dbo.Sites
                    WHERE   ( [IDSite] = @IDSite
                              OR @IDSite IS NULL
                              OR [CustomerID] = @IDSite
                            )
                    ORDER BY SiteDescription;
                END;
        END;
		END;


GO
