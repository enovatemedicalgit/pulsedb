SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE Procedure [dbo].[prcBatterySuperReport] --@IDSite AS INT
AS
    BEGIN
--select s.sitename,isnull(bpl.Description,'Mobius ' + Cast(bpl2.AmpHours/1000 as varchar(10)) + ' AH Battery'),
--SerialNo,LastPostDateUTC as 'Last Reported to Pulse', CreatedDateUTC as 'Manufactured Date', CycleCount, MaxCycleCount, FullChargeCapacity,
--cast(IIF(wd.DurationYears > 4, 'N/A' ,cast(wd.DurationYears as varchar(4))) as varchar(9))  as 'Warranty Years', 
--Convert(Date, dateadd(day,isnull( (select durationdays  from warrantyduration where serialnumberprefix = left(ass.SerialNo,7)),1460),cast('20' + Substring(ass.Serialno,8,2) + '-' + Substring(ass.SerialNo,10,2) + '-01' as datetime) )) as 'Expiration Date',
--ass.CycleCount,ass.MaxCycleCount,isnull(nullif([dbo].[fnGetBatteryCapacity_Int] (ass.SerialNo,ass.FullChargeCapacity),0),0) as 'Capacity Health'
-- from assets ass left join sites s on s.IDSite = ass.SiteID left join PartNumber bpl on bpl.PartNumber = LEFT(ass.SerialNo, 7) left join BatteryPartList bpl2 on bpl2.PartNumber = LEFT(ass.SerialNo, 7) left join Customers cus on ass.SiteID = cus.idcustomer  left join WarrantyDuration wd on wd.SerialNumberPrefix = left(ass.serialno,7)  
-- where  [dbo].[SerialNumberClassification](ass.serialno) = 1 and ass.siteid = @IDSite and IDAssetType =5 and Retired = 0 order by s.SiteName, [Last Reported to Pulse] desc, serialno,ass.CycleCount



        SELECT  s.SiteName ,
                ISNULL(bpl.Description,
                       'Mobius ' + CAST(bpl2.AmpHours / 1000 AS VARCHAR(10))
                       + ' AH Battery') ,
                ass.SerialNo ,
                bpl2.Generation AS Generation ,
                ass.LastPostDateUTC AS 'Last Reported to Pulse' ,
                ass.CreatedDateUTC AS 'Manufactured Date' ,
                ass.CycleCount ,
                ass.MaxCycleCount ,
                ass.FullChargeCapacity ,
                IIF(ass.LastPostDateUTC < DATEADD(MONTH, -1, GETDATE()), 'No', 'Yes') AS 'ReportedInLast30Days' ,
                CAST(IIF(wd.DurationYears > 4, 'N/A', CAST(wd.DurationYears AS VARCHAR(4))) AS VARCHAR(9)) AS 'Warranty Years' , 
--Convert(Date, dateadd(day,isnull( (select durationdays  from warrantyduration where serialnumberprefix = left(ass.SerialNo,7)),1460),cast('20' + Substring(ass.Serialno,8,2) + '-' + Substring(ass.SerialNo,10,2) + '-01' as datetime) )) as 'Expiration Date',
                ass.CycleCount ,
                ass.MaxCycleCount ,
                ISNULL(isnull(dbo.fnGetBatteryCapacity_Int(ass.SerialNo,
                                                              ass.FullChargeCapacity),
                              0), 0) AS 'Capacity Health'
        FROM    dbo.Assets ass
                LEFT JOIN dbo.Sites s ON s.IDSite = ass.SiteID
                LEFT JOIN dbo.PartNumber bpl ON bpl.PartNumber = LEFT(ass.SerialNo,
                                                              7)
                LEFT JOIN dbo.BatteryPartList bpl2 ON bpl2.PartNumber = LEFT(ass.SerialNo,
                                                              7)
                LEFT JOIN dbo.Customers cus ON ass.SiteID = cus.IDCustomer
                LEFT JOIN dbo.WarrantyDuration wd ON wd.SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
        WHERE   dbo.SerialNumberClassification(ass.SerialNo) = 1
                AND ass.IDAssetType = 5
                AND ass.SiteID NOT IN ( 1, 0, 4, 28, 17 )
                AND ass.Retired = 0 AND ISNULL(NULLIF(dbo.fnGetBatteryCapacity_Int(ass.SerialNo,
                                                                              ass.FullChargeCapacity),
                                              0), 0) >= 60 AND ISNULL(NULLIF(dbo.fnGetBatteryCapacity_Int(ass.SerialNo,
                                                                                                            ass.FullChargeCapacity),
                                                                            0), 0) <= 80
              --  AND s.IDSite = @IDSite
        ORDER BY s.SiteName ,
                [Last Reported to Pulse] DESC ,
                ass.SerialNo ,
                ass.CycleCount;


    END;




GO
