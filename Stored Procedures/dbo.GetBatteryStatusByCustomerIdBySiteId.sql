SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*<HeaderBlock>*/
CREATE PROCEDURE [dbo].[GetBatteryStatusByCustomerIdBySiteId]
    (
      @IdCustomer INT = NULL ,
      @IdSite INT = 693
    )
AS /*</HeaderBlock>*/


/*<TestScriptHeader>
DECLARE @IdCustomer  Int
DECLARE @IdSite  Int
SET @IdCustomer = 67 --  = 67
SET @IdSite = NULL -- =  68
</TestScriptHeader>*/

    BEGIN
        DECLARE @SelectedSites TABLE ( SiteId INT NOT NULL );
	
	/*
			 Comment out the first IF statement to force a user to choose one 
			 parameter.  Uncomment it if you want the report to pull back every
			 site initially
	*/
	
	--IF @IdSite IS NULL AND @IdCustomer IS NULL	
	--	BEGIN
	--	INSERT INTO @SelectedSites Select IdSite from dbo.Sites where IsInternal = 0 OR IsInternal IS NULL 
	--	END

        IF @IdSite IS NULL
            AND @IdCustomer IS NOT NULL
            BEGIN
                INSERT  INTO @SelectedSites
                        SELECT  IDSite
                        FROM    dbo.Sites
                        WHERE   CustomerID = @IdCustomer;
            END;
        IF @IdSite IS NOT NULL
            BEGIN
                INSERT  INTO @SelectedSites
                VALUES  ( @IdSite );
            END;
        SELECT  base.SiteName ,
                base.Description ,
                base.SerialNo ,
                base.Generation ,
                base.LastReportedToPulse ,
                base.DaysSinceLastReport ,
                base.ManufacturedDate ,
                base.CycleCount ,
                base.MaxCycleCount ,
                base.FullChargeCapacity ,
                base.RecentlyReportedToPulse ,
                base.WarrantyYears ,
                base.ExpirationDate ,
                base.CapacityHealth ,
                CASE WHEN base.CapacityHealth > 85 THEN 'Over 85'
                     WHEN base.CapacityHealth BETWEEN 70 AND 85 THEN '70 to 85'
                     WHEN base.CapacityHealth < 70 THEN 'Under 70'
                     ELSE 'Not Available'
                END AS BatteryHealth ,
                CASE WHEN base.CapacityHealth > 85 THEN 1
                     WHEN base.CapacityHealth BETWEEN 70 AND 85 THEN 2
                     WHEN base.CapacityHealth < 70 THEN 3
                     ELSE 4
                END AS BatteryHealthOrdinal
        FROM    ( SELECT    s.SiteName ,
                            ISNULL(bpl.Description,
                                   'Mobius '
                                   + CAST(bpl2.AmpHours / 1000 AS VARCHAR(10))
                                   + ' AH Battery') AS Description ,
                            a.SerialNo ,
                            bpl2.Generation AS Generation ,
                            a.LastPostDateUTC AS LastReportedToPulse ,
                            DATEDIFF(d, a.LastPostDateUTC, GETDATE()) AS DaysSinceLastReport ,
                            CAST(a.CreatedDateUTC AS DATE) AS ManufacturedDate ,
                            a.CycleCount AS CycleCount ,
                            a.MaxCycleCount AS MaxCycleCount ,
                            a.FullChargeCapacity AS FullChargeCapacity ,
                            IIF(a.LastPostDateUTC < DATEADD(MONTH, -3, GETDATE()), 'No', 'Yes') AS RecentlyReportedToPulse ,
                            CAST(IIF(wd.DurationYears > 4, 'N/A', CAST(wd.DurationYears AS VARCHAR(4))) AS VARCHAR(9)) AS WarrantyYears ,
                            CAST(CONVERT(DATE, DATEADD(DAY,
                                                       ISNULL(( SELECT
                                                              DurationDays
                                                              FROM
                                                              dbo.WarrantyDuration
                                                              WHERE
                                                              SerialNumberPrefix = LEFT(a.SerialNo,
                                                              7)
                                                              ), 1460),
                                                       CAST('20'
                                                       + SUBSTRING(a.SerialNo,
                                                              8, 2) + '-'
                                                       + SUBSTRING(a.SerialNo,
                                                              10, 2) + '-01' AS DATETIME))) AS DATE) AS ExpirationDate ,
                            ISNULL(NULLIF(dbo.fnGetBatteryCapacity_Int(a.SerialNo,
                                                              a.FullChargeCapacity),
                                          0), 0) AS CapacityHealth
                  FROM      dbo.Assets a
                            LEFT JOIN dbo.Sites s ON s.IDSite = a.SiteID
                            LEFT JOIN dbo.PartNumber bpl ON bpl.PartNumber = LEFT(a.SerialNo,
                                                              7)
                            LEFT JOIN dbo.BatteryPartList bpl2 ON bpl2.PartNumber = LEFT(a.SerialNo,
                                                              7)
                            LEFT JOIN dbo.Customers cus ON a.SiteID = cus.IDCustomer
                            LEFT JOIN dbo.WarrantyDuration wd ON wd.SerialNumberPrefix = LEFT(a.SerialNo,
                                                              7)
                            INNER JOIN @SelectedSites ss ON s.IDSite = ss.SiteId
                  WHERE     dbo.SerialNumberClassification(a.SerialNo) = 1
                            AND a.IDAssetType = 5
                            AND a.Retired = 0
                ) base
        ORDER BY base.SiteName ASC ,
                base.Generation DESC ,
                base.CapacityHealth DESC;
    END;



GO
