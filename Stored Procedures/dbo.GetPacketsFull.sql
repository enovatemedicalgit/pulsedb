SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[GetPacketsFull]
    @SiteID VARCHAR(20) = NULL ,
    @AssetSerialNumber VARCHAR(20) = NULL ,
    @RowLimit INT = 1000 ,
    @StartDate DATETIME = NULL ,
    @EndDate DATETIME = NULL ,
    @CustomerID VARCHAR(10) = NULL
    WITH RECOMPILE
AS
    BEGIN

        IF ( @RowLimit IS NULL )
            BEGIN
                SET @RowLimit = 400;
            END;
        DECLARE @sdate DATETIME = @StartDate;
        DECLARE @edate DATETIME = @EndDate;
        DECLARE @sCust INT = @CustomerID;
        DECLARE @sSite INT = @SiteID;
        IF ( @StartDate IS NULL )
            BEGIN
                SET @StartDate = DATEADD(DAY, -7, GETDATE());
                SET @EndDate = GETDATE();
            END;

        SET @sdate = @StartDate;
        SET @edate = @EndDate;


        IF ( @sCust IS NOT NULL
             AND LEN(@AssetSerialNumber) < 7
           )
            BEGIN
                SELECT TOP ( @RowLimit )
                        dt.Description AS AssetTypeDescription ,
                        sd.CreatedDateUTC ,
                        bu.SiteDescription ,
                        cust.CustomerName AS IDN ,
                        sd.DeviceSerialNumber ,
                        sd.BatterySerialNumber ,
                        a.ActivityDescription ,
                        ap.Description AS AccessPointDescription ,
                        d.AssetNumber ,
                        d.Floor ,
                        d.Wing ,
                        d.Other ,
                        sd.Bay ,
                        sd.FullChargeCapacity ,
                        sd.Voltage ,
                        sd.Amps ,
                        sd.Temperature ,
                        sd.ChargeLevel ,
                        sd.CycleCount ,
                        sd.MaxCycleCount ,
                        sd.VoltageCell1 ,
                        sd.VoltageCell2 ,
                        sd.VoltageCell3 ,
                        sd.FETStatus ,
                        sd.RemainingTime ,
                        sd.BatteryName ,
                        sd.Efficiency ,
                        sd.ControlBoardRevision ,
                        sd.LCDRevision ,
                        sd.HolsterChargerRevision ,
                        sd.DCBoardOneRevision ,
                        sd.DCBoardTwoRevision ,
                        sd.WifiFirmwareRevision ,
                        sd.BayWirelessRevision ,
                        sd.Bay1ChargerRevision ,
                        sd.Bay2ChargerRevision ,
                        sd.Bay3ChargerRevision ,
                        sd.Bay4ChargerRevision ,
                        sd.BackupBatteryVoltage ,
                        bub.BUBStatusDescription ,
                        sd.IsAC ,
                        sd.DCUnit1AVolts ,
                        sd.DCUnit1ACurrent ,
                        sd.DCUnit1BVolts ,
                        sd.DCUnit1BCurrent ,
                        sd.DCUnit2AVolts ,
                        sd.DCUnit2ACurrent ,
                        sd.DCUnit2BVolts ,
                        sd.DCUnit2BCurrent ,
                        sd.Move ,
                        sd.BatteryStatus ,
                        sd.SafetyStatus ,
                        sd.PermanentFailureStatus ,
                        sd.PermanentFailureAlert ,
                        sd.BatteryChargeStatus ,
                        sd.BatterySafetyAlert ,
                        sd.BatteryOpStatus ,
                        sd.BatteryMode ,
                        sd.DC1Error ,
                        sd.DC1Status ,
                        sd.DC2Error ,
                        sd.DC2Status ,
                        sd.MouseFailureNotification ,
                        sd.KeyboardFailureNotification ,
                        sd.WindowsShutdownNotification ,
                        sd.LinkQuality ,
                        sd.IP ,
                        sd.DeviceMAC ,
                        sd.APMAC ,
                        sd.ControlBoardSerialNumber ,
                        sd.HolsterBoardSerialNumber ,
                        sd.LCDBoardSerialNumber ,
                        sd.DC1BoardSerialNumber ,
                        sd.DC2BoardSerialNumber ,
                        sd.BayWirelessBoardSerialNumber ,
                        sd.Bay1BoardSerialNumber ,
                        sd.Bay2BoardSerialNumber ,
                        sd.Bay3BoardSerialNumber ,
                        sd.Bay4BoardSerialNumber ,
                        sd.CommandCode ,
                        bub.BUBStatusDescription ,
                        cart.ProductSerialNumber AS CartSerialNumber ,
                        bud.Description AS Department ,
                        sd.SourceIPAddress ,
                        ISNULL(sd.MeasuredVoltage, 0) AS MeasuredVoltage ,
                        ISNULL(sd.BatteryErrorCode, 0) AS BatteryErrorCode
                FROM    dbo.viewAllSessionData (NOLOCK) sd
                        JOIN dbo.Assets (NOLOCK) d ON sd.DeviceSerialNumber = d.SerialNo
                        LEFT JOIN dbo.Departments bud ON d.DepartmentID = bud.IDDepartment
                        LEFT JOIN dbo.Sites (NOLOCK) bu ON d.SiteID = bu.IDSite
                        LEFT JOIN dbo.AssetType (NOLOCK) dt ON sd.DeviceType = dt.IDAssetType
                        LEFT JOIN dbo.vwActivity (NOLOCK) a ON sd.Activity = a.ActivityCode
                        LEFT JOIN dbo.AccessPoint (NOLOCK) ap ON sd.APMAC = ap.MACAddress
                        LEFT JOIN dbo.BUBStatus (NOLOCK) bub ON d.IDAssetType = bub.AssetTypeID
                                                            AND sd.BackupBatteryStatus = bub.BUBStatusCode
                                                            AND sd.ControlBoardRevision BETWEEN bub.RevisionLow
                                                              AND
                                                              bub.RevisionHigh
                        LEFT JOIN dbo.vwProductAIO (NOLOCK) aio ON d.SerialNo = aio.DeviceSerialNo
                        LEFT JOIN dbo.vwProductDCMonitor (NOLOCK) dcmonitor ON d.SerialNo = dcmonitor.DeviceSerialNo
                        LEFT JOIN dbo.vwProductCart (NOLOCK) cart ON d.SerialNo = cart.DeviceSerialNo
                
                        LEFT JOIN dbo.Sites buAccessPoint ON ap.SiteID = buAccessPoint.IDSite
                        LEFT JOIN dbo.Customers cust ON bu.CustomerID = cust.IDCustomer
                WHERE   bu.CustomerID = @sCust; -- by CreatedDateUTC desc -- OPTION (OPTIMIZE FOR UNKNOWN)-- 
            END;
        IF ( @AssetSerialNumber IS NULL
             OR LEN(@AssetSerialNumber) < 2
           )
            BEGIN
                SELECT TOP ( @RowLimit )
                        dt.Description AS AssetTypeDescription ,
                        sd.CreatedDateUTC ,
                        bu.SiteDescription ,
                        cust.CustomerName AS IDN ,
                        sd.DeviceSerialNumber ,
                        sd.BatterySerialNumber ,
                        a.ActivityDescription ,
                        ap.Description AS AccessPointDescription ,
                        d.AssetNumber ,
                        d.Floor ,
                        d.Wing ,
                        d.Other ,
                        sd.Bay ,
                        sd.FullChargeCapacity ,
                        sd.Voltage ,
                        sd.Amps ,
                        sd.Temperature ,
                        sd.ChargeLevel ,
                        sd.CycleCount ,
                        sd.MaxCycleCount ,
                        sd.VoltageCell1 ,
                        sd.VoltageCell2 ,
                        sd.VoltageCell3 ,
                        sd.FETStatus ,
                        sd.RemainingTime ,
                        sd.BatteryName ,
                        sd.Efficiency ,
                        sd.ControlBoardRevision ,
                        sd.LCDRevision ,
                        sd.HolsterChargerRevision ,
                        sd.DCBoardOneRevision ,
                        sd.DCBoardTwoRevision ,
                        sd.WifiFirmwareRevision ,
                        sd.BayWirelessRevision ,
                        sd.Bay1ChargerRevision ,
                        sd.Bay2ChargerRevision ,
                        sd.Bay3ChargerRevision ,
                        sd.Bay4ChargerRevision ,
                        sd.BackupBatteryVoltage ,
                        bub.BUBStatusDescription ,
                        sd.IsAC ,
                        sd.DCUnit1AVolts ,
                        sd.DCUnit1ACurrent ,
                        sd.DCUnit1BVolts ,
                        sd.DCUnit1BCurrent ,
                        sd.DCUnit2AVolts ,
                        sd.DCUnit2ACurrent ,
                        sd.DCUnit2BVolts ,
                        sd.DCUnit2BCurrent ,
                        sd.Move ,
                        sd.BatteryStatus ,
                        sd.SafetyStatus ,
                        sd.PermanentFailureStatus ,
                        sd.PermanentFailureAlert ,
                        sd.BatteryChargeStatus ,
                        sd.BatterySafetyAlert ,
                        sd.BatteryOpStatus ,
                        sd.BatteryMode ,
                        sd.DC1Error ,
                        sd.DC1Status ,
                        sd.DC2Error ,
                        sd.DC2Status ,
                        sd.MouseFailureNotification ,
                        sd.KeyboardFailureNotification ,
                        sd.WindowsShutdownNotification ,
                        sd.LinkQuality ,
                        sd.IP ,
                        sd.DeviceMAC ,
                        sd.APMAC ,
                        sd.ControlBoardSerialNumber ,
                        sd.HolsterBoardSerialNumber ,
                        sd.LCDBoardSerialNumber ,
                        sd.DC1BoardSerialNumber ,
                        sd.DC2BoardSerialNumber ,
                        sd.BayWirelessBoardSerialNumber ,
                        sd.Bay1BoardSerialNumber ,
                        sd.Bay2BoardSerialNumber ,
                        sd.Bay3BoardSerialNumber ,
                        sd.Bay4BoardSerialNumber ,
                        sd.CommandCode ,
                        bub.BUBStatusDescription ,
                        cart.ProductSerialNumber AS CartSerialNumber ,
                        bud.Description AS Department ,
                        sd.SourceIPAddress ,
                        ISNULL(sd.MeasuredVoltage, 0) AS MeasuredVoltage ,
                        ISNULL(sd.BatteryErrorCode, 0) AS BatteryErrorCode
                FROM    dbo.viewAllSessionData (NOLOCK) sd
                        JOIN dbo.Assets (NOLOCK) d ON sd.DeviceSerialNumber = d.SerialNo
                        LEFT JOIN dbo.Departments bud ON d.DepartmentID = bud.IDDepartment
                        LEFT JOIN dbo.Sites (NOLOCK) bu ON d.SiteID = bu.IDSite
                        LEFT JOIN dbo.AssetType (NOLOCK) dt ON sd.DeviceType = dt.IDAssetType
                        LEFT JOIN dbo.vwActivity (NOLOCK) a ON sd.Activity = a.ActivityCode
                        LEFT JOIN dbo.AccessPoint (NOLOCK) ap ON sd.APMAC = ap.MACAddress
                        LEFT JOIN dbo.BUBStatus (NOLOCK) bub ON d.IDAssetType = bub.AssetTypeID
                                                            AND sd.BackupBatteryStatus = bub.BUBStatusCode
                                                            AND sd.ControlBoardRevision BETWEEN bub.RevisionLow
                                                              AND
                                                              bub.RevisionHigh
                        LEFT JOIN dbo.vwProductAIO (NOLOCK) aio ON d.SerialNo = aio.DeviceSerialNo
                        LEFT JOIN dbo.vwProductDCMonitor (NOLOCK) dcmonitor ON d.SerialNo = dcmonitor.DeviceSerialNo
                        LEFT JOIN dbo.vwProductCart (NOLOCK) cart ON d.SerialNo = cart.DeviceSerialNo
                      
                        LEFT JOIN dbo.Sites buAccessPoint ON ap.SiteID = buAccessPoint.IDSite
                        LEFT JOIN dbo.Customers cust ON bu.CustomerID = cust.IDCustomer
                WHERE   d.SiteID = @sSite; --order by CreatedDateUTC desc -- OPTION (OPTIMIZE FOR UNKNOWN)-- 
            END;
        ELSE
            BEGIN
                SELECT TOP ( @RowLimit )
                        dt.Description AS AssetTypeDescription ,
                        sd.CreatedDateUTC ,
                        bu.SiteDescription ,
                        cust.CustomerName AS IDN ,
                        sd.DeviceSerialNumber ,
                        sd.BatterySerialNumber ,
                        a.ActivityDescription ,
                        ap.Description AS AccessPointDescription ,
                        d.AssetNumber ,
                        d.Floor ,
                        d.Wing ,
                        d.Other ,
                        sd.Bay ,
                        sd.FullChargeCapacity ,
                        sd.Voltage ,
                        sd.Amps ,
                        sd.Temperature ,
                        sd.ChargeLevel ,
                        sd.CycleCount ,
                        sd.MaxCycleCount ,
                        sd.VoltageCell1 ,
                        sd.VoltageCell2 ,
                        sd.VoltageCell3 ,
                        sd.FETStatus ,
                        sd.RemainingTime ,
                        sd.BatteryName ,
                        sd.Efficiency ,
                        sd.ControlBoardRevision ,
                        sd.LCDRevision ,
                        sd.HolsterChargerRevision ,
                        sd.DCBoardOneRevision ,
                        sd.DCBoardTwoRevision ,
                        sd.WifiFirmwareRevision ,
                        sd.BayWirelessRevision ,
                        sd.Bay1ChargerRevision ,
                        sd.Bay2ChargerRevision ,
                        sd.Bay3ChargerRevision ,
                        sd.Bay4ChargerRevision ,
                        sd.BackupBatteryVoltage ,
                        bub.BUBStatusDescription ,
                        sd.IsAC ,
                        sd.DCUnit1AVolts ,
                        sd.DCUnit1ACurrent ,
                        sd.DCUnit1BVolts ,
                        sd.DCUnit1BCurrent ,
                        sd.DCUnit2AVolts ,
                        sd.DCUnit2ACurrent ,
                        sd.DCUnit2BVolts ,
                        sd.DCUnit2BCurrent ,
                        sd.Move ,
                        sd.BatteryStatus ,
                        sd.SafetyStatus ,
                        sd.PermanentFailureStatus ,
                        sd.PermanentFailureAlert ,
                        sd.BatteryChargeStatus ,
                        sd.BatterySafetyAlert ,
                        sd.BatteryOpStatus ,
                        sd.BatteryMode ,
                        sd.DC1Error ,
                        sd.DC1Status ,
                        sd.DC2Error ,
                        sd.DC2Status ,
                        sd.MouseFailureNotification ,
                        sd.KeyboardFailureNotification ,
                        sd.WindowsShutdownNotification ,
                        sd.LinkQuality ,
                        sd.IP ,
                        sd.DeviceMAC ,
                        sd.APMAC ,
                        sd.ControlBoardSerialNumber ,
                        sd.HolsterBoardSerialNumber ,
                        sd.LCDBoardSerialNumber ,
                        sd.DC1BoardSerialNumber ,
                        sd.DC2BoardSerialNumber ,
                        sd.BayWirelessBoardSerialNumber ,
                        sd.Bay1BoardSerialNumber ,
                        sd.Bay2BoardSerialNumber ,
                        sd.Bay3BoardSerialNumber ,
                        sd.Bay4BoardSerialNumber ,
                        sd.CommandCode ,
                        bub.BUBStatusDescription ,
                        cart.ProductSerialNumber AS CartSerialNumber ,
                        bud.Description AS Department ,
                        sd.SourceIPAddress ,
                        ISNULL(sd.MeasuredVoltage, 0) AS MeasuredVoltage ,
                        ISNULL(sd.BatteryErrorCode, 0) AS BatteryErrorCode
                FROM    dbo.viewAllSessionData sd
                        JOIN dbo.Assets (NOLOCK) d ON sd.DeviceSerialNumber = d.SerialNo
                        LEFT JOIN dbo.Departments bud ON d.DepartmentID = bud.IDDepartment
                        LEFT JOIN dbo.Sites (NOLOCK) bu ON d.SiteID = bu.IDSite
                        LEFT JOIN dbo.AssetType (NOLOCK) dt ON sd.DeviceType = dt.IDAssetType
                        LEFT JOIN dbo.vwActivity (NOLOCK) a ON sd.Activity = a.ActivityCode
                        LEFT JOIN dbo.AccessPoint (NOLOCK) ap ON sd.APMAC = ap.MACAddress
                        LEFT JOIN dbo.BUBStatus (NOLOCK) bub ON d.IDAssetType = bub.AssetTypeID
                                                            AND sd.BackupBatteryStatus = bub.BUBStatusCode
                                                            AND sd.ControlBoardRevision BETWEEN bub.RevisionLow
                                                              AND
                                                              bub.RevisionHigh
                        LEFT JOIN dbo.vwProductAIO (NOLOCK) aio ON d.SerialNo = aio.DeviceSerialNo
                        LEFT JOIN dbo.vwProductDCMonitor (NOLOCK) dcmonitor ON d.SerialNo = dcmonitor.DeviceSerialNo
                        LEFT JOIN dbo.vwProductCart (NOLOCK) cart ON d.SerialNo = cart.DeviceSerialNo
                      
                        LEFT JOIN dbo.Sites buAccessPoint ON ap.SiteID = buAccessPoint.IDSite
                        LEFT JOIN dbo.Customers cust ON bu.CustomerID = cust.IDCustomer
                WHERE   ( sd.BatterySerialNumber = @AssetSerialNumber
                          OR sd.DeviceSerialNumber = @AssetSerialNumber
                        )
                        AND sd.CreatedDateUTC BETWEEN @sdate AND @edate; -- OPTION (OPTIMIZE FOR UNKNOWN)--order by CreatedDateUTC desc -- order by CreatedDateUTC desc
            END;

    END;

GO
