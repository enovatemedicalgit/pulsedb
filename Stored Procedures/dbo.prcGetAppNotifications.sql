SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================

-- =============================================
CREATE Procedure [dbo].[prcGetAppNotifications]
    @SSID VARCHAR(40) = NULL ,
    @Username VARCHAR(40) = NULL ,
    @SiteID VARCHAR(4) = NULL ,
    @iDelete INT = 0
AS
    BEGIN
        IF ( @Username IS NOT NULL )
            BEGIN
                DECLARE @uCount INT;
                DECLARE @UserID INT;
                SET @UserID = ( SELECT  IDUser
                                FROM    dbo.[User]
                                WHERE   UserName = @Username
                              );
                SET @uCount = ( SELECT  COUNT(UserName)
                                FROM    dbo.[User]
                                WHERE   UserName = @Username
                              ); --AND LastLoginDateUTC < DATEADD(HOUR,-12,GETUTCDATE()))
                IF ( @uCount > 0
                     AND @iDelete = 0
                   )
                    BEGIN	
                        SELECT  *
                        FROM    dbo.AppNotifications
                        WHERE   CreatedDateUTC > DATEADD(HOUR, -24,
                                                         GETUTCDATE())
                                AND ( SingleUserID IS NULL
                                      OR SingleUserID = @UserID
                                    );
                    END;
                ELSE
                    BEGIN
                        DELETE  FROM dbo.AppNotifications
                        WHERE   CreatedDateUTC > DATEADD(HOUR, -24,
                                                         GETUTCDATE())
                                AND SingleUserID = @UserID;
                    END;
            END;
    END;


GO
