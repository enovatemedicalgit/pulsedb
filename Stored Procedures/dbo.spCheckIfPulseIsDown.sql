SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[spCheckIfPulseIsDown]
AS
    BEGIN
        DECLARE @FromAddress VARCHAR(150);
        DECLARE @FromName VARCHAR(150);
        DECLARE @LastPost AS DATETIME = NULL;
        SELECT TOP 1
                @FromAddress = a.DefaultFromAddress ,
                @FromName = b.DefaultFromName
        FROM    ( SELECT TOP 1
                            1 AS joinkey ,
                            SettingValue AS DefaultFromAddress
                  FROM      dbo.SystemSettings
                  WHERE     SettingKey = 'EMAIL_NOTIFICATIONS_DEFAULT_FROM_ADDRESS'
                ) a
                JOIN ( SELECT TOP 1
                                1 AS joinkey ,
                                SettingValue AS DefaultFromName
                       FROM     dbo.SystemSettings
                       WHERE    SettingKey = 'EMAIL_NOTIFICATIONS_DEFAULT_FROM_NAME'
                     ) b ON a.joinkey = b.joinkey;
        SET @LastPost = ( SELECT    MAX(LastPostDateUTC)
                          FROM      dbo.Assets WITH (NOLOCK)
                          WHERE     LastPostDateUTC IS NOT NULL
                        );
						DECLARE @rcnt AS VARCHAR(10);
	--		SET @rcnt = (SELECT CAST(COUNT(Row_ID) AS varchar(10)) FROM dbo.QueryString)  ;
		--	SET @rcnt = 'Pulse is ' + @rcnt  + ' Behind';
			SET @rcnt = 'Last Packet Processed at ' + CAST((SELECT TOP 1 CAST(CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, QS.CreatedDateUTC),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS NVARCHAR(50)) FROM dbo.QueryString AS QS WHERE QS.ProcessID IS NULL ) AS NVARCHAR(50))
			SET @rcnt = ISNULL(@rcnt,' ' );
        IF ( @LastPost < DATEADD(MINUTE, -60, GETUTCDATE()) )
            BEGIN
			
                EXEC dbo.usp_SendTextEmail 'enovatemedical-com.mail.protection.outlook.com',
                    'Pulse@EnovateMedical.com',
                    'arlow.farrell@enovatemedical.com;bill.murray@enovatemedical.com;jason.batts@enovatemedical.com;6159274414@vtext.com',
                    'Pulse Is Behind', @rcnt, '', '', 0;
			 EXEC dbo.sp_Kill @DBNAME = 'pulse'
            END
			       IF ( @LastPost < DATEADD(MINUTE, -210, GETUTCDATE()) )
            BEGIN
                EXEC dbo.usp_SendTextEmail 'enovatemedical-com.mail.protection.outlook.com',
                    'Pulse@EnovateMedical.com',
                    'arlow.farrell@enovatemedical.com;bill.murray@enovatemedical.com;jasob.batts@enovatemedical.com;6159274414@vtext.com',
                    'Pulse Is Behind', @rcnt, '', '', 0;
		 EXEC dbo.sp_Kill @DBNAME = 'pulse'
            END
    END;
GO
