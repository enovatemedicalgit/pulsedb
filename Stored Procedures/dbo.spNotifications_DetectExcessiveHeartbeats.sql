SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Arlow Farrell
-- Create date: 2012-05-22
-- Description:	Creates notifications for any
--              devices transmitting more than
--              one heart beat packet every
--              50 minutes.
--              
--              Only creates the notification
--              if/when an unresolved one 
--              doesn't already exist.
-- =============================================
CREATE PROCEDURE [dbo].[spNotifications_DetectExcessiveHeartbeats]
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

	/*
	-----------------------------------------------
	Find devices currently at Stinger

	Make notifications for these so they can
	be found during the QA process.

	-----------------------------------------------
	*/
        DECLARE @Priority INT;
        SET @Priority = 911;
        DECLARE @AlertConditionID INT;
        SET @AlertConditionID = 44;
        DECLARE @AlertConditionDescription VARCHAR(200);
        SET @AlertConditionDescription = ( SELECT TOP 1
                                                    [Description]
                                           FROM     dbo.AlertConditions
                                           WHERE    ROW_ID = @AlertConditionID
                                         );

        IF OBJECT_ID(N'tempdb..#a', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #a;
            END;

        SELECT DISTINCT
                DeviceSerialNumber ,
                MAX(CreatedDateUTC) AS CreatedDateUTC ,
                MAX(ROW_ID) AS ROW_ID --, getutcdate() as DetectionDateUTC
        INTO    #a
        FROM    NonSessionDataCurrent
        WHERE   CreatedDateUTC > DATEADD(MINUTE, -50, GETUTCDATE())
                AND Activity = 3
                AND DeviceSerialNumber <> ''
	--and DeviceSerialNumber not like 'NYS_%'
                AND SourceIPAddress LIKE '10.51.[0-1].%'
        GROUP BY DeviceSerialNumber
        HAVING  COUNT(DeviceSerialNumber) > 1;

        INSERT  INTO dbo.AlertMessage
                ( CreatedDateUTC ,
                  DeviceSerialNumber ,
                  BatterySerialNumber ,
                  [Description] ,
                  SiteID ,
                  AlertConditionID ,
                  AlertStatus ,
                  [Priority] ,
                  TablePointer ,
                  RowPointer
                )
                SELECT  a.CreatedDateUTC ,
                        a.DeviceSerialNumber ,
                        0 AS BatterySerialNumber ,
                        @AlertConditionDescription AS [Description] ,
                        d.SiteID ,
                        @AlertConditionID AS AlertConditionID ,
                        0 AS AlertStatus ,
                        @Priority AS [Priority] ,
                        'N' AS TablePointer ,
                        a.ROW_ID AS RowPointer
                FROM    #a a --join NonSessionData b on a.ROW_ID = b.ROW_ID
                        JOIN dbo.Assets d ON a.DeviceSerialNumber = d.SerialNo
                        LEFT JOIN ( SELECT  *
                                    FROM    dbo.AlertMessage
                                    WHERE   AlertConditionID = @AlertConditionID
                                            AND AlertStatus <> 20
                                  ) am ON a.DeviceSerialNumber = am.DeviceSerialNumber
                WHERE   am.ROW_ID IS NULL
                        AND d.IDAssetType IN ( 1, 3, 6 );



    END;



GO
