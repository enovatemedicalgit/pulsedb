SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC  [dbo].[spDashboardBatteryHealthLevels]
    @SiteID INT = 212 ,
    @Department INT = NULL
AS
    SET NOCOUNT ON;
    DECLARE @cntBatt AS INT;
    SET @cntBatt = ( SELECT COUNT(IDAsset)
                     FROM   dbo.Assets
                     WHERE  IDAssetType = 5
                            AND SiteID = @SiteID
                            AND FullChargeCapacity > 0
                            AND FullChargeCapacity IS NOT NULL
                            AND Retired = 0
                            AND ( DepartmentID = @Department
                                  OR @Department IS NULL
                                )
                   );

    SELECT  base.x ,
            base.y ,
            base.heat ,
            base.symbol ,
            @cntBatt
    FROM    ( SELECT    ( CAST(( ROW_NUMBER() OVER ( ORDER BY CONVERT(INT,  dbo.fnGetBatteryCapacity_Int(SerialNo,
                                                              FullChargeCapacity)) DESC ) )
                          / ( @cntBatt / 3.27 ) AS INT) ) + 1 AS y ,
                        ( CAST(( ROW_NUMBER() OVER ( ORDER BY CONVERT(INT,  dbo.fnGetBatteryCapacity_Int(SerialNo,
                                                              FullChargeCapacity)) DESC ) )
                          % ( @cntBatt / 3.27 ) AS INT) ) + 1 AS x ,
                        SerialNo AS symbol ,
                        CONVERT(float, dbo.fnGetBatteryCapacity_Int(SerialNo,
                                                              FullChargeCapacity))
                        / 100 AS heat
              FROM      dbo.Assets
              WHERE     IDAssetType = 5
                        AND SiteID = @SiteID
                        AND FullChargeCapacity > 0
                        AND FullChargeCapacity IS NOT NULL
                        AND Retired = 0
                        AND ( DepartmentID = @Department
                              OR @Department IS NULL
                            )
            ) AS base
    ORDER BY base.heat,base.x ,
            base.y;
	


GO
