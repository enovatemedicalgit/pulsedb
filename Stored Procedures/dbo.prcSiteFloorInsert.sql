SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcSiteFloorInsert]
    @Description NVARCHAR(50) ,
    @SiteID INT
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
	
	
    INSERT  INTO dbo.SiteFloors
            ( [Description], [SiteId] )
            SELECT  @Description ,
                    @SiteID;
 
    SELECT  IdSiteFloor
    FROM    dbo.SiteFloors
    WHERE   Description = @Description
            AND SiteId = @SiteID;



GO
