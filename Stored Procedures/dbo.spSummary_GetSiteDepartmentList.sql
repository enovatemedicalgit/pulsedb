SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Mike Jones And Arlow Farrell
-- Create date: 2015-05-20
-- Description:	Gets summary data for the supplied
--              date and business unit
-- =============================================
CREATE PROCEDURE [dbo].[spSummary_GetSiteDepartmentList]
    (
      @Date VARCHAR(10) ,
      @IDSite INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT  bud.Description AS Department ,
                @IDSite AS IDSite ,
                a.* ,
                b.AvgUtilization ,
                c.AvgEstimatedPCUtilization
        FROM    ( SELECT    [Date] ,
                            IDDepartment ,
                            SUM(ActiveWorkstationCount) AS ActiveWorkstationCount ,
                            SUM(AvailableWorkstationCount) AS AvailableWorkstationCount ,
                            SUM(OfflineWorkstationCount) AS OfflineWorkstationCount ,
                            SUM(LoChargeInsertsCount) AS LoChargeInsertsCount ,
                            SUM(HiChargeRemovalsCount) AS HiChargeRemovalsCount ,
                            SUM(SessionCount) AS SessionCount ,
                            AVG(AvgRunRate) AS AvgRunRate ,
                            AVG(AvgAmpDraw) AS AvgAmpDraw ,
                            AVG(COALESCE(AvgChargeLevel, 0)) AS AvgChargeLevel ,
                            AVG(HiChargeInserts) AS HiChargeInserts ,
                            AVG(LoChargeRemovals) AS LoChargeRemovals ,
                            AVG(AvgSignalQuality) AS AvgSignalQuality ,
                            AVG(RemainingCapacity) AS RemainingCapacity
                  FROM      dbo.SummarySiteDepartment
                  WHERE     [Date] = @Date
                            AND IDSite = @IDSite
                  GROUP BY  [Date] ,
                            IDDepartment
                ) a
                LEFT JOIN (
			-- get utilization 'average' (though it must exclude rows where utilization is 0)
                            SELECT  [Date] ,
                                    IDDepartment ,
                                    AVG(COALESCE(Utilization, 0)) AS AvgUtilization
                            FROM    dbo.SummarySiteDepartment
                            WHERE   Utilization > 0
                                    AND [Date] = @Date
                                    AND IDSite = @IDSite
                            GROUP BY [Date] ,
                                    IDDepartment
                          ) b ON a.Date = b.Date
                                 AND a.IDDepartment = b.IDDepartment
                LEFT JOIN (
			-- get AvgEstimatedPCUtilization 'average' (though it must exclude rows where AvgEstimatedPCUtilization is 0)
                            SELECT  [Date] ,
                                    IDDepartment ,
                                    AVG(COALESCE(AvgEstimatedPCUtilization, 0)) AS AvgEstimatedPCUtilization
                            FROM    dbo.SummarySiteDepartment
                            WHERE   AvgEstimatedPCUtilization != 0
                                    AND [Date] = @Date
                                    AND IDSite = @IDSite
                            GROUP BY [Date] ,
                                    IDDepartment
                          ) c ON a.Date = c.Date
                                 AND a.IDDepartment = c.IDDepartment
                JOIN dbo.Departments bud ON a.IDDepartment = bud.IDDepartment
        ORDER BY bud.Description;

    END;

GO
