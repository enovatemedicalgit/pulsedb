SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[prcDashGetSiteAssetsSummary](
	 @SiteID INT = 657)
AS
	SELECT
		  a.SiteId
		, COALESCE(a.DepartmentId, 0) AS                                                      DepartmentId
		, ISNULL(ISNULL('Mobius '+bpl.Generation+' Battery',  IIF(a.IDAssetType IN (2,4), CAST(a.BayCount AS NVARCHAR(5)) + ' Bay ' + at.Category, at.Category)), IIF(a.IDAssetType IN (2,4),a.BayCount + ' Bay ' + at.Category, at.category)) AS Category
		, COUNT(DISTINCT a.SerialNo) AS                                                       cnt
		  -- , a.IDAssetType
	FROM
		dbo.Assets a
	LEFT JOIN
		dbo.AssetType at
		ON a.IDAssetType = at.IDAssetType
	LEFT JOIN
		dbo.PartNumber AS PN
		ON SUBSTRING(a.serialno, 1, 7) = pn.partnumber
	LEFT JOIN
		dbo.BatteryPartList AS BPL
		ON SUBSTRING(a.serialno, 1, 7) = bpl.PartNumber
	WHERE  a.SiteID = @SiteID AND a.Retired = 0 AND a.LastPostDateUTC > DATEADD(DAY, -15,GETUTCDATE())
	GROUP BY
		    a.SiteId
		  , COALESCE(a.DepartmentId, 0),
		  a.BayCount
	, ISNULL(ISNULL('Mobius '+bpl.Generation+' Battery',  IIF(a.IDAssetType IN (2,4), cast(a.BayCount AS NVARCHAR(5)) + ' Bay ' + at.Category, at.Category)), IIF(a.IDAssetType IN (2,4), a.BayCount + ' Bay ' + at.Category, at.Category))
		ORDER BY ISNULL(ISNULL('Mobius '+bpl.Generation+' Battery',  IIF(a.IDAssetType IN (2,4), cast(a.BayCount AS NVARCHAR(5)) + ' Bay ' + at.Category, at.Category)), IIF(a.IDAssetType IN (2,4), a.BayCount + ' Bay ' + at.Category, at.Category)) desc
GO
