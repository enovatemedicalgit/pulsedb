SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Description:	Returns count of workstations
--              in service, as needed for the
--              dashboard feature in CAST
-- =============================================
CREATE PROCEDURE [dbo].[spDash_WorkstationsInService] ( @CustomerID INT )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @startingsdrowid INT;
        DECLARE @startingnsdrowid INT;
        SET @startingsdrowid = ( SELECT MAX(ROW_ID)
                                 FROM   dbo.SessionDataCurrent
                                 WHERE  CreatedDateUTC < DATEADD(HOUR, -24,
                                                              GETUTCDATE())
                               );

        SET @startingnsdrowid = ( SELECT    MAX(ROW_ID)
                                  FROM      dbo.NonSessionDataCurrent
                                  WHERE     CreatedDateUTC < DATEADD(HOUR, -24,
                                                              GETUTCDATE())
                                );

        SELECT  COUNT(*) AS WorkstationsInService
        FROM    ( SELECT    allpackets.DeviceSerialNumber --, sum(packets) as packets
                  FROM      ( SELECT DISTINCT
                                        sd.DeviceSerialNumber ,
                                        COUNT(sd.ROW_ID) AS packets
                              FROM      dbo.SessionDataCurrent sd
                                        JOIN dbo.Assets d ON sd.DeviceSerialNumber = d.SerialNo
                              WHERE     d.SiteID IN (
                                        SELECT  d.SiteID
                                        FROM    dbo.Sites
                                        WHERE   CustomerID = @CustomerID
                                        UNION
                                        SELECT  @CustomerID AS IDCustomer )
                                        AND sd.DeviceType IN (
                                        SELECT  IDAssetType
                                        FROM    dbo.AssetType
                                        WHERE   Category = 'Workstation' )
                                        AND sd.ROW_ID > @startingsdrowid
                              GROUP BY  sd.DeviceSerialNumber
                              UNION
                              SELECT DISTINCT
                                        sd.DeviceSerialNumber ,
                                        COUNT(sd.ROW_ID) AS packets
                              FROM      dbo.NonSessionDataCurrent sd
                                        JOIN dbo.Assets d ON sd.DeviceSerialNumber = d.SerialNo
                              WHERE     d.SiteID IN (
                                        SELECT  d.SiteID
                                        FROM    dbo.Sites
                                        WHERE   CustomerID = @CustomerID
                                        UNION
                                        SELECT  @CustomerID AS IDCustomer )
                                        AND sd.DeviceType IN (
                                        SELECT  IDAssetType
                                        FROM    dbo.AssetType
                                        WHERE   Category = 'Workstation' )
                                        AND sd.ROW_ID > @startingnsdrowid
                              GROUP BY  sd.DeviceSerialNumber
                            ) allpackets
                            LEFT JOIN ( SELECT DISTINCT
                                                DeviceSerialNumber
                                        FROM    dbo.AlertMessage
                                        WHERE   AlertStatus <> 20
                                                AND SiteID IN (
                                                SELECT  SiteID
                                                FROM    dbo.Sites
                                                WHERE   CustomerID = @CustomerID
                                                UNION
                                                SELECT  @CustomerID AS IDCustomer )
                                                AND [Priority] = '911'
                                      ) unresolvednotifications ON allpackets.DeviceSerialNumber = unresolvednotifications.DeviceSerialNumber
                  WHERE     allpackets.packets >= 10
                            AND unresolvednotifications.DeviceSerialNumber IS NULL
                  GROUP BY  allpackets.DeviceSerialNumber
                ) asdf;



    END;



GO
