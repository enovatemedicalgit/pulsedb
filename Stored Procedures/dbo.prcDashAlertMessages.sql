SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE Procedure [dbo].[prcDashAlertMessages] @SiteId AS INT = 212
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
 SELECT TOP 300
                *
        FROM    ( SELECT TOP 270
                            am.Priority ,
                            am.CreatedDateUTC ,
                            ISNULL(am.SFCID, 'No Case') AS SFCID ,
                            CASE WHEN am.Priority = 211 THEN 'Info'
                                 WHEN am.Priority = 911 THEN 'Alert'
                                 ELSE 'Notification'
                            END AS PacketType ,
                            CASE WHEN ac.AppliesTo = 1 THEN ass.SerialNo
                                 WHEN ac.AppliesTo = 0 THEN ass.SerialNo
                                 ELSE assbatt.SerialNo
                            END AS SerialNumber ,
                           SUBSTRING(am.Description,1,20) AS FriendlyDescription ,
                            CASE WHEN ac.AppliesTo = 1 THEN 'WKSTN'
                                 WHEN ac.AppliesTo = 2 THEN 'BATT'
                                 ELSE 'CHRG'
                            END AS AssetDesc
                  FROM      dbo.AlertMessage am
                            LEFT JOIN dbo.AlertConditions ac ON am.AlertConditionID = ac.ROW_ID
                            LEFT JOIN dbo.Assets ass ON am.DeviceSerialNumber = ass.SerialNo
                            LEFT JOIN dbo.Assets assbatt ON am.BatterySerialNumber = assbatt.SerialNo
                  WHERE     am.Priority = 511
                            AND am.CreatedDateUTC > DATEADD(MONTH, -5,
                                                            GETUTCDATE())
                  UNION ALL
                  SELECT TOP 100
                            am.Priority ,
                            am.CreatedDateUTC ,
                            ISNULL(am.SFCID, 'No Case') AS SFCID ,
                            CASE WHEN am.Priority = 211 THEN 'Info'
                                 WHEN am.Priority = 911 THEN 'Alert'
                                 ELSE 'Notification'
                            END AS PacketType ,
                            CASE WHEN ac.AppliesTo = 1 THEN ass.SerialNo
                                 WHEN ac.AppliesTo = 0 THEN ass.SerialNo
                                 ELSE assbatt.SerialNo
                            END AS SerialNumber ,
                            am.FriendlyDescription ,
                            CASE WHEN ac.AppliesTo = 1 THEN 'WKSTN'
                                 WHEN ac.AppliesTo = 2 THEN 'BATT'
                                 ELSE 'CHRG'
                            END AS AssetDesc
                  FROM      dbo.AlertMessage am
                            LEFT JOIN dbo.AlertConditions ac ON am.AlertConditionID = ac.ROW_ID
                            LEFT JOIN dbo.Assets ass ON am.DeviceSerialNumber = ass.SerialNo
                            LEFT JOIN dbo.Assets assbatt ON am.BatterySerialNumber = assbatt.SerialNo
                  WHERE     am.Priority = 911
                            AND am.CreatedDateUTC > DATEADD(MONTH, -5,
                                                            GETUTCDATE())
                ) asdf
        ORDER BY asdf.SerialNumber DESC;

    END;


GO
