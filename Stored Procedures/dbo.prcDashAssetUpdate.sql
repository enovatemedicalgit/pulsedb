SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcDashAssetUpdate]
    @IDAsset INT ,
    @SerialNo VARCHAR(50) = NULL ,
    @AssetNumber VARCHAR(50) = NULL ,
    @Floor VARCHAR(50) = NULL ,
    @Wing VARCHAR(50) = NULL ,
    @DepartmentID INT = NULL ,
    @SiteID INT = NULL ,
    @SiteFloorID INT = NULL ,
    @SiteWingID INT = NULL ,
    @Notes VARCHAR(50) = NULL ,
    @Other VARCHAR(50) = NULL ,
    @InternalPostDate DATE = NULL ,
    @IDUser INT = NULL ,
    @Manual INT = 0
AS
    SET NOCOUNT ON;  
    BEGIN	 
        IF ( @SiteFloorID IS NULL )
            BEGIN
                SET @SiteFloorID = ( SELECT TOP 1
                                            sf.IdSiteFloor
                                     FROM   dbo.SiteFloors AS sf
                                     WHERE  sf.SiteId = @SiteID
                                            AND sf.Description = @Floor
                                   );
                SET @SiteWingID = ( SELECT TOP 1
                                            SW.IdSiteWing
                                    FROM    dbo.SiteWings AS SW
                                    WHERE   SW.SiteId = @SiteID
                                            AND SW.Description = @Wing
                                  );
            END;

        BEGIN 
 
            UPDATE  dbo.Assets
            SET     [AssetNumber] = ISNULL(@AssetNumber, [AssetNumber]) ,
                    [DepartmentID] = ISNULL(@DepartmentID, [DepartmentID]) ,
                    [Floor] = ISNULL(@Floor, [Floor]) ,
                    [Wing] = ISNULL(@Wing, [Wing]) ,
                    [SiteID] = ISNULL(@SiteID, [SiteID]) ,
                    [SiteFloorID] = ISNULL(@SiteFloorID, [SiteFloorID]) ,
                    [Notes] = ISNULL(@Notes, [Notes]) ,
                    [Other] = ISNULL(@Other, [Other]) ,
                    [ModifiedBy] = ISNULL(@IDUser, [ModifiedBy]) ,
                    [LastInternalPostDateUTC] = ISNULL(@InternalPostDate,
                                                       [LastInternalPostDateUTC]) ,
                    [ManualAllocation] = @Manual
            WHERE   IDAsset = @IDAsset;
          
		    DECLARE @sID INT;
            DECLARE @iType INT;
            SET @sID = ( SELECT TOP 1
                                A.SiteID
                         FROM   dbo.Assets AS A
                         WHERE  A.IDAsset = @IDAsset
                       );
            SET @iType = ( SELECT TOP 1
                                    A.IDAssetType
                           FROM     dbo.Assets AS A
                           WHERE    A.IDAsset = @IDAsset
                         );

            IF ( @iType IN ( 5 ) )
                BEGIN
                    EXEC dbo.prcGetBattery @SiteID = @sID; -- int
                END;
            IF ( @iType IN ( 1, 6 ) )
                BEGIN
                    EXEC dbo.prcGetCharger @SiteID = @sID; -- int
                END;
            IF ( @iType IN ( 2, 4 ) )
                BEGIN
                EXEC dbo.prcGetCharger @SiteID = @sID
			    END;
        END;   
    END;
   


GO
