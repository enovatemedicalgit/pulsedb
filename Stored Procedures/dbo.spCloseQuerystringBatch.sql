SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spCloseQuerystringBatch]
    (
      @guid UNIQUEIDENTIFIER
    )
AS
    BEGIN
        SET NOCOUNT ON;

        UPDATE  dbo.QueryString
        SET     NineParsed = 0
        WHERE   ProcessID = @guid;
	
    END;


GO
