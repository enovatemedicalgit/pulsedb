SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE Procedure [dbo].[prcmobileLostAssetCommSummary]
    @SiteID VARCHAR(20) ,
    @AssetSerialNumber VARCHAR(20) = NULL
AS
    BEGIN
--if @AssetSerialNumber is null
        BEGIN
            SELECT  SiteDescription ,
                    Description AS AssetDescription ,
                    COUNT(SerialNo) AS AssetCount
            FROM    dbo.viewLostAssetCommSimple
            WHERE   AssetStatusID < 2
                    AND IDAssetType != 5
                    AND IDSite = @SiteID
                    AND Retired <> 1
                    AND ( (LastPostDateUTC < DATEADD(HOUR, -24, GETDATE())
                          OR ( IDAssetType IN ( 1, 6 )
                               AND LastPostDateUTC < DATEADD(DAY, -6,
                                                             GETDATE())
                             ))
                        )
            GROUP BY SiteDescription ,
                    Description ,
                    IDAssetType
            ORDER BY Description;
        END; 
--Else
--BEGIN
--select * from dbo.viewLostAssetComm where AssetStatusID <> 3 and IDSite=@SiteID and DeviceSerialNumber = @AssetSerialNumber
--END
    END;



GO
