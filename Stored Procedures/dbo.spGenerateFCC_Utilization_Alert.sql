SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
 
-- Description:	Creates alert for < 70 Utilization
-- =============================================
CREATE PROCEDURE [dbo].[spGenerateFCC_Utilization_Alert]
AS
    BEGIN

        IF OBJECT_ID('tempdb..#tempTable') IS NOT NULL
            BEGIN
                DROP TABLE #tempTable;
            END;
	
        CREATE TABLE #tempTable
            (
              id INT IDENTITY ,
              BatterySerialNumber VARCHAR(50)
            );
	
        INSERT  INTO #tempTable
                SELECT  k.BatterySerialNumber
                FROM    ( SELECT    BatterySerialNumber ,
                                    COUNT(*) AS Cnt
                          FROM      ( SELECT    *
                                      FROM      ( SELECT    a.Batteryserialnumber ,
                                                            DAY(a.CreatedDateUTC) AS dy ,
                                                            COUNT(*) AS cnt
                                                  FROM      dbo.SessionDatacurrent a
                                                            JOIN dbo.BatteryPartList b ON LEFT(a.BatterySerialNumber,
                                                              7) = b.PartNumber
                                                            JOIN dbo.Assets d ON a.Batteryserialnumber = d.SerialNo
                                                  WHERE     a.FullChargeCapacity < ( b.CalcAH
                                                              * .7 )
                                                            AND Voltage <> 0
                                                            AND a.CreatedDateUTC > DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                            AND a.ChargeLevel > 95
                                                            AND d.SiteID NOT IN (
                                                            4 )
                                                  GROUP BY  a.Batteryserialnumber ,
                                                            DAY(a.CreatedDateUTC)
                                                ) i
                                      WHERE     i.cnt >= 3
                                    ) j
                          GROUP BY  BatterySerialNumber
                        ) k
                WHERE   k.Cnt >= 3;


        DECLARE @cnt INT;
        DECLARE @buid INT;
        DECLARE @currBattery VARCHAR(50);
        DECLARE @row_id INT;
        DECLARE @apmac VARCHAR(50);
        DECLARE @DeviceSerialNumber VARCHAR(50);
        DECLARE @alertId INT;

        SET @alertId = 84;
        SET @cnt = 1;

        DECLARE @dt DATETIME;
        SET @dt = DATEADD(DAY, 1, GETUTCDATE());

        WHILE ( @cnt <= ( SELECT    MAX(id)
                          FROM      #tempTable
                        ) ) --have records
            BEGIN
                SET @currBattery = ( SELECT BatterySerialNumber
                                     FROM   #tempTable
                                     WHERE  id = @cnt
                                   );
                SET @buid = ( SELECT TOP 1
                                        SiteID
                              FROM      dbo.Assets
                              WHERE     SerialNo = ( SELECT BatterySerialNumber
                                                     FROM   #tempTable
                                                     WHERE  id = @cnt
                                                   )
                            );
		
                IF NOT EXISTS ( SELECT TOP 1
                                        *
                                FROM    dbo.AlertMessage
                                WHERE   ( BatterySerialNumber = @currBattery
                                          AND AlertConditionID = @alertId
                                          AND AlertStatus <> 20
                                        )
                                        OR ( BatterySerialNumber = @currBattery
                                             AND AlertConditionID = @alertId
                                             AND AlertStatus = 20
                                             AND CreatedDateUTC BETWEEN DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                              AND
                                                              GETUTCDATE()
                                           ) )
                    BEGIN
                        SELECT TOP 1
                                @row_id = a.ROW_ID ,
                                @apmac = a.APMAC ,
                                @DeviceSerialNumber = a.DeviceSerialNumber
                        FROM    dbo.SessionDataCurrent a
                                JOIN dbo.BatteryPartList b ON LEFT(a.BatterySerialNumber,
                                                              7) = b.PartNumber
                                JOIN dbo.Assets d ON a.Batteryserialnumber = d.SerialNo
                        WHERE   a.FullChargeCapacity < ( b.CalcAH * .7 )
                                AND Voltage <> 0
                                AND a.CreatedDateUTC > DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                AND a.ChargeLevel > 95
                                AND d.SiteID = @buid
                                AND a.Batteryserialnumber = @currBattery
                        ORDER BY a.CreatedDateUTC DESC;
			  
                        INSERT  INTO pulse.dbo.AlertMessage
                                ( [CreatedDateUTC] ,
                                  [DeviceSerialNumber] ,
                                  [BatterySerialNumber] ,
                                  [Description] ,
                                  [Notes] ,
                                  [StingerNotes] ,
                                  SiteID ,
                                  [EmailQueueID] ,
                                  [AlertConditionID] ,
                                  [AlertStatus] ,
                                  [QueuedForEmail] ,
                                  [QueuedForEmailUTC] ,
                                  [Priority] ,
                                  [TablePointer] ,
                                  [RowPointer] ,
                                  [ModifiedDateUTC] ,
                                  [ModifiedUserID] ,
                                  [Bay] ,
                                  [APMAC] ,
                                  [Source] ,
                                  [FriendlyDescription] ,
                                  [SFCID]
                                )
                        VALUES  ( GETUTCDATE() ,
                                  @DeviceSerialNumber ,
                                  @currBattery ,
                                  'Main battery at less than 60% capacity' ,
                                  '' ,
                                  '' ,
                                  @buid ,
                                  0 ,
                                  @alertId ,
                                  0 ,
                                  NULL ,
                                  NULL ,
                                  911 ,
                                  'S' ,
                                  @row_id ,
                                  NULL ,
                                  NULL ,
                                  NULL ,
                                  @apmac ,
                                  'S' ,
                                  'Main battery at less than 60% capacity' ,
                                  NULL
                                );
                    END;
		
                SET @cnt = @cnt + 1;
            END; -- while
	


        IF OBJECT_ID('tempdb..#tempTable') IS NOT NULL
            BEGIN
                DROP TABLE #tempTable;
            END;
    END;


GO
