SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcAddCustomer]
    @CASTBUID VARCHAR(50) = NULL ,
    @CustomerName VARCHAR(50) ,
    @SFCustomerID VARCHAR(50) = NULL ,
    @SytelineCustomerNum VARCHAR(50) = NULL ,
    @SytelineCustomerSeq VARCHAR(50) = NULL
AS
    SET NOCOUNT ON; 
  
	
  
		 
    INSERT  INTO dbo.Customers
            ( [CASTBUID] ,
              [CustomerName] ,
              [SFCustomerID] ,
              [SytelineCustomerNum] ,
              [SytelineCustomerSeq]
            )
            SELECT  @CASTBUID ,
                    @CustomerName ,
                    @SFCustomerID ,
                    @SytelineCustomerNum ,
                    @SytelineCustomerSeq;
	 

   


GO
