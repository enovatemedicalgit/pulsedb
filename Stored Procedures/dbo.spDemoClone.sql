SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================

-- Description:	Clones devices, sessiondata and
--              nonsessiondata for any device
--              listed on the DemoClone table
-- =============================================
CREATE PROCEDURE [dbo].[spDemoClone]
AS
    BEGIN

	/*

	1) establish clone devices on Device table, if not already there

		- will need to establish DepartmentID also - or just hard code them


	2) copy data from SessionData, substituting the serial number with a clone serial number and setting SessionID to 0

	   - create batteries on Device table, if they already there, but using fake serial numbers (replace the first three digits with 555)

	   - create access points on AccessPoint table, using 55 in the first two positions of the APMAC

	   - get most recent timestamp of last copy process so you don't duplicate any rows when copying

		   - use the most recent of these two:

			   - 8 days of data from specified devices (7 full days plus any data from today)

			   - pickup any packets received after job last ran

	3) copy data from NonSessionData, substituting the serial number with a clone serial number

	   - create batteries on Device table, if they already there, but using fake serial numbers (replace the first three digits with 555)

	   - create access points on AccessPoint table, using 55 in the first two positions of the APMAC




	4) log the timestamp of last cloning so you can know the starting point for the next copy process (assuming the next copy is scheduled within the coming 10 minutes)


	What about DeviceMAC and IP?
	Replace first segment of IP with 555.
	Replace beginning of DeviceMAC with 55:5
	Replace beginning of APMAC with 55:5

	*/

	-- set SessionID to 0 on all copied rows



	--return 0


        DECLARE @FromTimestampUTC DATETIME;
        DECLARE @ToTimestampUTC DATETIME;

        SET @FromTimestampUTC = DATEADD(DAY, -8, GETUTCDATE());
        SET @ToTimestampUTC = GETUTCDATE(); -- keep this for updating the DemoCloneRunLog so we can know when this job last ran

        IF ( SELECT COUNT(*)
             FROM   dbo.DemoCloneRunLog
           ) > 0
            BEGIN
                DECLARE @LastRanTimestampUTC DATETIME;
                SET @LastRanTimestampUTC = ( SELECT TOP 1
                                                    LastRanTimestampUTC
                                             FROM   dbo.DemoCloneRunLog
                                           );
		
                IF ( @LastRanTimestampUTC > @FromTimestampUTC )
                    BEGIN
			-- pick up where previous copy left off (if copy occurred more recently than 8 days ago)
                        SET @FromTimestampUTC = @LastRanTimestampUTC;
                    END;
		
                DELETE  FROM dbo.DemoCloneRunLog;
		
            END;

	 -- Log the timestamp so we can know where we left off (for next time the process runs)
        INSERT  INTO dbo.DemoCloneRunLog
                ( LastRanTimestampUTC )
        VALUES  ( @ToTimestampUTC );

	/*

		---------------   DEVICE COPY : TOP   ---------------
		
		Establish any needed rows on Departments table

	*/
        IF OBJECT_ID(N'tempdb..#ClonedDepartmentss', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #ClonedDepartmentss;
            END;


		-- determine which Departmentss will be needed (hold them in a temp table)
        SELECT DISTINCT
                dc.SiteID ,
                bud.Description ,
                0 AS ClonedDepartmentID ,
                d.DepartmentID AS SourceDepartmentID
        INTO    #ClonedDepartmentss
        FROM    dbo.DemoClone dc
                LEFT JOIN dbo.Assets d ON dc.SourceSerialNumber = d.SerialNo
                LEFT JOIN dbo.Departments bud ON d.DepartmentID = bud.IDDepartment
        WHERE   dc.CloneSerialNumber IN (
				-- clone serial numbers which are not yet on the device table
                SELECT  dc.CloneSerialNumber
                FROM    dbo.DemoClone dc
                        LEFT JOIN dbo.Assets d ON dc.CloneSerialNumber = d.SerialNo
                WHERE   d.SerialNo IS NULL );


		-- insert the Departments if it's not yet established for the demo business unit
        INSERT  INTO dbo.Departments
                ( SiteID ,
                  [Description]
                )
                SELECT  cbud.SiteID ,
                        cbud.Description
                FROM    #ClonedDepartmentss cbud
                        LEFT JOIN dbo.Departments bud ON cbud.SiteID = bud.SiteID
                                                     AND cbud.Description = bud.Description
                WHERE   bud.IDDepartment IS NULL
                        AND bud.Description <> 'Unallocated'; -- unallocated is a global department, used by any/all business units
		

		-- now update the ClonedDepartmentID column so you 
		-- will have a complete xref from the source DepartmentID to the new cloned one
        UPDATE  #ClonedDepartmentss
        SET     ClonedDepartmentID = bud.IDDepartment
        FROM    #ClonedDepartmentss cbud
                JOIN dbo.Departments bud ON cbud.SiteID = bud.SiteID
                                        AND cbud.Description = bud.Description;



	-- Fields that will need to be adjusted for cloned devices:
	--		AccessPointID
	--		ModifiedUserID (use 15)
	--		CreatedUserID (use 15)
	--		SiteID
	--		DepartmentID
	--		InvoiceNumber (?)
	--		DeviceMAC
	--		SerialNumber
	--		AssetNumber (blank it out)

        INSERT  INTO pulse.dbo.Assets
                ( [CreatedDateUTC] ,
                  SerialNo ,
                  [DeviceMAC] ,
                  [InvoiceNumber] ,
                  [Description] ,
                  [UtilizationLevel] ,
                  [SiteID] ,
                  IDAssetType ,
                  [IsActive] ,
                  [AssetNumber] ,
                  [DepartmentID] ,
                  [Wing] ,
                  [Floor] ,
                  [Other] ,
                  [WiFiFirmwareRevision] ,
                  [CommandCode] ,
                  [OutOfService] ,
                  [LastPostDateUTC] ,
                  [Availability] ,
                  [AccessPointId] ,
                  [Notes] ,
                  [Retired] ,
                  AssetStatusID
                )
                SELECT  d.CreatedDateUTC ,
                        dc.CloneSerialNumber ,
                        '55:5' + SUBSTRING(d.DeviceMAC,
                                           CHARINDEX(':', d.DeviceMAC) + 2, 20) AS [DeviceMAC] ,
                        'ENO12345869' AS [InvoiceNumber] ,
                        d.Description ,
                        d.UtilizationLevel ,
                        dc.SiteID ,
                        d.IDAssetType ,
                        d.IsActive ,
                        '' AS [AssetNumber] ,
                        cbud.ClonedDepartmentID ,
                        d.Wing ,
                        d.Floor ,
                        d.Other ,
                        d.WiFiFirmwareRevision ,
                        d.CommandCode ,
                        d.OutOfService ,
                        d.LastPostDateUTC ,
                        d.Availability ,
                        0 AS [AccessPointID] ,
                        'For demo purposes only' AS [Notes] ,
                        d.Retired ,
                        d.AssetStatusID
                FROM    dbo.DemoClone dc
                        LEFT JOIN dbo.Assets d ON dc.SourceSerialNumber = d.SerialNo
                        LEFT JOIN ( SELECT DISTINCT
                                            ClonedDepartmentID ,
                                            SourceDepartmentID
                                    FROM    #ClonedDepartmentss
                                  ) cbud ON d.DepartmentID = cbud.SourceDepartmentID
                WHERE   dc.CloneSerialNumber IN (
			-- clone serial numbers which are not yet on the device table
                        SELECT  dc.CloneSerialNumber
                        FROM    dbo.DemoClone dc
                                LEFT JOIN dbo.Assets d ON dc.CloneSerialNumber = d.SerialNo
                        WHERE   d.SerialNo IS NULL );
		
	/*	---------------   DEVICE COPY : END   ---------------	*/

        EXEC dbo.spDemoClone_SessionData @FromTimestampUTC; 
        EXEC dbo.spDemoClone_NonSessionData @FromTimestampUTC; 


    END;

GO
