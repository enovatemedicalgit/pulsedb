SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE Procedure [dbo].[prcGetSystemSettings] --'','','','37167','','','','37130',''
AS
    BEGIN
        SELECT  piv.APIURL ,
                piv.DASHBOARDURL ,
                piv.DefaultFromAddress ,
                piv.DefaultFromName ,
                piv.INSERVICEHOURS ,
                piv.OFFLINEDAYS ,
                piv.OUTOFCOMMUNICATION ,
                piv.SiteReplicatedID ,
                piv.SnapshotInterval ,
                piv.TestMode ,
                piv.TestNumHours ,
                piv.TestSiteID ,
                piv.TestSiteLastDuplication ,
                piv.TestStartTime ,
                s.SiteName AS TestSiteName
        FROM    ( SELECT    SettingKey ,
                            SettingValue
                  FROM      dbo.SystemSettings
                ) d PIVOT ( MAX(SettingValue) FOR SettingKey IN ( APIURL,
                                                              DASHBOARDURL,
                                                              DefaultFromAddress,
                                                              DefaultFromName,
                                                              INSERVICEHOURS,
                                                              OFFLINEDAYS,
                                                              OUTOFCOMMUNICATION,
                                                              SiteReplicatedID,
                                                              SnapshotInterval,
                                                              TestMode,
                                                              TestNumHours,
                                                              TestSiteID,
                                                              TestSiteLastDuplication,
                                                              TestStartTime ) ) piv
                LEFT JOIN dbo.Sites s ON s.IDSite = piv.TestSiteID;
    END;
    

GO
