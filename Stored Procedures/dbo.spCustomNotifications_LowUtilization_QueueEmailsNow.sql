SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- ==============================================
-- Author:		Arlow Farrell
-- Create date: 2013-02-20
-- Description:	Queues emails to users who
--              subscribe to the notification
--              listed as 'Utilization under 20%'
-- Modified 7/25/2013 DC
-- Rebranded, changed emails, colors, hyperlinks
-- ==============================================
CREATE PROCEDURE [dbo].[spCustomNotifications_LowUtilization_QueueEmailsNow]
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AlertSubscribeId INT;
        SET @AlertSubscribeId = 2;

	/*  
	
		This does one thing, twice:
		---------------------------
		First, sends email to HQ users regarding all HQ facilities
		Second, sends email to users associated only with an individual facility
	
	
	*/

        /* --------------------------------------------------------------- */

        /*    Create a temp table which maps users to their facilities     */

        /*    This is needed because HQ users must receive notifications   */

        /*    for ANY of their facilities.                                 */

        /*    Non-HQ users will only receive notifications for their       */

        /*    single facility.                                             */

        /* --------------------------------------------------------------- */

        IF OBJECT_ID(N'tempdb..#tempUserBUID', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tempUserBUID;
            END;
        SELECT  asub.UserID AS SubscribedUserId ,
                bu.CustomerID AS ParentSite ,
                bu.IDSite AS SubscribedSiteID ,
                CASE WHEN bu2.SiteType = 1 THEN 'HQ'
                     ELSE 'Facility'
                END AS EmailRecipientGrouping
        INTO    #tempUserBUID
        FROM    dbo.AlertSubscription asub
                JOIN dbo.Sites bu ON asub.SiteID = bu.IDSite
                                 OR asub.SiteID = bu.CustomerID
                JOIN dbo.[User] u ON asub.UserID = u.IDUser
			 JOIN dbo.UserSites us ON u.IDUser = us.User_Row_Id
                JOIN dbo.Sites bu2 ON us.SiteId = bu2.IDSite
        WHERE   asub.AlertSubscribeId = @AlertSubscribeId -- utilization below 20%
                AND bu.SiteType = 2; -- no need to show HQ BUIDs because devices should never be assigned to HQs 

        IF OBJECT_ID(N'tempdb..#tempLowUtilization', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tempLowUtilization;
            END;
        SELECT  [Year] ,
                [YearUTC] ,
                [Month] ,
                [MonthUTC] ,
                [Day] ,
                [DayUTC] ,
                [Date] ,
                [DateUTC] ,
                [SiteID] ,
                [DepartmentID] ,
                [SessionCount] ,
                [AvgSessionLengthHours] ,
                [AvgSessionLengthMinutes] ,
                [AvgRunRate] ,
                [ActiveDeviceCount] ,
                [Utilization] ,
                [NonSessionRecordCount] ,
                [CreatedDateUTC] ,
                [ROW_ID]
        INTO    #tempLowUtilization
        FROM    dbo.SummaryTrending
        WHERE   [Date] = CONVERT(VARCHAR(23), DATEADD(d, -1,
                                                      CONVERT(VARCHAR(10), GETUTCDATE(), 111)), 121) -- date is formatted like 2014-01-15 00:00:00.000 (with zeros in each of the time positions)
                AND Utilization < 0.2;


        /*  make a list of business units which had low utilization  */

        IF OBJECT_ID(N'tempdb..#tempBUList', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tempBUList;
            END;
        SELECT DISTINCT
                lu.SiteID ,
                bu.SiteDescription
        INTO    #tempBUList
        FROM    #tempLowUtilization lu
                JOIN #tempUserBUID ub ON lu.SiteID = ub.SubscribedSiteID
                JOIN dbo.Sites bu ON lu.SiteID = bu.IDSite
        WHERE   lu.Date = CONVERT(VARCHAR(23), DATEADD(d, -1,
                                                         CONVERT(VARCHAR(10), GETUTCDATE(), 111)), 121) -- date is formatted like 2014-01-15 00:00:00.000 (with zeros in each of the time positions)
                AND lu.Utilization < 0.2;


        /*  make a list of business unit departments which had low utilization  */

        IF OBJECT_ID(N'tempdb..#tempBUDeptList', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tempBUDeptList;
            END;
        SELECT DISTINCT
                lu.SiteID ,
                lu.DepartmentID ,
                bud.Description AS Department ,
                lu.Utilization
        INTO    #tempBUDeptList
        FROM    #tempLowUtilization lu
                JOIN dbo.Departments bud ON lu.SiteID = bud.SiteID
                                        AND lu.DepartmentID = bud.SiteID
        UNION
        SELECT DISTINCT
                SiteID ,
                DepartmentID ,
                'Unallocated' AS Department ,
                Utilization
        FROM    #tempLowUtilization
        WHERE   DepartmentID = 0;
	
	
	/*  --------------------------------------------------------------------------------------------
		Cycle through recipients twice: once for HQ users (because they can see multiple facilities) 
		and a second time for Facility users (because they should see only *their* facility)
		
		In other words, an email may be addressed to multiple recipients ONLY as long as those users
		have the same level of access to their facility or facilities. This prevents a user who
		should only see one facility's information from seeing the same email an HQ admin would see
		(because that email may contain data for other facilities).
		
		This will be done by 
		
			first: grab distinct list of SubscribedUserId, SubscribedSiteID 
			       and EmailRecipientGrouping from #tempUserBUID
			       where EmailRecipientGrouping = 'HQ'
			       
		   step 2: join back to #tempUserBUID and #tempLowUtilization (and other tables)
		           to get data to compose the email
		
		--------------------------------------------------------------------------------------------
	*/

        -- get email from (address and name), required for inserting into EmailQueue table
        DECLARE @FromAddress VARCHAR(150);
        DECLARE @FromName VARCHAR(150);
        SELECT TOP 1
                @FromAddress = a.DefaultFromAddress ,
                @FromName = b.DefaultFromName
        FROM    ( SELECT TOP 1
                            1 AS joinkey ,
                            SettingValue AS DefaultFromAddress
                  FROM      dbo.SystemSettings
                  WHERE     SettingKey = 'EMAIL_NOTIFICATIONS_DEFAULT_FROM_ADDRESS'
                ) a
                JOIN ( SELECT TOP 1
                                1 AS joinkey ,
                                SettingValue AS DefaultFromName
                       FROM     dbo.SystemSettings
                       WHERE    SettingKey = 'EMAIL_NOTIFICATIONS_DEFAULT_FROM_NAME'
                     ) b ON a.joinkey = b.joinkey;
        DECLARE @subject VARCHAR(100);
        DECLARE @body_message VARCHAR(MAX);
        DECLARE @Subscribing_DisplayText VARCHAR(50);
        SET @Subscribing_DisplayText = ( SELECT TOP 1
                                                Subscribing_DisplayText
                                         FROM   dbo.AlertSubscribe
                                         WHERE  AlertSubscribeId = @AlertSubscribeId
                                       );
        SET @subject = 'C.A.S.T. Notification: ' + @Subscribing_DisplayText;
        DECLARE @bodyheader VARCHAR(150);
        SET @bodyheader = @Subscribing_DisplayText;
	
        /* ------------------------------------------------------------  */

        /* This part is for HQ users only. See below for Facility users  */

        /* ------------------------------------------------------------  */

        DECLARE @ParentSite INT;
        DECLARE ParentSiteCursor CURSOR
        FOR
            SELECT DISTINCT
                    ParentSite
            FROM    #tempUserBUID
            WHERE   EmailRecipientGrouping = 'HQ' FOR READ ONLY;
        OPEN ParentSiteCursor;
        FETCH ParentSiteCursor INTO @ParentSite;
        WHILE ( @@FETCH_STATUS = 0 )
            BEGIN
                DECLARE @Recipients VARCHAR(MAX);
                DECLARE @RecipientsXML VARCHAR(MAX);
                SET @RecipientsXML = ( SELECT   CAST(( SELECT u.Email
                                                       FROM   dbo.[User] u
                                                              JOIN ( SELECT
                                                              *
                                                              FROM
                                                              dbo.AlertSubscription
                                                              WHERE
                                                              SiteID = @ParentSite
                                                              AND AlertSubscribeId = @AlertSubscribeId
                                                              ) s ON u.IDUser = s.UserID
                                                       WHERE  -- suppressed for testing (will be okay, even if it remains suppressed - it's just an additional safety net)
                                                     --u.SiteID = @ParentSite
                                                     --and 
                                                              s.UserID IN (
                                                              SELECT
                                                              User_ROW_ID
                                                              FROM
                                                              dbo.UserOptions
                                                              WHERE
                                                              OptionKey = 'CAN_LOGIN_TO_CAST'
                                                              AND OptionValue = 1 )
                                                     FOR
                                                       XML PATH('Recipients') ,
                                                           TYPE
                                                     ) AS NVARCHAR(MAX))
                                     );
                SET @Recipients = ( SELECT  REPLACE(REPLACE(REPLACE(REPLACE(@RecipientsXML,
                                                              '<Recipients>',
                                                              ''),
                                                              '</Recipients>',
                                                              ''), '<Email>',
                                                            ''), '</Email>',
                                                    '; ')
                                  );
	
                -- build the email content
                SET @body_message = N'<style>body {padding: 20px; color: Gray; font-family: segoe ui, Arial, Helvetica, sans-serif;} h4 {margin-bottom: 2px;} '
                    + N' h5 {margin-bottom: 2px;} .fyi {font-size: smaller;} '
                    + N'</style> ' + N'<h4>' + @bodyheader + '</h4> '
                    + N'<h5>Applies to:</h5>' + N'<blockquote>';
		
                -- get list of facilities for the current @ParentSite

                DECLARE @SubscribedSiteID INT;
                DECLARE @FacilityName VARCHAR(100);
                DECLARE FacilityList CURSOR
                FOR
                    SELECT DISTINCT
                            ub.SubscribedSiteID ,
                            bulist.SiteDescription AS Facility
                    FROM    #tempUserBUID ub
                            JOIN #tempBUList bulist ON ub.SubscribedSiteID = bulist.SiteID
                    WHERE   ub.ParentSite = @ParentSite
                            AND ub.EmailRecipientGrouping = 'HQ'
                    ORDER BY bulist.SiteDescription FOR READ ONLY;
                OPEN FacilityList;
                FETCH FacilityList INTO @SubscribedSiteID, @FacilityName;
                WHILE ( @@FETCH_STATUS = 0 )
                    BEGIN
                        SET @body_message = @body_message + @FacilityName
                            + N' Department(s): <ul>';
			
                        -- get list of departments for the current @FacilityName

                        DECLARE @Department VARCHAR(50);
                        DECLARE DepartmentList CURSOR
                        FOR
                            SELECT DISTINCT
                                    Department
                            FROM    #tempBUDeptList
                            WHERE   SiteID = @SubscribedSiteID
                            ORDER BY Department FOR READ ONLY;
                        OPEN DepartmentList;
                        FETCH DepartmentList INTO @Department;
                        WHILE ( @@FETCH_STATUS = 0 )
                            BEGIN
                                SET @body_message = @body_message + N'<li>'
                                    + @Department + '</li>';
                                FETCH DepartmentList INTO @Department;
                            END;
                        CLOSE DepartmentList;
                        DEALLOCATE DepartmentList;
                        SET @body_message = @body_message + N'</ul>';
                        FETCH FacilityList INTO @SubscribedSiteID,
                            @FacilityName;
                    END;
                CLOSE FacilityList;
                DEALLOCATE FacilityList;
                SET @body_message = @body_message + N'</blockquote>'
                    + N'<p>In each of the departments listed above, one or more of your Enovate Medical carts reported less than 20% utilization yesterday. This email is informational only. No specific action is required.</p>'
                    + N'<p>&nbsp;</p>'
                    + N'<p class=fyi>You received this email because you are currently subscribed to the '''
                    + @Subscribing_DisplayText
                    + ''' notification in CAST. <a href="https://cast.myenovate.com/optionalnotifications.aspx">Click here</a> to manage your notification subscriptions.</p>'
                    + N'<p>&nbsp;</p>' + N'<p>&nbsp;</p>';
                IF ( @Recipients <> NULL )
                    BEGIN			
                        -- build recipients list (for the current parentSite)
                        --	insert into EmailQueue ([Subject], ToAddresses, Body, Attempts, FromAddress, FromName)
                        SELECT  @subject ,
                                @Recipients ,
                                @body_message ,
                                0 ,
                                @FromAddress ,
                                @FromName; -- set attempts to 99 so the standard email processing app won't touch these rows
                    END;
                FETCH ParentSiteCursor INTO @ParentSite;
            END;
        CLOSE ParentSiteCursor;
        DEALLOCATE ParentSiteCursor;
	
        /* ------------------------------------------------------------  */

        /* This part is for Facility users only.                         */

        /* ------------------------------------------------------------  */

        DECLARE @SubscribedSiteID2 INT;
        DECLARE SubscribedSiteCursor CURSOR
        FOR
            SELECT DISTINCT
                    SubscribedSiteID
            FROM    #tempUserBUID
            WHERE   EmailRecipientGrouping = 'Facility' FOR READ ONLY;
        OPEN SubscribedSiteCursor;
        FETCH SubscribedSiteCursor INTO @SubscribedSiteID2;
        WHILE ( @@FETCH_STATUS = 0 )
            BEGIN
                DECLARE @Recipients2 VARCHAR(MAX);
                DECLARE @RecipientsXML2 VARCHAR(MAX);
                SET @RecipientsXML2 = ( SELECT  CAST(( SELECT u.Email
                                                       FROM   dbo.[User] u
                                                              JOIN ( SELECT
                                                              *
                                                              FROM
                                                              dbo.AlertSubscription
                                                              WHERE
                                                              SiteID = @SubscribedSiteID2
                                                              AND AlertSubscribeId = @AlertSubscribeId
                                                              ) s ON u.IDUser = s.UserID
                                                       WHERE  -- suppressed for testing (will be okay, even if it remains suppressed - it's just an additional safety net)
                                                      --u.SiteID = @ParentSite
                                                      --and 
                                                              s.UserID IN (
                                                              SELECT
                                                              User_ROW_ID
                                                              FROM
                                                              dbo.UserOptions
                                                              WHERE
                                                              OptionKey = 'CAN_LOGIN_TO_CAST'
                                                              AND OptionValue = 1 )
                                                     FOR
                                                       XML PATH('Recipients') ,
                                                           TYPE
                                                     ) AS NVARCHAR(MAX))
                                      );
                SET @Recipients2 = ( SELECT REPLACE(REPLACE(REPLACE(REPLACE(@RecipientsXML2,
                                                              '<Recipients>',
                                                              ''),
                                                              '</Recipients>',
                                                              ''), '<Email>',
                                                            ''), '</Email>',
                                                    '; ')
                                   );

                -- build the email content
                SET @body_message = N'<style>body {padding: 20px; background-color: #ffffff; color: #ccccc; font-family: segoe ui, Arial, Helvetica, sans-serif;} h4 {margin-bottom: 2px;} '
                    + N' h5 {margin-bottom: 2px;} .fyi {font-size: smaller;} '
                    + N'</style> ' + N'<h4>' + @bodyheader + '</h4> '
                    + N'<h5>Applies to:</h5>' + N'<blockquote>';
		
                -- get list of facilities for the current @ParentSite

                DECLARE @FacilityName2 VARCHAR(100);
                DECLARE FacilityList2 CURSOR
                FOR
                    SELECT DISTINCT
                            bulist.SiteDescription AS Facility
                    FROM    #tempUserBUID ub
                            JOIN #tempBUList bulist ON ub.SubscribedSiteID = bulist.SiteID
                    WHERE   ub.SubscribedSiteID = @SubscribedSiteID2
                            AND ub.EmailRecipientGrouping = 'Facility'
                    ORDER BY bulist.SiteDescription FOR READ ONLY;
                OPEN FacilityList2;
                FETCH FacilityList2 INTO @FacilityName2;
                WHILE ( @@FETCH_STATUS = 0 )
                    BEGIN
                        SET @body_message = @body_message + @FacilityName2
                            + N' Department(s): <ul>';
			
                        -- get list of departments for the current @FacilityName

                        DECLARE @Department2 VARCHAR(50);
                        DECLARE DepartmentList2 CURSOR
                        FOR
                            SELECT DISTINCT
                                    Department
                            FROM    #tempBUDeptList
                            WHERE   SiteID = @SubscribedSiteID2
                            ORDER BY Department FOR READ ONLY;
                        OPEN DepartmentList2;
                        FETCH DepartmentList2 INTO @Department2;
                        WHILE ( @@FETCH_STATUS = 0 )
                            BEGIN
                                SET @body_message = @body_message + N'<li>'
                                    + @Department2 + '</li>';
                                FETCH DepartmentList2 INTO @Department2;
                            END;
                        CLOSE DepartmentList2;
                        DEALLOCATE DepartmentList2;
                        SET @body_message = @body_message + N'</ul>';
                        FETCH FacilityList2 INTO @FacilityName2;
                    END;
                CLOSE FacilityList2;
                DEALLOCATE FacilityList2;
                SET @body_message = @body_message + N'</blockquote>'
                    + N'<p>In each of the departments listed above, one or more of your Enovate Medical carts reported less than 20% utilization yesterday. This email is informational only. No specific action is required.</p>'
                    + N'<p>&nbsp;</p>'
                    + N'<p class=fyi>You received this email because you are currently subscribed to the '''
                    + @Subscribing_DisplayText
                    + ''' notification in CAST. <a href="https://cast.myenovate.com/optionalnotifications.aspx">Click here</a> to manage your notification subscriptions.</p>'
                    + N'<p>&nbsp;</p>' + N'<p>&nbsp;</p>';
                IF ( @Recipients2 <> NULL )
                    BEGIN
                        -- build recipients list (for the current parentSite)
                        --insert into EmailQueue ([Subject], ToAddresses, Body, Attempts, FromAddress, FromName)
                        SELECT  @subject ,
                                @Recipients2 ,
                                @body_message ,
                                0 ,
                                @FromAddress ,
                                @FromName; -- set attempts to 99 so the standard email processing app won't touch these rows
                    END;
                FETCH SubscribedSiteCursor INTO @SubscribedSiteID2;
            END;
        CLOSE SubscribedSiteCursor;
        DEALLOCATE SubscribedSiteCursor;
        DECLARE @JobCode VARCHAR(20);
        SET @JobCode = 'ONLOWUTIL';
        UPDATE  dbo.Job
        SET     LastRanTimestampUTC = GETUTCDATE()
        WHERE   JobCode = @JobCode;
	
	
        /*Cleanup*/

        DROP TABLE #tempUserBUID;
        DROP TABLE #tempLowUtilization;
        DROP TABLE #tempBUList;
        DROP TABLE #tempBUDeptList;
    END;


GO
