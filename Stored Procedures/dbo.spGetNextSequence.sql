SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetNextSequence]
AS
    BEGIN
        SELECT  NEXT VALUE  FOR
                dbo.AccessPointBatch AS BatchID;
    END;

GO
