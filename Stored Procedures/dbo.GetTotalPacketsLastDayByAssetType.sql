SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GetTotalPacketsLastDayByAssetType]
AS
    SELECT  COUNT(*) AS TotalPackets ,
            b.IsSessionData ,
            a.Category ,
            a.Description
    FROM    ( SELECT --TOP 1000
                        sd.DeviceSerialNumber ,
                        sd.CreatedDateUTC ,
                        CAST(1 AS BIT) AS IsSessionData ,
                        sd.DeviceType ,
                        DATEADD(HOUR, DATEDIFF(HOUR, GETUTCDATE(), GETDATE()),
                                sd.CreatedDateUTC) CreatedDateLocal
              FROM      dbo.SessionDataCurrent sd --Current sd
              WHERE     DATEDIFF(DAY, sd.CreatedDateUTC, GETUTCDATE()) <= 1
              UNION ALL
              SELECT --TOP 1000
                        sd.DeviceSerialNumber ,
                        sd.CreatedDateUTC ,
                        CAST(0 AS BIT) AS IsSessionData ,
                        sd.DeviceType ,
                        DATEADD(HOUR, DATEDIFF(HOUR, GETUTCDATE(), GETDATE()),
                                sd.CreatedDateUTC) CreatedDateLocal
              FROM      dbo.NonSessionDataCurrent sd --Current sd
              WHERE     DATEDIFF(DAY, sd.CreatedDateUTC, GETUTCDATE()) <= 1
            ) AS b
            INNER JOIN dbo.AssetType AS a ON b.DeviceType = a.IDAssetType
    GROUP BY a.Category ,
            a.Description ,
            b.IsSessionData;
GO
