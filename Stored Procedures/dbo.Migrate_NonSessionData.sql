SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[Migrate_NonSessionData] ( @BatchSize INT = 5000 )
AS
    DECLARE @dateFloor DATETIME2;
    DECLARE @dateCalc DATETIME2;
    DECLARE @MaxCreatedDateOnHistorical DATETIME2; 
    DECLARE @MinCreatedDateOnCurrent DATETIME2;
    DECLARE @BatchEndPoint DATETIME2;
    DECLARE @BatchStartTime DATETIME2;
    DECLARE @BatchEndTime DATETIME2;
    DECLARE @BatchRowCount BIGINT;


		---------------------------------------------------
		-- set this to the number of days you want to keep (ie. -365 for one year, -120, -90, -30, etc. )
		---------------------------------------------------
    DECLARE @daysToKeep INT;
    SET @daysToKeep = 14; --(4 months plus buffer)
		
		----------------------------------------------------------------------------------------------------------------

    SET @dateCalc = DATEADD(DAY, ( @daysToKeep * -1 ), GETUTCDATE());

		-- @dateFloor becomes the date that is the start of the day for the day found above. 
		-- This way we remove records by full days.
    SET @dateFloor = CONVERT(DATETIME, ( CONVERT(VARCHAR(10), @dateCalc, 111) ));
    SET @MaxCreatedDateOnHistorical = ( SELECT  MAX(CreatedDateUTC)
                                        FROM    PulseHistorical.dbo.NonSessionData
                                      );


		 
		--LOOP : this starts looping through and deleting records by how many we set for each pass until we delete all records below the date floor
    WHILE EXISTS ( SELECT TOP 1
                            *
                   FROM     pulse.dbo.NonSessionDataCurrent
                   WHERE    ( CreatedDateUTC < @dateFloor
                              AND CreatedDateUTC > @MaxCreatedDateOnHistorical
                            )
                            OR CreatedDateUTC < DATEADD(DAY, -18, GETUTCDATE()) )
        BEGIN

			
			---------------------------------------------------
			-- Do the deletion from the table
			-- Set how many records to delete on each pass using the TOP command
			-- A low number prevents out of control transaction log growth.
			---------------------------------------------------
            BEGIN TRANSACTION; 
            SELECT  @MinCreatedDateOnCurrent = MIN(sub.CreatedDateUTC) ,
                    @BatchEndPoint = MAX(sub.CreatedDateUTC) ,
                    @BatchStartTime = GETUTCDATE()
            FROM    ( SELECT TOP ( @BatchSize )
                                *
                      FROM      pulse.dbo.NonSessionDataCurrent
                      WHERE     CreatedDateUTC > @MaxCreatedDateOnHistorical
                                AND CreatedDateUTC < @dateFloor
                      ORDER BY  CreatedDateUTC ASC
                    ) sub; 
		     
	
            INSERT  INTO PulseHistorical.dbo.NonSessionData
                    ( [DeviceSerialNumber] ,
                      [BatterySerialNumber] ,
                      [DeviceType] ,
                      [Activity] ,
                      [Bay] ,
                      [FullChargeCapacity] ,
                      [Voltage] ,
                      [Amps] ,
                      [Temperature] ,
                      [ChargeLevel] ,
                      [CycleCount] ,
                      [MaxCycleCount] ,
                      [VoltageCell1] ,
                      [VoltageCell2] ,
                      [VoltageCell3] ,
                      [FETStatus] ,
                      [RemainingTime] ,
                      [BatteryName] ,
                      [Efficiency] ,
                      [ControlBoardRevision] ,
                      [LCDRevision] ,
                      [HolsterChargerRevision] ,
                      [DCBoardOneRevision] ,
                      [DCBoardTwoRevision] ,
                      [WifiFirmwareRevision] ,
                      [BayWirelessRevision] ,
                      [Bay1ChargerRevision] ,
                      [Bay2ChargerRevision] ,
                      [Bay3ChargerRevision] ,
                      [Bay4ChargerRevision] ,
                      [MedBoardRevision] ,
                      [BackupBatteryVoltage] ,
                      [BackupBatteryStatus] ,
                      [IsAC] ,
                      [DCUnit1AVolts] ,
                      [DCUnit1ACurrent] ,
                      [DCUnit1BVolts] ,
                      [DCUnit1BCurrent] ,
                      [DCUnit2AVolts] ,
                      [DCUnit2ACurrent] ,
                      [DCUnit2BVolts] ,
                      [DCUnit2BCurrent] ,
                      [XValue] ,
                      [XMax] ,
                      [YValue] ,
                      [YMax] ,
                      [ZValue] ,
                      [ZMax] ,
                      [Move] ,
                      [BatteryStatus] ,
                      [SafetyStatus] ,
                      [PermanentFailureStatus] ,
                      [PermanentFailureAlert] ,
                      [BatteryChargeStatus] ,
                      [BatterySafetyAlert] ,
                      [BatteryOpStatus] ,
                      [BatteryMode] ,
                      [DC1Error] ,
                      [DC1Status] ,
                      [DC2Error] ,
                      [DC2Status] ,
                      [MouseFailureNotification] ,
                      [KeyboardFailureNotification] ,
                      [WindowsShutdownNotification] ,
                      [LinkQuality] ,
                      [IP] ,
                      [DeviceMAC] ,
                      [APMAC] ,
                      [LastTransmissionStatus] ,
                      [AuthType] ,
                      [ChannelNumber] ,
                      [PortNumber] ,
                      [DHCP] ,
                      [WEPKey] ,
                      [PassCode] ,
                      [StaticIP] ,
                      [SSID] ,
                      [SparePinSwitch] ,
                      [ControlBoardSerialNumber] ,
                      [HolsterBoardSerialNumber] ,
                      [LCDBoardSerialNumber] ,
                      [DC1BoardSerialNumber] ,
                      [DC2BoardSerialNumber] ,
                      [MedBoardSerialNumber] ,
                      [BayWirelessBoardSerialNumber] ,
                      [Bay1BoardSerialNumber] ,
                      [Bay2BoardSerialNumber] ,
                      [Bay3BoardSerialNumber] ,
                      [Bay4BoardSerialNumber] ,
                      [MedBoardDrawerOpenTime] ,
                      [MedBoardDrawerCount] ,
                      [MedBoardMotorUp] ,
                      [MedBoardMotorDown] ,
                      [MedBoardUnlockCount] ,
                      [MedBoardAdminPin] ,
                      [MedBoardNarcPin] ,
                      [MedBoardUserPin1] ,
                      [MedBoardUserPin2] ,
                      [MedBoardUserPin3] ,
                      [MedBoardUserPin4] ,
                      [MedBoardUserPin5] ,
                      [GenericError] ,
                      [SessionRecordType] ,
                      [QueryStringID] ,
                      [SessionID] ,
                      [CommandCode] ,
                      [CreatedDateUTC] ,
                      [ExaminedDateUTC] ,
                      [SourceIPAddress] ,
                      [MeasuredVoltage] ,
                      [BatteryErrorCode] ,
                      [InsertedDateUTC] ,
                      [QueryStringBatchID] ,
                      [CheckedByRX] ,
                      [PacketType]
                    )
                    SELECT TOP ( @BatchSize )
                            [DeviceSerialNumber] ,
                            [BatterySerialNumber] ,
                            [DeviceType] ,
                            [Activity] ,
                            [Bay] ,
                            [FullChargeCapacity] ,
                            [Voltage] ,
                            [Amps] ,
                            [Temperature] ,
                            [ChargeLevel] ,
                            [CycleCount] ,
                            [MaxCycleCount] ,
                            [VoltageCell1] ,
                            [VoltageCell2] ,
                            [VoltageCell3] ,
                            [FETStatus] ,
                            [RemainingTime] ,
                            [BatteryName] ,
                            [Efficiency] ,
                            [ControlBoardRevision] ,
                            [LCDRevision] ,
                            [HolsterChargerRevision] ,
                            [DCBoardOneRevision] ,
                            [DCBoardTwoRevision] ,
                            [WifiFirmwareRevision] ,
                            [BayWirelessRevision] ,
                            [Bay1ChargerRevision] ,
                            [Bay2ChargerRevision] ,
                            [Bay3ChargerRevision] ,
                            [Bay4ChargerRevision] ,
                            [MedBoardRevision] ,
                            [BackupBatteryVoltage] ,
                            [BackupBatteryStatus] ,
                            [IsAC] ,
                            [DCUnit1AVolts] ,
                            [DCUnit1ACurrent] ,
                            [DCUnit1BVolts] ,
                            [DCUnit1BCurrent] ,
                            [DCUnit2AVolts] ,
                            [DCUnit2ACurrent] ,
                            [DCUnit2BVolts] ,
                            [DCUnit2BCurrent] ,
                            [XValue] ,
                            [XMax] ,
                            [YValue] ,
                            [YMax] ,
                            [ZValue] ,
                            [ZMax] ,
                            [Move] ,
                            [BatteryStatus] ,
                            [SafetyStatus] ,
                            [PermanentFailureStatus] ,
                            [PermanentFailureAlert] ,
                            [BatteryChargeStatus] ,
                            [BatterySafetyAlert] ,
                            [BatteryOpStatus] ,
                            [BatteryMode] ,
                            [DC1Error] ,
                            [DC1Status] ,
                            [DC2Error] ,
                            [DC2Status] ,
                            [MouseFailureNotification] ,
                            [KeyboardFailureNotification] ,
                            [WindowsShutdownNotification] ,
                            [LinkQuality] ,
                            [IP] ,
                            [DeviceMAC] ,
                            [APMAC] ,
                            [LastTransmissionStatus] ,
                            [AuthType] ,
                            [ChannelNumber] ,
                            [PortNumber] ,
                            [DHCP] ,
                            [WEPKey] ,
                            [PassCode] ,
                            [StaticIP] ,
                            [SSID] ,
                            [SparePinSwitch] ,
                            [ControlBoardSerialNumber] ,
                            [HolsterBoardSerialNumber] ,
                            [LCDBoardSerialNumber] ,
                            [DC1BoardSerialNumber] ,
                            [DC2BoardSerialNumber] ,
                            [MedBoardSerialNumber] ,
                            [BayWirelessBoardSerialNumber] ,
                            [Bay1BoardSerialNumber] ,
                            [Bay2BoardSerialNumber] ,
                            [Bay3BoardSerialNumber] ,
                            [Bay4BoardSerialNumber] ,
                            [MedBoardDrawerOpenTime] ,
                            [MedBoardDrawerCount] ,
                            [MedBoardMotorUp] ,
                            [MedBoardMotorDown] ,
                            [MedBoardUnlockCount] ,
                            [MedBoardAdminPin] ,
                            [MedBoardNarcPin] ,
                            [MedBoardUserPin1] ,
                            [MedBoardUserPin2] ,
                            [MedBoardUserPin3] ,
                            [MedBoardUserPin4] ,
                            [MedBoardUserPin5] ,
                            [GenericError] ,
                            [SessionRecordType] ,
                            [QueryStringID] ,
                            [SessionID] ,
                            [CommandCode] ,
                            [CreatedDateUTC] ,
                            [ExaminedDateUTC] ,
                            [SourceIPAddress] ,
                            [MeasuredVoltage] ,
                            [BatteryErrorCode] ,
                            [InsertedDateUTC] ,
                            [QueryStringBatchID] ,
                            [CheckedByRX] ,
                            [PacketType]
                    FROM    pulse.dbo.NonSessionDataCurrent
                    WHERE   CreatedDateUTC > @MaxCreatedDateOnHistorical
                            AND CreatedDateUTC < @dateFloor
                    ORDER BY CreatedDateUTC ASC;

            COMMIT TRANSACTION; 

            DELETE  FROM pulse.dbo.NonSessionDataCurrent
            WHERE   ROW_ID IN (
                    SELECT TOP ( @BatchSize )
                            ROW_ID
                    FROM    pulse.dbo.NonSessionDataCurrent
                    WHERE   ( CreatedDateUTC > @MaxCreatedDateOnHistorical
                              AND CreatedDateUTC < @dateFloor
                            )
                            OR CreatedDateUTC < DATEADD(DAY, -18, GETUTCDATE())
                    ORDER BY CreatedDateUTC ASC );
            SET @BatchRowCount = @@ROWCOUNT;
            SET @BatchEndTime = GETUTCDATE();  
            SET @MaxCreatedDateOnHistorical = ( SELECT  MAX(CreatedDateUTC)
                                                FROM    PulseHistorical.dbo.NonSessionData
                                              );
 
        END;	
        
			 --DELETE  FROM Pulse.dbo.nonsessiondata_Summary
				--WHERE   CreatedDateUTC >= @MinCreatedDateOnCurrent
				--	   AND CreatedDateUTC < @BatchEndPoint;
			 --DELETE  FROM Pulse.dbo.nonsessiondata_Alerts
				--WHERE   CreatedDateUTC >= @MinCreatedDateOnCurrent
				--	   AND CreatedDateUTC < @BatchEndPoint;



			 --INSERT INTO PulseHistorical.dbo.MigrationLog (SourceTableName, DestTableName, OldestRecord, NewestRecord, BeginTime, EndTime, NoOfRows)
				--VALUES ('Pulse.dbo.nonsessiondataCurrent', 'PulseHistorical.dbo.nonsessiondata', @MinCreatedDateOnCurrent, @BatchEndPoint, @BatchStartTime, @BatchEndTime, @BatchRowCount) 		  
			 --END TRY
			 --BEGIN CATCH
			 --    SELECT   
				--ERROR_NUMBER() AS ErrorNumber  
				--,ERROR_SEVERITY() AS ErrorSeverity  
				--,ERROR_STATE() AS ErrorState  
				--,ERROR_PROCEDURE() AS ErrorProcedure  
				--,ERROR_LINE() AS ErrorLine  
				--,ERROR_MESSAGE() AS ErrorMessage;  
  
			 --IF @@TRANCOUNT > 0  
				--ROLLBACK TRANSACTION;  
				--BREAK;
		  --END CATCH;  
		  --IF @@TRANCOUNT > 0  		  
			 --COMMIT TRANSACTION;
    --        END;
		  --ALTER INDEX ALL ON Pulse.dbo.nonsessiondataCurrent
		  --REORGANIZE
		  --ALTER INDEX ALL ON PulseHistorical.dbo.nonsessiondata
		  --REORGANIZE

GO
