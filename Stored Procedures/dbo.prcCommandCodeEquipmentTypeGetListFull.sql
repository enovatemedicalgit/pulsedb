SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create PROCEDURE [dbo].[prcCommandCodeEquipmentTypeGetListFull] 
AS
BEGIN
	SET NOCOUNT ON;
   
	SELECT ID as EquipmentId, Description as EquipmentDescription
	FROM DeviceType	
	WHERE Category <> 'NotForSale'	
	ORDER BY EquipmentDescription desc
END


GO
