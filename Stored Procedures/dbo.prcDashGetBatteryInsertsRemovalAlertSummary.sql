SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Bill Murray>
-- Create date:	<8/16/2016>
-- Description:	
-- =============================================
CREATE Procedure [dbo].[prcDashGetBatteryInsertsRemovalAlertSummary]
-- Add the parameters for the stored procedure here
    @SiteID INT ,
    @DepartmentID INT = NULL ,
    @AlertId INT = NULL
AS
    BEGIN
        SET NOCOUNT ON;
        SELECT  DATEDIFF(DAY, ac.CreatedDate, GETDATE()) AS alertDay ,
                ac.AlertConditionID ,
                SUM(ac.Count) alertCount ,
                a.SiteID
        FROM    dbo.AlertCount ac
                JOIN dbo.Assets a ON a.SerialNo = ac.BatterySerialNumber
        WHERE   a.SiteID = @SiteID
                AND ( a.DepartmentID = @DepartmentID
                      OR @DepartmentID IS NULL
                    )
                AND ac.AlertConditionID IN ( 90, 91 )
        GROUP BY a.SiteID ,
                ac.AlertConditionID ,
                DATEDIFF(DAY, ac.CreatedDate, GETDATE())
        ORDER BY DATEDIFF(DAY, ac.CreatedDate, GETDATE());
    END;

GO
