SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:      <ENOVATE\bill.murray,BILLMURRAY>
-- Create date: <12/6/2016> <3:05 PM>
-- Description: 
--				Get Asset type info   
--
-- 
-- =============================================
CREATE PROCEDURE [dbo].[prcGetAssetType] @SerialNo AS VARCHAR(50)
AS
     BEGIN
         SET NOCOUNT ON;
         SELECT dbo.AssetType.Description,
                dbo.AssetType.Category,
                dbo.Assets.SerialNo,
                dbo.Assets.SiteID,
                dbo.Assets.DepartmentID,
			 dbo.Assets.Wing,
			 dbo.Assets.[Floor] AS FloorDesc,
			 dbo.Assets.AssetNumber
         FROM dbo.Assets
              INNER JOIN dbo.AssetType ON dbo.Assets.IDAssetType = dbo.AssetType.IDAssetType
         WHERE SerialNo = @SerialNo;
     END;
GO
