SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Arlow Farrell
-- Description:	Updates AssetStatusID column on Assets table
-- =============================================
CREATE PROCEDURE [dbo].[spAssets_UpdateAssetStatusID]
AS
    BEGIN
        SET NOCOUNT ON;

	
        IF OBJECT_ID(N'tempdb..#tmpDevicePacket', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tmpDevicePacket;
            END;

        SELECT  DeviceSerialNumber ,
                COUNT(ROW_ID) AS PacketCount
        INTO    #tmpDevicePacket
        FROM    dbo.NonSessionDataCurrent
        WHERE   CreatedDateUTC > DATEADD(HOUR, -96, GETUTCDATE())
                AND COALESCE(DeviceSerialNumber, '') != ''
        GROUP BY DeviceSerialNumber;

        SELECT  MAX(DeviceSerialNumber) AS DeviceSerialNumber ,
                COUNT(ROW_ID) AS PacketCount ,
                BatterySerialNumber
        INTO    #tmpBatteryPacket
        FROM    dbo.SessionDataCurrent
        WHERE   CreatedDateUTC > DATEADD(HOUR, -96, GETUTCDATE())
                AND COALESCE(DeviceSerialNumber, '') != ''
        GROUP BY BatterySerialNumber ,
                CreatedDateUTC
        ORDER BY CreatedDateUTC DESC;



	--insert into #tmpDevicePacket (DeviceSerialNumber, PacketCount)
	--SELECT DeviceSerialNumber, count(ROW_ID) as PacketCount
	--from Sessions
	--where EndDateUTC > DATEADD(hour, -96, getutcdate())
	--and coalesce(DeviceSerialNumber, '') != ''
	--group by DeviceSerialNumber

	-- Statuses:
	-- ---------
	--         In service:	at least 10 packets received within the past 48 hrs (DeviceStatusID = 2)
	--	        Available:	sent at least 1 packet but fewer than 10 packets in past 48 hrs (DeviceStatusID = 1)
	--            Offline:	no packets received within past 48 hrs (DeviceStatusID = 0)
	--	 Not commissioned:	No packets have been received since the device left Stinger (DeviceStatusID = 3)
	


        UPDATE  dbo.Assets
        SET     AssetStatusID = 3; -- Not commissioned
	
        UPDATE  dbo.Assets
        SET     AssetStatusID = 0;
	
	
        UPDATE  dbo.Assets
        SET     AssetStatusID = 2 ,
                IsActive = 1
        FROM    ( SELECT    DeviceSerialNumber ,
                            SUM(PacketCount) AS PacketCount
                  FROM      #tmpDevicePacket
                  GROUP BY  DeviceSerialNumber
                  HAVING    SUM(PacketCount) >= 10
                ) activedevices
        WHERE   SerialNo = activedevices.DeviceSerialNumber;
	
	
        UPDATE  dbo.Assets
        SET     AssetStatusID = 1 ,
                IsActive = 1
        FROM    ( SELECT    DeviceSerialNumber ,
                            SUM(PacketCount) AS PacketCount
                  FROM      #tmpDevicePacket
                  GROUP BY  DeviceSerialNumber
                  HAVING    SUM(PacketCount) < 10
                ) activedevices
        WHERE   SerialNo = activedevices.DeviceSerialNumber;
	

        UPDATE  dbo.Assets
        SET     AssetStatusID = 2 ,
                IsActive = 1
        FROM    ( SELECT    BatterySerialNumber ,
                            SUM(PacketCount) AS PacketCount
                  FROM      #tmpBatteryPacket
                  GROUP BY  BatterySerialNumber
                  HAVING    SUM(PacketCount) >= 10
                ) activedevices
        WHERE   SerialNo = activedevices.BatterySerialNumber;
	
	
        UPDATE  dbo.Assets
        SET     AssetStatusID = 1 ,
                IsActive = 1
        FROM    ( SELECT    BatterySerialNumber ,
                            SUM(PacketCount) AS PacketCount
                  FROM      #tmpBatteryPacket
                  GROUP BY  BatterySerialNumber
                  HAVING    SUM(PacketCount) < 10
                ) activedevices
        WHERE   SerialNo = activedevices.BatterySerialNumber;

        UPDATE  dbo.Assets
        SET     DepartmentID = a.DepartmentID
        FROM    ( SELECT    DepartmentID ,
                            BatterySerialNumber
                  FROM      dbo.Assets
                            JOIN #tmpBatteryPacket ON Assets.SerialNo = DeviceSerialNumber
                ) a
        WHERE   SerialNo = a.BatterySerialNumber;




        IF OBJECT_ID(N'tempdb..#tmpDevicePacket', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tmpDevicePacket;
            END;


    END;


GO
