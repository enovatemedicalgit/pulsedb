SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 

 
create PROCEDURE [dbo].[prcCommandCodeGet] (@CommandCodeID int, @DeviceTypeID int)
AS
BEGIN
	SET NOCOUNT ON;

	select coalesce(Parameter1FriendlyName, '') as Parameter1FriendlyName
	from DeviceTypeCommandCode 
	where CommandCodeID = @CommandCodeID
	and DeviceTypeID = @DeviceTypeID

END
GO
