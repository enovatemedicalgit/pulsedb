SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcDepartmentsSelectBySite] @SiteID INT = 212
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  

    BEGIN TRAN;

    SELECT  [IDDepartment] ,
            [Description] ,
            [SiteID]
    FROM    dbo.Departments
    WHERE   ( [SiteID] = @SiteID ); 

    COMMIT;



GO
