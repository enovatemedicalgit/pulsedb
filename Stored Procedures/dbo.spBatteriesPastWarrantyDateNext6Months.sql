SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[spBatteriesPastWarrantyDateNext6Months]
AS
    BEGIN	
 --select * from (
        SELECT  ISNULL(s.SiteName, cus.CustomerName) AS Account ,
                ass.SerialNo ,
                'Past Warranty Date' AS 'Opportunity Type' ,
                wd.DurationYears AS 'Warranty Years' ,
                CONVERT(DATE, DATEADD(DAY,
                                      ISNULL(( SELECT   DurationDays
                                               FROM     dbo.WarrantyDuration
                                               WHERE    SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
                                             ), 5000),
                                      CAST('20' + SUBSTRING(ass.SerialNo, 8, 2)
                                      + '-' + SUBSTRING(ass.SerialNo, 10, 2)
                                      + '-01' AS DATETIME))) AS 'Expiration Date' ,
                ass.CycleCount ,
                ass.MaxCycleCount ,
                ISNULL(NULLIF(dbo.fnGetBatteryCapacity_Int(ass.SerialNo,
                                                              ass.FullChargeCapacity),
                              0), 0) AS 'Capacity Health' ,
                ass.FullChargeCapacity ,
                ass.LastPostDateUTC
        FROM    dbo.Assets ass
                LEFT JOIN dbo.Sites s ON s.IDSite = ass.SiteID
                LEFT JOIN dbo.Customers cus ON ass.SiteID = cus.IDCustomer
                LEFT JOIN dbo.WarrantyDuration wd ON wd.SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
        WHERE   ass.LastPostDateUTC > DATEADD(DAY, -20, GETDATE())
                AND ass.IDAssetType = 5
                AND GETDATE() > DATEADD(DAY,
                                        ISNULL(-180
                                               + ( SELECT   DurationDays
                                                   FROM     dbo.WarrantyDuration
                                                   WHERE    SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
                                                 ), 5000),
                                        CAST('20' + SUBSTRING(ass.SerialNo, 8,
                                                              2) + '-'
                                        + SUBSTRING(ass.SerialNo, 10, 2)
                                        + '-01 12:00:00.000' AS DATETIME))
                AND dbo.SerialNumberClassification(ass.SerialNo) = 1
                AND ass.MaxCycleCount <> 0
                AND ass.MaxCycleCount <> 10
                AND ass.SiteID NOT IN ( 4, 28, 1125 )
                AND ass.FullChargeCapacity > 0
                AND ass.Retired = 0
        ORDER BY s.SiteDescription; 
 --union all
 -- select isnull(s.SiteName,cus.CustomerName) as Account,SerialNo,'Past Warranty Cycle Count' as 'Opportunity Type',wd.DurationYears as 'Warranty Years', dateadd(day,isnull( (select durationdays  from warrantyduration where serialnumberprefix = left(ass.SerialNo,7)),5000),cast('20' + Substring(ass.Serialno,8,2) + '-' + Substring(ass.SerialNo,10,2) + '-01' as datetime) ) as 'Expiration Date',CycleCount,MaxCycleCount,FullChargeCapacity,LastPostDateUTC from Assets ass left join sites s on s.idsite = ass.siteid left join Customers cus on ass.SiteID = cus.idcustomer left join warrantyduration wd on wd.SerialNumberPrefix = left(ass.serialno,7) where IDAssetType=5 and CycleCount > MaxCycleCount and MaxCycleCount!=0 and MaxCycleCount is not null and MaxCycleCount <> 0 and maxcyclecount <> 10 and CycleCount <> 0 and CycleCount is not null and s.idsite not in (1125,28,4) order by s.SiteName, MaxCycleCount desc)
 --fullrpt	

    END;



GO
