SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[GetAllAssetsReport]
    @SiteID VARCHAR(20) = NULL ,
    @AssetSerialNumber VARCHAR(20) = NULL ,
    @RowLimit INT = 1000 ,
    @StartDate DATETIME = NULL ,
    @EndDate DATETIME = NULL ,
    @CustomerID VARCHAR(10) = NULL
    WITH RECOMPILE
AS
    BEGIN

        IF ( @RowLimit IS NULL )
            BEGIN
                SET @RowLimit = 400;
            END;
        DECLARE @sdate DATETIME = @StartDate;
        DECLARE @edate DATETIME = @EndDate;
        DECLARE @sCust INT = @CustomerID;
        DECLARE @sSite INT = @SiteID;
        IF ( @StartDate IS NULL )
            BEGIN
                SET @StartDate = DATEADD(DAY, -7, GETDATE());
                SET @EndDate = GETDATE();
            END;

        SET @sdate = @StartDate;
        SET @edate = @EndDate;


        IF ( @sCust IS NOT NULL
             AND LEN(@AssetSerialNumber) < 7
           )
            BEGIN
                SELECT TOP ( @RowLimit )
                        si.SiteDescription ,
                        Assets.Description ,
                        SerialNo ,
                        aio.ProductSerialNumber AS AIOSerial ,
                        aio2.DeviceSerialNo AS CartSerial ,
                        aio3.ProductSerialNumber AS DCMonitorSerial ,
                        LastPostDateUTC ,
                        de.Description AS DeptDescription ,
                        Wing ,
                        Floor ,
                        Other ,
                        AssetNumber ,
                        IsActive ,
                        Retired AS Retired ,
                        CycleCount ,
                        COALESCE(CAST(( CAST(FullChargeCapacity AS DECIMAL(7,
                                                              2))
                                        / CAST(23760 AS DECIMAL(7, 2)) * 100 ) AS DECIMAL(7,
                                                              2)), 0) AS CalcCapacity
                FROM    dbo.Assets
                        LEFT JOIN dbo.Sites si ON si.IDSite = Assets.SiteID
                        LEFT JOIN dbo.Departments de ON de.IDDepartment = DepartmentID
                        LEFT JOIN dbo.vwProductAIO aio ON aio.DeviceSerialNo = SerialNo
                        LEFT JOIN dbo.vwProductCart aio2 ON aio2.DeviceSerialNo = SerialNo
                        LEFT JOIN dbo.vwProductDCMonitor aio3 ON aio3.DeviceSerialNo = SerialNo
                WHERE   Assets.SiteID IN ( SELECT   IDSite
                                           FROM     dbo.Sites
                                           WHERE    CustomerID = @sCust )
                ORDER BY si.CustomerID ,
                        si.SiteDescription ,
                        LastPostDateUTC DESC;
            END;


    END;

GO
