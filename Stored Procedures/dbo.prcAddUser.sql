SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcAddUser]
    @Username VARCHAR(70) = NULL ,
    @FirstName VARCHAR(70) = NULL ,
    @Password NVARCHAR(400) = NULL ,
    @LastName VARCHAR(70) = NULL ,
    @SiteID VARCHAR(20) = NULL ,
    @SiteList VARCHAR(300) = NULL
AS
SET NOCOUNT ON;

DECLARE @UserSiteCount INT;
DECLARE @IDUser INT;
DECLARE @site_table TABLE (
		idx SMALLINT PRIMARY KEY IDENTITY(1, 1)
		,site_id INT
		);

INSERT INTO
    @site_table( Site_Id )
SELECT
      item 
from   
	 dbo.SplitStringToTable(@SiteList,',')

SET @IDUser = (
		SELECT TOP 1 U.IDUser
		FROM dbo.[User] AS U
		WHERE U.UserName = @Username
			OR U.Email = @Username
		);

IF (@IDUser IS NULL)
BEGIN

	INSERT INTO dbo.[User] (
		CreatedDate
		,CreatedDateUTC
		,[Password]
		,UserName
		,Email
		,FirstName
		,LastName
		,IDSite
		)
	VALUES (
		GETDATE()
		,GETUTCDATE()
		,'Xyen2O4zJg0qBMPCEHQBTw=='
		,@Username
		,@Username
		,@FirstName
		,@LastName
		,CAST(@SiteID AS INT)
		);

	SET @IDUser = (
			SELECT TOP 1 U.IDUser
			FROM dbo.[User] AS U
			WHERE U.UserName = @Username
				OR U.Email = @Username
			);

	INSERT INTO dbo.USEROPTIONS (
		User_ROW_ID
		,OptionKey
	    ,OptionValue
		)
	VALUES (
		@IDUser
		,-- User_ROW_ID - int
		'CAN_LOGIN'
		,-- OptionKey - varchar(50)
		1 -- OptionValue - bit
		)

	INSERT INTO dbo.USEROPTIONS (
		User_ROW_ID
		,OptionKey
		,OptionValue
		)
	VALUES (
		@IDUser
		,-- User_ROW_ID - int
		'CAN_LOGIN_TO_PULSE'
		,-- OptionKey - varchar(50)
		1 -- OptionValue - bit
		)

	INSERT INTO dbo.USEROPTIONS (
		User_ROW_ID
		,OptionKey
		,OptionValue
		)
	VALUES (
		@IDUser
		,-- User_ROW_ID - int
		'CAN_MAINTAIN_USERS'
		,-- OptionKey - varchar(50)
		1 -- OptionValue - bit
		)

	INSERT INTO dbo.USEROPTIONS (
		User_ROW_ID
		,OptionKey
		,OptionValue
		)
	VALUES (
		@IDUser
		,-- User_ROW_ID - int
		'CAN_MAINTAIN_DEVICES'
		,-- OptionKey - varchar(50)
		1 -- OptionValue - bit
		)


END;
ELSE
BEGIN

	UPDATE dbo.[User]
	SET UserName = @Username
		,Email = @Username
		,Password = @Password		
		--@SiteID = @SiteID ,
		,FirstName = @FirstName
		,LastName = @LastName
	WHERE IDUser = @IDUser;
END


SET @UserSiteCount = (
		SELECT COUNT(User_ROW_ID)
		FROM dbo.UserSites
		WHERE User_ROW_ID = @IDUser
		);


IF (@UserSiteCount > 0)
	DELETE
	FROM dbo.UserSites
	WHERE User_ROW_ID = @IDUser;


INSERT INTO dbo.UserSites (
	   SiteID
	   ,User_ROW_ID
	   )
Select
	    site_id
	   ,@IDUser
	   From @Site_Table -- User_ROW_ID - int
	  

GO
