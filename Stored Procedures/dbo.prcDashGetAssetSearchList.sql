SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
	-- =============================================
-- Author:      <ENOVATE\arlow.farrell,ARLOWFARRELL>
-- Create date: <10/28/2016> <11:37 AM>
-- Description: 
--				   
--
-- 
-- =============================================
CREATE PROCEDURE [dbo].[prcDashGetAssetSearchList]
 @siteId AS INT = null
AS
 
    BEGIN
    SELECT a.SerialNo,A.IP,a.AssetNumber,a.IDAssetType,a.DeviceMAC,a.Notes,a.Other FROM dbo.Assets AS A WHERE a.SiteID = @siteId;
    END
GO
