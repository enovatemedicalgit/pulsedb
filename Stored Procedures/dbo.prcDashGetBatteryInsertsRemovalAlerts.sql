SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Bill Murray>
-- Create date:	<8/16/2016>
-- Description:	
-- =============================================
CREATE Procedure [dbo].[prcDashGetBatteryInsertsRemovalAlerts]
-- Add the parameters for the stored procedure here
    @SiteID INT ,
    @DepartmentID INT = NULL ,
    @TimePeriod INT = NULL
AS
    BEGIN
        SET NOCOUNT ON;
        SELECT  ac.CreatedDate ,
                ac.BatterySerialNumber ,
                ac.DeviceSerialNumber ,
                ac.AlertConditionID ,
                ac.Count ,
                ac.AlertCreated ,
                a.SiteID ,
                a.DepartmentID ,
                a.IDAssetType
        FROM    dbo.AlertCount ac
                JOIN dbo.Assets a ON a.SerialNo = ac.BatterySerialNumber
        WHERE   a.SiteID = @SiteID
                AND ( a.DepartmentID = @DepartmentID
                      OR @DepartmentID IS NULL
                    )
                AND ac.AlertConditionID IN ( 90, 91 )
        ORDER BY ac.CreatedDateUTC DESC;
    END;


GO
