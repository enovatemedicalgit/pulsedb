SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*################################################################################################
 Name				: Update_AssetsFromMostRecentPackets
 Date				: 10-03-2016
 Author				: christopher.stewart
 Company				: enovate 
 Purpose				: Update the assets table using information from incoming packets
					  Replaces inline script from IsPac
 Usage				:
 Impact				:
 Required Perm			:   
 Called by			:   
##################################################################################################
 ver		author					date			change  
 1.0		Christopher.Stewart			10032016		Replaces inline script from SSIS Package
#################################################################################################*/

/*##################################################################################################
 STEPS	  Description						 
 1		  Insert Most Recent packets into tempDB
 2		  Update statistics for battery type devices
 3		  Update statistics for workstation and charger type devices
 4		  Update SiteId for all assets, except those where UseIp = 0
 5		  Update SiteId for all assets, except those with UseAp = 0
 6		  Update SiteId enovate assets, and those with UseIP and UseAp Both = 0
 7		  Update Wing, Floor, SiteWingId and SiteFloorID
 8		  Update Department
 9		  Update Department	  
 10		  Update SiteId For Batteries
 11		  Update Retired Flag
 12		  Update Department for
 13		  Clean UP TempDB	  
#################################################################################################*/

CREATE PROCEDURE [dbo].[Update_AssetsFromMostRecentPacketsALF]
AS 
DECLARE @LapTimer AS DATETIME2
DECLARE @ProcessTimer AS DATETIME2 = GETDATE();


    IF OBJECT_ID('tempdb..#MostRecentPacketByDeviceByBattery') IS NOT NULL
        BEGIN
            DROP TABLE #MostRecentPacketByDeviceByBattery;
        END;



--PRINT dbo.GetMessageBorder();
--/*################################################################################################*/
--Step_1:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 1'; 
--		  RAISERROR('Insert Most Recent packets into tempDB',0,1) WITH NOWAIT;
							 
--/*################################################################################################*/


    CREATE TABLE #MostRecentPacketByDeviceByBattery
        (
          BatterySerialNumber VARCHAR(50) ,
          Bay1ChargerRevision VARCHAR(50) ,
          Bay2ChargerRevision VARCHAR(50) ,
          Bay3ChargerRevision VARCHAR(50) ,
          Bay4ChargerRevision VARCHAR(50) ,
          WifiFirmwareRevision VARCHAR(50) ,
          BayWirelessRevision VARCHAR(50) ,
          DeviceSerialNumber VARCHAR(50) ,
          MaxCycleCount INT ,
          APMac VARCHAR(50) ,
          CreatedDateUTC DATETIME ,
          ChargeLevel INT ,
          CycleCount INT ,
          FullChargeCapacity INT ,
          Temperature INT ,
          SourceIPAddress VARCHAR(20) ,
          IP VARCHAR(50) ,
          Bay VARCHAR(50) ,
          Activity INT ,
          DeviceType INT ,
          MOVE INT ,
          Amps DECIMAL(18, 2)
        );

--find mismatched devices
    INSERT  INTO #MostRecentPacketByDeviceByBattery
            ( BatterySerialNumber ,
              Bay1ChargerRevision ,
              Bay2ChargerRevision ,
              Bay3ChargerRevision ,
              Bay4ChargerRevision ,
              WifiFirmwareRevision ,
              BayWirelessRevision ,
              DeviceSerialNumber ,
              MaxCycleCount ,
              APMac ,
              CreatedDateUTC ,
              ChargeLevel ,
              CycleCount ,
              FullChargeCapacity ,
              Temperature ,
              SourceIPAddress ,
              IP ,
              Bay ,
              Activity ,
              DeviceType ,
              MOVE ,
              Amps
	        )
            SELECT  MostRecent.BatterySerialNumber ,
                    MostRecent.Bay1ChargerRevision ,
                    MostRecent.Bay2ChargerRevision ,
                    MostRecent.Bay3ChargerRevision ,
                    MostRecent.Bay4ChargerRevision ,
                    MostRecent.WifiFirmwareRevision ,
                    MostRecent.BayWirelessRevision ,
                    MostRecent.DeviceSerialNumber ,
                    MostRecent.MaxCycleCount ,
                    MostRecent.APMAC ,
                    MostRecent.CreatedDateUTC ,
                    MostRecent.ChargeLevel ,
                    MostRecent.CycleCount ,
                    MostRecent.FullChargeCapacity ,
                    MostRecent.Temperature ,
                    MostRecent.SourceIPAddress ,
                    MostRecent.IP ,
                    MostRecent.Bay ,
                    MostRecent.Activity ,
                    MostRecent.DeviceType ,
                    MostRecent.MOVE ,
                    MostRecent.Amps
            FROM    ( SELECT    nsd.BatterySerialNumber ,
                                ISNULL(nsd.Bay1ChargerRevision, '') AS Bay1ChargerRevision ,
                                ISNULL(nsd.Bay2ChargerRevision, '') AS Bay2ChargerRevision ,
                                ISNULL(nsd.Bay3ChargerRevision, '') AS Bay3ChargerRevision ,
                                ISNULL(nsd.Bay4ChargerRevision, '') AS Bay4ChargerRevision ,
                                ISNULL(nsd.WifiFirmwareRevision, '') AS WifiFirmwareRevision ,
                                ISNULL(nsd.BayWirelessRevision, '') AS BayWirelessRevision ,
                                nsd.DeviceSerialNumber ,
                                nsd.MaxCycleCount ,
                                nsd.APMAC ,
                                nsd.CreatedDateUTC ,
                                nsd.ChargeLevel ,
                                nsd.CycleCount ,
                                nsd.FullChargeCapacity ,
                                nsd.Temperature ,
                                nsd.SourceIPAddress AS SourceIPAddress ,
                                nsd.IP AS IP ,
                                nsd.Bay AS Bay ,
                                nsd.Activity AS Activity ,
                                nsd.DeviceType AS DeviceType ,
                                nsd.Move AS MOVE ,
                                nsd.Amps AS Amps ,
                                ROW_NUMBER() OVER ( PARTITION BY nsd.DeviceSerialNumber,
                                                    nsd.BatterySerialNumber ORDER BY nsd.CreatedDateUTC DESC ) AS RN
                      FROM      dbo.viewAllSessionData nsd WITH ( NOLOCK )
                      WHERE     nsd.CreatedDateUTC > DATEADD(hour, -5,
                                                             GETUTCDATE())
                    ) MostRecent
            WHERE   MostRecent.RN = 1
            ORDER BY MostRecent.CreatedDateUTC DESC;

--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_2:
		  --SET @LapTimer = GETDATE();
		  --PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 2';
		  --RAISERROR('Update statistics for battery type devices',0,1) WITH NOWAIT;

--/*################################################################################################*/

    UPDATE  dbo.Assets
    SET     LastPostDateUTC = t.CreatedDateUTC ,
            Temperature = t.Temperature ,
            AssetStatusID = 1 ,
            IsActive = 1 ,
            Activity = ISNULL(t.Activity, a.Activity) ,
            LastPacketType = IIF(t.DeviceType IN ( 1, 6 ), 1, 2) ,
            SiteID = ISNULL(( SELECT TOP 1
                                        SiteID
                              FROM      dbo.Assets
                              WHERE     SerialNo = t.DeviceSerialNumber
                            ), a.SiteID) ,
            Retired = 0 ,
            OutOfService = 0 ,
            SourceIPAddress = t.SourceIPAddress ,
            IP = t.IP ,
            APMAC = ISNULL(t.APMac, a.APMAC) ,
            AccessPointId = ISNULL(( SELECT TOP 1
                                            ROW_ID
                                     FROM   dbo.AccessPoint
                                     WHERE  MACAddress = t.APMac
                                            AND SiteID = a.SiteID
                                     ORDER BY ModifiedDateUTC DESC ,
                                            Floor DESC ,
                                            DeviceCount DESC
                                   ), a.AccessPointId)
    FROM    dbo.Assets a
            LEFT JOIN assetsbattery ab ON a.IDAsset = ab.IDAsset
            INNER JOIN #MostRecentPacketByDeviceByBattery t ON a.SerialNo = t.BatterySerialNumber WHERE a.idassettype = 5;

    UPDATE  Assetsbattery
    SET     Assetsbattery.ChargeLevel = CASE WHEN t.ChargeLevel IS NOT NULL
                                                  AND t.ChargeLevel <> 0
                                             THEN t.ChargeLevel
                                             ELSE ab.ChargeLevel
                                        END ,
            Assetsbattery.FullChargeCapacity = CASE WHEN t.FullChargeCapacity IS NOT NULL
                                                         AND t.ChargeLevel > 90
                                                         AND ass.IDAssetType IN (
                                                         2, 4 )
                                                         AND t.FullChargeCapacity != 22000
                                                         AND t.FullChargeCapacity <> 0
                                                    THEN t.FullChargeCapacity
                                                    ELSE ab.FullChargeCapacity
                                               END ,
            Assetsbattery.CycleCount = CASE WHEN t.CycleCount IS NOT NULL
                                                 AND t.CycleCount <> 0
                                            THEN t.CycleCount
                                            ELSE ab.CycleCount
                                       END ,
            Assetsbattery.MaxCycleCount = CASE WHEN t.MaxCycleCount IS NOT NULL
                                                    AND t.MaxCycleCount <> 0
                                               THEN t.MaxCycleCount
                                               ELSE ab.MaxCycleCount
                                          END
    FROM    Assetsbattery ab
            INNER JOIN dbo.Assets ass ON ab.idasset = ass.IDAsset
            INNER JOIN #MostRecentPacketByDeviceByBattery t ON ass.SerialNo = t.BatterySerialNumber WHERE ass.IDAssetType = 5;


--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_3:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 3';
--		  RAISERROR('Update statistics for workstation and charger type devices',0,1) WITH NOWAIT;

--/*################################################################################################*/


    UPDATE  dbo.Assets
    SET     Temperature = t.Temperature ,
            AssetStatusID = 1 ,
            IsActive = 1 ,
            WiFiFirmwareRevision = ISNULL(t.WifiFirmwareRevision,
                                          a.WiFiFirmwareRevision) ,
            Retired = 0 ,
            OutOfService = 0 ,
            SourceIPAddress = t.SourceIPAddress ,
            LastPostDateUTC = t.CreatedDateUTC ,
            IP = t.IP ,
            APMAC = ISNULL(t.APMac, a.APMAC) ,
            Amps = ISNULL(t.Amps, a.Amps) ,
            Activity = ISNULL(t.Activity, a.Activity) ,
            AccessPointId = ISNULL(( SELECT TOP 1
                                            ROW_ID
                                     FROM   dbo.AccessPoint
                                     WHERE  MACAddress = t.APMac
                                            AND SiteID = a.SiteID
                                     ORDER BY ModifiedDateUTC DESC ,
                                            Floor DESC ,
                                            DeviceCount DESC
                                   ), a.AccessPointId)
    FROM    dbo.Assets a
            INNER JOIN #MostRecentPacketByDeviceByBattery t ON a.SerialNo = t.DeviceSerialNumber WHERE a.idassettype IN (1,2,3,4,6);


    UPDATE  assetscharger
    SET     assetscharger.BayCount = IIF(t.Bay > 2, 4, a.BayCount) ,
            assetscharger.Bay1LastPostDateUTC = IIF(t.BatterySerialNumber IS NOT NULL
            AND ISNULL(t.Bay, 0) = 1, GETUTCDATE(), a.Bay1LastPostDateUTC) ,
            assetscharger.Bay2LastPostDateUTC = IIF(t.BatterySerialNumber IS NOT NULL
            AND ISNULL(t.Bay, 0) = 2, GETUTCDATE(), a.Bay1LastPostDateUTC) ,
            assetscharger.Bay3LastPostDateUTC = IIF(t.BatterySerialNumber IS NOT NULL
            AND ISNULL(t.Bay, 0) = 3, GETUTCDATE(), a.Bay1LastPostDateUTC) ,
            assetscharger.Bay4LastPostDateUTC = IIF(t.BatterySerialNumber IS NOT NULL
            AND ISNULL(t.Bay, 0) = 4, GETUTCDATE(), a.Bay1LastPostDateUTC) ,
            assetscharger.Bay1ChargerRevision = IIF(t.Bay1ChargerRevision IS NOT NULL
            AND ISNULL(t.Bay, 0) = 1, t.Bay1ChargerRevision, a.Bay1ChargerRevision) ,
            assetscharger.Bay2ChargerRevision = IIF(t.Bay2ChargerRevision IS NOT NULL
            AND ISNULL(t.Bay, 0) = 2, t.Bay2ChargerRevision, a.Bay2ChargerRevision) ,
            assetscharger.Bay3ChargerRevision = IIF(t.Bay3ChargerRevision IS NOT NULL
            AND ISNULL(t.Bay, 0) = 3, t.Bay3ChargerRevision, a.Bay3ChargerRevision) ,
            assetscharger.Bay4ChargerRevision = IIF(t.Bay4ChargerRevision IS NOT NULL
            AND ISNULL(t.Bay, 0) = 4, t.Bay4ChargerRevision, a.Bay4ChargerRevision)
    FROM    AssetsCharger a
            INNER JOIN dbo.Assets ass ON ass.IDAsset = a.idasset
            INNER JOIN #MostRecentPacketByDeviceByBattery t ON ass.SerialNo = t.DeviceSerialNumber
    WHERE   ass.IDAssetType IN ( 2, 4 );


    UPDATE  assetsworkstation
    SET     assetsworkstation.MOVE = t.MOVE
    FROM    assetsworkstation a
            LEFT JOIN dbo.Assets ass ON ass.IDAsset = a.idasset
            INNER JOIN #MostRecentPacketByDeviceByBattery t ON ass.SerialNo = t.DeviceSerialNumber
    WHERE   ass.IDAssetType IN ( 1, 6 );

--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_4:
		  SET @LapTimer = GETDATE();
		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 4';
		  RAISERROR('Update SiteId for all assets, except those where UseIp = 0',0,1) WITH NOWAIT;

--/*################################################################################################*/


    UPDATE  dbo.Assets
    SET     SiteID = ISNULL(( SELECT TOP 1
                                        sip.SiteID
                              FROM      dbo.SiteIPSubnet sip
                              WHERE     sip.SourceIPAddressBase = LEFT(t.SourceIPAddress,
                                                              LEN(t.SourceIPAddress)
                                                              - CHARINDEX('.',
                                                              REVERSE(t.SourceIPAddress)))
                                        AND sip.LANIPAddressBase = LEFT(t.IP,
                                                              LEN(t.IP)
                                                              - CHARINDEX('.',
                                                              REVERSE(t.IP)))
                                        AND sip.FacilityReliabilityRating > 70
                            ), a.SiteID)
    FROM    dbo.Assets a
            INNER JOIN #MostRecentPacketByDeviceByBattery t ON a.SerialNo = t.DeviceSerialNumber
                                                              AND a.SiteID NOT IN (
                                                              SELECT
                                                              SiteID
                                                              FROM
                                                              dbo.SiteAllocationExclusion
                                                              WHERE
                                                              UseIP = 0 )
                                                              AND ISNULL(a.ManualAllocation,
                                                              0) = 0;


--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_5:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 5';
--		  RAISERROR('Update SiteId for all assets, except those with UseAp = 0',0,1) WITH NOWAIT;

--/*################################################################################################*/

    UPDATE  dbo.Assets
    SET     SiteID = ISNULL(( SELECT TOP 1
                                        sip.SiteID
                              FROM      dbo.SiteAPMAC sip
                              WHERE     sip.APMAC = t.APMAC
                                        AND sip.FacilityReliabilityRating > 70
                            ), a.SiteID)
    FROM    dbo.Assets a
            INNER JOIN #MostRecentPacketByDeviceByBattery t ON a.SerialNo = t.DeviceSerialNumber
                                                              AND a.SiteID NOT IN (
                                                              SELECT
                                                              SiteID
                                                              FROM
                                                              dbo.SiteAllocationExclusion
                                                              WHERE
                                                              UseAP = 0 )
                                                              AND ISNULL(a.ManualAllocation,
                                                              0) = 0;


--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_6:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 6';
--		  RAISERROR('Update SiteId enovate assets, and those with UseIP and UseAp Both = 0',0,1) WITH NOWAIT;

--/*################################################################################################*/


    UPDATE  dbo.Assets
    SET     SiteID = ISNULL(( SELECT TOP 1
                                        sip.SiteID
                              FROM      dbo.AccessPoint sip
                              WHERE     sip.MACAddress = t.APMac
                            ), a.SiteID)
    FROM    dbo.Assets a
            INNER JOIN #MostRecentPacketByDeviceByBattery t ON a.SerialNo = t.DeviceSerialNumber
                                                              AND ( a.SiteID NOT IN (
                                                              SELECT
                                                              SiteID
                                                              FROM
                                                              dbo.SiteAllocationExclusion
                                                              WHERE
                                                              UseIP = 0
                                                              AND UseAP = 0 )
                                                              OR a.SiteID IN (
                                                              1125, 28 )
                                                              )
                                                              AND ISNULL(a.ManualAllocation,
                                                              0) = 0;


--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_7:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 7';
--		  RAISERROR('Update Wing',0,1) WITH NOWAIT;

--/*################################################################################################*/



--End of Site Allocation checks

--Now do wings

    UPDATE  dbo.Assets
    SET     Wing = ISNULL(( SELECT TOP 1
                                    sip.Wing
                            FROM    dbo.AccessPoint AS sip
                            WHERE   sip.MACAddress = t.APMac
                                    AND sip.Wing <> ''
                                    AND sip.Manual = 1
                            ORDER BY sip.DeviceCount DESC
                          ), a.Wing) ,
            Floor = ISNULL(( SELECT TOP 1
                                    sip.Floor
                             FROM   dbo.AccessPoint AS sip
                             WHERE  sip.MACAddress = t.APMac
                                    AND sip.Manual = 1
                                    AND sip.Floor <> ''
                             ORDER BY sip.Floor DESC
                           ), a.Floor) ,
            SiteWingID = ISNULL(( SELECT TOP 1
                                            sip.SiteWingId
                                  FROM      dbo.AccessPoint AS sip
                                  WHERE     sip.MACAddress = t.APMac
                                            AND sip.SiteWingId IS NOT NULL
                                            AND sip.Manual = 1
                                  ORDER BY  sip.DeviceCount DESC
                                ), a.SiteWingID) ,
            SiteFloorID = ISNULL(( SELECT TOP 1
                                            sip.SiteFloorID
                                   FROM     dbo.AccessPoint AS sip
                                   WHERE    sip.MACAddress = t.APMac
                                            AND sip.Manual = 1
                                            AND sip.SitefloorId IS NOT NULL
                                   ORDER BY sip.Floor DESC
                                 ), a.SiteFloorID)
    FROM    dbo.Assets a
            INNER JOIN #MostRecentPacketByDeviceByBattery t ON a.SerialNo = t.DeviceSerialNumber
                                                              AND ( a.SiteID NOT IN (
                                                              SELECT
                                                              SiteID
                                                              FROM
                                                              dbo.SiteAllocationExclusion
                                                              WHERE
                                                              UseAP = 0 )
                                                              OR a.SiteID IN (
                                                              1125, 28 )
                                                              )
                                                              AND ISNULL(a.ManualAllocation,
                                                              0) = 0;


--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_8:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 8';
--		  RAISERROR('Update Department based access points from manual scan',0,1) WITH NOWAIT;

--/*################################################################################################*/


    UPDATE  dbo.Assets
    SET     DepartmentID = ISNULL(( SELECT TOP 1
                                            sip.DepartmentID
                                    FROM    dbo.AccessPoint sip
                                    WHERE   sip.MACAddress = t.APMac
                                            AND sip.Manual = 1
                                            AND sip.DepartmentID != ''
                                            AND sip.DepartmentID NOT IN (
                                            SELECT  d.IDDepartment
                                            FROM    dbo.Departments d
                                            WHERE   d.SiteID = a.SiteID
                                                    AND d.DefaultForSite = 1 )
                                    ORDER BY sip.DeviceCount DESC
                                  ), a.DepartmentID)
    FROM    dbo.Assets a
            INNER JOIN #MostRecentPacketByDeviceByBattery t ON a.SerialNo = t.DeviceSerialNumber
                                                              AND ( a.SiteID NOT IN (
                                                              SELECT
                                                              SiteID
                                                              FROM
                                                              dbo.SiteAllocationExclusion
                                                              WHERE
                                                              UseAP = 0 )
                                                              OR a.SiteID IN (
                                                              1125, 28 )
                                                              )
                                                              AND ISNULL(a.ManualAllocation,
                                                              0) = 0
                                                              AND ( a.IDAssetType NOT IN (
                                                              2, 4 )
                                                              OR a.DepartmentID IN (
                                                              SELECT
                                                              d.IDDepartment
                                                              FROM
                                                              dbo.Departments d
                                                              WHERE
                                                              d.SiteID = a.SiteID
                                                              AND d.DefaultForSite = 1 )
                                                              );

--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_9:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 9';
--		  RAISERROR('Update Department based on access points without manual scan',0,1) WITH NOWAIT;

--/*################################################################################################*/

    UPDATE  dbo.Assets
    SET     DepartmentID = ISNULL(( SELECT TOP 1
                                            sip.DepartmentID
                                    FROM    dbo.AccessPoint sip
                                    WHERE   sip.MACAddress = t.APMac
                                            AND sip.Manual = 0
                                            AND sip.DepartmentID != ''
                                            AND sip.DepartmentID NOT IN (
                                            SELECT TOP 1
                                                    d.IDDepartment
                                            FROM    dbo.Departments d
                                            WHERE   d.SiteID = a.SiteID
                                                    AND d.DefaultForSite = 1
                                            ORDER BY sip.DeviceCount DESC )
                                  ), a.DepartmentID)
    FROM    dbo.Assets a
            INNER JOIN #MostRecentPacketByDeviceByBattery t ON a.SerialNo = t.DeviceSerialNumber
                                                              AND ( a.SiteID NOT IN (
                                                              SELECT
                                                              SiteID
                                                              FROM
                                                              dbo.SiteAllocationExclusion
                                                              WHERE
                                                              UseAP = 0 )
                                                              OR a.SiteID IN (
                                                              1125, 28 )
                                                              )
                                                              AND ISNULL(a.ManualAllocation,
                                                              0) = 0
                                                              AND ( a.IDAssetType NOT IN (
                                                              2, 4 )
                                                              OR a.DepartmentID IN (
                                                              SELECT TOP 1
                                                              d.IDDepartment
                                                              FROM
                                                              dbo.Departments d
                                                              WHERE
                                                              d.SiteID = a.SiteID
                                                              AND d.DefaultForSite = 1 )
                                                              );

--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_10:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 10';
--		  RAISERROR('Update SiteId For Batteries ',0,1) WITH NOWAIT;

--/*################################################################################################*/



    UPDATE  dbo.Assets
    SET     SiteID = ISNULL(( SELECT TOP 1
                                        SiteID
                              FROM      dbo.Assets
                              WHERE     SerialNo = t.DeviceSerialNumber
                            ), a.SiteID)
    FROM    dbo.Assets a
            INNER JOIN #MostRecentPacketByDeviceByBattery t ON a.SerialNo = t.BatterySerialNumber
    WHERE   ISNULL(a.ManualAllocation, 0) = 0;

		

--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_11:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 11';
--		  RAISERROR('Update Retired Flag ',0,1) WITH NOWAIT;

--/*################################################################################################*/



    UPDATE  dbo.Assets
    SET     Retired = 1
    WHERE   ( LastPostDateUTC < DATEADD(DAY, -29, GETUTCDATE())
              OR LastPostDateUTC IS NULL
            )
            AND CreatedDateUTC < DATEADD(MONTH, -3, GETUTCDATE());


--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_12:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 12';
--		  RAISERROR('Update to Default Department',0,1) WITH NOWAIT;

--/*################################################################################################*/
    UPDATE  dbo.Assets
    SET     DepartmentID = ( SELECT IDDepartment
                             FROM   dbo.Departments
                             WHERE  DefaultForSite = 1
                                    AND SiteID = asst.SiteID
                           )
    FROM    dbo.Assets asst
    WHERE   asst.SerialNo IN (
            SELECT  ass.SerialNo
            FROM    dbo.Assets ass
                    LEFT JOIN dbo.Sites s ON s.IDSite = ass.SiteID
                    LEFT JOIN dbo.Departments dep ON ass.DepartmentID = dep.IDDepartment
            WHERE   dep.SiteID <> ass.SiteID
                    AND ass.SiteID NOT IN ( 1125, 28 )
                    AND ass.IDAssetType <> 5 );



--PRINT dbo.GetLapTime(@LapTimer); 
--/*################################################################################################*/
--Step_13:
--		  SET @LapTimer = GETDATE();
--		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 13';
--		  RAISERROR('Clean up TempDB',0,1) WITH NOWAIT;

--/*################################################################################################*/

    IF OBJECT_ID('tempdb..#MostRecentPacketByDeviceByBattery') IS NOT NULL
        BEGIN
            DROP TABLE #MostRecentPacketByDeviceByBattery;
        END;

--PRINT dbo.GetLapTime(@LapTimer); 
--PRINT 'Combined Steps ' + dbo.GetLapTime(@ProcessTimer);
GO
