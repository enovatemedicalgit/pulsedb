SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spDashboardPulseWebALF]
    @SiteID INT = 633 ,
    @DepartmentIDList VARCHAR(200) = NULL
AS
    BEGIN
        SET NOCOUNT ON;

		
        IF ( LEN(@DepartmentIDList) < 2 )
            BEGIN
                SET @DepartmentIDList = NULL;
            END;		 

		

--insert into ErrorLog (Description,Source,CreatedDateUTC) values ( Cast(@SiteID as varchar(30)), 'SPDASHBOARDPULSEWEB PARAMS', GETUTCDATE())  
--- Workstations
        DECLARE @WorkstationsTotal INT;
        DECLARE @WorkstationsInService INT;
        DECLARE @WorkstationsInServiceWeek INT;
        DECLARE @WorkstationsAvailable INT;
        DECLARE @WorkstationsOffline INT;
        DECLARE @WorkstationAvgUtilization FLOAT;
        DECLARE @WorkstationPCAvgUtilization FLOAT;
        DECLARE @WorkstationsMoving INT;
        IF ( @DepartmentIDList IS NOT NULL )
            BEGIN
                SET @WorkstationsTotal = ( SELECT   COUNT(IDAsset) AS WorkstationsTotal
                                           FROM     dbo.Assets
                                           WHERE    SiteID = @SiteID
                                                    AND DepartmentID IN (
                                                    SELECT  item
                                                    FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                                    AND IDAssetType IN ( 1, 3,
                                                              6 )
                                                    AND Retired = 0
                                         );
                SET @WorkstationsInService = ( SELECT   COUNT(IDAsset) AS WorkstationsInService
                                               FROM     dbo.Assets
                                               WHERE    LastPostDateUTC > DATEADD(HOUR,
                                                              -1, GETUTCDATE())
                                                        AND ( Move > 0
                                                              OR Amps < -600
                                                            )
                                                        AND SiteID = @SiteID
                                                        AND DepartmentID IN (
                                                        SELECT
                                                              item
                                                        FROM  dbo.fnStringList2Table(@DepartmentIDList) )
                                                        AND IDAssetType IN ( 1,
                                                              3, 6 )
                                                        AND Retired = 0
                                             );
                SET @WorkstationsInServiceWeek = ( SELECT   COUNT(IDAsset) AS WorkstationsInService
                                                   FROM     dbo.Assets
                                                   WHERE    LastPostDateUTC > DATEADD(DAY,
                                                              -6, GETUTCDATE())
                                                            AND ( Move > 0
                                                              OR Amps < -600
                                                              )
                                                            AND SiteID = @SiteID
                                                            AND DepartmentID IN (
                                                            SELECT
                                                              item
                                                            FROM
                                                              dbo.fnStringList2Table(@DepartmentIDList) )
                                                            AND IDAssetType IN (
                                                            1, 3, 6 )
                                                            AND Retired = 0
                                                 );
                SET @WorkstationsAvailable = ( SELECT   ( COUNT(IDAsset)
                                                          - ( @WorkstationsInService ) ) AS WorkstationsAvailable
                                               FROM     dbo.Assets
                                               WHERE    LastPostDateUTC > DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                        AND SiteID = @SiteID
                                                        AND DepartmentID IN (
                                                        SELECT
                                                              item
                                                        FROM  dbo.fnStringList2Table(@DepartmentIDList) )
                                                        AND IDAssetType IN ( 1,
                                                              3, 6 )
                                                        AND Retired = 0
                                             );
                SET @WorkstationsOffline = ( SELECT ( COUNT(IDAsset) ) AS WorkstationsOffline
                                             FROM   dbo.Assets
                                             WHERE  LastPostDateUTC < DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                    AND SiteID = @SiteID
                                                    AND DepartmentID IN (
                                                    SELECT  item
                                                    FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                                    AND IDAssetType IN ( 1, 3,
                                                              6 )
                                                    AND Retired = 0
                                           ); 
                SET @WorkstationPCAvgUtilization = ( SELECT AVG(s.AvgEstimatedPCUtilization)
                                                     FROM   dbo.SummaryWorkstation s
                                                     WHERE  s.Date > DATEADD(DAY,
                                                              -3, GETUTCDATE())
                                                            AND s.Date < DATEADD(DAY,
                                                              -1, GETUTCDATE())
                                                            AND s.IDSite = @SiteID
                                                            AND s.IDDepartment IN (
                                                            SELECT
                                                              item
                                                            FROM
                                                              dbo.fnStringList2Table(@DepartmentIDList) )
                                                     GROUP BY s.IDSite
                                                   );

                --SET @WorkstationAvgUtilization = ( SELECT   ( CAST(@WorkstationsInServiceWeek AS DECIMAL(18,
                --                                              2))
                --                                              / IIF(CAST(@WorkstationsTotal AS DECIMAL(18,
                --                                              2)) > 1, CAST(@WorkstationsTotal AS DECIMAL(18,
                --                                              2)), 1) * 100 ) AS WorkstationAvgUtilization
                --                                 );
                SET @WorkstationAvgUtilization = ( SELECT   AVG(s.AvgEstimatedPCUtilization)
                                                   FROM     dbo.SummaryWorkstation s
                                                   WHERE    s.Date > DATEADD(DAY,
                                                              -3, GETUTCDATE())
                                                            AND s.Date < DATEADD(DAY,
                                                              -1, GETUTCDATE())
                                                            AND s.IDSite = @SiteID
                                                            AND s.IDDepartment IN (
                                                            SELECT
                                                              item
                                                            FROM
                                                              dbo.fnStringList2Table(@DepartmentIDList) )
                                                   GROUP BY s.IDSite
                                                 );
                SET @WorkstationsMoving = ( SELECT  ( COUNT(IDAsset) ) AS WorkstationsMoving
                                            FROM    dbo.Assets
                                            WHERE   LastPostDateUTC > DATEADD(HOUR,
                                                              -1, GETUTCDATE())
                                                    AND SiteID = @SiteID
                                                    AND DepartmentID IN (
                                                    SELECT  item
                                                    FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                                    AND IDAssetType IN ( 1, 3,
                                                              6 )
                                                    AND Move = 1
                                                    AND Retired = 0
                                          ); 
  
            END;
        IF ( @DepartmentIDList IS NULL )
            BEGIN
                SET @WorkstationsTotal = ( SELECT   COUNT(IDAsset) AS WorkstationsTotal
                                           FROM     dbo.Assets
                                           WHERE    SiteID = @SiteID
                                                    AND IDAssetType IN ( 1, 3,
                                                              6 )
                                                    AND Retired = 0
                                         );
                SET @WorkstationsInService = ( SELECT   COUNT(IDAsset) AS WorkstationsInService
                                               FROM     dbo.Assets
                                               WHERE    LastPostDateUTC > DATEADD(HOUR,
                                                              -1, GETUTCDATE())
                                                        AND ( Amps < -1000 OR move =1) 
                                                        AND SiteID = @SiteID
                                                        AND IDAssetType IN ( 1,
                                                              3, 6 )
                                                        AND Retired = 0
                                             );
                SET @WorkstationsInServiceWeek = ( SELECT   COUNT(IDAsset) AS WorkstationsInServiceWeek
                                                   FROM     dbo.Assets
                                                   WHERE    LastPostDateUTC > DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                             AND ( Amps < -1000 OR move =1) 
                                                            AND SiteID = @SiteID
                                                            AND IDAssetType IN (
                                                            1, 3, 6 )
                                                            AND Retired = 0
                                                 );
                SET @WorkstationsAvailable = ( SELECT   ( COUNT(IDAsset)
                                                          - ( @WorkstationsInService ) ) AS WorkstationsAvailable
                                               FROM     dbo.Assets
                                               WHERE    LastPostDateUTC > DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                        AND SiteID = @SiteID
													--	  AND ( Amps > -1000 AND move = 0) 
                                                        AND IDAssetType IN ( 1,
                                                              3, 6 )
                                                        AND Retired = 0
                                             );
                SET @WorkstationsOffline = ( SELECT ( COUNT(IDAsset) ) AS WorkstationsOffline
                                             FROM   dbo.Assets
                                             WHERE  LastPostDateUTC < DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                    AND SiteID = @SiteID
                                                    AND IDAssetType IN ( 1, 3,
                                                              6 )
                                                    AND Retired = 0
                                           ); 
                --SET @WorkstationPCAvgUtilization = ( SELECT AVG(s.AvgEstimatedPCUtilization) * 100
                --                                     FROM   dbo.SummaryWorkstation s
                --                                     WHERE  s.Date > DATEADD(DAY,
                --                                              -3, GETUTCDATE())
                --                                            AND s.Date < DATEADD(DAY,
                --                                              -1, GETUTCDATE())
                --                                            AND s.IDSite = @SiteID
                --                                     GROUP BY s.IDSite
                --                                   );
                SET @WorkstationPCAvgUtilization = ( SELECT AVG(s.EstimatedPCUtilization)
                                                            * 100
                                                     FROM   dbo.Sessions AS s
                                                     WHERE  s.EndDateUTC > DATEADD(DAY,
                                                              -3, GETUTCDATE())
                                                            AND s.EndDateUTC < DATEADD(DAY,
                                                              -1, GETUTCDATE())
                                                            AND s.SiteID = @SiteID
                                                     GROUP BY s.SiteID
                                                   );

                --SET @WorkstationAvgUtilization = ( SELECT  CAST( AVG(s.Utilization) AS FLOAT) * 100
                                                       
                --                                   FROM     dbo.SummaryWorkstation s
                --                                   WHERE    s.Date > DATEADD(DAY,
                --                                              -3, GETUTCDATE())
                --                                            AND s.Date < DATEADD(DAY,
                --                                              -1, GETUTCDATE())
                --                                            AND s.IDSite = @SiteID
                --                                   GROUP BY s.IDSite
                --                                 );
                SET @WorkstationAvgUtilization = ( SELECT   CAST(AVG(s.Utilization) AS FLOAT)
                                                            * 100
                                                   FROM     dbo.SummaryTrending s
                                                   WHERE    s.Date > DATEADD(DAY,
                                                              -3, GETUTCDATE())
                                                            AND s.Date < DATEADD(DAY,
                                                              -1, GETUTCDATE())
                                                            AND s.SiteID = @SiteID
                                                   GROUP BY s.SiteID
                                                 );

                --SET @WorkstationAvgUtilization = ( SELECT   ( CAST(@WorkstationsInServiceWeek AS DECIMAL(18,
                --                                              2))
                --                                              / IIF(CAST(@WorkstationsTotal AS DECIMAL(18,
                --                                              2)) > 1, CAST(@WorkstationsTotal AS DECIMAL(18,
                --                                              2)), 1) * 100 ) AS WorkstationAvgUtilization
                --                                 );
				   

 
                SET @WorkstationsMoving = ( SELECT  ( COUNT(IDAsset) ) AS WorkstationsMoving
                                            FROM    dbo.Assets
                                            WHERE   LastPostDateUTC > DATEADD(HOUR,
                                                              -1, GETUTCDATE())
                                                    AND SiteID = @SiteID
                                                    AND IDAssetType IN ( 1, 3,
                                                              6 )
                                                    AND Move = 1
                                                    AND Retired = 0
                                          ); 
            END;

--- ChargingBays
        DECLARE @ChargingBaysTotal INT;
        DECLARE @ChargingBaysCharging INT;
        DECLARE @ChargingBaysOccupied INT;
        DECLARE @ChargingBaysVacant INT;
        DECLARE @ChargingBaysOffline INT;
        IF ( @DepartmentIDList IS NOT NULL )
            BEGIN
                SET @ChargingBaysTotal = ( SELECT   SUM(BayCount) AS ChargingBaysTotal
                                           FROM     dbo.Assets
                                           WHERE    SiteID = @SiteID
                                                    AND DepartmentID IN (
                                                    SELECT  item
                                                    FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                                    AND Retired = 0
                                         );
                SET @ChargingBaysCharging = ( SELECT    COUNT(AC.IDAsset)
                                              FROM      dbo.vwChargers AS AC
                                              WHERE     AC.Bay1LastPostDateUTC > DATEADD(hour,
                                                              -24, GETUTCDATE())
                                                        AND AC.SiteID = @SiteID
                                                        AND AC.DepartmentID IN (
                                                        SELECT
                                                              item
                                                        FROM  dbo.fnStringList2Table(@DepartmentIDList) )
                                                        AND AC.IDAssetType IN ( 2,
                                                              4 )
                                                        AND AC.Retired = 0
                                                        AND AC.Bay1LastPostDateUTC IS NOT NULL
                                            )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.Assets
                        WHERE   Bay2LastPostDateUTC > DATEADD(hour,
                                                              -24,
                                                              GETUTCDATE())
                                AND SiteID = @SiteID
                                AND DepartmentID IN (
                                SELECT  item
                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                AND IDAssetType IN ( 2, 4 )
                                AND Retired = 0
                                AND Bay2LastPostDateUTC IS NOT NULL
                      )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.Assets
                        WHERE   Bay3LastPostDateUTC > DATEADD(hour,
                                                              -24,
                                                              GETUTCDATE())
                                AND SiteID = @SiteID
                                AND DepartmentID IN (
                                SELECT  item
                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                AND IDAssetType IN ( 2, 4 )
                                AND Retired = 0
                                AND Bay3LastPostDateUTC IS NOT NULL
                      )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.Assets
                        WHERE   Bay4LastPostDateUTC > DATEADD(hour,
                                                              -24,
                                                              GETUTCDATE())
                                AND SiteID = @SiteID
                                AND DepartmentID IN (
                                SELECT  item
                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                AND IDAssetType IN ( 2, 4 )
                                AND Retired = 0
                                AND Bay4LastPostDateUTC IS NOT NULL
                      );


                SET @ChargingBaysVacant = ( SELECT  COUNT(IDAsset)
                                            FROM    dbo.Assets
                                            WHERE   Bay1LastPostDateUTC <= DATEADD(MINUTE,
                                                              -8, GETUTCDATE())
                                                    AND Bay1LastPostDateUTC > DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                    AND SiteID = @SiteID
                                                    AND DepartmentID IN (
                                                    SELECT  item
                                                    FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                                    AND IDAssetType IN ( 2, 4 )
                                                    AND Retired = 0
                                                    AND Bay1LastPostDateUTC IS NOT NULL
                                          )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.Assets
                        WHERE   Bay2LastPostDateUTC <= DATEADD(MINUTE, -8,
                                                              GETUTCDATE())
                                AND Bay2LastPostDateUTC > DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                AND SiteID = @SiteID
                                AND DepartmentID IN (
                                SELECT  item
                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                AND IDAssetType IN ( 2, 4 )
                                AND Retired = 0
                                AND Bay2LastPostDateUTC IS NOT NULL
                      )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.Assets
                        WHERE   Bay3LastPostDateUTC <= DATEADD(MINUTE, -8,
                                                              GETUTCDATE())
                                AND Bay3LastPostDateUTC > DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                AND SiteID = @SiteID
                                AND DepartmentID IN (
                                SELECT  item
                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                AND IDAssetType IN ( 2, 4 )
                                AND Retired = 0
                                AND Bay3LastPostDateUTC IS NOT NULL
                      )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.Assets
                        WHERE   Bay4LastPostDateUTC <= DATEADD(MINUTE, -8,
                                                              GETUTCDATE())
                                AND Bay4LastPostDateUTC > DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                AND SiteID = @SiteID
                                AND DepartmentID IN (
                                SELECT  item
                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                AND IDAssetType IN ( 2, 4 )
                                AND Retired = 0
                                AND Bay4LastPostDateUTC IS NOT NULL
                      );


                SET @ChargingBaysOffline = ( SELECT COUNT(IDAsset)
                                             FROM   dbo.Assets
                                             WHERE  ( Bay1LastPostDateUTC <= DATEADD(DAY,
                                                              -7, GETUTCDATE()) )
                                                    AND SiteID = @SiteID
                                                    AND DepartmentID IN (
                                                    SELECT  item
                                                    FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                                    AND IDAssetType IN ( 2, 4 )
                                                    AND Retired = 0
                                           )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.Assets
                        WHERE   ( Bay2LastPostDateUTC <= DATEADD(DAY, -7,
                                                              GETUTCDATE()) )
                                AND SiteID = @SiteID
                                AND DepartmentID IN (
                                SELECT  item
                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                AND IDAssetType IN ( 2, 4 )
                                AND Retired = 0
                      )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.Assets
                        WHERE   ( Bay3LastPostDateUTC <= DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                  OR Bay3LastPostDateUTC IS NULL
                                )
                                AND SiteID = @SiteID
                                AND DepartmentID IN (
                                SELECT  item
                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                AND IDAssetType IN ( 2, 4 )
                                AND Retired = 0
                      )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.Assets
                        WHERE   ( Bay4LastPostDateUTC <= DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                  OR Bay4LastPostDateUTC IS NULL
                                )
                                AND SiteID = @SiteID
                                AND DepartmentID IN (
                                SELECT  item
                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                AND IDAssetType IN ( 2, 4 )
                                AND Retired = 0
                      );

            END;
        IF ( @DepartmentIDList IS  NULL )
            BEGIN

                SET @ChargingBaysTotal = ( SELECT   COUNT(VC.IDAsset) * 2
                                           FROM     dbo.vwChargers AS VC
                                           WHERE    ( VC.BayCount < 4
                                                      AND VC.BayCount IS NOT NULL
                                                    )
                                                    AND VC.SiteID = @SiteID
                                                    AND VC.Retired = 0
                                         )
                    + ( SELECT  COUNT(VC.IDAsset) * 4
                        FROM    dbo.vwChargers AS VC
                        WHERE   ( VC.BayCount = 4
                                  AND VC.BayCount IS NOT NULL
                                )
                                AND VC.SiteID = @SiteID
                                AND VC.Retired = 0
                      );
                SET @ChargingBaysCharging = ( SELECT    COUNT(CAB.IDAsset)
                                              FROM      dbo.vwChargers AS CAB
                                              WHERE     CAB.Bay1LastPostDateUTC > DATEADD(hour,
                                                              -24, GETUTCDATE())
                                                        AND CAB.SiteID = @SiteID
                                                        AND CAB.Retired = 0
                                                        AND ( CAB.Bay1Amps > 0
                                                              AND CAB.Bay1BSN IS NOT NULL
                                                              AND CAB.Bay1BSN <> '0'
                                                              AND CAB.Bay1BSN != 'No Batt SN'
                                                              AND CAB.Bay1Amps IS NOT NULL
                                                            )
                                            )
                    + ( SELECT  COUNT(CAB.IDAsset)
                        FROM    dbo.vwChargers AS CAB
                        WHERE   CAB.Bay2LastPostDateUTC > DATEADD(hour,
                                                              -24,
                                                              GETUTCDATE())
                                AND CAB.SiteID = @SiteID
                                AND CAB.Retired = 0
                                AND ( CAB.Bay2Amps > 0
                                      AND CAB.Bay2BSN IS NOT NULL
                                      AND CAB.Bay2BSN <> '0'
                                      AND CAB.Bay2BSN != 'No Batt SN'
                                      AND CAB.Bay2Amps IS NOT NULL
                                    )
                      )
                    + ( SELECT  COUNT(CAB.IDAsset)
                        FROM    dbo.vwChargers AS CAB
                        WHERE   CAB.Bay3LastPostDateUTC > DATEADD(hour,
                                                              -24,
                                                              GETUTCDATE())
                                AND CAB.SiteID = @SiteID
                                AND CAB.Retired = 0
                                AND ( CAB.Bay3Amps > 0
                                      AND CAB.Bay3BSN IS NOT NULL
                                      AND CAB.Bay3BSN <> '0'
                                      AND CAB.Bay3BSN != 'No Batt SN'
                                      AND CAB.Bay3Amps IS NOT NULL
                                    )
                      )
                    + ( SELECT  COUNT(CAB.IDAsset)
                        FROM    dbo.vwChargers AS CAB
                        WHERE   CAB.Bay4LastPostDateUTC > DATEADD(hour,
                                                              -24,
                                                              GETUTCDATE())
                                AND CAB.SiteID = @SiteID
                                AND CAB.Retired = 0
                                AND ( CAB.Bay4Amps > 0
                                      AND CAB.Bay4BSN IS NOT NULL
                                      AND CAB.Bay4BSN <> '0'
                                      AND CAB.Bay4BSN != 'No Batt SN'
                                      AND CAB.Bay4Amps IS NOT NULL
                                    )
                      );

                SET @ChargingBaysOccupied = ( SELECT    COUNT(IDAsset)
                                              FROM      dbo.vwChargers
                                              WHERE     Bay1LastPostDateUTC > DATEADD(hour,
                                                              -24, GETUTCDATE())
                                                        AND LastPostDateUTC > DATEADD(DAY,
                                                              -1, GETUTCDATE())
                                                        AND SiteID = @SiteID
                                                        AND Retired = 0
                                                        AND Bay1LastPostDateUTC IS NOT NULL
                                                        AND ( Bay1BSN IS NOT NULL
                                                              AND Bay1BSN != 'No Batt SN'
                                                              AND LEN(Bay1BSN) > 7
                                                              AND ( Bay1Amps >= 0
                                                 
                                                              )
                                                            )
                                            )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.vwChargers
                        WHERE   Bay2LastPostDateUTC > DATEADD(hour,
                                                              -24,
                                                              GETUTCDATE())
                                AND LastPostDateUTC > DATEADD(DAY, -1,
                                                              GETUTCDATE())
                                AND SiteID = @SiteID
                                AND Retired = 0
                                AND Bay2LastPostDateUTC IS NOT NULL
                                AND ( Bay2BSN IS NOT NULL
                                      AND Bay2BSN != 'No Batt SN'
                                      AND LEN(Bay2BSN) > 7
                                      AND ( Bay2Amps >= 0
                                     
                                          )
                                    )
                      )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.vwChargers
                        WHERE   Bay3LastPostDateUTC > DATEADD(hour,
                                                              -24,
                                                              GETUTCDATE())
                                AND LastPostDateUTC > DATEADD(DAY, -1,
                                                              GETUTCDATE())
                                AND SiteID = @SiteID
                                AND Retired = 0
                                AND BayCount > 2
                                AND Bay3LastPostDateUTC IS NOT NULL
                                AND ( Bay3BSN IS NOT NULL
                                      AND Bay3BSN != 'No Batt SN'
                                      AND LEN(Bay3BSN) > 7
                                      AND ( Bay3Amps >= 0
                                         
                                          )
                                    )
                      )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.vwChargers
                        WHERE   Bay4LastPostDateUTC > DATEADD(hour,
                                                              -24,
                                                              GETUTCDATE())
                                AND LastPostDateUTC > DATEADD(DAY, -1,
                                                              GETUTCDATE())
                                AND SiteID = @SiteID
                                AND Retired = 0
                                AND BayCount > 2
                                AND Bay4LastPostDateUTC IS NOT NULL
                                AND ( Bay4BSN IS NOT NULL
                                      AND Bay4BSN != 'No Batt SN'
                                      AND LEN(Bay4BSN) > 7
                                      AND ( Bay4Amps >= 0
                                     
                                          )
                                    )
                      );

				    

                SET @ChargingBaysVacant = ( SELECT  COUNT(IDAsset)
                                            FROM    dbo.vwChargers
                                            WHERE   LastPostDateUTC > DATEADD(hour,
                                                              -24, GETUTCDATE())
                                                    AND SiteID = @SiteID
                                                    AND Retired = 0
                                          
                                                    --AND ( Bay1Activity = 3
                                                    --      OR Bay1ChargeLevel = 0 or Bay1ChargeLevel IS NULL)                                                        
                                                    AND ( Bay1Amps <= 0
                                                          OR Bay1Amps IS NULL
                                                        )
                                                    AND ( Bay1BSN IS NULL
                                                          OR LEN(Bay1BSN) < 7
                                                          OR Bay1BSN = 'No Batt SN'
                                                        )
                                          )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.vwChargers
                        WHERE   LastPostDateUTC > DATEADD(hour,
                                                              -24,
                                                          GETUTCDATE())
                                AND SiteID = @SiteID
                                AND Retired = 0
                                                   
                                                    --AND ( Bay2Activity = 3
                                                    --      OR Bay2ChargeLevel = 0 or Bay2ChargeLevel IS NULL)                                                        
                                AND ( Bay2Amps <= 0
                                      OR Bay2Amps IS NULL
                                    )
                                AND ( Bay2BSN IS NULL
                                      OR LEN(Bay2BSN) < 7
                                      OR Bay2BSN = 'No Batt SN'
                                    )
                      )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.vwChargers
                        WHERE   SiteID = @SiteID
                                AND LastPostDateUTC > DATEADD(hour,
                                                              -24,
                                                              GETUTCDATE())
                                AND Retired = 0
                                AND BayCount > 2
                                AND ( Bay3Amps <= 0
                                      OR Bay3Amps IS NULL
                                    )
                                AND ( Bay3BSN IS NULL
                                      OR LEN(Bay3BSN) < 7
                                      OR Bay3BSN = 'No Batt SN'
                                    )
                      )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.vwChargers
                        WHERE   LastPostDateUTC > DATEADD(hour,
                                                              -24,
                                                          GETUTCDATE())
                                AND SiteID = @SiteID
                                AND Retired = 0
                                AND BayCount > 2
                                                    --AND ( Bay4Activity = 3
                                                    --      OR Bay4ChargeLevel = 0 or Bay4ChargeLevel IS NULL)                                                        
                                AND ( Bay4Amps <= 0
                                      OR Bay4Amps IS NULL
                                    )
                                AND ( Bay4BSN IS NULL
                                      OR LEN(Bay4BSN) < 7
                                      OR Bay4BSN = 'No Batt SN'
                                    )
                      ); 
		

                SET @ChargingBaysOffline = ( SELECT COUNT(VC.IDAsset)
                                             FROM   dbo.vwChargers AS VC
                                             WHERE  ( VC.Bay1LastPostDateUTC < DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                      AND VC.Bay1LastPostDateUTC IS NOT NULL
                                                    )
                                                    AND VC.LastPostDateUTC < DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                    AND VC.SiteID = @SiteID
                                                    AND VC.Retired = 0
                                           )
                    + ( SELECT  COUNT(VC.IDAsset)
                        FROM    dbo.vwChargers AS VC
                        WHERE   ( VC.Bay2LastPostDateUTC < DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                  AND VC.Bay2LastPostDateUTC IS NOT NULL
                                )
                                AND VC.LastPostDateUTC < DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                AND VC.SiteID = @SiteID
                                AND VC.Retired = 0
                      )
                    + ( SELECT  COUNT(VC.IDAsset)
                        FROM    dbo.vwChargers AS VC
                        WHERE   ( VC.Bay3LastPostDateUTC < DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                  AND VC.Bay3LastPostDateUTC IS NOT NULL
                                )
                                AND VC.LastPostDateUTC < DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                AND VC.BayCount = 4
                                AND VC.SiteID = @SiteID
                                AND VC.Retired = 0
                      )
                    + ( SELECT  COUNT(VC.IDAsset)
                        FROM    dbo.vwChargers AS VC
                        WHERE   ( ( VC.Bay4LastPostDateUTC < DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                  AND VC.Bay4LastPostDateUTC IS NOT NULL
                                  )
                                )
                                AND VC.LastPostDateUTC < DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                AND VC.BayCount = 4
                                AND VC.SiteID = @SiteID
                                AND VC.Retired = 0
                      );

            END;


--- Chargers
        DECLARE @ChargersTotal INT;
        DECLARE @ChargersCharging INT;
        DECLARE @ChargersVacant INT;
        DECLARE @ChargersOccupied INT;
        DECLARE @ChargersOnline INT;
        DECLARE @ChargersOffline INT;
        DECLARE @LowChargeInserts INT;
        DECLARE @HiChargeRemovals INT;
        IF ( @DepartmentIDList IS NOT NULL )
            BEGIN
                SET @ChargersTotal = ( SELECT   SUM(BayCount) AS ChargingBaysTotal
                                       FROM     dbo.Assets
                                       WHERE    SiteID = @SiteID
                                                AND DepartmentID IN (
                                                SELECT  item
                                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                                AND IDAssetType IN ( 2, 4 )
                                                AND Retired = 0
                                     );
                SET @ChargersCharging = ( SELECT    COUNT(IDAsset)
                                          FROM      dbo.Assets
                                          WHERE     Bay1LastPostDateUTC > DATEADD(MINUTE,
                                                              -8, GETUTCDATE())
                                                    AND SiteID = @SiteID
                                                    AND DepartmentID IN (
                                                    SELECT  item
                                                    FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                                    AND IDAssetType IN ( 2, 4 )
                                                    AND Retired = 0
                                                    AND Bay1LastPostDateUTC IS NOT NULL
                                        )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.Assets
                        WHERE   Bay2LastPostDateUTC > DATEADD(MINUTE, -8,
                                                              GETUTCDATE())
                                AND SiteID = @SiteID
                                AND DepartmentID IN (
                                SELECT  item
                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                AND IDAssetType IN ( 2, 4 )
                                AND Retired = 0
                                AND Bay2LastPostDateUTC IS NOT NULL
                      )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.Assets
                        WHERE   Bay3LastPostDateUTC > DATEADD(MINUTE, -8,
                                                              GETUTCDATE())
                                AND SiteID = @SiteID
                                AND DepartmentID IN (
                                SELECT  item
                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                AND IDAssetType IN ( 2, 4 )
                                AND Retired = 0
                                AND Bay3LastPostDateUTC IS NOT NULL
                      )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.Assets
                        WHERE   Bay4LastPostDateUTC > DATEADD(MINUTE, -8,
                                                              GETUTCDATE())
                                AND SiteID = @SiteID
                                AND DepartmentID IN (
                                SELECT  item
                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                AND IDAssetType IN ( 2, 4 )
                                AND Retired = 0
                                AND Bay4LastPostDateUTC IS NOT NULL
                      );


                SET @ChargersVacant = ( SELECT  COUNT(IDAsset)
                                        FROM    dbo.Assets
                                        WHERE   Bay1LastPostDateUTC <= DATEADD(MINUTE,
                                                              -8, GETUTCDATE())
                                                AND Bay1LastPostDateUTC > DATEADD(hour,
                                                              -24, GETUTCDATE())
                                                AND SiteID = @SiteID
                                                AND DepartmentID IN (
                                                SELECT  item
                                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                                AND IDAssetType IN ( 2, 4 )
                                                AND Retired = 0
                                                AND Bay1LastPostDateUTC IS NOT NULL
                                      )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.Assets
                        WHERE   Bay2LastPostDateUTC <= DATEADD(MINUTE, -8,
                                                              GETUTCDATE())
                                AND Bay2LastPostDateUTC > DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                AND SiteID = @SiteID
                                AND DepartmentID IN (
                                SELECT  item
                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                AND IDAssetType IN ( 2, 4 )
                                AND Retired = 0
                                AND Bay2LastPostDateUTC IS NOT NULL
                      )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.Assets
                        WHERE   Bay3LastPostDateUTC <= DATEADD(MINUTE, -8,
                                                              GETUTCDATE())
                                AND Bay3LastPostDateUTC > DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                AND SiteID = @SiteID
                                AND DepartmentID IN (
                                SELECT  item
                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                AND IDAssetType IN ( 2, 4 )
                                AND Retired = 0
                                AND Bay3LastPostDateUTC IS NOT NULL
                      )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.Assets
                        WHERE   Bay4LastPostDateUTC <= DATEADD(MINUTE, -8,
                                                              GETUTCDATE())
                                AND Bay4LastPostDateUTC > DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                AND SiteID = @SiteID
                                AND DepartmentID IN (
                                SELECT  item
                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                AND IDAssetType IN ( 2, 4 )
                                AND Retired = 0
                                AND Bay4LastPostDateUTC IS NOT NULL
                      );


                SET @ChargersOffline = ( SELECT COUNT(IDAsset)
                                         FROM   dbo.Assets
                                         WHERE  ( Bay1LastPostDateUTC <= DATEADD(DAY,
                                                              -7, GETUTCDATE()) )
                                                AND SiteID = @SiteID
                                                AND DepartmentID IN (
                                                SELECT  item
                                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                                AND IDAssetType IN ( 2, 4 )
                                                AND Retired = 0
                                       )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.Assets
                        WHERE   ( Bay2LastPostDateUTC <= DATEADD(DAY, -7,
                                                              GETUTCDATE()) )
                                AND SiteID = @SiteID
                                AND DepartmentID IN (
                                SELECT  item
                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                AND IDAssetType IN ( 2, 4 )
                                AND Retired = 0
                      )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.Assets
                        WHERE   ( Bay3LastPostDateUTC <= DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                  OR Bay3LastPostDateUTC IS NULL
                                )
                                AND SiteID = @SiteID
                                AND DepartmentID IN (
                                SELECT  item
                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                AND IDAssetType IN ( 2, 4 )
                                AND Retired = 0
                      )
                    + ( SELECT  COUNT(IDAsset)
                        FROM    dbo.Assets
                        WHERE   ( Bay4LastPostDateUTC <= DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                  OR Bay4LastPostDateUTC IS NULL
                                )
                                AND SiteID = @SiteID
                                AND DepartmentID IN (
                                SELECT  item
                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                AND IDAssetType IN ( 2, 4 )
                                AND Retired = 0
                      );

            END;
--        IF ( @DepartmentIDList IS  NULL )
--            BEGIN
        
--set @ChargersTotal =  (select count(idasset) as ChargersTotal from assets where  siteid = @SiteID  and DepartmentID in (SELECT item  from fnStringList2Table(@DepartmentIDList)) and IDAssetType in (2,4) and retired=0)

--set @ChargersCharging =  
--(select count(idasset)  from assets where LastPostDateUTC > dateadd(MINUTE,-8,getutcdate()) and siteid = @SiteID and DepartmentID in (SELECT item  from fnStringList2Table(@DepartmentIDList)) and IDAssetType in (2,4) and retired=0) 

--set @ChargersVacant =  
--(select count(idasset)  from assets where LastPostDateUTC < dateadd(MINUTE,-8,getutcdate()) and LastPostDateUTC > dateadd(day,-7,getutcdate()) and siteid = @SiteID  and DepartmentID in (SELECT item  from fnStringList2Table(@DepartmentIDList))  and IDAssetType in (2,4) and retired=0) 

--set @ChargersOffline =  
--(select count(idasset)  from assets where (LastPostDateUTC < dateadd(DAY,-7,getutcdate()) or LastPostDateUTC is  null) and siteid = @SiteID  and DepartmentID in (SELECT item  from fnStringList2Table(@DepartmentIDList)) and IDAssetType in (2,4) and retired=0) 
--set @LowChargeInserts = 
--(select top 1 isnull(s.LoChargeInsertsCount,0)  as 'LowChargeInserts' FROM SummaryWorkstation s  where IDSite = @SiteID and s.Date > dateadd(hour,-24,getutcdate()) and IDDepartment in (SELECT item  from fnStringList2Table(@DepartmentIDList)) )
--set @HiChargeRemovals=
--(select top 1 isnull(s.HiChargeRemovalsCount,0)  as 'HiChargeRemovals' FROM SummaryWorkstation s  where IDSite = @SiteID and s.Date > dateadd(hour,-24,getutcdate()) and IDDepartment in (SELECT item  from fnStringList2Table(@DepartmentIDList)))
--end
        IF ( @DepartmentIDList IS  NULL )
            BEGIN
                SET @ChargersTotal = ( SELECT   COUNT(IDAsset) AS ChargersTotal
                                       FROM     dbo.vwChargers
                                       WHERE    SiteID = @SiteID
                                                AND Retired = 0
                                     );

                SET @ChargersCharging = ( SELECT    COUNT(VC.IDAsset) AS ChargersCharging
                                          FROM      dbo.vwChargers AS VC
                                          WHERE     VC.LastPostDateUTC >= DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                    AND VC.SiteID = @SiteID
                                                    AND VC.Amps > 0
                                                    AND VC.Amps IS NOT NULL
                                                    AND VC.Retired = 0
                                        );
--set @ChargersOccupied =  
--(select COUNT(idasset) AS ChargersIdle  from assets where LastPostDateUTC >= dateadd(day,-7,getutcdate()) and siteid = @SiteID and IDAssetType in (2,4) AND amps < .1 and retired=0 AND (ChargeLevel >= 95 ) )

--set @ChargersVacant =  
--(select COUNT(idasset) from assets where LastPostDateUTC >= dateadd(DAY,-7,getutcdate()) and siteid = @SiteID AND (Activity = 3 OR chargelevel = 0) AND amps < .1 and IDAssetType in (2,4) and retired=0)  
                SET @ChargersOnline = ( SELECT  COUNT(IDAsset)
                                        FROM    dbo.vwChargers
                                        WHERE   ( LastPostDateUTC >= DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                  AND LastPostDateUTC IS NOT NULL
                                                )
                                                AND SiteID = @SiteID
                                                AND Retired = 0
                                      ); 

                SET @ChargersOffline = ( SELECT COUNT(IDAsset)
                                         FROM   dbo.vwChargers
                                         WHERE  ( LastPostDateUTC <= DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                  OR LastPostDateUTC IS NULL
                                                )
                                                AND SiteID = @SiteID
                                                AND Retired = 0
                                       ); 
                SET @LowChargeInserts = ( SELECT    COUNT(S2.DeviceSerialNumber) AS 'LowChargeInserts'
                                          FROM      dbo.Sessions AS S2
                                          WHERE     S2.StartDateUTC >= DATEADD(DAY,
                                                              -1, GETUTCDATE())
                                                    AND S2.StartChargeLevel <= 90
                                                    AND S2.SiteID = @SiteID
                                        );
                SET @HiChargeRemovals = ( SELECT    COUNT(S2.DeviceSerialNumber) AS 'HiChargeRemovals'
                                          FROM      dbo.Sessions AS S2
                                          WHERE     S2.StartDateUTC >= DATEADD(DAY,
                                                              -1, GETUTCDATE())
                                                    AND S2.EndChargeLevel >= 20
                                                    AND S2.SiteID = @SiteID
                                        );
            END;


--Batteries
        DECLARE @BatteriesTotal INT  = 0;
        DECLARE @BatteriesAvailable INT  = 0;
        DECLARE @BatteriesInUse INT  = 0;
        DECLARE @BatteriesCharging INT = 0;
        DECLARE @BatteriesDormant INT  = 0;
        DECLARE @BatteriesOffline INT  = 0;
        DECLARE @BatteriesFullyCharged INT  = 0;
        DECLARE @BatteriesAvgCapacity DECIMAL(18, 2) = 0.00;

        IF ( @DepartmentIDList IS NOT NULL )
            BEGIN
                SET @BatteriesTotal = ( SELECT  COUNT(IDAsset)
                                        FROM    dbo.Assets
                                        WHERE   SiteID = @SiteID
                                                AND DepartmentID IN (
                                                SELECT  item
                                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                                AND IDAssetType IN ( 5 )
                                                AND Retired = 0
                                      );  
                SET @BatteriesInUse = ( SELECT  COUNT(IDAsset)
                                        FROM    dbo.Assets
                                        WHERE   LastPacketType = 1
                                                AND DepartmentID IN (
                                                SELECT  item
                                                FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                                AND LastPostDateUTC >= DATEADD(MINUTE,
                                                              -1, GETUTCDATE())
                                                AND SiteID = @SiteID
                                                AND IDAssetType IN ( 5 )
                                                AND Retired = 0
                                                AND LastPostDateUTC IS NOT NULL
                                      );  

                SET @BatteriesCharging = ( SELECT   COUNT(IDAsset)
                                           FROM     dbo.Assets
                                           WHERE    LastPacketType = 2
                                                    AND DepartmentID IN (
                                                    SELECT  item
                                                    FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                                    AND LastPostDateUTC >= DATEADD(MINUTE,
                                                              -8, GETUTCDATE())
                                                    AND SiteID = @SiteID
                                                    AND IDAssetType IN ( 5 )
                                                    AND Retired = 0
                                                    AND LastPostDateUTC IS NOT NULL
                                         ); 

                SET @BatteriesDormant = ( SELECT    COUNT(IDAsset)
                                          FROM      dbo.Assets
                                          WHERE     LastPostDateUTC > DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                    AND LastPostDateUTC < DATEADD(MINUTE,
                                                              -8, GETUTCDATE())
                                                    AND SiteID = @SiteID
                                                    AND IDAssetType IN ( 5 )
                                                    AND DepartmentID IN (
                                                    SELECT  item
                                                    FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                                    AND Retired = 0
                                                    AND LastPostDateUTC IS NOT NULL
                                        ); 

                SET @BatteriesOffline = ( SELECT    COUNT(IDAsset)
                                          FROM      dbo.Assets
                                          WHERE     ( LastPostDateUTC <= DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                      OR ( LastPostDateUTC IS NULL
                                                           AND CreatedDateUTC > DATEADD(MONTH,
                                                              -3, GETUTCDATE())
                                                         )
                                                    )
                                                    AND SiteID = @SiteID
                                                    AND DepartmentID IN (
                                                    SELECT  item
                                                    FROM    dbo.fnStringList2Table(@DepartmentIDList) )
                                                    AND IDAssetType IN ( 5 )
                                                    AND Retired = 0
                                        ); 

                SET @BatteriesFullyCharged = ( SELECT   COUNT(IDAsset)
                                               FROM     dbo.Assets
                                               WHERE    ( LastPostDateUTC >= DATEADD(hour,
                                                              -24, GETUTCDATE()) )
                                                        AND ChargeLevel > 80
                                                        AND LastPacketType = 2
                                                        AND SiteID = @SiteID
														 AND LEN(SerialNo) > 7
                                                        AND DepartmentID IN (
                                                        SELECT
                                                              item
                                                        FROM  dbo.fnStringList2Table(@DepartmentIDList) )
                                                        AND IDAssetType IN ( 5 )
                                                        AND Retired = 0
                                             ); 
                SET @BatteriesAvgCapacity = ( SELECT    SUM(ISNULL(NULLIF(dbo.fnGetBatteryCapacity_Int(SerialNo,
                                                              FullChargeCapacity),
                                                              0), 90))
                                                        / ( @BatteriesCharging
                                                            + @BatteriesDormant
                                                            + @BatteriesInUse ) AS BatteriesAvgCapacity
                                              FROM      dbo.Assets
                                              WHERE     LastPostDateUTC > DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                        AND SiteID = @SiteID
                                                        AND FullChargeCapacity > 16000
                                                        AND FullChargeCapacity IS NOT NULL
                                                        AND DepartmentID IN (
                                                        SELECT
                                                              item
                                                        FROM  dbo.fnStringList2Table(@DepartmentIDList) )
                                                        AND IDAssetType IN ( 5 )
                                                        AND Retired = 0
                                                        AND LastPostDateUTC IS NOT NULL
                                            );
              
            END;

        IF ( @DepartmentIDList IS  NULL )
            BEGIN
                SET @BatteriesTotal = ( SELECT  COUNT(IDAsset)
                                        FROM    dbo.Assets
                                        WHERE   SiteID = @SiteID
                                                AND IDAssetType IN ( 5 )
                                                AND LastPostDateUTC > DATEADD(DAY,
                                                              -30, GETUTCDATE())
                                                AND Retired = 0
                                      );  
                SET @BatteriesInUse = ( SELECT  COUNT(AB.SerialNo)
                                        FROM    dbo.vwBatteries AS AB
                                        WHERE   AB.LastPacketType = 1
                                                AND AB.LastPostDateUTC >= DATEADD(hour,
                                                              -1, GETUTCDATE())
                                                AND AB.SiteID = @SiteID
                                                AND AB.IDAssetType IN ( 5 )
                                                AND AB.Retired = 0 AND amps IS NOT null
                                            
                                                AND AB.Amps < 0
                                                AND AB.LastPostDateUTC IS NOT NULL
                                      );  
                SET @BatteriesAvailable = ( SELECT  COUNT(AB.SerialNo)
                                            FROM    dbo.vwBatteries AS AB
                                            WHERE   
                                                     AB.LastPostDateUTC >= DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                    AND AB.SiteID = @SiteID
                                                    AND AB.IDAssetType IN ( 5 )
													AND  AB.LastPacketType = 2
                                                    AND AB.Retired = 0
                                                    AND (AB.Amps >= 0 or amps IS  NULL)	
                                                    AND AB.LastPostDateUTC IS NOT NULL
                                          );  

                SET @BatteriesCharging = ( SELECT   COUNT(IDAsset)
                                           FROM     dbo.Assets
                                           WHERE    LastPacketType = 2
                                                    AND LastPostDateUTC >= DATEADD(hour,
                                                              -24,
                                                              GETUTCDATE())
                                                    AND SiteID = @SiteID
                                                    AND IDAssetType IN ( 5 )
                                                    AND Retired = 0
                                                    AND LastPostDateUTC IS NOT NULL
                                                    AND ChargeLevel < 100 
                                                    AND ( Amps > 0 AND Amps IS NOT NULL
                                                      
                                                        )
                                         ); 
										 --Batteries havent reported IN 24 hours but NOT offline
                SET @BatteriesDormant = ( SELECT    COUNT(IDAsset)
                                          FROM      dbo.Assets
                                          WHERE     LastPostDateUTC > DATEADD(DAY,
                                                              -7, GETUTCDATE())
                                                    AND LastPostDateUTC < DATEADD(HOUR,
                                                              -24,
                                                              GETUTCDATE())
                                                    AND SiteID = @SiteID
                                                    AND IDAssetType IN ( 5 )
                                                    AND Retired = 0
                                                    AND LastPostDateUTC IS NOT NULL
                                        ); 

                SET @BatteriesOffline = ( SELECT    COUNT(IDAsset)
                                          FROM      dbo.Assets
                                          WHERE     ( LastPostDateUTC <= DATEADD(DAY,
                                                              -7, GETUTCDATE()) )
                                                    AND SiteID = @SiteID
                                                    AND IDAssetType IN ( 5 )
                                                    AND Retired = 0
                                                    AND LastPostDateUTC IS NOT NULL
                                        ); 
                SET @BatteriesFullyCharged = ( SELECT   COUNT(IDAsset)
                                               FROM     dbo.Assets
                                               WHERE    ( LastPostDateUTC >= DATEADD(HOUR,-24, GETUTCDATE()) )
                                                        AND SiteID = @SiteID
                                                        AND  ChargeLevel > 94
                                                              AND Amps = 0 AND Amps IS NOT NULL
                                                              
                                                            
                                                        AND LastPacketType = 2
                                                        AND IDAssetType IN ( 5 )
														 AND LEN(SerialNo) > 7
                                                        AND Retired = 0
                                                        AND LastPostDateUTC IS NOT NULL
                                             ); 
                SET @BatteriesAvgCapacity = ( SELECT    SUM(dbo.fnGetBatteryCapacity_Int(SerialNo,
                                                              FullChargeCapacity))
                                                        / ( @BatteriesTotal - @BatteriesOffline ) AS BatteriesAvgCapacity
                                              FROM      dbo.Assets
                                              WHERE     LastPostDateUTC > DATEADD(DAY,
                                                              -7,
                                                              GETUTCDATE())
                                              
                                                        AND FullChargeCapacity IS NOT NULL
                                                        AND SiteID = @SiteID
                                                        AND IDAssetType IN ( 5 )
                                                        AND Retired = 0
                                                        AND LastPostDateUTC IS NOT NULL
                                                        AND ( FullChargeCapacity IS NOT NULL
                                                              AND FullChargeCapacity > 0
                                                            )
                                            );
              
            END;
        SELECT  @WorkstationsTotal AS WorkstationsTotal ,
                @WorkstationsInService AS WorkstationsInService ,
                @WorkstationsInServiceWeek AS WorkstationsInServiceWeek ,
                @WorkstationsAvailable AS WorkstationsAvailable ,
                @WorkstationsOffline AS WorkstationsOffline ,
                @WorkstationsMoving AS WorkstationsMoving ,
                @WorkstationAvgUtilization AS WorkstationAvgUtilization ,
                @WorkstationPCAvgUtilization AS WorkstationPCAvgUtilization ,
                @ChargersTotal AS ChargersTotal ,
    --            @ChargersCharging AS ChargersCharging ,
				--@ChargersOccupied AS ChargersOccupied,
                --@ChargersVacant AS ChargersVacant ,
                @ChargersOnline AS ChargersOnline ,
                @ChargersOffline AS ChargersOffline ,
                @ChargingBaysTotal AS ChargingBaysTotal ,
                @ChargingBaysCharging AS ChargingBaysCharging ,
                @ChargingBaysOccupied AS ChargingBaysOccupied ,
                @ChargingBaysVacant AS ChargingBaysVacant ,
                @ChargingBaysOffline AS ChargingBaysOffline ,
                @HiChargeRemovals AS HiChargeRemovals ,
                @LowChargeInserts AS LowChargeInserts ,
                @BatteriesTotal AS BatteriesTotal ,
                @BatteriesInUse AS BatteriesInService ,
                @BatteriesAvailable AS BatteriesAvailable ,
                @BatteriesCharging AS BatteriesCharging ,
                @BatteriesFullyCharged AS BatteriesFullyCharged ,
                @BatteriesDormant AS BatteriesDormant ,
                @BatteriesOffline AS BatteriesOffline ,
                IIF(@BatteriesAvgCapacity < 101, @BatteriesAvgCapacity, 100) AS BatteriesAvgCapacity;

    END;


    SELECT  VC.SerialNo ,
            VC.Bay1BSN ,
					vc.Bay1ChargeLevel,
					vc.Bay1Amps,
					vc.Bay1Activity,
            VC.Bay1LastPostDateUTC ,
            VC.Bay2BSN ,
			vc.Bay2ChargeLevel,
					vc.Bay2Amps,
					vc.Bay2Activity,
            VC.Bay2LastPostDateUTC ,
            VC.Bay3BSN ,
					vc.Bay3ChargeLevel,
							vc.Bay3Amps,
					vc.Bay3Activity,
            VC.Bay3LastPostDateUTC ,
            VC.Bay4BSN ,
            VC.Bay4LastPostDateUTC ,
            VC.LastPostDateUTC ,
					vc.Bay4ChargeLevel,
							vc.Bay4Amps,
					vc.Bay4Activity,
            VC.BayCount ,
            VC.Retired
    FROM    dbo.vwChargers AS VC
    WHERE   VC.SiteID = 633
            AND VC.Retired = 0;


			SELECT * FROM dbo.vwWorkstations AS VW WHERE VW.SiteID = 633
   
SELECT VB.FullChargeCapacity ,
      VB.CycleCount ,
      VB.ChargeLevel ,
      VB.MaxCycleCount ,
      VB.Description ,
      VB.SerialNo ,
      VB.IDAssetType ,
      VB.CreatedDateUTC ,
      VB.Temperature ,
      VB.LastPostDateUTC ,
      VB.Retired ,
      VB.LastPacketType ,
      VB.RowPointer ,
      VB.Amps ,
	  [dbo].[fnGetBatteryCapacity_Int](vb.SerialNo,vb.FullChargeCapacity) AS CapacityHealth,
	  	    IIF(VB.ChargeLevel > 94 AND amps = 0 AND amps IS NOT null,1,0) AS 'FullyCharged',
			   IIF(ChargeLevel < 100 AND Amps > 0 AND Amps IS NOT NULL,1,0) AS 'Charging',
	  IIF(VB.LastPostDateUTC > DATEADD(HOUR,-24,GETUTCDATE()),1,0) AS 'ReportedPast24HR',
	    IIF(VB.LastPostDateUTC > DATEADD(HOUR,-24,GETUTCDATE()),1,0) AS 'CHGAbove95',
		  IIF(VB.LastPostDateUTC < DATEADD(day,-7,GETUTCDATE()),1,0) AS 'BatteryOffline'
     FROM dbo.vwBatteries AS VB WHERE SiteID = 633 AND retired = 0 








GO
