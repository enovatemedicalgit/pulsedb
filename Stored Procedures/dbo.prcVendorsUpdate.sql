SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcVendorsUpdate]
    @IDVendor INT ,
    @VendorName VARCHAR(150) = NULL
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
    BEGIN TRAN;

    UPDATE  dbo.Vendors
    SET     [VendorName] = @VendorName
    WHERE   [IDVendor] = @IDVendor;
	
	-- Begin Return Select <- do not remove
    SELECT  [IDVendor] ,
            [VendorName]
    FROM    dbo.Vendors
    WHERE   [IDVendor] = @IDVendor;	
	-- End Return Select <- do not remove

    COMMIT;



GO
