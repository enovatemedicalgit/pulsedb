SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================

-- =============================================
CREATE Procedure [dbo].[prcGetNetwork] @SiteID AS VARCHAR(6)
AS
    BEGIN
        SELECT TOP 1
                SSID ,
                ISNULL(WEPKey, WPAPassphrase) AS pass ,
                Channel ,
                NetAuthProtocolID
        FROM    dbo.Network
        WHERE   SiteID = @SiteID
        ORDER BY CreatedDateUTC DESC;
    END;


GO
