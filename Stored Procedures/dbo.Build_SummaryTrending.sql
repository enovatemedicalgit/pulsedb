SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*################################################################################################
 Name				: Build_SummaryTrending
 Date				: 
 Author				: christopher.stewart
 Company				: enovate 
 Purpose				: Build Summary Tables 
 Usage				:
 Impact				:
 Required Perm			:   
 Called by			:   
##################################################################################################
 ver		author					date			change  
 1.0		Arlow.Farrell			Unknown			initial [spSummaryTrending]
 1.1		Christopher.Stewart		06212016		Optimizition and Comments
#################################################################################################*/

/*##################################################################################################
 STEPS	  Description						 
 1		  SummaryActiveAssets_RollupNow
 2		  Populate Summary Trending
 3		  Populate MovementDetail
 		  Populate Movement
 		  Update Movement.AvgRunRate
 		  Update SummaryTrending.ActiveDeviceCount
 		  Update SummaryTrending.Utilization
 		  Populate SummarySiteAssets
 		  UpdateSummarySiteAssets
#################################################################################################*/

CREATE PROCEDURE [dbo].[Build_SummaryTrending]
AS
    DECLARE @LastUpdated AS DATE;	  -- Sets the endpoint for updates
    DECLARE @LapTimer AS DATETIME2 = GETDATE();  -- holds the begin and end tie of each step for perf testing
    DECLARE @ProcessTimer AS DATETIME2 = GETDATE();

    DECLARE @LastSummarized DATE;
    DECLARE @FromDate DATE;
    DECLARE @ToDate DATE;


    SELECT  @LastSummarized = CONVERT(DATE, MAX([Date])) ,
            @FromDate = DATEADD(d, -1, CONVERT(DATE, MAX([Date])))
    FROM    dbo.SummaryTrending;
    SET @ToDate = CONVERT(DATE, GETDATE());
    PRINT dbo.GetMessageBorder();
/*################################################################################################*/
    Step_1:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 1'; 
    RAISERROR('dbo.Build_SummaryActiveAssets',0,1) WITH NOWAIT;
							 
/*################################################################################################*/


    EXEC dbo.Build_SummaryActiveAssets

    PRINT dbo.GetLapTime(@LapTimer); 

/*################################################################################################*/
    Step_2:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 2';
    RAISERROR('Populate SummaryTrending',0,1) WITH NOWAIT;

/*################################################################################################*/


    PRINT '--Clear any existing records in target table for the selected date range';

    DELETE  FROM dbo.SummaryTrending
    WHERE   [Date] BETWEEN @FromDate AND @ToDate;

    PRINT '--Stage NonSession data in tempDB';
    SELECT  CONVERT(DATE, nsd.CreatedDateUTC) AS RecordDate ,
            d.SiteID AS SiteId ,
            d.DepartmentID AS DepartmentId ,
            COUNT(nsd.ROW_ID) AS NonSessionRecordCount
    INTO    #tmpNonSessionSummary
    FROM    dbo.NonSessionDataCurrent nsd WITH ( NOLOCK )
            LEFT JOIN dbo.Assets d WITH ( NOLOCK ) ON nsd.DeviceSerialNumber = d.SerialNo
    WHERE   d.Retired = 0
            AND nsd.CreatedDateUTC BETWEEN @FromDate AND @ToDate
            AND nsd.CreatedDateUTC IS NOT NULL
    GROUP BY CONVERT(DATE, nsd.CreatedDateUTC) ,
            d.SiteID ,
            d.DepartmentID;

    PRINT '-- Insert Combined Sessions and NonSessionDate into SummaryTrending';
    WITH    tmpSessionSummary
              AS ( SELECT   CONVERT(DATE, s.EndDateUTC) AS RecordDate ,
                            s.SiteID ,
                            d.DepartmentID ,
                            COUNT(s.ROW_ID) AS SessionCount ,
                            AVG(s.SessionLengthMinutes) / 60 AS AvgSessionLengthHours ,
                            AVG(s.SessionLengthMinutes) % 60 AS AvgSessionLengthMinutes ,
                            AVG(s.RunRate) AS AvgRunRate
                   FROM     dbo.Sessions s WITH ( NOLOCK )
                            LEFT JOIN dbo.Assets d WITH ( NOLOCK ) ON s.DeviceSerialNumber = d.SerialNo
                   WHERE    d.Retired = 0
                            AND s.EndDateUTC BETWEEN @FromDate AND @ToDate
                   GROUP BY CONVERT(DATE, s.EndDateUTC) ,
                            s.SiteID ,
                            d.DepartmentID
                 )
        INSERT  INTO dbo.SummaryTrending
                ( [Year] ,
                  [Month] ,
                  [Day] ,
                  [Date] ,
                  SiteID ,
                  DepartmentID ,
                  SessionCount ,
                  AvgSessionLengthHours ,
                  AvgSessionLengthMinutes ,
                  AvgRunRate ,
                  Utilization ,
                  NonSessionRecordCount
	            )
                SELECT  YEAR(ss.RecordDate) ,
                        MONTH(ss.RecordDate) ,
                        DAY(ss.RecordDate) ,
                        ss.RecordDate ,
                        ss.SiteID ,
                        ss.DepartmentID ,
                        ss.SessionCount ,
                        ss.AvgSessionLengthHours ,
                        ss.AvgSessionLengthMinutes ,
                        ss.AvgRunRate ,
                        0 ,
                        nss.NonSessionRecordCount
                FROM    tmpSessionSummary ss WITH ( NOLOCK )
                        INNER JOIN #tmpNonSessionSummary nss WITH ( NOLOCK ) ON ss.RecordDate = ss.RecordDate
                                                              AND ss.SiteID = nss.SiteID
                                                              AND ss.DepartmentID = nss.DepartmentID
                ORDER BY ss.RecordDate ,
                        ss.SiteID ,
                        ss.DepartmentID;
    DROP TABLE #tmpNonSessionSummary;

    PRINT dbo.GetLapTime(@LapTimer);

/*################################################################################################*/
    Step_3:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 3';
    RAISERROR('Populate MovementDetail',0,1) WITH NOWAIT;				
/*################################################################################################*/

    DELETE  FROM dbo.MovementDetail
    WHERE   [Date] BETWEEN @FromDate AND @ToDate;


    INSERT  INTO dbo.MovementDetail
            ( SiteID ,
              DepartmentID ,
              SerialNo ,
              [Date] ,
              [Hour] ,
              Moves
            )
            SELECT  d.SiteID ,
                    d.DepartmentID ,
                    sd.DeviceSerialNumber ,
                    CONVERT(DATE, sd.CreatedDateUTC) AS [Date] ,
                    DATEPART(hh, sd.CreatedDateUTC) AS Hour ,
                    COUNT(sd.Move) AS Moves
            FROM    dbo.SessionDataCurrent AS sd WITH ( NOLOCK )
                    INNER JOIN dbo.Assets AS d WITH ( NOLOCK ) ON sd.DeviceSerialNumber = d.SerialNo
                    LEFT JOIN dbo.MovementDetail AS md WITH ( NOLOCK ) ON d.SiteID = md.SiteID
                                                              AND d.DepartmentID = md.DepartmentID
                                                              AND d.SerialNo = md.SerialNo
                                                              AND CONVERT(DATE, sd.CreatedDateUTC) = md.Date
                                                              AND DATEPART(hh,
                                                              sd.CreatedDateUTC) = md.Hour
            WHERE   sd.Move > 0
                    AND md.ROW_ID IS NULL
                    AND d.Retired = 0
                    AND sd.CreatedDateUTC BETWEEN @FromDate AND @ToDate
            GROUP BY d.SiteID ,
                    d.DepartmentID ,
                    sd.DeviceSerialNumber ,
                    CONVERT(DATE, sd.CreatedDateUTC) ,
                    DATEPART(hh, sd.CreatedDateUTC);

    PRINT dbo.GetLapTime(@LapTimer);

/*################################################################################################*/
    Step_4:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 4';
    RAISERROR( 'Populate Movement',0,1) WITH NOWAIT;
/*################################################################################################*/

    PRINT '-- Summarize the rows that were just added to MovementDetail';

    DELETE  FROM dbo.Movement
    WHERE   [Date] BETWEEN @FromDate AND @ToDate;


    INSERT  INTO dbo.Movement
            ( SiteID ,
              DepartmentID ,
              DeviceSerialNumber ,
              [Date] ,
              HoursUtilized ,
              Utilization
		    )
            SELECT  dm.SiteID ,
                    dm.DepartmentID ,
                    dm.SerialNo ,
                    dm.Date ,
                    dm.Moves AS HoursUtilized ,
                    CONVERT(DECIMAL(5,2),( CONVERT(FLOAT, dm.Moves) / 24 )) AS Utilization
	
            FROM    ( SELECT    SiteID ,
                                DepartmentID AS DepartmentID ,
                                SerialNo ,
                                [Date] ,
                                COUNT(Moves) AS Moves
                      FROM      dbo.MovementDetail
                      WHERE     Moves > 0
                                AND Moves IS NOT NULL -- more than one move in a given hour
                      GROUP BY  SiteID ,
                                DepartmentID ,
                                SerialNo ,
                                [Date]
                    ) dm -- DeviceMovementByDate
      

    PRINT dbo.GetLapTime(@LapTimer);

/*################################################################################################*/
    Step_5:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(15), @LapTimer, 3) + ' Begin Step 5';
    RAISERROR ('Update Movement.AvgRunRate',0,1) WITH NOWAIT;
/*################################################################################################*/

    PRINT '-- update AvgRunRate column on Movement table so we can know AvgRunRate at the device/day level (needed for Utilization Trending, when user selects specific workstations)';

    UPDATE  dbo.Movement
    SET     AvgRunRate = s.AvgRunRate
    FROM    ( SELECT    CONVERT(DATE, EndDateUTC) AS EndDateUTC ,
                        DeviceSerialNumber ,
                        ISNULL(AVG(RunRate), 0) AS AvgRunRate
              FROM      dbo.Sessions
              WHERE     EndDateUTC > @LastSummarized
              GROUP BY  CONVERT(DATE, EndDateUTC) ,
                        DeviceSerialNumber
            ) s
    WHERE   s.EndDateUTC = Date
            AND s.DeviceSerialNumber = Movement.DeviceSerialNumber
            AND (Movement.AvgRunRate = 0 OR Movement.AvgRunRate IS NULL);
    PRINT dbo.GetLapTime(@LapTimer);

/*################################################################################################*/
    Step_6:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(15), @LapTimer, 3) + ' Begin Step 6';
    RAISERROR(' Update SummaryTrending.ActiveDeviceCount',0,1) WITH NOWAIT;
							

/*################################################################################################*/

    PRINT '-- update ActiveDeviceCount column on SummaryTrending';
    UPDATE  dbo.SummaryTrending
    SET     ActiveDeviceCount = COALESCE(q.ActiveDeviceCount, 0)
    FROM    ( SELECT    d.SiteID ,
                        d.DepartmentID ,
                        CONVERT(DATE, sd.CreatedDateUTC) AS [Date] ,
                        COUNT(DISTINCT sd.DeviceSerialNumber) AS ActiveDeviceCount
              FROM      dbo.SessionDataCurrent sd
                        INNER JOIN dbo.Assets d ON sd.DeviceSerialNumber = d.SerialNo
              WHERE     sd.CreatedDateUTC BETWEEN @FromDate AND @ToDate
                        AND d.Retired = 0
              GROUP BY  d.SiteID ,
                        d.DepartmentID ,
                        CONVERT(DATE, sd.CreatedDateUTC)
            ) q
    WHERE   SummaryTrending.SiteID = q.SiteID
            AND SummaryTrending.DepartmentID = q.DepartmentID
            AND SummaryTrending.Date = q.Date;
    PRINT dbo.GetLapTime(@LapTimer);

/*################################################################################################*/
    Step_7:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(15), @LapTimer, 3) + ' Begin Step 7';
    RAISERROR ('Update SummaryTrending.Utilization',0,1) WITH NOWAIT;					

/*################################################################################################*/

    UPDATE  dbo.SummaryTrending
    SET     Utilization = CAST(( CAST(HoursUtilized AS DECIMAL(8, 4))
                                 / TotalHours ) AS DECIMAL(6, 4))
    FROM    ( SELECT    m.SiteID ,
                        m.DepartmentID ,
                        m.Date ,
                        UtilizedWorkstations.UtilizedWorkstationCount * 24 AS TotalHours ,
                        SUM(m.HoursUtilized) AS HoursUtilized
              FROM      dbo.Movement m
                        JOIN ( SELECT   SiteID ,
                                        DepartmentID ,
                                        [Date] ,
                                        COUNT(ROW_ID) AS UtilizedWorkstationCount
                               FROM     dbo.Movement --WITH (INDEX(IX_Movement)) 
                               WHERE    [Date] BETWEEN @FromDate AND @ToDate
                                        AND HoursUtilized > 0
                               GROUP BY SiteID ,
                                        DepartmentID ,
                                        [Date]
                             ) UtilizedWorkstations ON m.Date = UtilizedWorkstations.Date
                                                       AND m.SiteID = UtilizedWorkstations.SiteID
                                                       AND m.DepartmentID = UtilizedWorkstations.DepartmentID
              WHERE     m.Date BETWEEN @FromDate AND @ToDate
              GROUP BY  m.SiteID ,
                        m.DepartmentID ,
                        m.Date ,
                        UtilizedWorkstations.UtilizedWorkstationCount * 24
            ) asdf
    WHERE   SummaryTrending.SiteID = asdf.SiteID
            AND SummaryTrending.DepartmentID = asdf.DepartmentID
            AND SummaryTrending.Date = asdf.Date
            AND SummaryTrending.Date BETWEEN @FromDate
                                       AND     @ToDate;

    PRINT dbo.GetLapTime(@LapTimer);

/*################################################################################################*/
    Step_8:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(15), @LapTimer, 3) + ' Begin Step 8';
    RAISERROR( 'Populate SummarySiteAssets',0,1) WITH NOWAIT;
                                    
/*################################################################################################*/
    DECLARE @MostRecentSummaryActiveDevicesDate DATETIME;

    SELECT  @MostRecentSummaryActiveDevicesDate = MAX(CreatedDateUTC)
    FROM    dbo.SummaryActiveAssets;

    PRINT '--Delete all records from SummarySiteAssets';

    DELETE  FROM dbo.SummarySiteAssets
    WHERE   CreatedDateUTC > @MostRecentSummaryActiveDevicesDate;

    PRINT '--Insert into SummarySiteAssets';

    INSERT  INTO dbo.SummarySiteAssets
            ( IDCustomer ,
              IDSite ,
              IDDepartment ,
              WorkstationCount ,
              ChargerCount ,
              BatteryCount ,
              ActiveWorkstationCount ,
              ActiveChargerCount ,
              ActiveBatteryCount
            )
            SELECT  bu.CustomerID ,
                    a.SiteID ,
                    a.DepartmentID ,
                    a.WorkstationCount ,
                    a.ChargerCount ,
                    a.BatteryCount ,
                    COALESCE(b.ActiveWorkstationCount, 0) AS ActiveWorkstationCount ,
                    COALESCE(b.ActiveChargerCount, 0) AS ActiveChargerCount ,
                    COALESCE(b.ActiveBatteryCount, 0) AS ActiveBatteryCount
            FROM    ( SELECT    d.SiteID ,
                                d.DepartmentID ,
                                SUM(CASE WHEN d.IDAssetType IN ( 1, 3, 6 )
                                         THEN 1
                                         ELSE 0
                                    END) AS WorkstationCount ,
                                SUM(CASE WHEN d.IDAssetType IN ( 2, 4 ) THEN 1
                                         ELSE 0
                                    END) AS ChargerCount ,
                                SUM(CASE WHEN d.IDAssetType = 5 THEN 1
                                         ELSE 0
                                    END) AS BatteryCount
                      FROM      dbo.Assets d
                      WHERE     d.Retired = 0
                      GROUP BY  d.SiteID ,
                                d.DepartmentID
                    ) a
                    LEFT JOIN ( SELECT  *
                                FROM    dbo.SummaryActiveAssets
                                WHERE   CreatedDateUTC = @MostRecentSummaryActiveDevicesDate
                              ) b ON a.SiteID = b.SiteID
                                     AND a.DepartmentID = b.DepartmentID
                    LEFT JOIN dbo.Sites bu ON a.SiteID = bu.IDSite;

    PRINT dbo.GetLapTime(@LapTimer);
/*################################################################################################*/
    Step_9:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(15), @LapTimer, 3) + ' Begin Step 9';
    RAISERROR ('UpdateSummarySiteAssets',0,1) WITH NOWAIT;
					

/*################################################################################################*/

    DECLARE @CreatedDateUTC DATETIME;
    SET @CreatedDateUTC = CONVERT(DATE, GETDATE());
	
	
	
    UPDATE  dbo.SummarySiteAssets
    SET     DormantBatteryCount = saa.DormantBatteryCount ,
            CountFullBatteries = saa.CountFullBatteries ,
            BatteryInWorkstationCount = saa.BatteryInWorkstationCount ,
            BatteryInChargerCount = saa.BatteryInChargerCount ,
            VacantChargerBays = saa.VacantChargerBays ,
            ActiveChargerBays = saa.ActiveChargerBays ,
            InServiceWorkstationCount = saa.InServiceWorkstationCount ,
            AvailableWorkstationCount = saa.AvailableWorkstationCount ,
            DormantWorkstationCount = saa.DormantWorkstationCount ,
            BayCount = saa.BayCount
    FROM    ( SELECT    sa.SiteID ,
                        sa.DepartmentID ,
                        sa.DormantBatteryCount ,
                        sa.CountFullBatteries ,
                        sa.BatteryInWorkstationCount ,
                        sa.BatteryInChargerCount ,
                        sa.VacantChargerBays ,
                        sa.ActiveChargerBays ,
                        sa.InServiceWorkstationCount ,
                        sa.AvailableWorkstationCount ,
                        sa.DormantWorkstationCount ,
                        sa.BayCount
              FROM      dbo.SummaryActiveAssets sa
              WHERE     sa.CreatedDateUTC = @CreatedDateUTC
            ) saa
    WHERE   IDSite = saa.SiteID
            AND IDDepartment = saa.DepartmentID;


    PRINT dbo.GetLapTime(@LapTimer);
/*################################################################################################*/
Step_10:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(15), @LapTimer, 3) + ' Begin Step 10';
    RAISERROR ('Build_SummarySiteDepartment',0,1) WITH NOWAIT;
					
  PRINT dbo.GetLapTime(@LapTimer);
/*################################################################################################*/
    PRINT '	-- required for CAST 2.0 landing page';
    EXEC dbo.Build_SummarySiteDepartment;

/*################################################################################################*/
    Step_11:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(15), @LapTimer, 3) + ' Begin Step 11';
    RAISERROR ('Build_SummaryWorkstation',0,1) WITH NOWAIT;
					
    PRINT dbo.GetLapTime(@LapTimer);
/*################################################################################################*/
    PRINT '	-- required for CAST 2.0 landing page';
    EXEC dbo.Build_SummaryWorkstation;
    
/*################################################################################################*/
    Step_12:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(15), @LapTimer, 3) + ' Begin Step 10';
    RAISERROR ('Build_SummaryTotal',0,1) WITH NOWAIT;
					
    PRINT dbo.GetLapTime(@LapTimer);
/*################################################################################################*/
    PRINT '	-- required for CAST 2.0 landing page';
    EXEC dbo.Build_SummaryTotals;

    PRINT dbo.GetLapTime(@LapTimer);
/*################################################################################################*/
    Step_13:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(15), @LapTimer, 3) + ' Begin Step 11';
    RAISERROR ('spSummary_BatteryRunRate_Append',0,1) WITH NOWAIT;

/*################################################################################################*/
    PRINT '-- append battery run rate rows to SummaryBatteryRunRate';

    EXEC dbo.spSummary_BatteryRunRate_Append;

PRINT dbo.GetLapTime(@LapTimer);
PRINT 'Combined Steps ' + dbo.GetLapTime(@ProcessTimer);



GO
