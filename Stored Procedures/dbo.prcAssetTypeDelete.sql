SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcAssetTypeDelete] @IDAssetType INT
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
    BEGIN TRAN;

    DELETE  FROM dbo.AssetType
    WHERE   [IDAssetType] = @IDAssetType;

    COMMIT;



GO
