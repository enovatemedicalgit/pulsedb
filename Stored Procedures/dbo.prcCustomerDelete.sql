SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcCustomerDelete] @IDCustomer VARCHAR(7)
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
    BEGIN TRAN;

    DELETE  FROM dbo.Customers
    WHERE   [IDCustomer] = CAST(@IDCustomer AS INT);

    COMMIT;



GO
