SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  CREATE PROCEDURE [dbo].[prcDashGetAmpsDrawnPerHourSummary] (
  @SiteID INT = 238 , @LookbackHours INT = 24
  )
  AS
      
	SELECT DATEDIFF(Hour, s.EndDateUTC, GETUTCDATE())  AS ampHour,  CONVERT(DECIMAL(3,2),SUM(-.001*s.AvgAmpDraw)/COUNT(*)) AS AvgAmpDraw, s.SiteID, a.DepartmentId
	FROM Sessions s 
	INNER JOIN Assets a	
		ON a.SerialNo = s.DeviceSerialNumber
	WHERE
		s.SiteID = @SiteId
	AND
	DATEDIFF(Hour, EndDateUTC, GETUTCDATE()) BETWEEN 1 AND  @LookbackHours 
	AND EXISTS (SELECT * FROM AssetType at WHERE at.IDAssetType =  a.IdAssetType  AND at.AssetCategoryId = 1)
	GROUP BY
		DATEDIFF(Hour, s.EndDateUTC, GETUTCDATE()),
		a.DepartmentId,
	    s.SiteID
	ORDER BY 
		DATEDIFF(Hour, s.EndDateUTC, GETUTCDATE()) DESC;
GO
