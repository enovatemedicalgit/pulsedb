SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcAssetsMove]
    @SerialList NVARCHAR(3000) ,
    @SiteID INT
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
    BEGIN TRAN;
    DECLARE @SQLs NVARCHAR(MAX);
    DECLARE @SQLSelect NVARCHAR(MAX);
    SET @SQLs = 'update [dbo].[Assets] SET SiteID ='
        + CAST(@SiteID AS NVARCHAR) + ' WHERE serialno in (' + @SerialList
        + ')';
    PRINT @SQLs;

	--UPDATE [dbo].[Assets]
	--SET   SiteID = 1125
	--WHERE  serialno in (@SerialList)
    EXEC sys.sp_executesql @SQLs;
    COMMIT;
    SELECT  @SQLs; 



GO
