SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcAssetsDelete] @IDAsset INT
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
    BEGIN TRAN;

    DELETE  FROM dbo.Assets
    WHERE   [IDAsset] = @IDAsset;

    COMMIT;



GO
