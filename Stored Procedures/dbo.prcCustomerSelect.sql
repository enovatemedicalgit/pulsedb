SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcCustomerSelect] @IDCustomer INT = NULL
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
    IF ( @IDCustomer = 4 )
        BEGIN
            SELECT  [IDCustomer] ,
                    [CustomerName]
            FROM    dbo.Customers;
        END;
    ELSE
        BEGIN 
            SELECT  [IDCustomer] ,
                    [CustomerName]
            FROM    dbo.Customers
            WHERE   [IDCustomer] = @IDCustomer; 
        END;



GO
