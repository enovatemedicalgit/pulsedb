SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[ELMAH_GetErrorsXml]
    (
      @Application NVARCHAR(60) ,
      @PageIndex INT = 0 ,
      @PageSize INT = 15 ,
      @TotalCount INT OUTPUT
    )
AS
    SET NOCOUNT ON;

    DECLARE @FirstTimeUTC DATETIME;
    DECLARE @FirstSequence INT;
    DECLARE @StartRow INT;
    DECLARE @StartRowIndex INT;

    SELECT  @TotalCount = COUNT(1)
    FROM    dbo.ELMAH_Error
    WHERE   [Application] = @Application;

    -- Get the ID of the first error for the requested page

    SET @StartRowIndex = @PageIndex * @PageSize + 1;

    IF @StartRowIndex <= @TotalCount
        BEGIN

            SET ROWCOUNT @StartRowIndex;

            SELECT  @FirstTimeUTC = [TimeUtc] ,
                    @FirstSequence = [Sequence]
            FROM    dbo.ELMAH_Error
            WHERE   [Application] = @Application
            ORDER BY [TimeUtc] DESC ,
                    [Sequence] DESC;

        END;
    ELSE
        BEGIN

            SET @PageSize = 0;

        END;

    -- Now set the row count to the requested page size and get
    -- all records below it for the pertaining application.

    SET ROWCOUNT @PageSize;

    SELECT  errorId = error.ErrorId ,
            application = error.Application ,
            host = error.Host ,
            type = error.Type ,
            source = error.Source ,
            message = error.Message ,
            [user] = error.[User] ,
            statusCode = error.StatusCode ,
            time = CONVERT(VARCHAR(50), error.TimeUtc, 126) + 'Z'
    FROM    dbo.ELMAH_Error error
    WHERE   [application] = @Application
            AND error.TimeUtc <= @FirstTimeUTC
            AND error.Sequence <= @FirstSequence
    ORDER BY error.TimeUtc DESC ,
            error.Sequence DESC
    FOR     XML AUTO;


GO
