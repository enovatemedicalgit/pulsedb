SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[prcDashBatteryWarrantyCalendar]
@intSiteId INT = 693
AS

DECLARE @SiteId INT;
SET @SiteId = @intSiteId;


SELECT ass.SerialNo  AS title
	,DATEADD(MINUTE,1,DATEADD(DAY, ISNULL((
				SELECT DurationDays
				FROM dbo.WarrantyDuration
				WHERE SerialNumberPrefix = LEFT(ass.SerialNo, 7)
				), 5000), CAST('20' + SUBSTRING(ass.SerialNo, 8, 2) + '-' + SUBSTRING(ass.SerialNo, 10, 2) + '-01' AS DATETIME))) AS [start]
	,DATEADD(MINUTE, - 1, DATEADD(DAY, ISNULL((
					SELECT DurationDays
					FROM dbo.WarrantyDuration
					WHERE SerialNumberPrefix = LEFT(ass.SerialNo, 7)
					), 5000) + 1, CAST('20' + SUBSTRING(ass.SerialNo, 8, 2) + '-' + SUBSTRING(ass.SerialNo, 10, 2) + '-01' AS DATETIME))) AS [end]
	,ISNULL(NULLIF(dbo.fnGetBatteryCapacity_Int(ass.SerialNo, ass.FullChargeCapacity), 0), 0) AS [Capacity Health]
FROM dbo.Assets ass
LEFT JOIN dbo.Sites s
	ON s.IDSite = ass.SiteID
LEFT JOIN dbo.Customers cus
	ON ass.SiteID = cus.IDCustomer
LEFT JOIN dbo.WarrantyDuration wd
	ON wd.SerialNumberPrefix = LEFT(ass.SerialNo, 7)
WHERE ass.LastPostDateUTC > DATEADD(DAY, - 30, GETDATE())
	AND ass.IDAssetType = 5
	AND dbo.SerialNumberClassification(ass.SerialNo) = 1
	AND ass.MaxCycleCount <> 0
	AND ass.MaxCycleCount <> 10
	AND ass.SiteID = @SiteId
	AND ass.FullChargeCapacity > 0
	AND ass.Retired = 0
	AND ass.CycleCount < ass.MaxCycleCount
	AND GETUTCDATE() < DATEADD(DAY, ISNULL((
				SELECT DurationDays
				FROM dbo.WarrantyDuration
				WHERE SerialNumberPrefix = LEFT(ass.SerialNo, 7)
				), 5000), CAST('20' + SUBSTRING(ass.SerialNo, 8, 2) + '-' + SUBSTRING(ass.SerialNo, 10, 2) + '-01' AS DATETIME))





GO
