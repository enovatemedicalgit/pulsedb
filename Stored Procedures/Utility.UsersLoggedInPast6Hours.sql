SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [Utility].[UsersLoggedInPast6Hours]
AS
BEGIN
	    /**************************************************	 Users Logged in Past 6Hours	    **************************************************/

	    SELECT
			 SUM(TotalUsers) AS   TotalUsers
		    , SUM(TotalEmUsers) AS TotalEMUsers
		    , SUM(TotalSites) AS   TotalSites
	    FROM
	    (
		   SELECT
				COUNT(DISTINCT email) AS  TotalUsers
			   , 0 AS                      TotalEmUsers
			   , COUNT(DISTINCT IDSite) AS TotalSites
		   FROM
			   dbo.[User]
		   WHERE   LastLoginDateUTC > DATEADD(HOUR, -6, GETUTCDATE())
				 AND UserTypeId = 0
		   UNION
		   SELECT
				0 AS                      TotalUsers
			   , COUNT(DISTINCT email) AS  TotalEmUsers
			   , COUNT(DISTINCT IDSite) AS TotalSites
		   FROM
			   dbo.[User]
		   WHERE  LastLoginDateUTC > DATEADD(HOUR, -6, GETUTCDATE())
				AND UserTypeId = 1
							   ) AS base;

END
GO
