SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--==========================================================
-- By: Mike Jones and Arlow Farrell
-- Create date: 5/20/2015
-- Description:	Gets summary data for the supplied
--              dates and business unit

-- Appended the parameter list and changed 
--the date option to a range. Removed extras and alloted for
--nulls on the AvgPCUtilizaiton...
-- ===========================================================

--TEST variables:
--declare @IDSite int = 257
--declare @StartDate varchar(10) = '2012/11/01'
--declare @EndDate varchar(10) = '2012/12/01'

CREATE PROCEDURE [dbo].[spSummary_GetSiteDepartmentList_DateRange]
    (
      @StartDate VARCHAR(10) ,
      @EndDate VARCHAR(10) ,
      @IDSite INT
    )
AS
    BEGIN
        SET NOCOUNT ON;
   
        SELECT  bud.Description AS Department ,
                @IDSite AS IDSite ,
                a.* ,
                COALESCE(b.AvgEstimatedPCUtilization, 0) AS AvgEstimatedPCUtilization
        FROM    ( SELECT    [Date] ,
                            IDDepartment ,
                            SUM(ActiveWorkstationCount) AS ActiveWorkstationCount ,
                            SUM(AvailableWorkstationCount) AS AvailableWorkstationCount ,
                            SUM(OfflineWorkstationCount) AS OfflineWorkstationCount ,
                            AVG(AvgAmpDraw) AS AvgAmpDraw
                  FROM      dbo.SummarySiteDepartment
                  WHERE     [Date] BETWEEN @StartDate AND @EndDate
                            AND IDSite = @IDSite
                  GROUP BY  [Date] ,
                            IDDepartment
                ) a
                JOIN ( SELECT   [Date] ,
                                IDDepartment ,
                                AVG(AvgEstimatedPCUtilization) AS AvgEstimatedPCUtilization
                       FROM     dbo.SummarySiteDepartment
                       WHERE    AvgEstimatedPCUtilization != 0
                                AND [Date] BETWEEN @StartDate AND @EndDate
                                AND IDSite = @IDSite
                       GROUP BY [Date] ,
                                IDDepartment
                     ) b ON a.Date = b.Date
                            AND a.IDDepartment = b.IDDepartment
                JOIN dbo.Departments bud ON a.IDDepartment = bud.IDDepartment
        ORDER BY bud.Description;
    END;








GO
