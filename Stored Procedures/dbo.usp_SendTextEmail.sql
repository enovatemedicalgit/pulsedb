SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_SendTextEmail]
    @ServerAddr NVARCHAR(128) ,
    @From NVARCHAR(128) ,
    @To NVARCHAR(1024) ,
    @Subject NVARCHAR(256) ,
    @Bodytext NVARCHAR(MAX) = 'This is a test text email from MS SQL server, do not reply.' ,
    @User NVARCHAR(128) = '' ,
    @Password NVARCHAR(128) = '' ,
    @SSLConnection INT = 0 ,
    @ServerPort INT = 25,
	@ReplyTo NVARCHAR(128) = null
AS
    DECLARE @hr INT;
    DECLARE @oSmtp INT;
    DECLARE @result INT;
    DECLARE @description NVARCHAR(255);
	IF (@ReplyTo IS NULL)
	BEGIN
	SET @ReplyTo = @From;
    end
    EXEC @hr = sys.sp_OACreate 'EASendMailObj.Mail', @oSmtp OUT; 
    IF @hr <> 0
        BEGIN
            PRINT 'Please make sure you have EASendMail Component installed!';
            EXEC @hr = sys.sp_OAGetErrorInfo @oSmtp, NULL, @description OUT;
            IF @hr = 0
                BEGIN
                    PRINT @description;
                END;
            RETURN;
        END;

    EXEC @hr = sys.sp_OASetProperty @oSmtp, 'LicenseCode',
        'ES-C1407722592-00456-1258F7A735A385E6-9E3B7CC1FU68811C';
    EXEC @hr = sys.sp_OASetProperty @oSmtp, 'ServerAddr', @ServerAddr;
    EXEC @hr = sys.sp_OASetProperty @oSmtp, 'ServerPort', @ServerPort;

    EXEC @hr = sys.sp_OASetProperty @oSmtp, 'UserName', @User;
    EXEC @hr = sys.sp_OASetProperty @oSmtp, 'Password', @Password;

    EXEC @hr = sys.sp_OASetProperty @oSmtp, 'FromAddr', @From;
	EXEC @hr = sys.sp_OASetProperty @oSmtp, 'ReplyTo', @ReplyTo;
    EXEC @hr = sys.sp_OAMethod @oSmtp, 'AddRecipientEx', NULL, @To, 0;

    EXEC @hr = sys.sp_OASetProperty @oSmtp, 'Subject', @Subject; 
    EXEC @hr = sys.sp_OASetProperty @oSmtp, 'BodyText', @Bodytext; 


    IF @SSLConnection > 0
        BEGIN
            EXEC @hr = sys.sp_OAMethod @oSmtp, 'SSL_init', NULL;
        END;

/* you can also add an attachment like this */
/*EXEC @hr = sp_OAMethod @oSmtp, 'AddAttachment', @result OUT, 'd:\test.jpg'*/
/*If @result <> 0 */
/*BEGIN*/
/*   EXEC @hr = sp_OAMethod @oSmtp, 'GetLastErrDescription', @description OUT*/
/*    PRINT 'failed to add attachment with the following error:'*/
/*    PRINT @description*/
/*END*/


    PRINT 'Start to send email ...'; 
    EXEC @hr = sys.sp_OASetProperty @oSmtp, 'BodyFormat', 1;
    EXEC @hr = sys.sp_OAMethod @oSmtp, 'SendMail', @result OUT; 

    IF @hr <> 0
        BEGIN
            EXEC @hr = sys.sp_OAGetErrorInfo @oSmtp, NULL, @description OUT;
            IF @hr = 0
                BEGIN
                    PRINT @description;
                END;
            RETURN;
        END;

    IF @result <> 0
        BEGIN
            EXEC @hr = sys.sp_OAMethod @oSmtp, 'GetLastErrDescription',
                @description OUT;
            PRINT 'failed to send email with the following error:';
            PRINT @description;
        END;
    ELSE
        BEGIN
            PRINT 'Email was sent successfully!';
        END;

    EXEC @hr = sys.sp_OADestroy @oSmtp;


GO
