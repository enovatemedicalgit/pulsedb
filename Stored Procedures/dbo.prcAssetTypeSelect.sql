SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcAssetTypeSelect] @IDAssetType INT
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  

    BEGIN TRAN;

    SELECT  [IDAssetType] ,
            [Category] ,
            [Description] 
   
    FROM    dbo.AssetType
    WHERE   ( [IDAssetType] = @IDAssetType
              OR @IDAssetType IS NULL
            ); 

    COMMIT;




GO
