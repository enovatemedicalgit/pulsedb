SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[GetTimeBands]
AS
    WITH    DateSequence ( UBound )
              AS ( SELECT   GETDATE() AS UBound
                   UNION ALL
                   SELECT   DATEADD(MINUTE, -15, DateSequence.UBound)
                   FROM     DateSequence
                   WHERE    DateSequence.UBound > DATEADD(WEEK, -2, GETDATE())
                 )
        SELECT  ROW_NUMBER() OVER ( ORDER BY DateSequence.UBound DESC ) AS Recency ,
                DATEADD(MINUTE, -15, DateSequence.UBound) AS LBound ,
                DateSequence.UBound
        FROM    DateSequence
    OPTION  ( MAXRECURSION 1400 );
GO
