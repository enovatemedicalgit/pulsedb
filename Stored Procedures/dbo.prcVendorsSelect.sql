SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcVendorsSelect] @IDVendor INT
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  

    BEGIN TRAN;

    SELECT  [IDVendor] ,
            [VendorName]
    FROM    dbo.Vendors
    WHERE   ( [IDVendor] = @IDVendor
              OR @IDVendor IS NULL
            ); 

    COMMIT;



GO
