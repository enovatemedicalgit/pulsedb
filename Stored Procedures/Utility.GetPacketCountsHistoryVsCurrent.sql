SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Utility].[GetPacketCountsHistoryVsCurrent]
AS
DECLARE
	  @DateFrom DATETIME,
	  @DateTo   DATETIME

SET @DateFrom = DATEADD(MONTH,-12,GETUTCDATE()) 
SET @DateTo = GETUTCDATE()

;
WITH CTE_DateSeries(HourlyDate) AS 
(
	SELECT
		  @DateFrom AS HourlyDate
	UNION ALL
	SELECT
		  DATEADD(Hour, 1, HourlyDate)
	FROM
		CTE_DateSeries
	WHERE  HourlyDate < @DateTo
)	

SELECT
    DATEPART(YEAR, HourlyDate) AS  YearPart,
    DATEPART(MONTH, HourlyDate) AS MonthPart,
    DATEPART(day,  HourlyDate) AS   DayPart,
    DATEPART(HOUR, HourlyDate) AS HourPart
INTO #OneHourBands
FROM
CTE_DateSeries
OPTION( MAXRECURSION 0)

 

SELECT  
     DATEPART(YEAR,CreatedDateUTC) AS YearPart
    ,DATEPART(MONTH,CreatedDateUTC) AS MonthPart
    ,DATEPART(DAY,CreatedDateUTC) AS DayPart
    ,DATEPART(HOUR,CreatedDateUTC) AS HourPart
    , COUNT(ROW_ID) AS Hist_NonSession
INTO 
    #HistNonSessionPacketsHourly
FROM
    PulseHistorical.dbo.NonSessionData
WHERE
    CreatedDateUTC > @DateFrom
GROUP BY
     DATEPART(YEAR,CreatedDateUTC) 
    ,DATEPART(MONTH,CreatedDateUTC) 
    ,DATEPART(DAY,CreatedDateUTC) 
    ,DATEPART(HOUR,CreatedDateUTC) 
ORDER BY
     DATEPART(YEAR,CreatedDateUTC)  
    ,DATEPART(MONTH,CreatedDateUTC) 
    ,DATEPART(DAY,CreatedDateUTC) 
    ,DATEPART(HOUR,CreatedDateUTC) 


SELECT  
     DATEPART(YEAR,CreatedDateUTC) AS YearPart
    ,DATEPART(MONTH,CreatedDateUTC) AS MonthPart
    ,DATEPART(DAY,CreatedDateUTC) AS DayPart
    ,DATEPART(HOUR,CreatedDateUTC) AS HourPart
    , COUNT(ROW_ID) AS Hist_Session
INTO 
    #HistSessionPacketsHourly
FROM
    PulseHistorical.dbo.SessionData
WHERE
    CreatedDateUTC > @DateFrom
GROUP BY
     DATEPART(YEAR,CreatedDateUTC) 
    ,DATEPART(MONTH,CreatedDateUTC) 
    ,DATEPART(DAY,CreatedDateUTC) 
    ,DATEPART(HOUR,CreatedDateUTC) 
ORDER BY
     DATEPART(YEAR,CreatedDateUTC)  
    ,DATEPART(MONTH,CreatedDateUTC) 
    ,DATEPART(DAY,CreatedDateUTC) 
    ,DATEPART(HOUR,CreatedDateUTC) 

SELECT  
     DATEPART(YEAR,CreatedDateUTC) AS YearPart
    ,DATEPART(MONTH,CreatedDateUTC) AS MonthPart
    ,DATEPART(DAY,CreatedDateUTC) AS DayPart
    ,DATEPART(HOUR,CreatedDateUTC) AS HourPart
    , COUNT(ROW_ID) AS Current_NonSession

INTO 
    #CurrentNonSessionPacketsHourly
FROM
    Pulse.dbo.NonSessionDataCurrent
WHERE
    CreatedDateUTC > @DateFrom
GROUP BY
     DATEPART(YEAR,CreatedDateUTC) 
    ,DATEPART(MONTH,CreatedDateUTC) 
    ,DATEPART(DAY,CreatedDateUTC) 
    ,DATEPART(HOUR,CreatedDateUTC) 
ORDER BY
     DATEPART(YEAR,CreatedDateUTC)  
    ,DATEPART(MONTH,CreatedDateUTC) 
    ,DATEPART(DAY,CreatedDateUTC) 
    ,DATEPART(HOUR,CreatedDateUTC) 
SELECT  
     DATEPART(YEAR,CreatedDateUTC) AS YearPart
    ,DATEPART(MONTH,CreatedDateUTC) AS MonthPart
    ,DATEPART(DAY,CreatedDateUTC) AS DayPart
    ,DATEPART(HOUR,CreatedDateUTC) AS HourPart
    , COUNT(ROW_ID) AS Current_Session
INTO
    #CurrentSessionPacketsHourly
FROM
    Pulse.dbo.SessionDataCurrent
WHERE
    CreatedDateUTC > @DateFrom
GROUP BY
     DATEPART(YEAR,CreatedDateUTC) 
    ,DATEPART(MONTH,CreatedDateUTC) 
    ,DATEPART(DAY,CreatedDateUTC) 
    ,DATEPART(HOUR,CreatedDateUTC) 
ORDER BY
     DATEPART(YEAR,CreatedDateUTC)  
    ,DATEPART(MONTH,CreatedDateUTC) 
    ,DATEPART(DAY,CreatedDateUTC) 
    ,DATEPART(HOUR,CreatedDateUTC) 





SELECT
      b.YearPart
    , b.MonthPart
    , b.DayPart
    , b.HourPart
    , COALESCE(hn.Hist_NonSession,0) AS Hist_NonSession
    , COALESCE(hs.Hist_Session,0) AS Hist_Session
    , COALESCE(cn.Current_NonSession,0) AS Current_NonSession
    , COALESCE(cs.Current_Session, 0) AS Current_Session
FROM 
    #OneHourBands b
LEFT JOIN
    #HistNonSessionPacketsHourly hn
    ON
	   b.YearPart = hn.YearPart
	   AND
	   b.MonthPart = hn.MonthPart
	   AND
	   b.DayPart = hn.DayPart
	   AND
	   b.HourPart = hn.HourPart
LEFT JOIN
    #HistSessionPacketsHourly hs
    ON
	   b.YearPart = hs.YearPart
	   AND
	   b.MonthPart = hs.MonthPart
	   AND
	   b.DayPart = hs.DayPart
	   AND
	   b.HourPart = hs.HourPart
LEFT JOIN
    #CurrentNonSessionPacketsHourly cn
    ON
	   b.YearPart = cn.YearPart
	   AND
	   b.MonthPart = cn.MonthPart
	   AND
	   b.DayPart = cn.DayPart
	   AND
	   b.HourPart = cn.HourPart
LEFT JOIN
    #CurrentSessionPacketsHourly cs
    ON
	   b.YearPart = cs.YearPart
	   AND
	   b.MonthPart = cs.MonthPart
	   AND
	   b.DayPart = cs.DayPart
	   AND
	   b.HourPart = cs.HourPart
WHERE
    b.YearPart >= 2016



SELECT 
     base.YearPart
    ,base.MonthPart
    ,base.DayPart
    ,base.QuarterDay
    ,SUM(base.Hist_NonSession) AS Hist_NonSession
    ,SUM(base.Hist_Session) AS Hist_Session
    ,SUM(base.Current_NonSession) AS Current_NonSession
    ,SUM(base.Current_Session) AS Current_Session
FROM
(
SELECT
      b.YearPart
    , b.MonthPart
    , b.DayPart
    , (b.HourPart/6) + 1 AS QuarterDay
    , COALESCE(hn.Hist_NonSession,0) AS Hist_NonSession
    , COALESCE(hs.Hist_Session,0) AS Hist_Session
    , COALESCE(cn.Current_NonSession,0) AS Current_NonSession
    , COALESCE(cs.Current_Session, 0) AS Current_Session
FROM 
    #OneHourBands b
LEFT JOIN
    #HistNonSessionPacketsHourly hn
    ON
	   b.YearPart = hn.YearPart
	   AND
	   b.MonthPart = hn.MonthPart
	   AND
	   b.DayPart = hn.DayPart
	   AND
	   b.HourPart = hn.HourPart
LEFT JOIN
    #HistSessionPacketsHourly hs
    ON
	   b.YearPart = hs.YearPart
	   AND
	   b.MonthPart = hs.MonthPart
	   AND
	   b.DayPart = hs.DayPart
	   AND
	   b.HourPart = hs.HourPart
LEFT JOIN
    #CurrentNonSessionPacketsHourly cn
    ON
	   b.YearPart = cn.YearPart
	   AND
	   b.MonthPart = cn.MonthPart
	   AND
	   b.DayPart = cn.DayPart
	   AND
	   b.HourPart = cn.HourPart
LEFT JOIN
    #CurrentSessionPacketsHourly cs
    ON
	   b.YearPart = cs.YearPart
	   AND
	   b.MonthPart = cs.MonthPart
	   AND
	   b.DayPart = cs.DayPart
	   AND
	   b.HourPart = cs.HourPart
WHERE
    b.YearPart >= 2016
) base
GROUP BY
     base.YearPart
    ,base.MonthPart
    ,base.DayPart
    ,base.QuarterDay
ORDER BY
     base.YearPart
    ,base.MonthPart
    ,base.DayPart
    ,base.QuarterDay


--DROP TABLE #OneHourBands
--DROP TABLE #HistNonSessionPacketsHourly


GO
