SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[prcAddUsername] @Username VARCHAR(70) = NULL
	,@FirstName VARCHAR(70) = NULL
	,@Password NVARCHAR(400) = NULL
	,@LastName VARCHAR(70) = NULL
	,@SiteID VARCHAR(20) = NULL
	,@SiteList VARCHAR(300) = NULL
	,@UserTypeId INT = 0
	,@CustomerId INT = NULL
AS
SET NOCOUNT ON;

DECLARE @UserSiteCount INT;
DECLARE @IDUser INT;
DECLARE @site_table TABLE (
	idx SMALLINT PRIMARY KEY IDENTITY(1, 1)
	,site_id INT
	);

IF (@Username LIKE '%enovatemedical.com%' OR @Username LIKE '%Enovate%')
BEGIN
	SET @SiteID = 1502
	SET @UserTypeId = 1
END

IF @SiteList IS NULL
BEGIN
	IF @CustomerId IS NULL
		AND @SiteId IS NOT NULL
		INSERT INTO @Site_table (site_id)
		VALUES (@SiteId)

	IF @CustomerId IS NOT NULL
		AND @SiteID IS NULL
	BEGIN
		INSERT INTO @site_table (site_id)
		SELECT IdSite AS site_id
		FROM sites
		WHERE CustomerId = @CustomerId

		SET @SiteId = (
				SELECT TOP 1 site_id
				FROM @site_table
				)
	END
END
ELSE
BEGIN
	INSERT INTO @site_table (Site_Id)
	SELECT item
	FROM dbo.SplitStringToTable(@SiteList, ',')

	IF @SiteId IS NULL
		SET @SiteId = (
				SELECT TOP 1 site_id
				FROM @site_table
				)
END

SET @IDUser = (
		SELECT TOP 1 U.IDUser
		FROM dbo.[User] AS U
		WHERE U.UserName = @Username
			OR U.Email = @Username
		);

IF (@IDUser IS NULL)
BEGIN
	INSERT INTO dbo.[User] (
		CreatedDate
		,CreatedDateUTC
		,[Password]
		,UserName
		,Email
		,FirstName
		,LastName
		,IDSite
		,UserTypeID
		)
	VALUES (
		GETDATE()
		,GETUTCDATE()
		,'Xyen2O4zJg0qBMPCEHQBTw=='
		,@Username
		,@Username
		,@FirstName
		,@LastName
		,CAST(@SiteID AS INT)
		,@UserTypeId
		);

	SET @IDUser = (
			SELECT TOP 1 U.IDUser
			FROM dbo.[User] AS U
			WHERE U.UserName = @Username
				OR U.Email = @Username
			);

	INSERT INTO dbo.USEROPTIONS (
		User_ROW_ID
		,OptionKey
		,OptionValue
		)
	VALUES (
		@IDUser
		,-- User_ROW_ID - int
		'CAN_LOGIN'
		,-- OptionKey - varchar(50)
		1 -- OptionValue - bit
		)

	INSERT INTO dbo.USEROPTIONS (
		User_ROW_ID
		,OptionKey
		,OptionValue
		)
	VALUES (
		@IDUser
		,-- User_ROW_ID - int
		'CAN_LOGIN_TO_PULSE'
		,-- OptionKey - varchar(50)
		1 -- OptionValue - bit
		)

	INSERT INTO dbo.USEROPTIONS (
		User_ROW_ID
		,OptionKey
		,OptionValue
		)
	VALUES (
		@IDUser
		,-- User_ROW_ID - int
		'CAN_MAINTAIN_USERS'
		,-- OptionKey - varchar(50)
		1 -- OptionValue - bit
		)

	INSERT INTO dbo.USEROPTIONS (
		User_ROW_ID
		,OptionKey
		,OptionValue
		)
	VALUES (
		@IDUser
		,-- User_ROW_ID - int
		'CAN_MAINTAIN_DEVICES'
		,-- OptionKey - varchar(50)
		1 -- OptionValue - bit
		)
END;
ELSE
BEGIN
	UPDATE dbo.[User]
	SET UserName = ISNULL(@Username, UserName)
		,Email = ISNULL(@Username, UserName)
		,Password = ISNULL(@Password, Password)
		,IdSite = ISNULL(@SiteID, IDSite)
		,FirstName = ISNULL(@FirstName, FirstName)
		,LastName = ISNULL(@LastName, LastName)
	FROM [User] u
	WHERE IDUser = @IDUser;
END

SET @UserSiteCount = (
		SELECT COUNT(User_ROW_ID)
		FROM dbo.UserSites
		WHERE User_ROW_ID = @IDUser
		);

IF (@UserSiteCount > 0)
	DELETE
	FROM dbo.UserSites
	WHERE User_ROW_ID = @IDUser;

INSERT INTO dbo.UserSites (
	SiteID
	,User_ROW_ID
	)
SELECT site_id
	,@IDUser
FROM @Site_Table -- User_ROW_ID - int
GO
