SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*################################################################################################
 Name				: Build_SummaryWorkstation
 Date				: 
 Author				: christopher.stewart
 Company				: enovate 
 Purpose				: Build SummaryWorkstation 
 Usage				:
 Impact				:
 Required Perm			:   
 Called by			:   
##################################################################################################
 ver		author					date			change  
 1.0		Arlow.Farrell			Unknown			initial part of [spSummarySiteDepartment_RollupNow]
 1.1		Christopher.Stewart		09302016		Optimizition and Comments
#################################################################################################*/

/*##################################################################################################
 STEPS	  Description						 
 1		  

#################################################################################################*/

CREATE PROCEDURE [dbo].[Build_SummaryWorkstation] (@ReportDate DATE = NULL)
AS

--DECLARE @ReportDate DATE = NULL;


DECLARE @Yesterdate DATE;
DECLARE @LapTimer DATETIME2;
IF @ReportDate IS NULL 
SET @Yesterdate =  DATEADD(DAY, -1, GETDATE());
ELSE
SET @YesterDate = @ReportDate
PRINT @Yesterdate;


PRINT dbo.GetMessageBorder();
/*################################################################################################*/
    Step_1:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 1'; 
    RAISERROR('Delete any existing records for ReportDate',0,1) WITH NOWAIT;
							 
/*################################################################################################*/


 
        DELETE  FROM dbo.SummaryWorkstation
        WHERE   CONVERT(date,[Date]) = @Yesterdate;

PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_2:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 2';
    RAISERROR( 'Populate SummaryWorkstation ',0,1) WITH NOWAIT;

/*################################################################################################*/


        INSERT  INTO dbo.SummaryWorkstation
                ( [Date] ,
                  SerialNo ,
                  AssetNumber ,
                  IDSite ,
                  IDDepartment ,
                  [Floor] ,
                  Wing ,
                  Other ,
                  [Status] ,
                  Utilization ,
                  AvgRunRate ,
                  SessionCount ,
                  LoChargeInsertsCount ,
                  HiChargeRemovalsCount ,
                  AvgAmpDraw ,
                  AvgEstimatedPCUtilization
                )
                  SELECT
				    CONVERT(VARCHAR(10),@Yesterdate,111) as [Date],
                        d.SerialNo AS SerialNo ,
                        d.AssetNumber ,
                        d.SiteID AS IDSite ,
                        ISNULL(d.DepartmentID, 0) AS IDDepartment ,
                        COALESCE(d.Floor, '') AS [Floor] ,
                        COALESCE(d.Wing, '') AS Wing ,
                        ' ' AS Other ,
                        CASE WHEN d.AssetStatusID = 2 THEN 'In service'
                             WHEN d.AssetStatusID = 1 THEN 'Available'
                             ELSE 'Offline'
                        END AS [Status] ,
                        COALESCE(m.Utilization, 0) AS Utilization ,
                        COALESCE(m.AvgRunRate, 0) AS AvgRunRate ,
                        COALESCE(counts.SessionCount, 0) AS SessionCount ,
                        COALESCE(counts.LoChargeInsertsCount, 0) AS LoChargeInsertsCount ,
                        COALESCE(counts.HiChargeRemovalsCount, 0) AS HiChargeRemovalsCount ,
                        COALESCE(counts.AvgAmpDraw, 0) AS AvgAmpDraw ,
                        COALESCE(pc.AvgEstimatedPCUtilization,0) AS AvgEstimatedPCUtilization
                FROM    dbo.Assets d
                        JOIN dbo.AssetStatus ds ON d.AssetStatusID = ds.IDAssetStatus
                        LEFT JOIN ( SELECT  DeviceSerialNumber ,
                                            Utilization ,
                                            AvgRunRate
                                    FROM    dbo.Movement
                                    WHERE   [Date] = @Yesterdate
                                  ) m ON d.SerialNo = m.DeviceSerialNumber
                        LEFT JOIN ( SELECT  DeviceSerialNumber ,
                                            AVG(EstimatedPCUtilization) AS AvgEstimatedPCUtilization
                                    FROM    dbo.Sessions
                                    WHERE   @Yesterdate BETWEEN StartDateUTC
                                                        AND    EndDateUTC
                                            AND COALESCE(EstimatedPCUtilization,
                                                         0) != 0
                                    GROUP BY DeviceSerialNumber
                                  ) pc ON d.SerialNo = pc.DeviceSerialNumber
                        LEFT JOIN ( SELECT  s.DeviceSerialNumber ,
                                            COUNT(s.ROW_ID) AS SessionCount ,
                                            SUM(CASE WHEN CONVERT(date, s.StartDateUTC) = @Yesterdate
                                                          AND s.StartChargeLevel < 90
                                                     THEN 1
                                                     ELSE 0
                                                END) AS LoChargeInsertsCount ,
                                            SUM(CASE WHEN CONVERT(Date, s.EndDateUTC) = @Yesterdate
                                                          AND s.EndChargeLevel > 10
                                                     THEN 1
                                                     ELSE 0
                                                END) AS HiChargeRemovalsCount ,
                                            AVG(COALESCE(s.AvgAmpDraw, 0)) AS AvgAmpDraw
                                    FROM    dbo.Sessions s
                                            JOIN dbo.Assets d ON s.DeviceSerialNumber = d.SerialNo
                                    WHERE   @Yesterdate BETWEEN  s.StartDateUTC
                                                        AND    s.EndDateUTC
                                    GROUP BY s.DeviceSerialNumber
                                  ) counts ON d.SerialNo = counts.DeviceSerialNumber
                WHERE   d.IDAssetType IN (
                        SELECT  [IDAssetType]
                        FROM    dbo.AssetType
                        WHERE   Category LIKE '%Workstation%' )
                        AND d.OutOfService = 0
                        AND d.Retired = 0;

    PRINT dbo.GetLapTime(@LapTimer); 


GO
