SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Description:	Summarizes yesterday's counts
--              and adds them to the table
--              SummaryBusinessUnitDepartment
--
--              Also populates SummaryTotals table
--
--              SEE DEPENDENCIES NOTE BELOW
--
-- =============================================
CREATE PROCEDURE [dbo].[spSummarySiteDepartment_RollupNow]
AS
    BEGIN
        SET NOCOUNT ON;
	/*

		Dependencies:
		-------------
			
		1. SummaryTrending table must already be populated with values for yesterday
		
		2. This only summarizes data for yesterday. If any days are skipped, a modified
		   version of this will need to be executed (running only one day at a time).

	*/

	/*  -----------------------------------------------------------------------------------  */
	/*  These values will need to go onto SummaryBusinessUnitDepartment for use with CAST 2  */
	/*  -----------------------------------------------------------------------------------  */

	/*

		Columns on SummaryBusinessUnitDepartment
		----------------------------------------
		[CreatedDateUTC] [datetime] NOT NULL,
		[Date] varchar(10) NOT NULL,
		[ParentBusinessUnitID] [int] NULL,
		[BusinessUnitID] [int] NULL,
		[BusinessUnitDepartmentID] [int] NULL,
		[ActiveWorkstationCount] [int] NULL,
		[AvailableWorkstationCount] [int] NULL,
		[OfflineWorkstationCount] [int] NULL,
		[LoChargeInsertsCount] [int] NULL,
		[HiChargeRemovalsCount] [int] NULL
	*/

        DECLARE @Yesterdate VARCHAR(10);
        SET @Yesterdate = CONVERT(VARCHAR(10), DATEADD(DAY, -1, GETDATE()), 111);
        PRINT @Yesterdate;
	

        DELETE  FROM dbo.SummarySiteDepartment
        WHERE   [Date] = @Yesterdate;

        INSERT  INTO dbo.SummarySiteDepartment
                ( [Date] ,
                  IDCustomer ,
                  IDSite ,
                  IDDepartment ,
                  ActiveWorkstationCount ,
                  AvailableWorkstationCount ,
                  OfflineWorkstationCount
                )
                SELECT  @Yesterdate ,
                        bu.CustomerID ,
                        d.SiteID ,
                        d.DepartmentID ,
                        SUM(CASE WHEN d.AssetStatusID = 2 THEN 1
                                 ELSE 0
                            END) --as ActiveWorkstations
                        ,
                        SUM(CASE WHEN d.AssetStatusID = 1 THEN 1
                                 ELSE 0
                            END) --as AvailableWorkstations
                        ,
                        SUM(CASE WHEN d.AssetStatusID = 0 THEN 1
                                 ELSE 0
                            END) --as OfflineWorkstations
                FROM    dbo.Assets d
                        JOIN dbo.Sites bu ON d.SiteID = bu.IDSite
                WHERE   d.IDAssetType IN ( 1, 3, 6 )
                        AND d.Retired = 0
                        AND d.OutOfService = 0
                GROUP BY bu.CustomerID ,
                        d.SiteID ,
                        d.DepartmentID ,
                        bu.SiteDescription;


        UPDATE  dbo.SummarySiteDepartment
        SET     LoChargeInsertsCount = counts.LoChargeInsertsCount ,
                HiChargeRemovalsCount = counts.HiChargeRemovalsCount ,
                SessionCount = counts.SessionCount ,
                AvgAmpDraw = counts.AvgAmpDraw ,
                AvgChargeLevel = counts.AvgChargeLevel ,
                HiChargeInserts = counts.HiChargeInsertsCount ,
                LoChargeRemovals = counts.LoChargeRemovalsCount ,
                AvgSignalQuality = counts.AvgSignal ,
                RemainingCapacity = counts.RemainingCapacity
        FROM    ( SELECT    s.SiteID ,
                            COALESCE(d.DepartmentID, 0) AS DepartmentID ,
                            COUNT(s.ROW_ID) AS SessionCount ,
                            SUM(CASE WHEN CONVERT(VARCHAR(10), s.StartDateUTC, 111) = @Yesterdate
                                          AND s.StartChargeLevel < 90 THEN 1
                                     ELSE 0
                                END) AS LoChargeInsertsCount ,
                            SUM(CASE WHEN CONVERT(VARCHAR(10), s.EndDateUTC, 111) = @Yesterdate
                                          AND s.EndChargeLevel > 10 THEN 1
                                     ELSE 0
                                END) AS HiChargeRemovalsCount ,
                            AVG(COALESCE(s.AvgAmpDraw, 0)) AS AvgAmpDraw ,
                            AVG(( COALESCE(s.StartChargeLevel, 1)
                                  + COALESCE(s.EndChargeLevel, 1) ) / 2) AS AvgChargeLevel ,
                            SUM(CASE WHEN CONVERT(VARCHAR(10), s.StartDateUTC, 111) = @Yesterdate
                                          AND s.EndChargeLevel < 90 THEN 1
                                     ELSE 0
                                END) AS LoChargeRemovalsCount ,
                            SUM(CASE WHEN CONVERT(VARCHAR(10), s.StartDateUTC, 111) = @Yesterdate
                                          AND s.StartChargeLevel > 90 THEN 1
                                     ELSE 0
                                END) AS HiChargeInsertsCount ,
                            AVG(COALESCE(s.AvgSignalQuality, 0)) AS AvgSignal ,
                            AVG(COALESCE(s.RemainingCapacity, 0)) AS RemainingCapacity
                  FROM      dbo.Sessions s
                            JOIN dbo.Assets d ON s.DeviceSerialNumber = d.SerialNo
                  WHERE     @Yesterdate BETWEEN CONVERT(VARCHAR(10), s.StartDateUTC, 111)
                                        AND     CONVERT(VARCHAR(10), s.EndDateUTC, 111)
                            AND d.OutOfService = 0
                            AND d.Retired = 0
                  GROUP BY  s.SiteID ,
                            d.DepartmentID
                ) counts
        WHERE   IDSite = counts.SiteID
                AND IDDepartment = counts.DepartmentID
                AND Date = @Yesterdate;


	-- update AvgEstimatedPCUtilization column
        UPDATE  dbo.SummarySiteDepartment
        SET     AvgEstimatedPCUtilization = counts.AvgEstimatedPCUtilization
        FROM    ( SELECT    s.SiteID ,
                            COALESCE(d.DepartmentID, 0) AS DepartmentID ,
                            AVG(COALESCE(s.EstimatedPCUtilization, 0)) AS AvgEstimatedPCUtilization
                  FROM      dbo.Sessions s
                            JOIN dbo.Assets d ON s.DeviceSerialNumber = d.SerialNo
                  WHERE     @Yesterdate BETWEEN CONVERT(VARCHAR(10), s.StartDateUTC, 111)
                                        AND     CONVERT(VARCHAR(10), s.EndDateUTC, 111)
                            AND d.OutOfService = 0
                            AND d.Retired = 0
                            AND s.EstimatedPCUtilization IS NOT NULL -- This will only average sessions which have a value (nulls will not impact the average)
                  GROUP BY  s.SiteID ,
                            d.DepartmentID
                ) counts
        WHERE   IDSite = counts.SiteID
                AND IDDepartment = counts.DepartmentID
                AND Date = @Yesterdate; 


	--select * from summarysitedepartment where idsite=238 order by date desc, iddepartment
	--select * from summarytrending where siteid=238 order by date desc, departmentid
        UPDATE  dbo.SummarySiteDepartment
        SET     Utilization = st.Utilization ,
                AvgRunRate = COALESCE(st.AvgRunRate, 0)
        FROM    dbo.SummaryTrending st
        WHERE   IDSite = st.SiteID
                AND IDDepartment = st.DepartmentID
                AND SummarySiteDepartment.Date = @Yesterdate
	---and st.avgrunrate is not null
                AND CONVERT(VARCHAR(10), st.Date, 111) = @Yesterdate;	
	-- temp fix for problem with sessions table that has rows for previous
	--Update SummarySiteDepartment set
	--	  HiChargeRemovalsCount = 0
	--	, SessionCount =0
	--	, AvgAmpDraw = 0
	--	, AvgEstimatedPCUtilization = 0
	--	, AvgChargeLevel = 0
	--	, HiChargeInserts = 0
	--	, LoChargeRemovals = 0
	--	, AvgSignalQuality = 0
	--	,RemainingCapacity = 0 where AvgRunRate < 1 and SessionCount >0


	-- now summarize at the workstation level
        DELETE  FROM dbo.SummaryWorkstation
        WHERE   [Date] = @Yesterdate;

        INSERT  INTO dbo.SummaryWorkstation
                ( [Date] ,
                  SerialNo ,
                  AssetNumber ,
                  IDSite ,
                  IDDepartment ,
                  [Floor] ,
                  Wing ,
                  Other ,
                  [Status] ,
                  Utilization ,
                  AvgRunRate ,
                  SessionCount ,
                  LoChargeInsertsCount ,
                  HiChargeRemovalsCount ,
                  AvgAmpDraw ,
                  AvgEstimatedPCUtilization
                )
                SELECT  @Yesterdate ,
                        d.SerialNo AS SerialNo ,
                        d.AssetNumber ,
                        d.SiteID AS IDSite ,
                        ISNULL(d.DepartmentID, 0) AS IDDepartment ,
                        COALESCE(d.Floor, '') AS [Floor] ,
                        COALESCE(d.Wing, '') AS Wing ,
                        ' ' AS Other ,
                        CASE WHEN d.AssetStatusID = 2 THEN 'In service'
                             WHEN d.AssetStatusID = 1 THEN 'Available'
                             ELSE 'Offline'
                        END AS [Status] ,
                        COALESCE(m.Utilization, 0) AS Utilization ,
                        COALESCE(m.AvgRunRate, 0) AS AvgRunRate ,
                        COALESCE(counts.SessionCount, 0) AS SessionCount ,
                        COALESCE(counts.LoChargeInsertsCount, 0) AS LoChargeInsertsCount ,
                        COALESCE(counts.HiChargeRemovalsCount, 0) AS HiChargeRemovalsCount ,
                        COALESCE(counts.AvgAmpDraw, 0) AS AvgAmpDraw ,
                        COALESCE(pc.AvgEstimatedPCUtilization,0) AS AvgEstimatedPCUtilization
                FROM    dbo.Assets d
                        JOIN dbo.AssetStatus ds ON d.AssetStatusID = ds.IDAssetStatus
                        LEFT JOIN ( SELECT  DeviceSerialNumber ,
                                            Utilization ,
                                            AvgRunRate
                                    FROM    dbo.Movement
                                    WHERE   CONVERT(VARCHAR(10), [Date], 111) = @Yesterdate
                                  ) m ON d.SerialNo = m.DeviceSerialNumber
                        LEFT JOIN ( SELECT  DeviceSerialNumber ,
                                            AVG(EstimatedPCUtilization) AS AvgEstimatedPCUtilization
                                    FROM    dbo.Sessions
                                    WHERE   @Yesterdate BETWEEN CONVERT(VARCHAR(10), StartDateUTC, 111)
                                                        AND   CONVERT(VARCHAR(10), EndDateUTC, 111)
                                            AND COALESCE(EstimatedPCUtilization,
                                                         0) != 0
                                    GROUP BY DeviceSerialNumber
                                  ) pc ON d.SerialNo = pc.DeviceSerialNumber
                        LEFT JOIN ( SELECT  s.DeviceSerialNumber ,
                                            COUNT(s.ROW_ID) AS SessionCount ,
                                            SUM(CASE WHEN CONVERT(VARCHAR(10), s.StartDateUTC, 111) = @Yesterdate
                                                          AND s.StartChargeLevel < 90
                                                     THEN 1
                                                     ELSE 0
                                                END) AS LoChargeInsertsCount ,
                                            SUM(CASE WHEN CONVERT(VARCHAR(10), s.EndDateUTC, 111) = @Yesterdate
                                                          AND s.EndChargeLevel > 10
                                                     THEN 1
                                                     ELSE 0
                                                END) AS HiChargeRemovalsCount ,
                                            AVG(COALESCE(s.AvgAmpDraw, 0)) AS AvgAmpDraw
                                    FROM    dbo.Sessions s
                                            JOIN dbo.Assets d ON s.DeviceSerialNumber = d.SerialNo
                                    WHERE   @Yesterdate BETWEEN CONVERT(VARCHAR(10), s.StartDateUTC, 111)
                                                        AND   CONVERT(VARCHAR(10), s.EndDateUTC, 111)
                                    GROUP BY s.DeviceSerialNumber
                                  ) counts ON d.SerialNo = counts.DeviceSerialNumber
                WHERE   d.IDAssetType IN (
                        SELECT  [IDAssetType]
                        FROM    dbo.AssetType
                        WHERE   Category LIKE '%Workstation%' )
                        AND d.OutOfService = 0
                        AND d.Retired = 0;


	/*            Populate SummaryTotals table with values for yesterday	*/
	
        DELETE  FROM dbo.SummaryTotals
        WHERE   SummaryDate = @Yesterdate;

        INSERT  INTO dbo.SummaryTotals
                ( SummaryDate ,
                  SortKey ,
                  HQID ,
                  SiteID ,
                  BatteryCount_Purchased ,
                  BatteryCount_Active ,
                  ChargerCount_Purchased ,
                  ChargerCount_Active ,
                  WorkstationCount_Active ,
                  WorkstationCount_Purchased
                )
                SELECT  @Yesterdate AS SummaryDate ,
                        1 AS SortKey ,
                        bu.CustomerID AS HQID ,
                        bu.IDSite AS SiteID ,
                        SUM(COALESCE(s.BatteryCount, 0)) AS BatteryCount ,
                        SUM(COALESCE(s.ActiveBatteryCount, 0)) AS ActiveBatteryCount ,
                        SUM(COALESCE(s.ChargerCount, 0)) AS ChargerCount ,
                        SUM(COALESCE(s.ActiveChargerCount, 0)) AS ActiveChargerCount ,
                        0 AS WorkstationCount_Active ,
                        0 AS WorkstationCount_Purchased
                FROM    dbo.Sites bu
                        LEFT JOIN dbo.SummarySiteAssets s ON s.IDSite = bu.IDSite
                WHERE   bu.CustomerID <> 0
                GROUP BY bu.CustomerID ,
                        bu.SiteDescription ,
                        bu.IDSite;

	-- now roll up totals for the HQs (for batteries and chargers, that is)
        INSERT  INTO dbo.SummaryTotals
                ( SummaryDate ,
                  SortKey ,
                  HQID ,
                  BatteryCount_Purchased ,
                  BatteryCount_Active ,
                  ChargerCount_Purchased ,
                  ChargerCount_Active ,
                  WorkstationCount_Active ,
                  WorkstationCount_Purchased
                )
                SELECT  @Yesterdate AS SummaryDate ,
                        0 AS SortKey ,
                        HQID ,
                        SUM(COALESCE(BatteryCount_Purchased, 0)) AS BatteryCount_Purchased ,
                        SUM(COALESCE(BatteryCount_Active, 0)) AS BatteryCount_Active ,
                        SUM(COALESCE(ChargerCount_Purchased, 0)) AS ChargerCount_Purchased ,
                        SUM(COALESCE(ChargerCount_Active, 0)) AS ChargerCount_Active ,
                        0 AS WorkstationCount_Active ,
                        0 AS WorkstationCount_Purchased
                FROM    dbo.SummaryTotals
                WHERE   SummaryDate = @Yesterdate
                GROUP BY HQID;


	-- now get workstation counts from SummaryBusinessUnitDepartment

	--   first for HQs
        UPDATE  dbo.SummaryTotals
        SET     WorkstationCount_Active = a.ActiveWorkstationCount ,
                WorkstationCount_Purchased = COALESCE(a.ActiveWorkstationCount
                                                      + a.AvailableWorkstationCount
                                                      + a.OfflineWorkstationCount,
                                                      0)
        FROM    ( SELECT    [Date] AS SummaryDate ,
                            IDCustomer AS HQID ,
                            NULL AS SiteID ,
                            COALESCE(SUM(ActiveWorkstationCount), 0) AS ActiveWorkstationCount ,
                            COALESCE(SUM(AvailableWorkstationCount), 0) AS AvailableWorkstationCount ,
                            COALESCE(SUM(OfflineWorkstationCount), 0) AS OfflineWorkstationCount
                  FROM      dbo.SummarySiteDepartment
                  WHERE     [Date] = @Yesterdate
                            AND IDCustomer <> 0
                  GROUP BY  [Date] ,
                            IDCustomer
                ) a
        WHERE   SummaryTotals.SummaryDate = a.SummaryDate
                AND SummaryTotals.HQID = a.HQID
                AND SummaryTotals.SiteID IS NULL;

	--   then for Facilities
        UPDATE  dbo.SummaryTotals
        SET     WorkstationCount_Active = a.ActiveWorkstationCount ,
                WorkstationCount_Purchased = COALESCE(a.ActiveWorkstationCount
                                                      + a.AvailableWorkstationCount
                                                      + a.OfflineWorkstationCount,
                                                      0)
        FROM    ( SELECT    [Date] AS SummaryDate ,
                            IDCustomer AS HQID ,
                            IDSite ,
                            COALESCE(SUM(ActiveWorkstationCount), 0) AS ActiveWorkstationCount ,
                            COALESCE(SUM(AvailableWorkstationCount), 0) AS AvailableWorkstationCount ,
                            COALESCE(SUM(OfflineWorkstationCount), 0) AS OfflineWorkstationCount
                  FROM      dbo.SummarySiteDepartment
                  WHERE     [Date] = @Yesterdate
                            AND IDCustomer <> 0
                  GROUP BY  [Date] ,
                            IDCustomer ,
                            IDSite
                ) a
        WHERE   SummaryTotals.SummaryDate = a.SummaryDate
                AND SummaryTotals.HQID = a.HQID
                AND SiteID = a.IDSite;
	
	/*  ******************************************************************* */



	
	
    END;




GO
