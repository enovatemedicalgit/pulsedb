SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Description:	Updates the AvgLinkQuality
--              column on AccessPoint using
--              values from SessionData and
--              NonSessionData.
--              
--              Avg represents the most recent
--             24 Hours.
--              
-- =============================================
CREATE PROCEDURE [dbo].[spAvgLinkQuality_Update]
AS
    BEGIN
        SET NOCOUNT ON;

        IF OBJECT_ID(N'tempdb..#tmpLinkQuality', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tmpLinkQuality;
            END;

        SELECT  APMAC ,
                LinkQuality
        INTO    #tmpLinkQuality
        FROM    dbo.SessionDataCurrent
        WHERE   CreatedDateUTC > DATEADD(DAY, -1, GETUTCDATE())
                AND ISNUMERIC(LinkQuality) = 1
                AND LinkQuality > 10
                AND LEN(LinkQuality) < 4
                AND LinkQuality IS NOT NULL;

        INSERT  INTO #tmpLinkQuality
                ( APMAC ,
                  LinkQuality
                )
                SELECT  APMAC ,
                        LinkQuality
                FROM    dbo.NonSessionDataCurrent
                WHERE   CreatedDateUTC > DATEADD(DAY, -1, GETUTCDATE())
                        AND LEN(LinkQuality) < 4
                        AND LinkQuality > 10
                        AND ISNUMERIC(LinkQuality) = 1
                        AND LinkQuality IS NOT NULL;

        DELETE  FROM #tmpLinkQuality
        WHERE   LEN(LinkQuality) > 3;

        CREATE NONCLUSTERED INDEX IX_tmpLinkQuality ON #tmpLinkQuality (APMAC, LinkQuality);

	-- remove rows with non-integer LinkQuality values
	


	-- reset AvgLinkQuality column so it only has values for access points used within past 7 days
	--update AccessPoint
	--set AvgLinkQuality = null

        UPDATE  dbo.AccessPoint
        SET     AvgLinkQuality = sub.AvgLinkQuality
        FROM    ( SELECT    APMAC ,
                            IIF(AVG(CAST(LinkQuality AS INT)) < 40, AVG(CAST(LinkQuality AS INT))
                            * 1.4, AVG(CAST(LinkQuality AS INT)) * 1.1) AS AvgLinkQuality
                  FROM      #tmpLinkQuality
                  WHERE     APMAC != ''
                  GROUP BY  APMAC
                ) sub
        WHERE   sub.APMAC = MACAddress;

        IF OBJECT_ID(N'tempdb..#tmpLinkQuality', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tmpLinkQuality;
            END;


    END;

GO
