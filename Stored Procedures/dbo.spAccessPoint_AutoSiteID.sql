SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Arlow Farrell
-- Create date: 2012-08-16
-- Description:	Detects business unit id to
--              access points and updates the
--              BusinessUnitID on AccessPoint table
--              based on recent packets received
--              for that access point.
--
--              Then updates existing Access Point
--              BusinessUnitIDs if they appear
--              to be out of sync with Radar
--
--	change log:
--	2014-03-06	Arlow Farrell		Fixed a logic error in the "partition by" section - modified to order by APMAC then SUM(DeviceCount) DESC.
--								This second sort order is the intended idea behind what was trying to be accomplished - establish priority by those who send the most packets.
--								
--								we turned off the auto-flip of accessPoint businessUnit affiliation by commenting out that section of code at the bottom of the proc.
-- =============================================
CREATE PROCEDURE [dbo].[spAccessPoint_AutoSiteID]
AS
    BEGIN
        SET NOCOUNT ON;

        IF ( SELECT COUNT(*)
             FROM   dbo.AccessPoint
             WHERE  SiteID = 0
                    OR SiteID = NULL
           ) > 0
            BEGIN

		-- get APMAC, businessunitid and count of devices currently reporting from each access point
                SELECT  a.APMAC ,
                        d.SiteID AS DeviceBusinessUnitID ,
                        COUNT(DISTINCT a.DeviceSerialNumber) AS DeviceCount
                INTO    #temp
                FROM    dbo.NonSessionDataCurrent a
                        JOIN dbo.Assets d ON a.DeviceSerialNumber = d.SerialNo
                WHERE   a.CreatedDateUTC > DATEADD(HOUR, -24, GETUTCDATE())
                        AND d.IDAssetType IN (1, 2, 4,6 )
                        AND a.APMAC IN ( SELECT ap.MACAddress
                                         FROM   dbo.AccessPoint ap
                                         WHERE  ISNULL(ap.SiteID, 0) = 0 )
                GROUP BY a.APMAC ,
                        d.SiteID;

                INSERT  INTO #temp
                        ( APMAC ,
                          DeviceBusinessUnitID ,
                          DeviceCount
                        )
                        SELECT  a.APMAC ,
                                d.SiteID AS DeviceBusinessUnitID ,
                                COUNT(DISTINCT a.DeviceSerialNumber) AS DeviceCount
                        FROM    dbo.SessionDataCurrent a
                                JOIN dbo.Assets d ON a.DeviceSerialNumber = d.SerialNo
                        WHERE   a.CreatedDateUTC > DATEADD(HOUR, -24,
                                                           GETUTCDATE())
                                AND d.IDAssetType IN ( 1,6)
                                AND a.APMAC IN (
                                SELECT  ap.MACAddress
                                FROM    dbo.AccessPoint ap
                                WHERE   ISNULL(ap.SiteID, 0) = 0 )
                        GROUP BY a.APMAC ,
                                d.SiteID;


                SELECT  APMAC ,
                        DeviceBusinessUnitID ,
                        SUM(DeviceCount) AS DeviceRowCount ,
                        ROW_NUMBER() OVER ( PARTITION BY APMAC ORDER BY APMAC, SUM(DeviceCount) DESC ) AS SelectionPriority
                INTO    #APMACBUIDS
                FROM    #temp
                GROUP BY APMAC ,
                        DeviceBusinessUnitID
                ORDER BY APMAC ,
                        SUM(DeviceCount) DESC ,
                        DeviceBusinessUnitID;

                DROP TABLE #temp;

		-- now set BusinessUnitID for any access point where
		-- the current BusinessUnitID is 0 and we have received packets
		-- where we can get the BusinessUnitID from the device(s) which
		-- most often transmit from this access point

                UPDATE  dbo.AccessPoint
                SET     SiteID = x.DeviceBusinessUnitID ,
                        DepartmentID = 0
                FROM    #APMACBUIDS x
                WHERE   MACAddress = x.APMAC
                        AND x.SelectionPriority = 1
                        AND SiteID = 0;

                DROP TABLE #APMACBUIDS;
            END; -- update AccessPoint rows where BusinessUnitID = 0




	-- Now update AccessPoint rows where the BusinessUnitID does not match 
	-- the BusinessUnitID being logically resolved via Radar (on table BusinessUnitAPMAC)
	--declare @ModifiedDateUTC datetime
	--set @ModifiedDateUTC = GETUTCDATE()

	--insert into AccessPointBusinessUnitChangeLog (AccessPointID, PreviousBusinessUnitID, NewBusinessUnitID, ModifiedDateUTC)
	--SELECT ap.ROW_ID as AccessPointID
	--	,ap.BusinessUnitID as PreviousBusinessUnitID
	--	,a.BusinessUnitID as NewBusinessUnitID
	--	,@ModifiedDateUTC
	--  FROM BusinessUnitAPMAC a 
	--  join AccessPoint ap on a.APMAC = ap.MACAddress
	--  where a.BusinessUnitID != ap.BusinessUnitID
	--  and a.FacilityReliabilityRating >= 90
	--  and coalesce(a.BusinessUnitID, 0) != 0


	--update AccessPoint
	--set BusinessUnitID = updt.NewBusinessUnitID
	--	, BusinessUnitDepartmentID = 0
	--from AccessPointBusinessUnitChangeLog updt
	--where AccessPoint.ROW_ID = updt.AccessPointID
	--and updt.ModifiedDateUTC = @ModifiedDateUTC


    END;
GO
