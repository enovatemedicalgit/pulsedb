SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE  [staging].[AppendSFCasesFromSRO]
AS
/*****************************************************************************************
			 Remember to explain to Arlow why putting last 3 digits of 
			 case number in syteline isn't enough to be accurrate more 
			 than half the time

			 -- LOL - Arlow
*****************************************************************************************/

DECLARE @CurrentRowId AS INT = 1;
DECLARE @CurrentSRONum AS VARCHAR(15) = NULL;
DECLARE @CurrentCaseString AS VARCHAR(55) = NULL;
DECLARE @CurrentCases AS TABLE (string VARCHAR(50)) 


INSERT INTO sfcaseSroXref (SroNumber, SFCaseNumber)
SELECT DISTINCT SroNumber, RIGHT('00' + SfCaseNoString,8) AS SFCaseNumber FROM staging.SroSfIntegration
WHERE ((LEN(RTRIM(LTRIM(sfCaseNoString))) = 8 AND (RTRIM(LTRIM(sfCaseNoString)) LIKE '0016%' OR RTRIM(LTRIM(sfCaseNoString)) LIKE '0015%' OR RTRIM(LTRIM(sfCaseNoString)) LIKE '0014%'))
	 OR
	 (LEN(RTRIM(LTRIM(sfCaseNoString))) = 6 AND (RTRIM(LTRIM(sfCaseNoString)) LIKE '16%' OR RTRIM(LTRIM(sfCaseNoString)) LIKE '15%' OR RTRIM(LTRIM(sfCaseNoString)) LIKE '14%'))
	 OR
	 (LEN(RTRIM(LTRIM(sfCaseNoString))) = 7 AND (RTRIM(LTRIM(sfCaseNoString)) LIKE '016%' OR RTRIM(LTRIM(sfCaseNoString)) LIKE '015%' OR RTRIM(LTRIM(sfCaseNoString)) LIKE '014%'))
	  OR
	 (LEN(RTRIM(LTRIM(sfCaseNoString))) = 9 AND (RTRIM(LTRIM(sfCaseNoString)) LIKE '00016%' OR RTRIM(LTRIM(sfCaseNoString)) LIKE '00015%' OR RTRIM(LTRIM(sfCaseNoString)) LIKE '00014%')))
	 AND
	 NOT EXISTS (SELECT * FROM sfCaseSroXRef x WHERE x.SroNumber = SroNumber AND x.SFCaseNumber = RIGHT('00' + SfCaseNoString,8)) AND ISNUMERIC(sfcaseNoString) = 1

IF  OBJECT_ID('tempdb..#ProcessCaseNumbers') IS NOT NULL
    DROP TABLE #ProcessCaseNumbers

SELECT ROW_NUMBER() OVER ( ORDER BY SroNumber) AS RowId, SroNumber, REPLACE(REPLACE(LTRIM(RTRIM(SfCaseNoString)),'(',''),')','') AS SfCaseNoString 
INTO #ProcessCaseNumbers
FROM  Staging.srosfintegration 
WHERE sroNumber NOT IN (SELECT SroNumber FROM SfCaseSROXref)

PRINT 'Begin While Loop'

WHILE @CurrentRowId <= (SELECT MAX(RowId) FROM #ProcessCaseNumbers)
BEGIN
    SELECT @CurrentSRONum = SroNumber, @CurrentCaseString  = sfCaseNoString
    FROM #ProcessCaseNumbers WHERE RowId = @CurrentRowID

    IF (SELECT COUNT(*) FROM dbo.SplitStringToTable(@CurrentCaseString,','))>1
    INSERT INTO 
	   @CurrentCases (string) 
    SELECT 
	   * FROM dbo.SplitStringToTable(@CurrentCaseString,',')


IF (SELECT COUNT(*) FROM dbo.SplitStringToTable(@CurrentCaseString,' '))>1
    INSERT INTO 
	   @CurrentCases (string) 
    SELECT 
	   * FROM dbo.SplitStringToTable(@CurrentCaseString,' ')

IF  ISNUMERIC(LTRIM(RTRIM(@CurrentCaseString)))=1 AND CHARINDEX(',',@CurrentCaseString) = 0 
    INSERT INTO 
	   @CurrentCases (string) 
    SELECT 
	   LTRIM(RTRIM(@CurrentCaseString))


IF (SELECT COUNT(*) FROM @CurrentCases WHERE IsNumeric(string) =1 AND LEN(string)>=3)>0
    INSERT INTO dbo.SfCaseSroXRef
            (SroNumber, SFCaseNumber)
    SELECT @CurrentSROnum, string 
    FROM @CurrentCases
    WHERE
	   NOT EXISTS (SELECT * FROM sfCaseSroXRef x WHERE x.SroNumber = @CurrentSRONum AND x.SFCaseNumber =  RIGHT('00' + LTRIM(RTRIM(string)),8))
	   AND ISNUMERIC(string) = 1 AND LEN(string)>=3
ELSE
    INSERT INTO dbo.SfCaseSroParsingErrors
            ( SroNumber, SfCaseString )
    SELECT @CurrentSROnum, @CurrentCaseString 

SET    @CurrentRowID = @CurrentRowID + 1
DELETE FROM @CurrentCases
END


UPDATE SfCaseSroXRef
SET SFCaseNumber = RIGHT('00' + SFCaseNumber,8)
WHERE 
    LEN(SFCaseNumber) = 6 AND SFCaseNumber LIKE '1%'




UPDATE SfCaseSroXRef
SET SFCaseNumber =  out.SfCaseNUmber
FROM
SfCaseSROXref
INNER JOIN
(
SELECT SroNumber, MAX(SfCaseNUmber) AS SfCaseNumber From
(
SELECT s.SroNumber, s.SFCaseNumber AS Fragment, LEFT(Ranges.CaseRange,5) + s.SFCaseNumber AS SfCaseNumber
  FROM
dbo.SfCaseSroXRef s
INNER JOIN
 (
  SELECT COUNT(SroNumber) AS TotalSROs, MIN(SroNumber) AS FirstSRONum, MAX(SroNumber) AS LastSroNumber, CaseRange From
  (
  SELECT SroNumber, LEFT(RIGHT( '00' + SFCaseNumber,8),5) + '000' AS CaseRange FROM dbo.SfCaseSroXRef  
  WHERE RIGHT( '00' + SFCaseNumber,8) LIKE '001%' 
  ) r
  GROUP BY CaseRange
  HAVING COUNT(SRONumber) > 25
  ) ranges
ON s.SroNumber BETWEEN Ranges.FirstSroNum AND Ranges.LastSroNumber
WHERE 
    LEN(s.SFCaseNumber) = 3 
) base
GROUP BY SroNumber
) out
ON out.SroNumber = SfCaseSroXRef.SroNumber
WHERE LEN(sfCaseSroXref.SfCaseNumber) = 3


UPDATE SfCaseSroXRef
SET SFCaseNumber =  '00168' + sfCaseNumber
WHERE LEN(SfCaseNumber) = 3


  ;WITH CTE AS
(
    SELECT  *,
            RN = ROW_NUMBER() OVER( PARTITION BY SroNumber,SCSXR.SFCaseNumber ORDER BY SroNumber desc)
    FROM dbo.SfCaseSroXRef  AS SCSXR
)
delete FROM CTE
WHERE RN > 1 




  ;WITH CTE AS
(
    SELECT  *,
            RN = ROW_NUMBER() OVER( PARTITION BY SroNumber,SCSXR.SFCaseNoString,SCSXR.SroCreatedDate ORDER BY SroCreatedDate desc)
    FROM staging.SroSfIntegration  AS SCSXR
)
delete FROM CTE
WHERE RN > 1 


  ;WITH CTE AS
(
    SELECT  *,
            RN = ROW_NUMBER() OVER( PARTITION BY SCSXR.SFCaseNum,SCSXR.CreateDate,SCSXR.sro_num ORDER BY SCSXR.sro_num,SCSXR.ShipDate desc)
    FROM dbo.Cases   AS SCSXR
)
delete FROM CTE
WHERE RN > 1 

GO
