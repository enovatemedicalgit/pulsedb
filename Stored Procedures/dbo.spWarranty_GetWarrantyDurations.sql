SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spWarranty_GetWarrantyDurations]
AS
    BEGIN
	
        SET NOCOUNT ON;
	
        SELECT  WarrantyDurationID AS Prefix_ID ,
                CASE WHEN DeviceType = 5 THEN 'Battery'
                END AS [DevType] ,
                ( DurationDays / 365 ) AS WarType ,
                SerialNumberPrefix AS Prefix
        FROM    dbo.WarrantyDuration; 
	 
    END;

GO
