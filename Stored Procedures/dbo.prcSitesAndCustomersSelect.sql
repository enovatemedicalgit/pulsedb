SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcSitesAndCustomersSelect] @IDSite INT = NULL
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
    BEGIN
        SELECT  [IDSite] ,
                '0' AS 'IsIDN' ,
                [SiteDescription]
        FROM    dbo.Sites
        UNION ALL
        SELECT  [IDCustomer] AS IDSite ,
                '1' AS 'IsIDN' ,
                [CustomerName] AS SiteDescription
        FROM    dbo.Customers
	   ORDER BY SiteDescription

    END;
	
	



GO
