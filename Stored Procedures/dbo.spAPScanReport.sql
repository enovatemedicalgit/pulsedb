SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*################################################################################################
 Name				: spAPScanReport
 Date				: 7/21/2012
 Author				: Arlow.Farrell
 Company			: enovate 
 Purpose			: Return AP's with who imported them and what site(name) they belong to; for checking on field techs.
 Usage				: Used in Rhythm.
##################################################################################################
 ver		author					date			change  
 1.0		Arlow.Farrell			7/21/2012			initial [spAPScanReport]
#################################################################################################*/

/**/
CREATE PROCEDURE [dbo].[spAPScanReport]
AS /**/
    BEGIN
        SELECT TOP 100000
                MAX(LTRIM(s.SiteName)) AS SiteName ,
                MAX(ISNULL(u.UserName,
                           IIF(ap.Manual = 1, 'Admin Import (Customer Supplied)', 'Pulse Detected'))) AS 'Username' ,
                COUNT(ap.MACAddress) AS 'AP Count' ,
                ( SELECT    COUNT(MACAddress)
                  FROM      dbo.AccessPoint
                  WHERE     SiteID = s.IDSite
                ) AS 'Total APs Associated with Site' ,
                CONVERT(VARCHAR(20), CAST(CAST(( SELECT COUNT(MACAddress)
                                                 FROM   dbo.AccessPoint
                                                 WHERE  SiteID = s.IDSite
                                                        AND Manual = 1
                                               ) AS FLOAT)
                / CAST(( SELECT COUNT(MACAddress)
                         FROM   dbo.AccessPoint
                         WHERE  SiteID = s.IDSite
                       ) AS FLOAT) * 100 AS INT)) + '%' AS 'Uploaded from Scans Versus Pulse Detected' ,
                MAX(CAST(CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, ap.ModifiedDateUTC),
                                                        DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS SMALLDATETIME)) AS 'Uploaded (Local Time)'
        FROM    dbo.AccessPoint ap
                LEFT JOIN dbo.Sites s ON ap.SiteID = s.IDSite
                LEFT JOIN dbo.[User] u ON ap.InsertedBy = u.IDUser
        WHERE   s.SiteName IS NOT NULL
                AND s.CustomerID != 4
        GROUP BY ap.Manual ,
                DATEADD(dd, ( DATEDIFF(dd, 0, ap.ModifiedDateUTC) ), 0) ,
                DATEADD(hh, ( DATEDIFF(hh, 0, ap.ModifiedDateUTC) ), 0) ,
                DATEADD(mm, ( DATEDIFF(mm, 0, ap.ModifiedDateUTC) ), 0) ,
                s.CustomerID ,
                s.IDSite
        ORDER BY ap.Manual DESC ,
                MAX(CAST(CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, ap.ModifiedDateUTC),
                                                        DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS SMALLDATETIME)) DESC ,
                s.CustomerID ,
                s.IDSite DESC;
	
    END;



GO
