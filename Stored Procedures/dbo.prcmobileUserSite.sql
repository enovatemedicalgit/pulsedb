SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcmobileUserSite]
    @Username VARCHAR(70) ,
    @Password VARCHAR(77) NULL
AS
    SET NOCOUNT ON;
    BEGIN
        SELECT  ISNULL(s.IDSite, C.IDCustomer) AS SiteId  ,
		 ISNULL(s.IDSite, C.IDCustomer) AS IDSite  ,
                u.CreatedDate ,
                u.CreatedDateUTC ,
                u.UserName ,
                u.Password ,
                u.FirstName ,
                u.LastName ,
                u.Email ,
                u.Title ,
                u.AccessLevel ,
                u.PrimaryPhone ,
                u.OtherPhone ,
                u.Notes ,
                u.UTC_Offset ,
                u.CreatedUserID ,
                u.ModifiedDateUTC ,
                u.IDUser ,
                u.LastLoginDateUTC ,
                u.UserTypeID ,
                s.RegionID ,
                s.SiteName ,
                s.SiteDescription ,
                s.CustomerID ,
                s.IsInternal ,
                s.SiteType ,
                s.Status ,
                s.StatusReason,
				 u.ProfileImage

        INTO    #usr
        FROM    dbo.[User] u
			 LEFT JOIN UserSites us ON u.IdUser = us.User_Row_Id
                LEFT JOIN dbo.Sites s ON us.SiteId = s.IDSite
                LEFT JOIN dbo.Customers AS C ON C.IDCustomer = u.IDSite
        WHERE   u.UserName = @Username
                AND u.Password = @Password;
        DECLARE @iusr INT; 
        SET @iusr = ( SELECT TOP 1
                                U.IDUser
                      FROM      #usr AS U
                    );
			  
        IF ( @@ROWCOUNT > 0 )
            BEGIN	
                DECLARE @cnttbl AS INT;
                SET @cnttbl = ( SELECT  COUNT(US.SessionKey)
                                FROM    dbo.UserSession AS US
                                WHERE   US.UserId = @iusr
                                        AND US.Active = 1
                              );
                IF ( @cnttbl > 0 )
                    BEGIN
                        UPDATE  dbo.UserSession
                        SET     Active = 1 ,
                                ExpirationDateUTC = DATEADD(MINUTE, 45,
                                                            ExpirationDateUTC)
                        WHERE   UserId = @iusr
                                AND Active = 1;
                    END;
                ELSE
                    BEGIN
                        INSERT  INTO dbo.UserSession
                                ( SessionKey ,
                                  UserId ,
                                  CreatedDateUTC ,
                                  Active
			                    )
                        VALUES  ( ( SELECT  NEWID()
                                  ) , -- SessionKey - uniqueidentifier
                                  ( SELECT  U.IDUser
                                    FROM    dbo.[User] AS U
                                    WHERE   U.UserName = @Username
                                            AND U.Password = @Password
                                  ) , -- UserId - int
                                  GETUTCDATE() , -- CreatedDateUTC - datetime
                                  1  -- Active - bit
			                    );
                    END;
            END;
			
        SELECT TOP 1 tusr.*	 ,
                US.SessionKey, us.ExpirationDateUTC
        FROM    #usr tusr
                LEFT JOIN dbo.UserSession AS US ON tusr.IDUser = US.UserId ORDER BY US.CreatedDateUTC desc;
	
    END;
GO
