SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [Utility].[CommunicationOverLastHour]
AS
BEGIN

	    /**************************************************	Communication Over Last Hour	    **************************************************/

	    SELECT
			 COUNT(DISTINCT ROW_ID) AS             PacketsProcessed
		    , COUNT(DISTINCT DeviceSerialNumber) AS DevicesCommunicating
		    , COUNT(DISTINCT SiteId) AS             SitesCommunicating
		    , COUNT(DISTINCT CustomerID) AS         CustomersCommunicating
	    FROM
	    (
		   SELECT
				*
		   FROM
			   dbo.SessionData_Summary
		   UNION ALL
		   SELECT
				*
		   FROM
			   dbo.NonSessionData_Summary
								    ) AS composite
	    INNER JOIN
		    dbo.Assets
	    ON composite.DeviceSerialNumber = assets.SerialNo
	    INNER JOIN
		    dbo.Sites
	    ON Assets.SiteId = Sites.IDSite
	    WHERE  composite.CreatedDateUTC >= DATEADD(HOUR, -1, GETUTCDATE());
END
GO
