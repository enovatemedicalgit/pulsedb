SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcSiteCommissionSeedTest]
    @Username VARCHAR(70) = NULL ,
    @SiteID VARCHAR(20) = 395 ,
    @SnapshotInterval INT = 10 ,
    @TestNumHours INT = 72 ,
    @LeaveAPs INT = 1 ,
    @RemoveChargers INT = 0 ,
    @RemoveManualFlag INT = 0
AS
    SET NOCOUNT ON; 
    DECLARE @TestSiteID AS INT;


    DECLARE @TestStartTime DATETIME2;
    SET @TestStartTime = GETUTCDATE();

    INSERT  INTO dbo.Sites
            ( SiteName ,
              SiteDescription ,
              CustomerID
            )
    VALUES  ( ( SELECT  SiteName + ' Test'
                FROM    dbo.Sites
                WHERE   IDSite = @SiteID
              ) ,
              ( SELECT  SiteName + ' Test'
                FROM    dbo.Sites
                WHERE   IDSite = @SiteID
              ) ,
              1425
            );
    SET @TestSiteID = ( SELECT  IDSite
                        FROM    dbo.Sites
                        WHERE   SiteName LIKE '% Test%'
                                AND CustomerID = 1425
                      );
    RAISERROR ('Test Site Created' , 0, 1) WITH NOWAIT;
    INSERT  INTO dbo.Departments
            ( Description ,
              SiteID ,
              DefaultForSite
            )
            SELECT  Description AS Description ,
                    @TestSiteID AS SiteID ,
                    DefaultForSite
            FROM    dbo.Departments
            WHERE   SiteID = @SiteID;

    INSERT  INTO dbo.Assets
            ( SerialNo ,
              Description ,
              IDAssetType ,
              SiteID ,
              DepartmentID ,
              ManualAllocation ,
              Floor ,
              Wing ,
              APMAC
            )
            SELECT  ass.SerialNo + 'tst' AS SerialNo ,
                    ass.Description + 'tst' AS Description ,
                    ass.IDAssetType ,
                    @TestSiteID ,
                    ( ISNULL(( SELECT TOP 1
                                        IDDepartment
                               FROM     dbo.Departments
                               WHERE    SiteID = @TestSiteID
                                        AND REPLACE(Description, 'tst', '') = ( SELECT TOP 1
                                                              Description
                                                              FROM
                                                              dbo.Departments
                                                              WHERE
                                                              SiteID = @SiteID
                                                              AND Description = REPLACE(dept.Description,
                                                              'tst', '')
                                                              )
                             ), ( SELECT    IDDepartment
                                  FROM      dbo.Departments
                                  WHERE     SiteID = @TestSiteID
                                            AND DefaultForSite = 1
                                )) ) ,
                    ass.ManualAllocation ,
                    ass.Floor ,
                    ass.Wing ,
                    ass.APMAC + 'tst'
            FROM    dbo.Assets ass
                    INNER JOIN dbo.Departments dept ON dept.IDDepartment = ass.DepartmentID
            WHERE   ass.SiteID = @SiteID
                    AND ass.LastPostDateUTC > DATEADD(HOUR, -24, GETUTCDATE());
    RAISERROR ('Assets Inserted' , 0, 1) WITH NOWAIT;

    DECLARE @HTML1 NVARCHAR(MAX);

    SELECT  *
    INTO    #temptassets
    FROM    ( SELECT    *
              FROM      dbo.Assets
              WHERE     SiteID = @TestSiteID
                        AND IDAssetType IN ( 2, 4 )
            ) AS asdfe;
    EXEC dbo.CustomTable2HTMLv3 '#temptassets', @HTML1 OUTPUT,
        'class="horizontal"', 0;
    EXEC dbo.usp_SendTextEmail 'enovatemedical-com.mail.protection.outlook.com',
        'Pulse@EnovateMedical.com', 'arlow.farrell@enovatemedical.com',
        'Pulse Site Test', @HTML1, '', '', 0;


    IF ( @LeaveAPs = 0 )
        BEGIN
            UPDATE  dbo.AccessPoint
            SET     MACAddress = 'tst' + MACAddress
            WHERE   SiteID = @SiteID;
        END;
    ELSE
        BEGIN
            INSERT  INTO dbo.AccessPoint
                    ( MACAddress ,
                      SiteID ,
                      DepartmentID ,
                      DeviceCount ,
                      Description ,
                      Wing ,
                      Floor ,
                      AvgLinkQuality ,
                      LastPostDateUTC ,
                      CreatedDateUTC ,
                      ModifiedDateUTC ,
                      Manual ,
                      InsertedBy ,
                      BatchID
                    )
                    SELECT  ap.MACAddress + 'tst' AS MACAddress ,
                            @TestSiteID AS SiteID ,
                            ISNULL(( SELECT TOP 1
                                            IDDepartment
                                     FROM   dbo.Departments
                                     WHERE  SiteID = @TestSiteID
                                            AND REPLACE(Description, 'tst', '') = ( SELECT TOP 1
                                                              Description
                                                              FROM
                                                              dbo.Departments
                                                              WHERE
                                                              SiteID = @SiteID
                                                              AND Description = REPLACE(dept.Description,
                                                              'tst', '')
                                                              )
                                   ), ( SELECT  IDDepartment
                                        FROM    dbo.Departments
                                        WHERE   SiteID = @TestSiteID
                                                AND DefaultForSite = 1
                                      )) ,
                            ap.DeviceCount ,
                            ap.Description ,
                            ap.Wing ,
                            ap.Floor ,
                            ap.AvgLinkQuality ,
                            ap.LastPostDateUTC ,
                            ap.CreatedDateUTC ,
                            ap.ModifiedDateUTC ,
                            ap.Manual ,
                            ap.InsertedBy ,
                            ap.BatchID
                    FROM    dbo.AccessPoint ap
                            INNER JOIN dbo.Departments dept ON dept.IDDepartment = ap.DepartmentID
                    WHERE   ap.SiteID = @SiteID
                            AND ap.LastPostDateUTC > DATEADD(DAY, -30,
                                                             GETUTCDATE());
        END;
    IF ( @RemoveChargers = 1 )
        BEGIN
            UPDATE  dbo.Assets
            SET     DepartmentID = ( SELECT IDDepartment
                                     FROM   dbo.Departments
                                     WHERE  SiteID = @TestSiteID
                                            AND DefaultForSite = 1
                                   )
            WHERE   Assets.SiteID = @TestSiteID
                    AND SerialNo LIKE '%tst%';
        END;
    IF ( @RemoveManualFlag = 1 )
        BEGIN
            UPDATE  dbo.Assets
            SET     ManualAllocation = 0
            WHERE   SiteID = @TestSiteID
                    AND SerialNo LIKE '%tst%'
                    AND IDAssetType NOT IN ( 2, 4 );
        END;
    UPDATE  dbo.Assets
    SET     DepartmentID = ( SELECT IDDepartment
                             FROM   dbo.Departments
                             WHERE  SiteID = @TestSiteID
                                    AND DefaultForSite = 1
                           )
    WHERE   Assets.SiteID = @TestSiteID
            AND SerialNo LIKE '%tst%'
            AND IDAssetType NOT IN ( 2, 4 );
    UPDATE  dbo.Assets
    SET     Floor = NULL ,
            Wing = NULL ,
            APMAC = APMAC + 'tst' ,
            SourceIPAddress = 'tst' + SourceIPAddress ,
            IP = 'tst' + IP ,
            AccessPointId = NULL
    WHERE   SiteID = @TestSiteID
            AND SerialNo LIKE '%tst%'
            AND IDAssetType NOT IN ( 2, 4 );
    UPDATE  dbo.Assets
    SET     SiteID = 4
    WHERE   SiteID = @TestSiteID
            AND SerialNo LIKE '%tst%'
            AND IDAssetType NOT IN ( 2, 4 );
    UPDATE  dbo.SystemSettings
    SET     SettingValue = 1
    WHERE   SettingKey = 'TestMode';
    UPDATE  dbo.SystemSettings
    SET     SettingValue = @TestSiteID
    WHERE   SettingKey = 'TestSiteID';
    UPDATE  dbo.SystemSettings
    SET     SettingValue = @SiteID
    WHERE   SettingKey = 'SiteReplicatedID';
    UPDATE  dbo.SystemSettings
    SET     SettingValue = @TestNumHours
    WHERE   SettingKey = 'TestNumHours';
    UPDATE  dbo.SystemSettings
    SET     SettingValue = @TestStartTime
    WHERE   SettingKey = 'TestStartTime';




--DECLARE @HTML11 NVARCHAR(MAX)
-- EXEC dbo.CustomTable2HTMLv3 '#tmpSnapshot',@HTML11 OUTPUT,'class="horizontal"',0

--exec usp_SendTextEmail 'enovatemedical-com.mail.protection.outlook.com', 'Pulse@EnovateMedical.com','mark.dalen@enovatemdical.com;arlow.farrell@enovatemedical.com', 'Pulse Site Test Removed/Unassigned Proof Snapshot' , @HTML11, '', '', 0
    RAISERROR ('Proof Snapshot Emailed' , 0, 1) WITH NOWAIT;

	--insert into  TestLog (ExcelString) values (@HTML11)
--UPDATE dbo.SystemSettings SET SettingValue = @SnapshotInterval WHERE SettingKey = 'SnapshotInterval'




GO
