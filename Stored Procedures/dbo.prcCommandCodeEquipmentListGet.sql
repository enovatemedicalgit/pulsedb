SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

 
create PROCEDURE [dbo].[prcCommandCodeEquipmentListGet] 
 
	@EquipmentTypeID int,
	@BusinessUnitID int,
	@CommandCodeID int
AS
BEGIN

	SET NOCOUNT ON;

		select d.ROW_ID,FriendlyDescription, 
		case
			when ISNULL(FriendlyDescription, '') = '' then SerialNumber
			else ISNULL(SerialNumber, '')
		end as ItemDescription,
		dtcc.Parameter1FriendlyName,
		dtcc.Parameter0FriendlyName
		from assets d left join (select DeviceTypeID, Parameter1FriendlyName, Parameter0FriendlyName from DeviceTypeCommandCode where CommandCodeID = @CommandCodeID) dtcc
		on d.IDAssetType = dtcc.DeviceTypeID
		where d.BusinessUnitID = @BusinessUnitID
		and d.DeviceTypeID = @EquipmentTypeID
		and case when ISNULL(FriendlyDescription, '') = '' then SerialNumber
			else ISNULL(SerialNumber, '')
		end  <> ''
		order by case when ISNULL(FriendlyDescription, '') = '' then SerialNumber
			else ISNULL(SerialNumber, '')
		end, d.ROW_ID
	
END

GO
