SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		<Bill Murray>
-- Create date:	<8/16/2016>
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[prcDashGetRunRateSummary]
	@SiteID int= 657, @LookbackDays int= 31
AS


	SELECT DATEDIFF(day, s.Date, GETDATE())  AS RunRateDayWeek, DATENAME(WEEKDAY,s.Date) AS RunRateDay, MAX(IIF(s.AvgSessionLengthMinutes+(s.AvgSessionLengthHours*60)>720,720,s.AvgSessionLengthMinutes+(s.AvgSessionLengthHours*60))) AS RunRate , s.SiteID, s.DepartmentID
	FROM dbo.SummaryTrending AS s 
	WHERE
		s.SiteID = @SiteId 
	AND
	DATEDIFF(day, s.Date, GETDATE()) BETWEEN 1 AND  @LookbackDays 
	GROUP BY
		DATEDIFF(day, s.Date, GETDATE()),
		DATENAME(WEEKDAY,s.Date)
		,
	s.DepartmentId,
	    s.SiteID
	ORDER BY 
		DATEDIFF(day, s.Date, GETDATE()) DESC;



GO
