SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:      <ENOVATE\bill.murray,BILLMURRAY>
-- Create date: <10/5/2016> <2:42 PM>
-- Description: 
--				Default Description of Stored Proc with Supporting Links, Notes, etc.     
--
-- 
-- =============================================
CREATE PROCEDURE [dbo].[prcDashGetSessionInfo] @SessionID VARCHAR(50)
AS
     BEGIN
         SET NOCOUNT ON;
         SET XACT_ABORT, QUOTED_IDENTIFIER, ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL ON;
         SET NUMERIC_ROUNDABORT OFF;
         BEGIN TRY
             SELECT CONVERT(NVARCHAR(50),[SessionKey]) AS SessionKey,
                    [UserId],
                    u.[CreatedDateUTC],
                    [Active],
                    [ExpirationDateUTC],
				[IDSite] AS siteId,
				[UserName],
				[FirstName],
				[LastName],
				[Email],
				[Title],
				[u].[PrimaryPhone],
				ISNULL([u].[Notes],'') AS Notes
             FROM dbo.UserSession us LEFT join dbo.[USER] u ON
			 us.UserId = u.IDUser
             WHERE [SessionKey] = @SessionID AND active = 1;
         END TRY
         BEGIN CATCH
             DECLARE @ErrorMessage NVARCHAR(4000);
             DECLARE @ErrorSeverity INT;
             DECLARE @ErrorState INT;
             SELECT @ErrorMessage ='SessionId = [' + @SessionID + '] ' + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE();
             RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
             INSERT INTO dbo.ErrorLog
             ([Description],
              [Source],
              [CreatedDateUTC]
             )
             VALUES
             (@ErrorMessage+' '+@ErrorSeverity+' '+@ErrorState,
              ' [prcDashGetSessionInfo] ',
              GETUTCDATE()
             );
         END CATCH;
     END;
GO
