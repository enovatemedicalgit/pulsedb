SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Arlow Farrell
-- Create date: 2011-03-29
-- Description:	Get business unit list for 
--              supplied user id
--
-- Modified:    2012-02-14
-- Reason:      To include 'TNStinger Medical HQ'
--              because batteries may now be
--              assigned to this HQ (but not to
--              other HQs). This HQ will only
--              be listed for Stinger users.
-- =============================================
CREATE PROCEDURE [dbo].[spGetSitesByUserName] ( @UserName VARCHAR(40) )
AS
    BEGIN

        BEGIN
            SELECT  IDSite ,
                    SiteDescription
            FROM    dbo.Sites
            WHERE   ( IDSite IN ( SELECT    SiteId
                                  FROM      dbo.UserSites us
								    INNER JOIN
								    dbo.[User] u ON us.User_Row_Id = u.IDUser 
								    AND
								    u.UserName = @UserName
                                 )
                      OR IDSite IN (
                      SELECT    IDSite
                      FROM      dbo.Sites
                      WHERE     CustomerID IN (
                                SELECT  CustomerID
                                FROM    dbo.Sites
                                WHERE   IDSite IN (
										  SELECT    SiteId
										  FROM      dbo.UserSites us
										  INNER JOIN
										  dbo.[User] u ON us.User_Row_Id = u.IDUser 
										  AND
										  u.UserName = @UserName ) 
								    ) )
                    )
		--and BUType = 2
            ORDER BY SiteDescription;
        END;
	--select ROW_ID, SiteDescription
	--from BusinessUnit
	--where 
	--	(
	--		ROW_ID in (select BusinessUnitID from [User] where ROW_ID = @UserID)
	--		or ParentBusinessUnit in (select BusinessUnitID from [User] where ROW_ID = @UserID)
	--	)
	--and BUType = 2
	--order by SiteDescription
    END;

GO
