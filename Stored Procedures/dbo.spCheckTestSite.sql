SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- Batch submitted through debugger: SQLQuery154.sql|7|0|C:\Users\afarrell\AppData\Local\Temp\~vs256E.sql

CREATE PROCEDURE [dbo].[spCheckTestSite]
AS
    BEGIN
 
        SET NOCOUNT ON;


        DECLARE @HTML5 NVARCHAR(MAX);
        DECLARE @HTML6 NVARCHAR(MAX);
        DECLARE @TestStartTime DATETIME2;
        DECLARE @Workstations INT;
        DECLARE @Chargers INT;
        DECLARE @Batteries INT;
        DECLARE @WorkstationsOriginal INT;
        DECLARE @ChargersOriginal INT;
        DECLARE @BatteriesOriginal INT;
        DECLARE @WorkstationsAllocated INT;
        DECLARE @ChargersAllocated INT;
        DECLARE @BatteriesAllocated INT;
        DECLARE @WorkstationsAllocatedOriginal INT;
        DECLARE @ChargersAllocatedOriginal INT;
        DECLARE @BatteriesAllocatedOriginal INT;
        DECLARE @WorkstationsAllocatedDepartment INT;
        DECLARE @ChargersAllocatedDepartment INT;
        DECLARE @BatteriesAllocatedDepartment INT;
        DECLARE @WorkstationsAllocatedDepartmentOriginal INT;
        DECLARE @ChargersAllocatedDepartmentOriginal INT;
        DECLARE @BatteriesAllocatedDepartmentOriginal INT;
        DECLARE @WorkstationsAllocatedCorrectly INT;
        DECLARE @ChargersAllocatedCorrectly INT;
        DECLARE @BatteriesAllocatedCorrectly INT;
        DECLARE @SiteReplicatedID INT; 
        DECLARE @LogDate DATETIME; 
        SET @SiteReplicatedID = ( SELECT    SettingValue
                                  FROM      dbo.SystemSettings
                                  WHERE     SettingKey = 'SiteReplicatedID'
                                );
        DECLARE @TestSiteID INT; 
        SET @TestSiteID = ( SELECT  SettingValue
                            FROM    dbo.SystemSettings
                            WHERE   SettingKey = 'TestSiteID'
                          );
        SET @TestStartTime = CAST(( SELECT  SettingValue
                                    FROM    dbo.SystemSettings
                                    WHERE   SettingKey = 'TestStartTime'
                                  ) AS DATETIME2);
        IF ( ( SELECT   SettingValue
               FROM     dbo.SystemSettings
               WHERE    SettingKey = 'TestMode'
             ) = '1' )
            BEGIN


                SET @Workstations = ( SELECT    ISNULL(COUNT(ass.SerialNo), 0)
                                      FROM      dbo.Assets ass
                                      WHERE     ass.IDAssetType IN ( 1, 6 )
                                                AND ass.SiteID = @TestSiteID
                                                AND ass.LastPostDateUTC > DATEADD(HOUR,
                                                              -24,
                                                              @TestStartTime)
                                    );
                SET @Chargers = ( SELECT    ISNULL(COUNT(ass.SerialNo), 0)
                                  FROM      dbo.Assets ass
                                  WHERE     ass.IDAssetType IN ( 2, 4 )
                                            AND ass.SiteID = @TestSiteID
                                            AND ass.LastPostDateUTC > DATEADD(HOUR,
                                                              -24,
                                                              @TestStartTime)
                                );
                SET @Batteries = ( SELECT   ISNULL(COUNT(ass.SerialNo), 0)
                                   FROM     dbo.Assets ass
                                   WHERE    ass.IDAssetType IN ( 5 )
                                            AND ass.SiteID = @TestSiteID
                                            AND ass.LastPostDateUTC > DATEADD(HOUR,
                                                              -24,
                                                              @TestStartTime)
                                 );
                SET @WorkstationsOriginal = ( SELECT    ISNULL(COUNT(ass.SerialNo),
                                                              0)
                                              FROM      dbo.Assets ass
                                              WHERE     ass.IDAssetType IN ( 1, 6 )
                                                        AND ass.SiteID = @SiteReplicatedID
                                                        AND ass.LastPostDateUTC > DATEADD(HOUR,
                                                              -24,
                                                              @TestStartTime)
                                            );
                SET @ChargersOriginal = ( SELECT    ISNULL(COUNT(ass.SerialNo), 0)
                                          FROM      dbo.Assets ass
                                          WHERE     ass.IDAssetType IN ( 2, 4 )
                                                    AND ass.SiteID = @SiteReplicatedID
                                                    AND ass.LastPostDateUTC > DATEADD(HOUR,
                                                              -24,
                                                              @TestStartTime)
                                        );
                SET @BatteriesOriginal = ( SELECT   ISNULL(COUNT(ass.SerialNo), 0)
                                           FROM     dbo.Assets ass
                                           WHERE    ass.IDAssetType IN ( 5 )
                                                    AND ass.SiteID = @SiteReplicatedID
                                                    AND ass.LastPostDateUTC > DATEADD(HOUR,
                                                              -24,
                                                              @TestStartTime)
                                         );
                SET @WorkstationsAllocated = ( SELECT   ISNULL(COUNT(ass.SerialNo),
                                                              0)
                                               FROM     dbo.Assets ass
                                               WHERE    ( ass.Floor IS NOT NULL
                                                          AND ass.Floor <> ''
                                                          AND ass.Floor <> ' '
                                                          OR ass.Wing IS NOT NULL
                                                        )
                                                        AND ass.IDAssetType IN ( 1,
                                                              6 )
                                                        AND ass.SiteID = @TestSiteID
                                                        AND ass.LastPostDateUTC > DATEADD(HOUR,
                                                              -24,
                                                              @TestStartTime)
                                             );
                SET @ChargersAllocated = ( SELECT   ISNULL(COUNT(ass.SerialNo), 0)
                                           FROM     dbo.Assets ass
                                           WHERE    ( ass.Floor IS NOT NULL
                                                      AND ass.Floor <> ''
                                                      AND ass.Floor <> ' '
                                                      OR ass.Wing IS NOT NULL
                                                    )
                                                    AND ass.IDAssetType IN ( 2, 4 )
                                                    AND ass.SiteID = @TestSiteID
                                                    AND ass.LastPostDateUTC > DATEADD(HOUR,
                                                              -24,
                                                              @TestStartTime)
                                         );
                SET @BatteriesAllocated = ( SELECT  ISNULL(COUNT(ass.SerialNo), 0)
                                            FROM    dbo.Assets ass
                                            WHERE   ( ass.Floor IS NOT NULL
                                                      AND ass.Floor <> ''
                                                      AND ass.Floor <> ' '
                                                      OR ass.Wing IS NOT NULL
                                                    )
                                                    AND ass.IDAssetType IN ( 5 )
                                                    AND ass.SiteID = @TestSiteID
                                                    AND ass.LastPostDateUTC > DATEADD(HOUR,
                                                              -24,
                                                              @TestStartTime)
                                          );
                SET @WorkstationsAllocatedOriginal = ( SELECT ISNULL(COUNT(ass.SerialNo),
                                                              0)
                                                       FROM   dbo.Assets ass
                                                       WHERE  ( ass.Floor IS NOT NULL
                                                              AND ass.Floor <> ''
                                                              AND ass.Floor <> ' '
                                                              OR ass.Wing IS NOT NULL
                                                              )
                                                              AND ass.IDAssetType IN (
                                                              1, 6 )
                                                              AND ass.SiteID = @SiteReplicatedID
                                                              AND ass.LastPostDateUTC > DATEADD(HOUR,
                                                              -24,
                                                              @TestStartTime)
                                                     );
                SET @ChargersAllocatedOriginal = ( SELECT   ISNULL(COUNT(ass.SerialNo),
                                                              0)
                                                   FROM     dbo.Assets ass
                                                   WHERE    ( ass.Floor IS NOT NULL
                                                              AND ass.Floor <> ''
                                                              AND ass.Floor <> ' '
                                                              OR ass.Wing IS NOT NULL
                                                            )
                                                            AND ass.IDAssetType IN (
                                                            2, 4 )
                                                            AND ass.SiteID = @SiteReplicatedID
                                                            AND ass.LastPostDateUTC > DATEADD(HOUR,
                                                              -24,
                                                              @TestStartTime)
                                                 );
                SET @BatteriesAllocatedOriginal = ( SELECT  ISNULL(COUNT(ass.SerialNo),
                                                              0)
                                                    FROM    dbo.Assets ass
                                                    WHERE   ( ass.Floor IS NOT NULL
                                                              AND ass.Floor <> ''
                                                              AND ass.Floor <> ' '
                                                              OR ass.Wing IS NOT NULL
                                                            )
                                                            AND ass.IDAssetType IN (
                                                            5 )
                                                            AND ass.SiteID = @SiteReplicatedID
                                                            AND ass.LastPostDateUTC > DATEADD(HOUR,
                                                              -24,
                                                              @TestStartTime)
                                                  );
                SET @WorkstationsAllocatedDepartment = ( SELECT
                                                              ISNULL(COUNT(ass.SerialNo),
                                                              0)
                                                         FROM dbo.Assets ass
                                                         WHERE
                                                              ass.DepartmentID IS NOT NULL
                                                              AND ass.DepartmentID NOT IN (
                                                              SELECT
                                                              IDDepartment
                                                              FROM
                                                              dbo.Departments
                                                              WHERE
                                                              SiteID = @TestSiteID
                                                              AND DefaultForSite = 1 )
                                                              AND ass.IDAssetType IN (
                                                              1, 6 )
                                                              AND ass.SiteID = @TestSiteID
                                                              AND ass.LastPostDateUTC > DATEADD(HOUR,
                                                              -24,
                                                              @TestStartTime)
                                                       );
                SET @ChargersAllocatedDepartment = ( SELECT ISNULL(COUNT(ass.SerialNo),
                                                              0)
                                                     FROM   dbo.Assets ass
                                                     WHERE  ass.DepartmentID IS NOT NULL
                                                            AND ass.DepartmentID NOT IN (
                                                            SELECT
                                                              IDDepartment
                                                            FROM
                                                              dbo.Departments
                                                            WHERE
                                                              SiteID = @TestSiteID
                                                              AND DefaultForSite = 1 )
                                                            AND ass.IDAssetType IN (
                                                            2, 4 )
                                                            AND ass.SiteID = @TestSiteID
                                                            AND ass.LastPostDateUTC > DATEADD(HOUR,
                                                              -24,
                                                              @TestStartTime)
                                                   );
                SET @BatteriesAllocatedDepartment = ( SELECT  ISNULL(COUNT(ass.SerialNo),
                                                              0)
                                                      FROM    dbo.Assets ass
                                                      WHERE   ass.DepartmentID IS NOT NULL
                                                              AND ass.DepartmentID NOT IN (
                                                              SELECT
                                                              IDDepartment
                                                              FROM
                                                              dbo.Departments
                                                              WHERE
                                                              SiteID = @TestSiteID
                                                              AND DefaultForSite = 1 )
                                                              AND ass.IDAssetType IN (
                                                              5 )
                                                              AND ass.SiteID = @TestSiteID
                                                              AND ass.LastPostDateUTC > DATEADD(HOUR,
                                                              -24,
                                                              @TestStartTime)
                                                    );
                SET @WorkstationsAllocatedDepartmentOriginal = ( SELECT
                                                              ISNULL(COUNT(ass.SerialNo),
                                                              0)
                                                              FROM
                                                              dbo.Assets ass
                                                              WHERE
                                                              ( ass.DepartmentID IS NOT NULL
                                                              AND ass.DepartmentID NOT IN (
                                                              SELECT
                                                              IDDepartment
                                                              FROM
                                                              dbo.Departments
                                                              WHERE
                                                              SiteID = @SiteReplicatedID
                                                              AND DefaultForSite = 1 )
                                                              )
                                                              AND ass.IDAssetType IN (
                                                              1, 6 )
                                                              AND ass.SiteID = @SiteReplicatedID
                                                              AND ass.LastPostDateUTC > DATEADD(HOUR,
                                                              -24,
                                                              @TestStartTime)
                                                              );
                SET @ChargersAllocatedDepartmentOriginal = ( SELECT
                                                              ISNULL(COUNT(ass.SerialNo),
                                                              0)
                                                             FROM
                                                              dbo.Assets ass
                                                             WHERE
                                                              ( ass.DepartmentID IS NOT NULL
                                                              AND ass.DepartmentID NOT IN (
                                                              SELECT
                                                              IDDepartment
                                                              FROM
                                                              dbo.Departments
                                                              WHERE
                                                              SiteID = @SiteReplicatedID
                                                              AND DefaultForSite = 1 )
                                                              )
                                                              AND ass.IDAssetType IN (
                                                              2, 4 )
                                                              AND ass.SiteID = @SiteReplicatedID
                                                              AND ass.LastPostDateUTC > DATEADD(HOUR,
                                                              -24,
                                                              @TestStartTime)
                                                           );
                SET @BatteriesAllocatedDepartmentOriginal = ( SELECT
                                                              ISNULL(COUNT(ass.SerialNo),
                                                              0)
                                                              FROM
                                                              dbo.Assets ass
                                                              WHERE
                                                              ( ass.DepartmentID IS NOT NULL
                                                              AND ass.DepartmentID NOT IN (
                                                              SELECT
                                                              IDDepartment
                                                              FROM
                                                              dbo.Departments
                                                              WHERE
                                                              SiteID = @SiteReplicatedID
                                                              AND DefaultForSite = 1 )
                                                              )
                                                              AND ass.IDAssetType IN (
                                                              5 )
                                                              AND ass.SiteID = @SiteReplicatedID
                                                              AND ass.LastPostDateUTC > DATEADD(HOUR,
                                                              -24,
                                                              @TestStartTime)
                                                            );
                SET @LogDate = GETUTCDATE();
                SET @WorkstationsAllocatedCorrectly = @WorkstationsAllocated;
                SET @ChargersAllocatedCorrectly = @ChargersAllocated;
                SET @BatteriesAllocatedCorrectly = @BatteriesAllocated;



                INSERT  INTO dbo.TestLogXY
                        ( LogDate ,
                          workstations ,
                          chargers ,
                          batteries ,
                          WorkstationsOriginal ,
                          ChargersOriginal ,
                          BatteriesOriginal ,
                          WorkstationsAllocated ,
                          ChargersAllocated ,
                          BatteriesAllocated ,
                          WorkstationsAllocatedOriginal ,
                          ChargersAllocatedOriginal ,
                          BatteriesAllocatedOriginal ,
                          WorkstationsAllocatedDepartment ,
                          ChargersAllocatedDepartment ,
                          BatteriesAllocatedDepartment ,
                          WorkstationsAllocatedDepartmentOriginal ,
                          ChargersAllocatedDepartmentOriginal ,
                          BatteriesAllocatedDepartmentOriginal ,
                          TestSiteID ,
                          SiteReplicatedID
                        )
                VALUES  ( @LogDate ,
                          @Workstations ,
                          @Chargers ,
                          @Batteries ,
                          @WorkstationsOriginal ,
                          @ChargersOriginal ,
                          @BatteriesOriginal ,
                          @WorkstationsAllocated ,
                          @ChargersAllocated ,
                          @BatteriesAllocated ,
                          @WorkstationsAllocatedOriginal ,
                          @ChargersAllocatedOriginal ,
                          @BatteriesAllocatedOriginal ,
                          @WorkstationsAllocatedDepartment ,
                          @ChargersAllocatedDepartment ,
                          @BatteriesAllocatedDepartment ,
                          @WorkstationsAllocatedDepartmentOriginal ,
                          @ChargersAllocatedDepartmentOriginal ,
                          @BatteriesAllocatedDepartmentOriginal ,
                          @TestSiteID ,
                          @SiteReplicatedID
                        );



            END;
    END;



GO
