SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[CalculateDistence] --'','','','37167','','','','37130',''
    (
      @ToAddress VARCHAR(100) = NULL ,
      @ToCity VARCHAR(100) = NULL ,
      @ToState VARCHAR(100) = NULL ,
      @ToPostCode VARCHAR(100) = 37067 ,
      @FromAddress VARCHAR(100) = NULL ,
      @FromCity VARCHAR(100) = NULL ,
      @FromState VARCHAR(100) = NULL ,
      @FromPostCode VARCHAR(100) = 37130 ,
      @DistanceInKilometers VARCHAR(100) OUTPUT
    )
AS
    DECLARE @Object AS INT;
    DECLARE @ResponseText AS VARCHAR(8000);
    PRINT CAST(@FromPostCode AS VARCHAR(8000));
    DECLARE @serviceUrl AS VARCHAR(500);
    SET @serviceUrl = 'http://maps.googleapis.com/maps/api/distancematrix/xml?origins='
        + @ToAddress + @ToCity + @ToState + @ToPostCode + '&destinations='
        + @FromAddress + @FromCity + @FromState + @FromPostCode
        + '&mode=driving&language=en-EN&units=metric;';
    EXEC sys.sp_OACreate 'MSXML2.XMLHTTP', @Object OUT;
    EXEC sys.sp_OAMethod @Object, 'open', NULL, 'get', @serviceUrl, --Your Web Service Url (invoked)
        'false';
    EXEC sys.sp_OAMethod @Object, 'send';
    EXEC sys.sp_OAMethod @Object, 'responseText', @ResponseText OUTPUT;

    DECLARE @Response AS XML;
    PRINT CAST(@Response AS VARCHAR(8000));
--Select @ResponseText as XMLList

    SET @Response = CAST(@ResponseText AS XML);

    DECLARE @Status AS VARCHAR(20);
    DECLARE @Distance AS VARCHAR(20);


    SET @Status = @Response.value('(DistanceMatrixResponse/row/element/status)[1]',
                                  'varchar(20)'); 
    PRINT @Status;
    IF ( @Status = 'ZERO_RESULTS' )
        BEGIN

            SET @Distance = @Status;
        END;
    ELSE
        BEGIN
            SET @Distance = @Response.value('(DistanceMatrixResponse/row/element/distance/text)[1]',
                                            'varchar(20)'); 
        END;
  --Select @Response.value('(DistanceMatrixResponse/row/element/distance/text)[1]', 'varchar(10)') as Distance 
    SELECT  @Distance AS Distance;
    EXEC sys.sp_OADestroy @Object;
GO
