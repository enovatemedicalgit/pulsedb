SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_AllSessionDataShort]
    @SiteId AS VARCHAR(10) = NULL ,
    @CustomerID AS VARCHAR(10) = NULL ,
    @DeviceSerialno AS VARCHAR(50) = NULL ,
    @BatterySerialno AS VARCHAR(50) = NULL ,
    @IDAssetType AS INT = NULL ,
    @BeginDate AS DATETIME ,
    @EndDate AS DATETIME
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        IF ( @BatterySerialno IS NOT NULL )
            BEGIN
                SELECT TOP 20
                        asd.CreatedDateUTC ,
                        asd.DeviceSerialNumber ,
                        at.Description ,
                        vwP.ProductSerialNumber AS CartSerialNumber ,
                        asd.BatterySerialNumber ,
                        asd.FullChargeCapacity ,
                        asd.ChargeLevel ,
                        asd.CycleCount ,
                        dept.IDDepartment ,
                        d.Floor AS SiteFloor ,
                        d.Wing ,
                        d.Other ,
		--asd.BUBStatusDescription,
                        asd.WifiFirmwareRevision ,
                        asd.Bay ,
                        asd.BayWirelessRevision ,
                        asd.Bay1ChargerRevision ,
                        asd.Bay2ChargerRevision ,
                        asd.Bay3ChargerRevision ,
                        asd.Bay4ChargerRevision ,
                        asd.LinkQuality ,
                        s.SiteDescription ,
                        cust.CustomerName ,
                        d.AssetNumber ,
		--asd.ActivityDescription,
                        asd.APMAC ,
                        asd.ControlBoardSerialNumber ,
                        asd.HolsterBoardSerialNumber ,
                        asd.SourceIPAddress ,
                        ap.Description AS AccessPointDescription ,
                        asd.IP ,
                        asd.DeviceMAC
                FROM    dbo.viewAllSessionData asd
                        LEFT JOIN dbo.Assets d ON asd.DeviceSerialNumber = d.SerialNo
                        LEFT JOIN dbo.AssetType at ON at.IDAssetType = d.IDAssetType
                        LEFT JOIN dbo.AccessPoint ap ON ap.ROW_ID = d.AccessPointId
                        LEFT JOIN dbo.Sites s ON s.IDSite = d.SiteID
                        LEFT JOIN dbo.Customers cust ON cust.IDCustomer = s.CustomerID
                        LEFT JOIN dbo.Departments dept ON dept.SiteID = s.IDSite
                        LEFT JOIN dbo.vwProductCart vwP ON d.SerialNo = vwP.DeviceSerialNo
                WHERE   d.SerialNo LIKE '%' + @BatterySerialno + '%'
                        AND asd.CreatedDateUTC BETWEEN @BeginDate
                                               AND     @EndDate
                ORDER BY vwP.CreatedDateUTC DESC;
            END;
        ELSE
            IF ( @DeviceSerialno IS NOT NULL )
                BEGIN
                    SELECT TOP 20
                            asd.CreatedDateUTC ,
                            asd.DeviceSerialNumber ,
                            at.Description ,
                            vwP.ProductSerialNumber AS CartSerialNumber ,
                            asd.BatterySerialNumber ,
                            asd.FullChargeCapacity ,
                            asd.ChargeLevel ,
                            asd.CycleCount ,
                            dept.IDDepartment ,
                            d.Floor AS SiteFloor ,
                            d.Wing ,
                            d.Other ,
		--asd.BUBStatusDescription,
                            asd.WifiFirmwareRevision ,
                            asd.Bay ,
                            asd.BayWirelessRevision ,
                            asd.Bay1ChargerRevision ,
                            asd.Bay2ChargerRevision ,
                            asd.Bay3ChargerRevision ,
                            asd.Bay4ChargerRevision ,
                            asd.LinkQuality ,
                            s.SiteDescription ,
                            cust.CustomerName ,
                            d.AssetNumber ,
		--asd.ActivityDescription,
                            asd.APMAC ,
                            asd.ControlBoardSerialNumber ,
                            asd.HolsterBoardSerialNumber ,
                            asd.SourceIPAddress ,
                            ap.Description AS AccessPointDescription ,
                            asd.IP ,
                            asd.DeviceMAC
                    FROM    dbo.viewAllSessionData asd
                            LEFT JOIN dbo.Assets d ON asd.DeviceSerialNumber = d.SerialNo
                            LEFT JOIN dbo.AssetType at ON at.IDAssetType = d.IDAssetType
                            LEFT JOIN dbo.AccessPoint ap ON ap.ROW_ID = d.AccessPointId
                            LEFT JOIN dbo.Sites s ON s.IDSite = d.SiteID
                            LEFT JOIN dbo.Customers cust ON cust.IDCustomer = s.CustomerID
                            LEFT JOIN dbo.Departments dept ON dept.SiteID = s.IDSite
                            LEFT JOIN dbo.vwProductCart vwP ON d.SerialNo = vwP.DeviceSerialNo
                    WHERE   d.SerialNo LIKE '%' + @DeviceSerialno + '%'
                            AND asd.CreatedDateUTC BETWEEN @BeginDate
                                                   AND     @EndDate
                    ORDER BY vwP.CreatedDateUTC DESC;
                END;
            ELSE
                IF ( @IDAssetType IS NULL )
                    BEGIN
                        SELECT TOP 20
                                asd.CreatedDateUTC ,
                                asd.DeviceSerialNumber ,
                                at.Description ,
                                vwP.ProductSerialNumber AS CartSerialNumber ,
                                asd.BatterySerialNumber ,
                                asd.FullChargeCapacity ,
                                asd.ChargeLevel ,
                                asd.CycleCount ,
                                dept.IDDepartment ,
                                d.Floor AS SiteFloor ,
                                d.Wing ,
                                d.Other ,
		--asd.BUBStatusDescription,
                                asd.WifiFirmwareRevision ,
                                asd.Bay ,
                                asd.BayWirelessRevision ,
                                asd.Bay1ChargerRevision ,
                                asd.Bay2ChargerRevision ,
                                asd.Bay3ChargerRevision ,
                                asd.Bay4ChargerRevision ,
                                asd.LinkQuality ,
                                s.SiteDescription ,
                                cust.CustomerName ,
                                d.AssetNumber ,
		--asd.ActivityDescription,
                                asd.APMAC ,
                                asd.ControlBoardSerialNumber ,
                                asd.HolsterBoardSerialNumber ,
                                asd.SourceIPAddress ,
                                ap.Description AS AccessPointDescription ,
                                asd.IP ,
                                asd.DeviceMAC
                        FROM    dbo.viewAllSessionData asd
                                LEFT JOIN dbo.Assets d ON asd.DeviceSerialNumber = d.SerialNo
                                LEFT JOIN dbo.AssetType at ON at.IDAssetType = d.IDAssetType
                                LEFT JOIN dbo.AccessPoint ap ON ap.ROW_ID = d.AccessPointId
                                LEFT JOIN dbo.Sites s ON s.IDSite = d.SiteID
                                LEFT JOIN dbo.Customers cust ON cust.IDCustomer = s.CustomerID
                                LEFT JOIN dbo.Departments dept ON dept.SiteID = s.IDSite
                                LEFT JOIN dbo.vwProductCart vwP ON d.SerialNo = vwP.DeviceSerialNo
                        WHERE   s.IDSite = @SiteId
                                AND asd.CreatedDateUTC BETWEEN @BeginDate
                                                       AND    @EndDate
                        ORDER BY vwP.CreatedDateUTC DESC;
                    END;
                ELSE
                    IF ( @IDAssetType IS NOT NULL )
                        BEGIN 
                            SELECT TOP 20
                                    asd.CreatedDateUTC ,
                                    asd.DeviceSerialNumber ,
                                    at.Description ,
                                    vwP.ProductSerialNumber AS CartSerialNumber ,
                                    asd.BatterySerialNumber ,
                                    asd.FullChargeCapacity ,
                                    asd.ChargeLevel ,
                                    asd.CycleCount ,
                                    dept.IDDepartment ,
                                    d.Floor AS SiteFloor ,
                                    d.Wing ,
                                    d.Other ,
		--asd.BUBStatusDescription,
                                    asd.WifiFirmwareRevision ,
                                    asd.Bay ,
                                    asd.BayWirelessRevision ,
                                    asd.Bay1ChargerRevision ,
                                    asd.Bay2ChargerRevision ,
                                    asd.Bay3ChargerRevision ,
                                    asd.Bay4ChargerRevision ,
                                    asd.LinkQuality ,
                                    s.SiteDescription ,
                                    cust.CustomerName ,
                                    d.AssetNumber ,
		--asd.ActivityDescription,
                                    asd.APMAC ,
                                    asd.ControlBoardSerialNumber ,
                                    asd.HolsterBoardSerialNumber ,
                                    asd.SourceIPAddress ,
                                    ap.Description AS AccessPointDescription ,
                                    asd.IP ,
                                    asd.DeviceMAC
                            FROM    dbo.viewAllSessionData asd
                                    LEFT JOIN dbo.Assets d ON asd.DeviceSerialNumber = d.SerialNo
                                    LEFT JOIN dbo.AssetType at ON at.IDAssetType = d.IDAssetType
                                    LEFT JOIN dbo.AccessPoint ap ON ap.ROW_ID = d.AccessPointId
                                    LEFT JOIN dbo.Sites s ON s.IDSite = d.SiteID
                                    LEFT JOIN dbo.Customers cust ON cust.IDCustomer = s.CustomerID
                                    LEFT JOIN dbo.Departments dept ON dept.SiteID = s.IDSite
                                    LEFT JOIN dbo.vwProductCart vwP ON d.SerialNo = vwP.DeviceSerialNo
                            WHERE   s.IDSite = @SiteId
                                    AND d.IDAssetType = @IDAssetType
                                    AND asd.CreatedDateUTC BETWEEN @BeginDate
                                                           AND
                                                              @EndDate
                            ORDER BY vwP.CreatedDateUTC DESC;
                        END;

    END;

GO
