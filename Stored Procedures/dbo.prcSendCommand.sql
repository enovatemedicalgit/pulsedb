SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- =============================================
CREATE PROCEDURE [dbo].[prcSendCommand] (@CommandCodeID int, @SerialNumber varchar(50), @Param1 varchar(50))
AS
begin

	declare @CommandText varchar(200)
	set @CommandText = (select top 1 CommandCodeText from CommandCode where ROW_ID = @CommandCodeID)
	set @CommandText = case when ltrim(rtrim(@Param1)) = '' then @CommandText else @CommandText + '[' + ltrim(rtrim(@Param1)) + ']' end

	update Assets
	set CommandCode = @CommandText
	where CommandCode = '0'
	and SerialNo = @SerialNumber

	if (select count(*) from CommandQueue where SerialNumber = @SerialNumber and CommandText = @CommandText and SentDateUTC = '01/01/1900') = 0
	begin
		insert into CommandQueue (SerialNumber, CommandText)
		values (@SerialNumber, @CommandText)
	end

end
GO
