SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcDepartmentsInsert]
    @Description NVARCHAR(50) ,
    @SiteID INT
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
    BEGIN TRAN;
	
    INSERT  INTO dbo.Departments
            ( [Description], [SiteID] )
            SELECT  @Description ,
                    @SiteID;
 
               
    COMMIT;



GO
