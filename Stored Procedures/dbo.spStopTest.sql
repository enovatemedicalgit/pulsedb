SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- Batch submitted through debugger: SQLQuery154.sql|7|0|C:\Users\afarrell\AppData\Local\Temp\~vs256E.sql

CREATE PROCEDURE [dbo].[spStopTest]
AS
    BEGIN
 
        SET NOCOUNT ON;
        DECLARE @rslts NVARCHAR(MAX);
        DECLARE @TestSiteID INT; 
        SET @TestSiteID = ( SELECT  SettingValue
                            FROM    dbo.SystemSettings
                            WHERE   SettingKey = 'TestSiteID'
                          );
        DECLARE @ReplicatedSiteID INT;
        SET @ReplicatedSiteID = ( SELECT    SettingValue
                                  FROM      dbo.SystemSettings
                                  WHERE     SettingKey = 'ReplicatedSiteID'
                                );

--select * into #tempt 
--from (select * from assets where siteid = @TestSiteID) as asdf
--EXEC dbo.CustomTable2HTMLv3 '#tempt',@rslts OUTPUT,'class="horizontal"',0
--insert into [PulseHistorical].[dbo].[TestLog] (ExcelString) values (@rslts)
--if (dateadd(HH,(select cast(SettingValue as int) from systemsettings where settingkey = 'TestNumHours'),(convert(DateTime2,(select SettingValue from systemsettings where settingkey = 'TestStartTime')))) > getutcdate() )
--begin

        UPDATE  dbo.SystemSettings
        SET     SettingValue = 0
        WHERE   SettingKey = 'TestMode';
        UPDATE  dbo.AccessPoint
        SET     MACAddress = REPLACE(MACAddress, 'tst', '')
        WHERE   SiteID = @ReplicatedSiteID;
        DELETE  FROM dbo.Sites
        WHERE   SiteName LIKE '% Test%'
                AND CustomerID = 1425;
        DELETE  FROM dbo.AccessPoint
        WHERE   SiteID = @TestSiteID;
        DELETE  FROM dbo.SiteIPSubnet
        WHERE   SiteID = @TestSiteID;
        DELETE  FROM dbo.Departments
        WHERE   SiteID = @TestSiteID;
        UPDATE  dbo.SystemSettings
        SET     SettingValue = 0
        WHERE   SettingKey = 'TestMode';
        UPDATE  dbo.SystemSettings
        SET     SettingValue = GETUTCDATE()
        WHERE   SettingKey = 'TestStartTime';
        UPDATE  dbo.SystemSettings
        SET     SettingValue = 0
        WHERE   SettingKey = 'SiteReplicatedID';
        UPDATE  dbo.SystemSettings
        SET     SettingValue = 0
        WHERE   SettingKey = 'TestSiteID';
        DELETE  FROM dbo.NonSessionDataCurrent
        WHERE   DeviceSerialNumber IN ( SELECT  SerialNo
                                        FROM    dbo.Assets
                                        WHERE   SiteID = @TestSiteID )
                AND NonSessionDataCurrent.CreatedDateUTC > DATEADD(HOUR, -12,
                                                              GETUTCDATE());
        DELETE  FROM dbo.SessionDataCurrent
        WHERE   DeviceSerialNumber IN ( SELECT  SerialNo
                                        FROM    dbo.Assets
                                        WHERE   SiteID = @TestSiteID )
                AND SessionDataCurrent.CreatedDateUTC > DATEADD(HOUR, -12,
                                                              GETUTCDATE());
        DELETE  FROM dbo.AssetsCharger
        WHERE   IDAsset IN (SELECT IDAsset FROM dbo.tblAssets WHERE SerialNo LIKE '%tst%');
		   DELETE  FROM dbo.tblAssets
        WHERE   IDAsset IN (SELECT IDAsset FROM dbo.tblAssets WHERE SerialNo LIKE '%tst%')
		        DELETE  FROM dbo.AssetsWorkstation
        WHERE   IDAsset IN (SELECT IDAsset FROM dbo.tblAssets WHERE SerialNo LIKE '%tst%');
		   DELETE  FROM dbo.tblAssets
        WHERE   IDAsset IN (SELECT IDAsset FROM dbo.tblAssets WHERE SerialNo LIKE '%tst%')
		 	        DELETE  FROM dbo.AssetsBattery
        WHERE   IDAsset IN (SELECT IDAsset FROM dbo.tblAssets WHERE SerialNo LIKE '%tst%');
		   DELETE  FROM dbo.tblAssets
        WHERE   IDAsset IN (SELECT IDAsset FROM dbo.tblAssets WHERE SerialNo LIKE '%tst%')
		 
        DELETE  FROM dbo.SiteAPMAC
        WHERE   APMAC LIKE '%tst%';
        DELETE  FROM dbo.Sites
        WHERE   SiteName LIKE '% Test%'
                AND CustomerID = 1425;
        DELETE  FROM dbo.NonSessionDataCurrent
        WHERE   DeviceSerialNumber LIKE '%tst'
                AND CreatedDateUTC > DATEADD(HOUR,
                                                              -12,
                                                              GETUTCDATE());
        DELETE  FROM dbo.SessionDataCurrent
        WHERE   DeviceSerialNumber LIKE '%tst'
                AND CreatedDateUTC > DATEADD(HOUR, -12,
                                                              GETUTCDATE());

--end

    END;

GO
