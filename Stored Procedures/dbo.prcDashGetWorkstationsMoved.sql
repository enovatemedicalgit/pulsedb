SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[prcDashGetWorkstationsMoved]
-- Add the parameters for the stored procedure here
 (   @SiteID INT = 1212)

AS
;
WITH WorkstationsUtilized AS
(
SELECT 
    moveDay,
    moveDayWeek,
    COUNT(DISTINCT DeviceSerialNumber) AS WorkstationsUtilized,
    SiteId,
    DepartmentId
FROM
(
    SELECT 
	    DATENAME(WEEKDAY,EndDateUTC) AS moveDay,
	    DATEDIFF(DAY,EndDateUTC,GETUTCDATE()) AS moveDayWeek,
	    tblAssets.SerialNo AS DeviceSerialNumber,
	    CASE	  
		  WHEN (- 1 * AvgAmpDraw) > UtilizationThreshhold
		  THEN 1
		  WHEN NumberOfMoves > 1
		  THEN 1
		  ELSE 0
	    END AS IsUtilized,
	    tblAssets.SiteId AS SiteId,
	    tblAssets.DepartmentId 
    FROM Sessions
    INNER JOIN tblAssets
	    ON Sessions.DeviceSerialNumber = tblAssets.SerialNo
    WHERE
	    DATEDIFF(DAY,EndDateUTC,GETUTCDATE())  <=14
	    AND
	    tblAssets.SiteId = @SiteId
) AS base
WHERE
    IsUtilized = 1
GROUP BY 
    moveDay,
    moveDayWeek,
    SiteId,
    DepartmentId

)
, WorkstationsTotal AS
(
    SELECT  DATENAME(WEEKDAY,CreatedDateUTC) AS moveDay,
		  DATEDIFF(DAY,CreatedDateUTC,GETUTCDATE()) AS moveDayWeek,
		  ActiveWorkstationCount AS WorkstationsTotal,
		  SiteId AS SiteId,
		  DepartmentId AS DepartmentId
		  FROM SummaryActiveAssets WHERE SiteId = @SiteId
	AND DATEDIFF(DAY,CreatedDateUTC,GETUTCDATE())  <=14
)


SELECT u.moveDay, u.moveDayWeek, u.WorkstationsUtilized AS WorkstationsMoved, t.WorkstationsTotal, u.SiteId, u.DepartmentID FROM WorkstationsUtilized u LEFT JOIN WorkstationsTotal t ON 
u.moveDay = t.moveday AND u.moveDayWeek = t.moveDayWeek AND u.SiteId = t.SiteId AND u.DepartmentID = t.DepartmentID
ORDER BY u.moveDayWeek DESC, u.DepartmentId ASC
GO
