SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cha-Ron Murphy
-- Create date: 3/6/2015
-- Description:	Emails list of batteries  Below 10% for Memeorial
-- =============================================
CREATE PROCEDURE [dbo].[spEmailListBatteriesBelow10_Percent]
AS
    BEGIN
        DECLARE @FromAddress VARCHAR(150);
        DECLARE @FromName VARCHAR(150);
        DECLARE @body_message VARCHAR(MAX);
        DECLARE @tmp TABLE
            (
              id INT IDENTITY ,
              batteryserialNumber VARCHAR(30) ,
              BusUnit VARCHAR(50) ,
              wing VARCHAR(50) ,
              _floor VARCHAR(50) ,
              other VARCHAR(50)
            );
	
	--insert into @tmp
	--select BatterySerialNumber from AlertMessage 
	--where 
	--BusinessUnitID=988 
	--and 
	--AlertConditionID=68 and AlertStatus=0
	
        INSERT  INTO @tmp
                SELECT  c.BatterySerialNumber ,
                        b.Description ,
                        a.Wing ,
                        a.Floor ,
                        a.Other
                FROM    dbo.AlertMessage c
                        JOIN dbo.Assets a ON c.DeviceSerialNumber = a.SerialNo
                        LEFT JOIN dbo.Departments b ON a.DepartmentID = b.IDDepartment
                WHERE   c.SiteID = 988
                        AND c.AlertConditionID = 68
                        AND c.AlertStatus = 0;
	
        SET @body_message = N'<table border="1"><tr><th>BatterySerialNumber</th><th>BusinessUnitDepartment</th><th>Wing</th><th>Floor</th><th>Other</th></tr>';
	
        DECLARE @serial VARCHAR(30);
        DECLARE @depart VARCHAR(100);
        DECLARE @wing VARCHAR(20);	
        DECLARE @floor VARCHAR(20);
        DECLARE @other VARCHAR(20);
        DECLARE @cnt INT;
        SET @cnt = 1;
	
        WHILE ( @cnt <= ( SELECT    MAX(id)
                          FROM      @tmp
                        ) )
            BEGIN
                SELECT  @serial = batteryserialNumber ,
                        @depart = BusUnit ,
                        @wing = wing ,
                        @floor = _floor ,
                        @other = other
                FROM    @tmp
                WHERE   id = @cnt;
		
                SET @body_message = @body_message + '<tr><td>' + @serial
                    + '</td><td>' + @depart + '</td><td>' + @wing
                    + '</td><td>' + @floor + '</td><td>' + @other
                    + '</td><tr>';	
		--set @body_message=@body_message + (select batteryserialnumber from @tmp where id=@cnt) + ','		
                SET @cnt = @cnt + 1;
            END;
	
	--if (LEN(@body_message) > 2)		   
	--	set @body_message= (select LEFT(@body_message,len(@body_message) -1))
	

        SELECT TOP 1
                @FromAddress = a.DefaultFromAddress ,
                @FromName = b.DefaultFromName
        FROM    ( SELECT TOP 1
                            1 AS joinkey ,
                            SettingValue AS DefaultFromAddress
                  FROM      dbo.SystemSettings
                  WHERE     SettingKey = 'EMAIL_NOTIFICATIONS_DEFAULT_FROM_ADDRESS'
                ) a
                JOIN ( SELECT TOP 1
                                1 AS joinkey ,
                                SettingValue AS DefaultFromName
                       FROM     dbo.SystemSettings
                       WHERE    SettingKey = 'EMAIL_NOTIFICATIONS_DEFAULT_FROM_NAME'
                     ) b ON a.joinkey = b.joinkey;
	
        IF @cnt > 1
            BEGIN
                INSERT  INTO dbo.EmailQueue
                        ( [Subject] ,
                          ToAddresses ,
                          Body ,
                          Attempts ,
                          FromAddress ,
                          FromName
                        )
                        SELECT  'CAST Batteries Below 10% Notification ' ,
                                'genheliossupport@mskcc.org' ,
                                @body_message ,
                                1 ,
                                @FromAddress ,
                                @FromName;
            END;


        UPDATE  dbo.AlertMessage
        SET     AlertStatus = 20
        WHERE   SiteID = 988
                AND AlertConditionID = 68
                AND AlertStatus = 0;

    END;


GO
