SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:      <ENOVATE\bill.murray,BILLMURRAY>
-- Create date: <10/26/2016> <9:52 AM>
-- Description: 
--				Default Description of Stored Proc with Supporting Links, Notes, etc.     
--
-- 
-- =============================================
CREATE PROCEDURE [dbo].[prcDashGetUserUrls]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT,
        QUOTED_IDENTIFIER,
        ANSI_NULLS,
        ANSI_PADDING,
        ANSI_WARNINGS,
        ARITHABORT,
        CONCAT_NULL_YIELDS_NULL ON;
        SET NUMERIC_ROUNDABORT OFF;
 
                
 
     
           SELECT email, sfuserurl  FROM [pulse].[dbo].[User]  WHERE SFUserUrl IS NOT NULL ORDER BY sfuserurl desc
 
        
 
    END;
GO
