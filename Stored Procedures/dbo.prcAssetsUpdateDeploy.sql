SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcAssetsUpdateDeploy]
    @UpdateOrSelect VARCHAR(2) = 0 ,
    @SerialNo VARCHAR(50) ,
    @AssetNumber VARCHAR(50) = NULL ,
    @Floor VARCHAR(50) = NULL ,
    @Wing VARCHAR(50) = NULL ,
    @DepartmentID INT = NULL ,
    @SiteID INT = NULL ,
    @SiteFloorID INT = NULL ,
    @Notes VARCHAR(50) = NULL ,
    @InternalPostDate DATE = NULL ,
    @Manual INT = 0
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	

    BEGIN 
        DECLARE @msg AS VARCHAR(100);
        SET @msg = 'Serialno = ' + @SerialNo;
        RAISERROR (@msg, 10, 1) WITH NOWAIT;

        IF ( @UpdateOrSelect = '1' )
            BEGIN
                RAISERROR ('Update', 10, 1) WITH NOWAIT;
                UPDATE  dbo.Assets
                SET     [AssetNumber] = ISNULL(@AssetNumber, [AssetNumber]) ,
                        [DepartmentID] = ISNULL(@DepartmentID, [DepartmentID]) ,
                        [Floor] = ISNULL(@Floor, [Floor]) ,
                        [Wing] = ISNULL(@Wing, [Wing]) ,
                        [SiteID] = ISNULL(@SiteID, [SiteID]) ,
                        [SiteFloorID] = ISNULL(@SiteFloorID, [SiteFloorID]) ,
                        [Notes] = ISNULL(@Notes, [Notes]) ,
                        [LastInternalPostDateUTC] = ISNULL(@InternalPostDate,
                                                           [LastInternalPostDateUTC]) ,
                        [ManualAllocation] = @Manual
                WHERE   [SerialNo] = @SerialNo;
            END;
        ELSE
            BEGIN
                RAISERROR ('Select', 10, 1) WITH NOWAIT;
	-- Begin Return Select <- do not remove
                SELECT  ass.IDAsset ,
                        s.SiteName AS SiteName ,
                        ass.IP ,
                        ass.SourceIPAddress ,
                        ass.LastPostDateUTC ,
                        ass.DepartmentID ,
                        ass.AssetNumber ,
                        ass.Wing ,
                        ass.Floor ,
                        ass.Description ,
                        ass.SerialNo ,
                        ass.SiteID
                FROM    dbo.Assets ass
                        LEFT JOIN dbo.Sites s ON ass.SiteID = s.IDSite
                WHERE   ass.SerialNo = @SerialNo;	
	-- End Return Select <- do not remove
            END;
    END;



GO
