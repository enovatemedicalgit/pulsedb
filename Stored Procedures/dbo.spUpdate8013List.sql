SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spUpdate8013List]
AS --begin
     --delete from Capture.dbo.CommandQueue where CreatedDateUTC < dateadd(MINUTE,-10,getdate()) and CreatedDateUTC > dateadd(HH,-900,getdate())
     --declare @sCommandText varchar(200)
     --set @sCommandText = (select top 1 CommandCodeText from CommandCode where ROW_ID = 51)
     --DECLARE @SN varchar(50)
     --DECLARE @SubtractionNum varchar(50)
     --DECLARE @DeviceSerial varchar(50)
     --DECLARE @Bay int
     --DECLARE db_cursor CURSOR FOR select SerialNumber from [dbo].[8013BatteryList3] where MaxCycleCount <> 10 or MaxCycleCount is null and LastPacketDate > dateadd(MINUTE,-20,getdate()) and LastPacketDate is not null
     --OPEN db_cursor   
     --FETCH NEXT FROM db_cursor INTO @SN   
     --WHILE @@FETCH_STATUS = 0   
     --BEGIN  
     --set @DeviceSerial = (select DeviceSerial from [dbo].[8013BatteryList3] where SerialNumber = @SN and Bay > 0 and (MaxCycleCount <> 10 or MaxCycleCount = '' or MaxCycleCount Is Null))
     --set @Bay = (select Bay from [dbo].[8013BatteryList3] where SerialNumber = @SN and Bay > 0 and (MaxCycleCount <> 10 and MaxCycleCount <> 0 or MaxCycleCount = '' or MaxCycleCount Is Null))
     ---- select MaxCycleCount from 8013BatteryList where SerialNumber = @SN
     ---- set @SubtractionNum = (@MaxCycleCount - 1)
     --Declare @cnt1 int
     --Declare @cnt2 int
     --Declare @cnt3 int
     --Declare @cnt4 int
     --set @cnt1 = (select count(serialnumber) from Capture.dbo.CommandQueue where SerialNumber = @DeviceSerial and CommandText like '809(1%' )
     --set @cnt2 = (select count(serialnumber) from Capture.dbo.CommandQueue where SerialNumber = @DeviceSerial and CommandText like '809(2%' )
     --set @cnt3 = (select count(serialnumber) from Capture.dbo.CommandQueue where SerialNumber = @DeviceSerial and CommandText like '809(3%' )
     --set @cnt4 = (select count(serialnumber) from Capture.dbo.CommandQueue where SerialNumber = @DeviceSerial and CommandText like '809(4%' )
     --if (@Bay is not null or @Bay > 0)
     --BEGIN
     --begin transaction 
     --	If((@Bay = 1 and @cnt1 < 1) or (@Bay = 2 and @cnt2 < 1) or (@Bay = 3 and @cnt3 < 1) or (@Bay = 4 and @cnt4 < 1))
     --		BEGIN
     --		insert into Capture.dbo.CommandQueue (SerialNumber, CommandText)
     --		values (@DeviceSerial, '809(' + CAST(@Bay as varchar(3)) + ')[' + '10' + ']')
     --		END
     --commit
     --END
     --       FETCH NEXT FROM db_cursor INTO @SN
     --END   
     --CLOSE db_cursor   
     --DEALLOCATE db_cursor
     --end

    BEGIN
        DECLARE @sCommandText VARCHAR(200);
        SET @sCommandText = ( SELECT TOP 1
                                        CommandCodeText
                              FROM      dbo.CommandCode
                              WHERE     ROW_ID = 51
                            );
        DECLARE @SN VARCHAR(50);
        DECLARE @SubtractionNum VARCHAR(50);
        DECLARE @DeviceSerial VARCHAR(50);
        DECLARE @Bay INT;
        DECLARE @OldMaxCycleCount INT;
        DECLARE db_cursor CURSOR
        FOR
            SELECT  BatterySerialNumber
            FROM    dbo.NonSessionDataCurrent
            WHERE   DeviceType IN ( 2, 5 )
                    AND MaxCycleCount = 10
                    AND ChargeLevel < 25
                    AND CreatedDateUTC > DATEADD(MINUTE, -6, GETDATE())
                    AND BatterySerialNumber NOT IN (
                    SELECT  SerialNumber
                    FROM    dbo.DisabledBatteryList )
            GROUP BY BatterySerialNumber;
        OPEN db_cursor;
        FETCH NEXT FROM db_cursor INTO @SN;
        WHILE @@FETCH_STATUS = 0
            BEGIN
                SET @DeviceSerial = ( SELECT TOP 1
                                                DeviceSerialNumber
                                      FROM      dbo.NonSessionDataCurrent
                                      WHERE     BatterySerialNumber = @SN
                                                AND Bay > 0
                                                AND ( CreatedDateUTC > DATEADD(MINUTE,
                                                              -9, GETDATE()) )
                                    );
                SET @Bay = ( SELECT TOP 1
                                    Bay
                             FROM   dbo.NonSessionDataCurrent
                             WHERE  BatterySerialNumber = @SN
                                    AND Bay > 0
                                    AND ( CreatedDateUTC > DATEADD(MINUTE, -9,
                                                              GETDATE()) )
                           );
                SET @OldMaxCycleCount = ( SELECT TOP 1
                                                    MaxCycleCount
                                          FROM      dbo.NonSessionDataCurrent
                                          WHERE     BatterySerialNumber = @SN
                                                    AND Bay > 0
                                                    AND ( MaxCycleCount > 10
                                                          AND MaxCycleCount <> 0
                                                          AND CreatedDateUTC > DATEADD(DAY,
                                                              -15, GETDATE())
                                                          AND CreatedDateUTC < DATEADD(DAY,
                                                              -1, GETDATE())
                                                        )
                                          ORDER BY  CreatedDateUTC ASC
                                        );
                IF ( @OldMaxCycleCount IS NULL
                     OR @OldMaxCycleCount < 100
                   )
                    BEGIN
                        SET @OldMaxCycleCount = ( SELECT TOP 1
                                                            MaxCycleCount
                                                  FROM      dbo.NonSessionDataCurrent
                                                  WHERE     BatterySerialNumber = @SN
                                                            AND ( MaxCycleCount > 10
                                                              AND MaxCycleCount <> 0
                                                              AND CreatedDateUTC > DATEADD(DAY,
                                                              -2, GETDATE())
                                                              )
                                                            AND CreatedDateUTC < DATEADD(DAY,
                                                              -6, GETDATE())
                                                  ORDER BY  CreatedDateUTC ASC
                                                );
                    END;
                IF ( @OldMaxCycleCount IS NULL
                     OR @OldMaxCycleCount < 100
                   )
                    BEGIN
                        SET @OldMaxCycleCount = ( SELECT TOP 1
                                                            MaxCycleCount
                                                  FROM      dbo.SessionDataCurrent
                                                  WHERE     BatterySerialNumber = @SN
                                                            AND ( MaxCycleCount > 10
                                                              AND MaxCycleCount <> 0
                                                              AND CreatedDateUTC > DATEADD(DAY,
                                                              -20, GETDATE())
                                                              )
                                                            AND CreatedDateUTC < DATEADD(DAY,
                                                              -6, GETDATE())
                                                  ORDER BY  CreatedDateUTC ASC
                                                );
                    END;
                -- select MaxCycleCount from 8013BatteryList where SerialNumber = @SN
                -- set @SubtractionNum = (@MaxCycleCount - 1)
                DECLARE @cnt1 INT;
                DECLARE @cnt2 INT;
                DECLARE @cnt3 INT;
                DECLARE @cnt4 INT;
                SET @cnt1 = ( SELECT    COUNT(SerialNumber)
                              FROM      dbo.CommandQueue
                              WHERE     SerialNumber = @DeviceSerial
                                        AND CommandText LIKE '809(1%'
                            );
                SET @cnt2 = ( SELECT    COUNT(SerialNumber)
                              FROM      dbo.CommandQueue
                              WHERE     SerialNumber = @DeviceSerial
                                        AND CommandText LIKE '809(2%'
                            );
                SET @cnt3 = ( SELECT    COUNT(SerialNumber)
                              FROM      dbo.CommandQueue
                              WHERE     SerialNumber = @DeviceSerial
                                        AND CommandText LIKE '809(3%'
                            );
                SET @cnt4 = ( SELECT    COUNT(SerialNumber)
                              FROM      dbo.CommandQueue
                              WHERE     SerialNumber = @DeviceSerial
                                        AND CommandText LIKE '809(4%'
                            );
                IF ( @Bay IS NOT NULL
                     AND ( @Bay = 1
                           OR @Bay = 2
                           OR @Bay = 3
                           OR @Bay = 4
                         )
                     AND @OldMaxCycleCount IS NOT NULL
                   )
                    BEGIN
                        BEGIN TRANSACTION;
                        IF ( ( @Bay = 1
                               AND @cnt1 < 1
                             )
                             OR ( @Bay = 2
                                  AND @cnt2 < 1
                                )
                             OR ( @Bay = 3
                                  AND @cnt3 < 1
                                )
                             OR ( @Bay = 4
                                  AND @cnt4 < 1
                                )
                           )
                            BEGIN
                                SET @sCommandText = '809('
                                    + CAST(@Bay AS VARCHAR(3)) + ')['
                                    + CAST(@OldMaxCycleCount AS VARCHAR(5))
                                    + ']';
                                IF @sCommandText IS NOT NULL
                                    BEGIN
                                        INSERT  INTO dbo.CommandQueue
                                                ( SerialNumber, CommandText )
                                        VALUES  ( @DeviceSerial, @sCommandText );
                                    END;
                            END;
                        COMMIT;
                    END;
                FETCH NEXT FROM db_cursor INTO @SN;
            END;
        CLOSE db_cursor;
        DEALLOCATE db_cursor;
    END;

GO
