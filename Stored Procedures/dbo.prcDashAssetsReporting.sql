SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:      <ENOVATE\bill.murray,BILLMURRAY>
-- Create date: <10/17/2016> <6:07 AM>
-- Description: 
--				Default Description of Stored Proc with Supporting Links, Notes, etc.     
--
-- 
-- =============================================
CREATE PROCEDURE [dbo].[prcDashAssetsReporting]
    @SiteId INT = 696 ,
    @DepartmentId INT = NULL
AS
    BEGIN
        SET NOCOUNT ON;
	
        
        SELECT  CONVERT(INT,(CONVERT(DECIMAL(18,2),(SELECT  COUNT(SerialNo)
                                FROM    dbo.Assets
                                WHERE   
                                         LastPostDateUTC > DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                        AND Retired = 0
                     
                                        AND SiteID = @SiteId
                             ) )/ CONVERT(DECIMAL(18,2),( SELECT   COUNT(SerialNo)
                                   FROM     dbo.Assets
                                   WHERE   
                                             Retired = 0
                                  
                                            AND SiteID = @SiteId
                                 )) *100) ) AS AssetsReportingPct ,
								 
								 (SELECT  COUNT(SerialNo)
                                FROM    dbo.Assets
                                WHERE  
                                         LastPostDateUTC > DATEADD(DAY, -7,
                                                              GETUTCDATE())
                                        AND Retired = 0
                                 
                                        AND SiteID = @SiteId
                             ) AS AssetsReporting ,
                ( SELECT    COUNT(SerialNo)
                  FROM      dbo.Assets
                  WHERE    
                             Retired = 0
                         --   AND LastPostDateUTC IS NOT NULL
                            AND SiteID = @SiteId
                ) AS AssetsTotal ,
                @SiteId AS siteID;
 

    END;
	

GO
