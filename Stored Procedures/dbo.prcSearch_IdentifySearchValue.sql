SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[prcSearch_IdentifySearchValue] (@searchvalue varchar(30))
AS
BEGIN
	SET NOCOUNT ON;

	if (@searchvalue like '__:__:__:__:__:__')
	begin
		if (select count(IDAsset) from assets (NOLOCK) where DeviceMAC = @searchvalue) > 0
		begin
			select 'sd.DeviceMAC' as ColumnName
			return
		end
		if (select count(ROW_ID) from AccessPoint (NOLOCK) where MACAddress = @searchvalue) > 0
		begin
			select 'APMAC' as ColumnName
			return
		end
		-- default to APMAC if/when not found in database (because if not found, it is more likely to be an access point MAC than a device MAC)
		select 'APMAC' as ColumnName
		return
	end

	if (@searchvalue like '%.%.%.%')
	begin
		select 'IP' as ColumnName
		return
	end

	if (isnumeric(@searchvalue) = 1)
	begin
		if (select count(IDAsset) from assets (NOLOCK) where serialno = @searchvalue and IDAssetType <> 5) > 0
		begin
			select 'sd.DeviceSerialNumber' as ColumnName
			return
		end
		if (select count(IDAsset) from assets (NOLOCK) where SerialNo = @searchvalue and IDAssetType = 5) > 0
		begin
			select 'sd.BatterySerialNumber' as ColumnName
			return
		end
		if (select count(ROW_ID) from dbo.AssetProduct AS AP (NOLOCK) where ProductSerialNumber = @searchvalue) > 0
		begin
			select top 1 case when pt.ProductType = 'AIO' then 'aio.ProductSerialNumber' when pt.ProductType = 'Cart' then 'cart.ProductSerialNumber' when pt.ProductType = 'DC Monitor' then 'dcmonitor.ProductSerialNumber' end as ColumnName from dbo.ProductType (NOLOCK) pt join dbo.AssetProduct  (NOLOCK) dp on dp.ProductTypeID = pt.ROW_ID where dp.ProductSerialNumber = @searchvalue
			return
		end
	end

	select 'AssetNumber' as ColumnName
	return

END
GO
