SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 -- =============================================
       -- Author:      <ENOVATE\arlow.farrell,ARLOWFARRELL>
       -- Create date: <10/31/2016> <11:47 PM>
       -- Description: 
       --				Default Description of Stored Proc with Supporting Links, Notes, etc.     
       --
       -- 
       -- =============================================
       CREATE PROCEDURE [dbo].[prcDashCaseComments] 
	   @SiteID int, @IdUser int, 
	   @CaseID varchar(MAX), @CommentText nvarchar(MAX), @CaseNum varchar(100)
       AS
           BEGIN
               SET NOCOUNT ON;
             
        INSERT INTO [dbo].[CaseComments]
           ([CaseID]
           ,[CaseNum]
           ,[IDUser]
           ,[CommentText]
           ,[SiteID])
     VALUES
           (@CaseID,@CaseNum,@IdUser,@CommentText,@SiteID)

           END;

GO
