SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Utility].[CommunicationOverLast24HoursByState]
AS
BEGIN	    /**************************************************	Communication Over Last 24 By State	    **************************************************/

	    SELECT
			 COUNT(DISTINCT ROW_ID) AS             PacketsProcessed
		    , COUNT(DISTINCT DeviceSerialNumber) AS DevicesCommunicating
		    , COUNT(DISTINCT SiteId) AS             SitesCommunicating
		    , COUNT(DISTINCT CustomerID) AS         CustomersCommunicating 
		    --, Sites.State AS StateAbbrv
	    FROM
	    (
		   SELECT
				*
		   FROM
			   dbo.SessionData_Summary
		   UNION ALL
		   SELECT
				*
		   FROM
			   dbo.NonSessionData_Summary
								    ) AS composite
	    INNER JOIN
		    dbo.Assets
	    ON composite.DeviceSerialNumber = assets.SerialNo
	    INNER JOIN
		    dbo.Sites
	    ON Assets.SiteId = Sites.IDSite
	    WHERE  composite.CreatedDateUTC >= DATEADD(HOUR, -24, GETUTCDATE());
END
GO
