SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[prcDashGetBatteryDailyUsage_New]
-- Add the parameters for the stored procedure here
     @SiteID INT = 693 
AS


    SELECT
		  DATENAME(WEEKDAY, s.EndDateUTC) AS ampDay ,
            DATEDIFF(DAY, s.EndDateUTC, GETUTCDATE()) AS ampDayWeek ,
            COUNT(DISTINCT s.BatterySerialNumber) BatteriesUsed ,
            COUNT(DISTINCT s.ROW_ID)  SessionsTotal ,
            ass.SiteID ,
            ass.DepartmentID
    FROM    dbo.Sessions s
            LEFT JOIN dbo.Assets ass ON ass.SiteID = s.SiteID
                                             AND ass.DepartmentID = s.DepartmentID 
            LEFT JOIN dbo.SummaryActiveAssets saa ON s.SiteID = saa.SiteID
                                                     AND s.DepartmentID = saa.DepartmentID
                                                     AND CONVERT(DATE, s.EndDateUTC) = CONVERT(DATE, saa.CreatedDateUTC)
    WHERE   DATEDIFF(DAY, s.EndDateUTC, GETUTCDATE()) <= 7
            AND ass.SiteID = @SiteID AND ass.retired = 0 
			
    GROUP BY DATENAME(WEEKDAY, s.EndDateUTC) ,
            DATEDIFF(DAY, s.EndDateUTC, GETUTCDATE()) ,
            ass.SiteID ,
            ass.DepartmentID
GO
