SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcGetRecycledAssets]
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	

    BEGIN 
        SELECT TOP 20000
                MAX(ass.SerialNo) AS 'SerialNumber' ,
                MAX(usertbl.FirstName) + ' ' + MAX(usertbl.LastName) AS 'Recycled By' ,
                MAX(ISNULL(s.SiteName, s2.SiteName)) AS 'Recycled From' ,
                MAX(ass.Description) AS 'Device Description' ,
                MAX(ass.SourceIPAddress) AS 'SourceIP' ,
                MAX(CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, ass.LastPostDateUTC),
                                                   DATENAME(TzOffset,
                                                            SYSDATETIMEOFFSET())))) AS 'Last Communicated'
        FROM    dbo.Assets ass
                LEFT JOIN dbo.AccessPoint ap ON ass.APMAC = ap.MACAddress
                LEFT JOIN dbo.SiteIPSubnet sip ON sip.SourceIPAddressBase = ( LEFT(ass.SourceIPAddress,
                                                              LEN(ass.SourceIPAddress)
                                                              - CHARINDEX('.',
                                                              REVERSE(ass.SourceIPAddress))) )
                LEFT JOIN dbo.Sites s2 ON sip.SiteID = s2.IDSite
                LEFT JOIN dbo.Sites s ON ap.SiteID = s.IDSite
                LEFT JOIN dbo.[User] usertbl ON ass.ModifiedBy = usertbl.IDUser
        WHERE   ass.SiteID = 1125
        GROUP BY ass.SerialNo
        ORDER BY [Recycled From] DESC ,
                [Last Communicated] DESC;
    END;
	


GO
