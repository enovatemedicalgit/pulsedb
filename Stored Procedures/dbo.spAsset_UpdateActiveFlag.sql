SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- Create date: 2011-10-17
-- Description:	sets the IsActive field to 0
--              for devices which have not 
--              transmitted in the past 3 days.
--
--              Uses LastPostDate column for
--              this evaluation.
--
--              number of days is a variable
--              which initially began with -3
--              to toggle devices which have not
--              communicated in three or more days
--              
--              Modified
--              To no longer automatically update
--              the retired field
--              
--              Modified  to also
--              toggle the Retired (bit) field
--              though it uses a different
--              inactivity threshold than the
--              IsActive column
--              
-- =============================================
CREATE PROCEDURE [dbo].[spAsset_UpdateActiveFlag]
AS
    BEGIN
        SET NOCOUNT ON;


        DECLARE @ActiveThreshold INT;
        SET @ActiveThreshold = -3;

	-- -- ETL Process makes sure the IsActive column
	-- -- is set to 1 (Yes or true) each time a device transmits
	-- -- so we only need to toggle them to 0 when they haven't transmitted in a while
        UPDATE  dbo.Assets
        SET     IsActive = 0
        WHERE   IsActive = 1
                AND LastPostDateUTC < DATEADD(d, @ActiveThreshold,
                                              GETUTCDATE());
	
	-- the section below was added 1-6-2012
	
	-- this section suppressed 11-26-2012
	-- Katie requested that no devices be automatically retired any more
	/*
	declare @RetirementThreshold int
	set @RetirementThreshold = -90
	
	update Device
	set Retired = 1
	where LastPostDateUTC < dateadd(d, @RetirementThreshold, getutcdate())
	*/
    END;

GO
