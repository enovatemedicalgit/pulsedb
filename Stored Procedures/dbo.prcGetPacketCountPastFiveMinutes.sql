SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  CREATE PROCEDURE [dbo].[prcGetPacketCountPastFiveMinutes] (
  @SiteID INT = 1212 , @LookbackMinutes INT = 3
  )
  AS
      
	SELECT DATEDIFF(minute, a.lastpostdateutc, GETUTCDATE())  AS packetMinute,  COUNT(*) AS packetCount,  (COUNT(*)*900) AS packetBytes, a.SiteID, a.DepartmentId
	FROM Sites s 
	INNER JOIN Assets a	
		ON a.SiteID = s.IDSite
	WHERE
		a.SiteID = @SiteId
	AND a.IDAssetType IN (1,2,3,4,6,7) and
	DATEDIFF(minute, a.lastpostdateutc, GETUTCDATE()) BETWEEN 1 AND  @LookbackMinutes
	--AND EXISTS (SELECT * FROM AssetType at WHERE at.IDAssetType =  a.IdAssetType  AND at.AssetCategoryId = 1)
	GROUP BY
		DATEDIFF(minute, a.lastpostdateutc, GETUTCDATE()),
		a.DepartmentId,
	    a.SiteID
	ORDER BY 
		DATEDIFF(minute, a.lastpostdateutc, GETUTCDATE()) DESC;
GO
