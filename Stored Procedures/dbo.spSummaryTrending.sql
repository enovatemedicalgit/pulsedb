SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[spSummaryTrending]
AS
    BEGIN
	 --SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        DECLARE @lastdateupdated AS DATE;
        RAISERROR (' SummaryActiveRollupNow' , 0, 1) WITH NOWAIT;
        EXEC dbo.spSummaryActiveAssets_RollupNow;
        RAISERROR (' SummaryActiveRollupNow complete' , 0, 1) WITH NOWAIT;

	-- update AccessPointID, LastPostDate and IsActive columns
	--exec spSummary_UpdateDeviceAccessPoint
	--exec spSummary_UpdateBatteryAccessPoint

	-- create notifications for workstations that have been offline
	-- more than 7 days
	-- exec spCreateNotificationsForOfflineWorkstations
	-- create notifications for chargers that have been offline
	-- more than 24 hours
	-- exec spCreateNotificationsForOfflineChargers

/* ============= OPTIMIZATION NEEDED HERE : TOP  ==============  */

        RAISERROR ('Select  Max([Date])	from SummaryTrending' , 0, 1) WITH NOWAIT;
        DECLARE @LAST_SUMMARIZED_DATE DATETIME;
        SELECT  @LAST_SUMMARIZED_DATE = MAX([Date])
        FROM    dbo.SummaryTrending;
	
	--delete from SummaryTrending where [Date] = @LAST_SUMMARIZED_DATE
	--delete from Movement where [Date] = (select max([Date]) from Movement)
	--delete from MovementDetail where [Date] = (select max([Date]) from MovementDetail)
	
	
	-- since date values do not include times, we need to add a day so we don't roll up data from a day which was already rolled up
	-- example: rolled up data for 3-15-2011 includes 24 hrs of data though the date value itself represents the first second of that date
        DECLARE @FromDate DATETIME;
        DECLARE @ToDate DATETIME;
        SET @FromDate = DATEADD(d, -6, @LAST_SUMMARIZED_DATE);
        SET @ToDate = CAST(CONVERT(VARCHAR(10), GETDATE(), 101) AS DATETIME);


	-- For May 31, needed to run one day at a time, consecutive times, until summarized data was current.
	-- (a week had passed since the job successfully completed, which allowed too much
	--  data to pile up, causing the job to consume all remaining disk space on the server every time it ran)
	-- set @FromDate = DATEADD(d, 1, @LAST_SUMMARIZED_DATE)
	-- set @ToDate = DATEADD(d, 1, @FromDate)

        IF OBJECT_ID(N'tempdb..#tmpSS', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tmpSS;
            END;

        RAISERROR ('Select into tmp' , 0, 1) WITH NOWAIT;
        PRINT 'Select into tmp';
        SELECT  YEAR(s.EndDateUTC) AS [Year] ,
                MONTH(s.EndDateUTC) AS [Month] ,
                DAY(s.EndDateUTC) AS [Day] ,
                CONVERT(VARCHAR(10), s.EndDateUTC, 101) AS [Date] ,
                s.SiteID ,
                d.DepartmentID ,
                COUNT(s.ROW_ID) AS SessionCount ,
                AVG(s.SessionLengthMinutes) / 60 AS AvgSessionLengthHours ,
                AVG(s.SessionLengthMinutes) % 60 AS AvgSessionLengthMinutes ,
                AVG(s.RunRate) AS AvgRunRate ,
                0 AS Utilization
        INTO    #tmpSS
        FROM    dbo.Sessions s
                LEFT JOIN ( SELECT  *
                            FROM    dbo.Assets
                          ) d ON s.DeviceSerialNumber = d.SerialNo
        WHERE   d.Retired = 0
                AND s.EndDateUTC BETWEEN @FromDate AND @ToDate
        GROUP BY YEAR(s.EndDateUTC) ,
                MONTH(s.EndDateUTC) ,
                DAY(s.EndDateUTC) ,
                CONVERT(VARCHAR(10), s.EndDateUTC, 101) ,
                s.SiteID ,
                d.DepartmentID;

        CREATE NONCLUSTERED INDEX IX_tmpSS ON #tmpSS ([Date], SiteID, DepartmentID);
	
        IF OBJECT_ID(N'tempdb..#tmpNonSessionSummary', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tmpNonSessionSummary;
            END;

        RAISERROR ('Select tmpNonsession' , 0, 1) WITH NOWAIT;
        PRINT 'Select tmpnonsession';
        SELECT  CONVERT(VARCHAR(10), nsd.CreatedDateUTC, 101) AS [Date] ,
                d.SiteID ,
                d.DepartmentID ,
                0 AS Utilization ,
                COUNT(nsd.ROW_ID) AS NonSessionRecordCount
        INTO    #tmpNonSessionSummary
        FROM    dbo.NonSessionData_Summary nsd
                LEFT JOIN ( SELECT  *
                            FROM    dbo.Assets
                          ) d ON nsd.DeviceSerialNumber = d.SerialNo
        WHERE   d.Retired = 0
                AND nsd.CreatedDateUTC BETWEEN @FromDate AND @ToDate
        GROUP BY CONVERT(VARCHAR(10), nsd.CreatedDateUTC, 101) ,
                d.SiteID ,
                d.DepartmentID;

        CREATE NONCLUSTERED INDEX IX_tmpNonSessionSummary ON #tmpNonSessionSummary ([Date], SiteID, DepartmentID);

        RAISERROR ('Insert into SummaryTrending' , 0, 1) WITH NOWAIT;
        PRINT 'Insert into summarytrending';
	--BEGIN TRANSACTION
        DECLARE @updateDate AS DATE;
        SELECT TOP 1
                @updateDate = [Date]
        FROM    #tmpSS;
        DELETE  FROM dbo.SummaryTrending
        WHERE   [Date] = @updateDate;
        INSERT  INTO dbo.SummaryTrending
                ( [Year] ,
                  [Month] ,
                  [Day] ,
                  [Date] ,
                  SiteID ,
                  DepartmentID ,
                  SessionCount ,
                  AvgSessionLengthHours ,
                  AvgSessionLengthMinutes ,
                  AvgRunRate ,
                  Utilization ,
                  NonSessionRecordCount
                )
                SELECT  ss.Year ,
                        ss.Month ,
                        ss.Day ,
                        ss.Date ,
                        ss.SiteID ,
                        ss.DepartmentID ,
                        ss.SessionCount ,
                        ss.AvgSessionLengthHours ,
                        ss.AvgSessionLengthMinutes ,
                        ss.AvgRunRate ,
                        ss.Utilization ,
                        NonSessionSummary.NonSessionRecordCount
                FROM    #tmpSS ss
                        JOIN #tmpNonSessionSummary NonSessionSummary ON ss.Date = NonSessionSummary.Date
                                                              AND ss.SiteID = NonSessionSummary.SiteID
                                                              AND ss.DepartmentID = NonSessionSummary.DepartmentID
                ORDER BY ss.Date DESC;
	--COMMIT
/* ============= OPTIMIZATION NEEDED HERE : END  ==============  */



-- add rows to MovementDetail
        RAISERROR ('Insert into MovementDetail' , 0, 1) WITH NOWAIT;
        PRINT 'Insert into movementdetail';
	-- MovementDetail : TOP : for each device, how many transmissions reported a move within a given hour?
	--BEGIN TRANSACTION
        INSERT  INTO dbo.MovementDetail
                ( SiteID ,
                  DepartmentID ,
                  SerialNo ,
                  [Date] ,
                  [Hour] ,
                  Moves
                )
                SELECT  d.SiteID ,
                        d.DepartmentID ,
                        sd.DeviceSerialNumber ,
                        CONVERT(VARCHAR(10), sd.CreatedDateUTC, 101) AS [Date] ,
                        DATEPART(hh, sd.CreatedDateUTC) AS [Hour] ,
                        COUNT(sd.Move) AS Moves
                FROM    dbo.SessionData_Summary sd
                        JOIN dbo.Assets d ON sd.DeviceSerialNumber = d.SerialNo
                        LEFT JOIN dbo.MovementDetail md ON d.SiteID = md.SiteID
                                                           AND d.DepartmentID = md.DepartmentID
                                                           AND d.SerialNo = md.SerialNo
                                                           AND CONVERT(VARCHAR(10), sd.CreatedDateUTC, 101) = md.Date
                                                           AND DATEPART(hh,
                                                              sd.CreatedDateUTC) = md.Hour
                WHERE   sd.Move > 0
	--and sd.XValue + sd.YValue + sd.ZValue > 0 -- suppressed by request from Katie Matter via email on 2-21-2012
                        AND md.ROW_ID IS NULL
                        AND d.Retired = 0
                        AND sd.CreatedDateUTC BETWEEN @FromDate AND @ToDate
	--and d.Retired = 0
                GROUP BY d.SiteID ,
                        d.DepartmentID ,
                        sd.DeviceSerialNumber ,
                        CONVERT(VARCHAR(10), sd.CreatedDateUTC, 101) ,
                        DATEPART(hh, sd.CreatedDateUTC); 
	--COMMIT
	-- MovementDetail: END


        RAISERROR ('Insert into Movement Detail 2' , 0, 1) WITH NOWAIT;
        PRINT 'Insert into movment 2';
	-- Summarize the rows that were just added to MovementDetail
	--BEGIN TRANSACTION
        INSERT  INTO dbo.Movement
                ( SiteID ,
                  DepartmentID ,
                  DeviceSerialNumber ,
                  [Date] ,
                  HoursUtilized ,
                  Utilization
                )
                SELECT  dm.SiteID ,
                        dm.DepartmentID ,
                        dm.SerialNo ,
                        dm.Date ,
                        dm.Moves AS HoursUtilized ,
                        CONVERT(DECIMAL(5,2),( CONVERT(FLOAT, dm.Moves) / 24 )) AS Utilization					
                FROM    (
		-- count of device transmissions where the move flag was set for more than one transmission within a given hour
                          SELECT    SiteID ,
                                    DepartmentID AS DepartmentID ,
                                    SerialNo ,
                                    [Date] ,
                                    COUNT(Moves) AS Moves
                          FROM      dbo.MovementDetail
                          WHERE     Moves > 0
                                    AND Moves IS NOT NULL -- more than one move in a given hour
                          GROUP BY  SiteID ,
                                    DepartmentID ,
                                    SerialNo ,
                                    [Date]
                        ) dm -- DeviceMovementByDate
                        LEFT JOIN dbo.Movement m ON dm.SiteID = m.SiteID
                                                    AND dm.DepartmentID = m.DepartmentID
                                                    AND dm.SerialNo = m.DeviceSerialNumber
                                                    AND dm.Date = m.Date
                WHERE   m.ROW_ID IS NULL;
	--COMMIT

        RAISERROR ('Update movement detail' , 0, 1) WITH NOWAIT;
        PRINT 'Update movement';
	-- update AvgRunRate column on Movement table so we can know AvgRunRate at the device/day level (needed for Utilization Trending, when user selects specific workstations)
	--BEGIN TRANSACTION
        UPDATE  dbo.Movement
        SET     AvgRunRate = s.AvgRunRate
        FROM    ( SELECT    CONVERT(VARCHAR(10), EndDateUTC, 101) AS EndDateUTC ,
                            DeviceSerialNumber ,
                            ISNULL(AVG(RunRate), 0) AS AvgRunRate
                  FROM      dbo.Sessions
                  WHERE     EndDateUTC > @LAST_SUMMARIZED_DATE
                  GROUP BY  CONVERT(VARCHAR(10), EndDateUTC, 101) ,
                            DeviceSerialNumber
                ) s
        WHERE   s.EndDateUTC = Date
                AND s.DeviceSerialNumber = Movement.DeviceSerialNumber
                AND (Movement.AvgRunRate = 0 OR Movement.AvgRunRate IS NULL);  
	--COMMIT



        RAISERROR ('Update SummaryTrending' , 0, 1) WITH NOWAIT;
        PRINT 'Update summarytrending';
	-- update ActiveDeviceCount column on SummaryTrending
	--BEGIN TRANSACTION
        UPDATE  dbo.SummaryTrending
        SET     ActiveDeviceCount = COALESCE(q.ActiveDeviceCount, 0)
        FROM    ( SELECT    d.SiteID ,
                            d.DepartmentID ,
                            CONVERT(VARCHAR(10), sd.CreatedDateUTC, 101) AS [Date] ,
                            COUNT(DISTINCT sd.DeviceSerialNumber) AS ActiveDeviceCount
                  FROM      dbo.SessionData_Summary sd
                            JOIN dbo.Assets d ON sd.DeviceSerialNumber = d.SerialNo
                  WHERE     sd.CreatedDateUTC BETWEEN @FromDate AND @ToDate
                            AND d.Retired = 0
                  GROUP BY  d.SiteID ,
                            d.DepartmentID ,
                            CONVERT(VARCHAR(10), sd.CreatedDateUTC, 101)
                ) q
        WHERE   SummaryTrending.SiteID = q.SiteID
                AND SummaryTrending.DepartmentID = q.DepartmentID
                AND SummaryTrending.Date = q.Date;
	--COMMIT

	--BEGIN TRANSACTION


        DECLARE @FDate VARCHAR(25);
        DECLARE @TDate VARCHAR(25);
        SET @FDate = CONVERT(VARCHAR(10), @FromDate, 20); 
        SET @TDate = CONVERT(VARCHAR(10), @ToDate, 20); 

        RAISERROR ('Update SummaryTrending 2' , 0, 1) WITH NOWAIT;
	
        DECLARE @st AS VARCHAR(4000);
        SET @st = 'update SummaryTrending
	set Utilization = cast((cast(HoursUtilized as decimal(8,4)) / TotalHours) as decimal(6,4))
	from 
	(
		SELECT m.SiteID
			  ,m.DepartmentID
			  ,m.[Date]
			  ,UtilizedWorkstations.UtilizedWorkstationCount * 24 as TotalHours
			  ,sum([HoursUtilized]) as HoursUtilized
		  FROM [Movement] m
		  
		  join (
				select SiteID, DepartmentID, [Date], count(ROW_ID) as UtilizedWorkstationCount
				from Movement WITH (INDEX(IX_Movement)) 
				where [Date] between ''' + @FDate + ''' and ''' + @TDate
            + ''' 
				and HoursUtilized > 0
				group by SiteID, DepartmentID, [Date]
				) UtilizedWorkstations
		  
		  on m.[Date] = UtilizedWorkstations.[Date]
		  and m.SiteID = UtilizedWorkstations.SiteID
		  and m.DepartmentID = UtilizedWorkstations.DepartmentID
		  where m.[Date] between ''' + @FDate + ''' and ''' + @TDate + ''' 
		  group by 
			   m.SiteID
			  ,m.DepartmentID
			  ,m.[Date]
			  ,UtilizedWorkstations.UtilizedWorkstationCount * 24   
	) asdf
	where SummaryTrending.SiteID = asdf.SiteID
	and SummaryTrending.DepartmentID = asdf.DepartmentID
	and SummaryTrending.[Date] = asdf.[Date]
	and SummaryTrending.[Date] between ''' + @FDate + ''' and ''' + @TDate
            + ''' ';
        EXEC (@st);



	--update SummaryTrending
	--set Utilization = cast((cast(HoursUtilized as decimal(8,4)) / TotalHours) as decimal(6,4))
	--from 
	--(
	--	SELECT m.SiteID
	--		  ,m.DepartmentID
	--		  ,m.[Date]
	--		  ,UtilizedWorkstations.UtilizedWorkstationCount * 24 as TotalHours
	--		  ,sum([HoursUtilized]) as HoursUtilized
	--	  FROM [Movement] m
		  
	--	  join (
	--			select SiteID, DepartmentID, [Date], count(ROW_ID) as UtilizedWorkstationCount
	--			from Movement
	--			where [Date] between @FromDate and @ToDate
	--			and HoursUtilized > 0
	--			group by SiteID, DepartmentID, [Date]
	--			) UtilizedWorkstations
		  
	--	  on m.[Date] = UtilizedWorkstations.[Date]
	--	  and m.SiteID = UtilizedWorkstations.SiteID
	--	  and m.DepartmentID = UtilizedWorkstations.DepartmentID
	--	  where m.[Date] between @FromDate and @ToDate
	--	  group by 
	--		   m.SiteID
	--		  ,m.DepartmentID
	--		  ,m.[Date]
	--		  ,UtilizedWorkstations.UtilizedWorkstationCount * 24   
	--) asdf
	--where SummaryTrending.SiteID = asdf.SiteID
	--and SummaryTrending.DepartmentID = asdf.DepartmentID
	--and SummaryTrending.[Date] = asdf.[Date]
	--and SummaryTrending.[Date] between @FromDate and @ToDate
	--COMMIT


	-- ***************************************************************
	--   Refresh SummaryBusinessUnitDevice table: TOP
	-- ***************************************************************
        DECLARE @MostRecentSummaryActiveDevicesDate DATETIME;
        SELECT  @MostRecentSummaryActiveDevicesDate = MAX(CreatedDateUTC)
        FROM    dbo.SummaryActiveAssets;
	

        RAISERROR ('delete from SummarySiteAssets' , 0, 1) WITH NOWAIT;
        DELETE  FROM dbo.SummarySiteAssets;
        RAISERROR ('Insert into SummarySiteAssets' , 0, 1) WITH NOWAIT;
	
	--BEGIN TRANSACTION
        INSERT  INTO dbo.SummarySiteAssets
                ( IDCustomer ,
                  IDSite ,
                  IDDepartment ,
                  WorkstationCount ,
                  ChargerCount ,
                  BatteryCount ,
                  ActiveWorkstationCount ,
                  ActiveChargerCount ,
                  ActiveBatteryCount
                )
                SELECT  bu.CustomerID ,
                        a.SiteID ,
                        a.DepartmentID ,
                        a.WorkstationCount ,
                        a.ChargerCount ,
                        a.BatteryCount ,
                        COALESCE(b.ActiveWorkstationCount, 0) AS ActiveWorkstationCount ,
                        COALESCE(b.ActiveChargerCount, 0) AS ActiveChargerCount ,
                        COALESCE(b.ActiveBatteryCount, 0) AS ActiveBatteryCount
                FROM    ( SELECT    d.SiteID ,
                                    d.DepartmentID ,
                                    SUM(CASE WHEN d.IDAssetType IN ( 1, 3, 6 )
                                             THEN 1
                                             ELSE 0
                                        END) AS WorkstationCount ,
                                    SUM(CASE WHEN d.IDAssetType IN ( 2, 4 )
                                             THEN 1
                                             ELSE 0
                                        END) AS ChargerCount ,
                                    SUM(CASE WHEN d.IDAssetType = 5 THEN 1
                                             ELSE 0
                                        END) AS BatteryCount
                          FROM      dbo.Assets d
                          WHERE     d.Retired = 0
                          GROUP BY  d.SiteID ,
                                    d.DepartmentID
                        ) a
                        LEFT JOIN ( SELECT  *
                                    FROM    dbo.SummaryActiveAssets
                                    WHERE   CreatedDateUTC = @MostRecentSummaryActiveDevicesDate
                                  ) b ON a.SiteID = b.SiteID
                                         AND a.DepartmentID = b.DepartmentID
                        LEFT JOIN dbo.Sites bu ON a.SiteID = bu.IDSite;

        DECLARE @CreatedDateUTC DATETIME;
        SET @CreatedDateUTC = CAST(CONVERT(VARCHAR(10), GETDATE(), 101) AS DATETIME);
        UPDATE  dbo.SummarySiteAssets
        SET     DormantBatteryCount = saa.DormantBatteryCount ,
                CountFullBatteries = saa.CountFullBatteries ,
                BatteryInWorkstationCount = saa.BatteryInWorkstationCount ,
                BatteryInChargerCount = saa.BatteryInChargerCount ,
                VacantChargerBays = saa.VacantChargerBays ,
                ActiveChargerBays = saa.ActiveChargerBays ,
                InServiceWorkstationCount = saa.InServiceWorkstationCount ,
                AvailableWorkstationCount = saa.AvailableWorkstationCount ,
                DormantWorkstationCount = saa.DormantWorkstationCount ,
                BayCount = saa.BayCount
        FROM    ( SELECT    sa.SiteID ,
                            sa.DepartmentID ,
                            sa.DormantBatteryCount ,
                            sa.CountFullBatteries ,
                            sa.BatteryInWorkstationCount ,
                            sa.BatteryInChargerCount ,
                            sa.VacantChargerBays ,
                            sa.ActiveChargerBays ,
                            sa.InServiceWorkstationCount ,
                            sa.AvailableWorkstationCount ,
                            sa.DormantWorkstationCount ,
                            sa.BayCount
                  FROM      dbo.SummaryActiveAssets sa
                  WHERE     sa.CreatedDateUTC = @CreatedDateUTC
                ) saa
        WHERE   IDSite = saa.SiteID
                AND IDDepartment = saa.DepartmentID;


	--COMMIT
	-- ***************************************************************
	--   Refresh SummaryBusinessUnitDevice table: END
	-- ***************************************************************
        RAISERROR ('At end...' , 0, 1) WITH NOWAIT;
        PRINT 'At end';
	-- required for CAST 2.0 landing page:
        EXEC dbo.spSummarySiteDepartment_RollupNow;

	-- append battery run rate rows to SummaryBatteryRunRate
        EXEC dbo.spSummary_BatteryRunRate_Append;

    END;


GO
