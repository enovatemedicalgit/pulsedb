SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[spSendShutDownToBRP]
AS
    BEGIN

        DECLARE @SN VARCHAR(50);
        DECLARE @DeviceSerial VARCHAR(50);
        DECLARE @LastPacketDate DATETIME;
        DECLARE @Bay INT;
        DECLARE @MaxCycleCount INT;
        DECLARE @BusinessUnitID INT;
        DECLARE @SiteDescription VARCHAR(50);
        DECLARE db_cursor CURSOR
        FOR
            SELECT  BatterySerialNumber
            FROM    dbo.brp
            WHERE   BatterySerialNumber IN (
                    SELECT  BatterySerialNumber
                    FROM    dbo.brp
                    WHERE   BatterySerialNumber IN (
                            SELECT  SerialNo
                            FROM    dbo.Assets
                            WHERE   LastPostDateUTC > DATEADD(DAY, -3,
                                                              GETDATE()) ) );

        OPEN db_cursor;   
        FETCH NEXT FROM db_cursor INTO @SN;   
        WHILE @@FETCH_STATUS = 0
            BEGIN  
                SET @MaxCycleCount = ( SELECT TOP 1
                                                MaxCycleCount
                                       FROM     dbo.viewAllSessionData
                                       WHERE    BatterySerialNumber = @SN
                                                AND ( MaxCycleCount <> 0
                                                      AND CreatedDateUTC > DATEADD(DAY,
                                                              -1, GETDATE())
                                                    )
                                       ORDER BY CreatedDateUTC DESC
                                     );
                SET @LastPacketDate = ( SELECT TOP 1
                                                CreatedDateUTC
                                        FROM    dbo.viewAllSessionData
                                        WHERE   BatterySerialNumber = @SN
                                                AND ( MaxCycleCount <> 0
                                                      AND CreatedDateUTC > DATEADD(DAY,
                                                              -1, GETDATE())
                                                    )
                                        ORDER BY CreatedDateUTC DESC
                                      );
                SET @DeviceSerial = ( SELECT TOP 1
                                                DeviceSerialNumber
                                      FROM      dbo.viewAllSessionData
                                      WHERE     BatterySerialNumber = @SN
                                                AND CreatedDateUTC > DATEADD(DAY,
                                                              -1, GETDATE())
                                      ORDER BY  CreatedDateUTC DESC
                                    );
                SET @SiteDescription = ( SELECT TOP 1
                                                SiteDescription
                                         FROM   dbo.Sites
                                         WHERE  IDSite = ( SELECT TOP 1
                                                              SiteID
                                                           FROM
                                                              dbo.Assets
                                                           WHERE
                                                              SerialNo = @SN
                                                         )
                                       );
                SET @Bay = ( SELECT TOP 1
                                    Bay
                             FROM   dbo.viewAllSessionData
                             WHERE  BatterySerialNumber = @SN
                                    AND ChargeLevel < 95
                                    AND CreatedDateUTC > DATEADD(DAY, -1,
                                                              GETDATE())
                                    AND Bay > 0
                                    AND Bay != ''
                                    AND MaxCycleCount > 0
                                    AND Bay IS NOT NULL
                                    AND MaxCycleCount IS NOT NULL
                             ORDER BY CreatedDateUTC DESC
                           );
                SET @BusinessUnitID = ( SELECT TOP 1
                                                SiteID
                                        FROM    dbo.Assets
                                        WHERE   SerialNo = @SN
                                      );
                IF ( @BusinessUnitID IS NULL )
                    BEGIN
                        SET @BusinessUnitID = ( SELECT TOP 1
                                                        SiteID
                                                FROM    dbo.Assets
                                                WHERE   SerialNo = @DeviceSerial
                                              );
                    END;
                SET @SiteDescription = ( SELECT TOP 1
                                                SiteDescription
                                         FROM   dbo.Sites
                                         WHERE  IDSite = @BusinessUnitID
                                       );

                IF ( @MaxCycleCount IS NOT NULL
                     AND @Bay IS NOT NULL
                     AND @Bay <> 0
                     AND @LastPacketDate IS NOT NULL
                     AND @MaxCycleCount <> 0
                     AND @DeviceSerial IS NOT NULL
                   )
                    BEGIN
                        BEGIN TRANSACTION; 
                        UPDATE  dbo.DisabledBatteryList
                        SET     Bay = @Bay ,
                                CommandSentOn = GETDATE() ,
                                SiteDescription = @SiteDescription ,
                                MaxCycleCount = @MaxCycleCount ,
                                LastUpdateDateTime = GETDATE() ,
                                LastPacketDate = @LastPacketDate ,
                                DeviceSerial = @DeviceSerial ,
                                SiteID = @BusinessUnitID
                        WHERE   SerialNumber = @SN;
                        IF ( @MaxCycleCount <> 10
                             AND @LastPacketDate > DATEADD(MINUTE, -20,
                                                           GETDATE())
                           )
                            BEGIN
                                INSERT  INTO dbo.CommandQueue
                                        ( SerialNumber ,
                                          CommandText
                                        )
                                VALUES  ( @DeviceSerial ,
                                          '809(' + CAST(@Bay AS VARCHAR(3))
                                          + ')[' + '10' + ']'
                                        );
		--PRINT N'Found SN=' + cast(@SN as nvarchar(30)) + N' and MCC = ' + CAST(@MaxCycleCount as nvarchar(3)) + N' ' + CAST(@DeviceSerial as nvarchar(14));
                            END;
                        COMMIT;

                    END;
                FETCH NEXT FROM db_cursor INTO @SN;
            END;   

        CLOSE db_cursor;   
        DEALLOCATE db_cursor;

    END;
GO
