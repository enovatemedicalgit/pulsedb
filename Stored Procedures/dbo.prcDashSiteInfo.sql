SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:      <ENOVATE\bill.murray,BILLMURRAY>
-- Create date: <10/31/2016> <3:24 PM>
-- Description: 
--				Default Description of Stored Proc with Supporting Links, Notes, etc.     
--
-- 
-- =============================================
CREATE PROCEDURE [dbo].[prcDashSiteInfo]
@SiteId AS INT = 1238
AS
    BEGIN
        SET NOCOUNT ON;
     SELECT  * FROM sites s LEFT JOIN staging.SFCustomerUserXWalk AS SCUXW ON s.SfAcctOwnerId = scuxw.OwnerId AND s.SfAccountId = scuxw.[Account ID] LEFT JOIN cases c ON c.SFCaseContactEmail = s.POCEmail AND c.SFCaseContactEmail IS NOT null  WHERE s.IDSite = @SiteId
      
 
    END;
GO
