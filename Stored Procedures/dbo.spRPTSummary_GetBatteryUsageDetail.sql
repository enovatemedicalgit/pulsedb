SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Arlow Farrell
-- Create date: 2011-12-13
-- Description:	Gets workstation detail rows
--              for workstationsummary.aspx page
--              in CAST. These are the rows
--              that display when the user
--              expands the department row.
--
--              Date must be formatted as follows:
--              2011/12/13
--
-- =============================================
CREATE PROCEDURE [dbo].[spRPTSummary_GetBatteryUsageDetail]
    (
      @Date VARCHAR(10) ,
      @BusinessUnitID INT
    )
AS
    BEGIN
        SET NOCOUNT ON;
		
        SELECT  s.SerialNo ,
                s.AssetNumber ,
                s.IDSite ,
                s.IDDepartment ,
                s.Floor ,
                s.Wing ,
                s.Other ,
                s.Status ,
                s.Utilization ,
                s.AvgRunRate ,
                s.SessionCount ,
                s.LoChargeInsertsCount ,
                s.HiChargeRemovalsCount ,
                s.AvgAmpDraw ,
                s.AvgEstimatedPCUtilization ,
                dbo.fnGetBatteryCapacity_Int(d.SerialNo, d.FullChargeCapacity) AS Capacity
        FROM    dbo.SummaryWorkstation s
                JOIN dbo.Assets d ON s.SerialNo = d.SerialNo
        WHERE   s.IDSite = @BusinessUnitID
                AND s.Date = @Date; 

    END;

    SELECT  *
    FROM    dbo.AlertMessage
    WHERE   BatterySerialNumber LIKE '%555%';
GO
