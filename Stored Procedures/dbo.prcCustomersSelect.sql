SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcCustomersSelect] @IDCustomer INT = 0
AS
    SET NOCOUNT ON; 
     

    BEGIN TRAN;
    IF ( @IDCustomer = 0 )
        BEGIN
            SELECT  [IDCustomer] ,
                    [CASTBUID] ,
                    [CustomerName] ,
                    [SFCustomerID] ,
                    [SytelineCustomerNum] ,
                    [SytelineCustomerSeq]
            FROM    dbo.Customers; 
        END;
    ELSE
        BEGIN
            SELECT  [IDCustomer] ,
                    [CASTBUID] ,
                    [CustomerName] ,
                    [SFCustomerID] ,
                    [SytelineCustomerNum] ,
                    [SytelineCustomerSeq]
            FROM    dbo.Customers
            WHERE   ( [IDCustomer] = @IDCustomer
                      OR @IDCustomer IS NULL
                    ); 
        END;
    COMMIT;




GO
