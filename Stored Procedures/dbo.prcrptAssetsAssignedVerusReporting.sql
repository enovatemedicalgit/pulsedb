SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcrptAssetsAssignedVerusReporting]
AS
    SET NOCOUNT ON; 
	
    BEGIN 
        SELECT  ass.SerialNo ,
                cus1.CustomerName AS 'Assigned Customer/IDN' ,
                s.SiteName AS 'Assigned Site' ,
                cus2.CustomerName AS 'Reporting Customer/IDN' ,
                s2.SiteName AS 'Reporting Site' ,
                s.CustomerID AS 'Assigned Customer/IDN' ,
                s.IDSite AS 'Assigned SiteID' ,
                s2.CustomerID AS 'Reporting IDN (CustomerID)' ,
                ass.SourceIPAddress AS 'Reporting Base' ,
                s2.IDSite AS 'Reporting SiteID'
        FROM    dbo.Assets ass
                LEFT JOIN dbo.Sites s ON ass.SiteID = s.IDSite
                LEFT JOIN dbo.SiteIPSubnet sip ON LEFT(ass.SourceIPAddress,
                                                   LEN(ass.SourceIPAddress)
                                                   - CHARINDEX('.',
                                                              REVERSE(ass.SourceIPAddress))) = sip.SourceIPAddressBase
                                              AND LEFT(ass.IP,
                                                       LEN(ass.IP)
                                                       - CHARINDEX('.',
                                                              REVERSE(ass.IP))) = sip.LANIPAddressBase
                LEFT JOIN dbo.Sites s2 ON s2.IDSite = sip.SiteID
                LEFT JOIN dbo.Customers cus1 ON s.CustomerID = cus1.IDCustomer
                LEFT JOIN dbo.Customers cus2 ON s2.CustomerID = cus2.IDCustomer
        WHERE   ass.Retired = 0
                AND s.IDSite <> s2.IDSite
                AND ass.LastPostDateUTC > DATEADD(WEEK, -2, GETDATE());
    END;


GO
