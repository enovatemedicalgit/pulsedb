SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--SELECT * FROM dbo.AlertMessage AS AM WHERE AM.SiteID = 696 ORDER BY AM.CreatedDateUTC desc

CREATE PROCEDURE [dbo].[prcDashGetNotifications]
    @SiteId int = 1212
AS
    BEGIN
    SELECT TOP 500
                    ISNULL(astat.Description, 'New, No Response') AS "Case Status" ,  
					CASE WHEN ac.AppliesTo = 1 THEN ass.SerialNo
                         WHEN ac.AppliesTo = 0 THEN ass.SerialNo
                         --ELSE ass2.SerialNo
					ELSE am.BatterySerialNumber
                    END AS SerialNumber ,
                    am.DeviceSerialNumber ,
                    am.BatterySerialNumber ,
                    am.Description ,
				
                     CONVERT(VARCHAR,CAST(CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, am.CreatedDateUTC),
                                                              DATENAME(TzOffset,
                                                              SYSDATETIMEOFFSET()))) AS DATETIME), 100) AS  'Event Date' ,
                    ass.Floor ,
                    ass.Wing ,
                    CASE WHEN am.Priority = 211 THEN 'Info'
						 WHEN am.Priority = 411 THEN 'Info'
                        
                         ELSE 'Notification'
                    END AS PacketType , 
					CASE WHEN am.Priority = 211 THEN 'Info'
					 WHEN am.Priority = 411 THEN 'Info'
                        
                         ELSE 'Notification'
                    END AS 'Notification Type',
				ac.AppliesTo,
                  
                    am.FriendlyDescription ,
                    CASE WHEN ac.AppliesTo = 1 THEN 'WKSTN'
                         WHEN ac.AppliesTo = 2 THEN 'BATT'
                         ELSE 'CHRG'
                    END AS AssetDesc,
				am.ROW_ID AS RowId
            FROM    dbo.AlertMessage am
                    LEFT JOIN dbo.AlertStatus astat ON am.AlertStatus = astat.ROW_ID
                    left JOIN dbo.Assets ass ON ass.SerialNo = am.DeviceSerialNumber
                    left JOIN dbo.Assets ass2 ON ass.SerialNo = am.BatterySerialNumber
                    LEFT JOIN dbo.AlertConditions ac ON am.AlertConditionID = ac.ROW_ID
                    WHERE  am.SiteID = @SiteID   
              and am.CreatedDateUTC > DATEADD(DAY,-30,GETUTCDATE()) AND am.Priority IN (211,411,511) AND ass.Retired = 0
            ORDER BY am.CreatedDateUTC DESC;
        END;




   

GO
