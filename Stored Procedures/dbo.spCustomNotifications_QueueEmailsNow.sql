SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Arlow Farrell
-- Create date: 2013-02-07
-- Description:	Composes and queues emails
--              when a notification has been
--              received and one or more users
--              have subscribed to that notification

-- Modified 7/25/2013 DC
-- Rebranding, changed colors, names, hyperlinks...etc
-- =============================================
CREATE PROCEDURE [dbo].[spCustomNotifications_QueueEmailsNow]
AS
    BEGIN
        SET NOCOUNT ON;

	-- This is intended to run every X minutes

	-- if last execution was more than 8 hours ago, limit qualifying notifications to the past 8 hours
	-- This is a safety net, preventing us from sending emails about "stale" notifications
        DECLARE @LastExecutedUTC DATETIME;
        IF ( SELECT COUNT(*)
             FROM   dbo.Job
             WHERE  JobCode = 'OPTNOTIF'
                    AND LastRanTimestampUTC > DATEADD(HOUR, -8, GETUTCDATE())
           ) > 0
            BEGIN
                SET @LastExecutedUTC = ( SELECT TOP 1
                                                LastRanTimestampUTC
                                         FROM   dbo.Job
                                         WHERE  JobCode = 'OPTNOTIF'
                                       );
            END;
        ELSE
            BEGIN
                SET @LastExecutedUTC = DATEADD(HOUR, -8, GETUTCDATE());
            END;

        IF ( SELECT COUNT(*)
             FROM   dbo.AlertSubscription
           ) > 0
            BEGIN

                IF OBJECT_ID(N'tempdb..#tempAlertMessage', N'U') IS NOT NULL
                    BEGIN
                        DROP TABLE #tempAlertMessage;
                    END;

                SELECT  am.*
                INTO    #tempAlertMessage
                FROM    dbo.AlertMessage am
                WHERE   am.CreatedDateUTC > @LastExecutedUTC
                        AND am.AlertConditionID IN (
                        SELECT DISTINCT
                                asubc.AlertConditionId
                        FROM    dbo.AlertSubscribeCondition asubc
                                JOIN dbo.AlertSubscription alsub ON asubc.AlertSubscribeId = alsub.AlertSubscribeId );

                IF OBJECT_ID(N'tempdb..#tempSiteAlertSubscriber', N'U') IS NOT NULL
                    BEGIN
                        DROP TABLE #tempSiteAlertSubscriber;
                    END;

		-- get distinct list of SiteID + AlertSubscribeId
                SELECT DISTINCT
                        SiteID ,
                        AlertSubscribeId
                INTO    #tempSiteAlertSubscriber
                FROM    dbo.AlertSubscription;

                IF OBJECT_ID(N'tempdb..#emailDetail', N'U') IS NOT NULL
                    BEGIN
                        DROP TABLE #emailDetail;
                    END;


                SELECT DISTINCT
                        asci.AlertSubscribeId ,
                        asub.Subscribing_DisplayText ,
                        asub.Notification_DisplayText ,
                        dt.Category AS DeviceType ,
                        tam.DeviceSerialNumber ,
                        tam.BatterySerialNumber ,
                        tam.SiteID
                INTO    #emailDetail
                FROM    #tempAlertMessage tam
                        JOIN dbo.AlertSubscribeCondition asci ON tam.AlertConditionID = asci.AlertConditionId
                        JOIN #tempSiteAlertSubscriber buas ON asci.AlertSubscribeId = buas.AlertSubscribeId
                                                              AND tam.SiteID = buas.SiteID
                        JOIN dbo.AlertSubscribe asub ON asci.AlertSubscribeId = asub.AlertSubscribeId
                        JOIN dbo.Assets d ON tam.DeviceSerialNumber = d.SerialNumber
                        JOIN AssetsType dt ON d.IDAssetType = dt.[ID];

                IF OBJECT_ID(N'tempdb..#emailHeader', N'U') IS NOT NULL
                    BEGIN
                        DROP TABLE #emailHeader;
                    END;


		-- For *each email* we need only the AlertSubscribeId and SiteId
		-- We can connect back to #emailDetail for the specifics
		-- If you want to count how many emails will be sent, this (#emailHeader) is the place to get that 
                SELECT DISTINCT
                        AlertSubscribeId ,
                        SiteID ,
                        Subscribing_DisplayText ,
                        Notification_DisplayText
                INTO    #emailHeader
                FROM    #emailDetail;


		-- get email from (address and name), required for inserting into EmailQueue table
                DECLARE @FromAddress VARCHAR(150);
                DECLARE @FromName VARCHAR(150);

                SELECT TOP 1
                        @FromAddress = a.DefaultFromAddress ,
                        @FromName = b.DefaultFromName
                FROM    ( SELECT TOP 1
                                    1 AS joinkey ,
                                    SettingValue AS DefaultFromAddress
                          FROM      dbo.SystemSettings
                          WHERE     SettingKey = 'EMAIL_NOTIFICATIONS_DEFAULT_FROM_ADDRESS'
                        ) a
                        JOIN ( SELECT TOP 1
                                        1 AS joinkey ,
                                        SettingValue AS DefaultFromName
                               FROM     dbo.SystemSettings
                               WHERE    SettingKey = 'EMAIL_NOTIFICATIONS_DEFAULT_FROM_NAME'
                             ) b ON a.joinkey = b.joinkey;

		-- FOR EACH ENTRY IN #emailHeader

                DECLARE @AlertSubscribeId INT;
                DECLARE @SiteId INT;
                DECLARE @Subscribing_DisplayText VARCHAR(50);
                DECLARE @Notification_DisplayText VARCHAR(MAX);

                DECLARE SiteCursor CURSOR
                FOR
                    SELECT DISTINCT
                            AlertSubscribeId ,
                            SiteID ,
                            Subscribing_DisplayText ,
                            Notification_DisplayText
                    FROM    #emailHeader FOR READ ONLY;

                OPEN SiteCursor;
                FETCH SiteCursor INTO @AlertSubscribeId, @SiteId,
                    @Subscribing_DisplayText, @Notification_DisplayText;
                WHILE ( @@FETCH_STATUS = 0 )
                    BEGIN
                        DECLARE @body_message VARCHAR(MAX);
                        DECLARE @Title VARCHAR(150);
                        DECLARE @Recipients VARCHAR(MAX);
                        DECLARE @RecipientsXML VARCHAR(MAX);
			

                        SET @RecipientsXML = ( SELECT   CAST(( SELECT
                                                              u.Email
                                                              FROM
                                                              dbo.[User] u
                                                              JOIN ( SELECT
                                                              *
                                                              FROM
                                                              dbo.AlertSubscription
                                                              WHERE
                                                              SiteID = @SiteId
                                                              AND AlertSubscribeId = @AlertSubscribeId
                                                              ) s ON u.IDUser = s.UserID
                                                              WHERE
                                                              u.IDSite = @SiteId
                                                              AND s.UserID IN (
                                                              SELECT
                                                              User_ROW_ID
                                                              FROM
                                                              dbo.UserOptions
                                                              WHERE
                                                              OptionKey = 'CAN_LOGIN_TO_CAST'
                                                              AND OptionValue = 1 )
                                                             FOR
                                                              XML
                                                              PATH('Recipients') ,
                                                              TYPE
                                                             ) AS NVARCHAR(MAX))
                                             );

                        SET @Recipients = ( SELECT  REPLACE(REPLACE(REPLACE(REPLACE(@RecipientsXML,
                                                              '<Recipients>',
                                                              ''),
                                                              '</Recipients>',
                                                              ''), '<Email>',
                                                              ''), '</Email>',
                                                            '; ')
                                          );


                        SET @Title = 'CAST Notification: '
                            + @Subscribing_DisplayText;
			
                        DECLARE @bodyheader VARCHAR(150);
                        SET @bodyheader = @Subscribing_DisplayText;


                        SET @body_message = N'<style>body {padding: 20px; color:Gray; font-family: segoe ui, Arial, Helvetica, sans-serif;} h4 {margin-bottom: 2px;} '
                            + N' h5 {margin-bottom: 2px;} .fyi {font-size: smaller;} '
                            + N'</style> ' + N'<h4>' + @bodyheader + '</h4> '
                            + N'<h5>Applies to:</h5>' + N'<blockquote>';
						
		   -- FOR EACH ENTRY IN #emailDetail
		   -- show applicable serial number(s)
		   
		   
		   /* ============ detail cursor : top ============ */
                        DECLARE @DSN VARCHAR(20);
                        DECLARE @BSN VARCHAR(20);
                        DECLARE @DeviceType VARCHAR(20);
		   
                        DECLARE DetailCursor CURSOR
                        FOR
                            SELECT DISTINCT
                                    DeviceSerialNumber ,
                                    BatterySerialNumber ,
                                    DeviceType
                            FROM    #emailDetail
                            WHERE   AlertSubscribeId = @AlertSubscribeId
                                    AND SiteID = @SiteId FOR READ ONLY;
		   
                        OPEN DetailCursor;
                        FETCH DetailCursor INTO @DSN, @BSN, @DeviceType;
                        WHILE ( @@FETCH_STATUS = 0 )
                            BEGIN
                                SET @body_message = @body_message + N''
                                    + @DeviceType + ': ' + @DSN
                                    + ', Battery: ' + @BSN + '<br>';
			   --print '   ' + @DeviceType + ': ' + @DSN + ', Battery: ' + @BSN
                                FETCH DetailCursor INTO @DSN, @BSN,
                                    @DeviceType;
                            END;
                        CLOSE DetailCursor;
                        DEALLOCATE DetailCursor;
		   /* ============ detail cursor : end ============ */

                        SET @body_message = @body_message + N'</blockquote>'
                            + N'<h5>Recommended action:</h5>' + N''
                            + @Notification_DisplayText + N'<p>&nbsp;</p>'
                            + N'<p class=fyi>You received this email because you are currently subscribed to the '''
                            + @Subscribing_DisplayText
                            + ''' notification in CAST. <a href="https://cast.myenovate.com/optionalnotifications.aspx">Click here</a> to manage your notification subscriptions.</p>'
                            + N'<p>&nbsp;</p>' + N'<p>&nbsp;</p>';

                        INSERT  INTO dbo.EmailQueue
                                ( [Subject] ,
                                  ToAddresses ,
                                  Body ,
                                  Attempts ,
                                  FromAddress ,
                                  FromName
                                )
                                SELECT  @Title ,
                                        @Recipients ,
                                        @body_message ,
                                        0 ,
                                        @FromAddress ,
                                        @FromName; -- set attempts to 99 so the standard email processing app won't touch these rows

                        FETCH SiteCursor INTO @AlertSubscribeId, @SiteId,
                            @Subscribing_DisplayText,
                            @Notification_DisplayText;
                    END;
                CLOSE SiteCursor;
                DEALLOCATE SiteCursor;

            END;

        UPDATE  dbo.Job
        SET     LastRanTimestampUTC = GETUTCDATE()
        WHERE   JobCode = 'OPTNOTIF';

		/*Cleanup*/			
        BEGIN TRY
            DROP TABLE #tempAlertMessage;
            DROP TABLE #tempSiteAlertSubscriber;
            DROP TABLE #emailDetail;
            DROP TABLE #emailHeader;
        END TRY
        BEGIN CATCH
        END CATCH;

    END;


-------------------------------------------------------------------------------------------------------------------------------------------------------------
--&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
-------------------------------------------------------------------------------------------------------------------------------------------------------------
--&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
-------------------------------------------------------------------------------------------------------------------------------------------------------------
--&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&



GO
