SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[spTestSiteResults] @ViewAll INT = 0
AS
    BEGIN
        DECLARE @TestSiteID INT;
        SET @TestSiteID = ( SELECT  SettingValue
                            FROM    dbo.SystemSettings
                            WHERE   SettingKey = 'TestSiteID'
                          );
        DECLARE @ReplicatedSiteID INT;
        SET @ReplicatedSiteID = ( SELECT    SettingValue
                                  FROM      dbo.SystemSettings
                                  WHERE     SettingKey = 'ReplicatedSiteID'
                                );
        IF ( @ViewAll = 0 )
            BEGIN
--SELECT TOP 3000 s.SiteName,tlxy.*  FROM [TestLogXY] tlxy left join Pulse.dbo.Sites s on s.IDSite = tlxy.testsiteid left join Pulse.dbo.Sites s2 on s2.IDSite = @ReplicatedSiteID  where tlxy.TestSiteID = @TestSiteID ORDER BY tlxy.WorkstationsAllocatedDepartment desc
                SELECT TOP 2000
                        s.SiteName ,
                        tlxy.LogDate ,
                        tlxy.workstations ,
                        tlxy.chargers ,
                        tlxy.batteries ,
                        tlxy.WorkstationsOriginal ,
                        tlxy.ChargersOriginal ,
                        tlxy.BatteriesOriginal ,
                        tlxy.WorkstationsAllocated ,
                        tlxy.WorkstationsAllocatedOriginal ,
                        tlxy.ChargersAllocated ,
                        tlxy.ChargersAllocatedOriginal ,
                        tlxy.WorkstationsAllocatedDepartment ,
                        tlxy.WorkstationsAllocatedDepartmentOriginal ,
                        tlxy.ChargersAllocatedDepartment ,
                        tlxy.ChargersAllocatedDepartmentOriginal
                FROM    dbo.TestLogXY tlxy
                        LEFT JOIN pulse.dbo.Sites s ON s.IDSite = tlxy.TestSiteID
                        LEFT JOIN pulse.dbo.Sites s2 ON s2.IDSite = @ReplicatedSiteID
                WHERE   tlxy.TestSiteID = @TestSiteID
                ORDER BY tlxy.WorkstationsAllocatedDepartment DESC;
            END;
        ELSE
            BEGIN
                SELECT  s.SiteName ,
                        tlxy.*
                FROM    dbo.TestLogXY tlxy
                        LEFT JOIN pulse.dbo.Sites s ON s.IDSite = tlxy.TestSiteID
                        LEFT JOIN pulse.dbo.Sites s2 ON s2.IDSite = ( SELECT TOP 1
                                                              SiteReplicatedID
                                                              FROM
                                                              dbo.TestLogXY
                                                              WHERE
                                                              TestSiteID = s.IDSite
                                                              )
                ORDER BY tlxy.LogDate DESC;
                SELECT  s.SiteName ,
                        tlxy.LogDate ,
                        tlxy.workstations ,
                        tlxy.chargers ,
                        tlxy.batteries ,
                        tlxy.WorkstationsOriginal ,
                        tlxy.ChargersOriginal ,
                        tlxy.BatteriesOriginal ,
                        tlxy.WorkstationsAllocated ,
                        tlxy.WorkstationsAllocatedOriginal ,
                        tlxy.ChargersAllocated ,
                        tlxy.ChargersAllocatedOriginal ,
                        tlxy.WorkstationsAllocatedDepartment ,
                        tlxy.WorkstationsAllocatedDepartmentOriginal ,
                        tlxy.ChargersAllocatedDepartment ,
                        tlxy.ChargersAllocatedDepartmentOriginal
                FROM    dbo.TestLogXY tlxy
                        LEFT JOIN pulse.dbo.Sites s ON s.IDSite = tlxy.TestSiteID
                        LEFT JOIN pulse.dbo.Sites s2 ON s2.IDSite = @ReplicatedSiteID
                ORDER BY tlxy.WorkstationsAllocatedDepartment DESC;
            END;
--convert(smalldatetime,convert(varchar(10),datepart(MONTH,tlxy.LogDate),101) + '-' + convert(varchar(10),datepart(Day,tlxy.LogDate),101) + ' ' + convert(varchar(10),datepart(HOUR,tlxy.LogDate),101) + ':' + convert(varchar(10),datepart(MINUTE,tlxy.LogDate),101))
    END;
GO
