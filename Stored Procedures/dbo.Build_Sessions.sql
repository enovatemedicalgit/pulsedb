SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*################################################################################################
 Name				: Build_Sesssions
 Date				: 
 Author				: christopher.stewart
 Company				: enovate 
 Purpose				: Build the Session Data Table
 Usage				:   
 Impact				:   
 Required Perm			:   
 Called by			:   
##################################################################################################
 ver		author					date			change  
 1.0		Arlow.Farrell			Unknown			initial [spSessionData_Rollup2]
 1.1		Christopher.Stewart		07012016			Optimizition and Comments
#################################################################################################*/

/*##################################################################################################
 STEPS		 Description						 
 1			 Gather all recent packets into #SessionCompare
 2			 Find the duration endpoints into #SessionRollup
 3			 Gather the packets that have the session endpoints into #NewSessionRows
 4			 Insert into Sessions
 5			 Tie packets back to Session.RowID
 6			 Update the Packet count on Sessions table
 7			 Update AvgAmpDraw,HiAmpDraw,NumberofMoves
 8			 Update Sessions Set EstimatedPCUtil
 9			 Update Sessions Set SessionLengthMinutes
 10			 Update Sessions Set RunRate
 11			 Update Sessions Set Starting/Ending Charge Levels

#################################################################################################*/
CREATE PROCEDURE [dbo].[Build_Sessions]
AS
DECLARE
	  @LapTimer AS DATETIME2;

	IF OBJECT_ID(N'tempdb..#SessionCompare', N'U') IS NOT NULL
	    BEGIN
		   DROP TABLE #SessionCompare;
	    END;
	IF OBJECT_ID(N'tempdb..#SessionRollup', N'U') IS NOT NULL
	    BEGIN
		   DROP TABLE #SessionRollup;
	    END;
	IF OBJECT_ID(N'tempdb..#NewSessionRows', N'U') IS NOT NULL
	    BEGIN
		   DROP TABLE #NewSessionRows;
	    END;


PRINT dbo.GetMessageBorder();
/*################################################################################################*/

			 Step_1:
			 SET @LapTimer = GETDATE();
			 PRINT CONVERT(VARCHAR(30), @LapTimer, 109)+' Begin Step 1';
			 RAISERROR('Gather all recent packets into #SessionCompare', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/

SELECT
	  ROW_NUMBER() OVER(PARTITION BY DeviceSerialNumber ORDER BY DeviceSerialNumber,
													 CreatedDateUTC) AS DeviceRowNumber,
	  CreatedDateUTC,
	  DeviceSerialNumber,
	  BatterySerialNumber,
	  ROW_ID,
	  IsSession
INTO
	#SessionCompare
FROM
( 
    SELECT DISTINCT
	     CreatedDateUTC,
		DeviceSerialNumber,
		BatterySerialNumber,
		ROW_ID,
	     IsSession FROM	   
	   (
		  SELECT
			    CreatedDateUTC,
			    DeviceSerialNumber,
			    BatterySerialNumber,
			    ROW_ID,
			    1 AS IsSession
		  FROM
			  SessionDataCurrent WITH (readpast)
		  WHERE  (SessionID = 0 OR SessionID IS NULL)
		  	    AND BatteryName IS NOT NULL
			    AND BatteryName <> ''
			    AND LinkQuality IS NOT NULL 
			    AND DeviceType IN
				(
				    SELECT
						 IDAssetType AS ID
				    FROM
					    AssetType
				    WHERE  Category = 'Workstation'
				)
				AND DeviceSerialNumber NOT IN('NotYetSet', '')
		  AND CreatedDateUTC > DATEADD(day, -3, GETUTCDATE())
		  AND Amps < -100	--< Added by arlow. Danny added this line on 3-1-2012 to expedite processing
		  UNION
		  SELECT
			    CreatedDateUTC,
			    DeviceSerialNumber,
			    BatterySerialNumber,
			    ROW_ID,
			    0 AS IsSession
		  FROM
			  NonSessionDataCurrent WITH (readpast)
		  WHERE  CreatedDateUTC > DATEADD(day, -1, GETUTCDATE())
			    AND IsAc = 1
			    AND BatteryName IS NOT NULL
			    AND BatteryName <> ''
			    AND LinkQuality IS NOT NULL
			    AND DeviceType IN
			    (
				SELECT
					  IDAssetType AS ID
				FROM
					AssetType
				WHERE  Category LIKE '%Workstation%'
				)

	   ) AS base
) AS DistinctBase

CREATE CLUSTERED INDEX IX_SessionCompare ON #SessionCompare(DeviceSerialNumber, DeviceRowNumber, IsSession, BatterySerialNumber);
PRINT dbo.GetLapTime(@LapTimer); 

/*################################################################################################*/

			 Step_2:
			 SET @LapTimer = GETDATE();
			 PRINT CONVERT(VARCHAR(30), @LapTimer, 109)+' Begin Step 2';
			 RAISERROR('Find the duration endpoints into #SessionRollup', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/

SELECT
	  ROW_NUMBER() OVER(PARTITION BY currow.DeviceSerialNumber ORDER BY currow.DeviceSerialNumber,
														   currow.CreatedDateUTC) AS RowNum,
	  CASE
		 WHEN prevrow.CreatedDateUTC IS NULL
			 AND currow.IsSession = 1
		 THEN currow.CreatedDateUTC
		 WHEN prevrow.IsSession = 0
			 AND currow.IsSession = 1
		 THEN currow.CreatedDateUTC
		 WHEN prevrow.BatterySerialNumber <> currow.BatterySerialNumber
			 AND currow.IsSession = 1
		 THEN currow.CreatedDateUTC
	  END AS   StartDateUTC,
	  CASE
		 WHEN currow.DeviceSerialNumber = nextrow.DeviceSerialNumber
			 AND nextrow.BatterySerialNumber <> currow.BatterySerialNumber
			 AND currow.IsSession = 1
		 THEN currow.CreatedDateUTC
		 WHEN currow.IsSession = 1
			 AND nextrow.IsSession = 0
		 THEN currow.CreatedDateUTC
	  END AS                                                                                      EndDateUTC,
	  currow.CreatedDateUTC,
	  currow.DeviceSerialNumber,
	  currow.BatterySerialNumber
INTO
	#SessionRollup
FROM
	#SessionCompare currow
	LEFT JOIN
	#SessionCompare nextrow ON currow.DeviceSerialNumber = nextrow.DeviceSerialNumber
						  AND currow.DeviceRowNumber = nextrow.DeviceRowNumber - 1
	LEFT JOIN
	#SessionCompare prevrow ON currow.DeviceSerialNumber = prevrow.DeviceSerialNumber
						  AND currow.DeviceRowNumber = prevrow.DeviceRowNumber + 1

-- exclude rows where there was no change in battery (keep only first and last rows of device/battery combination)
WHERE  CASE
		 WHEN prevrow.CreatedDateUTC IS NULL
			 AND currow.IsSession = 1
		 THEN currow.CreatedDateUTC
		 WHEN prevrow.IsSession = 0
			 AND currow.IsSession = 1
		 THEN currow.CreatedDateUTC
		 WHEN prevrow.BatterySerialNumber <> currow.BatterySerialNumber
			 AND currow.IsSession = 1
		 THEN currow.CreatedDateUTC
	  END IS NOT NULL
	  OR CASE
		    WHEN currow.DeviceSerialNumber = nextrow.DeviceSerialNumber
			    AND nextrow.BatterySerialNumber <> currow.BatterySerialNumber
			    AND currow.IsSession = 1
		    THEN currow.CreatedDateUTC
		    WHEN currow.IsSession = 1
			    AND nextrow.IsSession = 0
		    THEN currow.CreatedDateUTC
		END IS NOT NULL

    DROP TABLE #SessionCompare

    CREATE CLUSTERED INDEX IX_SessionRollup_Covered ON #SessionRollup (DeviceSerialNumber, BatterySerialNumber, StartDateUTC, EndDateUTC, RowNum)
	   
PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/

		   Step_3:
		   SET @LapTimer = GETDATE();
		   PRINT CONVERT(VARCHAR(30), @LapTimer, 109)+' Begin Step 3';
		   RAISERROR('Gather the packets that have the session endpoints into #NewSessionRows', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/

	 SELECT
	   CASE
		  WHEN currow.EndDateUTC IS NOT NULL
				AND currow.StartDateUTC IS NULL
				AND prevrow.StartDateUTC IS NOT NULL
		  THEN prevrow.StartDateUTC
		  WHEN currow.EndDateUTC IS NOT NULL
				AND prevrow.StartDateUTC IS NULL
				AND currow.StartDateUTC IS NOT NULL
		  THEN currow.StartDateUTC
		  WHEN currow.StartDateUTC IS NOT NULL
				AND prevrow.BatterySerialNumber <> currow.BatterySerialNumber
		  THEN currow.StartDateUTC
	   END AS               SessionStartDateUTC,
	   currow.EndDateUTC AS SessionEndDateUTC,
	   d.IDAssetType,
	   currow.DeviceSerialNumber,
	   currow.BatterySerialNumber
    INTO #NewSessionRows
    FROM
	   #SessionRollup currow
	   LEFT JOIN
	   #SessionRollup prevrow ON currow.DeviceSerialNumber = prevrow.DeviceSerialNumber
						  AND currow.RowNum = prevrow.RowNum + 1
	   LEFT JOIN
	   #SessionRollup nextrow ON currow.DeviceSerialNumber = nextrow.DeviceSerialNumber
						  AND currow.RowNum = nextrow.RowNum - 1
	   JOIN
	   dbo.Assets d ON currow.DeviceSerialNumber = d.SerialNo
    WHERE  CASE
		  WHEN currow.EndDateUTC IS NOT NULL
				AND currow.StartDateUTC IS NULL
				AND prevrow.StartDateUTC IS NOT NULL
		  THEN prevrow.StartDateUTC
		  WHEN currow.EndDateUTC IS NOT NULL
				AND prevrow.StartDateUTC IS NULL
				AND currow.StartDateUTC IS NOT NULL
		  THEN currow.StartDateUTC
		  WHEN currow.StartDateUTC IS NOT NULL
				AND prevrow.BatterySerialNumber <> currow.BatterySerialNumber
		  THEN currow.StartDateUTC
	   END IS NOT NULL
	   AND currow.EndDateUTC IS NOT NULL

	   DROP TABLE #SessionRollup

PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/

		   Step_4:
		   SET @LapTimer = GETDATE();
		   PRINT CONVERT(VARCHAR(30), @LapTimer, 109)+' Begin Step 4';
		   RAISERROR('Insert into Sessions', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/

	INSERT INTO Sessions
	(StartDateUTC,
	 EndDateUTC,
	 DeviceType,
	 DeviceSerialNumber,
	 BatterySerialNumber
	)

	SELECT
		  SessionStartDateUTC,
		  SessionEndDateUTC,
		  nsr.IDAssetType,
		  nsr.DeviceSerialNumber,
		  nsr.BatterySerialNumber
	FROM
		#NewSessionRows nsr
		LEFT JOIN
		Sessions existing ON nsr.DeviceSerialNumber = existing.DeviceSerialNumber
						 AND nsr.SessionStartDateUTC = existing.StartDateUTC
						 AND nsr.SessionEndDateUTC = existing.EndDateUTC
						 AND nsr.BatterySerialNumber = existing.BatterySerialNumber
	WHERE  existing.DeviceSerialNumber IS NULL
		  AND DATEDIFF(MINUTE,nsr.SessionStartDateUTC , existing.EndDateUTC) >=1

	DROP TABLE #NewSessionRows

PRINT dbo.GetLapTime(@LapTimer);

/*################################################################################################*/
Step_5:
				    SET @LapTimer = GETDATE();
				    PRINT CONVERT(VARCHAR(30), @LapTimer, 109)+' Begin Step 5';
				    RAISERROR('Tie packets back to Session.RowID', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/

	-- set the SessionID column on SessionDataCurrent for the rows we just rolled up
	UPDATE SessionDataCurrent
	 SET
		SessionID = Sessions.ROW_ID
	FROM Sessions
	WHERE  SessionID = 0
		  AND SessionDataCurrent.DeviceSerialNumber = Sessions.DeviceSerialNumber
		  AND SessionDataCurrent.BatterySerialNumber = Sessions.BatterySerialNumber
		  AND SessionDataCurrent.CreatedDateUTC BETWEEN Sessions.StartDateUTC AND Sessions.EndDateUTC;

PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
Step_6:
		  SET @LapTimer = GETDATE();
		  PRINT CONVERT(VARCHAR(30), @LapTimer, 109)+' Begin Step 6';
		  RAISERROR('Update the Packet count on Sessions table', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/

	-- set the PacketCount column on Sessions	
	UPDATE    Sessions
	 SET
		PacketCount = RowCnt
	FROM
	(
	    SELECT s.ROW_ID,
			 COUNT(SessionID) AS RowCnt
	    FROM
		    Sessions s
		    JOIN
		    SessionDataCurrent sd ON s.ROW_ID = sd.SessionID
	    WHERE  s.PacketCount IS NULL	-- added 7-7-2011 by Danny Bates (to allow update of older rows)
	    GROUP BY s.ROW_ID
	) counts
	WHERE Sessions.ROW_ID = counts.ROW_ID;
	PRINT dbo.GetLapTime(@LapTimer); 

/*################################################################################################*/
Step_7:

		   SET @LapTimer = GETDATE();
		   PRINT CONVERT(VARCHAR(30), @LapTimer, 109)+' Begin Step 7';
		   RAISERROR('Update AvgAmpDraw,HiAmpDraw,NumberofMoves', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/

	-- summarize AvgAmpDraw, NumberOfMoves and BatteryName
	UPDATE    [Sessions]
	 SET
		AvgAmpDraw = temptable.AvgAmpDraw,
		LowAmpDraw = temptable.LowAmpDraw, -- added 9-27-2012 by Danny Bates
		HiAmpDraw = temptable.HiAmpDraw, -- added 9-27-2012 by Danny Bates
		--,EstimatedPCUtilization = temptable.EstimatedPCUtilization	-- added 9-27-2012 by Danny Bates
		NumberOfMoves = temptable.NumberOfMoves,
		BatteryName = temptable.BatteryName,
		ChargePctConsumed = temptable.ChargePctConsumed	-- added 4-11-2011 by Danny Bates
		
	FROM
	(
	    SELECT SessionID,
			 AVG(Amps) AS AvgAmpDraw,
			 MAX(Amps) AS HiAmpDraw, -- added 9-27-2012 by Danny Bates
			 MIN(Amps) AS LowAmpDraw, -- added 9-27-2012 by Danny Bates
			 SUM([Move]) AS NumberOfMoves,
			 MIN(BatteryName) AS BatteryName,
			 MAX(ChargeLevel) - MIN(ChargeLevel) AS ChargePctConsumed	-- added 4-11-2011 by Danny Bates
	    FROM   [SessionDataCurrent] sd
	    WHERE  SessionID IN
	    (
		   SELECT ROW_ID
		   FROM   Sessions
		   WHERE  AvgAmpDraw IS NULL
				AND EndDateUTC > DATEADD(day, -7, GETUTCDATE())
	    )
			 --				 and SessionID <> 0) -- added 7-7-2011 to allow update of older rows
			 AND SessionID <> 0 -- moved 2012-09-27 by Danny Bates. was inside parenthesis, which should have caused a failure, though the job was succeeding somehow
			 AND CreatedDateUTC > DATEADD(day, -9, GETUTCDATE()) 	-- added 9-27-2012 by Danny Bates
	    GROUP BY SessionID
	) temptable
	WHERE [Sessions].ROW_ID = temptable.SessionID;
	PRINT dbo.GetLapTime(@LapTimer); 

/*################################################################################################*/

Step_8:
		   SET @LapTimer = GETDATE();
		   PRINT CONVERT(VARCHAR(30), @LapTimer, 109)+' Begin Step 8';
		   RAISERROR('Update Sessions Set EstimatedPCUtil', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/

	-- now update EstimatedPCUtilization column

/*
	Calculation for EstimatedPCUtilization, as provided by Katie Matter
	(1-((HiAmpDraw - AvgAmpDraw)/(HiAmpDraw - LowAmpDraw)))*100
	*/

	UPDATE Sessions
	 SET
		EstimatedPCUtilization = (1 - (((HiAmpDraw) - AvgAmpDraw) / (HiAmpDraw - LowAmpDraw)))
	WHERE  HiAmpDraw > LowAmpDraw
		  AND EndDateUTC > DATEADD(day, -7, GETUTCDATE())
		  AND EstimatedPCUtilization IS NULL;
	
	UPDATE Sessions
	SET
		EstimatedPCUtilization = 100
	WHERE  HiAmpDraw = LowAmpDraw
		  AND EndDateUTC > DATEADD(day, -7, GETUTCDATE())
		  AND EstimatedPCUtilization IS NULL;

	PRINT dbo.GetLapTime(@LapTimer); 

/*################################################################################################*/

Step_9:
	SET @LapTimer = GETDATE();
	PRINT CONVERT(VARCHAR(30), @LapTimer, 109)+' Begin Step 9';
	RAISERROR('Update Sessions Set SessionLengthMinutes', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/

	-- summarize SessionLengthMinutes and BusinessUnitID
	UPDATE Sessions
	 SET

		SessionLengthMinutes = DATEDIFF(n, StartDateUTC, EndDateUTC),
		SiteID = d.SiteId,
		DepartmentId = d.DepartmentId
	FROM Assets d
	WHERE  DeviceSerialNumber = d.SerialNo
		  AND d.Retired <> 1
		  AND EndDateUTC > DATEADD(DAY, -7, GETUTCDATE())
		  AND SessionLengthMinutes IS NULL;	-- added 7-7-2011 by Danny Bates (to allow update of older rows)

	PRINT dbo.GetLapTime(@LapTimer); 

/*################################################################################################*/

Step_10:
			  SET @LapTimer = GETDATE();
			  PRINT CONVERT(VARCHAR(30), @LapTimer, 109)+' Begin Step 10';
			  RAISERROR('Update Sessions Set RunRate', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/

	-- update the RunRate
	UPDATE Sessions
	 SET
		RunRate = ((100 * SessionLengthMinutes) / ChargePctConsumed)
	WHERE  EndDateUTC > DATEADD(DAY, -7, GETUTCDATE())
		  AND ISNULL(SessionLengthMinutes, 0) > 0
		  AND ISNULL(ChargePctConsumed, 0) > 0
		  AND ISNULL(RunRate, 0) = 0; 	-- added 7-7-2011 by Danny Bates (to allow update of older rows)

    	UPDATE Sessions
	SET
		RunRate = 0
	WHERE  EndDateUTC > DATEADD(DAY, -7, GETUTCDATE())
		  AND (ChargePctConsumed IS NULL OR ChargePctConsumed = 0)
		  AND RunRate IS NULL 	
	PRINT dbo.GetLapTime(@LapTimer); 

/*################################################################################################*/

Step_11:
		   SET @LapTimer = GETDATE();
		   PRINT CONVERT(VARCHAR(30), @LapTimer, 109)+' Begin Step 11';
		   RAISERROR('Update Sessions Set Starting/Ending Charge Levels', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/

	-- update Starting and Ending charge levels
	UPDATE [Sessions]
	 SET
		[Sessions].StartChargeLevel = sd.ChargeLevel,
		[Sessions].EndChargeLevel = sd2.ChargeLevel,
		[Sessions].AvgSignalQuality = sd.LinkQuality,
		[Sessions].RemainingCapacity = dbo.fnGetBatteryCapacity_int(sd.batteryserialnumber, sd.FullChargeCapacity)
	FROM SessionDataCurrent AS sd, SessionDataCurrent AS sd2
	WHERE  EndDateUTC > DATEADD(DAY, -7, GETUTCDATE())
		  AND [Sessions].ROW_ID = sd.SessionID
		  AND [Sessions].StartDateUTC = sd.CreatedDateUTC
		  AND [Sessions].ROW_ID = sd2.SessionID
		  AND [Sessions].EndDateUTC = sd2.CreatedDateUTC
		  AND ISNUMERIC(sd.LinkQuality) = 1;
	PRINT dbo.GetLapTime(@LapTimer); 




GO
