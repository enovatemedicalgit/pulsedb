SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Arlow Farrell
-- Create date: 2011-04-05
-- Description:	summarizes counts of active
--              workstations, chargers, batteries
--              reporting within past 7 days.
--              Adds rows to SummaryActiveDevices
-- =============================================
CREATE PROCEDURE [dbo].[spSummaryActiveAssets_RollupNow]
AS
    BEGIN

	
        SET NOCOUNT ON;

        DECLARE @CreatedDateUTC DATETIME;
        SET @CreatedDateUTC = CAST(CONVERT(VARCHAR(10), GETDATE(), 101) AS DATETIME);
        PRINT @CreatedDateUTC;

        DECLARE @starttime DATETIME;
        SET @starttime = GETDATE();
	---- only need one summary per day
        RAISERROR ('Delete from SummaryActiveAssets' , 0, 1) WITH NOWAIT;
        DELETE  FROM dbo.SummaryActiveAssets
        WHERE   CreatedDateUTC = @CreatedDateUTC;

	--BEGIN TRANSACTION
	--SELECT DATEDIFF(ss, @starttime, GETDATE())
	--RAISERROR ('Insert into SummaryActiveAssets from Session/NoSession Assets' , 0, 1) WITH NOWAIT
	---- establish a row for each business unit / department where a device has reported within the past 7 days
	--insert into SummaryActiveAssets (SiteID, DepartmentID, CreatedDateUTC)
	--select distinct d.SiteID, d.DepartmentID, @CreatedDateUTC
	--from SessionDataCurrent sd with (readpast)
	--join assets d
	--on sd.DeviceSerialNumber = d.SerialNo
	--where sd.CreatedDateUTC > DATEADD(d, -7, @CreatedDateUTC)
	--and d.OutOfService = 0
	--union
	--select distinct d.SiteID, d.DepartmentID, @CreatedDateUTC
	--from nonSessionDataCurrent   nsd with (readpast)
	--join assets d
	--on nsd.DeviceSerialNumber = d.SerialNo
	--where nsd.CreatedDateUTC > DATEADD(d, -7, @CreatedDateUTC)
	--and d.OutOfService = 0
        IF OBJECT_ID(N'tempdb..#tmpNSD', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tmpNSD;
            END;
        IF OBJECT_ID(N'tempdb..#tmpSD', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tmpSD;
            END;
        RAISERROR ('Insert into tmpNSD' , 0, 1) WITH NOWAIT;
        SELECT  *
        INTO    #tmpNSD
        FROM    ( SELECT    d.SiteID ,
                            d.DepartmentID ,
                            MAX(nsd.CreatedDateUTC) AS CreatedDateUtc ,
                            d.IDAssetType ,
                            nsd.DeviceSerialNumber
                  FROM      --nonSessionDataCurrent  nsd with (readpast) right join assets d on nsd.DeviceSerialNumber = d.Serialno ReadPast Screwed Things UP
                            dbo.NonSessionDataCurrent nsd
                            RIGHT JOIN dbo.Assets d ON nsd.DeviceSerialNumber = d.SerialNo
                  WHERE     nsd.CreatedDateUTC > DATEADD(d, -7,
                                                         @CreatedDateUTC)
                            AND d.Retired = 0
                            AND ( d.IsActive = 1
                                  AND ( d.OutOfService IS NULL
                                        OR d.OutOfService = 0
                                      )
                                )
                  GROUP BY  d.SiteID ,
                            d.DepartmentID ,
                            d.IDAssetType ,
                            nsd.DeviceSerialNumber
                ) x;
        CREATE INDEX IDX_CreatedDateUtc ON #tmpNSD(CreatedDateUtc);
        CREATE INDEX IDX_SiteID ON #tmpNSD(SiteID);
        CREATE INDEX IDX_DepartmentID ON #tmpNSD(DepartmentID);


        RAISERROR ('Insert into tmpSD' , 0, 1) WITH NOWAIT;
        SELECT  *
        INTO    #tmpSD
        FROM    ( SELECT    d.SiteID ,
                            d.DepartmentID ,
                            MAX(sd.CreatedDateUTC) AS CreatedDateUtc ,
                            d.IDAssetType ,
                            sd.DeviceSerialNumber
                  FROM      --SessionDataCurrent sd with (readpast) right join assets d on sd.DeviceSerialNumber = d.Serialno ReadPast Screwed Things UP
                            dbo.SessionDataCurrent sd
                            RIGHT JOIN dbo.Assets d ON sd.DeviceSerialNumber = d.SerialNo
                  WHERE     sd.CreatedDateUTC > DATEADD(d, -7, @CreatedDateUTC)
                            AND d.Retired = 0
                            AND ( d.IsActive = 1
                                  AND ( d.OutOfService IS NULL
                                        OR d.OutOfService = 0
                                      )
                                )
                  GROUP BY  d.SiteID ,
                            d.DepartmentID ,
                            d.IDAssetType ,
                            sd.DeviceSerialNumber
                ) x;
        CREATE INDEX IDXsd_CreatedDateUtc ON #tmpSD(CreatedDateUtc);
        CREATE INDEX IDXsd_SiteID ON #tmpSD(SiteID);
        CREATE INDEX IDXsd_DepartmentID ON #tmpNSD(DepartmentID);


        RAISERROR ('Insert into SummaryActiveAssets' , 0, 1) WITH NOWAIT;
	--insert into SummaryActiveAssets (SiteID, DepartmentID, CreatedDateUTC)
	--	select distinct SiteID, DepartmentID, @CreatedDateUTC
	--		from #tmpSD sd 
	--	union
	--		select distinct SiteID, DepartmentID, @CreatedDateUTC
	--	from #tmpNSD  nsd
        INSERT  INTO dbo.SummaryActiveAssets
                ( SiteID ,
                  DepartmentID ,
                  CreatedDateUTC
                )
                SELECT DISTINCT
                        SiteID ,
                        DepartmentID ,
                        @CreatedDateUTC
                FROM    dbo.Assets
                WHERE   IDAssetType IN ( 1, 2, 3, 4, 5, 6 )
                        AND Retired = 0; --BUG, BOM had only 2,4 here (just chargers)

        RAISERROR ('Update SummaryActiveAssets set ActiveWorkStationCount' , 0, 1) WITH NOWAIT;
        UPDATE  dbo.SummaryActiveAssets
        SET     ActiveWorkstationCount = workstations.ActiveWorkstationCount
        FROM    ( SELECT    s.SiteID ,
                            s.DepartmentID ,
                            COUNT(DISTINCT s.DeviceSerialNumber) AS ActiveWorkstationCount
                  FROM      #tmpSD s
                  WHERE     s.IDAssetType IN ( 1, 3, 6 )
                  GROUP BY  s.SiteID ,
                            s.DepartmentID
                ) workstations
        WHERE   SummaryActiveAssets.SiteID = workstations.SiteID
                AND SummaryActiveAssets.DepartmentID = workstations.DepartmentID
                AND SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC;

	--RAISERROR ('Update SummaryActiveAssets set AvailableWorkStationCount' , 0, 1) WITH NOWAIT
	--update SummaryActiveAssets
	--set AvailableWorkstationCount = workstations.AvailableWorkstationCount
	--from (
	--	select SiteID, d.DepartmentID, count(distinct s.DeviceSerialNumber) as AvailableWorkstationCount
	--	from SessionDataCurrent s
	--	left join Assets d
	--	on s.DeviceSerialNumber = d.serialno
	--	where s.CreatedDateUTC < DATEADD(hh, -2, @CreatedDateUTC) and  s.CreatedDateUTC > DATEADD(d, -2, @CreatedDateUTC)
	--	and d.IDAssetType in (1,3,6)
	--	and d.OutOfService = 0
	--	group by d.SiteID, d.DepartmentID
	--	) workstations
	--where SummaryActiveAssets.SiteID = workstations.SiteID
	--and SummaryActiveAssets.DepartmentID = workstations.DepartmentID
	--and SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC

        RAISERROR ('Update SummaryActiveAssets set AvailableWorkStationCount' , 0, 1) WITH NOWAIT;
        UPDATE  dbo.SummaryActiveAssets
        SET     AvailableWorkstationCount = workstations.AvailableWorkstationCount
        FROM    ( SELECT    s.SiteID ,
                            ISNULL(s.DepartmentID, 0) AS DepartmentID ,
                            COUNT(DISTINCT s.DeviceSerialNumber) AS AvailableWorkstationCount
                  FROM      #tmpSD s
                  WHERE     s.CreatedDateUtc < DATEADD(hh, -2, @CreatedDateUTC)
                            AND s.CreatedDateUtc > DATEADD(d, -7,
                                                           @CreatedDateUTC)
                            AND s.IDAssetType IN ( 1, 3, 6 )
                  GROUP BY  s.SiteID ,
                            DepartmentID
                ) workstations
        WHERE   SummaryActiveAssets.SiteID = workstations.SiteID
                AND SummaryActiveAssets.DepartmentID = workstations.DepartmentID
                AND SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC;

	--RAISERROR ('Update SummaryActiveAssets set InServiceWorkStationCount' , 0, 1) WITH NOWAIT
	--update SummaryActiveAssets
	--set InServiceWorkstationCount = workstations.ActiveWorkstationCount
	--from (
	--	select d.SiteID, d.DepartmentID, count(distinct s.DeviceSerialNumber) as ActiveWorkstationCount
	--	from SessionDataCurrent s
	--	left join Assets d
	--	on s.DeviceSerialNumber = d.serialno
	--	where s.CreatedDateUTC > DATEADD(hh, -2, @CreatedDateUTC)
	--	and d.IDAssetType in (1,3,6)
	--	and d.OutOfService = 0
	--	group by d.SiteID, d.DepartmentID
	--	) workstations
	--where SummaryActiveAssets.SiteID = workstations.SiteID
	--and SummaryActiveAssets.DepartmentID = workstations.DepartmentID
	--and SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC

        RAISERROR ('Update SummaryActiveAssets set InServiceWorkStationCount' , 0, 1) WITH NOWAIT;
        UPDATE  dbo.SummaryActiveAssets
        SET     InServiceWorkstationCount = workstations.ActiveWorkstationCount
        FROM    ( SELECT    s.SiteID ,
                            ISNULL(s.DepartmentID, 0) AS DepartmentID ,
                            COUNT(DISTINCT s.DeviceSerialNumber) AS ActiveWorkstationCount
                  FROM      #tmpSD s
                  WHERE     s.CreatedDateUtc > DATEADD(hh, -2, @CreatedDateUTC)
                            AND s.IDAssetType IN ( 1, 3, 6 )
                  GROUP BY  s.SiteID ,
                            DepartmentID
                ) workstations
        WHERE   SummaryActiveAssets.SiteID = workstations.SiteID
                AND SummaryActiveAssets.DepartmentID = workstations.DepartmentID
                AND SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC;
	
	--COMMIT

        IF OBJECT_ID(N'tempdb..#tmpChargers', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tmpChargers;
            END;
        RAISERROR ('Insert into #tmpchargers from nonSessionDataCurrent ' , 0, 1) WITH NOWAIT;
        SELECT  d.SiteID ,
                ISNULL(d.DepartmentID, 0) AS DepartmentID ,
                ISNULL(COUNT(DISTINCT d.SerialNo), 0) AS ActiveChargerCount
        INTO    #tmpChargers
        FROM    dbo.NonSessionDataCurrent nsd
                LEFT JOIN dbo.Assets d ON nsd.DeviceSerialNumber = d.SerialNo
        WHERE   nsd.CreatedDateUTC > DATEADD(d, -7, @CreatedDateUTC)
                AND d.Retired = 0
                AND nsd.DeviceType IN ( 2, 4 )
	--and d.OutOfService = 0
        GROUP BY d.SiteID ,
                d.DepartmentID;

        CREATE NONCLUSTERED INDEX IX_tmpChargers ON #tmpChargers (SiteID, DepartmentID, ActiveChargerCount);
	
		
	--BEGIN TRANSACTION
	-- count chargers
        RAISERROR ('Update SummaryActiveAssets set ActiveChargerCount' , 0, 1) WITH NOWAIT;
        UPDATE  dbo.SummaryActiveAssets
        SET     ActiveChargerCount = ISNULL(chargers.ActiveChargerCount, 0)
        FROM    #tmpChargers chargers
        WHERE   SummaryActiveAssets.SiteID = chargers.SiteID
                AND SummaryActiveAssets.DepartmentID = chargers.DepartmentID
                AND CreatedDateUTC = @CreatedDateUTC;
	--COMMIT

	--BEGIN TRANSACTION
	-- count batteries
        IF OBJECT_ID(N'tempdb..##tmpBatteryNSD', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tmpBatteryNSD;
            END;
        RAISERROR ('Insert into tmpBatteryNSD' , 0, 1) WITH NOWAIT;
        SELECT  *
        INTO    #tmpBatteryNSD
        FROM    ( SELECT    d.SiteID ,
                            d.DepartmentID ,
                            MAX(nsd.CreatedDateUTC) AS CreatedDateUtc ,
                            d.IDAssetType ,
                            nsd.BatterySerialNumber
                  FROM      dbo.NonSessionDataCurrent nsd WITH ( READPAST )
                            RIGHT JOIN dbo.Assets d ON nsd.BatterySerialNumber = d.SerialNo
                  WHERE     nsd.CreatedDateUTC > DATEADD(d, -7,
                                                         @CreatedDateUTC)
                            AND d.Retired = 0
                            AND d.OutOfService = 0
                            AND d.IsActive = 1
                  GROUP BY  d.SiteID ,
                            d.DepartmentID ,
                            d.IDAssetType ,
                            nsd.BatterySerialNumber
                ) x;
        CREATE INDEX IDXb_CreatedDateUtc ON #tmpBatteryNSD(CreatedDateUtc);
        CREATE INDEX IDXb_SiteID ON #tmpBatteryNSD(SiteID);
        CREATE INDEX IDXb_DepartmentID ON #tmpBatteryNSD(DepartmentID);
        IF OBJECT_ID(N'tempdb..##tmpBatterySD', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tmpBatterySD;
            END;

        RAISERROR ('Insert into tmpBatterySD' , 0, 1) WITH NOWAIT;
        SELECT  *
        INTO    #tmpBatterySD
        FROM    ( SELECT    d.SiteID ,
                            d.DepartmentID ,
                            MAX(sd.CreatedDateUTC) AS CreatedDateUtc ,
                            d.IDAssetType ,
                            sd.BatterySerialNumber
                  FROM      dbo.SessionDataCurrent sd WITH ( READPAST )
                            RIGHT JOIN dbo.Assets d ON sd.BatterySerialNumber = d.SerialNo
                  WHERE     sd.CreatedDateUTC > DATEADD(d, -7, @CreatedDateUTC)
                            AND d.Retired = 0
                            AND d.OutOfService = 0
                            AND d.IsActive = 1
                  GROUP BY  d.SiteID ,
                            d.DepartmentID ,
                            d.IDAssetType ,
                            sd.BatterySerialNumber
                ) x;
        CREATE INDEX IDXbs_CreatedDateUtc ON #tmpBatterySD(CreatedDateUtc);
        CREATE INDEX IDXbs_SiteID ON #tmpBatterySD(SiteID);
        CREATE INDEX IDXbs_DepartmentID ON #tmpBatterySD(DepartmentID);


        RAISERROR ('Update SummaryActiveAssets set ActiveBatteryCount' , 0, 1) WITH NOWAIT;
        UPDATE  dbo.SummaryActiveAssets
        SET     ActiveBatteryCount = batteries.ActiveBatteryCount
        FROM    ( SELECT    asdf.SiteID ,
                            asdf.DepartmentID ,
                            COUNT(DISTINCT asdf.BatterySerialNumber) AS ActiveBatteryCount
                  FROM      ( SELECT DISTINCT
                                        SiteID ,
                                        DepartmentID ,
                                        BatterySerialNumber
                              FROM      #tmpBatteryNSD
                              WHERE     ISNULL(BatterySerialNumber, '') <> ''
                              UNION
                              SELECT DISTINCT
                                        SiteID ,
                                        DepartmentID ,
                                        BatterySerialNumber
                              FROM      #tmpBatterySD
                              WHERE     ISNULL(BatterySerialNumber, '') <> ''
                            ) asdf
                  GROUP BY  asdf.SiteID ,
                            asdf.DepartmentID
                ) batteries
        WHERE   SummaryActiveAssets.SiteID = batteries.SiteID
                AND SummaryActiveAssets.DepartmentID = batteries.DepartmentID
                AND SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC;


	--update SummaryActiveAssets
	--set ActiveBatteryCount = batteries.ActiveBatteryCount
	--from (
	--	select SiteID, DepartmentID, count(distinct BatterySerialNumber) as ActiveBatteryCount
	--	from 
		
	--	(
	--		select distinct d.SiteID, d.DepartmentID, BatterySerialNumber
	--		from nonSessionDataCurrent  nsd
	--		left join Assets d
	--		on nsd.BatterySerialNumber = d.SerialNo
	--		where nsd.CreatedDateUTC > DATEADD(d, -7, @CreatedDateUTC) 
	--		and IsNull(BatterySerialNumber, '') <> ''
	--		and d.OutOfService = 0
	--		union
	--		select distinct d.SiteID, d.DepartmentID, BatterySerialNumber
	--		from SessionDataCurrent sd
	--		left join Assets d
	--		on sd.BatterySerialNumber = d.SerialNo
	--		where sd.CreatedDateUTC > DATEADD(d, -7, @CreatedDateUTC) 
	--		and IsNull(BatterySerialNumber, '') <> ''
	--		and d.OutOfService = 0
	--	) asdf
		
	--	group by SiteID, DepartmentID
	--) batteries
	--where SummaryActiveAssets.SiteID = batteries.SiteID
	--and SummaryActiveAssets.DepartmentID = batteries.DepartmentID
	--and SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC
	--COMMIT
        RAISERROR ('Update SummaryActiveAssets set DormantCount' , 0, 1) WITH NOWAIT;
        UPDATE  dbo.SummaryActiveAssets
        SET     DormantBatteryCount = batteries.DormantBatteryCount
        FROM    ( SELECT    asdf.SiteID ,
                            asdf.DepartmentID ,
                            COUNT(DISTINCT asdf.BatterySerialNumber) AS DormantBatteryCount
                  FROM      ( SELECT DISTINCT
                                        nsd.SiteID ,
                                        nsd.DepartmentID ,
                                        nsd.BatterySerialNumber
                              FROM      #tmpBatteryNSD nsd
                              WHERE     nsd.CreatedDateUtc > DATEADD(d, -7,
                                                              @CreatedDateUTC)
                                        AND nsd.CreatedDateUtc < DATEADD(d, -2,
                                                              @CreatedDateUTC)
                                        AND ISNULL(nsd.BatterySerialNumber, '') <> ''
                              UNION
                              SELECT DISTINCT
                                        sd.SiteID ,
                                        sd.DepartmentID ,
                                        sd.BatterySerialNumber
                              FROM      #tmpBatterySD sd
                              WHERE     sd.CreatedDateUtc > DATEADD(d, -7,
                                                              @CreatedDateUTC)
                                        AND sd.CreatedDateUtc < DATEADD(d, -2,
                                                              @CreatedDateUTC)
                                        AND ISNULL(sd.BatterySerialNumber, '') <> ''
                            ) asdf
                  GROUP BY  asdf.SiteID ,
                            asdf.DepartmentID
                ) batteries
        WHERE   SummaryActiveAssets.SiteID = batteries.SiteID
                AND SummaryActiveAssets.DepartmentID = batteries.DepartmentID
                AND SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC;
	
        RAISERROR ('Update SummaryActiveAssets set DormantWorkStationCount' , 0, 1) WITH NOWAIT;
        UPDATE  dbo.SummaryActiveAssets
        SET     DormantWorkstationCount = ws.DormantWorkStationCount
        FROM    ( SELECT    asdf.SiteID ,
                            asdf.DepartmentID ,
                            COUNT(DISTINCT asdf.DeviceSerialNumber) AS DormantWorkStationCount
                  FROM      ( SELECT DISTINCT
                                        nsd.SiteID ,
                                        nsd.DepartmentID ,
                                        nsd.DeviceSerialNumber
                              FROM      #tmpNSD nsd
                              WHERE     nsd.CreatedDateUtc > DATEADD(d, -7,
                                                              @CreatedDateUTC)
                                        AND nsd.CreatedDateUtc < DATEADD(d, -2,
                                                              @CreatedDateUTC)
                                        AND ISNULL(nsd.DeviceSerialNumber, '') <> ''
                              UNION
                              SELECT DISTINCT
                                        sd.SiteID ,
                                        sd.DepartmentID ,
                                        sd.DeviceSerialNumber
                              FROM      #tmpSD sd
                              WHERE     sd.CreatedDateUtc > DATEADD(d, -7,
                                                              @CreatedDateUTC)
                                        AND sd.CreatedDateUtc < DATEADD(d, -2,
                                                              @CreatedDateUTC)
                                        AND ISNULL(sd.DeviceSerialNumber, '') <> ''
                            ) asdf
                  GROUP BY  asdf.SiteID ,
                            asdf.DepartmentID
                ) ws
        WHERE   SummaryActiveAssets.SiteID = ws.SiteID
                AND SummaryActiveAssets.DepartmentID = ws.DepartmentID
                AND SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC;

        RAISERROR ('Update SummaryActiveAssets set BayCount' , 0, 1) WITH NOWAIT;
	--update SummaryActiveAssets
	--set BayCount = chg.TotalBays
	--from (
	-- select sum(Bays) as TotalBays, max(chargers.siteid) as siteid, max(chargers.departmentid) as departmentid from 
	--   (select case when bay4chargerrevision is null then  2 else 4 end as Bays, *
	--	  from assets a where IDAssetType=2 and AssetStatusID=2 ) chargers group by siteid,DepartmentID ) chg
 --where SummaryActiveAssets.SiteID = chg.SiteID
	--and SummaryActiveAssets.DepartmentID = chg.DepartmentID
	--and SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC

        UPDATE  dbo.SummaryActiveAssets
        SET     BayCount = chg.bays
        FROM    ( SELECT    SUM(BayCount) AS bays ,
                            SiteID ,
                            ISNULL(DepartmentID, 0) AS DepartmentID
                  FROM      dbo.Assets
                  WHERE     IDAssetType IN ( 2, 4 )
                  GROUP BY  SiteID ,
                            DepartmentID
                ) chg
        WHERE   SummaryActiveAssets.SiteID = chg.SiteID
                AND SummaryActiveAssets.DepartmentID = chg.DepartmentID
                AND SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC;



--	update SummaryActiveAssets
--	set ActiveChargerBays = chg.HasBattery
--	from (
--	select ml.Bay,
--sum(case when oat.BatterySerialNumber = 'No Batt SN' then 0 else 1 end) as HasBattery, 
--ml.SiteID, ml.DepartmentID 

--from (select Distinct DeviceSerialNumber,Bay, siteid, departmentid from nonSessionDataCurrent  with (readpast) left join assets a on DeviceSerialNumber = a.SerialNo where a.SerialNo in 
--(select SerialNo from Assets where idassettype=2 and AssetStatusID=2))  ml 
--outer apply (select top 1 * from nonSessionDataCurrent  nsd with (readpast) where ml.deviceserialnumber = nsd.deviceserialnumber and ml.bay = nsd.Bay 
--order by CreatedDateUTC desc) oat  where ml.bay is not null  group by siteid, departmentid, ml.bay 
--having sum(case when oat.BatterySerialNumber = 'No Batt SN' then 0 else 1 end)   > 0
-- ) as chg
-- where SummaryActiveAssets.SiteID = chg.SiteID
--	and SummaryActiveAssets.DepartmentID = chg.DepartmentID
--	and SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC

        RAISERROR ('Load temp table for ChargerBays' , 0, 1) WITH NOWAIT;
        IF OBJECT_ID(N'tempdb..#myTemp', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #mytemp;
            END;

        SELECT  *
        INTO    #mytemp
        FROM    ( SELECT    nd.DeviceSerialNumber ,
                            nd.Bay ,
                            nd.BatterySerialNumber ,
                            a.SiteID ,
                            a.DepartmentID ,
                            MAX(nd.CreatedDateUTC) AS createddateutc
                  FROM      dbo.NonSessionDataCurrent nd
                            LEFT JOIN dbo.Assets a ON nd.DeviceSerialNumber = a.SerialNo
                                                  AND a.IDAssetType IN ( 2, 4 )
                                                  AND a.AssetStatusID = 2
                                                  AND a.Retired = 0
                  WHERE     nd.CreatedDateUTC > @CreatedDateUTC
                            AND nd.Bay IS NOT NULL
                            AND a.SiteID IS NOT NULL
                  GROUP BY  a.SiteID ,
                            a.DepartmentID ,
                            nd.DeviceSerialNumber ,
                            nd.Bay ,
                            nd.BatterySerialNumber
                  HAVING    SUM(CASE WHEN nd.BatterySerialNumber = 'No Batt SN'
                                          OR nd.BatterySerialNumber = ''
                                          OR nd.BatterySerialNumber IS NULL
                                     THEN 0
                                     ELSE 1
                                END) > 0
                ) x;

--	RAISERROR ('Update SummaryActiveAssets set ActiveChargerbays' , 0, 1) WITH NOWAIT
--		update SummaryActiveAssets
--	set ActiveChargerBays = chg.HasBattery
--	from (
--	select Bay,
--sum(case when BatterySerialNumber = 'No Batt SN' or BatterySerialNumber = 'None' then 0 else 1 end) as HasBattery, 
--SiteID,DepartmentID from #myTemp group by siteid, departmentid, bay) as chg
-- where SummaryActiveAssets.SiteID = chg.SiteID
--	and SummaryActiveAssets.DepartmentID = chg.DepartmentID
--	and SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC

--	RAISERROR ('Update SummaryActiveAssets set VacantChargerBays' , 0, 1) WITH NOWAIT
--		update SummaryActiveAssets
--	set VacantChargerBays = chg.HasBattery
--	from (
--	select Bay,
--sum(case when BatterySerialNumber = 'No Batt SN' or BatterySerialNumber = 'None' then 1 else 0 end) as HasBattery, 
--SiteID,DepartmentID from #myTemp group by siteid, departmentid, bay) as chg
-- where SummaryActiveAssets.SiteID = chg.SiteID
--	and SummaryActiveAssets.DepartmentID = chg.DepartmentID
--	and SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC


-- update SummaryActiveAssets
--	set VacantChargerBays = chg.HasBattery
--	from (
--	select ml.Bay,
--sum(case when oat.BatterySerialNumber = 'No Batt SN' then 1 else 0 end) as HasBattery, 
--ml.SiteID, ml.DepartmentID 

--from (select Distinct DeviceSerialNumber,Bay, siteid, departmentid from nonSessionDataCurrent  with (readpast) left join assets a on DeviceSerialNumber = a.SerialNo where a.SerialNo in 
--(select SerialNo from Assets where idassettype=2 and AssetStatusID=2))  ml 
--outer apply (select top 1 * from nonSessionDataCurrent  nsd with (readpast) where ml.deviceserialnumber = nsd.deviceserialnumber and ml.bay = nsd.Bay 
--order by CreatedDateUTC desc) oat  where ml.bay is not null  group by siteid, departmentid, ml.bay 
--having sum(case when oat.BatterySerialNumber = 'No Batt SN' then 0 else 1 end)   < 1
-- ) as chg
-- where SummaryActiveAssets.SiteID = chg.SiteID
--	and SummaryActiveAssets.DepartmentID = chg.DepartmentID
--	and SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC

        IF OBJECT_ID(N'tempdb..#tmpBnW', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tmpBnW;
            END;

        RAISERROR ('BatteriesInWorkStationCount - load tmpBnW SessionDataCurrent' , 0, 1) WITH NOWAIT;

        SELECT  *
        INTO    #tmpBnW
        FROM    ( SELECT    t1.BatterySerialNumber ,
                            t1.Bay ,
                            t1.CreatedDateUTC ,
                            t1.DeviceSerialNumber ,
                            t1.ChargeLevel ,
                            d.IDAssetType ,
                            d.SiteID ,
                            d.DepartmentID
                  FROM      dbo.SessionDataCurrent t1
                            JOIN dbo.Assets d ON d.SerialNo = t1.DeviceSerialNumber
                            JOIN ( SELECT   BatterySerialNumber ,
                                            MAX(CreatedDateUTC) maxdate
                                   FROM     dbo.SessionDataCurrent
                                   GROUP BY BatterySerialNumber
                                 ) t2 ON t1.BatterySerialNumber = t2.BatterySerialNumber
                                         AND t1.CreatedDateUTC = t2.maxdate
                  WHERE     t1.CreatedDateUTC > DATEADD(d, -2, @CreatedDateUTC)
                            AND d.Retired = 0
                            AND ISNULL(t1.BatterySerialNumber, '') <> ''
                ) x;

        RAISERROR ('BatteriesInWorkStationCount - load tmpBnW nonSessionDataCurrent ' , 0, 1) WITH NOWAIT;

        INSERT  INTO #tmpBnW
                ( BatterySerialNumber ,
                  Bay ,
                  CreatedDateUTC ,
                  DeviceSerialNumber ,
                  ChargeLevel ,
                  IDAssetType ,
                  SiteID ,
                  DepartmentID
                )
                SELECT  t1.BatterySerialNumber ,
                        t1.Bay ,
                        t1.CreatedDateUTC ,
                        t1.DeviceSerialNumber ,
                        t1.ChargeLevel ,
                        d.IDAssetType ,
                        d.SiteID ,
                        d.DepartmentID
                FROM    dbo.NonSessionDataCurrent t1
                        JOIN dbo.Assets d ON d.SerialNo = t1.DeviceSerialNumber
                        JOIN ( SELECT   BatterySerialNumber ,
                                        MAX(CreatedDateUTC) maxdate
                               FROM     dbo.NonSessionDataCurrent
                               GROUP BY BatterySerialNumber
                             ) t2 ON t1.BatterySerialNumber = t2.BatterySerialNumber
                                     AND t1.CreatedDateUTC = t2.maxdate
                WHERE   t1.CreatedDateUTC > DATEADD(d, -2, @CreatedDateUTC)
                        AND d.Retired = 0
                        AND ISNULL(t1.BatterySerialNumber, '') <> '';
        CREATE INDEX IDXbnw_CreatedDateUtc ON #tmpBnW(CreatedDateUTC);
        CREATE INDEX IDxbnw_BATTERYSerialNumber ON #tmpBnW(BatterySerialNumber);

        RAISERROR ('BatteriesInWorkStationCount - remove all but last entry' , 0, 1) WITH NOWAIT;
        WITH    T AS ( SELECT   ROW_NUMBER() OVER ( PARTITION BY BatterySerialNumber ORDER BY CreatedDateUTC DESC ) AS rnum ,
                                *
                       FROM     #tmpBnW
                     )
            DELETE  FROM T
            WHERE   T.rnum > 1;
        WITH    TW
                  AS ( SELECT   ROW_NUMBER() OVER ( PARTITION BY DeviceSerialNumber ORDER BY CreatedDateUTC DESC ) AS rnum ,
                                *
                       FROM     #tmpBnW
                     )
            DELETE  FROM TW
            WHERE   TW.rnum > 1;

        IF OBJECT_ID(N'tempdb..#tmpAssets', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tmpAssets;
            END;	
        WITH    num
                  AS ( SELECT   number AS number
                       FROM     master..spt_values
                       WHERE    type = 'P'
                     )
            SELECT  d.SerialNo ,
                    d.BayCount ,
                    n.number + 1 AS baynum ,
                    d.SiteID ,
                    d.DepartmentID ,
                    d.IDAssetType
            INTO    #tmpAssets
            FROM    dbo.Assets d
                    INNER JOIN num n ON n.number < d.BayCount
            WHERE   d.IDAssetType IN ( 2, 4 )
                    AND d.Retired = 0; ---Added By arlow to make sure 4's were included

        RAISERROR ('Update Active Charger bays' , 0, 1) WITH NOWAIT;
        UPDATE  dbo.SummaryActiveAssets
        SET     ActiveChargerBays = chg.ActiveChargerbays
        FROM    ( SELECT    a.SiteID ,
                            a.DepartmentID ,
                            SUM(a.BayCount) AS ActiveChargerbays
                  FROM      dbo.Assets a
                  WHERE     a.IDAssetType IN ( 2, 4 )
                            AND a.Retired = 0
                            AND a.LastPostDateUTC > DATEADD(HOUR, -2, GETDATE())
                  GROUP BY  a.SiteID ,
                            a.DepartmentID
                ) chg
        WHERE   SummaryActiveAssets.SiteID = chg.SiteID
                AND SummaryActiveAssets.DepartmentID = chg.DepartmentID
                AND SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC;

        RAISERROR ('Update Vacant Charger bays' , 0, 1) WITH NOWAIT;
        UPDATE  dbo.SummaryActiveAssets
        SET     VacantChargerBays = SummaryActiveAssets.BayCount
                - ( ActiveChargerBays ) --+ chg.OfflineChargerbays )
        FROM    ( SELECT    a.SiteID ,
                            a.DepartmentID ,
                            SUM(a.BayCount) AS OfflineChargerbays
                  FROM      dbo.Assets a
                  WHERE     a.IDAssetType IN ( 2, 4 )
                            AND a.Retired = 0
                            AND a.LastPostDateUTC < DATEADD(DAY, -1, GETDATE())
                  GROUP BY  a.SiteID ,
                            a.DepartmentID
                ) chg
        WHERE   SummaryActiveAssets.SiteID = chg.SiteID
                AND SummaryActiveAssets.DepartmentID = chg.DepartmentID
                AND SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC;

        RAISERROR ('Update Offline Charger bays' , 0, 1) WITH NOWAIT;
        UPDATE  dbo.SummaryActiveAssets
        SET     OfflineChargerBays = chg.OfflineChargerbays
        FROM    ( SELECT    a.SiteID ,
                            a.DepartmentID ,
                            SUM(a.BayCount) AS OfflineChargerbays
                  FROM      dbo.Assets a
                  WHERE     a.IDAssetType IN ( 2, 4 )
                            AND a.Retired = 0
                            AND a.LastPostDateUTC < DATEADD(DAY, -1, GETDATE())
                  GROUP BY  a.SiteID ,
                            a.DepartmentID
                ) chg
        WHERE   SummaryActiveAssets.SiteID = chg.SiteID
                AND SummaryActiveAssets.DepartmentID = chg.DepartmentID
                AND SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC;



        RAISERROR ('Update SummaryActiveAssets set BatteriesInWorkStationCount' , 0, 1) WITH NOWAIT;

        UPDATE  dbo.SummaryActiveAssets
        SET     BatteryInWorkstationCount = x.BatteriesInWorkStationCount ,
                BatteryInChargerCount = x.BatteriesInChargerCount ,
                CountFullBatteries = x.CountFullBatteries
        FROM    ( SELECT    y.SiteID ,
                            y.DepartmentID ,
                            SUM(y.BatteryInWorkStationCount) AS BatteriesInWorkStationCount ,
                            SUM(y.BatteryInChargerCount) BatteriesInChargerCount ,
                            SUM(y.CountFullBatteries) CountFullBatteries
                  FROM      ( SELECT    SiteID ,
                                        DepartmentID ,
                                        BatterySerialNumber ,
                                        IDAssetType ,
                                        ChargeLevel ,
                                        CreatedDateUTC ,
                                        CASE WHEN IDAssetType = 1
                                                  OR IDAssetType = 3
                                                  OR IDAssetType = 6 THEN 1
                                             ELSE 0
                                        END AS BatteryInWorkStationCount ,
                                        CASE WHEN IDAssetType = 2
                                                  OR IDAssetType = 4 THEN 1
                                             ELSE 0
                                        END AS BatteryInChargerCount ,
                                        CASE WHEN ChargeLevel > 80 THEN 1
                                             ELSE 0
                                        END AS CountFullBatteries
                              FROM      #tmpBnW
                            ) y
                  GROUP BY  y.SiteID ,
                            y.DepartmentID
                ) x
        WHERE   SummaryActiveAssets.SiteID = x.SiteID
                AND SummaryActiveAssets.DepartmentID = x.DepartmentID
                AND SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC;

        DROP TABLE #tmpBnW;
--begin

--exec spAssets_UpdateAssetStatusID
--select count(*)  from Assets where IDAssetType=5 and AssetStatusID=2


--		declare @BatterySerialNumber varchar(50)
--	    declare @type as int = 0
--	        declare @SiteID as int = 0
--		       declare @DepartmentID as int = 0
--	declare @BatteryInWorkStationCount int = 0
--	declare @BatteryInChargerCount int = 0
--	declare @CountFullBatteries int = 0
--	declare @DormantBatteryCount int =0
--	declare @ChargeLevel int = 0
--	declare @LastSiteID int = 0
--	declare @LastDepartmentID int = 0

	
--	DECLARE db_cursor CURSOR FOR 	select batteries.BSN,batteries.SiteID,batteries.DepartmentID
--	from   (
--		select SiteID, DepartmentID, max(BatterySerialNumber) as BSN
--		from 
		
--		(
--			select  distinct d.SiteID, d.DepartmentID, nsd.BatterySerialNumber
--			from nonSessionDataCurrent  nsd
--			left join Assets d
--			--on nsd.DeviceSerialNumber = d.SerialNumber
--			on nsd.BatterySerialNumber = d.SerialNo
--			where nsd.CreatedDateUTC > DATEADD(d, -2, @CreatedDateUTC) 
--			and IsNull(BatterySerialNumber, '') <> ''
--			and d.OutOfService = 0
--			-- order by nsd.CreatedDateUTC desc
--			union
--			select distinct  d.SiteID, d.DepartmentID, sd.BatterySerialNumber
--			from SessionDataCurrent sd
--			left join Assets d
--			--on sd.DeviceSerialNumber = d.SerialNumber
--			on sd.BatterySerialNumber = d.SerialNo
--			where sd.CreatedDateUTC > DATEADD(d, -2, @CreatedDateUTC) 
--			and IsNull(BatterySerialNumber, '') <> ''
--			and d.OutOfService = 0
--			--order by sd.CreatedDateUTC desc
--		) asdf
		
--		group by SiteID, DepartmentID,batteryserialNumber
--	) batteries  join SummaryActiveAssets  on
--	 SummaryActiveAssets.SiteID = batteries.SiteID
--	and SummaryActiveAssets.DepartmentID = batteries.DepartmentID
--	and SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC




--OPEN db_cursor   
--FETCH NEXT FROM db_cursor INTO @BatterySerialNumber,@SiteID,@DepartmentID  
--       set @LastSiteID = @SiteID
--    set @LastDepartmentID = @DepartmentID
--WHILE @@FETCH_STATUS = 0   
--Begin 
--    if (@LastSiteID != @SiteID or @LastDepartmentID != @DepartmentID)
--    begin
--    print 'Updated ' + cast(@LastSiteID as varchar(50)) + 'With ' + @BatterySerialNumber
--update SummaryActiveAssets set BatteryInWorkstationCount = @BatteryInWorkstationCount, 
--BatteryInChargerCount = @BatteryInChargerCount, 
--CountFullBatteries = @CountFullBatteries 

--where DepartmentID = @LastDepartmentID and SiteID= @LastSiteID and CreatedDateUTC = @CreatedDateUTC
--    set @BatteryInWorkstationCount = 0
--    set @BatteryInChargerCount = 0
--   set  @CountFullBatteries = 0
--       set @LastSiteID = @SiteID
--    set @LastDepartmentID = @DepartmentID
--    end

--    set @type  = (select devicetype from MostRecentBatteryPacket (@BatterySerialNumber) )
--    set @ChargeLevel = (select chargelevel from MostRecentBatteryPacket (@BatterySerialNumber))

--    Begin Transaction
--if (@type = 1)
--begin
--    	set @BatteryInWorkStationCount  = @BatteryInWorkstationCount + 1
--end
--if (@type = 2)
--begin
--	set @BatteryInChargerCount  = @BatteryInChargerCount + 1
--end
--if(@ChargeLevel > 94)
--begin
--set @CountFullBatteries = @CountFullBatteries + 1
--end

--commit
--       FETCH NEXT FROM db_cursor INTO @BatterySerialNumber,@SiteID,@DepartmentID
--END   

--CLOSE db_cursor   
--DEALLOCATE db_cursor
--end




	--	update SummaryActiveAssets
	--set BatteryInWorkstationCount = batteries.BatteryInWorkstationCount
	--from (
	--	select SiteID, DepartmentID, count(distinct BatterySerialNumber) as BatteryInWorkstationCount
	--	from 		
	--	(
	--		select distinct d.SiteID, d.DepartmentID, BatterySerialNumber
	--		from MostRecentBatteryPacket ('311901315080621')
	--		where DeviceType in (1,3,6)
	--	) asdf
		
	--	group by SiteID, DepartmentID
	--) batteries
	--where SummaryActiveAssets.SiteID = batteries.SiteID
	--and SummaryActiveAssets.DepartmentID = batteries.DepartmentID
	--and SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC

	--	update SummaryActiveAssets
	--set BatteryinChargerCount = batteries.BatteryinChargerCount
	--from (
	--	select SiteID, DepartmentID, count(distinct BatterySerialNumber) as BatteryinChargerCount
	--	from 		
	--	(
	--		select distinct d.SiteID, d.DepartmentID, BatterySerialNumber
	--		from nonSessionDataCurrent  nsd
	--		left join Assets d
	--		--on nsd.DeviceSerialNumber = d.SerialNumber
	--		on nsd.BatterySerialNumber = d.SerialNo
	--		where nsd.CreatedDateUTC > DATEADD(d, -2, @CreatedDateUTC)
	--		and IsNull(BatterySerialNumber, '') <> ''
	--		and d.OutOfService = 0
	--		and nsd.DeviceType in (2,4)
	--		union
	--		select distinct d.SiteID, d.DepartmentID, BatterySerialNumber
	--		from SessionDataCurrent sd
	--		left join Assets d
	--		--on sd.DeviceSerialNumber = d.SerialNumber
	--		on sd.BatterySerialNumber = d.SerialNo
	--		where sd.CreatedDateUTC > DATEADD(d, -2, @CreatedDateUTC) 
	--		and IsNull(BatterySerialNumber, '') <> ''
	--		and d.OutOfService = 0
	--		and sd.DeviceType in (2,4)
	--	) asdf
		
	--	group by SiteID, DepartmentID
	--) batteries
	--where SummaryActiveAssets.SiteID = batteries.SiteID
	--and SummaryActiveAssets.DepartmentID = batteries.DepartmentID
	--and SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC

	--update SummaryActiveAssets
	--set CountFullBatteries = batteries.CountFullBatteries
	--from (
	--	select SiteID, DepartmentID, count(distinct BatterySerialNumber) as CountFullBatteries
	--	from 		
	--	(
	--		select top 1  d.SiteID, d.DepartmentID, BatterySerialNumber,nsd.chargelevel
	--		from nonSessionDataCurrent  nsd
	--		left join Assets d
	--		--on nsd.DeviceSerialNumber = d.SerialNumber
	--		on nsd.BatterySerialNumber = d.SerialNo
	--		where nsd.CreatedDateUTC > DATEADD(d, -2, @CreatedDateUTC) and nsd.chargelevel > 94
	--		and IsNull(BatterySerialNumber, '') <> ''
	--		and d.OutOfService = 0
	--		and nsd.DeviceType in (2,4) order by nsd.CreatedDateUTC desc
	--		union
	--		select top 1  d.SiteID, d.DepartmentID, BatterySerialNumber,sd.chargelevel
	--		from SessionDataCurrent sd
	--		left join Assets d
	--		--on sd.DeviceSerialNumber = d.SerialNumber
	--		on sd.BatterySerialNumber = d.SerialNo
	--		where sd.CreatedDateUTC > DATEADD(d, -2, @CreatedDateUTC) and sd.chargelevel> 94
	--		and IsNull(BatterySerialNumber, '') <> ''
	--		and d.OutOfService = 0
	--		and sd.DeviceType in (2,4) order by sd.CreatedDateUTC desc
	--	) asdf
		
	--	group by SiteID, DepartmentID
	--) batteries
	--where SummaryActiveAssets.SiteID = batteries.SiteID
	--and SummaryActiveAssets.DepartmentID = batteries.DepartmentID
	--and SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC


        IF OBJECT_ID(N'tempdb..#tmpChargers', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tmpChargers;
            END;
        IF OBJECT_ID(N'tempdb..##tmpNSD', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tmpNSD;
            END;
        IF OBJECT_ID(N'tempdb..##tmpSD', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tmpSD;
            END;

        IF OBJECT_ID(N'tempdb..##tmpBatteryNSD', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tmpBatteryNSD;
            END;
        IF OBJECT_ID(N'tempdb..##tmpBatterySD', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #tmpBatterySD;
            END;



    END;


GO
