SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[prcDashSF_test] @SiteId INT = 657
AS
     BEGIN
         CREATE TABLE #sfCases
         (idUser       INT,
          userName     VARCHAR(50),
          Title        VARCHAR(50),
          ProfileImage VARBINARY(MAX),
          CaseNumber   VARCHAR(8),
          CaseDesc     VARCHAR(50),
          CaseStatus   VARCHAR(10),
          LastUpdated  DATETIME
         );
         DECLARE @pic VARBINARY(MAX);
         DECLARE @title VARCHAR(50);
         DECLARE @username VARCHAR(50);
         SELECT @pic = profileimage,
                @title = Title,
                @userName = FirstName+' '+LastName
         FROM [user]
         WHERE Iduser = 1280;
         INSERT INTO #sfCases
         (idUser,
          UserName,
          Title,
          ProfileImage,
          CaseNumber,
          CaseDesc,
          CaseStatus,
          LastUpdated
         )
         VALUES
         (1280, -- idUser - int
          @username,
          @title, -- Title - varchar(50)
          @pic, -- ProfileImage - varbinary(max)
          '123456', -- CaseNumber - varchar(8)
          'Main battery at less than 60% capacity', -- CaseDesc - varchar(50)
          'Open', -- CaseStatus - varchar(10)
          GETDATE()
         );
         SELECT @pic = profileimage,
                @title = Title,
                @userName = FirstName+' '+LastName
         FROM [user]
         WHERE Iduser = 223;
         INSERT INTO #sfCases
         (idUser,
          userName,
          Title,
          ProfileImage,
          CaseNumber,
          CaseDesc,
          CaseStatus,
          LastUpdated
         )
         VALUES
         (1280, -- idUser - int
          @username,
          @title, -- Title - varchar(50)
          @pic, -- ProfileImage - varbinary(max)
          '556677', -- CaseNumber - varchar(8)
          'Charger not reporting', -- CaseDesc - varchar(50)
          'Open', -- CaseStatus - varchar(10)
          GETDATE()
         );
         SELECT @SiteId AS SiteId,
                idUser,
			 UserName,
                Title,
                ProfileImage,
			 CaseNumber,
                CaseDesc,
                CaseStatus,
                CONVERT(VARCHAR(24),LastUpdated,100) AS LastUpdated
         FROM #sfCases;
         DROP TABLE #sfCases;
     END;
GO
