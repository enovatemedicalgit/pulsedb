SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE Procedure [dbo].[prcLostAssetComm]
    @SiteID VARCHAR(20) ,
    @AssetSerialNumber VARCHAR(20) = NULL
AS
    BEGIN
--if @AssetSerialNumber is null
        BEGIN
            SELECT  *
            FROM    dbo.viewLostAssetCommSimple
            WHERE   IDAssetType != 5
                    AND IDSite = @SiteID
                    AND Retired <> 1
                    AND ( (LastPostDateUTC < DATEADD(HOUR, -24, GETDATE())
                          OR ( IDAssetType IN ( 1, 2, 6 )
                               AND LastPostDateUTC < DATEADD(DAY, -6,
                                                             GETDATE())
                             ))
                        )
            ORDER BY LastPostDateUTC DESC;
        END; 
--Else
--BEGIN
--select * from dbo.viewLostAssetComm where AssetStatusID <> 3 and IDSite=@SiteID and DeviceSerialNumber = @AssetSerialNumber
--END
    END;



GO
