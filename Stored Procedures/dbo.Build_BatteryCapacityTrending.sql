SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[Build_BatteryCapacityTrending]
AS
WITH BatteryCapacityByCycleCount
AS (
	SELECT BatterySerialNumber
		,(CycleCount / 20) * 20 AS CycleCount
		,AVG(FullChargeCapacity) AS FullChargeCapacity
		,COUNT(*) AS DataPoints
	FROM PulseHistorical.dbo.SessionData
	WHERE ChargeLevel >= 95
		--AND BatterySerialNumber IN (
		--	SELECT TOP 100 BatterySerialNumber
		--	FROM PulseHistorical.dbo.SessionData
		--	)
		AND LEFT(BatterySerialNumber, 7) IN (
			SELECT PartNumber
			FROM BatteryPartList
			)
	GROUP BY BatterySerialNumber
		,(CycleCount / 20) * 20
		--ORDER BY
		--      BatterySerialNumber 
		--    , CycleCount
	)
	,TrendWithRanking
AS (
	SELECT ROW_NUMBER() OVER (
			PARTITION BY BatterySerialNumber ORDER BY CycleCount DESC
			) AS Ordinal
		,*
	FROM BatteryCapacityByCycleCount
	)
	,MostRecent
AS (
	SELECT BatterySerialNumber
		,FullChargeCapacity AS fcc1
	FROM TrendWithRanking
	WHERE Ordinal = 1
	)
	,NextMostRecent
AS (
	SELECT BatterySerialNumber
		,FullChargeCapacity AS fcc2
	FROM TrendWithRanking
	WHERE Ordinal = 2
	)
SELECT uq.BatterySerialNumber
	,CONVERT(FLOAT, uq.CapacityLossOver20Cycles) / 20 AS LossPerCycle
	,CONVERT(FLOAT, a.CycleCount) / CONVERT(FLOAT, DATEDIFF(DAY, a.CreatedDateUTC, GETUTCDATE())) CyclesPerDay
INTO Pulse.dbo.BatteryCapacityTrending
FROM (
	SELECT MostRecent.BatterySerialNumber
		,fcc2 - fcc1 AS CapacityLossOver20Cycles
	FROM MostRecent
	INNER JOIN NextMostRecent
		ON NextMostRecent.BatterySerialNumber = MostRecent.BatterySerialNumber
	WHERE fcc2 > fcc1
	
	UNION
	
	SELECT MostRecent.BatterySerialNumber
		,0 AS CapacityLossOver20Cycles
	FROM MostRecent
	INNER JOIN NextMostRecent
		ON NextMostRecent.BatterySerialNumber = MostRecent.BatterySerialNumber
	WHERE fcc2 <= fcc1
	) uq
INNER JOIN dbo.Assets a
	ON uq.BatterySerialNumber = a.SerialNo
ORDER BY BatterySerialNumber
GO
