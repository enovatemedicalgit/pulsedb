SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[prcDashGetBatteryInefficiencies] 
(
	   @SiteID INT = 657
)
AS


SELECT 
    SiteId,
    DepartmentId,
    ReportDay,
    base.TotalBatteries,
    base.BatteryInefficiencies
FROM
(
SELECT   a.SiteId,
	    a.DepartmentId, 
	    a.ReportDay, 
	    a.TotalBatteries, 
	    COALESCE(b.BatteryInefficiencies,7) AS BatteryInefficiencies
FROM
(
SELECT
	 s.SiteId  AS SiteId,
	 s.DepartmentId DepartmentId, 
      0 AS ReportDay,
	 COUNT(s.ROW_ID) AS TotalBatteries
FROM 
	 Sessions s
WHERE
	 s.SiteId = @SiteID
	 AND s.EndDateUTC >= CONVERT(DATE,GETUTCDATE())

GROUP BY 
			s.SiteId,
			s.DepartmentId
) a
LEFT JOIN
(SELECT
	 SiteId,
	 DepartmentId, 
      COUNT(DISTINCT ROW_ID) AS BatteryInefficiencies
FROM 
	 AlertCount
	 INNER JOIN
	 Assets
	   ON BatterySerialNumber = SerialNo		  
WHERE AlertConditionID IN (90,91)
	 AND AlertCount.CreatedDateUTC >= CONVERT(DATE,GETUTCDATE())
GROUP BY 
	SiteId,
	DepartmentId	 ) b		
ON
 a.SiteId = b.SiteID
 AND
 a.DepartmentId = b.DepartmentId	 
UNION ALL
SELECT b.SiteId, b.DepartmentId, b.ReportDay, b.TotalBatteries, e.BatteryInefficiencies
FROM

(
SELECT
	 s.SiteId  AS SiteId,
	 s.DepartmentId DepartmentId, 
      DATEDIFF(DAY,CONVERT(DATE,s.EndDateUTC),GETUTCDATE()) AS ReportDay,
	 COUNT(s.ROW_ID) AS TotalBatteries
FROM 
	 Sessions s
WHERE
	 s.SiteId = @SiteID
	 AND s.EndDateUTC BETWEEN DATEADD(DAY,-14,GETUTCDATE()) AND DATEADD(DAY,-1,GETUTCDATE())

GROUP BY 
			s.SiteId,
			s.DepartmentId,
			DATEDIFF(DAY,CONVERT(DATE,s.EndDateUTC),GETUTCDATE())
) b

INNER JOIN

(
SELECT
	  ssd.IDSite AS                                                SiteId
	, ssd.IDDepartment  AS                                          DepartmentId
	, DATEDIFF(DAY, ssd.CreatedDateUTC, GETUTCDATE())  AS           ReportDay
	
	, SUM(ssd.HiChargeRemovalsCount + ssd.LoChargeInsertsCount) AS BatteryInefficiencies
FROM
	dbo.SummarySiteDepartment ssd
WHERE  ssd.IDSite = @SiteID
	  AND ssd.CreatedDateUTC BETWEEN DATEADD(DAY, -14, GETUTCDATE()) AND DATEADD(DAY, -1, GETUTCDATE())
GROUP BY
	    ssd.IDSite
	  , ssd.IDDepartment
	  , DATEDIFF(DAY, ssd.CreatedDateUTC, GETUTCDATE())
) e

ON 
   b.SiteId = e.SiteId
   AND b.DepartmentId = e.DepartmentId 
   AND b.ReportDay = e.ReportDay
)base

ORDER BY 
    ReportDay DESC,
    SiteId,
    DepartmentId ASC


GO
