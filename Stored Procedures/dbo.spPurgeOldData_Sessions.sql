SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spPurgeOldData_Sessions]
AS
    BEGIN
        SET NOCOUNT ON;

		----------------------------------------------------------------------------------------------------------------
		-- VARIABLES
		----------------------------------------------------------------------------------------------------------------
        DECLARE @dateFloor DATETIME;
        DECLARE @dateCalc DATETIME;
		
		---------------------------------------------------
		-- set this to the number of days you want to keep (ie. -365 for one year, -120, -90, -30, etc. )
		---------------------------------------------------
        DECLARE @daysToKeep INT;
            SET @daysToKeep = -60; --(4 months plus buffer)
		
		----------------------------------------------------------------------------------------------------------------

        SET @dateCalc = DATEADD(DAY, ( @daysToKeep * -1 ), GETUTCDATE());

		-- @dateFloor becomes the date that is the start of the day for the day found above. 
		-- This way we remove records by full days.
        SET @dateFloor = CONVERT(DATETIME, ( CONVERT(VARCHAR(10), @dateCalc, 111) ));

		
		--LOOP : this starts looping through and deleting records by how many we set for each pass until we delete all records below the date floor
        WHILE EXISTS ( SELECT   1
                       FROM     dbo.Sessions
                       WHERE    StartDateUTC < @dateFloor )
            BEGIN
		
			---------------------------------------------------
			-- Do the deletion from the table
			-- Set how many records to delete on each pass using the TOP command
			-- A low number prevents out of control transaction log growth.
			---------------------------------------------------
                DELETE  FROM dbo.Sessions
                WHERE   ROW_ID IN ( SELECT TOP 1000
                                            ROW_ID
                                    FROM    dbo.Sessions
                                    WHERE   StartDateUTC < @dateFloor
                                    ORDER BY StartDateUTC ASC );
            END;
    END;

GO
