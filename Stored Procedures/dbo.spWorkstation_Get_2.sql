SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Description:	Query for the Workstation.aspx
-- page in CAST. Delimits what is shown by 
-- UserID and if they are Stinger. 

-- Modified 6/28/2013 
-- added specific order by clause to accomodate
-- for telerik formatting on edits. 

-- Modified added LastPostDate
-- =============================================
CREATE PROCEDURE [dbo].[spWorkstation_Get_2]
    @UserID INT ,
    @UserIsStinger VARCHAR(5)
AS
    BEGIN
	
        SET NOCOUNT ON;
	--Variables for testing
 --   declare @UserID int = 813
	--declare @UserIsStinger varchar(5) = 'True'
	
        SELECT  d.IDAsset , 
		--d.Retired, 
                d.SerialNo ,
                d.InvoiceNumber , 
		--d.PartNumber, 
                d.AssetNumber , 
		--d.Model, 
		--pn.[Description], 
                d.Description ,
                d.SiteID ,
                bu.SiteDescription AS BusinessUnitName ,
                parentbu.CustomerName AS ParentBusinessUnitName ,
                d.DepartmentID ,
                dept.Description AS DepartmentName ,
                d.Floor AS AssignedFloor ,
                d.Wing AS AssignedWing , 
		--d.Other as AssignedOther, 
                d.IsActive ,
                CASE WHEN d.IsActive <> 0 THEN 'Y'
                     ELSE 'N'
                END AS IsActiveYN ,
                d.Notes , 
		--d.Notes, 
                d.IDAssetType ,
                dt.Description AS DeviceTypeDescription ,
                d.CreatedDateUTC ,
                d.LastPostDateUTC ,
                COALESCE(d.LastPostDateUTC, '2011-1-11 15:00:00.000') AS LastPostDateUTC ,
		--CASE 
		--	WHEN COALESCE(d.ModifiedUserID, 0) = 0 
		--		THEN '' 
		--		ELSE COALESCE(u.FirstName, '') + ' ' + COALESCE(u.LastName, '') 
		--	END AS ModifiedUserFullName, 
		--CASE 
		--	WHEN COALESCE(d.CreatedUserID, 0) = 0 
		--		THEN '' 
		--		ELSE  COALESCE(u2.FirstName, '') + ' ' + COALESCE(u2.LastName, '') 
		--	END AS CreatedUserFullName, 
		
		--ap.MACAddress + CASE 
		--					WHEN COALESCE(ap.[Description], '') NOT IN ('', ap.MACAddress) 
		--						THEN ' (' + ap.[Description] + ')' 
		--						ELSE '' 
		--					END + ' at ' + apbu.SiteDescription AS AccessPointDetail, 
		--ap.BusinessUnitID AS AccessPointBusinessUnitID, 
		--CASE 
		--	WHEN COALESCE(ap.BusinessUnitID, 0) <> COALESCE(d.BusinessUnitID, 0) 
		--		THEN 'Y' 
		--		ELSE 'N' 
		--	END AS LocationFlag, 
		--ap.[Floor] AS AccessPointFloor, 
		--ap.Wing AS AccessPointWing, 
		--ap.Other AS AccessPointOther, 
		--aio.ProductSerialNumber AS AIOSerialNumber, 
		--dcmonitor.ProductSerialNumber AS DCMonitorSerialNumber, 
		--cart.ProductSerialNumber AS CartSerialNumber, 
                d.Floor AS AccessPointFloor ,
                d.Wing AS AccessPointWing , 
	--	d.Other AS AccessPointOther, 
                d.SerialNo AS AIOSerialNumber ,
                d.SerialNo AS DCMonitorSerialNumber ,
                d.SerialNo AS CartSerialNumber , 
		--d.ControlBoardRevision, 
		--d.LCDRevision, 
		--d.HolsterChargerRevision, 
		--d.DCBoardOneRevision, 
		--d.DCBoardTwoRevision, 
		--d.WiFiFirmwareRevision, 
		--d.MedBoardRevision, 
                d.DeviceMAC ,
                ds.Description AS DeviceStatus --,		
		--Coalesce(Convert(varchar,PM.CreatedDateUTC,101), 'N/A') as PM,
		--Coalesce(PM.Result, 'N/A') as PM_Result,
		--Coalesce(Convert(varchar,RE.CreatedDateUTC,101), 'N/A') as Repair,
		--Coalesce(RE.Result, 'N/A') as Repair_Result,
		--Coalesce(Convert(varchar,CE.CreatedDateUTC,101), 'N/A') as Certification,
		--Coalesce(CE.Result, 'N/A') as Certification_Result
        FROM    dbo.Assets d --LEFT JOIN 
		--	PartNumber pn ON left(d.SerialNumber, 7) = pn.PartNumber 
                LEFT JOIN dbo.AssetStatus ds ON d.AssetStatusID = ds.IDAssetStatus 
		--LEFT JOIN 
		--	AccessPoint ap ON d.AccessPointID = ap.ROW_ID 
                LEFT JOIN dbo.Sites apbu ON d.SiteID = apbu.IDSite 
		--LEFT JOIN 
		--	[User] u ON d.ModifiedUserID = u.ROW_ID 
		--LEFT JOIN 
		--	[User] u2 ON d.CreatedUserID = u2.ROW_ID 	
		--LEFT JOIN 
		--	QA PM ON PM.DeviceSerialNumber = d.SerialNumber AND PM.Latest = 1 AND PM.QAStationID = 8
		--LEFT JOIN 
		--	QA RE On RE.DeviceSerialNumber = d.SerialNumber AND RE.Latest = 1 AND RE.QAStationID = 9	
		--LEFT JOIN
		--	QA CE On CE.DeviceSerialNumber = d.SerialNumber AND CE.Latest = 1 and CE.QAStationID = 10
                JOIN dbo.AssetType dt ON d.IDAssetType = dt.IDAssetType
                LEFT JOIN dbo.Sites bu ON d.SiteID = bu.IDSite
                LEFT JOIN dbo.Customers parentbu ON bu.CustomerID = parentbu.IDCustomer
                LEFT JOIN dbo.Departments dept ON d.DepartmentID = dept.IDDepartment 
		--LEFT JOIN 
		--	vwProductAIO aio ON d.SerialNumber = aio.DeviceSerialNumber 
		--LEFT JOIN 
		--	vwProductDCMONitor dcmONitor ON d.SerialNumber = dcmONitor.DeviceSerialNumber 
		--LEFT JOIN 
		--	vwProductCart cart ON d.SerialNumber = cart.DeviceSerialNumber 
        WHERE   d.IDAssetType IN ( 1, 3, 6, 8 )
                AND ( bu.IDSite IN ( SELECT SiteId
                                     FROM   dbo.[UserSites]
                                     WHERE  User_Row_Id = @UserID )
                      OR bu.CustomerID IN ( SELECT  SiteId
                                            FROM    dbo.[UserSites]
                                            WHERE   User_Row_Id = @UserID ) 
					--	OR bu.IDSite IN (SELECT distinct IDSite FROM Sites WHERE @UserIsStinger = 'True'))
                    )
        ORDER BY d.IDAsset DESC; 
    END;


GO
