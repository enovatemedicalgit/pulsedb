SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[spQueryString_Insert3]
    (
      @DEVICESERIALNUMBER VARCHAR(50) ,
      @QUERY VARCHAR(2000) ,
      @NINEPARSED BIT ,
      @SOURCEIPADDRESS VARCHAR(20) ,
      @SOURCETIMESTAMPUTC VARCHAR(23)
    )
AS
    BEGIN
        SET NOCOUNT ON;

        INSERT  INTO dbo.QueryString
                ( DeviceSerialNumber ,
                  Query ,
                  NineParsed ,
                  SourceIPAddress ,
                  SourceTimestampUTC
                )
	--INSERT INTO QueryStringHolding (DeviceSerialNumber, Query, NineParsed, SourceIPAddress, SourceTimestampUTC)
        VALUES  ( @DEVICESERIALNUMBER ,
                  @QUERY ,
                  @NINEPARSED ,
                  @SOURCEIPADDRESS ,
                  @SOURCETIMESTAMPUTC
                );

    END;






GO
