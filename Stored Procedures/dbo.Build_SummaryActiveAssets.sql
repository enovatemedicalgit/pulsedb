SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*################################################################################################
 Name				: Build_SummaryActiveAssets
 Date				: 
 Author				: christopher.stewart
 Company				: enovate 
 Purpose				: Build SummaryActiveAssets 
 Usage				:   
 Impact				:   
 Required Perm			:   
 Called by			:   
##################################################################################################
 ver		author					date			change  
 1.0		Arlow.Farrell			Unknown			initial [spSummaryActiveAssets_RollupNow]
 1.1		Christopher.Stewart		07012016			Optimizition and Comments
#################################################################################################*/

/*##################################################################################################
 STEPS		Description						 
 1			Delete from SummaryActiveAssets
 2			Insert into tmpSD
 3			Insert into SummaryActiveAssets
 4			Update SummaryActiveAssets set ActiveWorkStationCount
 5			Update SummaryActiveAssets set AvailableWorkStationCount
 6			Update SummaryActiveAssets set InServiceWorkStationCount
 7			Update SummaryActiveAssets set ActiveChargerCount
 8			Insert into tmpBatteryNSD
 9			Insert into tmpBatteryNSD
 10			Insert into tmpBatterySD
 11			Update SummaryActiveAssets set ActiveBatteryCount
 12			Update SummaryActiveAssets set DormantCount
 13			Update SummaryActiveAssets set DormantWorkStationCount
 14			Update SummaryActiveAssets set BayCount
 15			Load temp table for ChargerBays
 16			BatteriesInWorkStationCount - load tmpBnW SessionData_Summary
 17			BatteriesInWorkStationCount - load tmpBnW nonSessionData_Summary 
 18			BatteriesInWorkStationCount - remove all but last entry
 19			Update Active Charger bays
 20			Update Vacant Charger bays
 21			Update Offline Charger bays
 22			Update SummaryActiveAssets set BatteriesInWorkStationCount
 23			Clean Up TempDB
  
#################################################################################################*/

CREATE PROCEDURE [dbo].[Build_SummaryActiveAssets]
AS
    DECLARE @LapTimer AS DATETIME2 = GETDATE();
    SET NOCOUNT ON;
    DECLARE @CreatedDateUTC DATETIME;
    SET @CreatedDateUTC = CAST(CONVERT(VARCHAR(10), GETDATE(), 101) AS DATETIME);
    PRINT @CreatedDateUTC;
    DECLARE @starttime DATETIME;
    SET @starttime = GETDATE();
	    ---- only need one summary per day


    PRINT dbo.GetMessageBorder();
/*################################################################################################*/
    Step_0:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 0';
    RAISERROR('Delete from SummaryActiveAssets', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/
--PRINT dbo.GetLapTime(@LapTimer) 

    PRINT '--Deletes Existing records from SummaryActiveAssets So that it doesnt create duplicates';
    DELETE  FROM dbo.SummaryActiveAssets
    WHERE   CreatedDateUTC = @CreatedDateUTC;
    IF OBJECT_ID(N'tempdb..#tmpNSD', N'U') IS NOT NULL
        BEGIN
            DROP TABLE #tmpNSD;
        END;
    IF OBJECT_ID(N'tempdb..#tmpSD', N'U') IS NOT NULL
        BEGIN
            DROP TABLE #tmpSD;
        END;

    PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_1:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 1';
    RAISERROR('Delete from SummaryActiveAssets', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/


    PRINT 'INSERT into tmpNSD';
    SELECT  *
    INTO    #tmpNSD
    FROM    ( SELECT    d.SiteID ,
                        d.DepartmentID ,
                        MAX(nsd.CreatedDateUTC) AS CreatedDateUtc ,
                        d.IDAssetType ,
                        nsd.DeviceSerialNumber
              FROM      dbo.NonSessionData_Summary nsd
                        RIGHT JOIN dbo.Assets d ON nsd.DeviceSerialNumber = d.SerialNo
              WHERE     nsd.CreatedDateUTC > DATEADD(d, -7, @CreatedDateUTC)
                        AND d.Retired = 0
                        AND ( d.IsActive = 1
                              AND ( d.OutOfService IS NULL
                                    OR d.OutOfService = 0
                                  )
                            )
              GROUP BY  d.SiteID ,
                        d.DepartmentID ,
                        d.IDAssetType ,
                        nsd.DeviceSerialNumber
            ) x;
    CREATE INDEX IDX_CreatedDateUtc ON #tmpNSD(CreatedDateUtc);
    CREATE INDEX IDX_SiteID ON #tmpNSD(SiteID);
    CREATE INDEX IDX_DepartmentID ON #tmpNSD(DepartmentID);

    PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_2:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 2';
    RAISERROR('Insert into tmpSD', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/
    PRINT '-- Insert into #tmpSD';	    
    SELECT  *
    INTO    #tmpSD
    FROM    ( SELECT    d.SiteID ,
                        d.DepartmentID ,
                        MAX(sd.CreatedDateUTC) AS CreatedDateUtc ,
                        d.IDAssetType ,
                        sd.DeviceSerialNumber
              FROM      --SessionData_Summary sd with (readpast) right join assets d on sd.DeviceSerialNumber = d.Serialno ReadPast Screwed Things UP
                        dbo.SessionData_Summary sd
                        RIGHT JOIN dbo.Assets d ON sd.DeviceSerialNumber = d.SerialNo
              WHERE     sd.CreatedDateUTC > DATEADD(d, -7, @CreatedDateUTC)
                        AND d.Retired = 0
                        AND ( d.IsActive = 1
                              AND ( d.OutOfService IS NULL
                                    OR d.OutOfService = 0
                                  )
                            )
              GROUP BY  d.SiteID ,
                        d.DepartmentID ,
                        d.IDAssetType ,
                        sd.DeviceSerialNumber
            ) x;
    CREATE INDEX IDXsd_CreatedDateUtc ON #tmpSD(CreatedDateUtc);
    CREATE INDEX IDXsd_SiteID ON #tmpSD(SiteID);
    CREATE INDEX IDXsd_DepartmentID ON #tmpNSD(DepartmentID);


    PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_3:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 1';
    RAISERROR('Insert into SummaryActiveAssets', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/
    INSERT  INTO dbo.SummaryActiveAssets
            ( SiteID ,
              DepartmentID ,
              CreatedDateUTC
	        )
            SELECT DISTINCT
                    SiteID ,
                    DepartmentID ,
                    @CreatedDateUTC
            FROM    dbo.Assets
            WHERE   IDAssetType IN ( 1, 2, 3, 4, 5, 6 )
                    AND Retired = 0; 


    PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_4:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 4';
    RAISERROR('Update SummaryActiveAssets set ActiveWorkStationCount', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/

    UPDATE  dbo.SummaryActiveAssets
    SET     ActiveWorkstationCount = workstations.ActiveWorkstationCount
    FROM    ( SELECT    s.SiteID ,
                        s.DepartmentID ,
                        COUNT(DISTINCT s.DeviceSerialNumber) AS ActiveWorkstationCount
              FROM      #tmpSD s
              WHERE     s.IDAssetType IN ( 1, 3, 6 )
              GROUP BY  s.SiteID ,
                        s.DepartmentID
            ) workstations
    WHERE   SummaryActiveAssets.SiteID = workstations.SiteID
            AND SummaryActiveAssets.DepartmentID = workstations.DepartmentID
            AND SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC;

    PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_5:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 5';
    RAISERROR('Update SummaryActiveAssets set AvailableWorkStationCount', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/
    UPDATE  dbo.SummaryActiveAssets
    SET     AvailableWorkstationCount = workstations.AvailableWorkstationCount
    FROM    ( SELECT    s.SiteID ,
                        ISNULL(s.DepartmentID, 0) AS DepartmentID ,
                        COUNT(DISTINCT s.DeviceSerialNumber) AS AvailableWorkstationCount
              FROM      #tmpSD s
              WHERE     s.CreatedDateUtc < DATEADD(hh, -2, @CreatedDateUTC)
                        AND s.CreatedDateUtc > DATEADD(d, -7, @CreatedDateUTC)
                        AND s.IDAssetType IN ( 1, 3, 6 )
              GROUP BY  s.SiteID ,
                        DepartmentID
            ) workstations
    WHERE   SummaryActiveAssets.SiteID = workstations.SiteID
            AND SummaryActiveAssets.DepartmentID = workstations.DepartmentID
            AND SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC;

    PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_6:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 6';
    RAISERROR('Update SummaryActiveAssets set InServiceWorkStationCount', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/
	    
    UPDATE  dbo.SummaryActiveAssets
    SET     InServiceWorkstationCount = workstations.ActiveWorkstationCount
    FROM    ( SELECT    s.SiteID ,
                        ISNULL(s.DepartmentID, 0) AS DepartmentID ,
                        COUNT(DISTINCT s.DeviceSerialNumber) AS ActiveWorkstationCount
              FROM      #tmpSD s
              WHERE     s.CreatedDateUtc > DATEADD(hh, -2, @CreatedDateUTC)
                        AND s.IDAssetType IN ( 1, 3, 6 )
              GROUP BY  s.SiteID ,
                        DepartmentID
            ) workstations
    WHERE   SummaryActiveAssets.SiteID = workstations.SiteID
            AND SummaryActiveAssets.DepartmentID = workstations.DepartmentID
            AND SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC;

    PRINT dbo.GetLapTime(@LapTimer); 	



/*################################################################################################*/
    Step_7:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 7';
    RAISERROR('Insert into #tmpchargers from nonSessionData_Summary ', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/












    IF OBJECT_ID(N'tempdb..#tmpChargers', N'U') IS NOT NULL
        BEGIN
            DROP TABLE #tmpChargers;
        END;
	    
    SELECT  d.SiteID ,
            ISNULL(d.DepartmentID, 0) AS DepartmentID ,
            ISNULL(COUNT(DISTINCT d.SerialNo), 0) AS ActiveChargerCount
    INTO    #tmpChargers
    FROM    dbo.NonSessionData_Summary nsd
            LEFT JOIN dbo.Assets d ON nsd.DeviceSerialNumber = d.SerialNo
    WHERE   nsd.CreatedDateUTC > DATEADD(d, -7, @CreatedDateUTC)
            AND d.Retired = 0
            AND nsd.DeviceType IN ( 2, 4 )
	    --and d.OutOfService = 0
    GROUP BY d.SiteID ,
            d.DepartmentID;
    CREATE NONCLUSTERED INDEX IX_tmpChargers ON #tmpChargers(SiteID, DepartmentID, ActiveChargerCount);

    PRINT dbo.GetLapTime(@LapTimer); 	
/*################################################################################################*/
    Step_8:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 8';
    RAISERROR('Update SummaryActiveAssets set ActiveChargerCount', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/
	    
    UPDATE  dbo.SummaryActiveAssets
    SET     ActiveChargerCount = ISNULL(chargers.ActiveChargerCount, 0)
    FROM    #tmpChargers chargers
    WHERE   SummaryActiveAssets.SiteID = chargers.SiteID
            AND SummaryActiveAssets.DepartmentID = chargers.DepartmentID
            AND CreatedDateUTC = @CreatedDateUTC;
	    --COMMIT
	    --BEGIN TRANSACTION
	    -- count batteries

    PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_9:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 9';
    RAISERROR('Insert into tmpBatteryNSD', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/
    IF OBJECT_ID(N'tempdb..##tmpBatteryNSD', N'U') IS NOT NULL
        BEGIN
            DROP TABLE #tmpBatteryNSD;
        END;
	    
    SELECT  *
    INTO    #tmpBatteryNSD
    FROM    ( SELECT    d.SiteID ,
                        d.DepartmentID ,
                        MAX(nsd.CreatedDateUTC) AS CreatedDateUtc ,
                        d.IDAssetType ,
                        nsd.BatterySerialNumber
              FROM      dbo.NonSessionData_Summary nsd WITH ( READPAST )
                        RIGHT JOIN dbo.Assets d ON nsd.BatterySerialNumber = d.SerialNo
              WHERE     nsd.CreatedDateUTC > DATEADD(d, -7, @CreatedDateUTC)
                        AND d.Retired = 0
                        AND d.OutOfService = 0
                        AND d.IsActive = 1
              GROUP BY  d.SiteID ,
                        d.DepartmentID ,
                        d.IDAssetType ,
                        nsd.BatterySerialNumber
            ) x;
    CREATE INDEX IDXb_CreatedDateUtc ON #tmpBatteryNSD(CreatedDateUtc);
    CREATE INDEX IDXb_SiteID ON #tmpBatteryNSD(SiteID);
    CREATE INDEX IDXb_DepartmentID ON #tmpBatteryNSD(DepartmentID);

    PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_10:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 10';
    RAISERROR('Insert into tmpBatterySD', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/

    IF OBJECT_ID(N'tempdb..##tmpBatterySD', N'U') IS NOT NULL
        BEGIN
            DROP TABLE #tmpBatterySD;
        END;
	   
    SELECT  *
    INTO    #tmpBatterySD
    FROM    ( SELECT    d.SiteID ,
                        d.DepartmentID ,
                        MAX(sd.CreatedDateUTC) AS CreatedDateUtc ,
                        d.IDAssetType ,
                        sd.BatterySerialNumber
              FROM      dbo.SessionData_Summary sd WITH ( READPAST )
                        RIGHT JOIN dbo.Assets d ON sd.BatterySerialNumber = d.SerialNo
              WHERE     sd.CreatedDateUTC > DATEADD(d, -7, @CreatedDateUTC)
                        AND d.Retired = 0
                        AND d.OutOfService = 0
                        AND d.IsActive = 1
              GROUP BY  d.SiteID ,
                        d.DepartmentID ,
                        d.IDAssetType ,
                        sd.BatterySerialNumber
            ) x;
    CREATE INDEX IDXbs_CreatedDateUtc ON #tmpBatterySD(CreatedDateUtc);
    CREATE INDEX IDXbs_SiteID ON #tmpBatterySD(SiteID);
    CREATE INDEX IDXbs_DepartmentID ON #tmpBatterySD(DepartmentID);

    PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_11:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 11';
    RAISERROR('Update SummaryActiveAssets set ActiveBatteryCount', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/

    UPDATE  dbo.SummaryActiveAssets
    SET     ActiveBatteryCount = batteries.ActiveBatteryCount
    FROM    ( SELECT    asdf.SiteID ,
                        asdf.DepartmentID ,
                        COUNT(DISTINCT asdf.BatterySerialNumber) AS ActiveBatteryCount
              FROM      ( SELECT DISTINCT
                                    SiteID ,
                                    DepartmentID ,
                                    BatterySerialNumber
                          FROM      #tmpBatteryNSD
                          WHERE     ISNULL(BatterySerialNumber, '') <> ''
                          UNION
                          SELECT DISTINCT
                                    SiteID ,
                                    DepartmentID ,
                                    BatterySerialNumber
                          FROM      #tmpBatterySD
                          WHERE     ISNULL(BatterySerialNumber, '') <> ''
                        ) asdf
              GROUP BY  asdf.SiteID ,
                        asdf.DepartmentID
            ) batteries
    WHERE   SummaryActiveAssets.SiteID = batteries.SiteID
            AND SummaryActiveAssets.DepartmentID = batteries.DepartmentID
            AND SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC;

    PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_12:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 12';
    RAISERROR('Update SummaryActiveAssets set DormantCount', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/

	    
    UPDATE  dbo.SummaryActiveAssets
    SET     DormantBatteryCount = batteries.DormantBatteryCount
    FROM    ( SELECT    asdf.SiteID ,
                        asdf.DepartmentID ,
                        COUNT(DISTINCT asdf.BatterySerialNumber) AS DormantBatteryCount
              FROM      ( SELECT DISTINCT
                                    nsd.SiteID ,
                                    nsd.DepartmentID ,
                                    nsd.BatterySerialNumber
                          FROM      #tmpBatteryNSD nsd
                          WHERE     nsd.CreatedDateUtc > DATEADD(d, -7,
                                                              @CreatedDateUTC)
                                    AND nsd.CreatedDateUtc < DATEADD(d, -2,
                                                              @CreatedDateUTC)
                                    AND ISNULL(nsd.BatterySerialNumber, '') <> ''
                          UNION
                          SELECT DISTINCT
                                    sd.SiteID ,
                                    sd.DepartmentID ,
                                    sd.BatterySerialNumber
                          FROM      #tmpBatterySD sd
                          WHERE     sd.CreatedDateUtc > DATEADD(d, -7,
                                                              @CreatedDateUTC)
                                    AND sd.CreatedDateUtc < DATEADD(d, -2,
                                                              @CreatedDateUTC)
                                    AND ISNULL(sd.BatterySerialNumber, '') <> ''
                        ) asdf
              GROUP BY  asdf.SiteID ,
                        asdf.DepartmentID
            ) batteries
    WHERE   SummaryActiveAssets.SiteID = batteries.SiteID
            AND SummaryActiveAssets.DepartmentID = batteries.DepartmentID
            AND SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC;

    PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_13:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 13';
    RAISERROR('Update SummaryActiveAssets set DormantWorkStationCount', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/


	    
    UPDATE  dbo.SummaryActiveAssets
    SET     DormantWorkstationCount = ws.DormantWorkStationCount
    FROM    ( SELECT    asdf.SiteID ,
                        asdf.DepartmentID ,
                        COUNT(DISTINCT asdf.DeviceSerialNumber) AS DormantWorkStationCount
              FROM      ( SELECT DISTINCT
                                    nsd.SiteID ,
                                    nsd.DepartmentID ,
                                    nsd.DeviceSerialNumber
                          FROM      #tmpNSD nsd
                          WHERE     nsd.CreatedDateUtc > DATEADD(d, -7,
                                                              @CreatedDateUTC)
                                    AND nsd.CreatedDateUtc < DATEADD(d, -2,
                                                              @CreatedDateUTC)
                                    AND ISNULL(nsd.DeviceSerialNumber, '') <> ''
                          UNION
                          SELECT DISTINCT
                                    sd.SiteID ,
                                    sd.DepartmentID ,
                                    sd.DeviceSerialNumber
                          FROM      #tmpSD sd
                          WHERE     sd.CreatedDateUtc > DATEADD(d, -7,
                                                              @CreatedDateUTC)
                                    AND sd.CreatedDateUtc < DATEADD(d, -2,
                                                              @CreatedDateUTC)
                                    AND ISNULL(sd.DeviceSerialNumber, '') <> ''
                        ) asdf
              GROUP BY  asdf.SiteID ,
                        asdf.DepartmentID
            ) ws
    WHERE   SummaryActiveAssets.SiteID = ws.SiteID
            AND SummaryActiveAssets.DepartmentID = ws.DepartmentID
            AND SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC;

    PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_14:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 14';
    RAISERROR('Update SummaryActiveAssets set BayCount', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/

	   
    UPDATE  dbo.SummaryActiveAssets
    SET     BayCount = chg.bays
    FROM    ( SELECT    SUM(BayCount) AS bays ,
                        SiteID ,
                        ISNULL(DepartmentID, 0) AS DepartmentID
              FROM      dbo.Assets
              WHERE     IDAssetType IN ( 2, 4 )
              GROUP BY  SiteID ,
                        DepartmentID
            ) chg
    WHERE   SummaryActiveAssets.SiteID = chg.SiteID
            AND SummaryActiveAssets.DepartmentID = chg.DepartmentID
            AND SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC;

    PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_15:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 15';
    RAISERROR('Load temp table for ChargerBays', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/

	    
    IF OBJECT_ID(N'tempdb..#myTemp', N'U') IS NOT NULL
        BEGIN
            DROP TABLE #myTemp;
        END;
    SELECT  *
    INTO    #mytemp
    FROM    ( SELECT    nd.DeviceSerialNumber ,
                        nd.Bay ,
                        nd.BatterySerialNumber ,
                        a.SiteID ,
                        a.DepartmentID ,
                        MAX(nd.CreatedDateUTC) AS createddateutc
              FROM      dbo.NonSessionData_Summary nd
                        LEFT JOIN dbo.Assets a ON nd.DeviceSerialNumber = a.SerialNo
                                              AND a.IDAssetType IN ( 2, 4 )
                                              AND a.AssetStatusID = 2
                                              AND a.Retired = 0
              WHERE     nd.CreatedDateUTC > @CreatedDateUTC
                        AND nd.Bay IS NOT NULL
                        AND a.SiteID IS NOT NULL
              GROUP BY  a.SiteID ,
                        a.DepartmentID ,
                        nd.DeviceSerialNumber ,
                        nd.Bay ,
                        nd.BatterySerialNumber
              HAVING    SUM(CASE WHEN nd.BatterySerialNumber = 'No Batt SN'
                                      OR nd.BatterySerialNumber = ''
                                      OR nd.BatterySerialNumber IS NULL THEN 0
                                 ELSE 1
                            END) > 0
            ) x;


    PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_16:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 16';
    RAISERROR('BatteriesInWorkStationCount - load tmpBnW SessionData_Summary', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/

    IF OBJECT_ID(N'tempdb..#tmpBnW', N'U') IS NOT NULL
        BEGIN
            DROP TABLE #tmpBnW;
        END;
	    
    SELECT  *
    INTO    #tmpBnW
    FROM    ( SELECT    t1.BatterySerialNumber ,
                        t1.Bay ,
                        t1.CreatedDateUTC ,
                        t1.DeviceSerialNumber ,
                        t1.ChargeLevel ,
                        d.IDAssetType ,
                        d.SiteID ,
                        d.DepartmentID
              FROM      dbo.SessionData_Summary t1
                        JOIN dbo.Assets d ON d.SerialNo = t1.DeviceSerialNumber
                        JOIN ( SELECT   BatterySerialNumber ,
                                        MAX(CreatedDateUTC) maxdate
                               FROM     dbo.SessionData_Summary
                               GROUP BY BatterySerialNumber
                             ) t2 ON t1.BatterySerialNumber = t2.BatterySerialNumber
                                     AND t1.CreatedDateUTC = t2.maxdate
              WHERE     t1.CreatedDateUTC > DATEADD(d, -2, @CreatedDateUTC)
                        AND d.Retired = 0
                        AND ISNULL(t1.BatterySerialNumber, '') <> ''
            ) x;

    PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_17:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 17';
    RAISERROR('BatteriesInWorkStationCount - load tmpBnW nonSessionData_Summary ', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/


    INSERT  INTO #tmpBnW
            ( BatterySerialNumber ,
              Bay ,
              CreatedDateUTC ,
              DeviceSerialNumber ,
              ChargeLevel ,
              IDAssetType ,
              SiteID ,
              DepartmentID
	        )
            SELECT  t1.BatterySerialNumber ,
                    t1.Bay ,
                    t1.CreatedDateUTC ,
                    t1.DeviceSerialNumber ,
                    t1.ChargeLevel ,
                    d.IDAssetType ,
                    d.SiteID ,
                    d.DepartmentID
            FROM    dbo.NonSessionData_Summary t1
                    JOIN dbo.Assets d ON d.SerialNo = t1.DeviceSerialNumber
                    JOIN ( SELECT   BatterySerialNumber ,
                                    MAX(CreatedDateUTC) maxdate
                           FROM     dbo.NonSessionData_Summary
                           GROUP BY BatterySerialNumber
                         ) t2 ON t1.BatterySerialNumber = t2.BatterySerialNumber
                                 AND t1.CreatedDateUTC = t2.maxdate
            WHERE   t1.CreatedDateUTC > DATEADD(d, -2, @CreatedDateUTC)
                    AND d.Retired = 0
                    AND ISNULL(t1.BatterySerialNumber, '') <> '';
    CREATE INDEX IDXbnw_CreatedDateUtc ON #tmpBnW(CreatedDateUTC);
    CREATE INDEX IDxbnw_BATTERYSerialNumber ON #tmpBnW(BatterySerialNumber);

    PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_18:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 18';
    RAISERROR('BatteriesInWorkStationCount - remove all but last entry', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/

	    
    WITH    T AS ( SELECT   ROW_NUMBER() OVER ( PARTITION BY BatterySerialNumber ORDER BY CreatedDateUTC DESC ) AS rnum ,
                            *
                   FROM     #tmpBnW
                 )
        DELETE  FROM T
        WHERE   T.rnum > 1;
    WITH    TW
              AS ( SELECT   ROW_NUMBER() OVER ( PARTITION BY DeviceSerialNumber ORDER BY CreatedDateUTC DESC ) AS rnum ,
                            *
                   FROM     #tmpBnW
                 )
        DELETE  FROM TW
        WHERE   TW.rnum > 1;
    IF OBJECT_ID(N'tempdb..#tmpAssets', N'U') IS NOT NULL
        BEGIN
            DROP TABLE #tmpAssets;
        END;
    WITH    num
              AS ( SELECT   number AS number
                   FROM     master..spt_values
                   WHERE    type = 'P'
                 )
        SELECT  d.SerialNo ,
                d.BayCount ,
                n.number + 1 AS baynum ,
                d.SiteID ,
                d.DepartmentID ,
                d.IDAssetType
        INTO    #tmpAssets
        FROM    dbo.Assets d
                INNER JOIN num n ON n.number < d.BayCount
        WHERE   d.IDAssetType IN ( 2, 4 )
                AND d.Retired = 0; ---Added By arlow to make sure 4's were included

    PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_19:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 19';
    RAISERROR('Update Vacant Charger bays', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/
	    
    UPDATE  dbo.SummaryActiveAssets
    SET     ActiveChargerBays = chg.ActiveChargerbays
    FROM    ( SELECT    a.SiteID ,
                        a.DepartmentID ,
                        SUM(a.BayCount) AS ActiveChargerbays
              FROM      dbo.Assets a
              WHERE     a.IDAssetType IN ( 2, 4 )
                        AND a.Retired = 0
                        AND a.LastPostDateUTC > DATEADD(HOUR, -2, GETDATE())
              GROUP BY  a.SiteID ,
                        a.DepartmentID
            ) chg
    WHERE   SummaryActiveAssets.SiteID = chg.SiteID
            AND SummaryActiveAssets.DepartmentID = chg.DepartmentID
            AND SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC;

    PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_20:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 20';
    RAISERROR('Update Vacant Charger bays', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/
	    
    UPDATE  dbo.SummaryActiveAssets
    SET     VacantChargerBays = SummaryActiveAssets.BayCount
            - ( ActiveChargerBays ) --+ chg.OfflineChargerbays )
    FROM    ( SELECT    a.SiteID ,
                        a.DepartmentID ,
                        SUM(a.BayCount) AS OfflineChargerbays
              FROM      dbo.Assets a
              WHERE     a.IDAssetType IN ( 2, 4 )
                        AND a.Retired = 0
                        AND a.LastPostDateUTC < DATEADD(DAY, -1, GETDATE())
              GROUP BY  a.SiteID ,
                        a.DepartmentID
            ) chg
    WHERE   SummaryActiveAssets.SiteID = chg.SiteID
            AND SummaryActiveAssets.DepartmentID = chg.DepartmentID
            AND SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC;


    PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_21:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 21';
    RAISERROR('Update Offline Charger bays', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/
	    
    UPDATE  dbo.SummaryActiveAssets
    SET     OfflineChargerBays = chg.OfflineChargerbays
    FROM    ( SELECT    a.SiteID ,
                        a.DepartmentID ,
                        SUM(a.BayCount) AS OfflineChargerbays
              FROM      dbo.Assets a
              WHERE     a.IDAssetType IN ( 2, 4 )
                        AND a.Retired = 0
                        AND a.LastPostDateUTC < DATEADD(DAY, -1, GETDATE())
              GROUP BY  a.SiteID ,
                        a.DepartmentID
            ) chg
    WHERE   SummaryActiveAssets.SiteID = chg.SiteID
            AND SummaryActiveAssets.DepartmentID = chg.DepartmentID
            AND SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC;

    PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_22:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 22';
    RAISERROR('Update SummaryActiveAssets set BatteriesInWorkStationCount', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/
	    
    UPDATE  dbo.SummaryActiveAssets
    SET     BatteryInWorkstationCount = x.BatteriesInWorkStationCount ,
            BatteryInChargerCount = x.BatteriesInChargerCount ,
            CountFullBatteries = x.CountFullBatteries
    FROM    ( SELECT    y.SiteID ,
                        y.DepartmentID ,
                        SUM(y.BatteryInWorkStationCount) AS BatteriesInWorkStationCount ,
                        SUM(y.BatteryInChargerCount) BatteriesInChargerCount ,
                        SUM(y.CountFullBatteries) CountFullBatteries
              FROM      ( SELECT    SiteID ,
                                    DepartmentID ,
                                    BatterySerialNumber ,
                                    IDAssetType ,
                                    ChargeLevel ,
                                    CreatedDateUTC ,
                                    CASE WHEN IDAssetType = 1
                                              OR IDAssetType = 3
                                              OR IDAssetType = 6 THEN 1
                                         ELSE 0
                                    END AS BatteryInWorkStationCount ,
                                    CASE WHEN IDAssetType = 2
                                              OR IDAssetType = 4 THEN 1
                                         ELSE 0
                                    END AS BatteryInChargerCount ,
                                    CASE WHEN ChargeLevel > 80 THEN 1
                                         ELSE 0
                                    END AS CountFullBatteries
                          FROM      #tmpBnW
                        ) y
              GROUP BY  y.SiteID ,
                        y.DepartmentID
            ) x
    WHERE   SummaryActiveAssets.SiteID = x.SiteID
            AND SummaryActiveAssets.DepartmentID = x.DepartmentID
            AND SummaryActiveAssets.CreatedDateUTC = @CreatedDateUTC;

    PRINT dbo.GetLapTime(@LapTimer); 
/*################################################################################################*/
    Step_23:
    SET @LapTimer = GETDATE();
    PRINT CONVERT(VARCHAR(30), @LapTimer, 109) + ' Begin Step 23';
    RAISERROR('Clean Up Tempdb', 0, 1) WITH NOWAIT;
							 
/*################################################################################################*/
    DROP TABLE #tmpBnW;
    IF OBJECT_ID(N'tempdb..#tmpChargers', N'U') IS NOT NULL
        BEGIN
            DROP TABLE #tmpChargers;
        END;
    IF OBJECT_ID(N'tempdb..##tmpNSD', N'U') IS NOT NULL
        BEGIN
            DROP TABLE #tmpNSD;
        END;
    IF OBJECT_ID(N'tempdb..##tmpSD', N'U') IS NOT NULL
        BEGIN
            DROP TABLE #tmpSD;
        END;
    IF OBJECT_ID(N'tempdb..##tmpBatteryNSD', N'U') IS NOT NULL
        BEGIN
            DROP TABLE #tmpBatteryNSD;
        END;
    IF OBJECT_ID(N'tempdb..##tmpBatterySD', N'U') IS NOT NULL
        BEGIN
            DROP TABLE #tmpBatterySD;
        END;
GO
