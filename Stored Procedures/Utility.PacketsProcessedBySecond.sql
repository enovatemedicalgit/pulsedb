SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Utility].[PacketsProcessedBySecond]
AS
BEGIN
	    --*******************************************************************
	    -- Average number of records per second in Session/NonSession Data
	    --*******************************************************************
	    IF OBJECT_ID('tempdb..#nsd') IS NOT NULL
		   BEGIN
			  DROP TABLE #nsd;
		   END;
	    SELECT TOP 50000
			 CONVERT( SMALLDATETIME, DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), nsd.InsertedDateUTC)) AS InsertedTime
		    , nsd.InsertedDateUTC
		    , nsd.QueryStringID
	    INTO
		    #nsd
	    FROM
		    NonSessionData_Summary AS nsd
	    ORDER BY
			   nsd.ROW_ID DESC;
	    INSERT INTO #nsd
	    SELECT TOP 50000
			 CONVERT( SMALLDATETIME, DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), sd.InsertedDateUTC)) AS InsertedTime
		    , sd.InsertedDateUTC
		    , sd.QueryStringID
	    FROM
		    SessionData_Summary AS sd
	    ORDER BY
			   sd.ROW_ID DESC;
	    CREATE NONCLUSTERED INDEX ix_tmptbl_nsd_insertedDateUTC ON #nsd(InsertedTime);
	    CREATE NONCLUSTERED INDEX idx_tmptbl_sd_QueryStringId ON #nsd
	    (QueryStringId
	    )
			 INCLUDE(InsertedTime, InsertedDateUTC);
	    --Last 10 actual record counts per each second interval (shows spikes and flow rate of data)
	    SELECT TOP 11
			 InsertedTime
		    , COUNT(*) AS      recordsPerMin
		    , COUNT(*) / 60 AS avgRecordsPerSecond
	    FROM
		    #nsd
	    GROUP BY
			   InsertedTime
	    ORDER BY
			   InsertedTime DESC;


END 
GO
