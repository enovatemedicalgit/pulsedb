SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [Utility].[prcBackUpPulse]
    @strBackupPath AS NVARCHAR(255) = 'F:\EMPRDSQL1\pulse\FULL\PulseFull',@strBackupPath2 AS NVARCHAR(255) = 'H:\'
AS
    BEGIN
 
SET @strBackupPath = @strBackupPath + FORMAT(CURRENT_TIMESTAMP, 'yyyy.MM.dd') + '.RESTOREME';
SET @strBackupPath2 = @strBackupPath2 + FORMAT(CURRENT_TIMESTAMP, 'yyyy.MM.dd') + '.RESTOREME';
BACKUP DATABASE Pulse TO DISK = @strBackupPath WITH INIT, CHECKSUM;
BACKUP DATABASE PulseHistorical TO DISK = @strBackupPath2 WITH INIT, CHECKSUM;
    END;


GO
