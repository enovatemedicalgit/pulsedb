SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spBatteryVarianceReport]
AS
    BEGIN

        DECLARE @FromAddress VARCHAR(150);
        DECLARE @FromName VARCHAR(150);
        DECLARE @body_message VARCHAR(MAX);
	
        DECLARE @tmp TABLE
            (
              id INT IDENTITY ,
              hosiptal VARCHAR(100) ,
              SiteID VARCHAR(20) ,
              SerialNo VARCHAR(30) ,
              V_Diff INT ,
              CreatedDateUTC VARCHAR(50)
            );

        INSERT  INTO @tmp
                SELECT  bb.Hospital ,
                        bb.SiteID ,
                        bb.SerialNo ,
                        MAX(bb.V_Diff) AS 'Max Voltage Variance (mV)' ,
                        bb.CreatedDateUTC
		--,ShipDate,
		--CycleCount,FullChargeCapacity
                FROM    ( SELECT    f.Hospital ,
                                    f.SiteID ,
                                    f.SerialNo ,
                                    f.V_Diff ,
                                    aa.CreatedDateUTC
		--aa.CycleCount,aa.FullChargeCapacity,k.ShipDate
                          FROM      ( SELECT   DISTINCT
                                                d.SiteDescription AS 'Hospital' ,
                                                b.SerialNo ,
                                                b.SiteID ,
                                                ISNULL(a.PONumber, '') AS 'PONumber' ,
                                                j.V_Diff ,
                                                j.row_id_sd
                                      FROM      ( SELECT    c.BatterySerialNumber ,
                                                            MAX(c.V_Diff) AS V_Diff ,
                                                            c.ROW_ID AS row_id_sd
                                                  FROM      ( SELECT
                                                              *
                                                              FROM
                                                              ( SELECT
                                                              BatterySerialNumber ,
                                                              CreatedDateUTC ,
                                                              VoltageCell1 ,
                                                              VoltageCell2 ,
                                                              VoltageCell3 ,
                                                              dbo.fnMinValues(VoltageCell1,
                                                              VoltageCell2,
                                                              VoltageCell3) AS MinVal ,
                                                              dbo.fnMaxValues(VoltageCell1,
                                                              VoltageCell2,
                                                              VoltageCell3) AS MaxVal ,
                                                              dbo.fnMaxValues(VoltageCell1,
                                                              VoltageCell2,
                                                              VoltageCell3)
                                                              - dbo.fnMinValues(VoltageCell1,
                                                              VoltageCell2,
                                                              VoltageCell3) AS V_Diff ,
                                                              ROW_ID
                                                              FROM
                                                              dbo.viewAllSessionData
                                                              WHERE
                                                              BatterySerialNumber NOT IN ( ( SELECT
                                                              CAST(BatterySerialNumber AS VARCHAR(50)) AS SerialNumber
                                                              FROM
                                                              dbo.CheckedBatteries
                                                              )
                                                              )
                                                              AND BatterySerialNumber NOT IN ( ( SELECT
                                                              CAST(SerialNumber AS VARCHAR(50)) AS SerialNumber
                                                              FROM
                                                              dbo.DisabledBatteryList
                                                              )
                                                              )
                                                              AND VoltageCell1 <> 0
                                                              AND VoltageCell2 <> 0
                                                              AND VoltageCell3 <> 0
                                                              AND ( VoltageCell1
                                                              + VoltageCell2
                                                              + VoltageCell3 = Voltage )
                                                              AND CreatedDateUTC BETWEEN DATEADD(d,
                                                              -3, GETUTCDATE())
                                                              AND
                                                              GETUTCDATE()
                                                              ) a
                                                              WHERE
                                                              a.V_Diff BETWEEN 150 AND 3000
                                                              AND ( a.MinVal <> 0
                                                              AND a.MaxVal <> 0
                                                              )
                                                              AND ( a.BatterySerialNumber NOT LIKE '%3116605%' )
	--	and (BatterySerialNumber like '%8913%' or BatterySerialNumber  like '%8713%' or BatterySerialNumber  like '%9013%')
                                                            ) c
                                                  GROUP BY  c.BatterySerialNumber ,
                                                            c.ROW_ID
                                                ) j
                                                JOIN dbo.Assets b ON j.BatterySerialNumber = b.SerialNo
                                                JOIN dbo.Sites d ON b.SiteID = d.IDSite
                                                LEFT JOIN dbo.Warranty a ON a.SerialNumber = b.SerialNo
                                    ) f
                                    JOIN dbo.viewAllSessionData aa ON f.row_id_sd = aa.ROW_ID
                          GROUP BY  f.Hospital ,
                                    f.SerialNo ,
                                    f.V_Diff ,
                                    f.SiteID ,
                                    aa.CreatedDateUTC
		--,aa.CycleCount,aa.FullChargeCapacity,k.ShipDate
                        ) bb
                GROUP BY bb.Hospital ,
                        bb.SerialNo ,
                        bb.SiteID ,
                        bb.CreatedDateUTC
		--,CycleCount,FullChargeCapacity
		
		--,ShipDate
                ORDER BY bb.Hospital ,
                        [Max Voltage Variance (mV)] DESC;
	
    END;
	



    SELECT  *
    INTO    #tmp2
    FROM    ( SELECT    MAX(SerialNo) AS SerialNo ,
                        MAX(SiteID) AS HospitalID ,
                        MAX(hosiptal) AS Hospital ,
                        MAX(CreatedDateUTC) AS ApproximateDateOfImbalance ,
                        MAX(V_Diff) AS CellDifference
              FROM      @tmp
              GROUP BY  SerialNo
            ) AS asdf;

    DECLARE @HTML1 NVARCHAR(MAX);
    DECLARE @HTML2 NVARCHAR(MAX);
    EXEC dbo.CustomTable2HTMLv3 '#tmp2', @HTML1 OUTPUT, 'class="horizontal"',
        0;


    EXEC dbo.usp_SendTextEmail 'enovatemedical-com.mail.protection.outlook.com',
        'Pulse@EnovateMedical.com', 'arlow.farrell@enovatemedical.com',
        'Battery Variance Report', @HTML1, '', '', 0;

    EXEC dbo.usp_SendTextEmail 'enovatemedical-com.mail.protection.outlook.com',
        'Pulse@EnovateMedical.com', 'Gordon.Waid@enovatemedical.com',
        ' Battery Variance Report', @HTML1, '', '', 0;


    INSERT  INTO dbo.CheckedBatteries
            ( BatterySerialNumber
            )
            SELECT  SerialNo AS BatterySerialNumber
            FROM    #tmp2;




    SELECT  @HTML1;
-- SIMPLE VARIANCE REPORT BELOW		
--select * from (select * from (	select BatterySerialNumber,CreatedDateUTC,CycleCount,ChargeLevel,
--		VoltageCell1,VoltageCell2,VoltageCell3,
--		dbo.fnMinValues(VoltageCell1,VoltageCell2,VoltageCell3) as MinVal, 
--		dbo.fnMaxValues(VoltageCell1,VoltageCell2,VoltageCell3) as MaxVal,
--		  dbo.fnMaxValues(VoltageCell1,VoltageCell2,VoltageCell3) - dbo.fnMinValues(VoltageCell1,VoltageCell2,VoltageCell3) as V_Diff,ROW_ID
--		from SessionData   
--		where cyclecount between 10 and 3000 and MaxCycleCount <> 10 and  VoltageCell1 <> 0 and VoltageCell2 <> 0 and VoltageCell3 <> 0 and (VoltageCell1 + VoltageCell2 + VoltageCell3  between  Voltage - (Voltage * 0.05) and Voltage + (Voltage * 0.05)) and CreatedDateUTC between DATEADD(d,-1,GETUTCDATE()) and GETUTCDATE()) a where V_Diff > 150 and V_Diff <3000 and BatterySerialNumber not in (select Cast(BatterySerialNumber  as varchar(50)) as SerialNumber from checkedbatteries))  b order by BatterySerialNumber, CreatedDateUTC

GO
