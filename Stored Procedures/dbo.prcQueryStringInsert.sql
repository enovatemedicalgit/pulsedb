SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcQueryStringInsert]
    @CreatedDateUTC DATETIME = NULL ,
    @DeviceSerialNumber VARCHAR(50) = NULL ,
    @NeedsCaughtUp BIT = NULL ,
    @NineParsed BIT = NULL ,
    @Query VARCHAR(2000) = NULL ,
    @QueryStringBatchID INT ,
    @SourceIPAddress VARCHAR(20) = NULL ,
    @SourceTimestampUTC DATETIME = NULL
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
    BEGIN TRAN;
	
    INSERT  INTO dbo.QueryString
            ( [CreatedDateUTC] ,
              [DeviceSerialNumber] ,
              [NeedsCaughtUp] ,
              [NineParsed] ,
              [Query] ,
              [QueryStringBatchID] ,
              [SourceIPAddress] ,
              [SourceTimestampUTC]
            )
            SELECT  @CreatedDateUTC ,
                    @DeviceSerialNumber ,
                    @NeedsCaughtUp ,
                    @NineParsed ,
                    @Query ,
                    @QueryStringBatchID ,
                    @SourceIPAddress ,
                    @SourceTimestampUTC;
	
	-- Begin Return Select <- do not remove
    SELECT  [ROW_ID] ,
            [CreatedDateUTC] ,
            [DeviceSerialNumber] ,
            [NeedsCaughtUp] ,
            [NineParsed] ,
            [Query] ,
            [QueryStringBatchID] ,
            [SourceIPAddress] ,
            [SourceTimestampUTC]
    FROM    dbo.QueryString
    WHERE   [ROW_ID] = SCOPE_IDENTITY();
	-- End Return Select <- do not remove
               
    COMMIT;



GO
