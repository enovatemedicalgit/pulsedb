SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[spBatteriesOutOfWarrantyCombined]
AS
    BEGIN	
        SELECT  MAX(fullrpt.Account) AS Account ,
                MAX(fullrpt.SiteID) AS siteid ,
                fullrpt.SerialNo ,
                COUNT(fullrpt.SerialNo) AS cnt ,
                CAST(MAX(fullrpt.[Opportunity Type]) AS VARCHAR(30)) AS 'Opportunity Type' ,
                MAX(fullrpt.[Warranty Years]) AS 'Warranty Years' ,
                MAX(fullrpt.[Expiration Date]) AS 'Expiration Date' ,
                MAX(fullrpt.CycleCount) AS 'CycleCount' ,
                MAX(fullrpt.MaxCycleCount) AS 'MaxCycleCount' ,
                MAX(fullrpt.[Capacity Health]) AS 'Capacity Health %' ,
                MAX(fullrpt.FullChargeCapacity) AS 'FullChargeCapacity' ,
                MAX(fullrpt.LastPostDateUTC) AS 'LastPostDateUTC'
        INTO    #tfullrpt
        FROM    ( SELECT    ISNULL(s.SiteName, cus.CustomerName) AS Account ,
                            ass.SiteID ,
                            ass.SerialNo ,
                            'Past Warranty Date' AS 'Opportunity Type' ,
                            wd.DurationYears AS 'Warranty Years' ,
                            CONVERT(DATE, DATEADD(DAY,
                                                  ISNULL(( SELECT
                                                              DurationDays
                                                           FROM
                                                              dbo.WarrantyDuration
                                                           WHERE
                                                              SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
                                                         ), 5000),
                                                  CAST('20'
                                                  + SUBSTRING(ass.SerialNo, 8,
                                                              2) + '-'
                                                  + SUBSTRING(ass.SerialNo, 10,
                                                              2) + '-01' AS DATETIME))) AS 'Expiration Date' ,
                            ass.CycleCount ,
                            ass.MaxCycleCount ,
                            ISNULL(NULLIF(dbo.fnGetBatteryCapacity_Int(ass.SerialNo,
                                                              ass.FullChargeCapacity),
                                          0), 0) AS 'Capacity Health' ,
                            ass.FullChargeCapacity ,
                            ass.LastPostDateUTC
                  FROM      dbo.Assets ass
                            LEFT JOIN dbo.Sites s ON s.IDSite = ass.SiteID
                            LEFT JOIN dbo.Customers cus ON ass.SiteID = cus.IDCustomer
                            LEFT JOIN dbo.WarrantyDuration wd ON wd.SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
                  WHERE     ass.LastPostDateUTC > DATEADD(DAY, -30, GETDATE()) 
                            AND ass.IDAssetType = 5
                            AND GETDATE() > DATEADD(DAY,
                                                    ISNULL(( SELECT
                                                              DurationDays
                                                             FROM
                                                              dbo.WarrantyDuration
                                                             WHERE
                                                              SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
                                                           ), 5000),
                                                    CAST('20'
                                                    + SUBSTRING(ass.SerialNo,
                                                              8, 2) + '-'
                                                    + SUBSTRING(ass.SerialNo,
                                                              10, 2)
                                                    + '-01 12:00:00.000' AS DATETIME))
                            AND dbo.SerialNumberClassification(ass.SerialNo) = 1
                            AND ass.MaxCycleCount <> 0
                            AND ass.MaxCycleCount <> 10
                            AND ass.SiteID NOT IN ( 4, 28, 1125 )
                            AND ass.FullChargeCapacity > 0
                            AND ass.Retired = 0
                  UNION ALL
                  SELECT    ISNULL(s.SiteDescription, cus.CustomerName) AS Account ,
                            s.IDSite AS siteid ,
                            ass.SerialNo ,
                            'Past Warranty Cycle Count' AS 'Opportunity Type' ,
                            wd.DurationYears AS 'Warranty Years' ,
                            CONVERT(DATE, DATEADD(DAY,
                                                  ISNULL(( SELECT
                                                              DurationDays
                                                           FROM
                                                              dbo.WarrantyDuration
                                                           WHERE
                                                              SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
                                                         ), 5000),
                                                  CAST('20'
                                                  + SUBSTRING(ass.SerialNo, 8,
                                                              2) + '-'
                                                  + SUBSTRING(ass.SerialNo, 10,
                                                              2) + '-01' AS DATETIME))) AS 'Expiration Date' ,
                            ass.CycleCount ,
                            ass.MaxCycleCount ,
                            ISNULL(NULLIF(dbo.fnGetBatteryCapacity_Int(ass.SerialNo,
                                                              ass.FullChargeCapacity),
                                          0), 0) AS 'Capacity Health' ,
                            ass.FullChargeCapacity ,
                            ass.LastPostDateUTC
                  FROM      dbo.Assets ass
                            LEFT JOIN dbo.Sites s ON s.IDSite = ass.SiteID
                            LEFT JOIN dbo.Customers cus ON ass.SiteID = cus.IDCustomer
                            LEFT JOIN dbo.WarrantyDuration wd ON wd.SerialNumberPrefix = LEFT(ass.SerialNo,
                                                              7)
                  WHERE     ass.IDAssetType = 5
                            AND ass.CycleCount > ass.MaxCycleCount
                            AND ass.MaxCycleCount != 0
                            AND ass.MaxCycleCount IS NOT NULL
                            AND ass.MaxCycleCount <> 0
                            AND ass.MaxCycleCount <> 10
                            AND ass.FullChargeCapacity <> 22000
                            AND ass.CycleCount <> 0
                            AND ass.CycleCount IS NOT NULL
                            AND dbo.SerialNumberClassification(ass.SerialNo) = 1
                            AND ass.FullChargeCapacity > 0
                            AND ass.Retired = 0
                            AND s.IDSite NOT IN ( 1125, 28, 4 ) --order by s.SiteName, MaxCycleCount desc	
                ) fullrpt
        GROUP BY fullrpt.SerialNo; -- order by fullrpt.Account asc,SerialNo desc 

        UPDATE  #tfullrpt
        SET     [Opportunity Type] = 'Past Cycle Count/Warranty Date'
        WHERE   cnt > 1;
        SELECT  Account ,
                siteid ,
                SerialNo AS 'SerialNumber' ,
                [Opportunity Type] ,
                [Warranty Years] ,
                [Expiration Date] ,
                CycleCount ,
                MaxCycleCount ,
                [Capacity Health %] ,
                FullChargeCapacity ,
                LastPostDateUTC AS 'Last Reported To Pulse' ,
                ( SELECT    COUNT(IDAsset)
                  FROM      dbo.Assets
                  WHERE     Retired = 0
                            AND IDAssetType = 5
                            AND SiteID = ( SELECT   SiteID
                                           FROM     dbo.Assets
                                           WHERE    SerialNo = #tfullrpt.SerialNo
                                         )
                ) AS 'Total Batteries at Facility' ,
                CAST(CAST(FullChargeCapacity AS DECIMAL(18, 2))
                / CAST(2400.00 AS DECIMAL(18, 2)) * 0.85 AS DECIMAL(18, 2)) AS 'Estimated Run Hours' ,
                ISNULL(( SELECT TOP 1
                                CustomerAcct
                         FROM   dbo.SFCustomer
                         WHERE  SiteID = ( SELECT   SiteID
                                           FROM     dbo.Assets
                                           WHERE    SerialNo = #tfullrpt.SerialNo
                                         )
                       ), ( 'NA' )) AS 'SF Customer Acct #' ,
                ISNULL(( 'http://ipaddress.is/'
                         + ( SELECT TOP 1
                                    SourceIPAddress
                             FROM   dbo.Assets
                             WHERE  SiteID = ( SELECT   SiteID
                                               FROM     dbo.Assets
                                               WHERE    SerialNo = #tfullrpt.SerialNo
                                             )
                                    AND SourceIPAddress IS NOT NULL
                           ) ),
                       ( 'http://ipaddress.is/'
                         + ( SELECT TOP 1
                                    ( SourceIPAddressBase + '222' )
                             FROM   dbo.SiteIPSubnet
                             WHERE  SiteID = ( SELECT TOP 1
                                                        SiteID
                                               FROM     dbo.Assets
                                               WHERE    SerialNo = #tfullrpt.SerialNo
                                             )
                           ) )) AS 'Geographical Info Link (St,Zip,Area)'
        FROM    #tfullrpt WHERE [Capacity Health %] > 60 AND [Capacity Health %] < 80
        ORDER BY Account;
--     ;WITH CTE AS
--(
--    SELECT  *,
--            RN = ROW_NUMBER() OVER( PARTITION BY SerialNo
--                                    ORDER BY Account desc)
--  FROM #tfullrpt 
--)  
 
    END;



GO
