SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================

-- Description:	Populates the table
--              SiteIPSubnet which is
--              used for resolving business unit
--              ids for a given SourceIPAddress
-- =============================================
CREATE PROCEDURE [dbo].[spTestSiteIPDiscover] @IDSite INT = NULL
AS
    BEGIN
        SET NOCOUNT ON;




        IF OBJECT_ID(N'tempdb..#a', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #a;
            END;
        IF OBJECT_ID(N'tempdb..#DeviceIPDetail', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #DeviceIPDetail;
            END;
        IF OBJECT_ID(N'tempdb..#pbuIPBaseCounts', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #pbuIPBaseCounts;
            END;
        IF OBJECT_ID(N'tempdb..#buIPBaseCounts', N'U') IS NOT NULL
            BEGIN
                DROP TABLE #buIPBaseCounts;
            END;


	-- grab data from NonSessionData
        SELECT  d.SerialNo ,
                d.IP ,
                d.SourceIPAddress ,
                d.SiteID ,
                bu.CustomerID
        INTO    #a
        FROM    dbo.Assets d
                LEFT JOIN dbo.Sites bu ON d.SiteID = bu.IDSite
        WHERE   d.LastPostDateUTC > DATEADD(DAY, -30, GETUTCDATE())
                AND d.SourceIPAddress IS NOT NULL
                AND d.IDAssetType IN ( 1, 2, 6 )
                AND d.SiteID = @IDSite;

        SELECT  SerialNo ,
                IP ,
                SourceIPAddress ,
                SiteID ,
                CustomerID ,
                LEFT(IP, LEN(IP) - CHARINDEX('.', REVERSE(IP))) AS LANIPAddressBase ,
                LEFT(SourceIPAddress,
                     LEN(SourceIPAddress) - CHARINDEX('.',
                                                      REVERSE(SourceIPAddress))) AS SourceIPAddressBase ,
                LEFT(SourceIPAddress,
                     LEN(SourceIPAddress) - CHARINDEX('.',
                                                      REVERSE(SourceIPAddress))) AS IPBase ,
                'Y' AS Usable -- any rows with more than one ParentBusinessUnit will be updated to 'N' (further below)
        INTO    #DeviceIPDetail
        FROM    #a
        WHERE   SourceIPAddress LIKE '%.%'
                AND SiteID = @IDSite
        ORDER BY SiteID ,
                CustomerID ,
                SerialNo ,
                IP ,
                SourceIPAddress;


        CREATE NONCLUSTERED INDEX IX_DeviceIPDetail ON #DeviceIPDetail (SerialNo, IP, LANIPAddressBase,SourceIPAddressBase, SourceIPAddress, SiteID, CustomerID, IPBase, Usable);

        DELETE  FROM #a
        WHERE   SourceIPAddress LIKE '10.51.%'
                OR SourceIPAddress LIKE '0.0.%';


        DELETE  FROM #DeviceIPDetail
        WHERE   IPBase LIKE '10.51.%'
                OR IPBase LIKE '0.0.%'
                OR CustomerID IN ( 4, 394 ); -- exclude Enovate 


	-- count distinct IPBase values at Customer (IDN) level
	-- this is used for determining whether devices can be moved from one business unit to another (within the same parent business unit)
        SELECT  CustomerID ,
                COUNT(DISTINCT IPBase) AS IPBaseCount_CustomerID ,
                COUNT(DISTINCT LANIPAddressBase) AS LANIPAddressBaseCount_CustomerID
        INTO    #pbuIPBaseCounts
        FROM    #DeviceIPDetail
        GROUP BY CustomerID
        ORDER BY CustomerID;

	-- count distinct IPBase values at SiteID (facility) level
	-- this is used for determining whether devices can be moved from a given business unit
        SELECT  SiteID ,
                COUNT(DISTINCT SourceIPAddressBase) AS IPBaseCount_SiteID ,
                COUNT(DISTINCT LANIPAddressBase) AS LANIPAddressBaseCount_SiteID
        INTO    #buIPBaseCounts
        FROM    #DeviceIPDetail
        GROUP BY SiteID
        ORDER BY SiteID;


	-- if any IPBase is associated with multiple ParentBusinessUnits, do not allow it to be used for automatically assigning the business unit
        UPDATE  #DeviceIPDetail
        SET     [Usable] = 'N'
        WHERE   LANIPAddressBase IN ( SELECT    LANIPAddressBase
                                      FROM      #DeviceIPDetail
                                      GROUP BY  SourceIPAddressBase ,
                                                LANIPAddressBase
                                      HAVING    COUNT(DISTINCT CustomerID) > 1 )
                AND SourceIPAddressBase IN (
                SELECT  SourceIPAddressBase
                FROM    #DeviceIPDetail
                GROUP BY SourceIPAddressBase ,
                        LANIPAddressBase
                HAVING  COUNT(DISTINCT CustomerID) > 1 ); 



	

        INSERT  INTO dbo.SiteIPSubnet
                ( CustomerID ,
                  SiteID ,
                  IPBase ,
                  IPBaseTotalDeviceCount ,
                  DeviceCount ,
                  FacilityReliabilityRating ,
                  OnlyIPBaseForHQ ,
                  OnlyIPBaseForFacility ,
                  SourceIPAddressBase ,
                  LANIPAddressBase
                )
                SELECT  a.CustomerID ,
                        a.SiteID ,
                        MAX(a.IPBase) AS IPBase ,
                        b.IPBaseTotalDeviceCount ,
                        COUNT(DISTINCT a.SerialNo) AS DeviceCount ,
                        ( COUNT(DISTINCT a.SerialNo) * 100 )
                        / b.IPBaseTotalDeviceCount AS FacilityReliabilityRating ,
                        CASE WHEN pbu.IPBaseCount_CustomerID = 1 THEN 1
                             ELSE 0
                        END AS OnlyIPBaseForHQ ,
                        CASE WHEN bu.IPBaseCount_SiteID = 1 THEN 1
                             ELSE 0
                        END AS OnlyIPBaseForFacility ,
                        a.SourceIPAddressBase AS SourceIPAddressBase ,
                        a.LANIPAddressBase AS LANIPAddressBase
                FROM    #DeviceIPDetail a
                        JOIN ( SELECT   CustomerID ,
                                        MAX(IPBase) AS IPBase ,
                                        SourceIPAddressBase ,
                                        LANIPAddressBase ,
                                        COUNT(DISTINCT SerialNo) AS IPBaseTotalDeviceCount
                               FROM     #DeviceIPDetail
                               WHERE    CustomerID <> 0
                                        AND Usable = 'Y'
                                        AND SourceIPAddress != '0.0.0'
                               GROUP BY CustomerID ,
                                        SourceIPAddressBase ,
                                        LANIPAddressBase
                             ) b ON a.CustomerID = b.CustomerID
                                    AND a.IPBase = b.IPBase
                                    AND a.LANIPAddressBase = b.LANIPAddressBase
                        JOIN #pbuIPBaseCounts pbu ON a.CustomerID = pbu.CustomerID
                        JOIN #buIPBaseCounts bu ON a.SiteID = bu.SiteID
                WHERE   a.CustomerID <> 0
                        AND a.SiteID = @IDSite
                        AND a.Usable = 'Y'
	--and a.IPBase <> '0.0.0'
                GROUP BY a.CustomerID ,
                        a.SiteID ,
                        a.SourceIPAddressBase ,
                        a.LANIPAddressBase ,
                        b.IPBaseTotalDeviceCount ,
                        CASE WHEN pbu.IPBaseCount_CustomerID = 1 THEN 1
                             ELSE 0
                        END ,
                        CASE WHEN bu.IPBaseCount_SiteID = 1 THEN 1
                             ELSE 0
                        END
                ORDER BY a.CustomerID ,
                        a.SourceIPAddressBase ,
                        a.LANIPAddressBase ,
                        a.SiteID;
	

	/*
		Here is an opportunity to identify mismatched/misassigned devices
		if this writes to some kind of output log or table, then emails the
		results, including the list of devices which are assigned to each
		of these business units.
		The report header might say something like:
		
		"The following devices are reporting from the same (or similar) IP addresses,
		yet the devices are not all assigned to facilities within the same IDN/HQ"
		
	*/
        SELECT  *
        INTO    #tmp2
        FROM    ( SELECT DISTINCT
                            a.IPBase AS DMZWANIPBase ,
                            a.LANIPAddressBase ,
                            'N' AS Usable ,
                            a.CustomerID ,
                            ( SELECT TOP 1
                                        ass.SerialNo
                              FROM      dbo.Assets ass
                              WHERE     ass.SiteID = a.SiteID
                                        AND ass.IP LIKE '%'
                                        + a.LANIPAddressBase + '%'
                                        AND ass.SourceIPAddress LIKE '%'
                                        + a.SourceIPAddressBase + '%'
                            ) AS SerialNo ,
                            a.SiteID ,
                            bu.SiteDescription AS ParentDescription
                  FROM      #DeviceIPDetail a
                            JOIN dbo.Sites bu ON a.SiteID = bu.IDSite
                  WHERE     Usable = 'N'
                  UNION ALL
                  SELECT DISTINCT
                            a.IPBase AS DMZWANIPBase ,
                            a.LANIPAddressBase ,
                            'Y' AS Usable ,
                            a.CustomerID ,
                            ( SELECT TOP 1
                                        ass.SerialNo
                              FROM      dbo.Assets ass
                              WHERE     ass.SiteID = a.SiteID
                                        AND ass.IP LIKE '%'
                                        + a.LANIPAddressBase + '%'
                                        AND ass.SourceIPAddress LIKE '%'
                                        + a.SourceIPAddressBase + '%'
                            ) AS SerialNo ,
                            a.SiteID ,
                            bu.SiteDescription AS ParentDescription
                  FROM      #DeviceIPDetail a
                            JOIN dbo.Sites bu ON a.SiteID = bu.IDSite
                  WHERE     Usable = 'Y'
                ) fullrpt
        ORDER BY fullrpt.Usable ,
                fullrpt.DMZWANIPBase ,
                fullrpt.LANIPAddressBase ,
                fullrpt.CustomerID;

        DECLARE @cntE INT;
        DECLARE @HTML1 NVARCHAR(MAX);
        DECLARE @HTML2 NVARCHAR(MAX);

        EXEC dbo.CustomTable2HTMLv3 '#tmp2', @HTML1 OUTPUT,
            'class="horizontal"', 0;

        SET @cntE = ( SELECT    COUNT(Subject) AS cntE
                      FROM      dbo.EmailQueue
                      WHERE     Subject = 'Subnet Conflict Report'
                    );
        IF ( @cntE < 1 )
            BEGIN
                SET @cntE = ( SELECT    COUNT(Subject) AS cntE
                              FROM      dbo.EmailQueue
                              WHERE     Subject = 'Subnet Conflict Report'
                            );
            END;
        RAISERROR ('Inserted into SiteIPSubnet Table' , 0, 1) WITH NOWAIT;
        SELECT  *
        FROM    #tmp2;
	/* ============================================================================= */
	-- IF YOU USE THIS (BELOW) for a fallout report, BE SURE TO REFERENCE #DeviceIPDetail (WHERE Usable = 'N'), in addition to BusinessUnitIPSubnet
	/* ============================================================================= */
	   
	--select distinct
	--	a.SerialNo
	--	,a.IP as DeviceIP
	--	,bu2.CustomerName as Current_HQ
	--	,bu.SiteDescription as Current_Facility
	--	,bu3.CustomerName as Suggested_HQ
	--	,bu4.SiteDescription as Suggested_Facility
	
	--	,a.SourceIPAddress
	--	,a.BusinessUnitID
	--	,a.ParentBusinessUnit
	--	,b.BusinessUnitID as Suggested_ParentBusinessUnitID
	--	,c.BusinessUnitID as Suggested_BusinessUnitID
	--from #a a join SiteIPSubnet b on a.SourceIPAddress = b.IPBase
	--left join (select * from BusinessUnitIPSubnet where FacilityReliabilityRating >= 90) c on b.BusinessUnitID = c.ParentBusinessUnit and left(a.IP, len(a.IP) - charindex('.', reverse(a.IP))) = c.IPBase
	--left join (select * from SiteIPSubnet where FacilityReliabilityRating >= 80) c on b.SiteID = c.SiteID and left(a.IP, len(a.IP) - charindex('.', reverse(a.IP))) = c.LANIPAddressBase
	--join Sites bu on a.SiteID = bu.IDSite 
	--join Customers bu2 on a.CustomerID = bu2.IDCustomer
	--join Customers bu3 on b.CustomerID = bu3.IDCustomer
	--left join Sites bu4 on c.SiteID = bu4.IDSite
	--where a.CustomerID <> b.CustomerID
	--and a.SerialNo <> ''
	--and c.BusinessUnitID is null
	--order by bu3.CustomerName, bu4.SiteDescription, a.SerialNo










    END;

GO
