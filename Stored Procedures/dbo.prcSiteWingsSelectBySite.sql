SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcSiteWingsSelectBySite] @SiteID INT
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  

    BEGIN TRAN;

    SELECT  IdSiteWing ,
            [Description] ,
            [SiteId] ,
            [DefaultForSite]
    FROM    dbo.SiteWings
    WHERE   ( [SiteId] = @SiteID ); 

    COMMIT;



GO
