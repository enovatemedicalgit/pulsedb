SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcCustomersDelete] @IDCustomer INT
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	
    BEGIN TRAN;

    DELETE  FROM dbo.Customers
    WHERE   [IDCustomer] = @IDCustomer;

    COMMIT;



GO
