SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [Utility].[UnprocessedPacketsBySource] @SiteID INT = 1128
AS
	BEGIN

	    /**************************************************	Unprocessed Packets By Source	    **************************************************/

	    SELECT
			 t.Source
		    , ISNULL(t2.UnProcessed, 0) AS UnProcessed
	    FROM
	    (
		   SELECT
				'qsW' AS Source
		   UNION ALL
		   SELECT
				'qsE'
					) AS t
	    LEFT JOIN
	    (
		   SELECT
				CASE
				    WHEN ISNULL(ip.SourceIPAddress_Left, '0') = '0'
				    THEN 'qsW'
				    ELSE 'qsE'
				END AS      Source
			   , COUNT(*) AS UnProcessed
		   FROM
			   dbo.QueryString AS qs WITH (nolock)
		   LEFT JOIN
			   dbo.PriorityIP AS ip WITH (nolock)
			   ON LEFT(qs.SourceIPAddress, 6) = ip.SourceIPAddress_Left
				 OR LEFT(qs.SourceIPAddress, 8) = ip.SourceIPAddress_Left
		   WHERE  NineParsed = 0
		   GROUP BY
				  CASE
					 WHEN ISNULL(ip.SourceIPAddress_Left, '0') = '0'
					 THEN 'qsW'
					 ELSE 'qsE'
				  END
					) AS t2
	    ON t.Source = t2.Source
	    UNION ALL
	    SELECT
			 j.Source
		    , ISNULL(j2.UnProcessed, 0) AS UnProcessed
	    FROM
	    (
		   SELECT
				'diW' AS Source
		   UNION ALL
		   SELECT
				'diE'
					) AS j
	    LEFT JOIN
	    (
		   SELECT
				CASE
				    WHEN ISNULL(ip.SourceIPAddress_Left, '0') = '0'
				    THEN 'diW'
				    ELSE 'diE'
				END AS      Source
			   , COUNT(*) AS UnProcessed
		   FROM
			   PULSE.dbo.DataImport AS qs WITH (nolock)
		   LEFT JOIN
			   dbo.PriorityIP AS ip WITH (nolock)
			   ON LEFT(qs.SourceIPAddress, 6) = ip.SourceIPAddress_Left
				 OR LEFT(qs.SourceIPAddress, 8) = ip.SourceIPAddress_Left
		   GROUP BY
				  CASE
					 WHEN ISNULL(ip.SourceIPAddress_Left, '0') = '0'
					 THEN 'diW'
					 ELSE 'diE'
				  END
					) AS j2
	    ON j.Source = j2.Source
	    ORDER BY
			   Source;

	   END

GO
