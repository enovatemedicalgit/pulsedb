SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create PROC  [dbo].[usp_SitesUpdate]
    @SiteName VARCHAR(150) = NULL ,
    @SiteDescription VARCHAR(200) = NULL ,
    @CustomerID VARCHAR(5) ,
    @SfCustomerAcctID VARCHAR(20) = NULL ,
    @SfAccountId VARCHAR(20) = NULL ,
    @SfAcctOwnerId VARCHAR(20) = NULL ,
    @IPAddr VARCHAR(50) = NULL ,
    @IsInternal BIT = NULL ,
    @SiteType INT = NULL ,
    @Status VARCHAR(2) = NULL ,
    @StatusReason VARCHAR(4) = NULL ,
    @RegionID INT = NULL,
	@SiteId INT = NULL
AS
    SET NOCOUNT ON; 
 
	SET IDENTITY_INSERT dbo.Sites OFF

    UPDATE sites SET 
          [SiteName] = @SiteName ,
              [SiteDescription] =@SiteName,
              [CustomerID] = CAST(@CustomerID AS INT),
              [SfCustomerAcctID] = @SfCustomerAcctID,
              [SfAccountId]  =    @SfAccountId ,
              [SfAcctOwnerId]=  @SfAcctOwnerId
			  WHERE IDSite = @SiteId

 
     
                 

GO
