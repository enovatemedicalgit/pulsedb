SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Description:	Pulled from device transmission
-- search page in CAST. Grabs detailed session
-- information by sessionID. 
-- =============================================
CREATE PROCEDURE [dbo].[spSessionData_BySessionID_Get] @SessionID INT
AS
    BEGIN
	
        SET NOCOUNT ON;

        SELECT  sd.* ,
                d.Floor ,
                d.Wing ,
                d.Other ,
                bub.BUBStatusDescription ,
                a.ActivityDescription ,
                ap.Description AS AccessPointDescription
        FROM    dbo.SessionDataCurrent sd
                LEFT JOIN dbo.BUBStatus bub ON sd.DeviceType = bub.AssetTypeID
                                           AND sd.BackupBatteryStatus = bub.BUBStatusCode
                                           AND sd.ControlBoardRevision BETWEEN bub.RevisionLow
                                                              AND
                                                              bub.RevisionHigh
                LEFT JOIN dbo.Assets d ON sd.DeviceSerialNumber = d.SerialNo
                LEFT JOIN dbo.vwActivity a ON sd.Activity = a.ActivityCode
                LEFT JOIN dbo.AccessPoint ap ON sd.APMAC = ap.MACAddress
        WHERE   sd.SessionID = @SessionID;

    END;

GO
