SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[prcCommandCodeParameterNamesGet] (@CommandCodeID int, @DeviceTypeID int)
AS
BEGIN
	SET NOCOUNT ON;

	select coalesce(Parameter1FriendlyName, '') as Parameter1FriendlyName
		, coalesce(Parameter0FriendlyName, '') as Parameter0FriendlyName
	from DeviceTypeCommandCode 
	where CommandCodeID = @CommandCodeID
	and DeviceTypeID = @DeviceTypeID

END


GO
