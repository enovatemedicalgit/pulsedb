SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcGetQSPacket] @SerialNo VARCHAR(50)
AS
    SET NOCOUNT ON; 
 
	
    BEGIN 
        SELECT TOP 1
                *
        FROM    dbo.QueryString
        WHERE   DeviceSerialNumber = @SerialNo
                AND CreatedDateUTC > DATEADD(MINUTE, -20, GETDATE())
        ORDER BY CreatedDateUTC DESC;

		IF (@@ROWCOUNT < 1) 
		BEGIN
		 SELECT TOP 1
                *
        FROM    dbo.NonSessionDataCurrent
        WHERE   DeviceSerialNumber = @SerialNo
                AND CreatedDateUTC > DATEADD(MINUTE, -20, GETDATE())
        ORDER BY CreatedDateUTC DESC;
        END
        	IF (@@ROWCOUNT < 1) 
		BEGIN
		 SELECT TOP 1
                serialno,lastpostdateutc AS CreatedDateUTC
        FROM    dbo.Assets 
        WHERE   SerialNo = @SerialNo
                AND LastPostDateUTC > DATEADD(MINUTE, -20, GETDATE())
        ORDER BY LastPostDateUTC DESC;
        end
    END;
	


GO
