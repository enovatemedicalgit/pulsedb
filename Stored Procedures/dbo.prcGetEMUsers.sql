SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:      <ENOVATE\arlow.farrell,ARLOWFARRELL>
-- Create date: <10/13/2016> <8:31 PM>
-- Description: 
--				Enovate (@enovatemedical.com) Employees   
--
-- 
-- =============================================
CREATE PROCEDURE [dbo].[prcGetEMUsers]
AS
BEGIN
SET NOCOUNT ON;

SELECT U.FirstName + ' ' + U.LastName AS UserFL,U.LastLoginDateUTC,ut.Description AS UserLevel FROM dbo.[User] AS U LEFT JOIN dbo.UserType AS UT ON u.UserTypeID=ut.IDUserType WHERE U.UserName LIKE '%enovatemedical%' ORDER BY ut.IDUserType desc,U.LastLoginDateUTC DESC

END;


GO
