SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spInsertAccessPoint]
    @SiteID VARCHAR(255) ,
    @MacAddr VARCHAR(50) ,
    @Dept INT = 0 ,
    @Floor VARCHAR(255) = '' ,
    @Description VARCHAR(255) = ''
AS
    BEGIN
         -- SET NOCOUNT ON added to prevent extra result sets from
         -- interfering with SELECT statements.
        SET NOCOUNT ON;
         -- mitigate the avglinkquality for now
    
        DECLARE @rownum INT;

	   
        DECLARE @dept_id VARCHAR(50);
        SET @dept_id = ISNULL(@dept_id, '');

        IF NOT EXISTS ( SELECT  SiteID
                        FROM    dbo.AccessPoint
                        WHERE   SiteID = @SiteID
                                AND MACAddress = @MacAddr )
            BEGIN
                DECLARE @defaultDept INT= 0;
                SELECT  @defaultDept = IDDepartment
                FROM    dbo.Departments
                WHERE   DefaultForSite = 1
                        AND SiteID = @SiteID;
                IF @Dept <> 0
                    SET @defaultDept = @Dept;
                DECLARE @devCount INT= 0;
                SELECT  @devCount = COUNT(IDAsset) / 4
                FROM    dbo.Assets
                WHERE   SiteID = @SiteID;

                INSERT  INTO dbo.AccessPoint
                        ( SiteID ,
                          MACAddress ,
                          DepartmentID ,
                          CreatedDateUTC ,
                          ModifiedDateUTC ,
                          [Floor] ,
                          [Description] 
                     
				        )
                VALUES  ( @SiteID ,
                          @MacAddr ,
                          @defaultDept ,
                          GETDATE() ,
                          GETDATE() ,
                          @Floor ,
                          @Description 
                          
				        );

			 
                INSERT  INTO dbo.ErrorLog
                        ( Description ,
                          [Source] ,
                          CreatedDateUTC
                        )
                VALUES  ( 'Inserted new access point for SiteID:' + @SiteID
                          + ' Mac=' + @MacAddr + ' deptid=' + @dept_id ,
                          'Webapi' ,
                          GETDATE()
                        );
            END;
        ELSE
            BEGIN
                SELECT TOP 1
                        @rownum = ROW_ID
                FROM    dbo.AccessPoint
                WHERE   SiteID = @SiteID
                        AND MACAddress = @MacAddr
                ORDER BY AvgLinkQuality DESC;
                IF @Dept <> 0
                    BEGIN
                        UPDATE  dbo.AccessPoint
                        SET     Manual = 1 ,
                                SiteID = @SiteID ,
                                AvgLinkQuality = AvgLinkQuality ,
                                DepartmentID = @defaultDept ,
                                [Floor] = @Floor ,
                                ModifiedDateUTC = GETDATE() ,
                                CreatedDateUTC = ISNULL(CreatedDateUTC,
                                                        GETDATE()) ,
                                [Description] = @Description
                        WHERE   MACAddress = @MacAddr
                                AND ROW_ID = @rownum;
                    END;
                ELSE
                    BEGIN
                        UPDATE  dbo.AccessPoint
                        SET     Manual = 1 ,
                                SiteID = @SiteID ,
                                ModifiedDateUTC = GETDATE() ,
                                CreatedDateUTC = ISNULL(CreatedDateUTC,
                                                        GETDATE()) ,
                                [Description] = @Description
                        WHERE   MACAddress = @MacAddr
                                AND ROW_ID = @rownum;
                    END;
                INSERT  INTO dbo.ErrorLog
                        ( Description ,
                          [Source] ,
                          CreatedDateUTC
                        )
                VALUES  ( 'Updated access point for Siteid:' + @SiteID
                          + ' Mac=' + @MacAddr + ' deptid=' + @dept_id ,
                          'Webapi' ,
                          GETDATE()
                        );
            END;
    END;
GO
