SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROC [dbo].[prcGetDisabledBatteries]
AS
    SET NOCOUNT ON; 
    SET XACT_ABORT ON;  
	

    BEGIN 
        SELECT TOP 500
                dbl.SerialNumber ,
                ass.MaxCycleCount ,
                dbl.CommandSentOn AS CommandSent ,
                dbl.Bay ,
                dbl.DeviceSerial AS ChargerSerial ,
                CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, ass.LastPostDateUTC),
                                               DATENAME(TzOffset,
                                                        SYSDATETIMEOFFSET()))) AS 'Last Communicated' ,
                bu.SiteDescription ,
                dbl.InsertedDateUTC AS AddedToList ,
                dbl.ManualEntryUser AS AddedBy
        FROM    dbo.DisabledBatteryList dbl
                LEFT JOIN dbo.Assets ass ON dbl.SerialNumber = ass.SerialNo
                LEFT JOIN dbo.Sites bu ON ass.SiteID = bu.IDSite
        WHERE   dbl.ManualEntryUser IS NOT NULL
                AND ass.SiteID NOT IN ( 4, 28 )
                AND ass.LastPostDateUTC > DATEADD(DAY, -30, GETUTCDATE())
        ORDER BY ass.LastPostDateUTC DESC;
    END;
	


GO
