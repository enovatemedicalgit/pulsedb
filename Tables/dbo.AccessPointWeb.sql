CREATE TABLE [dbo].[AccessPointWeb]
(
[APMAC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PoolId] [int] NOT NULL,
[SiteID] [int] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'For Arlow', 'SCHEMA', N'dbo', 'TABLE', N'AccessPointWeb', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AccessPointWeb', 'COLUMN', N'APMAC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AccessPointWeb', 'COLUMN', N'PoolId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to Sites.IdSite', 'SCHEMA', N'dbo', 'TABLE', N'AccessPointWeb', 'COLUMN', N'SiteID'
GO
