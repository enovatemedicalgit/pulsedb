CREATE TABLE [dbo].[BatteryCapacityTrending]
(
[BatterySerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LossPerCycle] [float] NULL,
[CyclesPerDay] [float] NULL
) ON [PRIMARY]
GO
