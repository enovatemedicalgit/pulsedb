CREATE TABLE [dbo].[SiteIPSubnet]
(
[CustomerID] [int] NOT NULL,
[SiteID] [int] NULL,
[IPBase] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IPBaseTotalDeviceCount] [int] NULL,
[DeviceCount] [int] NULL,
[FacilityReliabilityRating] [int] NULL,
[OnlyIPBaseForHQ] [int] NOT NULL,
[OnlyIPBaseForFacility] [int] NOT NULL,
[SourceIPAddressBase] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LANIPAddressBase] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_SiteIPSubnet_SourceIPAddressBase_LANIPAddressBase_FacilityReliabilityRating] ON [dbo].[SiteIPSubnet] ([SourceIPAddressBase], [LANIPAddressBase], [FacilityReliabilityRating]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SiteIPSubnet] ADD CONSTRAINT [FK_SiteIPSubnet_Sites] FOREIGN KEY ([SiteID]) REFERENCES [dbo].[Sites] ([IDSite])
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteIPSubnet', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteIPSubnet', 'COLUMN', N'CustomerID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteIPSubnet', 'COLUMN', N'DeviceCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteIPSubnet', 'COLUMN', N'FacilityReliabilityRating'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteIPSubnet', 'COLUMN', N'IPBase'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteIPSubnet', 'COLUMN', N'IPBaseTotalDeviceCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteIPSubnet', 'COLUMN', N'LANIPAddressBase'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteIPSubnet', 'COLUMN', N'OnlyIPBaseForFacility'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteIPSubnet', 'COLUMN', N'OnlyIPBaseForHQ'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'SiteIPSubnet', 'COLUMN', N'SiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteIPSubnet', 'COLUMN', N'SourceIPAddressBase'
GO
