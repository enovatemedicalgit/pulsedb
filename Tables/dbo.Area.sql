CREATE TABLE [dbo].[Area]
(
[IDArea] [int] NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Beacon1Major] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Beacon1Minor] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Beacon1BatteryLVL] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Beacon2Major] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Beacon2Minor] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Beacon2BatteryLVL] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IDSite] [int] NOT NULL,
[IDDepartment] [int] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Descriptors table for home office sites', 'SCHEMA', N'dbo', 'TABLE', N'Area', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Area', 'COLUMN', N'Beacon1BatteryLVL'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Area', 'COLUMN', N'Beacon1Major'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Area', 'COLUMN', N'Beacon1Minor'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Area', 'COLUMN', N'Beacon2BatteryLVL'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Area', 'COLUMN', N'Beacon2Major'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Area', 'COLUMN', N'Beacon2Minor'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Area', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Area', 'COLUMN', N'IDArea'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Area', 'COLUMN', N'IDDepartment'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Area', 'COLUMN', N'IDSite'
GO
