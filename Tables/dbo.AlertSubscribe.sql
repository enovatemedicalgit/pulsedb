CREATE TABLE [dbo].[AlertSubscribe]
(
[AlertSubscribeId] [int] NOT NULL IDENTITY(1, 1),
[Subscribing_DisplayText] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Subscribing_DisplayMoreText] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Notification_DisplayText] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AltProcess] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Active] [bit] NOT NULL CONSTRAINT [DF_AlertSubscribe_Active] DEFAULT ((1)),
[CreatedDateUTC] [datetime] NOT NULL CONSTRAINT [DF_AlertSubscribe_CreatedDateUTC] DEFAULT (getutcdate()),
[CreatedUserIID] [int] NOT NULL,
[ModifiedDateUTC] [datetime] NOT NULL CONSTRAINT [DF_AlertSubscribe_ModifiedDateUTC] DEFAULT (getutcdate()),
[ModifiedUserID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AlertSubscribe] ADD CONSTRAINT [PK_AlertSubscribe] PRIMARY KEY CLUSTERED  ([AlertSubscribeId]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'For Arlow', 'SCHEMA', N'dbo', 'TABLE', N'AlertSubscribe', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertSubscribe', 'COLUMN', N'Active'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertSubscribe', 'COLUMN', N'AlertSubscribeId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Describes any process which is responsible for gen', 'SCHEMA', N'dbo', 'TABLE', N'AlertSubscribe', 'COLUMN', N'AltProcess'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertSubscribe', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertSubscribe', 'COLUMN', N'CreatedUserIID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'AlertSubscribe', 'COLUMN', N'ModifiedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'AlertSubscribe', 'COLUMN', N'ModifiedUserID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertSubscribe', 'COLUMN', N'Notification_DisplayText'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertSubscribe', 'COLUMN', N'Subscribing_DisplayMoreText'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertSubscribe', 'COLUMN', N'Subscribing_DisplayText'
GO
