CREATE TABLE [dbo].[UserSettings]
(
[ROW_ID] [int] NOT NULL,
[User_ROW_ID] [int] NOT NULL,
[SettingKey] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SettingValue] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserSettings] ADD CONSTRAINT [PK_UserSettings] PRIMARY KEY CLUSTERED  ([ROW_ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserSettings] ADD CONSTRAINT [FK_UserSettings_User] FOREIGN KEY ([User_ROW_ID]) REFERENCES [dbo].[User] ([IDUser])
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'UserSettings', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'UserSettings', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'UserSettings', 'COLUMN', N'SettingKey'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'UserSettings', 'COLUMN', N'SettingValue'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'UserSettings', 'COLUMN', N'User_ROW_ID'
GO
