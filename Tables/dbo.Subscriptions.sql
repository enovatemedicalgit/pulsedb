CREATE TABLE [dbo].[Subscriptions]
(
[SubscriptionKey] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserID] [int] NOT NULL
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Subscriptions] ON [dbo].[Subscriptions] ([SubscriptionKey], [UserID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Subscriptions', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Subscriptions', 'COLUMN', N'SubscriptionKey'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Subscriptions', 'COLUMN', N'UserID'
GO
