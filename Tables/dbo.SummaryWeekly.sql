CREATE TABLE [dbo].[SummaryWeekly]
(
[ROW_ID] [int] NOT NULL IDENTITY(1, 1),
[Date] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HQID] [int] NOT NULL,
[SiteID] [int] NOT NULL,
[DepartmentID] [int] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RowType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActiveWorkstationCount] [int] NOT NULL,
[AvailableWorkstationCount] [int] NOT NULL,
[OfflineWorkstationCount] [int] NOT NULL,
[UtilizationTW] [decimal] (6, 4) NULL,
[UtilizationLW] [decimal] (6, 4) NULL,
[RunRateTW] [int] NOT NULL,
[RunRateLW] [int] NOT NULL,
[AvgAmpDrawTW] [decimal] (18, 2) NULL,
[AvgAmpDrawLW] [decimal] (18, 2) NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWeekly', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'The number of workstations that have the active flag set to true', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWeekly', 'COLUMN', N'ActiveWorkstationCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of workstations that have sent a session packet in the last 7 days but not more recently than 2 hours ago.', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWeekly', 'COLUMN', N'AvailableWorkstationCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Summary row for AvgAmpDraw last week', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWeekly', 'COLUMN', N'AvgAmpDrawLW'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Summary row for AvgAmpDraw this week', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWeekly', 'COLUMN', N'AvgAmpDrawTW'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The day part of the week ending date', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWeekly', 'COLUMN', N'Date'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to the Department table', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWeekly', 'COLUMN', N'DepartmentID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWeekly', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Identifier of the customers network domain', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWeekly', 'COLUMN', N'HQID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of workstations that have not communicated in the last 7 days', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWeekly', 'COLUMN', N'OfflineWorkstationCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWeekly', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWeekly', 'COLUMN', N'RowType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Summary row for RunRate last week', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWeekly', 'COLUMN', N'RunRateLW'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Summary row for RunRate this week', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWeekly', 'COLUMN', N'RunRateTW'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to the Site table', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWeekly', 'COLUMN', N'SiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Summary row for Utilization last week', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWeekly', 'COLUMN', N'UtilizationLW'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Summary row for Utilization this week', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWeekly', 'COLUMN', N'UtilizationTW'
GO
