CREATE TABLE [dbo].[SummarySiteAssets]
(
[CreatedDateUTC] [datetime] NOT NULL CONSTRAINT [DF_SummarySiteAssets_CreatedDateUTC] DEFAULT (getutcdate()),
[IDCustomer] [int] NULL,
[IDSite] [int] NULL,
[IDDepartment] [int] NULL CONSTRAINT [DF_SummarySiteAssets_IDDepartment] DEFAULT ((0)),
[WorkstationCount] [int] NULL CONSTRAINT [DF_SummarySiteAssets_WorkstationCount] DEFAULT ((0)),
[ChargerCount] [int] NULL CONSTRAINT [DF_SummarySiteAssets_ChargerCount] DEFAULT ((0)),
[BatteryCount] [int] NULL CONSTRAINT [DF_SummarySiteAssets_BatteryCount] DEFAULT ((0)),
[ActiveWorkstationCount] [int] NULL CONSTRAINT [DF_SummarySiteAssets_ActiveWorkstationCount] DEFAULT ((0)),
[ActiveChargerCount] [int] NULL CONSTRAINT [DF_SummarySiteAssets_ActiveChargerCount] DEFAULT ((0)),
[ActiveBatteryCount] [int] NULL CONSTRAINT [DF_SummarySiteAssets_ActiveBatteryCount] DEFAULT ((0)),
[DormantBatteryCount] [int] NULL CONSTRAINT [DF_SummarySiteAssets_DormantBatteryC0ount] DEFAULT ((0)),
[CountFullBatteries] [int] NULL CONSTRAINT [DF_SummarySiteAssets_CountFullBatteries] DEFAULT ((0)),
[BatteryInWorkstationCount] [int] NULL CONSTRAINT [DF_SummarySiteAssets_BatteryInWorkstationCount] DEFAULT ((0)),
[BatteryInChargerCount] [int] NULL CONSTRAINT [DF_SummarySiteAssets_BatteryInChargerCount] DEFAULT ((0)),
[VacantChargerBays] [int] NULL CONSTRAINT [DF_SummarySiteAssets_VacantChargerBays] DEFAULT ((0)),
[ActiveChargerBays] [int] NULL CONSTRAINT [DF_SummarySiteAssets_ActiveChargerBays] DEFAULT ((0)),
[InServiceWorkstationCount] [int] NULL CONSTRAINT [DF_SummarySiteAssets_InServiceWorkstationCount] DEFAULT ((0)),
[AvailableWorkstationCount] [int] NULL CONSTRAINT [DF_SummarySiteAssets_AvailableWorkstationCount] DEFAULT ((0)),
[DormantWorkstationCount] [int] NULL CONSTRAINT [DF_SummarySiteAssets_DormantWorkstationCount] DEFAULT ((0)),
[BayCount] [int] NULL CONSTRAINT [DF_SummarySiteAssets_BayCount] DEFAULT ((0)),
[OfflineChargerBays] [int] NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteAssets', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of batteries with the active flag set to true', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteAssets', 'COLUMN', N'ActiveBatteryCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of charger bays with the active flag set to true', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteAssets', 'COLUMN', N'ActiveChargerBays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of chargers with the active flag set to true', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteAssets', 'COLUMN', N'ActiveChargerCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The number of workstations that have the active flag set to true', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteAssets', 'COLUMN', N'ActiveWorkstationCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of workstations that have sent a session packet in the last 7 days but not more recently than 2 hours ago.', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteAssets', 'COLUMN', N'AvailableWorkstationCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Total number of batteries', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteAssets', 'COLUMN', N'BatteryCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of batteries in chargers', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteAssets', 'COLUMN', N'BatteryInChargerCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of batteries in workstations', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteAssets', 'COLUMN', N'BatteryInWorkstationCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of bays', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteAssets', 'COLUMN', N'BayCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Total number of chargers', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteAssets', 'COLUMN', N'ChargerCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Count of batteries with charge level over 80 percent', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteAssets', 'COLUMN', N'CountFullBatteries'
GO
EXEC sp_addextendedproperty N'MS_Description', N'the report date', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteAssets', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of batteries where the last packet is less than 7 days old but more than 2 days old.', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteAssets', 'COLUMN', N'DormantBatteryCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of workstatioins where the last packet is less than 7 days old but more than 2 days old.', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteAssets', 'COLUMN', N'DormantWorkstationCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to Customer', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteAssets', 'COLUMN', N'IDCustomer'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to Department', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteAssets', 'COLUMN', N'IDDepartment'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to Site', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteAssets', 'COLUMN', N'IDSite'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of workstations that have communicated in the last two days', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteAssets', 'COLUMN', N'InServiceWorkstationCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The number of bays on the device that haven''t communicated a battery packet in the last day', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteAssets', 'COLUMN', N'OfflineChargerBays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of charger bays with no battery in them', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteAssets', 'COLUMN', N'VacantChargerBays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Total count of workstations', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteAssets', 'COLUMN', N'WorkstationCount'
GO
