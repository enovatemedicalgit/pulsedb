CREATE TABLE [dbo].[SyteLineIntegration]
(
[ser_num] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[cust_num] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[cust_seq] [int] NULL,
[DeviceSerialNo] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
