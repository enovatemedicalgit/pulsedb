CREATE TABLE [dbo].[AlertMessage]
(
[CreatedDateUTC] [datetime] NULL,
[DeviceSerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BatterySerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Notes] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StingerNotes] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SiteID] [int] NULL,
[EmailQueueID] [int] NULL,
[AlertConditionID] [int] NOT NULL,
[AlertStatus] [int] NOT NULL,
[QueuedForEmail] [datetime] NULL,
[QueuedForEmailUTC] [datetime] NULL,
[Priority] [int] NOT NULL,
[TablePointer] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RowPointer] [int] NOT NULL,
[ModifiedDateUTC] [datetime] NULL,
[ModifiedUserID] [int] NULL,
[ROW_ID] [int] NOT NULL IDENTITY(1, 1),
[Bay] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[APMAC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Source] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FriendlyDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SFCID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CaseNum] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_AlertMessage_BatterySerialNumber_AlertConditionID_AlertStatus] ON [dbo].[AlertMessage] ([BatterySerialNumber], [AlertConditionID], [AlertStatus]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_AlertMessage_CreatedDateUTC_DeviceSerialNumber_BatterySerialNumber_AlertConditionID] ON [dbo].[AlertMessage] ([CreatedDateUTC]) INCLUDE ([AlertConditionID], [BatterySerialNumber], [DeviceSerialNumber]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_AlertMessage_DeviceSerialNumber_AlertConditionID_AlertStatus] ON [dbo].[AlertMessage] ([DeviceSerialNumber], [AlertConditionID], [AlertStatus]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_AlertMessage_SiteID_Priority_CreatedDateUTC_DeviceSerialNumber_Description] ON [dbo].[AlertMessage] ([SiteID], [Priority]) INCLUDE ([CreatedDateUTC], [Description], [DeviceSerialNumber]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Stores log of alert messages by device by date', 'SCHEMA', N'dbo', 'TABLE', N'AlertMessage', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertMessage', 'COLUMN', N'AlertStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertMessage', 'COLUMN', N'APMAC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Serial Number; Originates from ''bsn'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'AlertMessage', 'COLUMN', N'BatterySerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Bay Number; Originates from ''bay'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'AlertMessage', 'COLUMN', N'Bay'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertMessage', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertMessage', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertMessage', 'COLUMN', N'DeviceSerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertMessage', 'COLUMN', N'FriendlyDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertMessage', 'COLUMN', N'ModifiedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertMessage', 'COLUMN', N'ModifiedUserID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertMessage', 'COLUMN', N'Priority'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertMessage', 'COLUMN', N'QueuedForEmail'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertMessage', 'COLUMN', N'QueuedForEmailUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertMessage', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertMessage', 'COLUMN', N'SFCID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertMessage', 'COLUMN', N'SiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertMessage', 'COLUMN', N'StingerNotes'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertMessage', 'COLUMN', N'TablePointer'
GO
