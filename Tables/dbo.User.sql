CREATE TABLE [dbo].[User]
(
[CreatedDate] [datetime] NOT NULL,
[CreatedDateUTC] [datetime] NOT NULL,
[UserName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IDSite] [int] NULL,
[PrimaryPhone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OtherPhone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UTC_Offset] [int] NULL,
[CreatedUserID] [int] NULL,
[ModifiedDateUTC] [datetime] NULL,
[IDUser] [int] NOT NULL IDENTITY(1, 1),
[LastLoginDateUTC] [datetime] NULL,
[UserTypeID] [int] NULL CONSTRAINT [DF_User_UserTypeID] DEFAULT ((0)),
[RegionID] [int] NULL CONSTRAINT [DF_User_RegionID] DEFAULT ((0)),
[SFAcctOwnerID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProfileImage] [varbinary] (max) NULL,
[SFUserId] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SFUserUrl] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IDSites] [int] NOT NULL CONSTRAINT [DF_User_IDSites] DEFAULT ((0)),
[LoginCount] [int] NULL CONSTRAINT [DF_User_LoginCount] DEFAULT ((1))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[User] ADD CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED  ([IDUser]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20161018-221613] ON [dbo].[User] ([UserName]) INCLUDE ([IDSite], [IDUser]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20161018-221719] ON [dbo].[User] ([UserName], [Password]) INCLUDE ([Email], [FirstName], [IDSite], [IDUser], [LastName], [UserTypeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[User] ADD CONSTRAINT [FK_User_Sites] FOREIGN KEY ([IDSite]) REFERENCES [dbo].[Sites] ([IDSite])
GO
ALTER TABLE [dbo].[User] ADD CONSTRAINT [FK_User_UserType] FOREIGN KEY ([UserTypeID]) REFERENCES [dbo].[UserType] ([IDUserType])
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'User', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'User', 'COLUMN', N'CreatedDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'User', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'User', 'COLUMN', N'CreatedUserID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'User', 'COLUMN', N'Email'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'User', 'COLUMN', N'FirstName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'User', 'COLUMN', N'IDSite'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'User', 'COLUMN', N'IDUser'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'User', 'COLUMN', N'LastLoginDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'User', 'COLUMN', N'LastName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'User', 'COLUMN', N'ModifiedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'User', 'COLUMN', N'Notes'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'User', 'COLUMN', N'OtherPhone'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'User', 'COLUMN', N'Password'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'User', 'COLUMN', N'PrimaryPhone'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'User', 'COLUMN', N'RegionID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'User', 'COLUMN', N'SFAcctOwnerID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'User', 'COLUMN', N'Title'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'User', 'COLUMN', N'UserName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'User', 'COLUMN', N'UserTypeID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'User', 'COLUMN', N'UTC_Offset'
GO
