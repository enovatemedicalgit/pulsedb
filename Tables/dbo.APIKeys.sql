CREATE TABLE [dbo].[APIKeys]
(
[Consumer] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ConsumerKey] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [sha3keyindex] ON [dbo].[APIKeys] ([ConsumerKey]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Security information', 'SCHEMA', N'dbo', 'TABLE', N'APIKeys', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'APIKeys', 'COLUMN', N'Consumer'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'APIKeys', 'COLUMN', N'ConsumerKey'
GO
