CREATE TABLE [dbo].[AssetDetail]
(
[IDAssetDetail] [int] NOT NULL IDENTITY(1, 1),
[DetailName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DetailValue] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DetailDescription] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AssetID] [int] NOT NULL,
[IDAssetType] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AssetDetail] ADD CONSTRAINT [PK_AssetDetail] PRIMARY KEY CLUSTERED  ([IDAssetDetail]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Value pair details tied to assets by AssetId.   **', 'SCHEMA', N'dbo', 'TABLE', N'AssetDetail', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetDetail', 'COLUMN', N'AssetID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetDetail', 'COLUMN', N'DetailDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetDetail', 'COLUMN', N'DetailName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetDetail', 'COLUMN', N'DetailValue'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetDetail', 'COLUMN', N'IDAssetDetail'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetDetail', 'COLUMN', N'IDAssetType'
GO
