CREATE TABLE [staging].[SiteCleanUp]
(
[SiteName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SfAccountId] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Important Notes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SfCustomerAcctID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POCEmail] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActiveDevices] [float] NULL,
[StatusReason] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
