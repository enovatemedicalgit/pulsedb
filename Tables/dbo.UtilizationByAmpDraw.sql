CREATE TABLE [dbo].[UtilizationByAmpDraw]
(
[DeviceSerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDateLocal] [datetime] NULL,
[CurrentBand] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Recency] [bigint] NULL,
[LBound] [datetime] NULL,
[UBound] [datetime] NULL
) ON [PRIMARY]
GO
