CREATE TABLE [dbo].[SiteIP]
(
[SiteID] [int] NULL,
[IP] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Locked] [bit] NOT NULL CONSTRAINT [DF_SiteIP_Locked] DEFAULT ((0)),
[CreatedDateUTC] [datetime] NOT NULL,
[ModifiedDateUTC] [datetime] NOT NULL,
[ModifiedUserID] [int] NULL,
[ROW_ID] [int] NOT NULL IDENTITY(1, 1),
[SourceIPAddressBase] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LanIPAddressBase] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteIP', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteIP', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteIP', 'COLUMN', N'IP'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteIP', 'COLUMN', N'LanIPAddressBase'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteIP', 'COLUMN', N'Locked'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'SiteIP', 'COLUMN', N'ModifiedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'SiteIP', 'COLUMN', N'ModifiedUserID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'SiteIP', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'SiteIP', 'COLUMN', N'SiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteIP', 'COLUMN', N'SourceIPAddressBase'
GO
