CREATE TABLE [dbo].[PriorityIP]
(
[SourceIPAddress_Left] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ROW_ID] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PriorityIP] ADD CONSTRAINT [PK_PriorityIP] PRIMARY KEY CLUSTERED  ([ROW_ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PriorityIP', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'PriorityIP', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PriorityIP', 'COLUMN', N'SourceIPAddress_Left'
GO
