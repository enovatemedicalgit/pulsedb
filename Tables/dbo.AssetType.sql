CREATE TABLE [dbo].[AssetType]
(
[IDAssetType] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Category] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AssetCategoryId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AssetType] ADD CONSTRAINT [PK_AssetType] PRIMARY KEY CLUSTERED  ([IDAssetType]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetType', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetType', 'COLUMN', N'Category'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetType', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetType', 'COLUMN', N'IDAssetType'
GO
