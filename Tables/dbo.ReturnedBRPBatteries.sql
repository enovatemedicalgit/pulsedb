CREATE TABLE [dbo].[ReturnedBRPBatteries]
(
[BatterySerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'ReturnedBRPBatteries', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Serial Number; Originates from ''bsn'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'ReturnedBRPBatteries', 'COLUMN', N'BatterySerialNumber'
GO
