CREATE TABLE [dbo].[AlertCount]
(
[CreatedDate] [datetime] NULL,
[CreatedDateUTC] [datetime] NULL,
[DeviceSerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BatterySerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlertConditionID] [int] NOT NULL,
[Count] [int] NOT NULL,
[ROW_ID] [int] NOT NULL IDENTITY(1, 1),
[AlertCreated] [int] NULL CONSTRAINT [DF_AlertCount_AlertCreated] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AlertCount] ADD CONSTRAINT [PK_AlertCount] PRIMARY KEY CLUSTERED  ([ROW_ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Table that summarizes recent alersts by device', 'SCHEMA', N'dbo', 'TABLE', N'AlertCount', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertCount', 'COLUMN', N'AlertConditionID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Serial Number; Originates from ''bsn'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'AlertCount', 'COLUMN', N'BatterySerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertCount', 'COLUMN', N'Count'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertCount', 'COLUMN', N'CreatedDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertCount', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertCount', 'COLUMN', N'DeviceSerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertCount', 'COLUMN', N'ROW_ID'
GO
