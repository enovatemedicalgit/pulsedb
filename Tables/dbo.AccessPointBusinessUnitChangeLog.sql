CREATE TABLE [dbo].[AccessPointBusinessUnitChangeLog]
(
[AccessPointID] [int] NOT NULL,
[PreviousSiteID] [int] NULL,
[NewSiteID] [int] NULL,
[ModifiedDateUTC] [datetime] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'For Arlow', 'SCHEMA', N'dbo', 'TABLE', N'AccessPointBusinessUnitChangeLog', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AccessPointBusinessUnitChangeLog', 'COLUMN', N'AccessPointID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AccessPointBusinessUnitChangeLog', 'COLUMN', N'ModifiedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AccessPointBusinessUnitChangeLog', 'COLUMN', N'PreviousSiteID'
GO
