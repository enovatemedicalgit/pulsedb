CREATE TABLE [dbo].[UserOptionKeys]
(
[KeyName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[KeyDescription] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DefaultValue] [bit] NOT NULL CONSTRAINT [DF_UserOptionKeys_DefaultValue] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserOptionKeys] ADD CONSTRAINT [PK_UserOptionKeys] PRIMARY KEY CLUSTERED  ([KeyName]) ON [PRIMARY]
GO
