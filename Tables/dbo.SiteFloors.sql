CREATE TABLE [dbo].[SiteFloors]
(
[IdSiteFloor] [int] NOT NULL IDENTITY(1, 1),
[SiteId] [int] NOT NULL,
[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultForSite] [bit] NULL CONSTRAINT [DF_SiteFloors_IsDefault] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SiteFloors] ADD CONSTRAINT [PK_SiteFloors] PRIMARY KEY CLUSTERED  ([IdSiteFloor]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SiteFloors] ADD CONSTRAINT [FK_SiteFloors_Sites] FOREIGN KEY ([SiteId]) REFERENCES [dbo].[Sites] ([IDSite])
GO
