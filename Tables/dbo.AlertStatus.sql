CREATE TABLE [dbo].[AlertStatus]
(
[ROW_ID] [int] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AlertStatus] ADD CONSTRAINT [PK_AlertStatus] PRIMARY KEY CLUSTERED  ([ROW_ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Descriptors of the status of alerts', 'SCHEMA', N'dbo', 'TABLE', N'AlertStatus', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertStatus', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'AlertStatus', 'COLUMN', N'ROW_ID'
GO
