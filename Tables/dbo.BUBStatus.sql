CREATE TABLE [dbo].[BUBStatus]
(
[AssetTypeID] [int] NOT NULL,
[BUBStatusCode] [int] NOT NULL,
[BUBStatusDescription] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RevisionLow] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RevisionHigh] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ROW_ID] [int] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'For Arlow**', 'SCHEMA', N'dbo', 'TABLE', N'BUBStatus', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to AssetTypes', 'SCHEMA', N'dbo', 'TABLE', N'BUBStatus', 'COLUMN', N'AssetTypeID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BUBStatus', 'COLUMN', N'BUBStatusCode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BUBStatus', 'COLUMN', N'BUBStatusDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BUBStatus', 'COLUMN', N'RevisionHigh'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BUBStatus', 'COLUMN', N'RevisionLow'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'BUBStatus', 'COLUMN', N'ROW_ID'
GO
