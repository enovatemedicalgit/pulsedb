CREATE TABLE [dbo].[Subscription]
(
[SubscriptionKey] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Title] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastSentDateUTC] [datetime] NULL,
[LastSentForDate] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContentHeaderHTML] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContentFooterHTML] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Subscription] ADD CONSTRAINT [PK_Subscription] PRIMARY KEY CLUSTERED  ([SubscriptionKey]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Subscription', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Subscription', 'COLUMN', N'ContentFooterHTML'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Subscription', 'COLUMN', N'ContentHeaderHTML'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Subscription', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Subscription', 'COLUMN', N'LastSentDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Subscription', 'COLUMN', N'LastSentForDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'A key value which should never change. Other proce', 'SCHEMA', N'dbo', 'TABLE', N'Subscription', 'COLUMN', N'SubscriptionKey'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Subscription', 'COLUMN', N'Title'
GO
