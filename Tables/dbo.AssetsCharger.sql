CREATE TABLE [dbo].[AssetsCharger]
(
[IDAsset] [int] NOT NULL IDENTITY(100000, 1),
[BayWirelessRevision] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bay1ChargerRevision] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bay2ChargerRevision] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bay3ChargerRevision] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bay4ChargerRevision] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BayCount] [int] NULL,
[Bay1LastPostDateUTC] [datetime] NULL,
[Bay2LastPostDateUTC] [datetime] NULL,
[Bay3LastPostDateUTC] [datetime] NULL,
[Bay4LastPostDateUTC] [datetime] NULL,
[Bay1Amps] [decimal] (18, 2) NULL,
[Bay2Amps] [decimal] (18, 2) NULL,
[Bay3Amps] [decimal] (18, 2) NULL,
[Bay4Amps] [decimal] (18, 2) NULL,
[Bay1Activity] [int] NULL,
[Bay2Activity] [int] NULL,
[Bay3Activity] [int] NULL,
[Bay4Activity] [int] NULL,
[Bay1BSN] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bay2BSN] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bay3BSN] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bay4BSN] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bay1ChargeLevel] [int] NULL,
[Bay2ChargeLevel] [int] NULL,
[Bay3ChargeLevel] [int] NULL,
[Bay4ChargeLevel] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AssetsCharger] ADD CONSTRAINT [pk_AssetsCharger] PRIMARY KEY CLUSTERED  ([IDAsset]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AssetsCharger] ADD CONSTRAINT [fk_AssetsCharger_Assets] FOREIGN KEY ([IDAsset]) REFERENCES [dbo].[tblAssets] ([IDAsset])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Table of devices derived from packet information.', 'SCHEMA', N'dbo', 'TABLE', N'AssetsCharger', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Firmware revision number', 'SCHEMA', N'dbo', 'TABLE', N'AssetsCharger', 'COLUMN', N'Bay1ChargerRevision'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The date of the last insert or removal packet for this bay', 'SCHEMA', N'dbo', 'TABLE', N'AssetsCharger', 'COLUMN', N'Bay1LastPostDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Firmware revision number', 'SCHEMA', N'dbo', 'TABLE', N'AssetsCharger', 'COLUMN', N'Bay2ChargerRevision'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The date of the last insert or removal packet for this bay', 'SCHEMA', N'dbo', 'TABLE', N'AssetsCharger', 'COLUMN', N'Bay2LastPostDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Firmware revision number', 'SCHEMA', N'dbo', 'TABLE', N'AssetsCharger', 'COLUMN', N'Bay3ChargerRevision'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The date of the last insert or removal packet for this bay', 'SCHEMA', N'dbo', 'TABLE', N'AssetsCharger', 'COLUMN', N'Bay3LastPostDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Firmware revision number', 'SCHEMA', N'dbo', 'TABLE', N'AssetsCharger', 'COLUMN', N'Bay4ChargerRevision'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The date of the last insert or removal packet for this bay', 'SCHEMA', N'dbo', 'TABLE', N'AssetsCharger', 'COLUMN', N'Bay4LastPostDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The number of charging bays on the device, if the device is a charger', 'SCHEMA', N'dbo', 'TABLE', N'AssetsCharger', 'COLUMN', N'BayCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Bay Wireless Firmware Revision; Originates from ''bwr'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'AssetsCharger', 'COLUMN', N'BayWirelessRevision'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'AssetsCharger', 'COLUMN', N'IDAsset'
GO
