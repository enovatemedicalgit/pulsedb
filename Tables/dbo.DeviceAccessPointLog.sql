CREATE TABLE [dbo].[DeviceAccessPointLog]
(
[DeviceSerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[APMAC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDateUTC] [datetime] NULL CONSTRAINT [DF_DeviceAccessPointLog_CreatedDateUTC] DEFAULT (getutcdate())
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DeviceAccessPointLog] ON [dbo].[DeviceAccessPointLog] ([DeviceSerialNumber], [APMAC], [CreatedDateUTC]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'DeviceAccessPointLog', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'DeviceAccessPointLog', 'COLUMN', N'APMAC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'DeviceAccessPointLog', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'DeviceAccessPointLog', 'COLUMN', N'DeviceSerialNumber'
GO
