CREATE TABLE [dbo].[WarrantyDuration]
(
[WarrantyDurationID] [int] NOT NULL,
[SerialNumberPrefix] [int] NOT NULL,
[DurationDays] [int] NOT NULL,
[DeviceType] [int] NOT NULL,
[DurationYears] [int] NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'WarrantyDuration', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Equivalent to AssetTypeId. Originates from ''type'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'WarrantyDuration', 'COLUMN', N'DeviceType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'WarrantyDuration', 'COLUMN', N'DurationDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'WarrantyDuration', 'COLUMN', N'DurationYears'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'WarrantyDuration', 'COLUMN', N'SerialNumberPrefix'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'WarrantyDuration', 'COLUMN', N'WarrantyDurationID'
GO
