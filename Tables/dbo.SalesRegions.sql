CREATE TABLE [dbo].[SalesRegions]
(
[IDRegion] [tinyint] NOT NULL,
[Name ] [varchar] (55) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SalesRegions', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SalesRegions', 'COLUMN', N'IDRegion'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SalesRegions', 'COLUMN', N'Name '
GO
