CREATE TABLE [dbo].[SummaryActiveAssets]
(
[SiteID] [int] NULL,
[DepartmentID] [int] NULL,
[ActiveChargerCount] [int] NULL CONSTRAINT [DF_SummaryActiveAssets_ActiveChargerCount] DEFAULT ((0)),
[ActiveWorkstationCount] [int] NULL CONSTRAINT [DF_SummaryActiveAssets_ActiveWorkstationCount] DEFAULT ((0)),
[ActiveBatteryCount] [int] NULL CONSTRAINT [DF_SummaryActiveAssets_ActiveBatteryCount] DEFAULT ((0)),
[CreatedDateUTC] [datetime] NULL,
[ROW_ID] [int] NOT NULL IDENTITY(1, 1),
[DormantBatteryCount] [int] NULL CONSTRAINT [DF_SummaryActiveAssets_DormantBatteryCount] DEFAULT ((0)),
[BatteryInWorkstationCount] [int] NULL CONSTRAINT [DF_SummaryActiveAssets_BatteryInWorkstationCount] DEFAULT ((0)),
[BatteryInChargerCount] [int] NULL CONSTRAINT [DF_SummaryActiveAssets_BatteryInChargerCount] DEFAULT ((0)),
[CountFullBatteries] [int] NULL CONSTRAINT [DF_SummaryActiveAssets_CountFullBatteries] DEFAULT ((0)),
[VacantChargerBays] [int] NULL CONSTRAINT [DF_SummaryActiveAssets_VacantChargerBays] DEFAULT ((0)),
[ActiveChargerBays] [int] NULL CONSTRAINT [DF_SummaryActiveAssets_ActiveChargerBays] DEFAULT ((0)),
[InServiceWorkstationCount] [int] NULL CONSTRAINT [DF_SummaryActiveAssets_InServiceWorkstationCount] DEFAULT ((0)),
[AvailableWorkstationCount] [int] NULL CONSTRAINT [DF_SummaryActiveAssets_AvailableWorkstationCount] DEFAULT ((0)),
[BayCount] [int] NULL CONSTRAINT [DF_SummaryActiveAssets_BayCount] DEFAULT ((0)),
[DormantWorkstationCount] [int] NULL CONSTRAINT [DF_SummaryActiveAssets_DormantWorkstationCount] DEFAULT ((0)),
[OfflineChargerBays] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SummaryActiveAssets] ADD CONSTRAINT [PK_SummaryActiveAssets] PRIMARY KEY CLUSTERED  ([ROW_ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SummaryActiveAssets', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from batteries.ActiveBatteryCount eleme', 'SCHEMA', N'dbo', 'TABLE', N'SummaryActiveAssets', 'COLUMN', N'ActiveBatteryCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of charger bays with the active flag set to true', 'SCHEMA', N'dbo', 'TABLE', N'SummaryActiveAssets', 'COLUMN', N'ActiveChargerBays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of chargers with the active flag set to true', 'SCHEMA', N'dbo', 'TABLE', N'SummaryActiveAssets', 'COLUMN', N'ActiveChargerCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The number of workstations that have the active flag set to true', 'SCHEMA', N'dbo', 'TABLE', N'SummaryActiveAssets', 'COLUMN', N'ActiveWorkstationCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of workstations that have sent a session packet in the last 7 days but not more recently than 2 hours ago.', 'SCHEMA', N'dbo', 'TABLE', N'SummaryActiveAssets', 'COLUMN', N'AvailableWorkstationCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of batteries in chargers', 'SCHEMA', N'dbo', 'TABLE', N'SummaryActiveAssets', 'COLUMN', N'BatteryInChargerCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of batteries in workstations', 'SCHEMA', N'dbo', 'TABLE', N'SummaryActiveAssets', 'COLUMN', N'BatteryInWorkstationCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of bays', 'SCHEMA', N'dbo', 'TABLE', N'SummaryActiveAssets', 'COLUMN', N'BayCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Count of batteries with charge level over 80 percent', 'SCHEMA', N'dbo', 'TABLE', N'SummaryActiveAssets', 'COLUMN', N'CountFullBatteries'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The report date', 'SCHEMA', N'dbo', 'TABLE', N'SummaryActiveAssets', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to the Department table', 'SCHEMA', N'dbo', 'TABLE', N'SummaryActiveAssets', 'COLUMN', N'DepartmentID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of batteries where the last packet is less than 7 days old but more than 2 days old.', 'SCHEMA', N'dbo', 'TABLE', N'SummaryActiveAssets', 'COLUMN', N'DormantBatteryCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of workstatioins where the last packet is less than 7 days old but more than 2 days old.', 'SCHEMA', N'dbo', 'TABLE', N'SummaryActiveAssets', 'COLUMN', N'DormantWorkstationCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of workstations that have communicated in the last 2 days', 'SCHEMA', N'dbo', 'TABLE', N'SummaryActiveAssets', 'COLUMN', N'InServiceWorkstationCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The number of bays on the device that haven''t communicated a battery packet in the last day', 'SCHEMA', N'dbo', 'TABLE', N'SummaryActiveAssets', 'COLUMN', N'OfflineChargerBays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'SummaryActiveAssets', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to the Site table', 'SCHEMA', N'dbo', 'TABLE', N'SummaryActiveAssets', 'COLUMN', N'SiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The bay count for the device minus the active bay count which is the number of bays on the device  that have communicated a battery packet in the last two hours.', 'SCHEMA', N'dbo', 'TABLE', N'SummaryActiveAssets', 'COLUMN', N'VacantChargerBays'
GO
