CREATE TABLE [dbo].[tblAssets]
(
[IDAsset] [int] NOT NULL IDENTITY(100000, 1),
[Description] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SerialNo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IDAssetType] [int] NOT NULL,
[SoftwareVersion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SiteID] [int] NOT NULL,
[DepartmentID] [int] NULL,
[Image] [varbinary] (max) NULL,
[ImageFilename] [varchar] (51) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDateUTC] [datetime] NULL,
[AssetStatusID] [int] NULL CONSTRAINT [DF_Assets_AssetStatusID] DEFAULT ((0)),
[Activity] [int] NULL,
[Wing] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SiteWingID] [int] NULL,
[Floor] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SiteFloorID] [int] NULL,
[OutOfService] [bit] NULL CONSTRAINT [DF_Assets_OutOfService] DEFAULT ((0)),
[AssetNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsActive] [bit] NULL CONSTRAINT [DF_Assets_IsActive] DEFAULT ((1)),
[CommandCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeviceMAC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Availability] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FriendlyDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WarrantyExpDate] [date] NULL,
[Temperature] [int] NULL,
[LastPostDateUTC] [datetime] NULL,
[Other] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Retired] [int] NULL,
[AccessPointId] [int] NULL,
[LastInternalPostDateUTC] [datetime] NULL,
[WiFiFirmwareRevision] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IP] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Assets_IP] DEFAULT ('0.0.0.0'),
[SourceIPAddress] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Assets_SourceIPAddress] DEFAULT ('0.0.0.0'),
[APMAC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastPacketType] [int] NULL,
[RowPointer] [int] NULL,
[Amps] [decimal] (18, 2) NULL,
[UtilizationThreshhold] [decimal] (18, 2) NULL,
[RemoveFromField] [bit] NULL CONSTRAINT [DF_Assets_RemoveFromField] DEFAULT ((0)),
[ModifiedBy] [int] NULL,
[ManualAllocation] [int] NULL CONSTRAINT [DF_Assets_ManualAllocation] DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAssets] ADD CONSTRAINT [PK_Assets] PRIMARY KEY CLUSTERED  ([IDAsset]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Assets_DepartmentID_SerialNo_IDAssetType_SiteID] ON [dbo].[tblAssets] ([DepartmentID]) INCLUDE ([IDAssetType], [SerialNo], [SiteID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [AssetTypeID] ON [dbo].[tblAssets] ([IDAssetType]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150610-091549] ON [dbo].[tblAssets] ([SerialNo]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150422-172559] ON [dbo].[tblAssets] ([SiteID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAssets] ADD CONSTRAINT [FK_Assets_Sites] FOREIGN KEY ([SiteID]) REFERENCES [dbo].[Sites] ([IDSite])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Table of devices derived from packet information.', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to AccessPiont table', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'AccessPointId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The activity code on the last packet', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'Activity'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Current; Originates from ''amps'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'Amps'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The mac address of the access point from which the last packet was received', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'APMAC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'end-users own id number for the device', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'AssetNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'fk to the AssetStatus table', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'AssetStatusID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Available devices are devices that have been used in the last 2 days but have not  been used in the last 2 hours', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'Availability'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Command Code + 1000; Originates from ''cmc'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'CommandCode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The date that asset was added to the table', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to Departments', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'DepartmentID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Customer Facing description of the asset.', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'DeviceMAC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'the location of the device as of the last packet', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'Floor'
GO
EXEC sp_addextendedproperty N'MS_Description', N'a short description of the product', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'FriendlyDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'IDAsset'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to AssetType table', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'IDAssetType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The binary storage space for image files', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'Image'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The name of the file stored in the Image Column', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'ImageFilename'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Invoice number imported from ERP', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'InvoiceNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The IP of the device when the last packet was received', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'IP'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Device has been used in the last two hours', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'IsActive'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Not used', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'LastInternalPostDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The value of the type on the last packet received', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'LastPacketType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'date time of the last packet received from this device.', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'LastPostDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'When set to 1, the asset will not run to through the auto-allocation', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'ManualAllocation'
GO
EXEC sp_addextendedproperty N'MS_Description', N'not used', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'ModifiedBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'Notes'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'Other'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Similar to retirement of a device.  Triggered manually', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'OutOfService'
GO
EXEC sp_addextendedproperty N'MS_Description', N'When set to 1 indicates that the device has been scheduled to romove from field', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'RemoveFromField'
GO
EXEC sp_addextendedproperty N'MS_Description', N'the device has been retired from the field', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'Retired'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'RowPointer'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Device Serial Number', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'SerialNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Fk to Sites', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'SiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to SiteWings', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'SiteWingID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'the firmware version of the asset', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'SoftwareVersion'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The source IP address that last packet was received from', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'SourceIPAddress'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Temperature; Originates from ''temp'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'Temperature'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Currently not used', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'WarrantyExpDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'WiFi Firmware Revision; Originates from ''wfr'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'WiFiFirmwareRevision'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Location at site', 'SCHEMA', N'dbo', 'TABLE', N'tblAssets', 'COLUMN', N'Wing'
GO
