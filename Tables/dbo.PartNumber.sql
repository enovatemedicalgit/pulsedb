CREATE TABLE [dbo].[PartNumber]
(
[PartNumber] [char] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ROW_ID] [int] NOT NULL,
[FriendlyDescription] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PartNumber', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PartNumber', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PartNumber', 'COLUMN', N'PartNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'PartNumber', 'COLUMN', N'ROW_ID'
GO
