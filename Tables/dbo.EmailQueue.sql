CREATE TABLE [dbo].[EmailQueue]
(
[Subject] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ToAddresses] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CcAddresses] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BccAddresses] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Body] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateCreatedUTC] [datetime] NULL CONSTRAINT [DF_EmailQueue_DateCreatedUTC] DEFAULT (getutcdate()),
[DateSentUTC] [datetime] NULL,
[LastSendAttemptUTC] [datetime] NULL,
[Attempts] [int] NULL CONSTRAINT [DF_EmailQueue_Attempts] DEFAULT ((0)),
[AlertMessageID] [int] NULL,
[Processed] [bit] NOT NULL CONSTRAINT [DF_EmailQueue_Processed] DEFAULT ((0)),
[ROW_ID] [int] NOT NULL IDENTITY(1, 1),
[AlternateKey] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FromAddress] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FromName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[EmailQueue] ADD CONSTRAINT [PK_EmailQueue] PRIMARY KEY CLUSTERED  ([ROW_ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EmailQueue', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EmailQueue', 'COLUMN', N'AlertMessageID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EmailQueue', 'COLUMN', N'AlternateKey'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EmailQueue', 'COLUMN', N'Attempts'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EmailQueue', 'COLUMN', N'BccAddresses'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EmailQueue', 'COLUMN', N'Body'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EmailQueue', 'COLUMN', N'CcAddresses'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EmailQueue', 'COLUMN', N'DateCreatedUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EmailQueue', 'COLUMN', N'DateSentUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EmailQueue', 'COLUMN', N'FromAddress'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EmailQueue', 'COLUMN', N'FromName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EmailQueue', 'COLUMN', N'LastSendAttemptUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EmailQueue', 'COLUMN', N'Processed'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'EmailQueue', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EmailQueue', 'COLUMN', N'Subject'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EmailQueue', 'COLUMN', N'ToAddresses'
GO
