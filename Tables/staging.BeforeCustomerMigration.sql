CREATE TABLE [staging].[BeforeCustomerMigration]
(
[IdSite] [float] NULL,
[CustomerID] [float] NULL,
[IDCustomer] [float] NULL,
[CustomerName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SFCustomerID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SytelineCustomerNum] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CASTBUID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SytelineCustomerSeq] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusReason] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
