CREATE TABLE [dbo].[SummarySiteDepartment]
(
[CreatedDateUTC] [datetime] NOT NULL CONSTRAINT [DF_SummarySiteAsset2_CreatedDateUTC] DEFAULT (getutcdate()),
[Date] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IDCustomer] [int] NULL,
[IDSite] [int] NULL,
[IDDepartment] [int] NULL CONSTRAINT [DF_SummarySiteAsset2_SiteIDDepartment] DEFAULT ((0)),
[ActiveWorkstationCount] [int] NULL,
[AvailableWorkstationCount] [int] NULL,
[OfflineWorkstationCount] [int] NULL,
[LoChargeInsertsCount] [int] NULL CONSTRAINT [DF_SummarySiteAsset2_LoChargeInsertsCount] DEFAULT ((0)),
[HiChargeRemovalsCount] [int] NULL CONSTRAINT [DF_SummarySiteAsset2_HiChargeRemovalsCount] DEFAULT ((0)),
[SessionCount] [int] NULL CONSTRAINT [DF_SummarySiteAsset2_SessionCount] DEFAULT ((0)),
[Utilization] [decimal] (6, 4) NOT NULL CONSTRAINT [DF_SummarySiteDepartment_Utilization] DEFAULT ((0)),
[AvgRunRate] [int] NOT NULL CONSTRAINT [DF_SummarySiteDepartment_RunRate] DEFAULT ((0)),
[AvgAmpDraw] [decimal] (18, 2) NOT NULL CONSTRAINT [DF_SummarySiteDepartment_AvgAmpDraw] DEFAULT ((0)),
[AvgEstimatedPCUtilization] [decimal] (18, 2) NULL CONSTRAINT [DF_SummarySiteDepartment_AvgEstimatedPCUtilization] DEFAULT ((0)),
[AvgChargeLevel] [decimal] (18, 2) NULL,
[HiChargeInserts] [int] NULL CONSTRAINT [DF_SummarySiteDepartment_HiChargeInserts] DEFAULT ((0)),
[LoChargeRemovals] [int] NULL CONSTRAINT [DF_SummarySiteDepartment_LoChargeRemovals] DEFAULT ((0)),
[AvgSignalQuality] [decimal] (18, 2) NULL CONSTRAINT [DF_SummarySiteDepartment_AvgSignalQuality] DEFAULT ((0)),
[RemainingCapacity] [decimal] (18, 2) NULL CONSTRAINT [DF_SummarySiteDepartment_RemainingCapacity] DEFAULT ((0))
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteDepartment', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'The number of workstations that have the active flag set to true', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteDepartment', 'COLUMN', N'ActiveWorkstationCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of workstations that have sent a session packet in the last 7 days but not more recently than 2 hours ago.', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteDepartment', 'COLUMN', N'AvailableWorkstationCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'the mean of the amp draw of all included devices', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteDepartment', 'COLUMN', N'AvgAmpDraw'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mean of charge level for batteries', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteDepartment', 'COLUMN', N'AvgChargeLevel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteDepartment', 'COLUMN', N'AvgEstimatedPCUtilization'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mean of Run Rate', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteDepartment', 'COLUMN', N'AvgRunRate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mean signal quality rating', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteDepartment', 'COLUMN', N'AvgSignalQuality'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The report date', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteDepartment', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The day part of the date', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteDepartment', 'COLUMN', N'Date'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of insert packets where the battery device is charged over 90 percent', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteDepartment', 'COLUMN', N'HiChargeInserts'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of insert packets where the battery device is charged over 10 percent', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteDepartment', 'COLUMN', N'HiChargeRemovalsCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to Customer', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteDepartment', 'COLUMN', N'IDCustomer'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to Department', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteDepartment', 'COLUMN', N'IDDepartment'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to Site', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteDepartment', 'COLUMN', N'IDSite'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of insert packets where the battery device is charged under 10 percent', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteDepartment', 'COLUMN', N'LoChargeInsertsCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of insert packets where the battery device is charged under 90 percent', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteDepartment', 'COLUMN', N'LoChargeRemovals'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The number of workstations that have not communicated in over 24 hours', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteDepartment', 'COLUMN', N'OfflineWorkstationCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The avg remaining capacity of batteries in included devices', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteDepartment', 'COLUMN', N'RemainingCapacity'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The number of sessions in opened or closed amoung all included devices', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteDepartment', 'COLUMN', N'SessionCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SummarySiteDepartment', 'COLUMN', N'Utilization'
GO
