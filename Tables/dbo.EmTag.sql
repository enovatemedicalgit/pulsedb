CREATE TABLE [dbo].[EmTag]
(
[EmTagID] [int] NOT NULL IDENTITY(1, 1),
[MajorID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MinorID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BatteryChargeLVL] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IDSite] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EmTag] ADD CONSTRAINT [PK_EmTag] PRIMARY KEY CLUSTERED  ([EmTagID]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [PKEmTagID] ON [dbo].[EmTag] ([EmTagID], [MinorID], [IDSite]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EmTag] ADD CONSTRAINT [FK_EmTag_EmTag] FOREIGN KEY ([EmTagID]) REFERENCES [dbo].[EmTag] ([EmTagID])
GO
ALTER TABLE [dbo].[EmTag] ADD CONSTRAINT [FK_EmTag_EmTag1] FOREIGN KEY ([EmTagID]) REFERENCES [dbo].[EmTag] ([EmTagID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EmTag', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EmTag', 'COLUMN', N'BatteryChargeLVL'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EmTag', 'COLUMN', N'EmTagID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EmTag', 'COLUMN', N'IDSite'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EmTag', 'COLUMN', N'MajorID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'EmTag', 'COLUMN', N'MinorID'
GO
