CREATE TABLE [staging].[BatteryHealthStats]
(
[SerialNo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AgeInDays] [int] NULL,
[CycleCount] [int] NULL,
[FullChargeCapacity] [int] NULL,
[CalcAH] [int] NULL,
[AmpHours] [int] NOT NULL,
[MaxCapacity] [int] NULL,
[maxRatio] [float] NULL,
[Generation] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HealthFromCalcAh] [float] NULL,
[HealthFromAmpHourRating] [float] NULL,
[HealthFromMax] [float] NULL,
[ReportDate] [date] NULL
) ON [PRIMARY]
GO
