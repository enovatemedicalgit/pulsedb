CREATE TABLE [dbo].[AssetSiteChangeLog]
(
[DeviceID] [int] NOT NULL,
[PreviousSiteID] [int] NULL,
[NewSiteID] [int] NULL,
[ModifiedDateUTC] [datetime] NOT NULL CONSTRAINT [DF_AssetSiteChangeLog_ModifiedDateUTC] DEFAULT (getutcdate())
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_AssetSiteChangeLog_DeviceID] ON [dbo].[AssetSiteChangeLog] ([DeviceID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_AssetSiteChangeLog_ModifiedDateUTC] ON [dbo].[AssetSiteChangeLog] ([ModifiedDateUTC]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Log table based on DeviceId that tracks movement of a device between sites.', 'SCHEMA', N'dbo', 'TABLE', N'AssetSiteChangeLog', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetSiteChangeLog', 'COLUMN', N'DeviceID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetSiteChangeLog', 'COLUMN', N'ModifiedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetSiteChangeLog', 'COLUMN', N'PreviousSiteID'
GO
