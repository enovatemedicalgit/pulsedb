CREATE TABLE [staging].[BadContactInfo]
(
[SroNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ContactEmail] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SroCreatedBy] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SfCustomerAcctID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
