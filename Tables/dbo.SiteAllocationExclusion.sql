CREATE TABLE [dbo].[SiteAllocationExclusion]
(
[SiteID] [int] NOT NULL,
[UseAP] [bit] NULL,
[UseIP] [bit] NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteAllocationExclusion', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteAllocationExclusion', 'COLUMN', N'SiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteAllocationExclusion', 'COLUMN', N'UseAP'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteAllocationExclusion', 'COLUMN', N'UseIP'
GO
