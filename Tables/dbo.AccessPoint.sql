CREATE TABLE [dbo].[AccessPoint]
(
[ROW_ID] [int] NOT NULL IDENTITY(1, 1),
[MACAddress] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SiteID] [int] NULL,
[DepartmentID] [int] NULL,
[DeviceCount] [int] NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Wing] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SiteWingId] [int] NULL,
[Floor] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SiteFloorId] [int] NULL,
[AvgLinkQuality] [int] NULL,
[LastPostDateUTC] [datetime] NULL,
[CreatedDateUTC] [datetime] NULL,
[ModifiedDateUTC] [datetime] NULL,
[Manual] [bit] NULL,
[InsertedBy] [int] NULL,
[BatchID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AccessPoint] ADD CONSTRAINT [PK_AccessPoint] PRIMARY KEY CLUSTERED  ([ROW_ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_AccessPoint_MACAddress_Manual_Floor] ON [dbo].[AccessPoint] ([MACAddress], [Manual], [Floor]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AccessPoint] ADD CONSTRAINT [FK_AccessPoint_Sites] FOREIGN KEY ([SiteID]) REFERENCES [dbo].[Sites] ([IDSite])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Access point data collected from telemetry packets by MAC address.  Updated by auto assignment.', 'SCHEMA', N'dbo', 'TABLE', N'AccessPoint', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Estimate of the strength of signal averaged over the devices most recently connencted to it', 'SCHEMA', N'dbo', 'TABLE', N'AccessPoint', 'COLUMN', N'AvgLinkQuality'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The BatchID from the last packet to update this AP record', 'SCHEMA', N'dbo', 'TABLE', N'AccessPoint', 'COLUMN', N'BatchID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The first time a device connected to this MAC Address', 'SCHEMA', N'dbo', 'TABLE', N'AccessPoint', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to Departments.IdDepartment', 'SCHEMA', N'dbo', 'TABLE', N'AccessPoint', 'COLUMN', N'DepartmentID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Used internally.', 'SCHEMA', N'dbo', 'TABLE', N'AccessPoint', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of chargers and workstations that have communicated with the AP since the last nightly summary job.', 'SCHEMA', N'dbo', 'TABLE', N'AccessPoint', 'COLUMN', N'DeviceCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Location information on the access point based on chargers and workstations that are frequently communicating from it.', 'SCHEMA', N'dbo', 'TABLE', N'AccessPoint', 'COLUMN', N'Floor'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AccessPoint', 'COLUMN', N'InsertedBy'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The last time this record was updated', 'SCHEMA', N'dbo', 'TABLE', N'AccessPoint', 'COLUMN', N'LastPostDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The MAC address of the access point', 'SCHEMA', N'dbo', 'TABLE', N'AccessPoint', 'COLUMN', N'MACAddress'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AccessPoint', 'COLUMN', N'Manual'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AccessPoint', 'COLUMN', N'ModifiedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'AccessPoint', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to Sites.IDSite', 'SCHEMA', N'dbo', 'TABLE', N'AccessPoint', 'COLUMN', N'SiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Location information on the access point based on chargers and workstations that are frequently communicating from it.', 'SCHEMA', N'dbo', 'TABLE', N'AccessPoint', 'COLUMN', N'Wing'
GO
