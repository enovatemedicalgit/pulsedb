CREATE TABLE [dbo].[BatteryPartList]
(
[BatteryPartID] [int] NOT NULL IDENTITY(1, 1),
[PartNumber] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AmpHours] [int] NOT NULL,
[CalcAH] [int] NULL,
[Generation] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BatteryPartList] ADD CONSTRAINT [PK_BatteryPartList] PRIMARY KEY CLUSTERED  ([BatteryPartID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Information on different generations of batteries by part number', 'SCHEMA', N'dbo', 'TABLE', N'BatteryPartList', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BatteryPartList', 'COLUMN', N'AmpHours'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BatteryPartList', 'COLUMN', N'BatteryPartID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BatteryPartList', 'COLUMN', N'CalcAH'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BatteryPartList', 'COLUMN', N'Generation'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BatteryPartList', 'COLUMN', N'PartNumber'
GO
