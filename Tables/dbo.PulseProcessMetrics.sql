CREATE TABLE [dbo].[PulseProcessMetrics]
(
[LastPacketParse] [datetime] NULL,
[LastAlertSent] [datetime] NULL,
[AlertsSentToSF] [int] NULL,
[AlertsGenerated] [int] NULL,
[SessionRowsInserted] [int] NULL,
[LastRollup] [datetime] NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PulseProcessMetrics', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PulseProcessMetrics', 'COLUMN', N'AlertsGenerated'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PulseProcessMetrics', 'COLUMN', N'AlertsSentToSF'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PulseProcessMetrics', 'COLUMN', N'LastAlertSent'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PulseProcessMetrics', 'COLUMN', N'LastPacketParse'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PulseProcessMetrics', 'COLUMN', N'LastRollup'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PulseProcessMetrics', 'COLUMN', N'SessionRowsInserted'
GO
