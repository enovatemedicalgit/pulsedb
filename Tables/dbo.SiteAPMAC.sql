CREATE TABLE [dbo].[SiteAPMAC]
(
[CustomerID] [int] NOT NULL,
[SiteID] [int] NULL,
[APMAC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[APMACTotalDeviceCount] [int] NULL,
[DeviceCount] [int] NULL,
[FacilityReliabilityRating] [int] NULL,
[OnlyAPMACForHQ] [int] NOT NULL,
[OnlyAPMACForFacility] [int] NOT NULL,
[CreatedDateUTC] [date] NULL CONSTRAINT [DF_SiteAPMAC_CreatedDateUTC] DEFAULT (getdate())
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_SiteAPMAC_APMACFacilityReliabilityRating] ON [dbo].[SiteAPMAC] ([APMAC], [FacilityReliabilityRating]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_SiteAPMAC_APMAC_FacilityReliabilityRating] ON [dbo].[SiteAPMAC] ([FacilityReliabilityRating], [APMAC]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SiteAPMAC] ADD CONSTRAINT [FK_SiteAPMac_Sites] FOREIGN KEY ([SiteID]) REFERENCES [dbo].[Sites] ([IDSite])
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteAPMAC', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteAPMAC', 'COLUMN', N'APMAC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteAPMAC', 'COLUMN', N'APMACTotalDeviceCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteAPMAC', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteAPMAC', 'COLUMN', N'CustomerID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteAPMAC', 'COLUMN', N'DeviceCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteAPMAC', 'COLUMN', N'FacilityReliabilityRating'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteAPMAC', 'COLUMN', N'OnlyAPMACForFacility'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteAPMAC', 'COLUMN', N'OnlyAPMACForHQ'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'SiteAPMAC', 'COLUMN', N'SiteID'
GO
