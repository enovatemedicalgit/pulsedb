CREATE TABLE [dbo].[TicketField]
(
[TicketFieldID] [int] NOT NULL,
[FieldName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EnovateOnly] [bit] NOT NULL,
[Enabled] [bit] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TicketField', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TicketField', 'COLUMN', N'Enabled'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TicketField', 'COLUMN', N'EnovateOnly'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TicketField', 'COLUMN', N'FieldName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TicketField', 'COLUMN', N'TicketFieldID'
GO
