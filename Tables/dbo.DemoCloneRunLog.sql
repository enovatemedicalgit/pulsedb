CREATE TABLE [dbo].[DemoCloneRunLog]
(
[LastRanTimestampUTC] [datetime] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'DemoCloneRunLog', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'DemoCloneRunLog', 'COLUMN', N'LastRanTimestampUTC'
GO
