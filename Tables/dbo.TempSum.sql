CREATE TABLE [dbo].[TempSum]
(
[CreatedDateUTC] [datetime] NOT NULL,
[Date] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CustomerID] [int] NULL,
[SiteID] [int] NULL,
[DepartmentID] [int] NULL,
[ActiveWorkstationCount] [int] NULL,
[AvailableWorkstationCount] [int] NULL,
[OfflineWorkstationCount] [int] NULL,
[LoChargeInsertsCount] [int] NULL,
[HiChargeRemovalsCount] [int] NULL,
[SessionCount] [int] NULL,
[Utilization] [decimal] (6, 4) NOT NULL,
[AvgRunRate] [int] NOT NULL,
[AvgAmpDraw] [decimal] (18, 2) NOT NULL,
[AvgEstimatedPCUtilization] [decimal] (18, 2) NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TempSum', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TempSum', 'COLUMN', N'ActiveWorkstationCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TempSum', 'COLUMN', N'AvailableWorkstationCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TempSum', 'COLUMN', N'AvgAmpDraw'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TempSum', 'COLUMN', N'AvgEstimatedPCUtilization'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TempSum', 'COLUMN', N'AvgRunRate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TempSum', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TempSum', 'COLUMN', N'CustomerID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TempSum', 'COLUMN', N'Date'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TempSum', 'COLUMN', N'DepartmentID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TempSum', 'COLUMN', N'HiChargeRemovalsCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TempSum', 'COLUMN', N'LoChargeInsertsCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TempSum', 'COLUMN', N'OfflineWorkstationCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TempSum', 'COLUMN', N'SessionCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'TempSum', 'COLUMN', N'SiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TempSum', 'COLUMN', N'Utilization'
GO
