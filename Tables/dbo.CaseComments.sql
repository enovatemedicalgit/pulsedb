CREATE TABLE [dbo].[CaseComments]
(
[CaseID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CaseNum] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IDUser] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CommentText] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedDateUTC] [datetime2] NULL CONSTRAINT [DF_CaseComments_CreatedDateUTC] DEFAULT (getutcdate()),
[ParentID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPublished] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
