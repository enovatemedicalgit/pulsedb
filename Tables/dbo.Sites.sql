CREATE TABLE [dbo].[Sites]
(
[IDSite] [int] NOT NULL IDENTITY(1, 1),
[SiteName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SiteDescription] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerID] [int] NOT NULL,
[SfCustomerAcctID] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SfAccountId] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SfAcctOwnerId] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IPAddr] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsInternal] [bit] NULL,
[SiteType] [int] NULL,
[Status] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusReason] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Sites_StatusReason] DEFAULT ('N'),
[RegionID] [int] NULL,
[POCEmail] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActiveDevices] [int] NULL CONSTRAINT [DF_Sites_ActiveDevices] DEFAULT ((0)),
[TotalDevices] [int] NULL CONSTRAINT [DF_Sites_TotalDevices] DEFAULT ((0)),
[OnlineDevicesPercentage] [decimal] (18, 2) NULL CONSTRAINT [DF_Sites_OnlinePercentage] DEFAULT ((0.00)),
[LastLoginDateUTC] [datetime2] NULL,
[LicensedForRhythm] [int] NULL,
[SalesRepID] [int] NULL,
[State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZipCode] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Sites] ADD CONSTRAINT [PK_Sites] PRIMARY KEY CLUSTERED  ([IDSite]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Sites] ADD CONSTRAINT [FK_Sites_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([IDCustomer])
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sites', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sites', 'COLUMN', N'CustomerID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sites', 'COLUMN', N'IDSite'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sites', 'COLUMN', N'IPAddr'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sites', 'COLUMN', N'IsInternal'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Distribution email for Pulse/SFDC emails for a site.', 'SCHEMA', N'dbo', 'TABLE', N'Sites', 'COLUMN', N'POCEmail'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sites', 'COLUMN', N'RegionID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sites', 'COLUMN', N'SfAccountId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sites', 'COLUMN', N'SfAcctOwnerId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sites', 'COLUMN', N'SfCustomerAcctID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sites', 'COLUMN', N'SiteDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sites', 'COLUMN', N'SiteName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sites', 'COLUMN', N'SiteType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sites', 'COLUMN', N'Status'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sites', 'COLUMN', N'StatusReason'
GO
