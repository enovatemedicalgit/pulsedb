CREATE TABLE [dbo].[AlertConditions]
(
[AlertType] [int] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Logic] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Enable] [bit] NOT NULL,
[EmailUser] [bit] NOT NULL,
[EmailStinger] [bit] NOT NULL,
[VisibleToClient] [bit] NOT NULL,
[CreatedDateUTC] [datetime] NULL,
[CreatedUserID] [int] NULL,
[ModifiedDateUTC] [datetime] NULL,
[ModifiedUserID] [int] NULL,
[ConditionAllowance] [int] NOT NULL,
[AppliesTo] [int] NOT NULL,
[ROW_ID] [int] NOT NULL,
[FriendlyDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LongDescription] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Descriptor Table for Alerts', 'SCHEMA', N'dbo', 'TABLE', N'AlertConditions', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Code attached to the level of severity of the alert', 'SCHEMA', N'dbo', 'TABLE', N'AlertConditions', 'COLUMN', N'AlertType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertConditions', 'COLUMN', N'AppliesTo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertConditions', 'COLUMN', N'ConditionAllowance'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date the condition was added to the list', 'SCHEMA', N'dbo', 'TABLE', N'AlertConditions', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to User.IdUser **', 'SCHEMA', N'dbo', 'TABLE', N'AlertConditions', 'COLUMN', N'CreatedUserID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'English description of the error', 'SCHEMA', N'dbo', 'TABLE', N'AlertConditions', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertConditions', 'COLUMN', N'EmailStinger'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertConditions', 'COLUMN', N'EmailUser'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Controls whether the alert is generated', 'SCHEMA', N'dbo', 'TABLE', N'AlertConditions', 'COLUMN', N'Enable'
GO
EXEC sp_addextendedproperty N'MS_Description', N'More detailed than description column', 'SCHEMA', N'dbo', 'TABLE', N'AlertConditions', 'COLUMN', N'FriendlyDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'psuedo code description of what values triggered the alert', 'SCHEMA', N'dbo', 'TABLE', N'AlertConditions', 'COLUMN', N'Logic'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Rarely used, expanded version of friendly description column', 'SCHEMA', N'dbo', 'TABLE', N'AlertConditions', 'COLUMN', N'LongDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date the record was modified', 'SCHEMA', N'dbo', 'TABLE', N'AlertConditions', 'COLUMN', N'ModifiedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'idUser of person who modified the alert', 'SCHEMA', N'dbo', 'TABLE', N'AlertConditions', 'COLUMN', N'ModifiedUserID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Unique id on table', 'SCHEMA', N'dbo', 'TABLE', N'AlertConditions', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Used by customer facing UI to determine whether to present', 'SCHEMA', N'dbo', 'TABLE', N'AlertConditions', 'COLUMN', N'VisibleToClient'
GO
