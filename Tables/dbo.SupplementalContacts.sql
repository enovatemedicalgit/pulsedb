CREATE TABLE [dbo].[SupplementalContacts]
(
[CreatedDate] [datetime] NOT NULL,
[CreatedDateUTC] [datetime] NOT NULL,
[UserName] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrimaryPhone] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SfCustomerAcctId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
