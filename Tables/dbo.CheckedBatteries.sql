CREATE TABLE [dbo].[CheckedBatteries]
(
[BatterySerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InsertedDateUTC] [datetime2] NULL CONSTRAINT [DF_CheckedBatteries_InsertedDateUTC] DEFAULT (getutcdate())
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [ClusteredIndex-20160714-132451] ON [dbo].[CheckedBatteries] ([BatterySerialNumber]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Batteries that have been checked by Pulse/Gordon for Imbalances', 'SCHEMA', N'dbo', 'TABLE', N'CheckedBatteries', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Serial Number; Originates from ''bsn'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'CheckedBatteries', 'COLUMN', N'BatterySerialNumber'
GO
