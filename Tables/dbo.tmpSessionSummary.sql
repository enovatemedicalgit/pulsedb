CREATE TABLE [dbo].[tmpSessionSummary]
(
[RecordDate] [date] NULL,
[SiteID] [int] NULL,
[DepartmentID] [int] NULL,
[SessionCount] [int] NULL,
[AvgSessionLengthHours] [int] NULL,
[AvgSessionLengthMinutes] [int] NULL,
[AvgRunRate] [int] NULL,
[Utilization] [int] NOT NULL
) ON [PRIMARY]
GO
