CREATE TABLE [dbo].[AssetTypeDetail]
(
[IDAssetTypeDetail] [int] NOT NULL IDENTITY(1, 1),
[TypeDetailName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TypeDetailValue] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TypeDetailDesciption] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IDAssetType] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AssetTypeDetail] ADD CONSTRAINT [PK_AssetTypeDetail] PRIMARY KEY CLUSTERED  ([IDAssetTypeDetail]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AssetTypeDetail] ADD CONSTRAINT [FK_AssetTypeDetail_AssetType] FOREIGN KEY ([IDAssetType]) REFERENCES [dbo].[AssetType] ([IDAssetType])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Details for certain types of assets', 'SCHEMA', N'dbo', 'TABLE', N'AssetTypeDetail', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetTypeDetail', 'COLUMN', N'IDAssetType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetTypeDetail', 'COLUMN', N'IDAssetTypeDetail'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetTypeDetail', 'COLUMN', N'TypeDetailDesciption'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetTypeDetail', 'COLUMN', N'TypeDetailName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetTypeDetail', 'COLUMN', N'TypeDetailValue'
GO
