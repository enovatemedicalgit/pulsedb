CREATE TABLE [dbo].[SessionData_Location]
(
[DeviceSerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BatterySerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeviceType] [int] NOT NULL,
[Activity] [int] NOT NULL,
[XValue] [int] NULL,
[XMax] [int] NULL,
[YValue] [int] NULL,
[YMax] [int] NULL,
[ZValue] [int] NULL,
[ZMax] [int] NULL,
[Move] [int] NULL,
[LinkQuality] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeviceMAC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[APMAC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ROW_ID] [int] NOT NULL IDENTITY(1, 1),
[SourceIPAddress] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SessionData_Location] ADD CONSTRAINT [PK_PacketData_Location] PRIMARY KEY CLUSTERED  ([ROW_ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Serial Number; Originates from ''bsn'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'SessionData_Location', 'COLUMN', N'BatterySerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Equivalent to AssetTypeId. Originates from ''type'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'SessionData_Location', 'COLUMN', N'DeviceType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'WiFi Link Quality; Originates from ''lq'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'SessionData_Location', 'COLUMN', N'LinkQuality'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Move Flag; Originates from ''mov'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'SessionData_Location', 'COLUMN', N'Move'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery X Max Value; Originates from ''xm'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'SessionData_Location', 'COLUMN', N'XMax'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery X Value; Originates from ''xv'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'SessionData_Location', 'COLUMN', N'XValue'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Y Max Value; Originates from ''ym'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'SessionData_Location', 'COLUMN', N'YMax'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Y Value; Originates from ''yv'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'SessionData_Location', 'COLUMN', N'YValue'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Z Max Value; Originates from ''zm'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'SessionData_Location', 'COLUMN', N'ZMax'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Z Value; Originates from ''zv'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'SessionData_Location', 'COLUMN', N'ZValue'
GO
