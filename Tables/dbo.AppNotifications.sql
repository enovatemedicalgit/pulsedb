CREATE TABLE [dbo].[AppNotifications]
(
[CaptionText] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContentText] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Application] [nchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDateUTC] [datetime2] NULL CONSTRAINT [DF_AppNotifications_CreatedDateUTC] DEFAULT (getutcdate()),
[SingleUserID] [int] NULL
) ON [PRIMARY]
GO
