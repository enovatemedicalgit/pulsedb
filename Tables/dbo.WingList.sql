CREATE TABLE [dbo].[WingList]
(
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IDWing] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
