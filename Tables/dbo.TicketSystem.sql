CREATE TABLE [dbo].[TicketSystem]
(
[TicketSystemID] [int] NOT NULL,
[SystemName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceEmail] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SiteID] [int] NOT NULL,
[CreatedDateUTC] [datetime] NOT NULL,
[CreatedUserID] [int] NOT NULL,
[ModifiedDateUTC] [datetime] NULL,
[ModifiedUserID] [int] NULL,
[IsEnovate] [bit] NOT NULL,
[Enabled] [bit] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TicketSystem', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TicketSystem', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'TicketSystem', 'COLUMN', N'CreatedUserID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TicketSystem', 'COLUMN', N'Enabled'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TicketSystem', 'COLUMN', N'IsEnovate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'TicketSystem', 'COLUMN', N'ModifiedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'TicketSystem', 'COLUMN', N'ModifiedUserID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'TicketSystem', 'COLUMN', N'SiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TicketSystem', 'COLUMN', N'SourceEmail'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TicketSystem', 'COLUMN', N'SystemName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TicketSystem', 'COLUMN', N'TicketSystemID'
GO
