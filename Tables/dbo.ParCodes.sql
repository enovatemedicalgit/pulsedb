CREATE TABLE [dbo].[ParCodes]
(
[ColumnName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ParValue] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ParMeaning] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Display] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ROW_ID] [int] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'ParCodes', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'ParCodes', 'COLUMN', N'ColumnName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'ParCodes', 'COLUMN', N'Display'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'ParCodes', 'COLUMN', N'ParMeaning'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'ParCodes', 'COLUMN', N'ParValue'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'ParCodes', 'COLUMN', N'ROW_ID'
GO
