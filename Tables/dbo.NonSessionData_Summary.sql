CREATE TABLE [dbo].[NonSessionData_Summary]
(
[DeviceSerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BatterySerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeviceType] [int] NOT NULL,
[Activity] [int] NOT NULL,
[Bay] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullChargeCapacity] [int] NULL,
[Voltage] [decimal] (18, 2) NULL,
[Amps] [decimal] (18, 2) NULL,
[Temperature] [int] NULL,
[ChargeLevel] [int] NULL,
[CycleCount] [int] NULL,
[MaxCycleCount] [int] NULL,
[VoltageCell1] [decimal] (18, 2) NULL,
[VoltageCell2] [decimal] (18, 2) NULL,
[VoltageCell3] [decimal] (18, 2) NULL,
[RemainingTime] [int] NULL,
[Efficiency] [decimal] (18, 2) NULL,
[BackupBatteryVoltage] [decimal] (18, 2) NULL,
[BackupBatteryStatus] [int] NULL,
[IsAC] [bit] NULL,
[DCUnit1AVolts] [decimal] (18, 2) NULL,
[DCUnit1ACurrent] [decimal] (18, 2) NULL,
[DCUnit1BVolts] [decimal] (18, 2) NULL,
[DCUnit1BCurrent] [decimal] (18, 2) NULL,
[DCUnit2AVolts] [decimal] (18, 2) NULL,
[DCUnit2ACurrent] [decimal] (18, 2) NULL,
[DCUnit2BVolts] [decimal] (18, 2) NULL,
[DCUnit2BCurrent] [decimal] (18, 2) NULL,
[Move] [int] NULL,
[BatteryStatus] [int] NULL,
[SafetyStatus] [int] NULL,
[BatteryChargeStatus] [int] NULL,
[BatterySafetyAlert] [int] NULL,
[BatteryOpStatus] [int] NULL,
[BatteryMode] [int] NULL,
[LinkQuality] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GenericError] [int] NULL,
[SessionRecordType] [int] NULL,
[QueryStringID] [int] NOT NULL,
[SessionID] [int] NULL CONSTRAINT [DF_NSPacketData_Summary_SessionIDCurrent] DEFAULT ((0)),
[CommandCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ROW_ID] [int] NOT NULL IDENTITY(1, 1),
[CreatedDateUTC] [datetime] NULL,
[ExaminedDateUTC] [datetime] NULL,
[SourceIPAddress] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeasuredVoltage] [decimal] (18, 2) NULL,
[InsertedDateUTC] [datetime] NULL,
[CheckedByRX] [bit] NULL CONSTRAINT [DF_NSPacketData_Summary_CheckedByRXCurrent] DEFAULT ((0)),
[PacketType] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[NonSessionData_Summary] ADD CONSTRAINT [PK_NSPacketData_Summary] PRIMARY KEY CLUSTERED  ([ROW_ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_NonSessionData_Summary_By_CreatedDateUTC] ON [dbo].[NonSessionData_Summary] ([CreatedDateUTC] DESC) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_NonSessionData_Summary_OnCDate] ON [dbo].[NonSessionData_Summary] ([CreatedDateUTC]) INCLUDE ([ROW_ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_NonSessionData_Summary_By_DevSN_BattSN_DevType] ON [dbo].[NonSessionData_Summary] ([DeviceSerialNumber], [BatterySerialNumber], [DeviceType]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Current; Originates from ''amps'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'Amps'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Backup Battery Charger Status; Originates from ''bubs'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'BackupBatteryStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Backup Battery Voltage; Originates from ''bubv'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'BackupBatteryVoltage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Charging Status Flags; Originates from ''bcs'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'BatteryChargeStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Battery Mode Flags; Originates from ''bbm'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'BatteryMode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Operation Status Flags; Originates from ''bos'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'BatteryOpStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Safety Alert Flags; Originates from ''bsa'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'BatterySafetyAlert'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Serial Number; Originates from ''bsn'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'BatterySerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Battery Status Flags; Originates from ''bbs'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'BatteryStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Bay Number; Originates from ''bay'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'Bay'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Charge Level; Originates from ''cl'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'ChargeLevel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Command Code + 1000; Originates from ''cmc'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'CommandCode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Cycle Count; Originates from ''cc'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'CycleCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 1 Output Current A; Originates from ''da1c'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'DCUnit1ACurrent'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 1 Output Voltage A; Originates from ''da1v'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'DCUnit1AVolts'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 1 Output Current B; Originates from ''db1c'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'DCUnit1BCurrent'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 1 Output Voltage B; Originates from ''db1v'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'DCUnit1BVolts'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 2 Output Current A; Originates from ''da2c'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'DCUnit2ACurrent'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 2 Output Voltage A; Originates from ''da2v'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'DCUnit2AVolts'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 2 Output Current B; Originates from ''db2c'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'DCUnit2BCurrent'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 2 Output Voltage B; Originates from ''db2v'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'DCUnit2BVolts'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Equivalent to AssetTypeId. Originates from ''type'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'DeviceType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery FCC; Originates from ''fcc'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'FullChargeCapacity'
GO
EXEC sp_addextendedproperty N'MS_Description', N'AC Connected; Originates from ''acs'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'IsAC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'WiFi Link Quality; Originates from ''lq'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'LinkQuality'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Move Flag; Originates from ''mov'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'Move'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Remaining Time; Originates from ''rt'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'RemainingTime'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Safety Status Flags; Originates from ''bss'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'SafetyStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Temperature; Originates from ''temp'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'Temperature'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Voltage; Originates from ''vol'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'Voltage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Cell 1 Voltage; Originates from ''c1v'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'VoltageCell1'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Cell 2 Voltage; Originates from ''c2v'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'VoltageCell2'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Cell 3 Voltage; Originates from ''c3v'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'NonSessionData_Summary', 'COLUMN', N'VoltageCell3'
GO
