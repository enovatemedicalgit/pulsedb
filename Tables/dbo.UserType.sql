CREATE TABLE [dbo].[UserType]
(
[IDUserType] [int] NOT NULL IDENTITY(0, 1),
[Description] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserType] ADD CONSTRAINT [PK_UserType] PRIMARY KEY CLUSTERED  ([IDUserType]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'UserType', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'UserType', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'UserType', 'COLUMN', N'IDUserType'
GO
