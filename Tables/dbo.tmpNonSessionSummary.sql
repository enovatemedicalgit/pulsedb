CREATE TABLE [dbo].[tmpNonSessionSummary]
(
[RecordDate] [date] NULL,
[SiteID] [int] NULL,
[DepartmentID] [int] NULL,
[Utilization] [int] NOT NULL,
[NonSessionRecordCount] [int] NULL
) ON [PRIMARY]
GO
