CREATE TABLE [staging].[SFCustomerUserXWalk]
(
[Account Owner] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OwnerId] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Customer Acct #] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Billing City] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Billing State/Province] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Billing Zip/Postal Code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'staging', 'TABLE', N'SFCustomerUserXWalk', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'staging', 'TABLE', N'SFCustomerUserXWalk', 'COLUMN', N'Account ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'staging', 'TABLE', N'SFCustomerUserXWalk', 'COLUMN', N'Account Name'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'staging', 'TABLE', N'SFCustomerUserXWalk', 'COLUMN', N'Account Owner'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'staging', 'TABLE', N'SFCustomerUserXWalk', 'COLUMN', N'Billing City'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'staging', 'TABLE', N'SFCustomerUserXWalk', 'COLUMN', N'Billing State/Province'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'staging', 'TABLE', N'SFCustomerUserXWalk', 'COLUMN', N'Billing Zip/Postal Code'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'staging', 'TABLE', N'SFCustomerUserXWalk', 'COLUMN', N'Customer Acct #'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'staging', 'TABLE', N'SFCustomerUserXWalk', 'COLUMN', N'OwnerId'
GO
