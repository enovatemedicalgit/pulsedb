CREATE TABLE [dbo].[Job]
(
[JobCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[JobDescription] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastRanTimestampUTC] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Job] ADD CONSTRAINT [PK_Job] PRIMARY KEY CLUSTERED  ([JobCode]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Job', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Job', 'COLUMN', N'JobCode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Job', 'COLUMN', N'JobDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Job', 'COLUMN', N'LastRanTimestampUTC'
GO
