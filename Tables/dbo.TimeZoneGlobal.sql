CREATE TABLE [dbo].[TimeZoneGlobal]
(
[CountryCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Coordinates] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeZone] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StandardTime] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SummerTime] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OffsetMinutes_Standard] [int] NULL,
[OffsetMinutes_DaylightSavings] [int] NULL,
[Notes] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ROW_ID] [int] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TimeZoneGlobal', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TimeZoneGlobal', 'COLUMN', N'Comments'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TimeZoneGlobal', 'COLUMN', N'Coordinates'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TimeZoneGlobal', 'COLUMN', N'CountryCode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TimeZoneGlobal', 'COLUMN', N'Notes'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TimeZoneGlobal', 'COLUMN', N'OffsetMinutes_DaylightSavings'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TimeZoneGlobal', 'COLUMN', N'OffsetMinutes_Standard'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'TimeZoneGlobal', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TimeZoneGlobal', 'COLUMN', N'StandardTime'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TimeZoneGlobal', 'COLUMN', N'SummerTime'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TimeZoneGlobal', 'COLUMN', N'TimeZone'
GO
