CREATE TABLE [dbo].[WarrantyCode]
(
[WarrantyCode] [int] NOT NULL,
[WarrantyCodeType] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WarrantyCode] ADD CONSTRAINT [PK_WarrantyCode] PRIMARY KEY CLUSTERED  ([WarrantyCode]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_WarrantyCode] ON [dbo].[WarrantyCode] ([WarrantyCode], [WarrantyCodeType], [Description]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'WarrantyCode', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'WarrantyCode', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'WarrantyCode', 'COLUMN', N'WarrantyCode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Must either be START or END', 'SCHEMA', N'dbo', 'TABLE', N'WarrantyCode', 'COLUMN', N'WarrantyCodeType'
GO
