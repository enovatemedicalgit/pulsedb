CREATE TABLE [dbo].[AssetsWorkstation]
(
[IDAsset] [int] NOT NULL IDENTITY(100000, 1),
[UtilizationLevel] [decimal] (18, 2) NULL CONSTRAINT [DF_Assets_UtilizationLevel_AssetsWorkstation] DEFAULT ((0)),
[Move] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AssetsWorkstation] ADD CONSTRAINT [pk_AssetsWorkstation] PRIMARY KEY CLUSTERED  ([IDAsset]) ON [PRIMARY]
GO
