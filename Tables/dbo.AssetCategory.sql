CREATE TABLE [dbo].[AssetCategory]
(
[IDAssetCategory] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AssetCategory] ADD CONSTRAINT [PK_AssetCategory] PRIMARY KEY CLUSTERED  ([IDAssetCategory]) ON [PRIMARY]
GO
