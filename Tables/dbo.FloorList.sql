CREATE TABLE [dbo].[FloorList]
(
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IDFloor] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
