CREATE TABLE [dbo].[TicketSettings]
(
[TicketSettingsID] [int] NOT NULL,
[TicketSystemID] [int] NOT NULL,
[TicketFieldID] [int] NOT NULL,
[WrapperStart] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WrapperEnd] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Enabled] [bit] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TicketSettings', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TicketSettings', 'COLUMN', N'Enabled'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TicketSettings', 'COLUMN', N'TicketFieldID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TicketSettings', 'COLUMN', N'TicketSettingsID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TicketSettings', 'COLUMN', N'TicketSystemID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TicketSettings', 'COLUMN', N'WrapperEnd'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TicketSettings', 'COLUMN', N'WrapperStart'
GO
