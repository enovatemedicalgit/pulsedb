CREATE TABLE [dbo].[AssetAuditLog]
(
[IDAssetAudit] [int] NOT NULL IDENTITY(1, 1),
[AssetID] [int] NOT NULL,
[EventDateTime] [datetime] NOT NULL,
[LocationID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AssetAuditLog] ADD CONSTRAINT [PK_AssetAuditLog] PRIMARY KEY CLUSTERED  ([IDAssetAudit]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table is not used', 'SCHEMA', N'dbo', 'TABLE', N'AssetAuditLog', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetAuditLog', 'COLUMN', N'AssetID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetAuditLog', 'COLUMN', N'EventDateTime'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetAuditLog', 'COLUMN', N'IDAssetAudit'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetAuditLog', 'COLUMN', N'LocationID'
GO
