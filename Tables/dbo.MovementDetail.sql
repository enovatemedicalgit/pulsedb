CREATE TABLE [dbo].[MovementDetail]
(
[SiteID] [int] NOT NULL,
[DepartmentID] [int] NULL,
[SerialNo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Date] [datetime] NOT NULL,
[DateUTC] [datetime] NULL,
[Hour] [int] NOT NULL,
[HourUTC] [int] NULL,
[Moves] [int] NOT NULL,
[ROW_ID] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [ClusteredIndex-20150609-104852] ON [dbo].[MovementDetail] ([SerialNo]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'MovementDetail', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'MovementDetail', 'COLUMN', N'Date'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'MovementDetail', 'COLUMN', N'DateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'MovementDetail', 'COLUMN', N'DepartmentID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'MovementDetail', 'COLUMN', N'Hour'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'MovementDetail', 'COLUMN', N'HourUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'MovementDetail', 'COLUMN', N'Moves'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'MovementDetail', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'MovementDetail', 'COLUMN', N'SerialNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'MovementDetail', 'COLUMN', N'SiteID'
GO
