CREATE TABLE [staging].[TimeBands15]
(
[Recency] [bigint] NULL,
[LBound] [datetime] NULL,
[UBound] [datetime] NULL
) ON [PRIMARY]
GO
