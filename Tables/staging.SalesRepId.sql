CREATE TABLE [staging].[SalesRepId]
(
[Account Owner] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account Owner: Email] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Owner Role] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
