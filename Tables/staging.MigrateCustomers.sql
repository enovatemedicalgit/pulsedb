CREATE TABLE [staging].[MigrateCustomers]
(
[OldIdCustomer] [int] NULL,
[NewIdCustomer] [int] NULL,
[CustomerName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SFCustomerID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
