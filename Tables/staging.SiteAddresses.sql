CREATE TABLE [staging].[SiteAddresses]
(
[Account Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account External ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Shipping Address Line 1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Shipping City] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Shipping State/Province] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Shipping Zip/Postal Code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
