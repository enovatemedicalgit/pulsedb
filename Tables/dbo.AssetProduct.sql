CREATE TABLE [dbo].[AssetProduct]
(
[ProductTypeID] [int] NOT NULL,
[ProductSerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeviceSerialNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Active] [bit] NOT NULL,
[CreatedDateUTC] [datetime] NULL,
[CreatedUserID] [int] NOT NULL,
[DivorceDateUTC] [datetime] NULL,
[ModifiedUserID] [int] NULL,
[ROW_ID] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Xwalk table of Assets to Produt Type', 'SCHEMA', N'dbo', 'TABLE', N'AssetProduct', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetProduct', 'COLUMN', N'Active'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetProduct', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'AssetProduct', 'COLUMN', N'CreatedUserID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'AssetProduct', 'COLUMN', N'DeviceSerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetProduct', 'COLUMN', N'DivorceDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'AssetProduct', 'COLUMN', N'ModifiedUserID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetProduct', 'COLUMN', N'ProductSerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetProduct', 'COLUMN', N'ProductTypeID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'AssetProduct', 'COLUMN', N'ROW_ID'
GO
