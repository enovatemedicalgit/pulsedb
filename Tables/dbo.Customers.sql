CREATE TABLE [dbo].[Customers]
(
[IDCustomer] [int] NOT NULL IDENTITY(1, 1),
[CustomerName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SFCustomerID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SytelineCustomerNum] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CASTBUID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SytelineCustomerSeq] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusReason] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SitesOnline] [int] NULL CONSTRAINT [DF_Customers_SitesOnline] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Customers] ADD CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED  ([IDCustomer]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Basic customer info.  Parent of sites', 'SCHEMA', N'dbo', 'TABLE', N'Customers', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Customers', 'COLUMN', N'CASTBUID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Customers', 'COLUMN', N'CustomerName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Customers', 'COLUMN', N'IDCustomer'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Customers', 'COLUMN', N'SFCustomerID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Customers', 'COLUMN', N'Status'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Customers', 'COLUMN', N'StatusReason'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Customers', 'COLUMN', N'SytelineCustomerNum'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Customers', 'COLUMN', N'SytelineCustomerSeq'
GO
