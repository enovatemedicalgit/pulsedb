CREATE TABLE [dbo].[Network]
(
[SiteID] [int] NOT NULL,
[SSID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Channel] [int] NOT NULL,
[NetAuthProtocolID] [int] NOT NULL,
[WEPKey] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WPAPassphrase] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DHCP] [bit] NOT NULL,
[IPAddress] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubnetMask] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DNSServer] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultGateway] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDateUTC] [datetime] NULL,
[CreatedUserID] [int] NULL,
[ModifiedDateUTC] [datetime] NULL,
[ModifiedUserID] [int] NULL,
[ROW_ID] [int] NOT NULL IDENTITY(1, 1),
[Successful] [int] NULL,
[IDUser] [int] NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Network', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Network', 'COLUMN', N'Channel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Network', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'Network', 'COLUMN', N'CreatedUserID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'Network', 'COLUMN', N'DefaultGateway'
GO
EXEC sp_addextendedproperty N'MS_Description', N'WiFi DHCP; Originates from ''dhcp'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'Network', 'COLUMN', N'DHCP'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'Network', 'COLUMN', N'DNSServer'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Network', 'COLUMN', N'IPAddress'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'Network', 'COLUMN', N'ModifiedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'Network', 'COLUMN', N'ModifiedUserID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'Network', 'COLUMN', N'NetAuthProtocolID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'Network', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'Network', 'COLUMN', N'SiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'WiFi SSID; Originates from ''ssid'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'Network', 'COLUMN', N'SSID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'Network', 'COLUMN', N'SubnetMask'
GO
EXEC sp_addextendedproperty N'MS_Description', N'WiFi WEP Key; Originates from ''wep'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'Network', 'COLUMN', N'WEPKey'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'Network', 'COLUMN', N'WPAPassphrase'
GO
