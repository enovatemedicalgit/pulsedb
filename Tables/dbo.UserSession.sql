CREATE TABLE [dbo].[UserSession]
(
[SessionKey] [uniqueidentifier] NOT NULL,
[UserId] [int] NOT NULL,
[CreatedDateUTC] [datetime] NULL CONSTRAINT [DF_UserSession_CreatedDateUTC] DEFAULT (getutcdate()),
[Active] [bit] NULL,
[ExpirationDateUTC] [datetime2] NULL CONSTRAINT [DF_UserSession_ExpirationDateUTC] DEFAULT (dateadd(minute,(45),getutcdate()))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserSession] ADD CONSTRAINT [FK_UserSession_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([IDUser])
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'UserSession', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'UserSession', 'COLUMN', N'Active'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'UserSession', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'UserSession', 'COLUMN', N'SessionKey'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'UserSession', 'COLUMN', N'UserId'
GO
