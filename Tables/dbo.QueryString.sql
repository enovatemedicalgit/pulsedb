CREATE TABLE [dbo].[QueryString]
(
[ROW_ID] [int] NOT NULL IDENTITY(1, 1),
[QueryStringBatchID] [int] NOT NULL CONSTRAINT [DF_QueryString_QueryStringBatchID] DEFAULT ((100)),
[DeviceSerialNumber] [varchar] (90) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Query] [varchar] (5000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NineParsed] [bit] NULL,
[CreatedDateUTC] [datetime] NULL CONSTRAINT [DF_QueryString_CreatedDateUTC] DEFAULT (getutcdate()),
[SourceIPAddress] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceTimestampUTC] [datetime] NULL,
[NeedsCaughtUp] [bit] NULL CONSTRAINT [DF_QueryString_NeedsCaughtUp] DEFAULT ((0)),
[ProcessID] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[QueryString] ADD CONSTRAINT [PK_QueryString] PRIMARY KEY CLUSTERED  ([ROW_ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'QueryString', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'QueryString', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'QueryString', 'COLUMN', N'DeviceSerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'QueryString', 'COLUMN', N'NeedsCaughtUp'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'QueryString', 'COLUMN', N'NineParsed'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'QueryString', 'COLUMN', N'ProcessID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'QueryString', 'COLUMN', N'Query'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'QueryString', 'COLUMN', N'QueryStringBatchID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'QueryString', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'QueryString', 'COLUMN', N'SourceIPAddress'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'QueryString', 'COLUMN', N'SourceTimestampUTC'
GO
