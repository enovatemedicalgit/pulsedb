CREATE TABLE [dbo].[PacketErrorTable]
(
[QueryStringID] [int] NULL,
[QueryStringBatchID] [int] NULL,
[CreatedDateUTC] [datetime] NULL,
[SourceTimestampUTC] [datetime] NULL,
[IsSessionData] [bit] NULL,
[DeviceSerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BatterySerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FixedBatterySerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeviceType] [int] NULL,
[Activity] [int] NULL,
[Bay] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullChargeCapacity] [int] NULL,
[Voltage] [numeric] (18, 2) NULL,
[Amps] [numeric] (18, 2) NULL,
[Temperature] [int] NULL,
[ChargeLevel] [int] NULL,
[CycleCount] [int] NULL,
[MaxCycleCount] [int] NULL,
[VoltageCell1] [numeric] (18, 2) NULL,
[VoltageCell2] [numeric] (18, 2) NULL,
[VoltageCell3] [numeric] (18, 2) NULL,
[FETStatus] [int] NULL,
[RemainingTime] [int] NULL,
[BatteryName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Efficiency] [numeric] (18, 2) NULL,
[ControlBoardRevision] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LCDRevision] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HolsterChargerRevision] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DCBoardOneRevision] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DCBoardTwoRevision] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WifiFirmwareRevision] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BayWirelessRevision] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bay1ChargerRevision] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bay2ChargerRevision] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bay3ChargerRevision] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bay4ChargerRevision] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedBoardRevision] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BackupBatteryVoltage] [numeric] (18, 2) NULL,
[BackupBatteryStatus] [int] NULL,
[IsAC] [bit] NULL,
[DCUnit1AVolts] [numeric] (18, 2) NULL,
[DCUnit1ACurrent] [numeric] (18, 2) NULL,
[DCUnit1BVolts] [numeric] (18, 2) NULL,
[DCUnit1BCurrent] [numeric] (18, 2) NULL,
[DCUnit2AVolts] [numeric] (18, 2) NULL,
[DCUnit2ACurrent] [numeric] (18, 2) NULL,
[DCUnit2BVolts] [numeric] (18, 2) NULL,
[DCUnit2BCurrent] [numeric] (18, 2) NULL,
[XValue] [int] NULL,
[XMax] [int] NULL,
[YValue] [int] NULL,
[YMax] [int] NULL,
[ZValue] [int] NULL,
[ZMax] [int] NULL,
[Move] [int] NULL,
[BatteryStatus] [int] NULL,
[SafetyStatus] [int] NULL,
[PermanentFailureStatus] [int] NULL,
[PermanentFailureAlert] [int] NULL,
[BatteryChargeStatus] [int] NULL,
[BatterySafetyAlert] [int] NULL,
[BatteryOpStatus] [int] NULL,
[BatteryMode] [int] NULL,
[DC1Error] [int] NULL,
[DC1Status] [int] NULL,
[DC2Error] [int] NULL,
[DC2Status] [int] NULL,
[MouseFailureNotification] [int] NULL,
[KeyboardFailureNotification] [int] NULL,
[WindowsShutdownNotification] [int] NULL,
[LinkQuality] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeviceMAC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[APMAC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastTransmissionStatus] [bit] NULL,
[AuthType] [int] NULL,
[ChannelNumber] [int] NULL,
[PortNumber] [int] NULL,
[DHCP] [int] NULL,
[WEPKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PassCode] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StaticIP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SSID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SparePinSwitch] [int] NULL,
[ControlBoardSerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HolsterBoardSerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LCDBoardSerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DC1BoardSerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DC2BoardSerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedBoardSerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BayWirelessBoardSerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bay1BoardSerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bay2BoardSerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bay3BoardSerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bay4BoardSerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedBoardDrawerOpenTime] [int] NULL,
[MedBoardDrawerCount] [int] NULL,
[MedBoardMotorUp] [int] NULL,
[MedBoardMotorDown] [int] NULL,
[MedBoardUnlockCount] [int] NULL,
[MedBoardAdminPin] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedBoardNarcPin] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedBoardUserPin1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedBoardUserPin2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedBoardUserPin3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedBoardUserPin4] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MedBoardUserPin5] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GenericError] [int] NULL,
[CommandCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceIPAddress] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeasuredVoltage] [numeric] (18, 2) NULL,
[BatteryErrorCode] [int] NULL,
[HasError] [bit] NULL,
[ErrorCodeId] [int] NULL,
[ErrorMessage] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [ClusteredIndex-20160204-145349] ON [dbo].[PacketErrorTable] ([BatterySerialNumber]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20160930-171208] ON [dbo].[PacketErrorTable] ([CreatedDateUTC]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20160930-171329] ON [dbo].[PacketErrorTable] ([ErrorCodeId]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'Activity'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Current; Originates from ''amps'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'Amps'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'APMAC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'WiFi Authentication Type; Originates from ''auth'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'AuthType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Backup Battery Charger Status; Originates from ''bubs'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'BackupBatteryStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Backup Battery Voltage; Originates from ''bubv'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'BackupBatteryVoltage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Charging Status Flags; Originates from ''bcs'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'BatteryChargeStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'BatteryErrorCode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Battery Mode Flags; Originates from ''bbm'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'BatteryMode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Name; Originates from ''nam'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'BatteryName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Operation Status Flags; Originates from ''bos'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'BatteryOpStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Safety Alert Flags; Originates from ''bsa'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'BatterySafetyAlert'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Serial Number; Originates from ''bsn'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'BatterySerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Battery Status Flags; Originates from ''bbs'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'BatteryStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Bay Number; Originates from ''bay'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'Bay'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Bay 1 Board Serial Number; Originates from ''b1s'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'Bay1BoardSerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'Bay1ChargerRevision'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Bay 2 Board Serial Number; Originates from ''b2s'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'Bay2BoardSerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'Bay2ChargerRevision'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Bay 3 Board Serial Number; Originates from ''b3s'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'Bay3BoardSerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'Bay3ChargerRevision'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Bay 4 Board Serial Number; Originates from ''b4s'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'Bay4BoardSerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'Bay4ChargerRevision'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Bay Wireless Board Serial Number; Originates from ''bws'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'BayWirelessBoardSerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Bay Wireless Firmware Revision; Originates from ''bwr'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'BayWirelessRevision'
GO
EXEC sp_addextendedproperty N'MS_Description', N'WiFi Channel Number; Originates from ''cnum'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'ChannelNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Charge Level; Originates from ''cl'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'ChargeLevel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Command Code + 1000; Originates from ''cmc'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'CommandCode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'ControlBoardRevision'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Cart Board Serial Number; Originates from ''cbs'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'ControlBoardSerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Cycle Count; Originates from ''cc'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'CycleCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 1 Board Serial Number; Originates from ''d1bs'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'DC1BoardSerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 1 Errors; Originates from ''dc1e'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'DC1Error'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 1 Status; Originates from ''dc1s'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'DC1Status'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 2 Board Serial Number; Originates from ''d2bs'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'DC2BoardSerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 2 Errors; Originates from ''dc2e'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'DC2Error'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 2 Status; Originates from ''dc2s'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'DC2Status'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 1 Firmware Revision; Originates from ''d1r'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'DCBoardOneRevision'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 2 Firmware Revision; Originates from ''d2r'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'DCBoardTwoRevision'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 1 Output Current A; Originates from ''da1c'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'DCUnit1ACurrent'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 1 Output Voltage A; Originates from ''da1v'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'DCUnit1AVolts'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 1 Output Current B; Originates from ''db1c'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'DCUnit1BCurrent'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 1 Output Voltage B; Originates from ''db1v'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'DCUnit1BVolts'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 2 Output Current A; Originates from ''da2c'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'DCUnit2ACurrent'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 2 Output Voltage A; Originates from ''da2v'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'DCUnit2AVolts'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 2 Output Current B; Originates from ''db2c'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'DCUnit2BCurrent'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DC 2 Output Voltage B; Originates from ''db2v'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'DCUnit2BVolts'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'DeviceMAC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'DeviceSerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Equivalent to AssetTypeId. Originates from ''type'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'DeviceType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'WiFi DHCP; Originates from ''dhcp'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'DHCP'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'Efficiency'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'ErrorCodeId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'ErrorMessage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery FET Status; Originates from ''fet'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'FETStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'FixedBatterySerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery FCC; Originates from ''fcc'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'FullChargeCapacity'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'GenericError'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'HasError'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Holster Board Serial Number; Originates from ''hbs'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'HolsterBoardSerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Holster Firmware Revision; Originates from ''hcr'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'HolsterChargerRevision'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'IP'
GO
EXEC sp_addextendedproperty N'MS_Description', N'AC Connected; Originates from ''acs'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'IsAC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'IsSessionData'
GO
EXEC sp_addextendedproperty N'MS_Description', N'No Keyboard Flag; Originates from ''K'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'KeyboardFailureNotification'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Transmission Good Flag; Originates from ''tg'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'LastTransmissionStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'LCD Board Serial Number; Originates from ''lbs'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'LCDBoardSerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'LCD Firmware Revision; Originates from ''lcr'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'LCDRevision'
GO
EXEC sp_addextendedproperty N'MS_Description', N'WiFi Link Quality; Originates from ''lq'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'LinkQuality'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'MaxCycleCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'MeasuredVoltage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Medboard Admin Pin; Originates from ''admp'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'MedBoardAdminPin'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Medboard Drawer Count; Originates from ''mdc'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'MedBoardDrawerCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Medboard Drawer Open Time; Originates from ''dot'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'MedBoardDrawerOpenTime'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Medboard Motor Down Current; Originates from ''mmdc'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'MedBoardMotorDown'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Medboard Motor Up Current; Originates from ''mmuc'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'MedBoardMotorUp'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Medboard Narc Pin; Originates from ''nrcp'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'MedBoardNarcPin'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Medboard Firmware Revision; Originates from ''mbr'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'MedBoardRevision'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Medboard Board Serial Number; Originates from ''mbs'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'MedBoardSerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Medboard Unlock Count; Originates from ''ulc'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'MedBoardUnlockCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Medboard User Pin 1; Originates from ''up1'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'MedBoardUserPin1'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Medboard User Pin 2; Originates from ''up2'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'MedBoardUserPin2'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Medboard User Pin 3; Originates from ''up3'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'MedBoardUserPin3'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Medboard User Pin 4; Originates from ''up4'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'MedBoardUserPin4'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Medboard User Pin 5; Originates from ''up5'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'MedBoardUserPin5'
GO
EXEC sp_addextendedproperty N'MS_Description', N'No Mouse Flag; Originates from ''M'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'MouseFailureNotification'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Move Flag; Originates from ''mov'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'Move'
GO
EXEC sp_addextendedproperty N'MS_Description', N'WiFi Pass Code; Originates from ''pasc'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'PassCode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery PF Alert Flags; Originates from ''pfa'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'PermanentFailureAlert'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery PF Status Flags; Originates from ''pfs'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'PermanentFailureStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'WiFi IP Port Number; Originates from ''ipp'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'PortNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'QueryStringBatchID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'QueryStringID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Remaining Time; Originates from ''rt'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'RemainingTime'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Safety Status Flags; Originates from ''bss'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'SafetyStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'SourceIPAddress'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'SourceTimestampUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'SparePinSwitch'
GO
EXEC sp_addextendedproperty N'MS_Description', N'WiFi SSID; Originates from ''ssid'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'SSID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'WiFi Static IP Address; Originates from ''sip'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'StaticIP'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Temperature; Originates from ''temp'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'Temperature'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Voltage; Originates from ''vol'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'Voltage'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Cell 1 Voltage; Originates from ''c1v'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'VoltageCell1'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Cell 2 Voltage; Originates from ''c2v'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'VoltageCell2'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Cell 3 Voltage; Originates from ''c3v'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'VoltageCell3'
GO
EXEC sp_addextendedproperty N'MS_Description', N'WiFi WEP Key; Originates from ''wep'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'WEPKey'
GO
EXEC sp_addextendedproperty N'MS_Description', N'WiFi Firmware Revision; Originates from ''wfr'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'WifiFirmwareRevision'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Windows Shut Down Flag; Originates from ''W'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'WindowsShutdownNotification'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery X Max Value; Originates from ''xm'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'XMax'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery X Value; Originates from ''xv'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'XValue'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Y Max Value; Originates from ''ym'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'YMax'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Y Value; Originates from ''yv'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'YValue'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Z Max Value; Originates from ''zm'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'ZMax'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Z Value; Originates from ''zv'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'PacketErrorTable', 'COLUMN', N'ZValue'
GO
