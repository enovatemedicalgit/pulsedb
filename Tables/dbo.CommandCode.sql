CREATE TABLE [dbo].[CommandCode]
(
[ROW_ID] [int] NOT NULL IDENTITY(1, 1),
[Description] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CommandCodeText] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StingerOnly] [bit] NOT NULL CONSTRAINT [DF_CommandCode_StingerOnly] DEFAULT ((1)),
[IsBatteryCommand] [bit] NOT NULL CONSTRAINT [DF_CommandCode_IsBatteryCommand] DEFAULT ((0)),
[X_ID] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CommandCode] ADD CONSTRAINT [PK_CommandCode] PRIMARY KEY CLUSTERED  ([ROW_ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Not used', 'SCHEMA', N'dbo', 'TABLE', N'CommandCode', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'CommandCode', 'COLUMN', N'CommandCodeText'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'CommandCode', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'CommandCode', 'COLUMN', N'IsBatteryCommand'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'CommandCode', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'CommandCode', 'COLUMN', N'StingerOnly'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'CommandCode', 'COLUMN', N'X_ID'
GO
