CREATE TABLE [dbo].[AlertSubscription]
(
[AlertSubscriptionId] [int] NOT NULL IDENTITY(1, 1),
[AlertSubscribeId] [int] NOT NULL,
[UserID] [int] NOT NULL,
[SiteID] [int] NOT NULL,
[CreatedDateUTC] [datetime] NOT NULL CONSTRAINT [DF_AlertSubscription_CreatedDateUTC] DEFAULT (getutcdate()),
[CreatedUserID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AlertSubscription] ADD CONSTRAINT [PK_AlertSubscription] PRIMARY KEY CLUSTERED  ([AlertSubscriptionId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_AlertSubscription] ON [dbo].[AlertSubscription] ([AlertSubscribeId], [UserID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Table is not used', 'SCHEMA', N'dbo', 'TABLE', N'AlertSubscription', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertSubscription', 'COLUMN', N'AlertSubscribeId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertSubscription', 'COLUMN', N'AlertSubscriptionId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertSubscription', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertSubscription', 'COLUMN', N'CreatedUserID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertSubscription', 'COLUMN', N'SiteID'
GO
