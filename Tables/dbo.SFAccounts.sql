CREATE TABLE [dbo].[SFAccounts]
(
[AccountOwnerAlias] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentAccount] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BillingState] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerAcct] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SFAccounts', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'SFAccounts', 'COLUMN', N'AccountID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'SFAccounts', 'COLUMN', N'AccountName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'SFAccounts', 'COLUMN', N'AccountOwnerAlias'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'SFAccounts', 'COLUMN', N'BillingState'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'SFAccounts', 'COLUMN', N'CustomerAcct'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'SFAccounts', 'COLUMN', N'ParentAccount'
GO
