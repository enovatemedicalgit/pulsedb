CREATE TABLE [dbo].[AssetStatus]
(
[IDAssetStatus] [int] NOT NULL,
[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LongDescription] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Descriptors table of asset status labels', 'SCHEMA', N'dbo', 'TABLE', N'AssetStatus', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetStatus', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetStatus', 'COLUMN', N'IDAssetStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AssetStatus', 'COLUMN', N'LongDescription'
GO
