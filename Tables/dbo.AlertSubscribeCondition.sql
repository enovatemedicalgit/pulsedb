CREATE TABLE [dbo].[AlertSubscribeCondition]
(
[AlertSubscribeConditionId] [int] NOT NULL IDENTITY(1, 1),
[AlertSubscribeId] [int] NOT NULL,
[AlertConditionId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AlertSubscribeCondition] ADD CONSTRAINT [PK_AlertSubscribeCondition] PRIMARY KEY CLUSTERED  ([AlertSubscribeConditionId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_AlertSubscribeCondition] ON [dbo].[AlertSubscribeCondition] ([AlertSubscribeConditionId], [AlertConditionId], [AlertSubscribeId]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Table is not used', 'SCHEMA', N'dbo', 'TABLE', N'AlertSubscribeCondition', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertSubscribeCondition', 'COLUMN', N'AlertConditionId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertSubscribeCondition', 'COLUMN', N'AlertSubscribeConditionId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'AlertSubscribeCondition', 'COLUMN', N'AlertSubscribeId'
GO
