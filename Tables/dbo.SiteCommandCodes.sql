CREATE TABLE [dbo].[SiteCommandCodes]
(
[SiteID] [int] NOT NULL,
[CommandCode] [int] NULL,
[LastRun] [datetime] NULL,
[FrequencyMinutes] [int] NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteCommandCodes', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Command Code + 1000; Originates from ''cmc'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'SiteCommandCodes', 'COLUMN', N'CommandCode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteCommandCodes', 'COLUMN', N'FrequencyMinutes'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SiteCommandCodes', 'COLUMN', N'LastRun'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'SiteCommandCodes', 'COLUMN', N'SiteID'
GO
