CREATE TABLE [dbo].[UserSites]
(
[SiteID] [int] NOT NULL,
[User_ROW_ID] [int] NOT NULL,
[IDUserSites] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserSites] ADD CONSTRAINT [PK_UserSites] PRIMARY KEY CLUSTERED  ([IDUserSites]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserSites] ADD CONSTRAINT [FK_UserSites_Sites] FOREIGN KEY ([SiteID]) REFERENCES [dbo].[Sites] ([IDSite])
GO
ALTER TABLE [dbo].[UserSites] ADD CONSTRAINT [FK_UserSites_User] FOREIGN KEY ([User_ROW_ID]) REFERENCES [dbo].[User] ([IDUser])
GO
