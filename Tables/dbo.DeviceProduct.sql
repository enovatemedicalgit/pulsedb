CREATE TABLE [dbo].[DeviceProduct]
(
[ProductTypeID] [int] NOT NULL,
[ProductSerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeviceSerialNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Active] [bit] NOT NULL,
[CreatedDateUTC] [datetime] NULL,
[CreatedUserID] [int] NOT NULL,
[DivorceDateUTC] [datetime] NULL,
[ModifiedUserID] [int] NULL,
[ROW_ID] [int] NOT NULL
) ON [PRIMARY]
GO
