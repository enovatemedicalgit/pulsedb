CREATE TABLE [dbo].[AssetsBattery]
(
[IDAsset] [int] NOT NULL IDENTITY(100000, 1),
[FullChargeCapacity] [int] NULL,
[CycleCount] [int] NULL,
[ChargeLevel] [int] NULL,
[MaxCycleCount] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AssetsBattery] ADD CONSTRAINT [pk_AssetsBattery] PRIMARY KEY CLUSTERED  ([IDAsset]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AssetsBattery] ADD CONSTRAINT [fk_AssetsBattery_Assets] FOREIGN KEY ([IDAsset]) REFERENCES [dbo].[tblAssets] ([IDAsset])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Table of devices derived from packet information.', 'SCHEMA', N'dbo', 'TABLE', N'AssetsBattery', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Charge Level; Originates from ''cl'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'AssetsBattery', 'COLUMN', N'ChargeLevel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Cycle Count; Originates from ''cc'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'AssetsBattery', 'COLUMN', N'CycleCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery FCC; Originates from ''fcc'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'AssetsBattery', 'COLUMN', N'FullChargeCapacity'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'AssetsBattery', 'COLUMN', N'IDAsset'
GO
EXEC sp_addextendedproperty N'MS_Description', N'For battery devices, estimates the number of cycles before the battery health begins to degrade', 'SCHEMA', N'dbo', 'TABLE', N'AssetsBattery', 'COLUMN', N'MaxCycleCount'
GO
