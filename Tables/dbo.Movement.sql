CREATE TABLE [dbo].[Movement]
(
[SiteID] [int] NOT NULL,
[DepartmentID] [int] NULL,
[DeviceSerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Date] [datetime] NULL,
[DateUTC] [datetime] NULL,
[HoursUtilized] [int] NOT NULL,
[Utilization] [decimal] (5, 2) NOT NULL,
[AvgRunRate] [int] NULL,
[ROW_ID] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Movement] ON [dbo].[Movement] ([Date]) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [ClusteredIndex-20150609-104537] ON [dbo].[Movement] ([DeviceSerialNumber]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20150609-104630] ON [dbo].[Movement] ([SiteID], [DepartmentID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Movement', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Movement', 'COLUMN', N'AvgRunRate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Movement', 'COLUMN', N'Date'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Movement', 'COLUMN', N'DateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Movement', 'COLUMN', N'DepartmentID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'Movement', 'COLUMN', N'DeviceSerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Movement', 'COLUMN', N'HoursUtilized'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'Movement', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'Movement', 'COLUMN', N'SiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Movement', 'COLUMN', N'Utilization'
GO
