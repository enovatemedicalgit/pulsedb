CREATE TABLE [dbo].[BusinessUnit]
(
[CreatedDateUTC] [datetime] NULL,
[BUType] [int] NULL,
[Address1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Zip] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SiteDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BedCount] [int] NULL,
[IsStinger] [bit] NOT NULL,
[AlertEmailList] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentBusinessUnit] [int] NOT NULL,
[PrimaryContact] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedUserID] [int] NULL,
[ModifiedDateUTC] [datetime] NULL,
[ModifiedUserID] [int] NULL,
[ROW_ID] [int] NOT NULL,
[TimeZoneID] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DaylightSavingsFlag] [bit] NULL,
[CustomerCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SalesforceID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusReason] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StingerNotes] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Site information', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'AccountType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'Address1'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'Address2'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'AlertEmailList'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'BedCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'BUType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'City'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'CreatedUserID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'CustomerCode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'DaylightSavingsFlag'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'IsStinger'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'ModifiedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'ModifiedUserID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'Notes'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'ParentBusinessUnit'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'PhoneNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'PrimaryContact'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'SalesforceID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'SiteDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'State'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'Status'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'StatusReason'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'StingerNotes'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'TimeZoneID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'BusinessUnit', 'COLUMN', N'Zip'
GO
