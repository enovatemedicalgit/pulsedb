CREATE TABLE [dbo].[SummaryWorkstation]
(
[ROW_ID] [int] NOT NULL IDENTITY(1, 1),
[Date] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SerialNo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AssetNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IDSite] [int] NOT NULL,
[IDDepartment] [int] NOT NULL,
[Floor] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Wing] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Other] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Utilization] [decimal] (6, 4) NOT NULL,
[AvgRunRate] [int] NOT NULL,
[SessionCount] [int] NOT NULL,
[LoChargeInsertsCount] [int] NOT NULL,
[HiChargeRemovalsCount] [int] NOT NULL,
[AvgAmpDraw] [decimal] (18, 2) NULL,
[AvgEstimatedPCUtilization] [decimal] (18, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SummaryWorkstation] ADD CONSTRAINT [PK_SummaryWorkstation] PRIMARY KEY CLUSTERED  ([ROW_ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWorkstation', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'customer''s internal id for the device', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWorkstation', 'COLUMN', N'AssetNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'the avg amp draw for the device over 24 hours', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWorkstation', 'COLUMN', N'AvgAmpDraw'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWorkstation', 'COLUMN', N'AvgEstimatedPCUtilization'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mean of Run Rate', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWorkstation', 'COLUMN', N'AvgRunRate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'the day portion of the report date', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWorkstation', 'COLUMN', N'Date'
GO
EXEC sp_addextendedproperty N'MS_Description', N'the assigned floor of the device based on the last communication', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWorkstation', 'COLUMN', N'Floor'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of insert packets where the battery device is charged over 10 percent', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWorkstation', 'COLUMN', N'HiChargeRemovalsCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to Department', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWorkstation', 'COLUMN', N'IDDepartment'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to Site', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWorkstation', 'COLUMN', N'IDSite'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of insert packets where the battery device is charged under 10 percent', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWorkstation', 'COLUMN', N'LoChargeInsertsCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWorkstation', 'COLUMN', N'Other'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWorkstation', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The devices serial number', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWorkstation', 'COLUMN', N'SerialNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The number of sessions for the device over 24 hours', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWorkstation', 'COLUMN', N'SessionCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWorkstation', 'COLUMN', N'Status'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWorkstation', 'COLUMN', N'Utilization'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Location at a particular site', 'SCHEMA', N'dbo', 'TABLE', N'SummaryWorkstation', 'COLUMN', N'Wing'
GO
