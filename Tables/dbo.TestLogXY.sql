CREATE TABLE [dbo].[TestLogXY]
(
[workstations] [int] NULL,
[chargers] [int] NULL,
[batteries] [int] NULL,
[LogDate] [datetime] NULL CONSTRAINT [DF_TestLogXY_LogDate] DEFAULT (getutcdate()),
[TestSiteID] [int] NULL,
[WorkstationsOriginal] [int] NULL,
[ChargersOriginal] [int] NULL,
[BatteriesOriginal] [int] NULL,
[WorkstationsAllocated] [int] NULL,
[ChargersAllocated] [int] NULL,
[BatteriesAllocated] [int] NULL,
[WorkstationsAllocatedOriginal] [int] NULL,
[ChargersAllocatedOriginal] [int] NULL,
[BatteriesAllocatedOriginal] [int] NULL,
[WorkstationsAllocatedDepartment] [int] NULL,
[ChargersAllocatedDepartment] [int] NULL,
[BatteriesAllocatedDepartment] [int] NULL,
[WorkstationsAllocatedDepartmentOriginal] [int] NULL,
[ChargersAllocatedDepartmentOriginal] [int] NULL,
[BatteriesAllocatedDepartmentOriginal] [int] NULL,
[SiteReplicatedID] [int] NULL
) ON [PRIMARY]
GO
