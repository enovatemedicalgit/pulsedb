CREATE TABLE [dbo].[UserOptions]
(
[ROW_ID] [int] NOT NULL IDENTITY(1, 1),
[User_ROW_ID] [int] NOT NULL,
[OptionKey] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OptionValue] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserOptions] ADD CONSTRAINT [PK_UserOptions] PRIMARY KEY CLUSTERED  ([ROW_ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserOptions] ADD CONSTRAINT [FK_UserOptions_User] FOREIGN KEY ([User_ROW_ID]) REFERENCES [dbo].[User] ([IDUser])
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'UserOptions', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'UserOptions', 'COLUMN', N'OptionKey'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'UserOptions', 'COLUMN', N'OptionValue'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'UserOptions', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'UserOptions', 'COLUMN', N'User_ROW_ID'
GO
