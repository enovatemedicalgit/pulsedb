CREATE TABLE [dbo].[SalesTerritories]
(
[TerritoryId] [tinyint] NOT NULL,
[TerritoryName] [varchar] (55) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SfAcctOwnerId] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegionId] [tinyint] NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SalesTerritories', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SalesTerritories', 'COLUMN', N'RegionId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SalesTerritories', 'COLUMN', N'SfAcctOwnerId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SalesTerritories', 'COLUMN', N'TerritoryId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SalesTerritories', 'COLUMN', N'TerritoryName'
GO
