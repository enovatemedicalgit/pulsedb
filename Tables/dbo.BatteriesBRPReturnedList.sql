CREATE TABLE [dbo].[BatteriesBRPReturnedList]
(
[BatterySerialNumber] [float] NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'For Arlow**', 'SCHEMA', N'dbo', 'TABLE', N'BatteriesBRPReturnedList', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Serial Number; Originates from ''bsn'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'BatteriesBRPReturnedList', 'COLUMN', N'BatterySerialNumber'
GO
