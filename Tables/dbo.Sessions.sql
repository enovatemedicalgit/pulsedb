CREATE TABLE [dbo].[Sessions]
(
[StartDateUTC] [datetime2] (3) NULL,
[EndDateUTC] [datetime2] (3) NULL,
[DeviceType] [int] NULL,
[DeviceSerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BatterySerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AvgAmpDraw] [decimal] (18, 2) NULL,
[NumberOfMoves] [int] NULL,
[SessionLengthMinutes] [int] NULL,
[BatteryName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SiteID] [int] NULL,
[PacketCount] [int] NULL,
[ChargePctConsumed] [int] NULL,
[RunRate] [int] NULL,
[ROW_ID] [int] NOT NULL IDENTITY(1, 1),
[StartChargeLevel] [int] NULL,
[EndChargeLevel] [int] NULL,
[HiAmpDraw] [decimal] (18, 2) NULL,
[LowAmpDraw] [decimal] (18, 2) NULL,
[EstimatedPCUtilization] [decimal] (18, 2) NULL,
[AvgSignalQuality] [decimal] (18, 2) NULL,
[RemainingCapacity] [decimal] (18, 2) NULL,
[DepartmentID] [int] NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [DSN] ON [dbo].[Sessions] ([DeviceSerialNumber]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [DType] ON [dbo].[Sessions] ([DeviceType]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [EDate] ON [dbo].[Sessions] ([EndDateUTC]) ON [PRIMARY]
GO
CREATE UNIQUE CLUSTERED INDEX [RowIDClust] ON [dbo].[Sessions] ([ROW_ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [SId] ON [dbo].[Sessions] ([SiteID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [<Name of Missing Index, sysname,>] ON [dbo].[Sessions] ([SiteID]) INCLUDE ([AvgAmpDraw], [BatterySerialNumber], [DeviceSerialNumber], [EndChargeLevel], [EndDateUTC], [NumberOfMoves], [PacketCount], [SessionLengthMinutes], [StartChargeLevel], [StartDateUTC]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [SDate] ON [dbo].[Sessions] ([StartDateUTC]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sessions', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sessions', 'COLUMN', N'AvgAmpDraw'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sessions', 'COLUMN', N'AvgSignalQuality'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Name; Originates from ''nam'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'Sessions', 'COLUMN', N'BatteryName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Serial Number; Originates from ''bsn'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'Sessions', 'COLUMN', N'BatterySerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sessions', 'COLUMN', N'ChargePctConsumed'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'Sessions', 'COLUMN', N'DeviceSerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Equivalent to AssetTypeId. Originates from ''type'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'Sessions', 'COLUMN', N'DeviceType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sessions', 'COLUMN', N'EndChargeLevel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sessions', 'COLUMN', N'EndDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sessions', 'COLUMN', N'EstimatedPCUtilization'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sessions', 'COLUMN', N'HiAmpDraw'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sessions', 'COLUMN', N'LowAmpDraw'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sessions', 'COLUMN', N'NumberOfMoves'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sessions', 'COLUMN', N'PacketCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sessions', 'COLUMN', N'RemainingCapacity'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'Sessions', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sessions', 'COLUMN', N'RunRate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sessions', 'COLUMN', N'SessionLengthMinutes'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'Sessions', 'COLUMN', N'SiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sessions', 'COLUMN', N'StartChargeLevel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Sessions', 'COLUMN', N'StartDateUTC'
GO
