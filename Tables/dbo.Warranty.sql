CREATE TABLE [dbo].[Warranty]
(
[ROW_ID] [int] NOT NULL IDENTITY(1, 1),
[EffectiveDate] [date] NOT NULL,
[ExpirationDate] [date] NOT NULL,
[EndDate] [date] NULL,
[DurationDays] [int] NOT NULL,
[SerialNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductTypeID] [int] NOT NULL,
[SerialNumberPreviouslyCovered] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SerialNumberSubsequentlyCovered] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WarrantyStartCode] [int] NOT NULL,
[WarrantyEndCode] [int] NOT NULL,
[Notes] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Retired] [bit] NOT NULL,
[PONumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoiceNo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Warranty] ADD CONSTRAINT [PK_Warranty_TEST] PRIMARY KEY CLUSTERED  ([ROW_ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_warranty_test_PO_SN] ON [dbo].[Warranty] ([SerialNumber], [PONumber]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_warranty_code_serial] ON [dbo].[Warranty] ([SerialNumber], [WarrantyStartCode], [WarrantyEndCode]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Warranty_warrantyEndCode] ON [dbo].[Warranty] ([WarrantyEndCode]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Warranty_warrantyStartCode] ON [dbo].[Warranty] ([WarrantyStartCode]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Warranty', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Warranty', 'COLUMN', N'DurationDays'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Warranty', 'COLUMN', N'EffectiveDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Warranty', 'COLUMN', N'EndDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Warranty', 'COLUMN', N'ExpirationDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Warranty', 'COLUMN', N'InvoiceNo'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Warranty', 'COLUMN', N'Notes'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Warranty', 'COLUMN', N'PONumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Warranty', 'COLUMN', N'ProductTypeID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Warranty', 'COLUMN', N'Retired'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'Warranty', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Warranty', 'COLUMN', N'SerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Warranty', 'COLUMN', N'SerialNumberPreviouslyCovered'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Warranty', 'COLUMN', N'SerialNumberSubsequentlyCovered'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Warranty', 'COLUMN', N'WarrantyEndCode'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'Warranty', 'COLUMN', N'WarrantyStartCode'
GO
