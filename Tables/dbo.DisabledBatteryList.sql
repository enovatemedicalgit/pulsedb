CREATE TABLE [dbo].[DisabledBatteryList]
(
[SerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaxCycleCount] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bay] [int] NULL,
[DeviceSerial] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastUpdateDateTime] [datetime] NULL,
[LastPacketDate] [datetime] NULL,
[SiteID] [int] NULL,
[ManualEntryUser] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InsertedDateUTC] [datetime] NULL,
[SiteDescription] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CommandSentOn] [datetime] NULL
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [ClusteredIndex-20160714-132535] ON [dbo].[DisabledBatteryList] ([SerialNumber]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'DisabledBatteryList', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Bay Number; Originates from ''bay'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'DisabledBatteryList', 'COLUMN', N'Bay'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'DisabledBatteryList', 'COLUMN', N'CommandSentOn'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'DisabledBatteryList', 'COLUMN', N'DeviceSerial'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'DisabledBatteryList', 'COLUMN', N'InsertedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'DisabledBatteryList', 'COLUMN', N'LastPacketDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'DisabledBatteryList', 'COLUMN', N'LastUpdateDateTime'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'DisabledBatteryList', 'COLUMN', N'ManualEntryUser'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'DisabledBatteryList', 'COLUMN', N'MaxCycleCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'DisabledBatteryList', 'COLUMN', N'SerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'DisabledBatteryList', 'COLUMN', N'SiteDescription'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from Manual', 'SCHEMA', N'dbo', 'TABLE', N'DisabledBatteryList', 'COLUMN', N'SiteID'
GO
