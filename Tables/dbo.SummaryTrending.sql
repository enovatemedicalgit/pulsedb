CREATE TABLE [dbo].[SummaryTrending]
(
[Year] [int] NULL,
[YearUTC] [int] NULL,
[Month] [int] NULL,
[MonthUTC] [int] NULL,
[Day] [int] NULL,
[DayUTC] [int] NULL,
[Date] [datetime] NULL,
[DateUTC] [datetime] NULL,
[SiteID] [int] NULL,
[DepartmentID] [int] NULL,
[SessionCount] [int] NULL,
[AvgSessionLengthHours] [int] NULL,
[AvgSessionLengthMinutes] [int] NULL,
[AvgRunRate] [int] NULL,
[ActiveDeviceCount] [int] NULL,
[Utilization] [decimal] (6, 4) NOT NULL,
[NonSessionRecordCount] [int] NULL,
[CreatedDateUTC] [datetime] NULL,
[ROW_ID] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTrending', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Count of devices with active flag set to true', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTrending', 'COLUMN', N'ActiveDeviceCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mean of Run Rate', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTrending', 'COLUMN', N'AvgRunRate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mean of Session duration', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTrending', 'COLUMN', N'AvgSessionLengthHours'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mean of Session duration', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTrending', 'COLUMN', N'AvgSessionLengthMinutes'
GO
EXEC sp_addextendedproperty N'MS_Description', N'the report date', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTrending', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date dimension', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTrending', 'COLUMN', N'Date'
GO
EXEC sp_addextendedproperty N'MS_Description', N'not used', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTrending', 'COLUMN', N'DateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Day of month dimension', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTrending', 'COLUMN', N'Day'
GO
EXEC sp_addextendedproperty N'MS_Description', N'not used', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTrending', 'COLUMN', N'DayUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to the Department table', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTrending', 'COLUMN', N'DepartmentID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Month dimension', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTrending', 'COLUMN', N'Month'
GO
EXEC sp_addextendedproperty N'MS_Description', N'not used', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTrending', 'COLUMN', N'MonthUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Count of the number of regular communication packets not including action packets', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTrending', 'COLUMN', N'NonSessionRecordCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTrending', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Count of sessions.  A session begins when a battery is inserted and ends when it is pulled.', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTrending', 'COLUMN', N'SessionCount'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to the Site table', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTrending', 'COLUMN', N'SiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTrending', 'COLUMN', N'Utilization'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Year dimension', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTrending', 'COLUMN', N'Year'
GO
EXEC sp_addextendedproperty N'MS_Description', N'not used', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTrending', 'COLUMN', N'YearUTC'
GO
