CREATE TABLE [dbo].[CommandQueue]
(
[CreatedDateUTC] [datetime] NOT NULL CONSTRAINT [DF_CommandQueue_CreatedDateUTC] DEFAULT (getutcdate()),
[SerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CommandText] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SentDateUTC] [datetime] NOT NULL CONSTRAINT [DF_CommandQueue_SentDateUTC] DEFAULT (((1)/(1))/(1900)),
[CompleteDateUTC] [datetime] NOT NULL CONSTRAINT [DF_CommandQueue_CompleteDateUTC] DEFAULT (((1)/(1))/(1900)),
[NotificationConditionID] [int] NOT NULL CONSTRAINT [DF_CommandQueue_NotificationConditionID] DEFAULT ((0)),
[ROW_ID] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CommandQueue] ADD CONSTRAINT [PK_CommandQueue] PRIMARY KEY CLUSTERED  ([ROW_ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'For Arlow**', 'SCHEMA', N'dbo', 'TABLE', N'CommandQueue', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'CommandQueue', 'COLUMN', N'CommandText'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'CommandQueue', 'COLUMN', N'CompleteDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'CommandQueue', 'COLUMN', N'CreatedDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'NotificationConditionID is required so we do not ''', 'SCHEMA', N'dbo', 'TABLE', N'CommandQueue', 'COLUMN', N'NotificationConditionID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'CommandQueue', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'CommandQueue', 'COLUMN', N'SentDateUTC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'CommandQueue', 'COLUMN', N'SerialNumber'
GO
