CREATE TABLE [dbo].[SummaryBatteryRunRate]
(
[Date] [date] NOT NULL,
[SiteID] [int] NOT NULL,
[BatterySerialNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AvgRunRate] [int] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SummaryBatteryRunRate', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mean of Run Rate', 'SCHEMA', N'dbo', 'TABLE', N'SummaryBatteryRunRate', 'COLUMN', N'AvgRunRate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Battery Serial Number; Originates from ''bsn'' key value pair in packet string', 'SCHEMA', N'dbo', 'TABLE', N'SummaryBatteryRunRate', 'COLUMN', N'BatterySerialNumber'
GO
EXEC sp_addextendedproperty N'MS_Description', N'the report date', 'SCHEMA', N'dbo', 'TABLE', N'SummaryBatteryRunRate', 'COLUMN', N'Date'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to the Site table', 'SCHEMA', N'dbo', 'TABLE', N'SummaryBatteryRunRate', 'COLUMN', N'SiteID'
GO
