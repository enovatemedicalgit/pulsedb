CREATE TABLE [staging].[SFAccountOwners]
(
[SalesforceAcctOwnerID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Region__c] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'staging', 'TABLE', N'SFAccountOwners', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'staging', 'TABLE', N'SFAccountOwners', 'COLUMN', N'Email'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'staging', 'TABLE', N'SFAccountOwners', 'COLUMN', N'FirstName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'staging', 'TABLE', N'SFAccountOwners', 'COLUMN', N'LastName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'staging', 'TABLE', N'SFAccountOwners', 'COLUMN', N'Region__c'
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'staging', 'TABLE', N'SFAccountOwners', 'COLUMN', N'SalesforceAcctOwnerID'
GO
