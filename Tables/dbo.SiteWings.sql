CREATE TABLE [dbo].[SiteWings]
(
[IdSiteWing] [int] NOT NULL IDENTITY(1, 1),
[SiteId] [int] NOT NULL,
[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultForSite] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SiteWings] ADD CONSTRAINT [PK_SiteWings] PRIMARY KEY CLUSTERED  ([IdSiteWing]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SiteWings] ADD CONSTRAINT [FK_SiteWings_Sites] FOREIGN KEY ([SiteId]) REFERENCES [dbo].[Sites] ([IDSite])
GO
