CREATE TABLE [dbo].[Terms]
(
[IDTerm] [int] NULL,
[TermDescription] [nvarchar] (350) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TermKeywords] [nvarchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
