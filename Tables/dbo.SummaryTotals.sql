CREATE TABLE [dbo].[SummaryTotals]
(
[ROW_ID] [int] NOT NULL IDENTITY(1, 1),
[HQID] [int] NOT NULL,
[SiteID] [int] NULL,
[SortKey] [int] NOT NULL,
[BatteryCount_Purchased] [int] NOT NULL,
[BatteryCount_Active] [int] NOT NULL,
[ChargerCount_Purchased] [int] NOT NULL,
[ChargerCount_Active] [int] NOT NULL,
[WorkstationCount_Purchased] [int] NOT NULL,
[WorkstationCount_Active] [int] NOT NULL,
[SummaryDate] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SummaryTotals] ADD CONSTRAINT [PK_SummaryTotals] PRIMARY KEY CLUSTERED  ([ROW_ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTotals', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Total number of batteries that have communicated from the customer', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTotals', 'COLUMN', N'BatteryCount_Active'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Total number of batteries that have been assigned to the customer', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTotals', 'COLUMN', N'BatteryCount_Purchased'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Total number of chargers that have communicated from the customer', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTotals', 'COLUMN', N'ChargerCount_Active'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Total number of chargers that have been assigned to the customer', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTotals', 'COLUMN', N'ChargerCount_Purchased'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Identifier for the customers headquarters and network domain', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTotals', 'COLUMN', N'HQID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Originates from SSIS', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTotals', 'COLUMN', N'ROW_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to the Site table', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTotals', 'COLUMN', N'SiteID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Used to sort the records', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTotals', 'COLUMN', N'SortKey'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The report date', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTotals', 'COLUMN', N'SummaryDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of workstations that have communicated from the customer', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTotals', 'COLUMN', N'WorkstationCount_Active'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of workstations that have been assigned to the customer', 'SCHEMA', N'dbo', 'TABLE', N'SummaryTotals', 'COLUMN', N'WorkstationCount_Purchased'
GO
