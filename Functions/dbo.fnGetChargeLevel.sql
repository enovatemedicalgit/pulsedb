SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnGetChargeLevel]
(  @BattSN varchar(50) = null
)
RETURNS int
AS
BEGIN
	
	--test variables
	--declare @BattSN varchar(50) = '3118313130808659'
	--declare @FCC int = 3099
	
	DECLARE @ChargeLevel AS INT
	SET @ChargeLevel = (SELECT TOP 1 chargelevel FROM dbo.SessionDataCurrent WHERE BatterySerialNumber = @BattSN AND CreatedDateUTC > DATEADD(MINUTE,-20,getutcdate()) AND chargelevel IS NOT NULL AND chargelevel > 0 )
	RETURN @ChargeLevel
END


GO
