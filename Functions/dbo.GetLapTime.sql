SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE FUNCTION [dbo].[GetLapTime]
(
	@StartTime AS DATETIME2
)
RETURNS VARCHAR(255)
AS
BEGIN

    RETURN 'Step Completed in : ' 
		  +  RIGHT('0' +CONVERT(VARCHAR(3),DATEDIFF(MINUTE,@StartTime, GETDATE())/60),2) + ':' 
		  +  RIGHT('0' + CONVERT(VARCHAR(2),DATEDIFF(SECOND,@STartTime,GETDATE())/60),2)+ ':' 
		  +  RIGHT('0' + CONVERT(VARCHAR(2),DATEDIFF(SECOND,@STartTime,GETDATE())%60),2) +
		   CHAR(10) + '####################################################################################################'

END


GO
