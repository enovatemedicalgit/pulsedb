SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[PacketToXml] 
(
		@OriginalPacket VARCHAR(MAX)
)
RETURNS XML
AS


BEGIN
	DECLARE -- @OriginalPacket VARCHAR(MAX), 
	@PacketLen int, @CurrentElement VARCHAR(MAX),  @RemainingPacket VARCHAR(MAX), @ElementName VARCHAR(200), @ElementValue VARCHAR(200), @XMLString VARCHAR(MAX)
	

	--SET @OriginalPacket =  'type=1&a=0&bsn=811516030736&csn=311723300606&fcc=25010&vol=10829&amps=3966&temp=85&cl=31&cc=11&c1v=3612&c2v=3612&c3v=3604&da1v=12402&da1c=&db1v=19042&db1c=0&da2v12314&da2c=0&db2v=13768&fet=6&cbr=6.75&wfr=2.32&lq=100&ip=172.28.176.99&mac=54781addb669&bbs=135&bubs=4&bss=0000&pfa=0000&bsa=0000&bos=-8189&bcs=0200&bbm=1'
	
	SET @RemainingPacket = @OriginalPacket
	SET @XMLString = ''
	WHILE LEN(@RemainingPacket) > 0 
	Begin
		IF	CHARINDEX('&',@RemainingPacket,0) = 0
			BEGIN
				SET @CurrentElement = SUBSTRING(@RemainingPacket,0,Len(@RemainingPacket)+1)
				SET @RemainingPacket = Substring(@RemainingPacket,Len(@RemainingPacket),0)
			END
		ELSE
			BEGIN
				SET @CurrentElement = SUBSTRING(@RemainingPacket,0,CHARINDEX('&',@RemainingPacket,0))
				SET @RemainingPacket = Substring(@RemainingPacket,LEN(@CurrentElement)+2,Len(@RemainingPacket) - LEN(@CurrentElement))
			END
	
		IF CHARINDEX('=',@CurrentElement,0) = 0
			BEGIN
				SET @ElementName = 'BadValuePair'
				SET @ElementValue = REPLACE(@CurrentElement,'%3a',':')
			END
		ELSE
			BEGIN
				SET @ElementName = SUBSTRING(@CurrentElement,0,CHARINDEX('=',@CurrentElement,0))
				SET @ElementValue = REPLACE(SUBSTRING(@CurrentElement,CHARINDEX('=',@CurrentElement,0)+1, 2000),'%3a',':')
			END
		--select @ElementName
		--Select @ElementValue

		SET @XMLString = @XMLString + '<'+ @ElementName +'>'+ @ElementValue +'</' + @ElementName +'>'
		--SELECT CAST('<packet>' + @XMLString + '</packet>' as XML)
	
	END 
	

	-- SELECT CAST('<packet>' + @XMLString + '</packet>' as VARCHAR(MAX))


	RETURN CAST('<packet>' + @XMLString + '</packet>' as XML) 


	

END
--

--Select TOP 100  Query,  dbo.PacketToXml([Query]) as PacketAsXML From QueryString
GO
