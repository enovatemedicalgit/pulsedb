SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create FUNCTION [dbo].[IsPriorityIP] (@IP varchar(20))
RETURNS int
AS
BEGIN
	DECLARE @retval int
	SET @retval = (select count(*) from PriorityIP where @IP like SourceIPAddress_Left + '%')
	RETURN @retval
END



GO
