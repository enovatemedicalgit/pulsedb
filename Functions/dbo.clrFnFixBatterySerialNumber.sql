SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE FUNCTION [dbo].[clrFnFixBatterySerialNumber] (@batterySerialNumber [nvarchar] (50), @batteryName [nvarchar] (50))
RETURNS [nvarchar] (50)
WITH EXECUTE AS CALLER
EXTERNAL NAME [SqlCLRUtils].[Utils].[FixBatterySerialNumber]
GO
