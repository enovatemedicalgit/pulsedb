SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[fnHourlyUtilizationByHQ]
(	
	@FromDate datetime,
	@ToDate datetime,
	@HQID int
)
RETURNS TABLE 
AS
RETURN 
(

	--declare @FromDate datetime
	--declare @ToDate datetime
	--declare @HQID int
	--set @FromDate = '12/16/2011 12:00 AM'
	--set @ToDate = '12/17/2011 12:00 AM'	
	--set @HQID = 53


	select start_date as [Date], convert(varchar, start_date, 101) + ' ' + right('0' + cast(DATEPART(hh, start_date) as varchar), 2) + ':00' as DateAndHour, @HQID as HQID, bu.IDSite as SiteID, bu.SiteDescription as Description, avg(coalesce(HourlyUtilization, 0)) as HourlyUtilization
	from generateDateTable(convert(varchar(10), @FromDate, 101) + ' ' + cast(datepart(hh, @FromDate) as varchar) + ':00', convert(varchar(10), @ToDate, 101) + ' ' + cast(datepart(hh, @ToDate) as varchar) + ':00', 'hour', 1) timetable
	cross join (select IDSite, SiteDescription from Sites where idsite = @HQID) bu
	left join (
				select
					DATEADD(hh, [Hour], [Date]) as [Date] 
					,sad.SiteID
					,sad.DepartmentID
					,d.[Description]
					,sad.ActiveWorkstationCount
					,ut.UtilizedWorkstations
					,cast(cast((cast(ut.UtilizedWorkstations as decimal(7,4)) / sad.ActiveWorkstationCount) as decimal(6,4))*100 as decimal(6,2)) as HourlyUtilization

				from SummaryActiveAssets sad
				join (

						select SiteID, DepartmentID, [Date], [Hour], count(SerialNo) as UtilizedWorkstations
						from MovementDetail
						join Sites bu on MovementDetail.SiteID = bu.IDSite
						where bu.IDSite = @HQID
						and MovementDetail.Moves > 1 
						and [Date] between convert(varchar(10), @FromDate, 101) and convert(varchar(10), @ToDate, 101)
						group by MovementDetail.SiteID, DepartmentID, [Date], [Hour]

					) ut
					on sad.SiteID = ut.SiteID
					and sad.DepartmentID = ut.DepartmentID
					and sad.CreatedDateUTC = ut.[Date]
				join Sites bu on sad.SiteID = bu.IDSite
				join Departments d
				on sad.DepartmentID = d.IDDepartment
				where bu.CustomerID = @HQID
				and sad.CreatedDateUTC between convert(varchar(10), @FromDate, 101) and convert(varchar(10), @ToDate, 101)
				and sad.ActiveWorkstationCount > 0
			) utilization
	on timetable.start_date = utilization.[Date]
	and bu.IDSite = utilization.SiteID
	GROUP BY start_date, convert(varchar, start_date, 101) + ' ' + right('0' + cast(DATEPART(hh, start_date) as varchar), 2) + ':00', bu.IDSite, bu.SiteDescription


)


GO
