SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cha-Ron Murphy
-- Create date: 1/7/2015
-- Description:	Return min of 3 values
-- =============================================
CREATE FUNCTION [dbo].[fnMinValues] 
(
	@v1 decimal(18,2),
	@v2 decimal(18,2),
	@v3 decimal(18,2)
)
RETURNS decimal(18,2)
AS
BEGIN

	DECLARE @Result decimal(18,2)
	
		if (@v2 < @v1)
			set @Result=@v2
		else
		    set @Result=@v1
		    
		if (@v3 < @Result)
			set @Result= @v3


	RETURN @Result

END

GO
