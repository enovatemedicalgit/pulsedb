SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Coding Culprit: Deborah Cunningham
-- Create date: 12/18/2012
-- Description:	To avoid redundancy but keep 
-- the info available. This function will grab
-- the latest PONumber entered into the warranty
-- table by serial number. 
-- =============================================
Create FUNCTION [dbo].[MostRecentPONumberBySerialNo]
(	
	@SerialNumber varchar(50) 	
)
RETURNS TABLE 
AS
RETURN 
(	
	Select TOP 1 ROW_ID,
	(CASE WHEN w.ExpirationDate >= GETDATE() and w.WarrantyEndCode = 0 THEN 'Y' ELSE 'N' END) As InWarranty,
	w.EffectiveDate As WarEffDate,
	w.ExpirationDate As WarExpDate,
	w.EndDate As WarEndDate,
	--(Select dbo.GetWarrantyYrsFromSerialNumber(@SerialNumber) + ' Year') As BatteryType,
	--(Select dbo.GetWarrantyYrsFromSerialNumber('311801312120117')) As BatteryType,
	wc1.[Description] As StartDescription,
	wc2.[Description] As EndDescription,
	w.PONumber
	From Warranty w left join
	WarrantyCode wc1
	on w.WarrantyStartCode = wc1.WarrantyCode
	left join
	WarrantyCode wc2
	on w.WarrantyEndCode = wc2.WarrantyCode	
	--Where w.SerialNumber = '311801312120117' --@SerialNumber	
	where w.SerialNumber = @SerialNumber
	--Order by ROW_ID desc
)

GO
