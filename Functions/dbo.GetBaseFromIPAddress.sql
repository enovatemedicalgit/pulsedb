SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[GetBaseFromIPAddress] 
(
	-- Add the parameters for the function here
	@IPAddress VARCHAR(15)
)
RETURNS VARCHAR(12)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ResultVar VARCHAR(12)

	-- Add the T-SQL statements to compute the return value here
	Set @ResultVar = left(@IPaddress, len(@IPAddress) - charindex('.', reverse(@IPAddress)))

	-- Return the result of the function
	RETURN @ResultVar
END
GO
