SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:      <ENOVATE\arlow.farrell,ARLOWFARRELL>
-- Create date: <10/4/2016> <2:48 PM>
-- Description: 
--				Retrieves last Charger Bay Packet   
--
-- 
-- =============================================
CREATE FUNCTION [dbo].[fn_GetLastChargerBayPacketActivityOrChargeLebel]
(  @ChargerSerial VARCHAR(18),@Bay AS INT,@Act1C2Amps3 AS INT=1,@LastPacketDate AS DATETIME2 = getutcdate 
)
RETURNS int
AS
BEGIN	
DECLARE @returnVal AS INT = null;
 
     
		IF (@Act1C2Amps3 = 1)
		begin
				SET @returnVal = (SELECT top 1 VASD.ChargeLevel
				from dbo.NonSessionDataCurrent AS VASD where VASD.DeviceSerialNumber = @ChargerSerial AND bay = @Bay AND VASD.CreatedDateUTC = @LastPacketDate
				order by CreatedDateUtc DESC)
					RETURN @returnVal;
         END
		 ELSE IF (@Act1C2Amps3 = 2)
		 BEGIN
				SET @returnVal = (SELECT top 1 VASD.Activity
				from dbo.NonSessionDataCurrent  AS VASD where VASD.DeviceSerialNumber = @ChargerSerial AND bay = @Bay AND VASD.CreatedDateUTC = @LastPacketDate
				order by CreatedDateUtc DESC)
				RETURN @returnVal;
		 END       
		 	 ELSE IF (@Act1C2Amps3 = 3)
		 BEGIN
				SET @returnVal = (SELECT top 1 VASD.Amps
				from dbo.NonSessionDataCurrent  AS VASD where VASD.DeviceSerialNumber = @ChargerSerial AND bay = @Bay AND VASD.CreatedDateUTC = @LastPacketDate
				order by CreatedDateUtc DESC)
				RETURN @returnVal;
		 END   
		 RETURN @returnVal;
    END;



GO
