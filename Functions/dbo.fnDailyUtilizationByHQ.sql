SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[fnDailyUtilizationByHQ] 
(	
	@FromDate datetime,
	@ToDate datetime,
	@SiteID int
)
RETURNS TABLE 
AS
RETURN 
(

	--declare @FromDate datetime
	--declare @ToDate datetime
	--set @FromDate = '12/16/2011 12:00 AM'
	--set @ToDate = '12/22/2011 12:00 AM'	
	--declare @HQID int
	--set @HQID = 53

	select start_date as [Date],  bu.IDSite as SiteID, bu.SiteDescription as Description, avg(cast(coalesce(Utilization, 0) * 100 as decimal(6,2))) as Utilization, avg(coalesce(AvgRunRate, 0)) as AvgRunRate
	from generateDateTable(convert(varchar(10), @FromDate, 101), convert(varchar(10), dateadd(d, -1, @ToDate), 101), 'day', 1) timetable
	cross join (select IDSite, SiteDescription from Sites WHERE IDSite IN (SELECT idsite FROM sites WHERE customerid= @SiteID) OR  IDSite = @SiteID) bu
	left join (
				select
					[Date]
					,st.SiteID
					,AvgRunRate
					,Utilization
				from SummaryTrending st
				where st.SiteID IN (SELECT idsite FROM sites WHERE customerid= @SiteID) OR st.SiteID = @SiteID
				and st.[Date] between convert(varchar(10), @FromDate, 101) and convert(varchar(10), @ToDate, 101)
			) utilization
	on timetable.start_date = utilization.[Date]
	and bu.IDSite = utilization.SiteID
	GROUP BY start_date, bu.IDSite, bu.SiteDescription
)


GO
