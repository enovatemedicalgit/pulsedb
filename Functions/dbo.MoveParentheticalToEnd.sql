SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[MoveParentheticalToEnd] 
(
   @InputString VARCHAR(500)
    
)
RETURNS VARCHAR(500)
AS
BEGIN
    DECLARE @OpenParen INT
    DECLARE @CloseParen INT
    DECLARE @ParenString VARCHAR(200)



    SET @OpenParen = CHARINDEX('(',@InputString)
    SET @CloseParen = CHARINDEX(')',@InputString)
    IF @OpenParen = 1 AND @CloseParen > @OpenParen
	   BEGIN
		  SET @ParenString = SUBSTRING(@InputString, @OpenParen + 1, @CloseParen - @OpenParen - 1 )
		  RETURN LTRIM(RTRIM(RIGHT(@InputString, LEN(@InputString) - @CloseParen)))  + ' (' +  @ParenString  + ')'
	   END
    ELSE
	   BEGIN
		  RETURN @InputString
	   END

    RETURN @InputString
		 

END
GO
