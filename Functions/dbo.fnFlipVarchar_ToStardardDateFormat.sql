SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ======================================================
-- Description:	Ease of use when displaying dates
-- takes varchar(10) yyyy/mm/dd format and flips
-- it to mm/dd/yyyy for display purposes and retains
-- the varchar data type. This is for displaying purposes
-- within CAST. Some of the summary tables only have a 
-- Date field (not UTC) and is a varchar datatype.

-- Used again with Utilization Trending page for the Avg
-- Moves and Charge Utilization.
-- =======================================================
create FUNCTION [dbo].[fnFlipVarchar_ToStardardDateFormat] 
(
	@DateIn varchar(10)
)
RETURNS varchar(10)
AS
BEGIN
	--declare @dateIn varchar(10) = '2013/04/01'
		declare @DateOut varchar(10)
		declare @y varchar(5)
		declare @m varchar(3)
		declare @d varchar(2)
		declare @yyyymmdd varchar(10) = @dateIn 
		set @y = Substring (@yyyymmdd, 1 , 4)
		set @m = SUBSTRING(@yyyymmdd, 6, 8)
		set @d = SUBSTRING(@yyyymmdd, 9, 10)
		set @DateOut = @m + @d + '/' + @y
		return @DateOut

END

GO
