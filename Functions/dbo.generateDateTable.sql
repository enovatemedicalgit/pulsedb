SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:        Dan G. Switzer, II
-- Create date: Jan 8, 2009
-- Description:    Creates a table with a set of date ranges
-- =============================================
create function [dbo].[generateDateTable] 
(
    -- Add the parameters for the function here
    @start_date datetime
    , @end_date datetime
    , @datepart varchar(20) = 'day'
    , @step int = 1
)
returns @dates table 
(
    -- Add the column definitions for the TABLE variable here
    start_date datetime, 
    end_date datetime
)
as
begin
    -- if we're doing calculations based on the days (not time) then strip the time out
    if( @datepart in ('year', 'yy', 'yyyy', 'quarter', 'qq', 'q', 'month', 'mm', 'm', 'dayofyear', 'dy', 'y', 'day', 'dd', 'd', 'week', 'wk', 'ww') )
    begin
        set @start_date = cast(floor(cast(@start_date as float)) as datetime)
        set @end_date = cast(floor(cast(@end_date as float)) as datetime)
    end

    declare @new_start datetime

    while @start_date <= @end_date
    begin
        -- get the new starting row
        set @new_start = (case 
            when @datepart in ('year', 'yy', 'yyyy') then dateadd(yy, @step, @start_date)
            when @datepart in ('quarter', 'qq', 'q') then dateadd(qq, @step, @start_date)
            when @datepart in ('month', 'mm', 'm') then dateadd(mm, @step, @start_date) 
            when @datepart in ('dayofyear', 'dy', 'y') then dateadd(dy, @step, @start_date) 
            when @datepart in ('day', 'dd', 'd') then dateadd(dd, @step, @start_date) 
            when @datepart in ('week', 'wk', 'ww') then dateadd(ww, @step, @start_date) 
            when @datepart in ('hour', 'hh') then dateadd(hh, @step, @start_date) 
            when @datepart in ('minute', 'mi', 'n') then dateadd(n, @step, @start_date) 
            when @datepart in ('second', 'ss', 's') then dateadd(s, @step, @start_date) 
            when @datepart in ('millisecond', 'ms') then dateadd(ms, @step, @start_date) 
            else dateadd(dd, @step, @start_date)
        end)

        -- insert a new row
        insert 
            @dates 
        (
            start_date
            , end_date
        ) values (
            @start_date
            -- since MSSQL is only accurate to 3.33ms, we need to subtract 3 ms to get the upper range in time
            , dateadd(ms, -3, @new_start)
        )
        
        -- update the starting row
        set @start_date = @new_start
    end
    
    return 
end
GO
