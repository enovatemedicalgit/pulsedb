SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create FUNCTION [dbo].[fnHourlyMovesByWorkstation]
(	
	@FromDate datetime,
	@ToDate datetime,
	@SiteID int
)
RETURNS TABLE 
AS
RETURN 
(
	select start_date as [Date], convert(varchar, start_date, 101) + ' ' + right('0' + cast(DATEPART(hh, start_date) as varchar), 2) + ':00' as DateAndHour, @SiteID as SiteID, workstations.SerialNo, coalesce(md.Moves, 0) as Moves
	from generateDateTable(convert(varchar(10), @FromDate, 101) + ' ' + cast(datepart(hh, @FromDate) as varchar) + ':00', convert(varchar(10), @ToDate, 101) + ' ' + cast(datepart(hh, @ToDate) as varchar) + ':00', 'hour', 1) timetable
	cross join (select distinct SerialNo from MovementDetail where SiteID = @SiteID and [Date] between convert(varchar(10), @FromDate, 101) and convert(varchar(10), @ToDate, 101)) workstations
	left join (

				select SerialNo, DATEADD(hh, [Hour], [Date]) as [Date], [Hour], SUM(Moves) as Moves
				from MovementDetail
				where SiteID = @SiteID
				and [Date] between convert(varchar(10), @FromDate, 101) and convert(varchar(10), @ToDate, 101)
				group by SerialNo, [Date], [Hour]

			) md
	on timetable.start_date = md.[Date]
	and workstations.SerialNo = md.SerialNo
)

GO
