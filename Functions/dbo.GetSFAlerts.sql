SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE FUNCTION [dbo].[GetSFAlerts] ()
RETURNS TABLE AS
RETURN (Select 
		a.ROW_ID,
		a.SiteID as SiteID, 
		a.CreatedDateUTC as Dates, 
		b.SiteDescription,
		a.AlertConditionID,
		a.DeviceSerialNumber as DeviceSN, 
		a.BatterySerialNumber as BatterySN,   
		a.[Description], 
		c.LongDescription,
		a.SFCID,
		'Recommended Action: ' + c.FriendlyDescription as RecommendedAction, 
		CASE WHEN a.AlertStatus = 0 THEN 'New, No Action Taken' 
		END as [Status],
		CASE a.Notes 
			WHEN '' THEN '' 
			ELSE 'Tech Notes: ' + a.Notes 
		END as Notes, 	
		d.AssetNumber, 
		f.MACAddress as AccessPoint, 
		d.Wing, 
		d.[Floor], 
		a.RowPointer,
		a.TablePointer,
		CASE WHEN Coalesce(a.ModifiedUserID, 0) = 0 
			THEN '' else 'Enovate Tech: ' + Coalesce(u.FirstName, '') + ' ' + Coalesce(u.LastName, '') 
		END as CARE,
		Isnull(e.[Description],'Unallocated') as Department,   
		a.CreatedDateUTC,
		 ROW_NUMBER() over ( Partition by a.SiteID, a.DeviceSerialNumber, a.BatterySerialNumber 
		order by a.SiteID, a.DeviceSerialNumber, a.BatterySerialNumber)
		as [count]        
	    
		from AlertMessage a 
		left join AlertConditions c on a.AlertConditionID = c.ROW_ID 
		left join Sites b on b.IDSite = a.SiteID 
		left join [User] u on a.ModifiedUserID = u.IDUser 
		left join Assets d on d.SerialNo = a.DeviceSerialNumber 
		left join Departments e on e.IDDepartment = d.DepartmentID and e.SiteID = d.SiteID
		left join AccessPoint f on f.ROW_ID = d.AccessPointID 
	    
		where 
		(a.AlertStatus = 0) and a.Priority = 911 and a.AlertConditionID > 0 and a.CreatedDateUTC > convert(varchar(10), DATEADD(day, -10, getutcdate()), 111)
		--a.ROW_ID=4012035
		and left(a.[Description], 2) <> 'CR' 
		and a.BatterySerialNumber not like '%00000%'
		and a.DeviceSerialNumber not like '%00000%'
		and d.SiteID > 0		
		--and a.AlertConditionID <> 81		--Check Charger Fan notification
		--and a.APMAC not in (select distinct MACAddress from  AccessPoint where Description like '%WOW%' or Description like '%enovate%' or BusinessUnitID=28 or BusinessUnitID=4)
		and d.SiteID not in (select cast(SettingValue as int) from systemsettings where SettingKey = 'TestSiteID') and d.SiteID not in (597,4,
		687,
		689,
		691,
		692,
		--693,
		694,
		709,
		710,
		711,
		717,
		718,
		719,
		720,
		721,
		774,
		688,
		155, --Enovate Eval
		28 -- Enovate
		) AND b.CustomerID NOT IN (4)
)
		










GO
