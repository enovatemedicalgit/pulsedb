SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Danny Bates
-- Create date: 2012-12-17
-- Description:	Returns a table (one row)
--              containing the most recent
--              packet for the supplied
--              battery serial number
-- =============================================
CREATE FUNCTION [dbo].[MostRecentBatteryPacket](@BatterySerialNumber varchar(20))
RETURNS TABLE 
AS
RETURN 
(
			select top 1 * 
			from 
			(
				select top 1 *
					,'S' as SrcTbl
				from SessionDataCurrent where BatterySerialNumber = @BatterySerialNumber 
				--and CreatedDateUTC = (select LastPostDateUTC from Assets where SerialNo = @BatterySerialNumber)
				order by CreatedDateUtc desc
				union all
				select top 1 *
					,'N' as SrcTbl
				from NonSessionDataCurrent where BatterySerialNumber = @BatterySerialNumber 
				--and CreatedDateUTC = (select LastPostDateUTC from Assets where SerialNo = @BatterySerialNumber)
				order by CreatedDateUtc desc
			) packets order by CreatedDateUtc desc
)


GO
