SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Danny Bates
-- Create date: 2012-07-17
-- Description:	Returns daily run rate rows
--              for each battery, for
--              the supplied date range and
--              business unit
-- =============================================
create FUNCTION [dbo].[fnDailyRunRateByBattery] 
(	
	@FromDate datetime,
	@ToDate datetime,
	@BusinessUnitID int
)
RETURNS TABLE 
AS
RETURN 
(
	--set @FromDate = '6/13/2011 2:00:00 AM'
	--set @ToDate = '6/14/2011 6:00:00 PM'	
	--set @BusinessUnitID = 50

	select start_date as [Date], @BusinessUnitID as BusinessUnitID, batteries.BatterySerialNumber, coalesce(AvgRunRate, 0) as AvgRunRate
	from generateDateTable(convert(varchar(10), @FromDate, 101), convert(varchar(10), dateadd(d, -1, @ToDate), 101), 'day', 1) timetable
	cross join (select distinct BatterySerialNumber from SummaryBatteryRunRate where SiteID = @BusinessUnitID and [Date] between @FromDate and @ToDate) batteries
	left join (
				select [Date], BatterySerialNumber, AvgRunRate
				from [SummaryBatteryRunRate]
				where [Date] between @FromDate and @ToDate
				and SiteID = @BusinessUnitID
			) m
	on timetable.start_date = m.[Date]
	and batteries.BatterySerialNumber = m.BatterySerialNumber
)


GO
