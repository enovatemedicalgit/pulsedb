SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Danny Bates
-- Create date: 2012-07-18
-- Description:	Returns a value for identifying
--              whether a BATTERY serial number format 
--              is expected and/or valid
--
--              Return values:
--              0 Invalid serial number format
--              1 Valid serial number format
--              2 Demo serial number format
--
/*

	SerialNumberClass
	0 = Invalid (unexpected format, should not be kept or treated as a true serial number)
	1 = Valid (most useful, a true serial number)
	2 = Demo (data should be kept, but the serial number is artificial)


*/
-- =============================================
CREATE FUNCTION [dbo].[SerialNumberClassification] (@SerialNumber varchar(50))
RETURNS int
AS
BEGIN
	declare @retval int
	set @retval = case
		when len(@SerialNumber) != 15 then 0
		when LEFT(@SerialNumber, 7) = '3116605' then 0
		when lower(@SerialNumber) like '%[a-z]%' then 0
		when left(@SerialNumber,4) = '0000' then 0
		when left(@SerialNumber,6) = '311000' then 0
			when left(@SerialNumber,6) = '311999' then 0
		when Substring(@SerialNumber,8,2) = '00' then 0
		when Substring(@SerialNumber,10,2) = '00' then 0
		when left(@SerialNumber,3) = '000' then 0
		when RIGHT(@SerialNumber, 7) in ('0005100', '0005200', '0005300', '0005400', '0005500', '0005600', '0000030', '0000060') then 0
		when LEFT(@SerialNumber, 3) = '555' then 2
		else 1 
	end 
	return @retval
END
GO
