SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Coding Culprit: Deborah Cunningham
-- Create date: 9/6/2013
-- Description:	AH has increased on some batteries
-- this changes the original computation of how 
-- battery capacity is determined. There is now 
-- a table to keep track of what AH(Amp Hours) 
-- goes with what part number. 
--
-- At creation date it is called from:
-- CAST Web - Search & EquipmentTrending
-- spNonSessionDetail_GetTrending
-- spSessionDetail_GetTrending
-- =============================================
CREATE FUNCTION [dbo].[fnGetBatteryCapacity_Int]
(  @BattSN varchar(50) = null,
	@FCC int = 25000
)
RETURNS int
AS
BEGIN
	
	--test variables
	--declare @BattSN varchar(50) = '3118313130808659'
	--declare @FCC int = 3099
	
	Declare @Capacity int
	Declare @PartNumber varchar(10) = Left(@BattSN, 7)
	Declare @AssetType int
	Declare @AmpHours int = 
		(Select CalcAH from BatteryPartList where PartNumber =  @PartNumber ) 
	
	--if battery not listed / default to 22k
	If(@AmpHours is null)
	Begin
	
	   set @AmpHours  = 24500 
	End

	If(@AmpHours is null) 
	Begin
	set @Capacity = 0
		RETURN @Capacity
	End
	
	Set @Capacity= (Select Case
				When (@FCC >= (@AmpHours -1)) Then 100
				When (@FCC > 0) Then((@FCC * 100 /@AmpHours) )  
			Else 0 End)
	
	RETURN @Capacity

END



GO
