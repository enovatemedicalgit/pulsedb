SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Arlow Farrell
-- Create date: 2016-11-04
-- Description:	Returns daily utilization rows
--              for each workstation, for
--              the supplied date range and
--              business unit
-- =============================================
create FUNCTION [dbo].[fnDailyUtilizationByWorkstation] 
(	
	@FromDate datetime,
	@ToDate datetime,
	@SiteID int
)
RETURNS TABLE 
AS
RETURN 
(

	--declare @FromDate datetime
	--declare @ToDate datetime
	--declare @BusinessUnitID int
	--set @FromDate = '6/13/2011 2:00:00 AM'
	--set @ToDate = '6/14/2011 6:00:00 PM'	
	--set @BusinessUnitID = 50

	select start_date as [Date], @SiteID as SiteId, workstations.DeviceSerialNumber, cast(coalesce(m.Utilization, 0) * 100 as decimal(6,2)) as Utilization, coalesce(AvgRunRate, 0) as AvgRunRate
	from generateDateTable(convert(varchar(10), @FromDate, 101), convert(varchar(10), dateadd(d, -1, @ToDate), 101), 'day', 1) timetable
	cross join (select distinct DeviceSerialNumber from Movement where SiteID = @SiteID and [Date] between convert(varchar(10), @FromDate, 101) and convert(varchar(10), @ToDate, 101)) workstations
	left join (
				select
					[Date]
					,SiteID
					,HoursUtilized
					,DeviceSerialNumber
					,AvgRunRate
					,Utilization
				from Movement
				where SiteID = @SiteID
				and [Date] between convert(varchar(10), @FromDate, 101) and convert(varchar(10), @ToDate, 101)
			) m
	on timetable.start_date = m.[Date]
	and workstations.DeviceSerialNumber = m.DeviceSerialNumber
)

GO
