SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[prcUtilClean]
(
	@inputString VARCHAR(500)
)
RETURNS VARCHAR(500)
AS
BEGIN

    -- Two Spaces to one space
    SET @inputString = REPLACE(@inputString,'  ',' ');

    -- Turn to all caps
    SET @inputString = UPPER(@inputString);

    -- Directions
    SET @inputString = REPLACE(@inputString,' north',' N');
    SET @inputString = REPLACE(@inputString,' south',' S');
    SET @inputString = REPLACE(@inputString,' east',' E');
    SET @inputString = REPLACE(@inputString,' west',' W');
    SET @inputString = REPLACE(@inputString,' northeast',' NE');
    SET @inputString = REPLACE(@inputString,'  northwest','NW');
    SET @inputString = REPLACE(@inputString,'  southeast',' SE');
    SET @inputString = REPLACE(@inputString,'  southwest',' SW');

    -- Two Spaces to one space
    SET @inputString = REPLACE(@inputString,'  ',' ');

    RETURN @inputString;
END
GO
